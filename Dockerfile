####################################################################################################
# Steps taken to create this image                                                                 #
#                                                                                                  #
# docker build -t fiscalizador-contabil:1.0.0 .                                                    #
# docker run --name fiscalizador-contabil -p 8000:8000 --rm -d fiscalizador-contabil:1.0.0         #
# docker-machine ip default                                                                        #
#                                                                                                  #
####################################################################################################

FROM openjdk:11-jre-slim
LABEL maintainer 'André Santos <m1andsas@gmail.com>'

RUN mkdir -p /usr/app
COPY ["${WORKSPACE}/target/fiscalizador-contabil.jar", "/usr/app"]
WORKDIR /usr/app
EXPOSE 8000
ENTRYPOINT ["java","-jar","fiscalizador-contabil.jar"]

####################################################################################################
#                                                                                                  #
# End of file                                                                                      #
#                                                                                                  #
####################################################################################################
