package com.br.app.fiscal.domain;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Set;

import com.br.app.fiscal.model.Agagente;
import com.br.app.fiscal.model.Agagenteimpcnae;
import com.br.app.fiscal.model.Agagenteresp;
import com.br.app.fiscal.model.Agagentetiporeceita;
import com.br.app.fiscal.model.Agagentetransp;
import com.br.app.fiscal.model.Aggrupo;
import com.br.app.fiscal.model.Aglimcredito;
import com.br.app.fiscal.model.Aglimcreditosol;
import com.br.app.fiscal.model.Agtipo;
import com.br.app.fiscal.model.Agvistotipo;
import com.br.app.fiscal.model.Cobrplano;
import com.br.app.fiscal.model.Comtabela;
import com.br.app.fiscal.model.Confempr;
import com.br.app.fiscal.model.Confemprpart;
import com.br.app.fiscal.model.Contcontrato;
import com.br.app.fiscal.model.Crmchamado;
import com.br.app.fiscal.model.Ctbbem;
import com.br.app.fiscal.model.Ctbbemveicinfracao;
import com.br.app.fiscal.model.Ctbfechamento;
import com.br.app.fiscal.model.Ctbplanoconta;
import com.br.app.fiscal.model.Ecarquivamento;
import com.br.app.fiscal.model.Eccertificado;
import com.br.app.fiscal.model.Eccheckagenda;
import com.br.app.fiscal.model.Eccheckstatus;
import com.br.app.fiscal.model.Eccheckstatusagente;
import com.br.app.fiscal.model.Eccnd;
import com.br.app.fiscal.model.Eccorresplancamento;
import com.br.app.fiscal.model.Ecdecore;
import com.br.app.fiscal.model.Ecdoclancamento;
import com.br.app.fiscal.model.Ecpgtolancamento;
import com.br.app.fiscal.model.Finagencia;
import com.br.app.fiscal.model.Finconta;
import com.br.app.fiscal.model.Finemprestimo;
import com.br.app.fiscal.model.Finfluxoplanoconta;
import com.br.app.fiscal.model.Finlancamento;
import com.br.app.fiscal.model.Finoperadora;
import com.br.app.fiscal.model.Finplanoconta;
import com.br.app.fiscal.model.Fises;
import com.br.app.fiscal.model.Fisnatjur;
import com.br.app.fiscal.model.Folcolaborador;
import com.br.app.fiscal.model.Folcolaboradordep;
import com.br.app.fiscal.model.Folconvenio;
import com.br.app.fiscal.model.Folconveniotipo;
import com.br.app.fiscal.model.Folsindicato;
import com.br.app.fiscal.model.Gerimovel;
import com.br.app.fiscal.model.Gerveiculo;
import com.br.app.fiscal.model.Opoperacao;
import com.br.app.fiscal.model.Oporcdetag;
import com.br.app.fiscal.model.Optransacao;
import com.br.app.fiscal.model.Orcplanoconta;
import com.br.app.fiscal.model.Prdcplano;
import com.br.app.fiscal.model.Prdcplanoetapa;
import com.br.app.fiscal.model.Prodapolice;
import com.br.app.fiscal.model.Prodmarca;
import com.br.app.fiscal.model.Prtabela;

public class AgagenteDto {
	
    private Integer codagagente;
    private String nomeagagente;
    private String cnpjcpf;
    private String ierg;
    private String inscmun;
    private short nfeindfinal;
    private short nfeindiedest;
    private String inscsuframa;
    private short indoptributaria;
    private short indenquadramento;
    private short indtipoempresa;
    private String insccrc;
    private short indsubsttrib;
    private short indtipocomercio;
    private String passnumero;
    private Timestamp passemissao;
    private Timestamp passvalidade;
    private String estrnumero;
    private Timestamp estrvalidade;
    private String cnhnumero;
    private String cnhformulario;
    private Timestamp cnhexpedicao;
    private Timestamp cnhvalidade;
    private Timestamp cnhprimeirahab;
    private String eleitornumero;
    private String eleitorzona;
    private String eleitorsecao;
    private Timestamp eleitoremissao;
    private String reservnumero;
    private String reservlotacao;
    private Timestamp reservemissao;
    private Date datanascabertura;
    private Timestamp datasituacao;
    private Agtipo agtipo;
    private Finplanoconta finplanoconta;
    private Finplanoconta finplanoconta2;
    private Finfluxoplanoconta finfluxoplanoconta;
    private Finfluxoplanoconta finfluxoplanoconta2;
    private Orcplanoconta orcplanoconta;
    private Orcplanoconta orcplanoconta2;
    private Agvistotipo agvistotipo;
    private Agagentetiporeceita agagentetiporeceita;
    private Fisnatjur fisnatjur;
    private Aggrupo aggrupo;
    private Ctbplanoconta ctbplanoconta;
    private Ctbplanoconta ctbplanoconta2;
    private Set<Agagente> agagente7;
    private Agagente agagente6;
    private Set<Agagente> agagente5;
    private Agagente agagente4;
    private Cobrplano cobrplano;
    private Opoperacao opoperacao;
    private Prtabela prtabela;
    private Set<Agagenteimpcnae> agagenteimpcnae;
    private Set<Agagenteresp> agagenteresp;
    private Set<Agagentetransp> agagentetransp;
    private Set<Aglimcredito> aglimcredito;
    private Set<Aglimcreditosol> aglimcreditosol;
    private Set<Comtabela> comtabela;
    private Set<Confempr> confempr;
    private Set<Confemprpart> confemprpart;
    private Set<Contcontrato> contcontrato;
    private Set<Contcontrato> contcontrato2;
    private Set<Crmchamado> crmchamado;
    private Set<Ctbbem> ctbbem;
    private Set<Ctbbemveicinfracao> ctbbemveicinfracao;
    private Set<Ctbfechamento> ctbfechamento;
    private Set<Ctbfechamento> ctbfechamento2;
    private Set<Ecarquivamento> ecarquivamento;
    private Set<Eccertificado> eccertificado;
    private Set<Eccertificado> eccertificado2;
    private Set<Eccertificado> eccertificado3;
    private Set<Eccheckagenda> eccheckagenda;
    private Set<Eccheckstatus> eccheckstatus;
    private Set<Eccheckstatusagente> eccheckstatusagente;
    private Set<Eccnd> eccnd;
    private Set<Eccnd> eccnd2;
    private Set<Eccorresplancamento> eccorresplancamento;
    private Set<Eccorresplancamento> eccorresplancamento2;
    private Set<Ecdecore> ecdecore;
    private Set<Ecdoclancamento> ecdoclancamento;
    private Set<Ecpgtolancamento> ecpgtolancamento;
    private Set<Finagencia> finagencia;
    private Set<Finagencia> finagencia2;
    private Set<Finconta> finconta;
    private Set<Finemprestimo> finemprestimo;
    private Set<Finemprestimo> finemprestimo2;
    private Set<Finlancamento> finlancamento;
    private Set<Finoperadora> finoperadora;
    private Set<Fises> fises;
    private Set<Folcolaborador> folcolaborador;
    private Set<Folcolaborador> folcolaborador2;
    private Set<Folcolaborador> folcolaborador3;
    private Set<Folcolaboradordep> folcolaboradordep;
    private Set<Folconvenio> folconvenio;
    private Set<Folconveniotipo> folconveniotipo;
    private Set<Folsindicato> folsindicato;
    private Set<Gerimovel> gerimovel;
    private Set<Gerveiculo> gerveiculo;
    private Set<Oporcdetag> oporcdetag;
    private Set<Optransacao> optransacao;
    private Set<Prdcplano> prdcplano;
    private Set<Prdcplanoetapa> prdcplanoetapa;
    private Set<Prodapolice> prodapolice;
    private Set<Prodapolice> prodapolice2;
    private Set<Prodmarca> prodmarca;
    
    public AgagenteDto() {
    	super();
    }

	public Integer getCodagagente() {
		return codagagente;
	}

	public void setCodagagente(Integer codagagente) {
		this.codagagente = codagagente;
	}

	public String getNomeagagente() {
		return nomeagagente;
	}

	public void setNomeagagente(String nomeagagente) {
		this.nomeagagente = nomeagagente;
	}

	public String getCnpjcpf() {
		return cnpjcpf;
	}

	public void setCnpjcpf(String cnpjcpf) {
		this.cnpjcpf = cnpjcpf;
	}

	public String getIerg() {
		return ierg;
	}

	public void setIerg(String ierg) {
		this.ierg = ierg;
	}

	public String getInscmun() {
		return inscmun;
	}

	public void setInscmun(String inscmun) {
		this.inscmun = inscmun;
	}

	public short getNfeindfinal() {
		return nfeindfinal;
	}

	public void setNfeindfinal(short nfeindfinal) {
		this.nfeindfinal = nfeindfinal;
	}

	public short getNfeindiedest() {
		return nfeindiedest;
	}

	public void setNfeindiedest(short nfeindiedest) {
		this.nfeindiedest = nfeindiedest;
	}

	public String getInscsuframa() {
		return inscsuframa;
	}

	public void setInscsuframa(String inscsuframa) {
		this.inscsuframa = inscsuframa;
	}

	public short getIndoptributaria() {
		return indoptributaria;
	}

	public void setIndoptributaria(short indoptributaria) {
		this.indoptributaria = indoptributaria;
	}

	public short getIndenquadramento() {
		return indenquadramento;
	}

	public void setIndenquadramento(short indenquadramento) {
		this.indenquadramento = indenquadramento;
	}

	public short getIndtipoempresa() {
		return indtipoempresa;
	}

	public void setIndtipoempresa(short indtipoempresa) {
		this.indtipoempresa = indtipoempresa;
	}

	public String getInsccrc() {
		return insccrc;
	}

	public void setInsccrc(String insccrc) {
		this.insccrc = insccrc;
	}

	public short getIndsubsttrib() {
		return indsubsttrib;
	}

	public void setIndsubsttrib(short indsubsttrib) {
		this.indsubsttrib = indsubsttrib;
	}

	public short getIndtipocomercio() {
		return indtipocomercio;
	}

	public void setIndtipocomercio(short indtipocomercio) {
		this.indtipocomercio = indtipocomercio;
	}

	public String getPassnumero() {
		return passnumero;
	}

	public void setPassnumero(String passnumero) {
		this.passnumero = passnumero;
	}

	public Timestamp getPassemissao() {
		return passemissao;
	}

	public void setPassemissao(Timestamp passemissao) {
		this.passemissao = passemissao;
	}

	public Timestamp getPassvalidade() {
		return passvalidade;
	}

	public void setPassvalidade(Timestamp passvalidade) {
		this.passvalidade = passvalidade;
	}

	public String getEstrnumero() {
		return estrnumero;
	}

	public void setEstrnumero(String estrnumero) {
		this.estrnumero = estrnumero;
	}

	public Timestamp getEstrvalidade() {
		return estrvalidade;
	}

	public void setEstrvalidade(Timestamp estrvalidade) {
		this.estrvalidade = estrvalidade;
	}

	public String getCnhnumero() {
		return cnhnumero;
	}

	public void setCnhnumero(String cnhnumero) {
		this.cnhnumero = cnhnumero;
	}

	public String getCnhformulario() {
		return cnhformulario;
	}

	public void setCnhformulario(String cnhformulario) {
		this.cnhformulario = cnhformulario;
	}

	public Timestamp getCnhexpedicao() {
		return cnhexpedicao;
	}

	public void setCnhexpedicao(Timestamp cnhexpedicao) {
		this.cnhexpedicao = cnhexpedicao;
	}

	public Timestamp getCnhvalidade() {
		return cnhvalidade;
	}

	public void setCnhvalidade(Timestamp cnhvalidade) {
		this.cnhvalidade = cnhvalidade;
	}

	public Timestamp getCnhprimeirahab() {
		return cnhprimeirahab;
	}

	public void setCnhprimeirahab(Timestamp cnhprimeirahab) {
		this.cnhprimeirahab = cnhprimeirahab;
	}

	public String getEleitornumero() {
		return eleitornumero;
	}

	public void setEleitornumero(String eleitornumero) {
		this.eleitornumero = eleitornumero;
	}

	public String getEleitorzona() {
		return eleitorzona;
	}

	public void setEleitorzona(String eleitorzona) {
		this.eleitorzona = eleitorzona;
	}

	public String getEleitorsecao() {
		return eleitorsecao;
	}

	public void setEleitorsecao(String eleitorsecao) {
		this.eleitorsecao = eleitorsecao;
	}

	public Timestamp getEleitoremissao() {
		return eleitoremissao;
	}

	public void setEleitoremissao(Timestamp eleitoremissao) {
		this.eleitoremissao = eleitoremissao;
	}

	public String getReservnumero() {
		return reservnumero;
	}

	public void setReservnumero(String reservnumero) {
		this.reservnumero = reservnumero;
	}

	public String getReservlotacao() {
		return reservlotacao;
	}

	public void setReservlotacao(String reservlotacao) {
		this.reservlotacao = reservlotacao;
	}

	public Timestamp getReservemissao() {
		return reservemissao;
	}

	public void setReservemissao(Timestamp reservemissao) {
		this.reservemissao = reservemissao;
	}

	public Date getDatanascabertura() {
		return datanascabertura;
	}

	public void setDatanascabertura(Date datanascabertura) {
		this.datanascabertura = datanascabertura;
	}

	public Timestamp getDatasituacao() {
		return datasituacao;
	}

	public void setDatasituacao(Timestamp datasituacao) {
		this.datasituacao = datasituacao;
	}

	public Agtipo getAgtipo() {
		return agtipo;
	}

	public void setAgtipo(Agtipo agtipo) {
		this.agtipo = agtipo;
	}

	public Finplanoconta getFinplanoconta() {
		return finplanoconta;
	}

	public void setFinplanoconta(Finplanoconta finplanoconta) {
		this.finplanoconta = finplanoconta;
	}

	public Finplanoconta getFinplanoconta2() {
		return finplanoconta2;
	}

	public void setFinplanoconta2(Finplanoconta finplanoconta2) {
		this.finplanoconta2 = finplanoconta2;
	}

	public Finfluxoplanoconta getFinfluxoplanoconta() {
		return finfluxoplanoconta;
	}

	public void setFinfluxoplanoconta(Finfluxoplanoconta finfluxoplanoconta) {
		this.finfluxoplanoconta = finfluxoplanoconta;
	}

	public Finfluxoplanoconta getFinfluxoplanoconta2() {
		return finfluxoplanoconta2;
	}

	public void setFinfluxoplanoconta2(Finfluxoplanoconta finfluxoplanoconta2) {
		this.finfluxoplanoconta2 = finfluxoplanoconta2;
	}

	public Orcplanoconta getOrcplanoconta() {
		return orcplanoconta;
	}

	public void setOrcplanoconta(Orcplanoconta orcplanoconta) {
		this.orcplanoconta = orcplanoconta;
	}

	public Orcplanoconta getOrcplanoconta2() {
		return orcplanoconta2;
	}

	public void setOrcplanoconta2(Orcplanoconta orcplanoconta2) {
		this.orcplanoconta2 = orcplanoconta2;
	}

	public Agvistotipo getAgvistotipo() {
		return agvistotipo;
	}

	public void setAgvistotipo(Agvistotipo agvistotipo) {
		this.agvistotipo = agvistotipo;
	}

	public Agagentetiporeceita getAgagentetiporeceita() {
		return agagentetiporeceita;
	}

	public void setAgagentetiporeceita(Agagentetiporeceita agagentetiporeceita) {
		this.agagentetiporeceita = agagentetiporeceita;
	}

	public Fisnatjur getFisnatjur() {
		return fisnatjur;
	}

	public void setFisnatjur(Fisnatjur fisnatjur) {
		this.fisnatjur = fisnatjur;
	}

	public Aggrupo getAggrupo() {
		return aggrupo;
	}

	public void setAggrupo(Aggrupo aggrupo) {
		this.aggrupo = aggrupo;
	}

	public Ctbplanoconta getCtbplanoconta() {
		return ctbplanoconta;
	}

	public void setCtbplanoconta(Ctbplanoconta ctbplanoconta) {
		this.ctbplanoconta = ctbplanoconta;
	}

	public Ctbplanoconta getCtbplanoconta2() {
		return ctbplanoconta2;
	}

	public void setCtbplanoconta2(Ctbplanoconta ctbplanoconta2) {
		this.ctbplanoconta2 = ctbplanoconta2;
	}

	public Set<Agagente> getAgagente7() {
		return agagente7;
	}

	public void setAgagente7(Set<Agagente> agagente7) {
		this.agagente7 = agagente7;
	}

	public Agagente getAgagente6() {
		return agagente6;
	}

	public void setAgagente6(Agagente agagente6) {
		this.agagente6 = agagente6;
	}

	public Set<Agagente> getAgagente5() {
		return agagente5;
	}

	public void setAgagente5(Set<Agagente> agagente5) {
		this.agagente5 = agagente5;
	}

	public Agagente getAgagente4() {
		return agagente4;
	}

	public void setAgagente4(Agagente agagente4) {
		this.agagente4 = agagente4;
	}

	public Cobrplano getCobrplano() {
		return cobrplano;
	}

	public void setCobrplano(Cobrplano cobrplano) {
		this.cobrplano = cobrplano;
	}

	public Opoperacao getOpoperacao() {
		return opoperacao;
	}

	public void setOpoperacao(Opoperacao opoperacao) {
		this.opoperacao = opoperacao;
	}

	public Prtabela getPrtabela() {
		return prtabela;
	}

	public void setPrtabela(Prtabela prtabela) {
		this.prtabela = prtabela;
	}

	public Set<Agagenteimpcnae> getAgagenteimpcnae() {
		return agagenteimpcnae;
	}

	public void setAgagenteimpcnae(Set<Agagenteimpcnae> agagenteimpcnae) {
		this.agagenteimpcnae = agagenteimpcnae;
	}

	public Set<Agagenteresp> getAgagenteresp() {
		return agagenteresp;
	}

	public void setAgagenteresp(Set<Agagenteresp> agagenteresp) {
		this.agagenteresp = agagenteresp;
	}

	public Set<Agagentetransp> getAgagentetransp() {
		return agagentetransp;
	}

	public void setAgagentetransp(Set<Agagentetransp> agagentetransp) {
		this.agagentetransp = agagentetransp;
	}

	public Set<Aglimcredito> getAglimcredito() {
		return aglimcredito;
	}

	public void setAglimcredito(Set<Aglimcredito> aglimcredito) {
		this.aglimcredito = aglimcredito;
	}

	public Set<Aglimcreditosol> getAglimcreditosol() {
		return aglimcreditosol;
	}

	public void setAglimcreditosol(Set<Aglimcreditosol> aglimcreditosol) {
		this.aglimcreditosol = aglimcreditosol;
	}

	public Set<Comtabela> getComtabela() {
		return comtabela;
	}

	public void setComtabela(Set<Comtabela> comtabela) {
		this.comtabela = comtabela;
	}

	public Set<Confempr> getConfempr() {
		return confempr;
	}

	public void setConfempr(Set<Confempr> confempr) {
		this.confempr = confempr;
	}

	public Set<Confemprpart> getConfemprpart() {
		return confemprpart;
	}

	public void setConfemprpart(Set<Confemprpart> confemprpart) {
		this.confemprpart = confemprpart;
	}

	public Set<Contcontrato> getContcontrato() {
		return contcontrato;
	}

	public void setContcontrato(Set<Contcontrato> contcontrato) {
		this.contcontrato = contcontrato;
	}

	public Set<Contcontrato> getContcontrato2() {
		return contcontrato2;
	}

	public void setContcontrato2(Set<Contcontrato> contcontrato2) {
		this.contcontrato2 = contcontrato2;
	}

	public Set<Crmchamado> getCrmchamado() {
		return crmchamado;
	}

	public void setCrmchamado(Set<Crmchamado> crmchamado) {
		this.crmchamado = crmchamado;
	}

	public Set<Ctbbem> getCtbbem() {
		return ctbbem;
	}

	public void setCtbbem(Set<Ctbbem> ctbbem) {
		this.ctbbem = ctbbem;
	}

	public Set<Ctbbemveicinfracao> getCtbbemveicinfracao() {
		return ctbbemveicinfracao;
	}

	public void setCtbbemveicinfracao(Set<Ctbbemveicinfracao> ctbbemveicinfracao) {
		this.ctbbemveicinfracao = ctbbemveicinfracao;
	}

	public Set<Ctbfechamento> getCtbfechamento() {
		return ctbfechamento;
	}

	public void setCtbfechamento(Set<Ctbfechamento> ctbfechamento) {
		this.ctbfechamento = ctbfechamento;
	}

	public Set<Ctbfechamento> getCtbfechamento2() {
		return ctbfechamento2;
	}

	public void setCtbfechamento2(Set<Ctbfechamento> ctbfechamento2) {
		this.ctbfechamento2 = ctbfechamento2;
	}

	public Set<Ecarquivamento> getEcarquivamento() {
		return ecarquivamento;
	}

	public void setEcarquivamento(Set<Ecarquivamento> ecarquivamento) {
		this.ecarquivamento = ecarquivamento;
	}

	public Set<Eccertificado> getEccertificado() {
		return eccertificado;
	}

	public void setEccertificado(Set<Eccertificado> eccertificado) {
		this.eccertificado = eccertificado;
	}

	public Set<Eccertificado> getEccertificado2() {
		return eccertificado2;
	}

	public void setEccertificado2(Set<Eccertificado> eccertificado2) {
		this.eccertificado2 = eccertificado2;
	}

	public Set<Eccertificado> getEccertificado3() {
		return eccertificado3;
	}

	public void setEccertificado3(Set<Eccertificado> eccertificado3) {
		this.eccertificado3 = eccertificado3;
	}

	public Set<Eccheckagenda> getEccheckagenda() {
		return eccheckagenda;
	}

	public void setEccheckagenda(Set<Eccheckagenda> eccheckagenda) {
		this.eccheckagenda = eccheckagenda;
	}

	public Set<Eccheckstatus> getEccheckstatus() {
		return eccheckstatus;
	}

	public void setEccheckstatus(Set<Eccheckstatus> eccheckstatus) {
		this.eccheckstatus = eccheckstatus;
	}

	public Set<Eccheckstatusagente> getEccheckstatusagente() {
		return eccheckstatusagente;
	}

	public void setEccheckstatusagente(Set<Eccheckstatusagente> eccheckstatusagente) {
		this.eccheckstatusagente = eccheckstatusagente;
	}

	public Set<Eccnd> getEccnd() {
		return eccnd;
	}

	public void setEccnd(Set<Eccnd> eccnd) {
		this.eccnd = eccnd;
	}

	public Set<Eccnd> getEccnd2() {
		return eccnd2;
	}

	public void setEccnd2(Set<Eccnd> eccnd2) {
		this.eccnd2 = eccnd2;
	}

	public Set<Eccorresplancamento> getEccorresplancamento() {
		return eccorresplancamento;
	}

	public void setEccorresplancamento(Set<Eccorresplancamento> eccorresplancamento) {
		this.eccorresplancamento = eccorresplancamento;
	}

	public Set<Eccorresplancamento> getEccorresplancamento2() {
		return eccorresplancamento2;
	}

	public void setEccorresplancamento2(Set<Eccorresplancamento> eccorresplancamento2) {
		this.eccorresplancamento2 = eccorresplancamento2;
	}

	public Set<Ecdecore> getEcdecore() {
		return ecdecore;
	}

	public void setEcdecore(Set<Ecdecore> ecdecore) {
		this.ecdecore = ecdecore;
	}

	public Set<Ecdoclancamento> getEcdoclancamento() {
		return ecdoclancamento;
	}

	public void setEcdoclancamento(Set<Ecdoclancamento> ecdoclancamento) {
		this.ecdoclancamento = ecdoclancamento;
	}

	public Set<Ecpgtolancamento> getEcpgtolancamento() {
		return ecpgtolancamento;
	}

	public void setEcpgtolancamento(Set<Ecpgtolancamento> ecpgtolancamento) {
		this.ecpgtolancamento = ecpgtolancamento;
	}

	public Set<Finagencia> getFinagencia() {
		return finagencia;
	}

	public void setFinagencia(Set<Finagencia> finagencia) {
		this.finagencia = finagencia;
	}

	public Set<Finagencia> getFinagencia2() {
		return finagencia2;
	}

	public void setFinagencia2(Set<Finagencia> finagencia2) {
		this.finagencia2 = finagencia2;
	}

	public Set<Finconta> getFinconta() {
		return finconta;
	}

	public void setFinconta(Set<Finconta> finconta) {
		this.finconta = finconta;
	}

	public Set<Finemprestimo> getFinemprestimo() {
		return finemprestimo;
	}

	public void setFinemprestimo(Set<Finemprestimo> finemprestimo) {
		this.finemprestimo = finemprestimo;
	}

	public Set<Finemprestimo> getFinemprestimo2() {
		return finemprestimo2;
	}

	public void setFinemprestimo2(Set<Finemprestimo> finemprestimo2) {
		this.finemprestimo2 = finemprestimo2;
	}

	public Set<Finlancamento> getFinlancamento() {
		return finlancamento;
	}

	public void setFinlancamento(Set<Finlancamento> finlancamento) {
		this.finlancamento = finlancamento;
	}

	public Set<Finoperadora> getFinoperadora() {
		return finoperadora;
	}

	public void setFinoperadora(Set<Finoperadora> finoperadora) {
		this.finoperadora = finoperadora;
	}

	public Set<Fises> getFises() {
		return fises;
	}

	public void setFises(Set<Fises> fises) {
		this.fises = fises;
	}

	public Set<Folcolaborador> getFolcolaborador() {
		return folcolaborador;
	}

	public void setFolcolaborador(Set<Folcolaborador> folcolaborador) {
		this.folcolaborador = folcolaborador;
	}

	public Set<Folcolaborador> getFolcolaborador2() {
		return folcolaborador2;
	}

	public void setFolcolaborador2(Set<Folcolaborador> folcolaborador2) {
		this.folcolaborador2 = folcolaborador2;
	}

	public Set<Folcolaborador> getFolcolaborador3() {
		return folcolaborador3;
	}

	public void setFolcolaborador3(Set<Folcolaborador> folcolaborador3) {
		this.folcolaborador3 = folcolaborador3;
	}

	public Set<Folcolaboradordep> getFolcolaboradordep() {
		return folcolaboradordep;
	}

	public void setFolcolaboradordep(Set<Folcolaboradordep> folcolaboradordep) {
		this.folcolaboradordep = folcolaboradordep;
	}

	public Set<Folconvenio> getFolconvenio() {
		return folconvenio;
	}

	public void setFolconvenio(Set<Folconvenio> folconvenio) {
		this.folconvenio = folconvenio;
	}

	public Set<Folconveniotipo> getFolconveniotipo() {
		return folconveniotipo;
	}

	public void setFolconveniotipo(Set<Folconveniotipo> folconveniotipo) {
		this.folconveniotipo = folconveniotipo;
	}

	public Set<Folsindicato> getFolsindicato() {
		return folsindicato;
	}

	public void setFolsindicato(Set<Folsindicato> folsindicato) {
		this.folsindicato = folsindicato;
	}

	public Set<Gerimovel> getGerimovel() {
		return gerimovel;
	}

	public void setGerimovel(Set<Gerimovel> gerimovel) {
		this.gerimovel = gerimovel;
	}

	public Set<Gerveiculo> getGerveiculo() {
		return gerveiculo;
	}

	public void setGerveiculo(Set<Gerveiculo> gerveiculo) {
		this.gerveiculo = gerveiculo;
	}

	public Set<Oporcdetag> getOporcdetag() {
		return oporcdetag;
	}

	public void setOporcdetag(Set<Oporcdetag> oporcdetag) {
		this.oporcdetag = oporcdetag;
	}

	public Set<Optransacao> getOptransacao() {
		return optransacao;
	}

	public void setOptransacao(Set<Optransacao> optransacao) {
		this.optransacao = optransacao;
	}

	public Set<Prdcplano> getPrdcplano() {
		return prdcplano;
	}

	public void setPrdcplano(Set<Prdcplano> prdcplano) {
		this.prdcplano = prdcplano;
	}

	public Set<Prdcplanoetapa> getPrdcplanoetapa() {
		return prdcplanoetapa;
	}

	public void setPrdcplanoetapa(Set<Prdcplanoetapa> prdcplanoetapa) {
		this.prdcplanoetapa = prdcplanoetapa;
	}

	public Set<Prodapolice> getProdapolice() {
		return prodapolice;
	}

	public void setProdapolice(Set<Prodapolice> prodapolice) {
		this.prodapolice = prodapolice;
	}

	public Set<Prodapolice> getProdapolice2() {
		return prodapolice2;
	}

	public void setProdapolice2(Set<Prodapolice> prodapolice2) {
		this.prodapolice2 = prodapolice2;
	}

	public Set<Prodmarca> getProdmarca() {
		return prodmarca;
	}

	public void setProdmarca(Set<Prodmarca> prodmarca) {
		this.prodmarca = prodmarca;
	}    

}