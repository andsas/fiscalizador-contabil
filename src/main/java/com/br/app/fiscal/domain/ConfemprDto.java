package com.br.app.fiscal.domain;

import java.util.Set;

import com.br.app.fiscal.model.Agagente;
import com.br.app.fiscal.model.Agconf;
import com.br.app.fiscal.model.Comtabela;
import com.br.app.fiscal.model.Confempr;
import com.br.app.fiscal.model.Confemprpart;
import com.br.app.fiscal.model.Confplanoconta;
import com.br.app.fiscal.model.Ctbbem;
import com.br.app.fiscal.model.Ctbfechamento;
import com.br.app.fiscal.model.Ctblancamento;
import com.br.app.fiscal.model.Ctbplanoconta;
import com.br.app.fiscal.model.Ecconf;
import com.br.app.fiscal.model.Ecdoclancamento;
import com.br.app.fiscal.model.Fincaixa;
import com.br.app.fiscal.model.Finconf;
import com.br.app.fiscal.model.Finfluxoplanoconta;
import com.br.app.fiscal.model.Finplanoconta;
import com.br.app.fiscal.model.Folcolaborador;
import com.br.app.fiscal.model.Folhorariotrabalho;
import com.br.app.fiscal.model.Impoptributaria;
import com.br.app.fiscal.model.Impplanofiscal;
import com.br.app.fiscal.model.Opconfentrada;
import com.br.app.fiscal.model.Opconfsaida;
import com.br.app.fiscal.model.Oporc;
import com.br.app.fiscal.model.Opsolicitacao;
import com.br.app.fiscal.model.Orcplanoconta;

public class ConfemprDto {

	private Integer codconfempr;
	private String nomeconfempr;
	private String nomefantasia;
	private String inscest;
	private String cnpj;
	private String nfecertificado;
	private String nfeenviopath;
	private String nfecancpath;
	private String nfeinupath;
	private String nfeconspath;
	private String logo;
	private short nfeambiente;
	private short tipoconfpaiempr;
	private String emailhost;
	private String emailnome;
	private String emaillogin;
	private String emailsenha;
	private short emailssl;
	private short emailseg;
	private short emailcopia;
	private int emailporta;
	private String nfsecertificado;
	private String nfseenviopath;
	private short nfseambiente;
	private int cormenu;
	private Set<Agconf> agconf;
	private Set<Comtabela> comtabela;
	private Agagente agagente;
	private Set<Confempr> confempr3;
	private Confempr confempr2;
	private Set<Confemprpart> confemprpart;
	private Set<Confplanoconta> confplanoconta;
	private Set<Ctbbem> ctbbem;
	private Set<Ctbfechamento> ctbfechamento;
	private Set<Ctblancamento> ctblancamento;
	private Set<Ctbplanoconta> ctbplanoconta;
	private Set<Ecconf> ecconf;
	private Set<Ecdoclancamento> ecdoclancamento;
	private Set<Fincaixa> fincaixa;
	private Set<Finconf> finconf;
	private Set<Finfluxoplanoconta> finfluxoplanoconta;
	private Set<Finplanoconta> finplanoconta;
	private Set<Folcolaborador> folcolaborador;
	private Set<Folhorariotrabalho> folhorariotrabalho;
	private Set<Impoptributaria> impoptributaria;
	private Set<Impplanofiscal> impplanofiscal;
	private Set<Opconfentrada> opconfentrada;
	private Set<Opconfsaida> opconfsaida;
	private Set<Oporc> oporc;
	private Set<Opsolicitacao> opsolicitacao;
	private Set<Orcplanoconta> orcplanoconta;

	public ConfemprDto() {
		super();
	}

	public Integer getCodconfempr() {
		return codconfempr;
	}

	public void setCodconfempr(Integer codconfempr) {
		this.codconfempr = codconfempr;
	}

	public String getNomeconfempr() {
		return nomeconfempr;
	}

	public void setNomeconfempr(String nomeconfempr) {
		this.nomeconfempr = nomeconfempr;
	}

	public String getNomefantasia() {
		return nomefantasia;
	}

	public void setNomefantasia(String nomefantasia) {
		this.nomefantasia = nomefantasia;
	}

	public String getInscest() {
		return inscest;
	}

	public void setInscest(String inscest) {
		this.inscest = inscest;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNfecertificado() {
		return nfecertificado;
	}

	public void setNfecertificado(String nfecertificado) {
		this.nfecertificado = nfecertificado;
	}

	public String getNfeenviopath() {
		return nfeenviopath;
	}

	public void setNfeenviopath(String nfeenviopath) {
		this.nfeenviopath = nfeenviopath;
	}

	public String getNfecancpath() {
		return nfecancpath;
	}

	public void setNfecancpath(String nfecancpath) {
		this.nfecancpath = nfecancpath;
	}

	public String getNfeinupath() {
		return nfeinupath;
	}

	public void setNfeinupath(String nfeinupath) {
		this.nfeinupath = nfeinupath;
	}

	public String getNfeconspath() {
		return nfeconspath;
	}

	public void setNfeconspath(String nfeconspath) {
		this.nfeconspath = nfeconspath;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public short getNfeambiente() {
		return nfeambiente;
	}

	public void setNfeambiente(short nfeambiente) {
		this.nfeambiente = nfeambiente;
	}

	public short getTipoconfpaiempr() {
		return tipoconfpaiempr;
	}

	public void setTipoconfpaiempr(short tipoconfpaiempr) {
		this.tipoconfpaiempr = tipoconfpaiempr;
	}

	public String getEmailhost() {
		return emailhost;
	}

	public void setEmailhost(String emailhost) {
		this.emailhost = emailhost;
	}

	public String getEmailnome() {
		return emailnome;
	}

	public void setEmailnome(String emailnome) {
		this.emailnome = emailnome;
	}

	public String getEmaillogin() {
		return emaillogin;
	}

	public void setEmaillogin(String emaillogin) {
		this.emaillogin = emaillogin;
	}

	public String getEmailsenha() {
		return emailsenha;
	}

	public void setEmailsenha(String emailsenha) {
		this.emailsenha = emailsenha;
	}

	public short getEmailssl() {
		return emailssl;
	}

	public void setEmailssl(short emailssl) {
		this.emailssl = emailssl;
	}

	public short getEmailseg() {
		return emailseg;
	}

	public void setEmailseg(short emailseg) {
		this.emailseg = emailseg;
	}

	public short getEmailcopia() {
		return emailcopia;
	}

	public void setEmailcopia(short emailcopia) {
		this.emailcopia = emailcopia;
	}

	public int getEmailporta() {
		return emailporta;
	}

	public void setEmailporta(int emailporta) {
		this.emailporta = emailporta;
	}

	public String getNfsecertificado() {
		return nfsecertificado;
	}

	public void setNfsecertificado(String nfsecertificado) {
		this.nfsecertificado = nfsecertificado;
	}

	public String getNfseenviopath() {
		return nfseenviopath;
	}

	public void setNfseenviopath(String nfseenviopath) {
		this.nfseenviopath = nfseenviopath;
	}

	public short getNfseambiente() {
		return nfseambiente;
	}

	public void setNfseambiente(short nfseambiente) {
		this.nfseambiente = nfseambiente;
	}

	public int getCormenu() {
		return cormenu;
	}

	public void setCormenu(int cormenu) {
		this.cormenu = cormenu;
	}

	public Set<Agconf> getAgconf() {
		return agconf;
	}

	public void setAgconf(Set<Agconf> agconf) {
		this.agconf = agconf;
	}

	public Set<Comtabela> getComtabela() {
		return comtabela;
	}

	public void setComtabela(Set<Comtabela> comtabela) {
		this.comtabela = comtabela;
	}

	public Agagente getAgagente() {
		return agagente;
	}

	public void setAgagente(Agagente agagente) {
		this.agagente = agagente;
	}

	public Set<Confempr> getConfempr3() {
		return confempr3;
	}

	public void setConfempr3(Set<Confempr> confempr3) {
		this.confempr3 = confempr3;
	}

	public Confempr getConfempr2() {
		return confempr2;
	}

	public void setConfempr2(Confempr confempr2) {
		this.confempr2 = confempr2;
	}

	public Set<Confemprpart> getConfemprpart() {
		return confemprpart;
	}

	public void setConfemprpart(Set<Confemprpart> confemprpart) {
		this.confemprpart = confemprpart;
	}

	public Set<Confplanoconta> getConfplanoconta() {
		return confplanoconta;
	}

	public void setConfplanoconta(Set<Confplanoconta> confplanoconta) {
		this.confplanoconta = confplanoconta;
	}

	public Set<Ctbbem> getCtbbem() {
		return ctbbem;
	}

	public void setCtbbem(Set<Ctbbem> ctbbem) {
		this.ctbbem = ctbbem;
	}

	public Set<Ctbfechamento> getCtbfechamento() {
		return ctbfechamento;
	}

	public void setCtbfechamento(Set<Ctbfechamento> ctbfechamento) {
		this.ctbfechamento = ctbfechamento;
	}

	public Set<Ctblancamento> getCtblancamento() {
		return ctblancamento;
	}

	public void setCtblancamento(Set<Ctblancamento> ctblancamento) {
		this.ctblancamento = ctblancamento;
	}

	public Set<Ctbplanoconta> getCtbplanoconta() {
		return ctbplanoconta;
	}

	public void setCtbplanoconta(Set<Ctbplanoconta> ctbplanoconta) {
		this.ctbplanoconta = ctbplanoconta;
	}

	public Set<Ecconf> getEcconf() {
		return ecconf;
	}

	public void setEcconf(Set<Ecconf> ecconf) {
		this.ecconf = ecconf;
	}

	public Set<Ecdoclancamento> getEcdoclancamento() {
		return ecdoclancamento;
	}

	public void setEcdoclancamento(Set<Ecdoclancamento> ecdoclancamento) {
		this.ecdoclancamento = ecdoclancamento;
	}

	public Set<Fincaixa> getFincaixa() {
		return fincaixa;
	}

	public void setFincaixa(Set<Fincaixa> fincaixa) {
		this.fincaixa = fincaixa;
	}

	public Set<Finconf> getFinconf() {
		return finconf;
	}

	public void setFinconf(Set<Finconf> finconf) {
		this.finconf = finconf;
	}

	public Set<Finfluxoplanoconta> getFinfluxoplanoconta() {
		return finfluxoplanoconta;
	}

	public void setFinfluxoplanoconta(Set<Finfluxoplanoconta> finfluxoplanoconta) {
		this.finfluxoplanoconta = finfluxoplanoconta;
	}

	public Set<Finplanoconta> getFinplanoconta() {
		return finplanoconta;
	}

	public void setFinplanoconta(Set<Finplanoconta> finplanoconta) {
		this.finplanoconta = finplanoconta;
	}

	public Set<Folcolaborador> getFolcolaborador() {
		return folcolaborador;
	}

	public void setFolcolaborador(Set<Folcolaborador> folcolaborador) {
		this.folcolaborador = folcolaborador;
	}

	public Set<Folhorariotrabalho> getFolhorariotrabalho() {
		return folhorariotrabalho;
	}

	public void setFolhorariotrabalho(Set<Folhorariotrabalho> folhorariotrabalho) {
		this.folhorariotrabalho = folhorariotrabalho;
	}

	public Set<Impoptributaria> getImpoptributaria() {
		return impoptributaria;
	}

	public void setImpoptributaria(Set<Impoptributaria> impoptributaria) {
		this.impoptributaria = impoptributaria;
	}

	public Set<Impplanofiscal> getImpplanofiscal() {
		return impplanofiscal;
	}

	public void setImpplanofiscal(Set<Impplanofiscal> impplanofiscal) {
		this.impplanofiscal = impplanofiscal;
	}

	public Set<Opconfentrada> getOpconfentrada() {
		return opconfentrada;
	}

	public void setOpconfentrada(Set<Opconfentrada> opconfentrada) {
		this.opconfentrada = opconfentrada;
	}

	public Set<Opconfsaida> getOpconfsaida() {
		return opconfsaida;
	}

	public void setOpconfsaida(Set<Opconfsaida> opconfsaida) {
		this.opconfsaida = opconfsaida;
	}

	public Set<Oporc> getOporc() {
		return oporc;
	}

	public void setOporc(Set<Oporc> oporc) {
		this.oporc = oporc;
	}

	public Set<Opsolicitacao> getOpsolicitacao() {
		return opsolicitacao;
	}

	public void setOpsolicitacao(Set<Opsolicitacao> opsolicitacao) {
		this.opsolicitacao = opsolicitacao;
	}

	public Set<Orcplanoconta> getOrcplanoconta() {
		return orcplanoconta;
	}

	public void setOrcplanoconta(Set<Orcplanoconta> orcplanoconta) {
		this.orcplanoconta = orcplanoconta;
	}

}
