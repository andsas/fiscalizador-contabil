package com.br.app.fiscal.domain;

import com.br.app.fiscal.model.Confempr;

public class ConfplanocontaDto {
	
	private Integer codconfplanoconta;
	private short tipoconfplanoconta;
    private String mascara;
    private String mascstr;
    private short niveis;
    private String nivel1;
    private String nivel2;
    private String nivel3;
    private String nivel4;
    private String nivel5;
    private String nivel6;
    private String nivel7;
    private String nivel8;
    private String nivel9;
    private Confempr confempr;

    public ConfplanocontaDto() {
        super();
    }

	public Integer getCodconfplanoconta() {
		return codconfplanoconta;
	}

	public void setCodconfplanoconta(Integer codconfplanoconta) {
		this.codconfplanoconta = codconfplanoconta;
	}

	public short getTipoconfplanoconta() {
		return tipoconfplanoconta;
	}

	public void setTipoconfplanoconta(short tipoconfplanoconta) {
		this.tipoconfplanoconta = tipoconfplanoconta;
	}

	public String getMascara() {
		return mascara;
	}

	public void setMascara(String mascara) {
		this.mascara = mascara;
	}

	public String getMascstr() {
		return mascstr;
	}

	public void setMascstr(String mascstr) {
		this.mascstr = mascstr;
	}

	public short getNiveis() {
		return niveis;
	}

	public void setNiveis(short niveis) {
		this.niveis = niveis;
	}

	public String getNivel1() {
		return nivel1;
	}

	public void setNivel1(String nivel1) {
		this.nivel1 = nivel1;
	}

	public String getNivel2() {
		return nivel2;
	}

	public void setNivel2(String nivel2) {
		this.nivel2 = nivel2;
	}

	public String getNivel3() {
		return nivel3;
	}

	public void setNivel3(String nivel3) {
		this.nivel3 = nivel3;
	}

	public String getNivel4() {
		return nivel4;
	}

	public void setNivel4(String nivel4) {
		this.nivel4 = nivel4;
	}

	public String getNivel5() {
		return nivel5;
	}

	public void setNivel5(String nivel5) {
		this.nivel5 = nivel5;
	}

	public String getNivel6() {
		return nivel6;
	}

	public void setNivel6(String nivel6) {
		this.nivel6 = nivel6;
	}

	public String getNivel7() {
		return nivel7;
	}

	public void setNivel7(String nivel7) {
		this.nivel7 = nivel7;
	}

	public String getNivel8() {
		return nivel8;
	}

	public void setNivel8(String nivel8) {
		this.nivel8 = nivel8;
	}

	public String getNivel9() {
		return nivel9;
	}

	public void setNivel9(String nivel9) {
		this.nivel9 = nivel9;
	}

	public Confempr getConfempr() {
		return confempr;
	}

	public void setConfempr(Confempr confempr) {
		this.confempr = confempr;
	}

}