package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="AGAGENTE")
public class Agagente implements Serializable {	
	
	private static final long serialVersionUID = 1L;

	/** Primary key. */
    protected static final String PK = "codagagente";

    @Id
    @Column(name="CODAGAGENTE", unique=true, nullable=false, precision=10)
    private Integer codagagente;
    @Column(name="NOMEAGAGENTE", nullable=false, length=250)
    private String nomeagagente;
    @Column(name="CNPJCPF", length=20)
    private String cnpjcpf;
    @Column(name="IERG", length=20)
    private String ierg;
    @Column(name="INSCMUN", length=20)
    private String inscmun;
    @Column(name="NFEINDFINAL", precision=5)
    private short nfeindfinal;
    @Column(name="NFEINDIEDEST", precision=5)
    private short nfeindiedest;
    @Column(name="INSCSUFRAMA", length=20)
    private String inscsuframa;
    @Column(name="INDOPTRIBUTARIA", precision=5)
    private short indoptributaria;
    @Column(name="INDENQUADRAMENTO", precision=5)
    private short indenquadramento;
    @Column(name="INDTIPOEMPRESA", precision=5)
    private short indtipoempresa;
    @Column(name="INSCCRC", length=20)
    private String insccrc;
    @Column(name="INDSUBSTTRIB", precision=5)
    private short indsubsttrib;
    @Column(name="INDTIPOCOMERCIO", precision=5)
    private short indtipocomercio;
    @Column(name="PASSNUMERO", length=40)
    private String passnumero;
    @Column(name="PASSEMISSAO")
    private Timestamp passemissao;
    @Column(name="PASSVALIDADE")
    private Timestamp passvalidade;
    @Column(name="ESTRNUMERO", length=40)
    private String estrnumero;
    @Column(name="ESTRVALIDADE")
    private Timestamp estrvalidade;
    @Column(name="CNHNUMERO", length=40)
    private String cnhnumero;
    @Column(name="CNHFORMULARIO", length=40)
    private String cnhformulario;
    @Column(name="CNHEXPEDICAO")
    private Timestamp cnhexpedicao;
    @Column(name="CNHVALIDADE")
    private Timestamp cnhvalidade;
    @Column(name="CNHPRIMEIRAHAB")
    private Timestamp cnhprimeirahab;
    @Column(name="ELEITORNUMERO", length=40)
    private String eleitornumero;
    @Column(name="ELEITORZONA", length=40)
    private String eleitorzona;
    @Column(name="ELEITORSECAO", length=40)
    private String eleitorsecao;
    @Column(name="ELEITOREMISSAO")
    private Timestamp eleitoremissao;
    @Column(name="RESERVNUMERO", length=40)
    private String reservnumero;
    @Column(name="RESERVLOTACAO", length=40)
    private String reservlotacao;
    @Column(name="RESERVEMISSAO")
    private Timestamp reservemissao;
    @Column(name="DATANASCABERTURA")
    private Date datanascabertura;
    @Column(name="DATASITUACAO")
    private Timestamp datasituacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGTIPO", nullable=false)
    private Agtipo agtipo;
    @ManyToOne
    @JoinColumn(name="FINCONTACREDITO")
    private Finplanoconta finplanoconta;
    @ManyToOne
    @JoinColumn(name="FINCONTADEBITO")
    private Finplanoconta finplanoconta2;
    @ManyToOne
    @JoinColumn(name="FLUXOCONTACREDITO")
    private Finfluxoplanoconta finfluxoplanoconta;
    @ManyToOne
    @JoinColumn(name="FLUXOCONTADEBITO")
    private Finfluxoplanoconta finfluxoplanoconta2;
    @ManyToOne
    @JoinColumn(name="ORCCONTACREDITO")
    private Orcplanoconta orcplanoconta;
    @ManyToOne
    @JoinColumn(name="ORCCONTADEBITO")
    private Orcplanoconta orcplanoconta2;
    @ManyToOne
    @JoinColumn(name="CODAGVISTOTIPO")
    private Agvistotipo agvistotipo;
    @ManyToOne
    @JoinColumn(name="CODAGRECEITAAGENTETIPO")
    private Agagentetiporeceita agagentetiporeceita;
    @ManyToOne
    @JoinColumn(name="CODAGNATJURAGENTE")
    private Fisnatjur fisnatjur;
    @ManyToOne
    @JoinColumn(name="CODAGGRUPO")
    private Aggrupo aggrupo;
    @ManyToOne
    @JoinColumn(name="CTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta2;
    @OneToMany(mappedBy="agagente6")
    private Set<Agagente> agagente7;
    @ManyToOne
    @JoinColumn(name="CODAGTRANSP")
    private Agagente agagente6;
    @OneToMany(mappedBy="agagente4")
    private Set<Agagente> agagente5;
    @ManyToOne
    @JoinColumn(name="CODAGVENDEDOR")
    private Agagente agagente4;
    @ManyToOne
    @JoinColumn(name="CODAGCOBRPLANO")
    private Cobrplano cobrplano;
    @ManyToOne
    @JoinColumn(name="CODAGOPOPERACAO")
    private Opoperacao opoperacao;
    @ManyToOne
    @JoinColumn(name="CODAGPRTABELA")
    private Prtabela prtabela;
    @OneToMany(mappedBy="agagente")
    private Set<Agagenteimpcnae> agagenteimpcnae;
    @OneToMany(mappedBy="agagente")
    private Set<Agagenteresp> agagenteresp;
    @OneToMany(mappedBy="agagente")
    private Set<Agagentetransp> agagentetransp;
    @OneToMany(mappedBy="agagente")
    private Set<Aglimcredito> aglimcredito;
    @OneToMany(mappedBy="agagente")
    private Set<Aglimcreditosol> aglimcreditosol;
    @OneToMany(mappedBy="agagente")
    private Set<Comtabela> comtabela;
    @OneToMany(mappedBy="agagente")
    private Set<Confempr> confempr;
    @OneToMany(mappedBy="agagente")
    private Set<Confemprpart> confemprpart;
    @OneToMany(mappedBy="agagente")
    private Set<Contcontrato> contcontrato;
    @OneToMany(mappedBy="agagente2")
    private Set<Contcontrato> contcontrato2;
    @OneToMany(mappedBy="agagente")
    private Set<Crmchamado> crmchamado;
    @OneToMany(mappedBy="agagente")
    private Set<Ctbbem> ctbbem;
    @OneToMany(mappedBy="agagente")
    private Set<Ctbbemveicinfracao> ctbbemveicinfracao;
    @OneToMany(mappedBy="agagente")
    private Set<Ctbfechamento> ctbfechamento;
    @OneToMany(mappedBy="agagente2")
    private Set<Ctbfechamento> ctbfechamento2;
    @OneToMany(mappedBy="agagente")
    private Set<Ecarquivamento> ecarquivamento;
    @OneToMany(mappedBy="agagente")
    private Set<Eccertificado> eccertificado;
    @OneToMany(mappedBy="agagente2")
    private Set<Eccertificado> eccertificado2;
    @OneToMany(mappedBy="agagente3")
    private Set<Eccertificado> eccertificado3;
    @OneToMany(mappedBy="agagente")
    private Set<Eccheckagenda> eccheckagenda;
    @OneToMany(mappedBy="agagente")
    private Set<Eccheckstatus> eccheckstatus;
    @OneToMany(mappedBy="agagente")
    private Set<Eccheckstatusagente> eccheckstatusagente;
    @OneToMany(mappedBy="agagente")
    private Set<Eccnd> eccnd;
    @OneToMany(mappedBy="agagente2")
    private Set<Eccnd> eccnd2;
    @OneToMany(mappedBy="agagente")
    private Set<Eccorresplancamento> eccorresplancamento;
    @OneToMany(mappedBy="agagente2")
    private Set<Eccorresplancamento> eccorresplancamento2;
    @OneToMany(mappedBy="agagente")
    private Set<Ecdecore> ecdecore;
    @OneToMany(mappedBy="agagente")
    private Set<Ecdoclancamento> ecdoclancamento;
    @OneToMany(mappedBy="agagente")
    private Set<Ecpgtolancamento> ecpgtolancamento;
    @OneToMany(mappedBy="agagente")
    private Set<Finagencia> finagencia;
    @OneToMany(mappedBy="agagente2")
    private Set<Finagencia> finagencia2;
    @OneToMany(mappedBy="agagente")
    private Set<Finconta> finconta;
    @OneToMany(mappedBy="agagente")
    private Set<Finemprestimo> finemprestimo;
    @OneToMany(mappedBy="agagente2")
    private Set<Finemprestimo> finemprestimo2;
    @OneToMany(mappedBy="agagente")
    private Set<Finlancamento> finlancamento;
    @OneToMany(mappedBy="agagente")
    private Set<Finoperadora> finoperadora;
    @OneToMany(mappedBy="agagente")
    private Set<Fises> fises;
    @OneToMany(mappedBy="agagente")
    private Set<Folcolaborador> folcolaborador;
    @OneToMany(mappedBy="agagente2")
    private Set<Folcolaborador> folcolaborador2;
    @OneToMany(mappedBy="agagente3")
    private Set<Folcolaborador> folcolaborador3;
    @OneToMany(mappedBy="agagente")
    private Set<Folcolaboradordep> folcolaboradordep;
    @OneToMany(mappedBy="agagente")
    private Set<Folconvenio> folconvenio;
    @OneToMany(mappedBy="agagente")
    private Set<Folconveniotipo> folconveniotipo;
    @OneToMany(mappedBy="agagente")
    private Set<Folsindicato> folsindicato;
    @OneToMany(mappedBy="agagente")
    private Set<Gerimovel> gerimovel;
    @OneToMany(mappedBy="agagente")
    private Set<Gerveiculo> gerveiculo;
    @OneToMany(mappedBy="agagente")
    private Set<Oporcdetag> oporcdetag;
    @OneToMany(mappedBy="agagente")
    private Set<Optransacao> optransacao;
    @OneToMany(mappedBy="agagente")
    private Set<Prdcplano> prdcplano;
    @OneToMany(mappedBy="agagente")
    private Set<Prdcplanoetapa> prdcplanoetapa;
    @OneToMany(mappedBy="agagente")
    private Set<Prodapolice> prodapolice;
    @OneToMany(mappedBy="agagente2")
    private Set<Prodapolice> prodapolice2;
    @OneToMany(mappedBy="agagente")
    private Set<Prodmarca> prodmarca;

    /** Default constructor. */
    public Agagente() {
        super();
    }

	/**
     * Access method for codagagente.
     *
     * @return the current value of codagagente
     */
    public Integer getCodagagente() {
        return codagagente;
    }

    /**
     * Setter method for codagagente.
     *
     * @param aCodagagente the new value for codagagente
     */
    public void setCodagagente(Integer aCodagagente) {
        codagagente = aCodagagente;
    }

    /**
     * Access method for nomeagagente.
     *
     * @return the current value of nomeagagente
     */
    public String getNomeagagente() {
        return nomeagagente;
    }

    /**
     * Setter method for nomeagagente.
     *
     * @param aNomeagagente the new value for nomeagagente
     */
    public void setNomeagagente(String aNomeagagente) {
        nomeagagente = aNomeagagente;
    }

    /**
     * Access method for cnpjcpf.
     *
     * @return the current value of cnpjcpf
     */
    public String getCnpjcpf() {
        return cnpjcpf;
    }

    /**
     * Setter method for cnpjcpf.
     *
     * @param aCnpjcpf the new value for cnpjcpf
     */
    public void setCnpjcpf(String aCnpjcpf) {
        cnpjcpf = aCnpjcpf;
    }

    /**
     * Access method for ierg.
     *
     * @return the current value of ierg
     */
    public String getIerg() {
        return ierg;
    }

    /**
     * Setter method for ierg.
     *
     * @param aIerg the new value for ierg
     */
    public void setIerg(String aIerg) {
        ierg = aIerg;
    }

    /**
     * Access method for inscmun.
     *
     * @return the current value of inscmun
     */
    public String getInscmun() {
        return inscmun;
    }

    /**
     * Setter method for inscmun.
     *
     * @param aInscmun the new value for inscmun
     */
    public void setInscmun(String aInscmun) {
        inscmun = aInscmun;
    }

    /**
     * Access method for nfeindfinal.
     *
     * @return the current value of nfeindfinal
     */
    public short getNfeindfinal() {
        return nfeindfinal;
    }

    /**
     * Setter method for nfeindfinal.
     *
     * @param aNfeindfinal the new value for nfeindfinal
     */
    public void setNfeindfinal(short aNfeindfinal) {
        nfeindfinal = aNfeindfinal;
    }

    /**
     * Access method for nfeindiedest.
     *
     * @return the current value of nfeindiedest
     */
    public short getNfeindiedest() {
        return nfeindiedest;
    }

    /**
     * Setter method for nfeindiedest.
     *
     * @param aNfeindiedest the new value for nfeindiedest
     */
    public void setNfeindiedest(short aNfeindiedest) {
        nfeindiedest = aNfeindiedest;
    }

    /**
     * Access method for inscsuframa.
     *
     * @return the current value of inscsuframa
     */
    public String getInscsuframa() {
        return inscsuframa;
    }

    /**
     * Setter method for inscsuframa.
     *
     * @param aInscsuframa the new value for inscsuframa
     */
    public void setInscsuframa(String aInscsuframa) {
        inscsuframa = aInscsuframa;
    }

    /**
     * Access method for indoptributaria.
     *
     * @return the current value of indoptributaria
     */
    public short getIndoptributaria() {
        return indoptributaria;
    }

    /**
     * Setter method for indoptributaria.
     *
     * @param aIndoptributaria the new value for indoptributaria
     */
    public void setIndoptributaria(short aIndoptributaria) {
        indoptributaria = aIndoptributaria;
    }

    /**
     * Access method for indenquadramento.
     *
     * @return the current value of indenquadramento
     */
    public short getIndenquadramento() {
        return indenquadramento;
    }

    /**
     * Setter method for indenquadramento.
     *
     * @param aIndenquadramento the new value for indenquadramento
     */
    public void setIndenquadramento(short aIndenquadramento) {
        indenquadramento = aIndenquadramento;
    }

    /**
     * Access method for indtipoempresa.
     *
     * @return the current value of indtipoempresa
     */
    public short getIndtipoempresa() {
        return indtipoempresa;
    }

    /**
     * Setter method for indtipoempresa.
     *
     * @param aIndtipoempresa the new value for indtipoempresa
     */
    public void setIndtipoempresa(short aIndtipoempresa) {
        indtipoempresa = aIndtipoempresa;
    }

    /**
     * Access method for insccrc.
     *
     * @return the current value of insccrc
     */
    public String getInsccrc() {
        return insccrc;
    }

    /**
     * Setter method for insccrc.
     *
     * @param aInsccrc the new value for insccrc
     */
    public void setInsccrc(String aInsccrc) {
        insccrc = aInsccrc;
    }

    /**
     * Access method for indsubsttrib.
     *
     * @return the current value of indsubsttrib
     */
    public short getIndsubsttrib() {
        return indsubsttrib;
    }

    /**
     * Setter method for indsubsttrib.
     *
     * @param aIndsubsttrib the new value for indsubsttrib
     */
    public void setIndsubsttrib(short aIndsubsttrib) {
        indsubsttrib = aIndsubsttrib;
    }

    /**
     * Access method for indtipocomercio.
     *
     * @return the current value of indtipocomercio
     */
    public short getIndtipocomercio() {
        return indtipocomercio;
    }

    /**
     * Setter method for indtipocomercio.
     *
     * @param aIndtipocomercio the new value for indtipocomercio
     */
    public void setIndtipocomercio(short aIndtipocomercio) {
        indtipocomercio = aIndtipocomercio;
    }

    /**
     * Access method for passnumero.
     *
     * @return the current value of passnumero
     */
    public String getPassnumero() {
        return passnumero;
    }

    /**
     * Setter method for passnumero.
     *
     * @param aPassnumero the new value for passnumero
     */
    public void setPassnumero(String aPassnumero) {
        passnumero = aPassnumero;
    }

    /**
     * Access method for passemissao.
     *
     * @return the current value of passemissao
     */
    public Timestamp getPassemissao() {
        return passemissao;
    }

    /**
     * Setter method for passemissao.
     *
     * @param aPassemissao the new value for passemissao
     */
    public void setPassemissao(Timestamp aPassemissao) {
        passemissao = aPassemissao;
    }

    /**
     * Access method for passvalidade.
     *
     * @return the current value of passvalidade
     */
    public Timestamp getPassvalidade() {
        return passvalidade;
    }

    /**
     * Setter method for passvalidade.
     *
     * @param aPassvalidade the new value for passvalidade
     */
    public void setPassvalidade(Timestamp aPassvalidade) {
        passvalidade = aPassvalidade;
    }

    /**
     * Access method for estrnumero.
     *
     * @return the current value of estrnumero
     */
    public String getEstrnumero() {
        return estrnumero;
    }

    /**
     * Setter method for estrnumero.
     *
     * @param aEstrnumero the new value for estrnumero
     */
    public void setEstrnumero(String aEstrnumero) {
        estrnumero = aEstrnumero;
    }

    /**
     * Access method for estrvalidade.
     *
     * @return the current value of estrvalidade
     */
    public Timestamp getEstrvalidade() {
        return estrvalidade;
    }

    /**
     * Setter method for estrvalidade.
     *
     * @param aEstrvalidade the new value for estrvalidade
     */
    public void setEstrvalidade(Timestamp aEstrvalidade) {
        estrvalidade = aEstrvalidade;
    }

    /**
     * Access method for cnhnumero.
     *
     * @return the current value of cnhnumero
     */
    public String getCnhnumero() {
        return cnhnumero;
    }

    /**
     * Setter method for cnhnumero.
     *
     * @param aCnhnumero the new value for cnhnumero
     */
    public void setCnhnumero(String aCnhnumero) {
        cnhnumero = aCnhnumero;
    }

    /**
     * Access method for cnhformulario.
     *
     * @return the current value of cnhformulario
     */
    public String getCnhformulario() {
        return cnhformulario;
    }

    /**
     * Setter method for cnhformulario.
     *
     * @param aCnhformulario the new value for cnhformulario
     */
    public void setCnhformulario(String aCnhformulario) {
        cnhformulario = aCnhformulario;
    }

    /**
     * Access method for cnhexpedicao.
     *
     * @return the current value of cnhexpedicao
     */
    public Timestamp getCnhexpedicao() {
        return cnhexpedicao;
    }

    /**
     * Setter method for cnhexpedicao.
     *
     * @param aCnhexpedicao the new value for cnhexpedicao
     */
    public void setCnhexpedicao(Timestamp aCnhexpedicao) {
        cnhexpedicao = aCnhexpedicao;
    }

    /**
     * Access method for cnhvalidade.
     *
     * @return the current value of cnhvalidade
     */
    public Timestamp getCnhvalidade() {
        return cnhvalidade;
    }

    /**
     * Setter method for cnhvalidade.
     *
     * @param aCnhvalidade the new value for cnhvalidade
     */
    public void setCnhvalidade(Timestamp aCnhvalidade) {
        cnhvalidade = aCnhvalidade;
    }

    /**
     * Access method for cnhprimeirahab.
     *
     * @return the current value of cnhprimeirahab
     */
    public Timestamp getCnhprimeirahab() {
        return cnhprimeirahab;
    }

    /**
     * Setter method for cnhprimeirahab.
     *
     * @param aCnhprimeirahab the new value for cnhprimeirahab
     */
    public void setCnhprimeirahab(Timestamp aCnhprimeirahab) {
        cnhprimeirahab = aCnhprimeirahab;
    }

    /**
     * Access method for eleitornumero.
     *
     * @return the current value of eleitornumero
     */
    public String getEleitornumero() {
        return eleitornumero;
    }

    /**
     * Setter method for eleitornumero.
     *
     * @param aEleitornumero the new value for eleitornumero
     */
    public void setEleitornumero(String aEleitornumero) {
        eleitornumero = aEleitornumero;
    }

    /**
     * Access method for eleitorzona.
     *
     * @return the current value of eleitorzona
     */
    public String getEleitorzona() {
        return eleitorzona;
    }

    /**
     * Setter method for eleitorzona.
     *
     * @param aEleitorzona the new value for eleitorzona
     */
    public void setEleitorzona(String aEleitorzona) {
        eleitorzona = aEleitorzona;
    }

    /**
     * Access method for eleitorsecao.
     *
     * @return the current value of eleitorsecao
     */
    public String getEleitorsecao() {
        return eleitorsecao;
    }

    /**
     * Setter method for eleitorsecao.
     *
     * @param aEleitorsecao the new value for eleitorsecao
     */
    public void setEleitorsecao(String aEleitorsecao) {
        eleitorsecao = aEleitorsecao;
    }

    /**
     * Access method for eleitoremissao.
     *
     * @return the current value of eleitoremissao
     */
    public Timestamp getEleitoremissao() {
        return eleitoremissao;
    }

    /**
     * Setter method for eleitoremissao.
     *
     * @param aEleitoremissao the new value for eleitoremissao
     */
    public void setEleitoremissao(Timestamp aEleitoremissao) {
        eleitoremissao = aEleitoremissao;
    }

    /**
     * Access method for reservnumero.
     *
     * @return the current value of reservnumero
     */
    public String getReservnumero() {
        return reservnumero;
    }

    /**
     * Setter method for reservnumero.
     *
     * @param aReservnumero the new value for reservnumero
     */
    public void setReservnumero(String aReservnumero) {
        reservnumero = aReservnumero;
    }

    /**
     * Access method for reservlotacao.
     *
     * @return the current value of reservlotacao
     */
    public String getReservlotacao() {
        return reservlotacao;
    }

    /**
     * Setter method for reservlotacao.
     *
     * @param aReservlotacao the new value for reservlotacao
     */
    public void setReservlotacao(String aReservlotacao) {
        reservlotacao = aReservlotacao;
    }

    /**
     * Access method for reservemissao.
     *
     * @return the current value of reservemissao
     */
    public Timestamp getReservemissao() {
        return reservemissao;
    }

    /**
     * Setter method for reservemissao.
     *
     * @param aReservemissao the new value for reservemissao
     */
    public void setReservemissao(Timestamp aReservemissao) {
        reservemissao = aReservemissao;
    }

    /**
     * Access method for datanascabertura.
     *
     * @return the current value of datanascabertura
     */
    public Date getDatanascabertura() {
        return datanascabertura;
    }

    /**
     * Setter method for datanascabertura.
     *
     * @param aDatanascabertura the new value for datanascabertura
     */
    public void setDatanascabertura(Date aDatanascabertura) {
        datanascabertura = aDatanascabertura;
    }

    /**
     * Access method for datasituacao.
     *
     * @return the current value of datasituacao
     */
    public Timestamp getDatasituacao() {
        return datasituacao;
    }

    /**
     * Setter method for datasituacao.
     *
     * @param aDatasituacao the new value for datasituacao
     */
    public void setDatasituacao(Timestamp aDatasituacao) {
        datasituacao = aDatasituacao;
    }

    /**
     * Access method for agtipo.
     *
     * @return the current value of agtipo
     */
    public Agtipo getAgtipo() {
        return agtipo;
    }

    /**
     * Setter method for agtipo.
     *
     * @param aAgtipo the new value for agtipo
     */
    public void setAgtipo(Agtipo aAgtipo) {
        agtipo = aAgtipo;
    }

    /**
     * Access method for finplanoconta.
     *
     * @return the current value of finplanoconta
     */
    public Finplanoconta getFinplanoconta() {
        return finplanoconta;
    }

    /**
     * Setter method for finplanoconta.
     *
     * @param aFinplanoconta the new value for finplanoconta
     */
    public void setFinplanoconta(Finplanoconta aFinplanoconta) {
        finplanoconta = aFinplanoconta;
    }

    /**
     * Access method for finplanoconta2.
     *
     * @return the current value of finplanoconta2
     */
    public Finplanoconta getFinplanoconta2() {
        return finplanoconta2;
    }

    /**
     * Setter method for finplanoconta2.
     *
     * @param aFinplanoconta2 the new value for finplanoconta2
     */
    public void setFinplanoconta2(Finplanoconta aFinplanoconta2) {
        finplanoconta2 = aFinplanoconta2;
    }

    /**
     * Access method for finfluxoplanoconta.
     *
     * @return the current value of finfluxoplanoconta
     */
    public Finfluxoplanoconta getFinfluxoplanoconta() {
        return finfluxoplanoconta;
    }

    /**
     * Setter method for finfluxoplanoconta.
     *
     * @param aFinfluxoplanoconta the new value for finfluxoplanoconta
     */
    public void setFinfluxoplanoconta(Finfluxoplanoconta aFinfluxoplanoconta) {
        finfluxoplanoconta = aFinfluxoplanoconta;
    }

    /**
     * Access method for finfluxoplanoconta2.
     *
     * @return the current value of finfluxoplanoconta2
     */
    public Finfluxoplanoconta getFinfluxoplanoconta2() {
        return finfluxoplanoconta2;
    }

    /**
     * Setter method for finfluxoplanoconta2.
     *
     * @param aFinfluxoplanoconta2 the new value for finfluxoplanoconta2
     */
    public void setFinfluxoplanoconta2(Finfluxoplanoconta aFinfluxoplanoconta2) {
        finfluxoplanoconta2 = aFinfluxoplanoconta2;
    }

    /**
     * Access method for orcplanoconta.
     *
     * @return the current value of orcplanoconta
     */
    public Orcplanoconta getOrcplanoconta() {
        return orcplanoconta;
    }

    /**
     * Setter method for orcplanoconta.
     *
     * @param aOrcplanoconta the new value for orcplanoconta
     */
    public void setOrcplanoconta(Orcplanoconta aOrcplanoconta) {
        orcplanoconta = aOrcplanoconta;
    }

    /**
     * Access method for orcplanoconta2.
     *
     * @return the current value of orcplanoconta2
     */
    public Orcplanoconta getOrcplanoconta2() {
        return orcplanoconta2;
    }

    /**
     * Setter method for orcplanoconta2.
     *
     * @param aOrcplanoconta2 the new value for orcplanoconta2
     */
    public void setOrcplanoconta2(Orcplanoconta aOrcplanoconta2) {
        orcplanoconta2 = aOrcplanoconta2;
    }

    /**
     * Access method for agvistotipo.
     *
     * @return the current value of agvistotipo
     */
    public Agvistotipo getAgvistotipo() {
        return agvistotipo;
    }

    /**
     * Setter method for agvistotipo.
     *
     * @param aAgvistotipo the new value for agvistotipo
     */
    public void setAgvistotipo(Agvistotipo aAgvistotipo) {
        agvistotipo = aAgvistotipo;
    }

    /**
     * Access method for agagentetiporeceita.
     *
     * @return the current value of agagentetiporeceita
     */
    public Agagentetiporeceita getAgagentetiporeceita() {
        return agagentetiporeceita;
    }

    /**
     * Setter method for agagentetiporeceita.
     *
     * @param aAgagentetiporeceita the new value for agagentetiporeceita
     */
    public void setAgagentetiporeceita(Agagentetiporeceita aAgagentetiporeceita) {
        agagentetiporeceita = aAgagentetiporeceita;
    }

    /**
     * Access method for fisnatjur.
     *
     * @return the current value of fisnatjur
     */
    public Fisnatjur getFisnatjur() {
        return fisnatjur;
    }

    /**
     * Setter method for fisnatjur.
     *
     * @param aFisnatjur the new value for fisnatjur
     */
    public void setFisnatjur(Fisnatjur aFisnatjur) {
        fisnatjur = aFisnatjur;
    }

    /**
     * Access method for aggrupo.
     *
     * @return the current value of aggrupo
     */
    public Aggrupo getAggrupo() {
        return aggrupo;
    }

    /**
     * Setter method for aggrupo.
     *
     * @param aAggrupo the new value for aggrupo
     */
    public void setAggrupo(Aggrupo aAggrupo) {
        aggrupo = aAggrupo;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for agagente7.
     *
     * @return the current value of agagente7
     */
    public Set<Agagente> getAgagente7() {
        return agagente7;
    }

    /**
     * Setter method for agagente7.
     *
     * @param aAgagente7 the new value for agagente7
     */
    public void setAgagente7(Set<Agagente> aAgagente7) {
        agagente7 = aAgagente7;
    }

    /**
     * Access method for agagente6.
     *
     * @return the current value of agagente6
     */
    public Agagente getAgagente6() {
        return agagente6;
    }

    /**
     * Setter method for agagente6.
     *
     * @param aAgagente6 the new value for agagente6
     */
    public void setAgagente6(Agagente aAgagente6) {
        agagente6 = aAgagente6;
    }

    /**
     * Access method for agagente5.
     *
     * @return the current value of agagente5
     */
    public Set<Agagente> getAgagente5() {
        return agagente5;
    }

    /**
     * Setter method for agagente5.
     *
     * @param aAgagente5 the new value for agagente5
     */
    public void setAgagente5(Set<Agagente> aAgagente5) {
        agagente5 = aAgagente5;
    }

    /**
     * Access method for agagente4.
     *
     * @return the current value of agagente4
     */
    public Agagente getAgagente4() {
        return agagente4;
    }

    /**
     * Setter method for agagente4.
     *
     * @param aAgagente4 the new value for agagente4
     */
    public void setAgagente4(Agagente aAgagente4) {
        agagente4 = aAgagente4;
    }

    /**
     * Access method for cobrplano.
     *
     * @return the current value of cobrplano
     */
    public Cobrplano getCobrplano() {
        return cobrplano;
    }

    /**
     * Setter method for cobrplano.
     *
     * @param aCobrplano the new value for cobrplano
     */
    public void setCobrplano(Cobrplano aCobrplano) {
        cobrplano = aCobrplano;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Opoperacao getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Opoperacao aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for prtabela.
     *
     * @return the current value of prtabela
     */
    public Prtabela getPrtabela() {
        return prtabela;
    }

    /**
     * Setter method for prtabela.
     *
     * @param aPrtabela the new value for prtabela
     */
    public void setPrtabela(Prtabela aPrtabela) {
        prtabela = aPrtabela;
    }

    /**
     * Access method for agagenteimpcnae.
     *
     * @return the current value of agagenteimpcnae
     */
    public Set<Agagenteimpcnae> getAgagenteimpcnae() {
        return agagenteimpcnae;
    }

    /**
     * Setter method for agagenteimpcnae.
     *
     * @param aAgagenteimpcnae the new value for agagenteimpcnae
     */
    public void setAgagenteimpcnae(Set<Agagenteimpcnae> aAgagenteimpcnae) {
        agagenteimpcnae = aAgagenteimpcnae;
    }

    /**
     * Access method for agagenteresp.
     *
     * @return the current value of agagenteresp
     */
    public Set<Agagenteresp> getAgagenteresp() {
        return agagenteresp;
    }

    /**
     * Setter method for agagenteresp.
     *
     * @param aAgagenteresp the new value for agagenteresp
     */
    public void setAgagenteresp(Set<Agagenteresp> aAgagenteresp) {
        agagenteresp = aAgagenteresp;
    }

    /**
     * Access method for agagentetransp.
     *
     * @return the current value of agagentetransp
     */
    public Set<Agagentetransp> getAgagentetransp() {
        return agagentetransp;
    }

    /**
     * Setter method for agagentetransp.
     *
     * @param aAgagentetransp the new value for agagentetransp
     */
    public void setAgagentetransp(Set<Agagentetransp> aAgagentetransp) {
        agagentetransp = aAgagentetransp;
    }

    /**
     * Access method for aglimcredito.
     *
     * @return the current value of aglimcredito
     */
    public Set<Aglimcredito> getAglimcredito() {
        return aglimcredito;
    }

    /**
     * Setter method for aglimcredito.
     *
     * @param aAglimcredito the new value for aglimcredito
     */
    public void setAglimcredito(Set<Aglimcredito> aAglimcredito) {
        aglimcredito = aAglimcredito;
    }

    /**
     * Access method for aglimcreditosol.
     *
     * @return the current value of aglimcreditosol
     */
    public Set<Aglimcreditosol> getAglimcreditosol() {
        return aglimcreditosol;
    }

    /**
     * Setter method for aglimcreditosol.
     *
     * @param aAglimcreditosol the new value for aglimcreditosol
     */
    public void setAglimcreditosol(Set<Aglimcreditosol> aAglimcreditosol) {
        aglimcreditosol = aAglimcreditosol;
    }

    /**
     * Access method for comtabela.
     *
     * @return the current value of comtabela
     */
    public Set<Comtabela> getComtabela() {
        return comtabela;
    }

    /**
     * Setter method for comtabela.
     *
     * @param aComtabela the new value for comtabela
     */
    public void setComtabela(Set<Comtabela> aComtabela) {
        comtabela = aComtabela;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Set<Confempr> getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Set<Confempr> aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for confemprpart.
     *
     * @return the current value of confemprpart
     */
    public Set<Confemprpart> getConfemprpart() {
        return confemprpart;
    }

    /**
     * Setter method for confemprpart.
     *
     * @param aConfemprpart the new value for confemprpart
     */
    public void setConfemprpart(Set<Confemprpart> aConfemprpart) {
        confemprpart = aConfemprpart;
    }

    /**
     * Access method for contcontrato.
     *
     * @return the current value of contcontrato
     */
    public Set<Contcontrato> getContcontrato() {
        return contcontrato;
    }

    /**
     * Setter method for contcontrato.
     *
     * @param aContcontrato the new value for contcontrato
     */
    public void setContcontrato(Set<Contcontrato> aContcontrato) {
        contcontrato = aContcontrato;
    }

    /**
     * Access method for contcontrato2.
     *
     * @return the current value of contcontrato2
     */
    public Set<Contcontrato> getContcontrato2() {
        return contcontrato2;
    }

    /**
     * Setter method for contcontrato2.
     *
     * @param aContcontrato2 the new value for contcontrato2
     */
    public void setContcontrato2(Set<Contcontrato> aContcontrato2) {
        contcontrato2 = aContcontrato2;
    }

    /**
     * Access method for crmchamado.
     *
     * @return the current value of crmchamado
     */
    public Set<Crmchamado> getCrmchamado() {
        return crmchamado;
    }

    /**
     * Setter method for crmchamado.
     *
     * @param aCrmchamado the new value for crmchamado
     */
    public void setCrmchamado(Set<Crmchamado> aCrmchamado) {
        crmchamado = aCrmchamado;
    }

    /**
     * Access method for ctbbem.
     *
     * @return the current value of ctbbem
     */
    public Set<Ctbbem> getCtbbem() {
        return ctbbem;
    }

    /**
     * Setter method for ctbbem.
     *
     * @param aCtbbem the new value for ctbbem
     */
    public void setCtbbem(Set<Ctbbem> aCtbbem) {
        ctbbem = aCtbbem;
    }

    /**
     * Access method for ctbbemveicinfracao.
     *
     * @return the current value of ctbbemveicinfracao
     */
    public Set<Ctbbemveicinfracao> getCtbbemveicinfracao() {
        return ctbbemveicinfracao;
    }

    /**
     * Setter method for ctbbemveicinfracao.
     *
     * @param aCtbbemveicinfracao the new value for ctbbemveicinfracao
     */
    public void setCtbbemveicinfracao(Set<Ctbbemveicinfracao> aCtbbemveicinfracao) {
        ctbbemveicinfracao = aCtbbemveicinfracao;
    }

    /**
     * Access method for ctbfechamento.
     *
     * @return the current value of ctbfechamento
     */
    public Set<Ctbfechamento> getCtbfechamento() {
        return ctbfechamento;
    }

    /**
     * Setter method for ctbfechamento.
     *
     * @param aCtbfechamento the new value for ctbfechamento
     */
    public void setCtbfechamento(Set<Ctbfechamento> aCtbfechamento) {
        ctbfechamento = aCtbfechamento;
    }

    /**
     * Access method for ctbfechamento2.
     *
     * @return the current value of ctbfechamento2
     */
    public Set<Ctbfechamento> getCtbfechamento2() {
        return ctbfechamento2;
    }

    /**
     * Setter method for ctbfechamento2.
     *
     * @param aCtbfechamento2 the new value for ctbfechamento2
     */
    public void setCtbfechamento2(Set<Ctbfechamento> aCtbfechamento2) {
        ctbfechamento2 = aCtbfechamento2;
    }

    /**
     * Access method for ecarquivamento.
     *
     * @return the current value of ecarquivamento
     */
    public Set<Ecarquivamento> getEcarquivamento() {
        return ecarquivamento;
    }

    /**
     * Setter method for ecarquivamento.
     *
     * @param aEcarquivamento the new value for ecarquivamento
     */
    public void setEcarquivamento(Set<Ecarquivamento> aEcarquivamento) {
        ecarquivamento = aEcarquivamento;
    }

    /**
     * Access method for eccertificado.
     *
     * @return the current value of eccertificado
     */
    public Set<Eccertificado> getEccertificado() {
        return eccertificado;
    }

    /**
     * Setter method for eccertificado.
     *
     * @param aEccertificado the new value for eccertificado
     */
    public void setEccertificado(Set<Eccertificado> aEccertificado) {
        eccertificado = aEccertificado;
    }

    /**
     * Access method for eccertificado2.
     *
     * @return the current value of eccertificado2
     */
    public Set<Eccertificado> getEccertificado2() {
        return eccertificado2;
    }

    /**
     * Setter method for eccertificado2.
     *
     * @param aEccertificado2 the new value for eccertificado2
     */
    public void setEccertificado2(Set<Eccertificado> aEccertificado2) {
        eccertificado2 = aEccertificado2;
    }

    /**
     * Access method for eccertificado3.
     *
     * @return the current value of eccertificado3
     */
    public Set<Eccertificado> getEccertificado3() {
        return eccertificado3;
    }

    /**
     * Setter method for eccertificado3.
     *
     * @param aEccertificado3 the new value for eccertificado3
     */
    public void setEccertificado3(Set<Eccertificado> aEccertificado3) {
        eccertificado3 = aEccertificado3;
    }

    /**
     * Access method for eccheckagenda.
     *
     * @return the current value of eccheckagenda
     */
    public Set<Eccheckagenda> getEccheckagenda() {
        return eccheckagenda;
    }

    /**
     * Setter method for eccheckagenda.
     *
     * @param aEccheckagenda the new value for eccheckagenda
     */
    public void setEccheckagenda(Set<Eccheckagenda> aEccheckagenda) {
        eccheckagenda = aEccheckagenda;
    }

    /**
     * Access method for eccheckstatus.
     *
     * @return the current value of eccheckstatus
     */
    public Set<Eccheckstatus> getEccheckstatus() {
        return eccheckstatus;
    }

    /**
     * Setter method for eccheckstatus.
     *
     * @param aEccheckstatus the new value for eccheckstatus
     */
    public void setEccheckstatus(Set<Eccheckstatus> aEccheckstatus) {
        eccheckstatus = aEccheckstatus;
    }

    /**
     * Access method for eccheckstatusagente.
     *
     * @return the current value of eccheckstatusagente
     */
    public Set<Eccheckstatusagente> getEccheckstatusagente() {
        return eccheckstatusagente;
    }

    /**
     * Setter method for eccheckstatusagente.
     *
     * @param aEccheckstatusagente the new value for eccheckstatusagente
     */
    public void setEccheckstatusagente(Set<Eccheckstatusagente> aEccheckstatusagente) {
        eccheckstatusagente = aEccheckstatusagente;
    }

    /**
     * Access method for eccnd.
     *
     * @return the current value of eccnd
     */
    public Set<Eccnd> getEccnd() {
        return eccnd;
    }

    /**
     * Setter method for eccnd.
     *
     * @param aEccnd the new value for eccnd
     */
    public void setEccnd(Set<Eccnd> aEccnd) {
        eccnd = aEccnd;
    }

    /**
     * Access method for eccnd2.
     *
     * @return the current value of eccnd2
     */
    public Set<Eccnd> getEccnd2() {
        return eccnd2;
    }

    /**
     * Setter method for eccnd2.
     *
     * @param aEccnd2 the new value for eccnd2
     */
    public void setEccnd2(Set<Eccnd> aEccnd2) {
        eccnd2 = aEccnd2;
    }

    /**
     * Access method for eccorresplancamento.
     *
     * @return the current value of eccorresplancamento
     */
    public Set<Eccorresplancamento> getEccorresplancamento() {
        return eccorresplancamento;
    }

    /**
     * Setter method for eccorresplancamento.
     *
     * @param aEccorresplancamento the new value for eccorresplancamento
     */
    public void setEccorresplancamento(Set<Eccorresplancamento> aEccorresplancamento) {
        eccorresplancamento = aEccorresplancamento;
    }

    /**
     * Access method for eccorresplancamento2.
     *
     * @return the current value of eccorresplancamento2
     */
    public Set<Eccorresplancamento> getEccorresplancamento2() {
        return eccorresplancamento2;
    }

    /**
     * Setter method for eccorresplancamento2.
     *
     * @param aEccorresplancamento2 the new value for eccorresplancamento2
     */
    public void setEccorresplancamento2(Set<Eccorresplancamento> aEccorresplancamento2) {
        eccorresplancamento2 = aEccorresplancamento2;
    }

    /**
     * Access method for ecdecore.
     *
     * @return the current value of ecdecore
     */
    public Set<Ecdecore> getEcdecore() {
        return ecdecore;
    }

    /**
     * Setter method for ecdecore.
     *
     * @param aEcdecore the new value for ecdecore
     */
    public void setEcdecore(Set<Ecdecore> aEcdecore) {
        ecdecore = aEcdecore;
    }

    /**
     * Access method for ecdoclancamento.
     *
     * @return the current value of ecdoclancamento
     */
    public Set<Ecdoclancamento> getEcdoclancamento() {
        return ecdoclancamento;
    }

    /**
     * Setter method for ecdoclancamento.
     *
     * @param aEcdoclancamento the new value for ecdoclancamento
     */
    public void setEcdoclancamento(Set<Ecdoclancamento> aEcdoclancamento) {
        ecdoclancamento = aEcdoclancamento;
    }

    /**
     * Access method for ecpgtolancamento.
     *
     * @return the current value of ecpgtolancamento
     */
    public Set<Ecpgtolancamento> getEcpgtolancamento() {
        return ecpgtolancamento;
    }

    /**
     * Setter method for ecpgtolancamento.
     *
     * @param aEcpgtolancamento the new value for ecpgtolancamento
     */
    public void setEcpgtolancamento(Set<Ecpgtolancamento> aEcpgtolancamento) {
        ecpgtolancamento = aEcpgtolancamento;
    }

    /**
     * Access method for finagencia.
     *
     * @return the current value of finagencia
     */
    public Set<Finagencia> getFinagencia() {
        return finagencia;
    }

    /**
     * Setter method for finagencia.
     *
     * @param aFinagencia the new value for finagencia
     */
    public void setFinagencia(Set<Finagencia> aFinagencia) {
        finagencia = aFinagencia;
    }

    /**
     * Access method for finagencia2.
     *
     * @return the current value of finagencia2
     */
    public Set<Finagencia> getFinagencia2() {
        return finagencia2;
    }

    /**
     * Setter method for finagencia2.
     *
     * @param aFinagencia2 the new value for finagencia2
     */
    public void setFinagencia2(Set<Finagencia> aFinagencia2) {
        finagencia2 = aFinagencia2;
    }

    /**
     * Access method for finconta.
     *
     * @return the current value of finconta
     */
    public Set<Finconta> getFinconta() {
        return finconta;
    }

    /**
     * Setter method for finconta.
     *
     * @param aFinconta the new value for finconta
     */
    public void setFinconta(Set<Finconta> aFinconta) {
        finconta = aFinconta;
    }

    /**
     * Access method for finemprestimo.
     *
     * @return the current value of finemprestimo
     */
    public Set<Finemprestimo> getFinemprestimo() {
        return finemprestimo;
    }

    /**
     * Setter method for finemprestimo.
     *
     * @param aFinemprestimo the new value for finemprestimo
     */
    public void setFinemprestimo(Set<Finemprestimo> aFinemprestimo) {
        finemprestimo = aFinemprestimo;
    }

    /**
     * Access method for finemprestimo2.
     *
     * @return the current value of finemprestimo2
     */
    public Set<Finemprestimo> getFinemprestimo2() {
        return finemprestimo2;
    }

    /**
     * Setter method for finemprestimo2.
     *
     * @param aFinemprestimo2 the new value for finemprestimo2
     */
    public void setFinemprestimo2(Set<Finemprestimo> aFinemprestimo2) {
        finemprestimo2 = aFinemprestimo2;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Set<Finlancamento> getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Set<Finlancamento> aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for finoperadora.
     *
     * @return the current value of finoperadora
     */
    public Set<Finoperadora> getFinoperadora() {
        return finoperadora;
    }

    /**
     * Setter method for finoperadora.
     *
     * @param aFinoperadora the new value for finoperadora
     */
    public void setFinoperadora(Set<Finoperadora> aFinoperadora) {
        finoperadora = aFinoperadora;
    }

    /**
     * Access method for fises.
     *
     * @return the current value of fises
     */
    public Set<Fises> getFises() {
        return fises;
    }

    /**
     * Setter method for fises.
     *
     * @param aFises the new value for fises
     */
    public void setFises(Set<Fises> aFises) {
        fises = aFises;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Set<Folcolaborador> getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Set<Folcolaborador> aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Access method for folcolaborador2.
     *
     * @return the current value of folcolaborador2
     */
    public Set<Folcolaborador> getFolcolaborador2() {
        return folcolaborador2;
    }

    /**
     * Setter method for folcolaborador2.
     *
     * @param aFolcolaborador2 the new value for folcolaborador2
     */
    public void setFolcolaborador2(Set<Folcolaborador> aFolcolaborador2) {
        folcolaborador2 = aFolcolaborador2;
    }

    /**
     * Access method for folcolaborador3.
     *
     * @return the current value of folcolaborador3
     */
    public Set<Folcolaborador> getFolcolaborador3() {
        return folcolaborador3;
    }

    /**
     * Setter method for folcolaborador3.
     *
     * @param aFolcolaborador3 the new value for folcolaborador3
     */
    public void setFolcolaborador3(Set<Folcolaborador> aFolcolaborador3) {
        folcolaborador3 = aFolcolaborador3;
    }

    /**
     * Access method for folcolaboradordep.
     *
     * @return the current value of folcolaboradordep
     */
    public Set<Folcolaboradordep> getFolcolaboradordep() {
        return folcolaboradordep;
    }

    /**
     * Setter method for folcolaboradordep.
     *
     * @param aFolcolaboradordep the new value for folcolaboradordep
     */
    public void setFolcolaboradordep(Set<Folcolaboradordep> aFolcolaboradordep) {
        folcolaboradordep = aFolcolaboradordep;
    }

    /**
     * Access method for folconvenio.
     *
     * @return the current value of folconvenio
     */
    public Set<Folconvenio> getFolconvenio() {
        return folconvenio;
    }

    /**
     * Setter method for folconvenio.
     *
     * @param aFolconvenio the new value for folconvenio
     */
    public void setFolconvenio(Set<Folconvenio> aFolconvenio) {
        folconvenio = aFolconvenio;
    }

    /**
     * Access method for folconveniotipo.
     *
     * @return the current value of folconveniotipo
     */
    public Set<Folconveniotipo> getFolconveniotipo() {
        return folconveniotipo;
    }

    /**
     * Setter method for folconveniotipo.
     *
     * @param aFolconveniotipo the new value for folconveniotipo
     */
    public void setFolconveniotipo(Set<Folconveniotipo> aFolconveniotipo) {
        folconveniotipo = aFolconveniotipo;
    }

    /**
     * Access method for folsindicato.
     *
     * @return the current value of folsindicato
     */
    public Set<Folsindicato> getFolsindicato() {
        return folsindicato;
    }

    /**
     * Setter method for folsindicato.
     *
     * @param aFolsindicato the new value for folsindicato
     */
    public void setFolsindicato(Set<Folsindicato> aFolsindicato) {
        folsindicato = aFolsindicato;
    }

    /**
     * Access method for gerimovel.
     *
     * @return the current value of gerimovel
     */
    public Set<Gerimovel> getGerimovel() {
        return gerimovel;
    }

    /**
     * Setter method for gerimovel.
     *
     * @param aGerimovel the new value for gerimovel
     */
    public void setGerimovel(Set<Gerimovel> aGerimovel) {
        gerimovel = aGerimovel;
    }

    /**
     * Access method for gerveiculo.
     *
     * @return the current value of gerveiculo
     */
    public Set<Gerveiculo> getGerveiculo() {
        return gerveiculo;
    }

    /**
     * Setter method for gerveiculo.
     *
     * @param aGerveiculo the new value for gerveiculo
     */
    public void setGerveiculo(Set<Gerveiculo> aGerveiculo) {
        gerveiculo = aGerveiculo;
    }

    /**
     * Access method for oporcdetag.
     *
     * @return the current value of oporcdetag
     */
    public Set<Oporcdetag> getOporcdetag() {
        return oporcdetag;
    }

    /**
     * Setter method for oporcdetag.
     *
     * @param aOporcdetag the new value for oporcdetag
     */
    public void setOporcdetag(Set<Oporcdetag> aOporcdetag) {
        oporcdetag = aOporcdetag;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for prdcplano.
     *
     * @return the current value of prdcplano
     */
    public Set<Prdcplano> getPrdcplano() {
        return prdcplano;
    }

    /**
     * Setter method for prdcplano.
     *
     * @param aPrdcplano the new value for prdcplano
     */
    public void setPrdcplano(Set<Prdcplano> aPrdcplano) {
        prdcplano = aPrdcplano;
    }

    /**
     * Access method for prdcplanoetapa.
     *
     * @return the current value of prdcplanoetapa
     */
    public Set<Prdcplanoetapa> getPrdcplanoetapa() {
        return prdcplanoetapa;
    }

    /**
     * Setter method for prdcplanoetapa.
     *
     * @param aPrdcplanoetapa the new value for prdcplanoetapa
     */
    public void setPrdcplanoetapa(Set<Prdcplanoetapa> aPrdcplanoetapa) {
        prdcplanoetapa = aPrdcplanoetapa;
    }

    /**
     * Access method for prodapolice.
     *
     * @return the current value of prodapolice
     */
    public Set<Prodapolice> getProdapolice() {
        return prodapolice;
    }

    /**
     * Setter method for prodapolice.
     *
     * @param aProdapolice the new value for prodapolice
     */
    public void setProdapolice(Set<Prodapolice> aProdapolice) {
        prodapolice = aProdapolice;
    }

    /**
     * Access method for prodapolice2.
     *
     * @return the current value of prodapolice2
     */
    public Set<Prodapolice> getProdapolice2() {
        return prodapolice2;
    }

    /**
     * Setter method for prodapolice2.
     *
     * @param aProdapolice2 the new value for prodapolice2
     */
    public void setProdapolice2(Set<Prodapolice> aProdapolice2) {
        prodapolice2 = aProdapolice2;
    }

    /**
     * Access method for prodmarca.
     *
     * @return the current value of prodmarca
     */
    public Set<Prodmarca> getProdmarca() {
        return prodmarca;
    }

    /**
     * Setter method for prodmarca.
     *
     * @param aProdmarca the new value for prodmarca
     */
    public void setProdmarca(Set<Prodmarca> aProdmarca) {
        prodmarca = aProdmarca;
    }

    /**
     * Compares the key for this instance with another Agagente.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Agagente and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Agagente)) {
            return false;
        }
        Agagente that = (Agagente) other;
        if (this.getCodagagente() != that.getCodagagente()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Agagente.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Agagente)) return false;
        return this.equalKeys(other) && ((Agagente)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodagagente();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Agagente |");
        sb.append(" codagagente=").append(getCodagagente());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codagagente", Integer.valueOf(getCodagagente()));
        return ret;
    }

}
