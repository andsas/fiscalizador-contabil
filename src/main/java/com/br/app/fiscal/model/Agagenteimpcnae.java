package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="AGAGENTEIMPCNAE")
public class Agagenteimpcnae implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codagagenteimpcnae";

    @Id
    @Column(name="CODAGAGENTEIMPCNAE", unique=true, nullable=false, precision=10)
    private int codagagenteimpcnae;
    @Column(name="OBS")
    private String obs;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODIMPCNAE", nullable=false)
    private Impcnae impcnae;

    /** Default constructor. */
    public Agagenteimpcnae() {
        super();
    }

    /**
     * Access method for codagagenteimpcnae.
     *
     * @return the current value of codagagenteimpcnae
     */
    public int getCodagagenteimpcnae() {
        return codagagenteimpcnae;
    }

    /**
     * Setter method for codagagenteimpcnae.
     *
     * @param aCodagagenteimpcnae the new value for codagagenteimpcnae
     */
    public void setCodagagenteimpcnae(int aCodagagenteimpcnae) {
        codagagenteimpcnae = aCodagagenteimpcnae;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for impcnae.
     *
     * @return the current value of impcnae
     */
    public Impcnae getImpcnae() {
        return impcnae;
    }

    /**
     * Setter method for impcnae.
     *
     * @param aImpcnae the new value for impcnae
     */
    public void setImpcnae(Impcnae aImpcnae) {
        impcnae = aImpcnae;
    }

    /**
     * Compares the key for this instance with another Agagenteimpcnae.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Agagenteimpcnae and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Agagenteimpcnae)) {
            return false;
        }
        Agagenteimpcnae that = (Agagenteimpcnae) other;
        if (this.getCodagagenteimpcnae() != that.getCodagagenteimpcnae()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Agagenteimpcnae.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Agagenteimpcnae)) return false;
        return this.equalKeys(other) && ((Agagenteimpcnae)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodagagenteimpcnae();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Agagenteimpcnae |");
        sb.append(" codagagenteimpcnae=").append(getCodagagenteimpcnae());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codagagenteimpcnae", Integer.valueOf(getCodagagenteimpcnae()));
        return ret;
    }

}
