package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="AGAGENTERESP")
public class Agagenteresp implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codagagenteresp";

    @Id
    @Column(name="CODAGAGENTERESP", unique=true, nullable=false, precision=10)
    private int codagagenteresp;
    @Column(name="OBS")
    private String obs;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGTIPOAGENTERESP", nullable=false)
    private Agagenteresptipo agagenteresptipo;
    @OneToMany(mappedBy="agagenteresp")
    private Set<Optransacaoresp> optransacaoresp;

    /** Default constructor. */
    public Agagenteresp() {
        super();
    }

    /**
     * Access method for codagagenteresp.
     *
     * @return the current value of codagagenteresp
     */
    public int getCodagagenteresp() {
        return codagagenteresp;
    }

    /**
     * Setter method for codagagenteresp.
     *
     * @param aCodagagenteresp the new value for codagagenteresp
     */
    public void setCodagagenteresp(int aCodagagenteresp) {
        codagagenteresp = aCodagagenteresp;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagenteresptipo.
     *
     * @return the current value of agagenteresptipo
     */
    public Agagenteresptipo getAgagenteresptipo() {
        return agagenteresptipo;
    }

    /**
     * Setter method for agagenteresptipo.
     *
     * @param aAgagenteresptipo the new value for agagenteresptipo
     */
    public void setAgagenteresptipo(Agagenteresptipo aAgagenteresptipo) {
        agagenteresptipo = aAgagenteresptipo;
    }

    /**
     * Access method for optransacaoresp.
     *
     * @return the current value of optransacaoresp
     */
    public Set<Optransacaoresp> getOptransacaoresp() {
        return optransacaoresp;
    }

    /**
     * Setter method for optransacaoresp.
     *
     * @param aOptransacaoresp the new value for optransacaoresp
     */
    public void setOptransacaoresp(Set<Optransacaoresp> aOptransacaoresp) {
        optransacaoresp = aOptransacaoresp;
    }

    /**
     * Compares the key for this instance with another Agagenteresp.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Agagenteresp and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Agagenteresp)) {
            return false;
        }
        Agagenteresp that = (Agagenteresp) other;
        if (this.getCodagagenteresp() != that.getCodagagenteresp()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Agagenteresp.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Agagenteresp)) return false;
        return this.equalKeys(other) && ((Agagenteresp)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodagagenteresp();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Agagenteresp |");
        sb.append(" codagagenteresp=").append(getCodagagenteresp());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codagagenteresp", Integer.valueOf(getCodagagenteresp()));
        return ret;
    }

}
