package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="AGAGENTERESPTIPO")
public class Agagenteresptipo implements Serializable {    
	
	
	
	private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codagagenteresptipo";

    @Id
    @Column(name="CODAGAGENTERESPTIPO", unique=true, nullable=false, precision=10)
    private int codagagenteresptipo;
    @Column(name="DESCAGAGENTERESPTIPO", nullable=false, length=250)
    private String descagagenteresptipo;
    @OneToMany(mappedBy="agagenteresptipo")
    private Set<Agagenteresp> agagenteresp;

    /** Default constructor. */
    public Agagenteresptipo() {
        super();
    }

    /**
     * Access method for codagagenteresptipo.
     *
     * @return the current value of codagagenteresptipo
     */
    public int getCodagagenteresptipo() {
        return codagagenteresptipo;
    }

    /**
     * Setter method for codagagenteresptipo.
     *
     * @param aCodagagenteresptipo the new value for codagagenteresptipo
     */
    public void setCodagagenteresptipo(int aCodagagenteresptipo) {
        codagagenteresptipo = aCodagagenteresptipo;
    }

    /**
     * Access method for descagagenteresptipo.
     *
     * @return the current value of descagagenteresptipo
     */
    public String getDescagagenteresptipo() {
        return descagagenteresptipo;
    }

    /**
     * Setter method for descagagenteresptipo.
     *
     * @param aDescagagenteresptipo the new value for descagagenteresptipo
     */
    public void setDescagagenteresptipo(String aDescagagenteresptipo) {
        descagagenteresptipo = aDescagagenteresptipo;
    }

    /**
     * Access method for agagenteresp.
     *
     * @return the current value of agagenteresp
     */
    public Set<Agagenteresp> getAgagenteresp() {
        return agagenteresp;
    }

    /**
     * Setter method for agagenteresp.
     *
     * @param aAgagenteresp the new value for agagenteresp
     */
    public void setAgagenteresp(Set<Agagenteresp> aAgagenteresp) {
        agagenteresp = aAgagenteresp;
    }

    /**
     * Compares the key for this instance with another Agagenteresptipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Agagenteresptipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Agagenteresptipo)) {
            return false;
        }
        Agagenteresptipo that = (Agagenteresptipo) other;
        if (this.getCodagagenteresptipo() != that.getCodagagenteresptipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Agagenteresptipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Agagenteresptipo)) return false;
        return this.equalKeys(other) && ((Agagenteresptipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodagagenteresptipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Agagenteresptipo |");
        sb.append(" codagagenteresptipo=").append(getCodagagenteresptipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codagagenteresptipo", Integer.valueOf(getCodagagenteresptipo()));
        return ret;
    }

}
