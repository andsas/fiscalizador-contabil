package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="AGAGENTETIPORECEITA")
public class Agagentetiporeceita implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codagagentetiporeceita";

    @Id
    @Column(name="CODAGAGENTETIPORECEITA", unique=true, nullable=false, precision=10)
    private int codagagentetiporeceita;
    @Column(name="DESCAGAGENTETIPORECEITA", nullable=false, length=250)
    private String descagagentetiporeceita;
    @OneToMany(mappedBy="agagentetiporeceita")
    private Set<Agagente> agagente;

    /** Default constructor. */
    public Agagentetiporeceita() {
        super();
    }

    /**
     * Access method for codagagentetiporeceita.
     *
     * @return the current value of codagagentetiporeceita
     */
    public int getCodagagentetiporeceita() {
        return codagagentetiporeceita;
    }

    /**
     * Setter method for codagagentetiporeceita.
     *
     * @param aCodagagentetiporeceita the new value for codagagentetiporeceita
     */
    public void setCodagagentetiporeceita(int aCodagagentetiporeceita) {
        codagagentetiporeceita = aCodagagentetiporeceita;
    }

    /**
     * Access method for descagagentetiporeceita.
     *
     * @return the current value of descagagentetiporeceita
     */
    public String getDescagagentetiporeceita() {
        return descagagentetiporeceita;
    }

    /**
     * Setter method for descagagentetiporeceita.
     *
     * @param aDescagagentetiporeceita the new value for descagagentetiporeceita
     */
    public void setDescagagentetiporeceita(String aDescagagentetiporeceita) {
        descagagentetiporeceita = aDescagagentetiporeceita;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Compares the key for this instance with another Agagentetiporeceita.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Agagentetiporeceita and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Agagentetiporeceita)) {
            return false;
        }
        Agagentetiporeceita that = (Agagentetiporeceita) other;
        if (this.getCodagagentetiporeceita() != that.getCodagagentetiporeceita()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Agagentetiporeceita.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Agagentetiporeceita)) return false;
        return this.equalKeys(other) && ((Agagentetiporeceita)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodagagentetiporeceita();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Agagentetiporeceita |");
        sb.append(" codagagentetiporeceita=").append(getCodagagentetiporeceita());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codagagentetiporeceita", Integer.valueOf(getCodagagentetiporeceita()));
        return ret;
    }

}
