package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="AGAGENTETRANSP")
public class Agagentetransp implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codagagentetransp";

    @Id
    @Column(name="CODAGAGENTETRANSP", unique=true, nullable=false, precision=10)
    private int codagagentetransp;
    @Column(name="TIPOAGAGENTETRANSP", nullable=false, precision=5)
    private short tipoagagentetransp;
    @Column(name="RNTC", precision=10)
    private int rntc;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODCONFUF")
    private Confuf confuf;

    /** Default constructor. */
    public Agagentetransp() {
        super();
    }

    /**
     * Access method for codagagentetransp.
     *
     * @return the current value of codagagentetransp
     */
    public int getCodagagentetransp() {
        return codagagentetransp;
    }

    /**
     * Setter method for codagagentetransp.
     *
     * @param aCodagagentetransp the new value for codagagentetransp
     */
    public void setCodagagentetransp(int aCodagagentetransp) {
        codagagentetransp = aCodagagentetransp;
    }

    /**
     * Access method for tipoagagentetransp.
     *
     * @return the current value of tipoagagentetransp
     */
    public short getTipoagagentetransp() {
        return tipoagagentetransp;
    }

    /**
     * Setter method for tipoagagentetransp.
     *
     * @param aTipoagagentetransp the new value for tipoagagentetransp
     */
    public void setTipoagagentetransp(short aTipoagagentetransp) {
        tipoagagentetransp = aTipoagagentetransp;
    }

    /**
     * Access method for rntc.
     *
     * @return the current value of rntc
     */
    public int getRntc() {
        return rntc;
    }

    /**
     * Setter method for rntc.
     *
     * @param aRntc the new value for rntc
     */
    public void setRntc(int aRntc) {
        rntc = aRntc;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for confuf.
     *
     * @return the current value of confuf
     */
    public Confuf getConfuf() {
        return confuf;
    }

    /**
     * Setter method for confuf.
     *
     * @param aConfuf the new value for confuf
     */
    public void setConfuf(Confuf aConfuf) {
        confuf = aConfuf;
    }

    /**
     * Compares the key for this instance with another Agagentetransp.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Agagentetransp and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Agagentetransp)) {
            return false;
        }
        Agagentetransp that = (Agagentetransp) other;
        if (this.getCodagagentetransp() != that.getCodagagentetransp()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Agagentetransp.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Agagentetransp)) return false;
        return this.equalKeys(other) && ((Agagentetransp)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodagagentetransp();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Agagentetransp |");
        sb.append(" codagagentetransp=").append(getCodagagentetransp());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codagagentetransp", Integer.valueOf(getCodagagentetransp()));
        return ret;
    }

}
