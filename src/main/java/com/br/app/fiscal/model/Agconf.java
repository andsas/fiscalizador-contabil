package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="AGCONF")
public class Agconf implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codagconf";

    @Id
    @Column(name="CODAGCONF", unique=true, nullable=false, precision=10)
    private int codagconf;
    @Column(name="CTBPLANOCONTACRIAR", precision=5)
    private short ctbplanocontacriar;
    @Column(name="TESTCNPJCPF", precision=5)
    private short testcnpjcpf;
    @Column(name="TESTIERG", precision=5)
    private short testierg;
    @Column(name="TESTENDERECOVAZIO", precision=5)
    private short testenderecovazio;
    @Column(name="TESTCONTATOVAZIO", precision=5)
    private short testcontatovazio;
    @Column(name="AGLIMCREDITOEXPIRACAODIAS", precision=5)
    private short aglimcreditoexpiracaodias;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="CODCTBPLANOCONTAPAI")
    private Ctbplanoconta ctbplanoconta;

    /** Default constructor. */
    public Agconf() {
        super();
    }

    /**
     * Access method for codagconf.
     *
     * @return the current value of codagconf
     */
    public int getCodagconf() {
        return codagconf;
    }

    /**
     * Setter method for codagconf.
     *
     * @param aCodagconf the new value for codagconf
     */
    public void setCodagconf(int aCodagconf) {
        codagconf = aCodagconf;
    }

    /**
     * Access method for ctbplanocontacriar.
     *
     * @return the current value of ctbplanocontacriar
     */
    public short getCtbplanocontacriar() {
        return ctbplanocontacriar;
    }

    /**
     * Setter method for ctbplanocontacriar.
     *
     * @param aCtbplanocontacriar the new value for ctbplanocontacriar
     */
    public void setCtbplanocontacriar(short aCtbplanocontacriar) {
        ctbplanocontacriar = aCtbplanocontacriar;
    }

    /**
     * Access method for testcnpjcpf.
     *
     * @return the current value of testcnpjcpf
     */
    public short getTestcnpjcpf() {
        return testcnpjcpf;
    }

    /**
     * Setter method for testcnpjcpf.
     *
     * @param aTestcnpjcpf the new value for testcnpjcpf
     */
    public void setTestcnpjcpf(short aTestcnpjcpf) {
        testcnpjcpf = aTestcnpjcpf;
    }

    /**
     * Access method for testierg.
     *
     * @return the current value of testierg
     */
    public short getTestierg() {
        return testierg;
    }

    /**
     * Setter method for testierg.
     *
     * @param aTestierg the new value for testierg
     */
    public void setTestierg(short aTestierg) {
        testierg = aTestierg;
    }

    /**
     * Access method for testenderecovazio.
     *
     * @return the current value of testenderecovazio
     */
    public short getTestenderecovazio() {
        return testenderecovazio;
    }

    /**
     * Setter method for testenderecovazio.
     *
     * @param aTestenderecovazio the new value for testenderecovazio
     */
    public void setTestenderecovazio(short aTestenderecovazio) {
        testenderecovazio = aTestenderecovazio;
    }

    /**
     * Access method for testcontatovazio.
     *
     * @return the current value of testcontatovazio
     */
    public short getTestcontatovazio() {
        return testcontatovazio;
    }

    /**
     * Setter method for testcontatovazio.
     *
     * @param aTestcontatovazio the new value for testcontatovazio
     */
    public void setTestcontatovazio(short aTestcontatovazio) {
        testcontatovazio = aTestcontatovazio;
    }

    /**
     * Access method for aglimcreditoexpiracaodias.
     *
     * @return the current value of aglimcreditoexpiracaodias
     */
    public short getAglimcreditoexpiracaodias() {
        return aglimcreditoexpiracaodias;
    }

    /**
     * Setter method for aglimcreditoexpiracaodias.
     *
     * @param aAglimcreditoexpiracaodias the new value for aglimcreditoexpiracaodias
     */
    public void setAglimcreditoexpiracaodias(short aAglimcreditoexpiracaodias) {
        aglimcreditoexpiracaodias = aAglimcreditoexpiracaodias;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Compares the key for this instance with another Agconf.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Agconf and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Agconf)) {
            return false;
        }
        Agconf that = (Agconf) other;
        if (this.getCodagconf() != that.getCodagconf()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Agconf.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Agconf)) return false;
        return this.equalKeys(other) && ((Agconf)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodagconf();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Agconf |");
        sb.append(" codagconf=").append(getCodagconf());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codagconf", Integer.valueOf(getCodagconf()));
        return ret;
    }

}
