package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="AGGRUPO")
public class Aggrupo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codaggrupo";

    @Id
    @Column(name="CODAGGRUPO", unique=true, nullable=false, length=20)
    private String codaggrupo;
    @Column(name="DESCAGGRUPO", nullable=false, length=250)
    private String descaggrupo;
    @Column(name="CODAGPARENTGRUPO", length=20)
    private String codagparentgrupo;
    @Column(name="NIVEL", precision=5)
    private short nivel;
    @OneToMany(mappedBy="aggrupo")
    private Set<Agagente> agagente;
    @OneToMany(mappedBy="aggrupo")
    private Set<Comtabela> comtabela;

    /** Default constructor. */
    public Aggrupo() {
        super();
    }

    /**
     * Access method for codaggrupo.
     *
     * @return the current value of codaggrupo
     */
    public String getCodaggrupo() {
        return codaggrupo;
    }

    /**
     * Setter method for codaggrupo.
     *
     * @param aCodaggrupo the new value for codaggrupo
     */
    public void setCodaggrupo(String aCodaggrupo) {
        codaggrupo = aCodaggrupo;
    }

    /**
     * Access method for descaggrupo.
     *
     * @return the current value of descaggrupo
     */
    public String getDescaggrupo() {
        return descaggrupo;
    }

    /**
     * Setter method for descaggrupo.
     *
     * @param aDescaggrupo the new value for descaggrupo
     */
    public void setDescaggrupo(String aDescaggrupo) {
        descaggrupo = aDescaggrupo;
    }

    /**
     * Access method for codagparentgrupo.
     *
     * @return the current value of codagparentgrupo
     */
    public String getCodagparentgrupo() {
        return codagparentgrupo;
    }

    /**
     * Setter method for codagparentgrupo.
     *
     * @param aCodagparentgrupo the new value for codagparentgrupo
     */
    public void setCodagparentgrupo(String aCodagparentgrupo) {
        codagparentgrupo = aCodagparentgrupo;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for comtabela.
     *
     * @return the current value of comtabela
     */
    public Set<Comtabela> getComtabela() {
        return comtabela;
    }

    /**
     * Setter method for comtabela.
     *
     * @param aComtabela the new value for comtabela
     */
    public void setComtabela(Set<Comtabela> aComtabela) {
        comtabela = aComtabela;
    }

    /**
     * Compares the key for this instance with another Aggrupo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Aggrupo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Aggrupo)) {
            return false;
        }
        Aggrupo that = (Aggrupo) other;
        Object myCodaggrupo = this.getCodaggrupo();
        Object yourCodaggrupo = that.getCodaggrupo();
        if (myCodaggrupo==null ? yourCodaggrupo!=null : !myCodaggrupo.equals(yourCodaggrupo)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Aggrupo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Aggrupo)) return false;
        return this.equalKeys(other) && ((Aggrupo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodaggrupo() == null) {
            i = 0;
        } else {
            i = getCodaggrupo().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Aggrupo |");
        sb.append(" codaggrupo=").append(getCodaggrupo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codaggrupo", getCodaggrupo());
        return ret;
    }

}
