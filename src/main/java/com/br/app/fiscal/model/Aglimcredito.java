package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="AGLIMCREDITO")
public class Aglimcredito implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codaglimcredito";

    @Id
    @Column(name="CODAGLIMCREDITO", unique=true, nullable=false, precision=10)
    private int codaglimcredito;
    @Column(name="LIMCREDITO", nullable=false, precision=15, scale=4)
    private BigDecimal limcredito;
    @Column(name="DATALIBERACAO", nullable=false)
    private Timestamp dataliberacao;
    @Column(name="DATAPEDIDO", nullable=false)
    private Timestamp datapedido;
    @Column(name="LIMUSADO", precision=15, scale=4)
    private BigDecimal limusado;
    @Column(name="OBS")
    private String obs;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODAGLIMSOLCREDITO")
    private Aglimcreditosol aglimcreditosol;

    /** Default constructor. */
    public Aglimcredito() {
        super();
    }

    /**
     * Access method for codaglimcredito.
     *
     * @return the current value of codaglimcredito
     */
    public int getCodaglimcredito() {
        return codaglimcredito;
    }

    /**
     * Setter method for codaglimcredito.
     *
     * @param aCodaglimcredito the new value for codaglimcredito
     */
    public void setCodaglimcredito(int aCodaglimcredito) {
        codaglimcredito = aCodaglimcredito;
    }

    /**
     * Access method for limcredito.
     *
     * @return the current value of limcredito
     */
    public BigDecimal getLimcredito() {
        return limcredito;
    }

    /**
     * Setter method for limcredito.
     *
     * @param aLimcredito the new value for limcredito
     */
    public void setLimcredito(BigDecimal aLimcredito) {
        limcredito = aLimcredito;
    }

    /**
     * Access method for dataliberacao.
     *
     * @return the current value of dataliberacao
     */
    public Timestamp getDataliberacao() {
        return dataliberacao;
    }

    /**
     * Setter method for dataliberacao.
     *
     * @param aDataliberacao the new value for dataliberacao
     */
    public void setDataliberacao(Timestamp aDataliberacao) {
        dataliberacao = aDataliberacao;
    }

    /**
     * Access method for datapedido.
     *
     * @return the current value of datapedido
     */
    public Timestamp getDatapedido() {
        return datapedido;
    }

    /**
     * Setter method for datapedido.
     *
     * @param aDatapedido the new value for datapedido
     */
    public void setDatapedido(Timestamp aDatapedido) {
        datapedido = aDatapedido;
    }

    /**
     * Access method for limusado.
     *
     * @return the current value of limusado
     */
    public BigDecimal getLimusado() {
        return limusado;
    }

    /**
     * Setter method for limusado.
     *
     * @param aLimusado the new value for limusado
     */
    public void setLimusado(BigDecimal aLimusado) {
        limusado = aLimusado;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for aglimcreditosol.
     *
     * @return the current value of aglimcreditosol
     */
    public Aglimcreditosol getAglimcreditosol() {
        return aglimcreditosol;
    }

    /**
     * Setter method for aglimcreditosol.
     *
     * @param aAglimcreditosol the new value for aglimcreditosol
     */
    public void setAglimcreditosol(Aglimcreditosol aAglimcreditosol) {
        aglimcreditosol = aAglimcreditosol;
    }

    /**
     * Compares the key for this instance with another Aglimcredito.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Aglimcredito and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Aglimcredito)) {
            return false;
        }
        Aglimcredito that = (Aglimcredito) other;
        if (this.getCodaglimcredito() != that.getCodaglimcredito()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Aglimcredito.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Aglimcredito)) return false;
        return this.equalKeys(other) && ((Aglimcredito)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodaglimcredito();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Aglimcredito |");
        sb.append(" codaglimcredito=").append(getCodaglimcredito());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codaglimcredito", Integer.valueOf(getCodaglimcredito()));
        return ret;
    }

}
