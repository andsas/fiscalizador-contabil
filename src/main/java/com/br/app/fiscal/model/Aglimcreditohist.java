package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="AGLIMCREDITOHIST")
public class Aglimcreditohist implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codaglimcreditohist";

    @Id
    @Column(name="CODAGLIMCREDITOHIST", unique=true, nullable=false, precision=10)
    private int codaglimcreditohist;
    @Column(name="NOMETABELA", nullable=false, length=60)
    private String nometabela;
    @Column(name="CODTABELA", nullable=false, length=60)
    private String codtabela;
    @Column(name="VALOR", nullable=false, precision=15, scale=4)
    private BigDecimal valor;
    @Column(name="DESCAGLIMCREDITOHIST", nullable=false, length=250)
    private String descaglimcreditohist;
    @Column(name="DATA", nullable=false)
    private Timestamp data;

    /** Default constructor. */
    public Aglimcreditohist() {
        super();
    }

    /**
     * Access method for codaglimcreditohist.
     *
     * @return the current value of codaglimcreditohist
     */
    public int getCodaglimcreditohist() {
        return codaglimcreditohist;
    }

    /**
     * Setter method for codaglimcreditohist.
     *
     * @param aCodaglimcreditohist the new value for codaglimcreditohist
     */
    public void setCodaglimcreditohist(int aCodaglimcreditohist) {
        codaglimcreditohist = aCodaglimcreditohist;
    }

    /**
     * Access method for nometabela.
     *
     * @return the current value of nometabela
     */
    public String getNometabela() {
        return nometabela;
    }

    /**
     * Setter method for nometabela.
     *
     * @param aNometabela the new value for nometabela
     */
    public void setNometabela(String aNometabela) {
        nometabela = aNometabela;
    }

    /**
     * Access method for codtabela.
     *
     * @return the current value of codtabela
     */
    public String getCodtabela() {
        return codtabela;
    }

    /**
     * Setter method for codtabela.
     *
     * @param aCodtabela the new value for codtabela
     */
    public void setCodtabela(String aCodtabela) {
        codtabela = aCodtabela;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(BigDecimal aValor) {
        valor = aValor;
    }

    /**
     * Access method for descaglimcreditohist.
     *
     * @return the current value of descaglimcreditohist
     */
    public String getDescaglimcreditohist() {
        return descaglimcreditohist;
    }

    /**
     * Setter method for descaglimcreditohist.
     *
     * @param aDescaglimcreditohist the new value for descaglimcreditohist
     */
    public void setDescaglimcreditohist(String aDescaglimcreditohist) {
        descaglimcreditohist = aDescaglimcreditohist;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Compares the key for this instance with another Aglimcreditohist.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Aglimcreditohist and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Aglimcreditohist)) {
            return false;
        }
        Aglimcreditohist that = (Aglimcreditohist) other;
        if (this.getCodaglimcreditohist() != that.getCodaglimcreditohist()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Aglimcreditohist.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Aglimcreditohist)) return false;
        return this.equalKeys(other) && ((Aglimcreditohist)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodaglimcreditohist();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Aglimcreditohist |");
        sb.append(" codaglimcreditohist=").append(getCodaglimcreditohist());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codaglimcreditohist", Integer.valueOf(getCodaglimcreditohist()));
        return ret;
    }

}
