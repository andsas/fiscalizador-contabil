package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="AGLIMCREDITOSOL")
public class Aglimcreditosol implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codaglimcreditosol";

    @Id
    @Column(name="CODAGLIMCREDITOSOL", unique=true, nullable=false, precision=10)
    private int codaglimcreditosol;
    @Column(name="LIMCREDITO", nullable=false, precision=15, scale=4)
    private BigDecimal limcredito;
    @Column(name="DESCAGLIMCREDITOSOL", nullable=false, length=250)
    private String descaglimcreditosol;
    @Column(name="OBS")
    private String obs;
    @Column(name="DATA")
    private Timestamp data;
    @Column(name="APROVADA", precision=5)
    private short aprovada;
    @Column(name="REPROVADAMOTIVO")
    private String reprovadamotivo;
    @OneToMany(mappedBy="aglimcreditosol")
    private Set<Aglimcredito> aglimcredito;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIOSOL", nullable=false)
    private Confusuario confusuario;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIOAUT")
    private Confusuario confusuario2;

    /** Default constructor. */
    public Aglimcreditosol() {
        super();
    }

    /**
     * Access method for codaglimcreditosol.
     *
     * @return the current value of codaglimcreditosol
     */
    public int getCodaglimcreditosol() {
        return codaglimcreditosol;
    }

    /**
     * Setter method for codaglimcreditosol.
     *
     * @param aCodaglimcreditosol the new value for codaglimcreditosol
     */
    public void setCodaglimcreditosol(int aCodaglimcreditosol) {
        codaglimcreditosol = aCodaglimcreditosol;
    }

    /**
     * Access method for limcredito.
     *
     * @return the current value of limcredito
     */
    public BigDecimal getLimcredito() {
        return limcredito;
    }

    /**
     * Setter method for limcredito.
     *
     * @param aLimcredito the new value for limcredito
     */
    public void setLimcredito(BigDecimal aLimcredito) {
        limcredito = aLimcredito;
    }

    /**
     * Access method for descaglimcreditosol.
     *
     * @return the current value of descaglimcreditosol
     */
    public String getDescaglimcreditosol() {
        return descaglimcreditosol;
    }

    /**
     * Setter method for descaglimcreditosol.
     *
     * @param aDescaglimcreditosol the new value for descaglimcreditosol
     */
    public void setDescaglimcreditosol(String aDescaglimcreditosol) {
        descaglimcreditosol = aDescaglimcreditosol;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for aprovada.
     *
     * @return the current value of aprovada
     */
    public short getAprovada() {
        return aprovada;
    }

    /**
     * Setter method for aprovada.
     *
     * @param aAprovada the new value for aprovada
     */
    public void setAprovada(short aAprovada) {
        aprovada = aAprovada;
    }

    /**
     * Access method for reprovadamotivo.
     *
     * @return the current value of reprovadamotivo
     */
    public String getReprovadamotivo() {
        return reprovadamotivo;
    }

    /**
     * Setter method for reprovadamotivo.
     *
     * @param aReprovadamotivo the new value for reprovadamotivo
     */
    public void setReprovadamotivo(String aReprovadamotivo) {
        reprovadamotivo = aReprovadamotivo;
    }

    /**
     * Access method for aglimcredito.
     *
     * @return the current value of aglimcredito
     */
    public Set<Aglimcredito> getAglimcredito() {
        return aglimcredito;
    }

    /**
     * Setter method for aglimcredito.
     *
     * @param aAglimcredito the new value for aglimcredito
     */
    public void setAglimcredito(Set<Aglimcredito> aAglimcredito) {
        aglimcredito = aAglimcredito;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for confusuario2.
     *
     * @return the current value of confusuario2
     */
    public Confusuario getConfusuario2() {
        return confusuario2;
    }

    /**
     * Setter method for confusuario2.
     *
     * @param aConfusuario2 the new value for confusuario2
     */
    public void setConfusuario2(Confusuario aConfusuario2) {
        confusuario2 = aConfusuario2;
    }

    /**
     * Compares the key for this instance with another Aglimcreditosol.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Aglimcreditosol and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Aglimcreditosol)) {
            return false;
        }
        Aglimcreditosol that = (Aglimcreditosol) other;
        if (this.getCodaglimcreditosol() != that.getCodaglimcreditosol()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Aglimcreditosol.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Aglimcreditosol)) return false;
        return this.equalKeys(other) && ((Aglimcreditosol)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodaglimcreditosol();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Aglimcreditosol |");
        sb.append(" codaglimcreditosol=").append(getCodaglimcreditosol());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codaglimcreditosol", Integer.valueOf(getCodaglimcreditosol()));
        return ret;
    }

}
