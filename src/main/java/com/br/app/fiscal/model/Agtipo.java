package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="AGTIPO")
public class Agtipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codagtipo";

    @Id
    @Column(name="CODAGTIPO", unique=true, nullable=false, precision=10)
    private int codagtipo;
    @Column(name="DESCAGTIPO", nullable=false, length=250)
    private String descagtipo;
    @Column(name="TIPOAGTIPO", nullable=false, precision=5)
    private short tipoagtipo;
    @OneToMany(mappedBy="agtipo")
    private Set<Agagente> agagente;
    @OneToMany(mappedBy="agtipo")
    private Set<Comtabela> comtabela;
    @OneToMany(mappedBy="agtipo")
    private Set<Opconfsaida> opconfsaida;

    /** Default constructor. */
    public Agtipo() {
        super();
    }

    /**
     * Access method for codagtipo.
     *
     * @return the current value of codagtipo
     */
    public int getCodagtipo() {
        return codagtipo;
    }

    /**
     * Setter method for codagtipo.
     *
     * @param aCodagtipo the new value for codagtipo
     */
    public void setCodagtipo(int aCodagtipo) {
        codagtipo = aCodagtipo;
    }

    /**
     * Access method for descagtipo.
     *
     * @return the current value of descagtipo
     */
    public String getDescagtipo() {
        return descagtipo;
    }

    /**
     * Setter method for descagtipo.
     *
     * @param aDescagtipo the new value for descagtipo
     */
    public void setDescagtipo(String aDescagtipo) {
        descagtipo = aDescagtipo;
    }

    /**
     * Access method for tipoagtipo.
     *
     * @return the current value of tipoagtipo
     */
    public short getTipoagtipo() {
        return tipoagtipo;
    }

    /**
     * Setter method for tipoagtipo.
     *
     * @param aTipoagtipo the new value for tipoagtipo
     */
    public void setTipoagtipo(short aTipoagtipo) {
        tipoagtipo = aTipoagtipo;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for comtabela.
     *
     * @return the current value of comtabela
     */
    public Set<Comtabela> getComtabela() {
        return comtabela;
    }

    /**
     * Setter method for comtabela.
     *
     * @param aComtabela the new value for comtabela
     */
    public void setComtabela(Set<Comtabela> aComtabela) {
        comtabela = aComtabela;
    }

    /**
     * Access method for opconfsaida.
     *
     * @return the current value of opconfsaida
     */
    public Set<Opconfsaida> getOpconfsaida() {
        return opconfsaida;
    }

    /**
     * Setter method for opconfsaida.
     *
     * @param aOpconfsaida the new value for opconfsaida
     */
    public void setOpconfsaida(Set<Opconfsaida> aOpconfsaida) {
        opconfsaida = aOpconfsaida;
    }

    /**
     * Compares the key for this instance with another Agtipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Agtipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Agtipo)) {
            return false;
        }
        Agtipo that = (Agtipo) other;
        if (this.getCodagtipo() != that.getCodagtipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Agtipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Agtipo)) return false;
        return this.equalKeys(other) && ((Agtipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodagtipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Agtipo |");
        sb.append(" codagtipo=").append(getCodagtipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codagtipo", Integer.valueOf(getCodagtipo()));
        return ret;
    }

}
