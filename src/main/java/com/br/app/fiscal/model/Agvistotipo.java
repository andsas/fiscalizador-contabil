package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="AGVISTOTIPO")
public class Agvistotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codagvistotipo";

    @Id
    @Column(name="CODAGVISTOTIPO", unique=true, nullable=false, length=20)
    private String codagvistotipo;
    @Column(name="DESCAGVISTOTIPO", nullable=false, length=250)
    private String descagvistotipo;
    @Column(name="OBS")
    private String obs;
    @OneToMany(mappedBy="agvistotipo")
    private Set<Agagente> agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFPAIS", nullable=false)
    private Confpais confpais;

    /** Default constructor. */
    public Agvistotipo() {
        super();
    }

    /**
     * Access method for codagvistotipo.
     *
     * @return the current value of codagvistotipo
     */
    public String getCodagvistotipo() {
        return codagvistotipo;
    }

    /**
     * Setter method for codagvistotipo.
     *
     * @param aCodagvistotipo the new value for codagvistotipo
     */
    public void setCodagvistotipo(String aCodagvistotipo) {
        codagvistotipo = aCodagvistotipo;
    }

    /**
     * Access method for descagvistotipo.
     *
     * @return the current value of descagvistotipo
     */
    public String getDescagvistotipo() {
        return descagvistotipo;
    }

    /**
     * Setter method for descagvistotipo.
     *
     * @param aDescagvistotipo the new value for descagvistotipo
     */
    public void setDescagvistotipo(String aDescagvistotipo) {
        descagvistotipo = aDescagvistotipo;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for confpais.
     *
     * @return the current value of confpais
     */
    public Confpais getConfpais() {
        return confpais;
    }

    /**
     * Setter method for confpais.
     *
     * @param aConfpais the new value for confpais
     */
    public void setConfpais(Confpais aConfpais) {
        confpais = aConfpais;
    }

    /**
     * Compares the key for this instance with another Agvistotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Agvistotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Agvistotipo)) {
            return false;
        }
        Agvistotipo that = (Agvistotipo) other;
        Object myCodagvistotipo = this.getCodagvistotipo();
        Object yourCodagvistotipo = that.getCodagvistotipo();
        if (myCodagvistotipo==null ? yourCodagvistotipo!=null : !myCodagvistotipo.equals(yourCodagvistotipo)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Agvistotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Agvistotipo)) return false;
        return this.equalKeys(other) && ((Agvistotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodagvistotipo() == null) {
            i = 0;
        } else {
            i = getCodagvistotipo().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Agvistotipo |");
        sb.append(" codagvistotipo=").append(getCodagvistotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codagvistotipo", getCodagvistotipo());
        return ret;
    }

}
