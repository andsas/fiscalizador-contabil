package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="COBRFORMA")
public class Cobrforma implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcobrforma";

    @Id
    @Column(name="CODCOBRFORMA", unique=true, nullable=false, precision=10)
    private int codcobrforma;
    @Column(name="DESCCOBRFORMA", nullable=false, length=250)
    private String desccobrforma;
    @Column(name="TIPOCOBRFORMA", nullable=false, precision=5)
    private short tipocobrforma;
    @Column(name="BANDEIRACOBRFORMA", precision=5)
    private short bandeiracobrforma;
    @ManyToOne
    @JoinColumn(name="CTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta2;
    @ManyToOne
    @JoinColumn(name="FINCONTACREDITO")
    private Finplanoconta finplanoconta;
    @ManyToOne
    @JoinColumn(name="FINCONTADEBITO")
    private Finplanoconta finplanoconta2;
    @ManyToOne
    @JoinColumn(name="FLUXOCONTACREDITO")
    private Finfluxoplanoconta finfluxoplanoconta;
    @ManyToOne
    @JoinColumn(name="FLUXOCONTADEBITO")
    private Finfluxoplanoconta finfluxoplanoconta2;
    @ManyToOne
    @JoinColumn(name="ORCCONTACREDITO")
    private Orcplanoconta orcplanoconta;
    @ManyToOne
    @JoinColumn(name="ORCCONTADEBITO")
    private Orcplanoconta orcplanoconta2;
    @ManyToOne
    @JoinColumn(name="CODFINOPERADORA")
    private Finoperadora finoperadora;
    @OneToMany(mappedBy="cobrforma")
    private Set<Cobrplano> cobrplano;
    @OneToMany(mappedBy="cobrforma")
    private Set<Finbaixa> finbaixa;
    @OneToMany(mappedBy="cobrforma")
    private Set<Folevento> folevento;
    @OneToMany(mappedBy="cobrforma")
    private Set<Optransacaocobr> optransacaocobr;
    @OneToMany(mappedBy="cobrforma")
    private Set<Parklancamento> parklancamento;

    /** Default constructor. */
    public Cobrforma() {
        super();
    }

    /**
     * Access method for codcobrforma.
     *
     * @return the current value of codcobrforma
     */
    public int getCodcobrforma() {
        return codcobrforma;
    }

    /**
     * Setter method for codcobrforma.
     *
     * @param aCodcobrforma the new value for codcobrforma
     */
    public void setCodcobrforma(int aCodcobrforma) {
        codcobrforma = aCodcobrforma;
    }

    /**
     * Access method for desccobrforma.
     *
     * @return the current value of desccobrforma
     */
    public String getDesccobrforma() {
        return desccobrforma;
    }

    /**
     * Setter method for desccobrforma.
     *
     * @param aDesccobrforma the new value for desccobrforma
     */
    public void setDesccobrforma(String aDesccobrforma) {
        desccobrforma = aDesccobrforma;
    }

    /**
     * Access method for tipocobrforma.
     *
     * @return the current value of tipocobrforma
     */
    public short getTipocobrforma() {
        return tipocobrforma;
    }

    /**
     * Setter method for tipocobrforma.
     *
     * @param aTipocobrforma the new value for tipocobrforma
     */
    public void setTipocobrforma(short aTipocobrforma) {
        tipocobrforma = aTipocobrforma;
    }

    /**
     * Access method for bandeiracobrforma.
     *
     * @return the current value of bandeiracobrforma
     */
    public short getBandeiracobrforma() {
        return bandeiracobrforma;
    }

    /**
     * Setter method for bandeiracobrforma.
     *
     * @param aBandeiracobrforma the new value for bandeiracobrforma
     */
    public void setBandeiracobrforma(short aBandeiracobrforma) {
        bandeiracobrforma = aBandeiracobrforma;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for finplanoconta.
     *
     * @return the current value of finplanoconta
     */
    public Finplanoconta getFinplanoconta() {
        return finplanoconta;
    }

    /**
     * Setter method for finplanoconta.
     *
     * @param aFinplanoconta the new value for finplanoconta
     */
    public void setFinplanoconta(Finplanoconta aFinplanoconta) {
        finplanoconta = aFinplanoconta;
    }

    /**
     * Access method for finplanoconta2.
     *
     * @return the current value of finplanoconta2
     */
    public Finplanoconta getFinplanoconta2() {
        return finplanoconta2;
    }

    /**
     * Setter method for finplanoconta2.
     *
     * @param aFinplanoconta2 the new value for finplanoconta2
     */
    public void setFinplanoconta2(Finplanoconta aFinplanoconta2) {
        finplanoconta2 = aFinplanoconta2;
    }

    /**
     * Access method for finfluxoplanoconta.
     *
     * @return the current value of finfluxoplanoconta
     */
    public Finfluxoplanoconta getFinfluxoplanoconta() {
        return finfluxoplanoconta;
    }

    /**
     * Setter method for finfluxoplanoconta.
     *
     * @param aFinfluxoplanoconta the new value for finfluxoplanoconta
     */
    public void setFinfluxoplanoconta(Finfluxoplanoconta aFinfluxoplanoconta) {
        finfluxoplanoconta = aFinfluxoplanoconta;
    }

    /**
     * Access method for finfluxoplanoconta2.
     *
     * @return the current value of finfluxoplanoconta2
     */
    public Finfluxoplanoconta getFinfluxoplanoconta2() {
        return finfluxoplanoconta2;
    }

    /**
     * Setter method for finfluxoplanoconta2.
     *
     * @param aFinfluxoplanoconta2 the new value for finfluxoplanoconta2
     */
    public void setFinfluxoplanoconta2(Finfluxoplanoconta aFinfluxoplanoconta2) {
        finfluxoplanoconta2 = aFinfluxoplanoconta2;
    }

    /**
     * Access method for orcplanoconta.
     *
     * @return the current value of orcplanoconta
     */
    public Orcplanoconta getOrcplanoconta() {
        return orcplanoconta;
    }

    /**
     * Setter method for orcplanoconta.
     *
     * @param aOrcplanoconta the new value for orcplanoconta
     */
    public void setOrcplanoconta(Orcplanoconta aOrcplanoconta) {
        orcplanoconta = aOrcplanoconta;
    }

    /**
     * Access method for orcplanoconta2.
     *
     * @return the current value of orcplanoconta2
     */
    public Orcplanoconta getOrcplanoconta2() {
        return orcplanoconta2;
    }

    /**
     * Setter method for orcplanoconta2.
     *
     * @param aOrcplanoconta2 the new value for orcplanoconta2
     */
    public void setOrcplanoconta2(Orcplanoconta aOrcplanoconta2) {
        orcplanoconta2 = aOrcplanoconta2;
    }

    /**
     * Access method for finoperadora.
     *
     * @return the current value of finoperadora
     */
    public Finoperadora getFinoperadora() {
        return finoperadora;
    }

    /**
     * Setter method for finoperadora.
     *
     * @param aFinoperadora the new value for finoperadora
     */
    public void setFinoperadora(Finoperadora aFinoperadora) {
        finoperadora = aFinoperadora;
    }

    /**
     * Access method for cobrplano.
     *
     * @return the current value of cobrplano
     */
    public Set<Cobrplano> getCobrplano() {
        return cobrplano;
    }

    /**
     * Setter method for cobrplano.
     *
     * @param aCobrplano the new value for cobrplano
     */
    public void setCobrplano(Set<Cobrplano> aCobrplano) {
        cobrplano = aCobrplano;
    }

    /**
     * Access method for finbaixa.
     *
     * @return the current value of finbaixa
     */
    public Set<Finbaixa> getFinbaixa() {
        return finbaixa;
    }

    /**
     * Setter method for finbaixa.
     *
     * @param aFinbaixa the new value for finbaixa
     */
    public void setFinbaixa(Set<Finbaixa> aFinbaixa) {
        finbaixa = aFinbaixa;
    }

    /**
     * Access method for folevento.
     *
     * @return the current value of folevento
     */
    public Set<Folevento> getFolevento() {
        return folevento;
    }

    /**
     * Setter method for folevento.
     *
     * @param aFolevento the new value for folevento
     */
    public void setFolevento(Set<Folevento> aFolevento) {
        folevento = aFolevento;
    }

    /**
     * Access method for optransacaocobr.
     *
     * @return the current value of optransacaocobr
     */
    public Set<Optransacaocobr> getOptransacaocobr() {
        return optransacaocobr;
    }

    /**
     * Setter method for optransacaocobr.
     *
     * @param aOptransacaocobr the new value for optransacaocobr
     */
    public void setOptransacaocobr(Set<Optransacaocobr> aOptransacaocobr) {
        optransacaocobr = aOptransacaocobr;
    }

    /**
     * Access method for parklancamento.
     *
     * @return the current value of parklancamento
     */
    public Set<Parklancamento> getParklancamento() {
        return parklancamento;
    }

    /**
     * Setter method for parklancamento.
     *
     * @param aParklancamento the new value for parklancamento
     */
    public void setParklancamento(Set<Parklancamento> aParklancamento) {
        parklancamento = aParklancamento;
    }

    /**
     * Compares the key for this instance with another Cobrforma.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Cobrforma and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Cobrforma)) {
            return false;
        }
        Cobrforma that = (Cobrforma) other;
        if (this.getCodcobrforma() != that.getCodcobrforma()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Cobrforma.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Cobrforma)) return false;
        return this.equalKeys(other) && ((Cobrforma)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcobrforma();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Cobrforma |");
        sb.append(" codcobrforma=").append(getCodcobrforma());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcobrforma", Integer.valueOf(getCodcobrforma()));
        return ret;
    }

}
