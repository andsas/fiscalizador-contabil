package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="COBRPLANO")
public class Cobrplano implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcobrplano";

    @Id
    @Column(name="CODCOBRPLANO", unique=true, nullable=false, precision=10)
    private int codcobrplano;
    @Column(name="DESCCOBRPLANO", nullable=false, length=250)
    private String desccobrplano;
    @OneToMany(mappedBy="cobrplano")
    private Set<Agagente> agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCOBRFORMA", nullable=false)
    private Cobrforma cobrforma;
    @OneToMany(mappedBy="cobrplano")
    private Set<Optransacaocobr> optransacaocobr;

    /** Default constructor. */
    public Cobrplano() {
        super();
    }

    /**
     * Access method for codcobrplano.
     *
     * @return the current value of codcobrplano
     */
    public int getCodcobrplano() {
        return codcobrplano;
    }

    /**
     * Setter method for codcobrplano.
     *
     * @param aCodcobrplano the new value for codcobrplano
     */
    public void setCodcobrplano(int aCodcobrplano) {
        codcobrplano = aCodcobrplano;
    }

    /**
     * Access method for desccobrplano.
     *
     * @return the current value of desccobrplano
     */
    public String getDesccobrplano() {
        return desccobrplano;
    }

    /**
     * Setter method for desccobrplano.
     *
     * @param aDesccobrplano the new value for desccobrplano
     */
    public void setDesccobrplano(String aDesccobrplano) {
        desccobrplano = aDesccobrplano;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Cobrforma getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Cobrforma aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Access method for optransacaocobr.
     *
     * @return the current value of optransacaocobr
     */
    public Set<Optransacaocobr> getOptransacaocobr() {
        return optransacaocobr;
    }

    /**
     * Setter method for optransacaocobr.
     *
     * @param aOptransacaocobr the new value for optransacaocobr
     */
    public void setOptransacaocobr(Set<Optransacaocobr> aOptransacaocobr) {
        optransacaocobr = aOptransacaocobr;
    }

    /**
     * Compares the key for this instance with another Cobrplano.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Cobrplano and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Cobrplano)) {
            return false;
        }
        Cobrplano that = (Cobrplano) other;
        if (this.getCodcobrplano() != that.getCodcobrplano()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Cobrplano.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Cobrplano)) return false;
        return this.equalKeys(other) && ((Cobrplano)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcobrplano();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Cobrplano |");
        sb.append(" codcobrplano=").append(getCodcobrplano());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcobrplano", Integer.valueOf(getCodcobrplano()));
        return ret;
    }

}
