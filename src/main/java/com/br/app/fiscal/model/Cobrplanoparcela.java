package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="COBRPLANOPARCELA")
public class Cobrplanoparcela implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcobrplanoparcela";

    @Id
    @Column(name="CODCOBRPLANOPARCELA", unique=true, nullable=false, precision=10)
    private int codcobrplanoparcela;
    @Column(name="CODCOBRPLANO", nullable=false, precision=10)
    private int codcobrplano;
    @Column(name="NUMCOBRPLANOPARCELA", precision=10)
    private int numcobrplanoparcela;
    @Column(name="DIAFIXO", precision=5)
    private short diafixo;
    @Column(name="DATAFIXA")
    private Date datafixa;
    @Column(name="DIAS", precision=5)
    private short dias;
    @Column(name="PERC", length=15)
    private double perc;

    /** Default constructor. */
    public Cobrplanoparcela() {
        super();
    }

    /**
     * Access method for codcobrplanoparcela.
     *
     * @return the current value of codcobrplanoparcela
     */
    public int getCodcobrplanoparcela() {
        return codcobrplanoparcela;
    }

    /**
     * Setter method for codcobrplanoparcela.
     *
     * @param aCodcobrplanoparcela the new value for codcobrplanoparcela
     */
    public void setCodcobrplanoparcela(int aCodcobrplanoparcela) {
        codcobrplanoparcela = aCodcobrplanoparcela;
    }

    /**
     * Access method for codcobrplano.
     *
     * @return the current value of codcobrplano
     */
    public int getCodcobrplano() {
        return codcobrplano;
    }

    /**
     * Setter method for codcobrplano.
     *
     * @param aCodcobrplano the new value for codcobrplano
     */
    public void setCodcobrplano(int aCodcobrplano) {
        codcobrplano = aCodcobrplano;
    }

    /**
     * Access method for numcobrplanoparcela.
     *
     * @return the current value of numcobrplanoparcela
     */
    public int getNumcobrplanoparcela() {
        return numcobrplanoparcela;
    }

    /**
     * Setter method for numcobrplanoparcela.
     *
     * @param aNumcobrplanoparcela the new value for numcobrplanoparcela
     */
    public void setNumcobrplanoparcela(int aNumcobrplanoparcela) {
        numcobrplanoparcela = aNumcobrplanoparcela;
    }

    /**
     * Access method for diafixo.
     *
     * @return the current value of diafixo
     */
    public short getDiafixo() {
        return diafixo;
    }

    /**
     * Setter method for diafixo.
     *
     * @param aDiafixo the new value for diafixo
     */
    public void setDiafixo(short aDiafixo) {
        diafixo = aDiafixo;
    }

    /**
     * Access method for datafixa.
     *
     * @return the current value of datafixa
     */
    public Date getDatafixa() {
        return datafixa;
    }

    /**
     * Setter method for datafixa.
     *
     * @param aDatafixa the new value for datafixa
     */
    public void setDatafixa(Date aDatafixa) {
        datafixa = aDatafixa;
    }

    /**
     * Access method for dias.
     *
     * @return the current value of dias
     */
    public short getDias() {
        return dias;
    }

    /**
     * Setter method for dias.
     *
     * @param aDias the new value for dias
     */
    public void setDias(short aDias) {
        dias = aDias;
    }

    /**
     * Access method for perc.
     *
     * @return the current value of perc
     */
    public double getPerc() {
        return perc;
    }

    /**
     * Setter method for perc.
     *
     * @param aPerc the new value for perc
     */
    public void setPerc(double aPerc) {
        perc = aPerc;
    }

    /**
     * Compares the key for this instance with another Cobrplanoparcela.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Cobrplanoparcela and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Cobrplanoparcela)) {
            return false;
        }
        Cobrplanoparcela that = (Cobrplanoparcela) other;
        if (this.getCodcobrplanoparcela() != that.getCodcobrplanoparcela()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Cobrplanoparcela.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Cobrplanoparcela)) return false;
        return this.equalKeys(other) && ((Cobrplanoparcela)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcobrplanoparcela();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Cobrplanoparcela |");
        sb.append(" codcobrplanoparcela=").append(getCodcobrplanoparcela());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcobrplanoparcela", Integer.valueOf(getCodcobrplanoparcela()));
        return ret;
    }

}
