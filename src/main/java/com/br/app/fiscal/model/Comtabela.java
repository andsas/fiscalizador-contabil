package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="COMTABELA")
public class Comtabela implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcomtabela";

    @Id
    @Column(name="CODCOMTABELA", unique=true, nullable=false, precision=10)
    private int codcomtabela;
    @Column(name="DESCCOMTABELA", nullable=false, length=250)
    private String desccomtabela;
    @Column(name="ATIVO", nullable=false, precision=5)
    private short ativo;
    @Column(name="FORMULA", nullable=false)
    private String formula;
    @Column(name="DATAVALIDADE", nullable=false)
    private Date datavalidade;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCOMCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="CODCOMCONFUF")
    private Confuf confuf;
    @ManyToOne
    @JoinColumn(name="CODCOMCONFCIDADE")
    private Confcidade confcidade;
    @ManyToOne
    @JoinColumn(name="CODCOMAGGRUPO")
    private Aggrupo aggrupo;
    @ManyToOne
    @JoinColumn(name="CODCOMAGTIPO")
    private Agtipo agtipo;
    @ManyToOne
    @JoinColumn(name="CODCOMAGAGENTE")
    private Agagente agagente;
    @OneToMany(mappedBy="comtabela")
    private Set<Optipo> optipo;

    /** Default constructor. */
    public Comtabela() {
        super();
    }

    /**
     * Access method for codcomtabela.
     *
     * @return the current value of codcomtabela
     */
    public int getCodcomtabela() {
        return codcomtabela;
    }

    /**
     * Setter method for codcomtabela.
     *
     * @param aCodcomtabela the new value for codcomtabela
     */
    public void setCodcomtabela(int aCodcomtabela) {
        codcomtabela = aCodcomtabela;
    }

    /**
     * Access method for desccomtabela.
     *
     * @return the current value of desccomtabela
     */
    public String getDesccomtabela() {
        return desccomtabela;
    }

    /**
     * Setter method for desccomtabela.
     *
     * @param aDesccomtabela the new value for desccomtabela
     */
    public void setDesccomtabela(String aDesccomtabela) {
        desccomtabela = aDesccomtabela;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for formula.
     *
     * @return the current value of formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Setter method for formula.
     *
     * @param aFormula the new value for formula
     */
    public void setFormula(String aFormula) {
        formula = aFormula;
    }

    /**
     * Access method for datavalidade.
     *
     * @return the current value of datavalidade
     */
    public Date getDatavalidade() {
        return datavalidade;
    }

    /**
     * Setter method for datavalidade.
     *
     * @param aDatavalidade the new value for datavalidade
     */
    public void setDatavalidade(Date aDatavalidade) {
        datavalidade = aDatavalidade;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for confuf.
     *
     * @return the current value of confuf
     */
    public Confuf getConfuf() {
        return confuf;
    }

    /**
     * Setter method for confuf.
     *
     * @param aConfuf the new value for confuf
     */
    public void setConfuf(Confuf aConfuf) {
        confuf = aConfuf;
    }

    /**
     * Access method for confcidade.
     *
     * @return the current value of confcidade
     */
    public Confcidade getConfcidade() {
        return confcidade;
    }

    /**
     * Setter method for confcidade.
     *
     * @param aConfcidade the new value for confcidade
     */
    public void setConfcidade(Confcidade aConfcidade) {
        confcidade = aConfcidade;
    }

    /**
     * Access method for aggrupo.
     *
     * @return the current value of aggrupo
     */
    public Aggrupo getAggrupo() {
        return aggrupo;
    }

    /**
     * Setter method for aggrupo.
     *
     * @param aAggrupo the new value for aggrupo
     */
    public void setAggrupo(Aggrupo aAggrupo) {
        aggrupo = aAggrupo;
    }

    /**
     * Access method for agtipo.
     *
     * @return the current value of agtipo
     */
    public Agtipo getAgtipo() {
        return agtipo;
    }

    /**
     * Setter method for agtipo.
     *
     * @param aAgtipo the new value for agtipo
     */
    public void setAgtipo(Agtipo aAgtipo) {
        agtipo = aAgtipo;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Set<Optipo> getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Set<Optipo> aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Compares the key for this instance with another Comtabela.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Comtabela and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Comtabela)) {
            return false;
        }
        Comtabela that = (Comtabela) other;
        if (this.getCodcomtabela() != that.getCodcomtabela()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Comtabela.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Comtabela)) return false;
        return this.equalKeys(other) && ((Comtabela)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcomtabela();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Comtabela |");
        sb.append(" codcomtabela=").append(getCodcomtabela());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcomtabela", Integer.valueOf(getCodcomtabela()));
        return ret;
    }

}
