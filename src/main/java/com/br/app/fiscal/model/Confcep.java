package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CONFCEP")
public class Confcep implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfcep";

    @Id
    @Column(name="CODCONFCEP", unique=true, nullable=false, length=20)
    private String codconfcep;
    @Column(name="LOGRADOURO", length=250)
    private String logradouro;
    @Column(name="BAIRRO", length=60)
    private String bairro;
    @Column(name="CEPINICIAL", precision=10)
    private int cepinicial;
    @Column(name="CEPFINAL", precision=10)
    private int cepfinal;
    @Column(name="NUMEROINICIAL", precision=10)
    private int numeroinicial;
    @Column(name="NUMEROFINAL", precision=10)
    private int numerofinal;
    @ManyToOne
    @JoinColumn(name="CODCONFCIDADE")
    private Confcidade confcidade;
    @OneToMany(mappedBy="confcep")
    private Set<Conflogradouro> conflogradouro;

    /** Default constructor. */
    public Confcep() {
        super();
    }

    /**
     * Access method for codconfcep.
     *
     * @return the current value of codconfcep
     */
    public String getCodconfcep() {
        return codconfcep;
    }

    /**
     * Setter method for codconfcep.
     *
     * @param aCodconfcep the new value for codconfcep
     */
    public void setCodconfcep(String aCodconfcep) {
        codconfcep = aCodconfcep;
    }

    /**
     * Access method for logradouro.
     *
     * @return the current value of logradouro
     */
    public String getLogradouro() {
        return logradouro;
    }

    /**
     * Setter method for logradouro.
     *
     * @param aLogradouro the new value for logradouro
     */
    public void setLogradouro(String aLogradouro) {
        logradouro = aLogradouro;
    }

    /**
     * Access method for bairro.
     *
     * @return the current value of bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Setter method for bairro.
     *
     * @param aBairro the new value for bairro
     */
    public void setBairro(String aBairro) {
        bairro = aBairro;
    }

    /**
     * Access method for cepinicial.
     *
     * @return the current value of cepinicial
     */
    public int getCepinicial() {
        return cepinicial;
    }

    /**
     * Setter method for cepinicial.
     *
     * @param aCepinicial the new value for cepinicial
     */
    public void setCepinicial(int aCepinicial) {
        cepinicial = aCepinicial;
    }

    /**
     * Access method for cepfinal.
     *
     * @return the current value of cepfinal
     */
    public int getCepfinal() {
        return cepfinal;
    }

    /**
     * Setter method for cepfinal.
     *
     * @param aCepfinal the new value for cepfinal
     */
    public void setCepfinal(int aCepfinal) {
        cepfinal = aCepfinal;
    }

    /**
     * Access method for numeroinicial.
     *
     * @return the current value of numeroinicial
     */
    public int getNumeroinicial() {
        return numeroinicial;
    }

    /**
     * Setter method for numeroinicial.
     *
     * @param aNumeroinicial the new value for numeroinicial
     */
    public void setNumeroinicial(int aNumeroinicial) {
        numeroinicial = aNumeroinicial;
    }

    /**
     * Access method for numerofinal.
     *
     * @return the current value of numerofinal
     */
    public int getNumerofinal() {
        return numerofinal;
    }

    /**
     * Setter method for numerofinal.
     *
     * @param aNumerofinal the new value for numerofinal
     */
    public void setNumerofinal(int aNumerofinal) {
        numerofinal = aNumerofinal;
    }

    /**
     * Access method for confcidade.
     *
     * @return the current value of confcidade
     */
    public Confcidade getConfcidade() {
        return confcidade;
    }

    /**
     * Setter method for confcidade.
     *
     * @param aConfcidade the new value for confcidade
     */
    public void setConfcidade(Confcidade aConfcidade) {
        confcidade = aConfcidade;
    }

    /**
     * Access method for conflogradouro.
     *
     * @return the current value of conflogradouro
     */
    public Set<Conflogradouro> getConflogradouro() {
        return conflogradouro;
    }

    /**
     * Setter method for conflogradouro.
     *
     * @param aConflogradouro the new value for conflogradouro
     */
    public void setConflogradouro(Set<Conflogradouro> aConflogradouro) {
        conflogradouro = aConflogradouro;
    }

    /**
     * Compares the key for this instance with another Confcep.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confcep and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confcep)) {
            return false;
        }
        Confcep that = (Confcep) other;
        Object myCodconfcep = this.getCodconfcep();
        Object yourCodconfcep = that.getCodconfcep();
        if (myCodconfcep==null ? yourCodconfcep!=null : !myCodconfcep.equals(yourCodconfcep)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confcep.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confcep)) return false;
        return this.equalKeys(other) && ((Confcep)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodconfcep() == null) {
            i = 0;
        } else {
            i = getCodconfcep().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confcep |");
        sb.append(" codconfcep=").append(getCodconfcep());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfcep", getCodconfcep());
        return ret;
    }

}
