package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CONFCIDADE")
public class Confcidade implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfcidade";

    @Id
    @Column(name="CODCONFCIDADE", unique=true, nullable=false, precision=10)
    private int codconfcidade;
    @Column(name="NOMECONFCIDADE", nullable=false, length=250)
    private String nomeconfcidade;
    @OneToMany(mappedBy="confcidade")
    private Set<Comtabela> comtabela;
    @OneToMany(mappedBy="confcidade")
    private Set<Confcep> confcep;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUF", nullable=false)
    private Confuf confuf;
    @OneToMany(mappedBy="confcidade")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="confcidade")
    private Set<Optransacao> optransacao;

    /** Default constructor. */
    public Confcidade() {
        super();
    }

    /**
     * Access method for codconfcidade.
     *
     * @return the current value of codconfcidade
     */
    public int getCodconfcidade() {
        return codconfcidade;
    }

    /**
     * Setter method for codconfcidade.
     *
     * @param aCodconfcidade the new value for codconfcidade
     */
    public void setCodconfcidade(int aCodconfcidade) {
        codconfcidade = aCodconfcidade;
    }

    /**
     * Access method for nomeconfcidade.
     *
     * @return the current value of nomeconfcidade
     */
    public String getNomeconfcidade() {
        return nomeconfcidade;
    }

    /**
     * Setter method for nomeconfcidade.
     *
     * @param aNomeconfcidade the new value for nomeconfcidade
     */
    public void setNomeconfcidade(String aNomeconfcidade) {
        nomeconfcidade = aNomeconfcidade;
    }

    /**
     * Access method for comtabela.
     *
     * @return the current value of comtabela
     */
    public Set<Comtabela> getComtabela() {
        return comtabela;
    }

    /**
     * Setter method for comtabela.
     *
     * @param aComtabela the new value for comtabela
     */
    public void setComtabela(Set<Comtabela> aComtabela) {
        comtabela = aComtabela;
    }

    /**
     * Access method for confcep.
     *
     * @return the current value of confcep
     */
    public Set<Confcep> getConfcep() {
        return confcep;
    }

    /**
     * Setter method for confcep.
     *
     * @param aConfcep the new value for confcep
     */
    public void setConfcep(Set<Confcep> aConfcep) {
        confcep = aConfcep;
    }

    /**
     * Access method for confuf.
     *
     * @return the current value of confuf
     */
    public Confuf getConfuf() {
        return confuf;
    }

    /**
     * Setter method for confuf.
     *
     * @param aConfuf the new value for confuf
     */
    public void setConfuf(Confuf aConfuf) {
        confuf = aConfuf;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Compares the key for this instance with another Confcidade.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confcidade and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confcidade)) {
            return false;
        }
        Confcidade that = (Confcidade) other;
        if (this.getCodconfcidade() != that.getCodconfcidade()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confcidade.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confcidade)) return false;
        return this.equalKeys(other) && ((Confcidade)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfcidade();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confcidade |");
        sb.append(" codconfcidade=").append(getCodconfcidade());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfcidade", Integer.valueOf(getCodconfcidade()));
        return ret;
    }

}
