package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="CONFCONTATO", indexes={@Index(name="confcontatoConfcontatoIdx1", columnList="NOMETABELA,CODTABELA,CONTTIPO")})
public class Confcontato implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfcontato";

    @Id
    @Column(name="CODCONFCONTATO", unique=true, nullable=false, precision=10)
    private int codconfcontato;
    @Column(name="NOMECONFCONTATO", nullable=false, length=250)
    private String nomeconfcontato;
    @Column(name="DESCRICAO")
    private String descricao;
    @Column(name="CONTEMAIL", length=250)
    private String contemail;
    @Column(name="CONTTELEFONE", length=20)
    private String conttelefone;
    @Column(name="CONTTIPO", nullable=false, precision=5)
    private short conttipo;
    @Column(name="NOMETABELA", length=60)
    private String nometabela;
    @Column(name="CODTABELA", precision=10)
    private int codtabela;
    @OneToMany(mappedBy="confcontato")
    private Set<Crmslaconfcontato> crmslaconfcontato;

    /** Default constructor. */
    public Confcontato() {
        super();
    }

    /**
     * Access method for codconfcontato.
     *
     * @return the current value of codconfcontato
     */
    public int getCodconfcontato() {
        return codconfcontato;
    }

    /**
     * Setter method for codconfcontato.
     *
     * @param aCodconfcontato the new value for codconfcontato
     */
    public void setCodconfcontato(int aCodconfcontato) {
        codconfcontato = aCodconfcontato;
    }

    /**
     * Access method for nomeconfcontato.
     *
     * @return the current value of nomeconfcontato
     */
    public String getNomeconfcontato() {
        return nomeconfcontato;
    }

    /**
     * Setter method for nomeconfcontato.
     *
     * @param aNomeconfcontato the new value for nomeconfcontato
     */
    public void setNomeconfcontato(String aNomeconfcontato) {
        nomeconfcontato = aNomeconfcontato;
    }

    /**
     * Access method for descricao.
     *
     * @return the current value of descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Setter method for descricao.
     *
     * @param aDescricao the new value for descricao
     */
    public void setDescricao(String aDescricao) {
        descricao = aDescricao;
    }

    /**
     * Access method for contemail.
     *
     * @return the current value of contemail
     */
    public String getContemail() {
        return contemail;
    }

    /**
     * Setter method for contemail.
     *
     * @param aContemail the new value for contemail
     */
    public void setContemail(String aContemail) {
        contemail = aContemail;
    }

    /**
     * Access method for conttelefone.
     *
     * @return the current value of conttelefone
     */
    public String getConttelefone() {
        return conttelefone;
    }

    /**
     * Setter method for conttelefone.
     *
     * @param aConttelefone the new value for conttelefone
     */
    public void setConttelefone(String aConttelefone) {
        conttelefone = aConttelefone;
    }

    /**
     * Access method for conttipo.
     *
     * @return the current value of conttipo
     */
    public short getConttipo() {
        return conttipo;
    }

    /**
     * Setter method for conttipo.
     *
     * @param aConttipo the new value for conttipo
     */
    public void setConttipo(short aConttipo) {
        conttipo = aConttipo;
    }

    /**
     * Access method for nometabela.
     *
     * @return the current value of nometabela
     */
    public String getNometabela() {
        return nometabela;
    }

    /**
     * Setter method for nometabela.
     *
     * @param aNometabela the new value for nometabela
     */
    public void setNometabela(String aNometabela) {
        nometabela = aNometabela;
    }

    /**
     * Access method for codtabela.
     *
     * @return the current value of codtabela
     */
    public int getCodtabela() {
        return codtabela;
    }

    /**
     * Setter method for codtabela.
     *
     * @param aCodtabela the new value for codtabela
     */
    public void setCodtabela(int aCodtabela) {
        codtabela = aCodtabela;
    }

    /**
     * Access method for crmslaconfcontato.
     *
     * @return the current value of crmslaconfcontato
     */
    public Set<Crmslaconfcontato> getCrmslaconfcontato() {
        return crmslaconfcontato;
    }

    /**
     * Setter method for crmslaconfcontato.
     *
     * @param aCrmslaconfcontato the new value for crmslaconfcontato
     */
    public void setCrmslaconfcontato(Set<Crmslaconfcontato> aCrmslaconfcontato) {
        crmslaconfcontato = aCrmslaconfcontato;
    }

    /**
     * Compares the key for this instance with another Confcontato.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confcontato and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confcontato)) {
            return false;
        }
        Confcontato that = (Confcontato) other;
        if (this.getCodconfcontato() != that.getCodconfcontato()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confcontato.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confcontato)) return false;
        return this.equalKeys(other) && ((Confcontato)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfcontato();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confcontato |");
        sb.append(" codconfcontato=").append(getCodconfcontato());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfcontato", Integer.valueOf(getCodconfcontato()));
        return ret;
    }

}
