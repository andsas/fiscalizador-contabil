package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CONFDICFIELD")
public class Confdicfield implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfdicfield";

    @Id
    @Column(name="CODCONFDICFIELD", unique=true, nullable=false, precision=10)
    private int codconfdicfield;
    @Column(name="NOMECONFDICFIELD", nullable=false, length=250)
    private String nomeconfdicfield;
    @Column(name="DISPLAY", nullable=false, length=250)
    private String display;
    @Column(name="CHAVE", precision=5)
    private short chave;
    @Column(name="REQUERIDO", precision=5)
    private short requerido;
    @Column(name="AUTOINC", precision=5)
    private short autoinc;
    @Column(name="SIZE", precision=10)
    private int size;
    @Column(name="RELATIONTAB", length=250)
    private String relationtab;
    @Column(name="RELATIONCLASS", length=250)
    private String relationclass;
    @Column(name="RELATIONLISTFIELD", length=250)
    private String relationlistfield;
    @Column(name="RELATIONTYPE", precision=5)
    private short relationtype;
    @Column(name="RELATIONPROPNAME", length=250)
    private String relationpropname;
    @Column(name="TIPO", length=40)
    private String tipo;
    @Column(name="SEARCHABLE", precision=5)
    private short searchable;
    @Column(name="LAZYSUBENT", precision=5)
    private short lazysubent;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFDICTAB", nullable=false)
    private Confdictab confdictab;

    /** Default constructor. */
    public Confdicfield() {
        super();
    }

    /**
     * Access method for codconfdicfield.
     *
     * @return the current value of codconfdicfield
     */
    public int getCodconfdicfield() {
        return codconfdicfield;
    }

    /**
     * Setter method for codconfdicfield.
     *
     * @param aCodconfdicfield the new value for codconfdicfield
     */
    public void setCodconfdicfield(int aCodconfdicfield) {
        codconfdicfield = aCodconfdicfield;
    }

    /**
     * Access method for nomeconfdicfield.
     *
     * @return the current value of nomeconfdicfield
     */
    public String getNomeconfdicfield() {
        return nomeconfdicfield;
    }

    /**
     * Setter method for nomeconfdicfield.
     *
     * @param aNomeconfdicfield the new value for nomeconfdicfield
     */
    public void setNomeconfdicfield(String aNomeconfdicfield) {
        nomeconfdicfield = aNomeconfdicfield;
    }

    /**
     * Access method for display.
     *
     * @return the current value of display
     */
    public String getDisplay() {
        return display;
    }

    /**
     * Setter method for display.
     *
     * @param aDisplay the new value for display
     */
    public void setDisplay(String aDisplay) {
        display = aDisplay;
    }

    /**
     * Access method for chave.
     *
     * @return the current value of chave
     */
    public short getChave() {
        return chave;
    }

    /**
     * Setter method for chave.
     *
     * @param aChave the new value for chave
     */
    public void setChave(short aChave) {
        chave = aChave;
    }

    /**
     * Access method for requerido.
     *
     * @return the current value of requerido
     */
    public short getRequerido() {
        return requerido;
    }

    /**
     * Setter method for requerido.
     *
     * @param aRequerido the new value for requerido
     */
    public void setRequerido(short aRequerido) {
        requerido = aRequerido;
    }

    /**
     * Access method for autoinc.
     *
     * @return the current value of autoinc
     */
    public short getAutoinc() {
        return autoinc;
    }

    /**
     * Setter method for autoinc.
     *
     * @param aAutoinc the new value for autoinc
     */
    public void setAutoinc(short aAutoinc) {
        autoinc = aAutoinc;
    }

    /**
     * Access method for size.
     *
     * @return the current value of size
     */
    public int getSize() {
        return size;
    }

    /**
     * Setter method for size.
     *
     * @param aSize the new value for size
     */
    public void setSize(int aSize) {
        size = aSize;
    }

    /**
     * Access method for relationtab.
     *
     * @return the current value of relationtab
     */
    public String getRelationtab() {
        return relationtab;
    }

    /**
     * Setter method for relationtab.
     *
     * @param aRelationtab the new value for relationtab
     */
    public void setRelationtab(String aRelationtab) {
        relationtab = aRelationtab;
    }

    /**
     * Access method for relationclass.
     *
     * @return the current value of relationclass
     */
    public String getRelationclass() {
        return relationclass;
    }

    /**
     * Setter method for relationclass.
     *
     * @param aRelationclass the new value for relationclass
     */
    public void setRelationclass(String aRelationclass) {
        relationclass = aRelationclass;
    }

    /**
     * Access method for relationlistfield.
     *
     * @return the current value of relationlistfield
     */
    public String getRelationlistfield() {
        return relationlistfield;
    }

    /**
     * Setter method for relationlistfield.
     *
     * @param aRelationlistfield the new value for relationlistfield
     */
    public void setRelationlistfield(String aRelationlistfield) {
        relationlistfield = aRelationlistfield;
    }

    /**
     * Access method for relationtype.
     *
     * @return the current value of relationtype
     */
    public short getRelationtype() {
        return relationtype;
    }

    /**
     * Setter method for relationtype.
     *
     * @param aRelationtype the new value for relationtype
     */
    public void setRelationtype(short aRelationtype) {
        relationtype = aRelationtype;
    }

    /**
     * Access method for relationpropname.
     *
     * @return the current value of relationpropname
     */
    public String getRelationpropname() {
        return relationpropname;
    }

    /**
     * Setter method for relationpropname.
     *
     * @param aRelationpropname the new value for relationpropname
     */
    public void setRelationpropname(String aRelationpropname) {
        relationpropname = aRelationpropname;
    }

    /**
     * Access method for tipo.
     *
     * @return the current value of tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * Setter method for tipo.
     *
     * @param aTipo the new value for tipo
     */
    public void setTipo(String aTipo) {
        tipo = aTipo;
    }

    /**
     * Access method for searchable.
     *
     * @return the current value of searchable
     */
    public short getSearchable() {
        return searchable;
    }

    /**
     * Setter method for searchable.
     *
     * @param aSearchable the new value for searchable
     */
    public void setSearchable(short aSearchable) {
        searchable = aSearchable;
    }

    /**
     * Access method for lazysubent.
     *
     * @return the current value of lazysubent
     */
    public short getLazysubent() {
        return lazysubent;
    }

    /**
     * Setter method for lazysubent.
     *
     * @param aLazysubent the new value for lazysubent
     */
    public void setLazysubent(short aLazysubent) {
        lazysubent = aLazysubent;
    }

    /**
     * Access method for confdictab.
     *
     * @return the current value of confdictab
     */
    public Confdictab getConfdictab() {
        return confdictab;
    }

    /**
     * Setter method for confdictab.
     *
     * @param aConfdictab the new value for confdictab
     */
    public void setConfdictab(Confdictab aConfdictab) {
        confdictab = aConfdictab;
    }

    /**
     * Compares the key for this instance with another Confdicfield.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confdicfield and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confdicfield)) {
            return false;
        }
        Confdicfield that = (Confdicfield) other;
        if (this.getCodconfdicfield() != that.getCodconfdicfield()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confdicfield.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confdicfield)) return false;
        return this.equalKeys(other) && ((Confdicfield)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfdicfield();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confdicfield |");
        sb.append(" codconfdicfield=").append(getCodconfdicfield());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfdicfield", Integer.valueOf(getCodconfdicfield()));
        return ret;
    }

}
