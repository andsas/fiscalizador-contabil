package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CONFDICTAB")
public class Confdictab implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfdictab";

    @Id
    @Column(name="CODCONFDICTAB", unique=true, nullable=false, precision=10)
    private int codconfdictab;
    @Column(name="NOMECONFDICTAB", nullable=false, length=250)
    private String nomeconfdictab;
    @Column(name="CLASSE", length=250)
    private String classe;
    @Column(name="ENT", length=250)
    private String ent;
    @Column(name="RULES", precision=5)
    private short rules;
    @Column(name="DESCRICAO")
    private String descricao;
    @Column(name="DESCAMIGAVEL", length=60)
    private String descamigavel;
    @Column(name="DESCRICAOMENU", length=250)
    private String descricaomenu;
    @Column(name="MOSTRARMENU", precision=5)
    private short mostrarmenu;
    @Column(name="DESCRICAOPAIMENU", length=250)
    private String descricaopaimenu;
    @OneToMany(mappedBy="confdictab")
    private Set<Confdicfield> confdicfield;
    @OneToMany(mappedBy="confdictab")
    private Set<Conftask> conftask;
    @OneToMany(mappedBy="confdictab")
    private Set<Wfent> wfent;
    @OneToMany(mappedBy="confdictab")
    private Set<Wffilareg> wffilareg;

    /** Default constructor. */
    public Confdictab() {
        super();
    }

    /**
     * Access method for codconfdictab.
     *
     * @return the current value of codconfdictab
     */
    public int getCodconfdictab() {
        return codconfdictab;
    }

    /**
     * Setter method for codconfdictab.
     *
     * @param aCodconfdictab the new value for codconfdictab
     */
    public void setCodconfdictab(int aCodconfdictab) {
        codconfdictab = aCodconfdictab;
    }

    /**
     * Access method for nomeconfdictab.
     *
     * @return the current value of nomeconfdictab
     */
    public String getNomeconfdictab() {
        return nomeconfdictab;
    }

    /**
     * Setter method for nomeconfdictab.
     *
     * @param aNomeconfdictab the new value for nomeconfdictab
     */
    public void setNomeconfdictab(String aNomeconfdictab) {
        nomeconfdictab = aNomeconfdictab;
    }

    /**
     * Access method for classe.
     *
     * @return the current value of classe
     */
    public String getClasse() {
        return classe;
    }

    /**
     * Setter method for classe.
     *
     * @param aClasse the new value for classe
     */
    public void setClasse(String aClasse) {
        classe = aClasse;
    }

    /**
     * Access method for ent.
     *
     * @return the current value of ent
     */
    public String getEnt() {
        return ent;
    }

    /**
     * Setter method for ent.
     *
     * @param aEnt the new value for ent
     */
    public void setEnt(String aEnt) {
        ent = aEnt;
    }

    /**
     * Access method for rules.
     *
     * @return the current value of rules
     */
    public short getRules() {
        return rules;
    }

    /**
     * Setter method for rules.
     *
     * @param aRules the new value for rules
     */
    public void setRules(short aRules) {
        rules = aRules;
    }

    /**
     * Access method for descricao.
     *
     * @return the current value of descricao
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Setter method for descricao.
     *
     * @param aDescricao the new value for descricao
     */
    public void setDescricao(String aDescricao) {
        descricao = aDescricao;
    }

    /**
     * Access method for descamigavel.
     *
     * @return the current value of descamigavel
     */
    public String getDescamigavel() {
        return descamigavel;
    }

    /**
     * Setter method for descamigavel.
     *
     * @param aDescamigavel the new value for descamigavel
     */
    public void setDescamigavel(String aDescamigavel) {
        descamigavel = aDescamigavel;
    }

    /**
     * Access method for descricaomenu.
     *
     * @return the current value of descricaomenu
     */
    public String getDescricaomenu() {
        return descricaomenu;
    }

    /**
     * Setter method for descricaomenu.
     *
     * @param aDescricaomenu the new value for descricaomenu
     */
    public void setDescricaomenu(String aDescricaomenu) {
        descricaomenu = aDescricaomenu;
    }

    /**
     * Access method for mostrarmenu.
     *
     * @return the current value of mostrarmenu
     */
    public short getMostrarmenu() {
        return mostrarmenu;
    }

    /**
     * Setter method for mostrarmenu.
     *
     * @param aMostrarmenu the new value for mostrarmenu
     */
    public void setMostrarmenu(short aMostrarmenu) {
        mostrarmenu = aMostrarmenu;
    }

    /**
     * Access method for descricaopaimenu.
     *
     * @return the current value of descricaopaimenu
     */
    public String getDescricaopaimenu() {
        return descricaopaimenu;
    }

    /**
     * Setter method for descricaopaimenu.
     *
     * @param aDescricaopaimenu the new value for descricaopaimenu
     */
    public void setDescricaopaimenu(String aDescricaopaimenu) {
        descricaopaimenu = aDescricaopaimenu;
    }

    /**
     * Access method for confdicfield.
     *
     * @return the current value of confdicfield
     */
    public Set<Confdicfield> getConfdicfield() {
        return confdicfield;
    }

    /**
     * Setter method for confdicfield.
     *
     * @param aConfdicfield the new value for confdicfield
     */
    public void setConfdicfield(Set<Confdicfield> aConfdicfield) {
        confdicfield = aConfdicfield;
    }

    /**
     * Access method for conftask.
     *
     * @return the current value of conftask
     */
    public Set<Conftask> getConftask() {
        return conftask;
    }

    /**
     * Setter method for conftask.
     *
     * @param aConftask the new value for conftask
     */
    public void setConftask(Set<Conftask> aConftask) {
        conftask = aConftask;
    }

    /**
     * Access method for wfent.
     *
     * @return the current value of wfent
     */
    public Set<Wfent> getWfent() {
        return wfent;
    }

    /**
     * Setter method for wfent.
     *
     * @param aWfent the new value for wfent
     */
    public void setWfent(Set<Wfent> aWfent) {
        wfent = aWfent;
    }

    /**
     * Access method for wffilareg.
     *
     * @return the current value of wffilareg
     */
    public Set<Wffilareg> getWffilareg() {
        return wffilareg;
    }

    /**
     * Setter method for wffilareg.
     *
     * @param aWffilareg the new value for wffilareg
     */
    public void setWffilareg(Set<Wffilareg> aWffilareg) {
        wffilareg = aWffilareg;
    }

    /**
     * Compares the key for this instance with another Confdictab.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confdictab and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confdictab)) {
            return false;
        }
        Confdictab that = (Confdictab) other;
        if (this.getCodconfdictab() != that.getCodconfdictab()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confdictab.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confdictab)) return false;
        return this.equalKeys(other) && ((Confdictab)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfdictab();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confdictab |");
        sb.append(" codconfdictab=").append(getCodconfdictab());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfdictab", Integer.valueOf(getCodconfdictab()));
        return ret;
    }

}
