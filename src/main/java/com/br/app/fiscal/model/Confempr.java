package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CONFEMPR")
public class Confempr implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfempr";

    @Id
    @Column(name="CODCONFEMPR", unique=true, nullable=false, precision=10)
    private Integer codconfempr;
    @Column(name="NOMECONFEMPR", nullable=false, length=250)
    private String nomeconfempr;
    @Column(name="NOMEFANTASIA", nullable=false, length=250)
    private String nomefantasia;
    @Column(name="INSCEST", nullable=false, length=20)
    private String inscest;
    @Column(name="CNPJ", nullable=false, length=20)
    private String cnpj;
    @Column(name="NFECERTIFICADO", length=250)
    private String nfecertificado;
    @Column(name="NFEENVIOPATH", length=250)
    private String nfeenviopath;
    @Column(name="NFECANCPATH", length=250)
    private String nfecancpath;
    @Column(name="NFEINUPATH", length=250)
    private String nfeinupath;
    @Column(name="NFECONSPATH", length=250)
    private String nfeconspath;
    @Column(name="LOGO")
    private String logo;
    @Column(name="NFEAMBIENTE", precision=5)
    private short nfeambiente;
    @Column(name="TIPOCONFPAIEMPR", precision=5)
    private short tipoconfpaiempr;
    @Column(name="EMAILHOST", length=250)
    private String emailhost;
    @Column(name="EMAILNOME", length=250)
    private String emailnome;
    @Column(name="EMAILLOGIN", length=250)
    private String emaillogin;
    @Column(name="EMAILSENHA", length=250)
    private String emailsenha;
    @Column(name="EMAILSSL", precision=5)
    private short emailssl;
    @Column(name="EMAILSEG", precision=5)
    private short emailseg;
    @Column(name="EMAILCOPIA", precision=5)
    private short emailcopia;
    @Column(name="EMAILPORTA", precision=10)
    private int emailporta;
    @Column(name="NFSECERTIFICADO", length=250)
    private String nfsecertificado;
    @Column(name="NFSEENVIOPATH", length=250)
    private String nfseenviopath;
    @Column(name="NFSEAMBIENTE", precision=5)
    private short nfseambiente;
    @Column(name="CORMENU", precision=10)
    private int cormenu;
    @OneToMany(mappedBy="confempr")
    private Set<Agconf> agconf;
    @OneToMany(mappedBy="confempr")
    private Set<Comtabela> comtabela;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @OneToMany(mappedBy="confempr2")
    private Set<Confempr> confempr3;
    @ManyToOne
    @JoinColumn(name="CODCONFPAIEMPR")
    private Confempr confempr2;
    @OneToMany(mappedBy="confempr")
    private Set<Confemprpart> confemprpart;
    @OneToMany(mappedBy="confempr")
    private Set<Confplanoconta> confplanoconta;
    @OneToMany(mappedBy="confempr")
    private Set<Ctbbem> ctbbem;
    @OneToMany(mappedBy="confempr")
    private Set<Ctbfechamento> ctbfechamento;
    @OneToMany(mappedBy="confempr")
    private Set<Ctblancamento> ctblancamento;
    @OneToMany(mappedBy="confempr")
    private Set<Ctbplanoconta> ctbplanoconta;
    @OneToMany(mappedBy="confempr")
    private Set<Ecconf> ecconf;
    @OneToMany(mappedBy="confempr")
    private Set<Ecdoclancamento> ecdoclancamento;
    @OneToMany(mappedBy="confempr")
    private Set<Fincaixa> fincaixa;
    @OneToMany(mappedBy="confempr")
    private Set<Finconf> finconf;
    @OneToMany(mappedBy="confempr")
    private Set<Finfluxoplanoconta> finfluxoplanoconta;
    @OneToMany(mappedBy="confempr")
    private Set<Finplanoconta> finplanoconta;
    @OneToMany(mappedBy="confempr")
    private Set<Folcolaborador> folcolaborador;
    @OneToMany(mappedBy="confempr")
    private Set<Folhorariotrabalho> folhorariotrabalho;
    @OneToMany(mappedBy="confempr")
    private Set<Impoptributaria> impoptributaria;
    @OneToMany(mappedBy="confempr")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="confempr")
    private Set<Opconfentrada> opconfentrada;
    @OneToMany(mappedBy="confempr")
    private Set<Opconfsaida> opconfsaida;
    @OneToMany(mappedBy="confempr")
    private Set<Oporc> oporc;
    @OneToMany(mappedBy="confempr")
    private Set<Opsolicitacao> opsolicitacao;
    @OneToMany(mappedBy="confempr")
    private Set<Orcplanoconta> orcplanoconta;

    /** Default constructor. */
    public Confempr() {
        super();
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public Integer getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(Integer aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for nomeconfempr.
     *
     * @return the current value of nomeconfempr
     */
    public String getNomeconfempr() {
        return nomeconfempr;
    }

    /**
     * Setter method for nomeconfempr.
     *
     * @param aNomeconfempr the new value for nomeconfempr
     */
    public void setNomeconfempr(String aNomeconfempr) {
        nomeconfempr = aNomeconfempr;
    }

    /**
     * Access method for nomefantasia.
     *
     * @return the current value of nomefantasia
     */
    public String getNomefantasia() {
        return nomefantasia;
    }

    /**
     * Setter method for nomefantasia.
     *
     * @param aNomefantasia the new value for nomefantasia
     */
    public void setNomefantasia(String aNomefantasia) {
        nomefantasia = aNomefantasia;
    }

    /**
     * Access method for inscest.
     *
     * @return the current value of inscest
     */
    public String getInscest() {
        return inscest;
    }

    /**
     * Setter method for inscest.
     *
     * @param aInscest the new value for inscest
     */
    public void setInscest(String aInscest) {
        inscest = aInscest;
    }

    /**
     * Access method for cnpj.
     *
     * @return the current value of cnpj
     */
    public String getCnpj() {
        return cnpj;
    }

    /**
     * Setter method for cnpj.
     *
     * @param aCnpj the new value for cnpj
     */
    public void setCnpj(String aCnpj) {
        cnpj = aCnpj;
    }

    /**
     * Access method for nfecertificado.
     *
     * @return the current value of nfecertificado
     */
    public String getNfecertificado() {
        return nfecertificado;
    }

    /**
     * Setter method for nfecertificado.
     *
     * @param aNfecertificado the new value for nfecertificado
     */
    public void setNfecertificado(String aNfecertificado) {
        nfecertificado = aNfecertificado;
    }

    /**
     * Access method for nfeenviopath.
     *
     * @return the current value of nfeenviopath
     */
    public String getNfeenviopath() {
        return nfeenviopath;
    }

    /**
     * Setter method for nfeenviopath.
     *
     * @param aNfeenviopath the new value for nfeenviopath
     */
    public void setNfeenviopath(String aNfeenviopath) {
        nfeenviopath = aNfeenviopath;
    }

    /**
     * Access method for nfecancpath.
     *
     * @return the current value of nfecancpath
     */
    public String getNfecancpath() {
        return nfecancpath;
    }

    /**
     * Setter method for nfecancpath.
     *
     * @param aNfecancpath the new value for nfecancpath
     */
    public void setNfecancpath(String aNfecancpath) {
        nfecancpath = aNfecancpath;
    }

    /**
     * Access method for nfeinupath.
     *
     * @return the current value of nfeinupath
     */
    public String getNfeinupath() {
        return nfeinupath;
    }

    /**
     * Setter method for nfeinupath.
     *
     * @param aNfeinupath the new value for nfeinupath
     */
    public void setNfeinupath(String aNfeinupath) {
        nfeinupath = aNfeinupath;
    }

    /**
     * Access method for nfeconspath.
     *
     * @return the current value of nfeconspath
     */
    public String getNfeconspath() {
        return nfeconspath;
    }

    /**
     * Setter method for nfeconspath.
     *
     * @param aNfeconspath the new value for nfeconspath
     */
    public void setNfeconspath(String aNfeconspath) {
        nfeconspath = aNfeconspath;
    }

    /**
     * Access method for logo.
     *
     * @return the current value of logo
     */
    public String getLogo() {
        return logo;
    }

    /**
     * Setter method for logo.
     *
     * @param aLogo the new value for logo
     */
    public void setLogo(String aLogo) {
        logo = aLogo;
    }

    /**
     * Access method for nfeambiente.
     *
     * @return the current value of nfeambiente
     */
    public short getNfeambiente() {
        return nfeambiente;
    }

    /**
     * Setter method for nfeambiente.
     *
     * @param aNfeambiente the new value for nfeambiente
     */
    public void setNfeambiente(short aNfeambiente) {
        nfeambiente = aNfeambiente;
    }

    /**
     * Access method for tipoconfpaiempr.
     *
     * @return the current value of tipoconfpaiempr
     */
    public short getTipoconfpaiempr() {
        return tipoconfpaiempr;
    }

    /**
     * Setter method for tipoconfpaiempr.
     *
     * @param aTipoconfpaiempr the new value for tipoconfpaiempr
     */
    public void setTipoconfpaiempr(short aTipoconfpaiempr) {
        tipoconfpaiempr = aTipoconfpaiempr;
    }

    /**
     * Access method for emailhost.
     *
     * @return the current value of emailhost
     */
    public String getEmailhost() {
        return emailhost;
    }

    /**
     * Setter method for emailhost.
     *
     * @param aEmailhost the new value for emailhost
     */
    public void setEmailhost(String aEmailhost) {
        emailhost = aEmailhost;
    }

    /**
     * Access method for emailnome.
     *
     * @return the current value of emailnome
     */
    public String getEmailnome() {
        return emailnome;
    }

    /**
     * Setter method for emailnome.
     *
     * @param aEmailnome the new value for emailnome
     */
    public void setEmailnome(String aEmailnome) {
        emailnome = aEmailnome;
    }

    /**
     * Access method for emaillogin.
     *
     * @return the current value of emaillogin
     */
    public String getEmaillogin() {
        return emaillogin;
    }

    /**
     * Setter method for emaillogin.
     *
     * @param aEmaillogin the new value for emaillogin
     */
    public void setEmaillogin(String aEmaillogin) {
        emaillogin = aEmaillogin;
    }

    /**
     * Access method for emailsenha.
     *
     * @return the current value of emailsenha
     */
    public String getEmailsenha() {
        return emailsenha;
    }

    /**
     * Setter method for emailsenha.
     *
     * @param aEmailsenha the new value for emailsenha
     */
    public void setEmailsenha(String aEmailsenha) {
        emailsenha = aEmailsenha;
    }

    /**
     * Access method for emailssl.
     *
     * @return the current value of emailssl
     */
    public short getEmailssl() {
        return emailssl;
    }

    /**
     * Setter method for emailssl.
     *
     * @param aEmailssl the new value for emailssl
     */
    public void setEmailssl(short aEmailssl) {
        emailssl = aEmailssl;
    }

    /**
     * Access method for emailseg.
     *
     * @return the current value of emailseg
     */
    public short getEmailseg() {
        return emailseg;
    }

    /**
     * Setter method for emailseg.
     *
     * @param aEmailseg the new value for emailseg
     */
    public void setEmailseg(short aEmailseg) {
        emailseg = aEmailseg;
    }

    /**
     * Access method for emailcopia.
     *
     * @return the current value of emailcopia
     */
    public short getEmailcopia() {
        return emailcopia;
    }

    /**
     * Setter method for emailcopia.
     *
     * @param aEmailcopia the new value for emailcopia
     */
    public void setEmailcopia(short aEmailcopia) {
        emailcopia = aEmailcopia;
    }

    /**
     * Access method for emailporta.
     *
     * @return the current value of emailporta
     */
    public int getEmailporta() {
        return emailporta;
    }

    /**
     * Setter method for emailporta.
     *
     * @param aEmailporta the new value for emailporta
     */
    public void setEmailporta(int aEmailporta) {
        emailporta = aEmailporta;
    }

    /**
     * Access method for nfsecertificado.
     *
     * @return the current value of nfsecertificado
     */
    public String getNfsecertificado() {
        return nfsecertificado;
    }

    /**
     * Setter method for nfsecertificado.
     *
     * @param aNfsecertificado the new value for nfsecertificado
     */
    public void setNfsecertificado(String aNfsecertificado) {
        nfsecertificado = aNfsecertificado;
    }

    /**
     * Access method for nfseenviopath.
     *
     * @return the current value of nfseenviopath
     */
    public String getNfseenviopath() {
        return nfseenviopath;
    }

    /**
     * Setter method for nfseenviopath.
     *
     * @param aNfseenviopath the new value for nfseenviopath
     */
    public void setNfseenviopath(String aNfseenviopath) {
        nfseenviopath = aNfseenviopath;
    }

    /**
     * Access method for nfseambiente.
     *
     * @return the current value of nfseambiente
     */
    public short getNfseambiente() {
        return nfseambiente;
    }

    /**
     * Setter method for nfseambiente.
     *
     * @param aNfseambiente the new value for nfseambiente
     */
    public void setNfseambiente(short aNfseambiente) {
        nfseambiente = aNfseambiente;
    }

    /**
     * Access method for cormenu.
     *
     * @return the current value of cormenu
     */
    public int getCormenu() {
        return cormenu;
    }

    /**
     * Setter method for cormenu.
     *
     * @param aCormenu the new value for cormenu
     */
    public void setCormenu(int aCormenu) {
        cormenu = aCormenu;
    }

    /**
     * Access method for agconf.
     *
     * @return the current value of agconf
     */
    public Set<Agconf> getAgconf() {
        return agconf;
    }

    /**
     * Setter method for agconf.
     *
     * @param aAgconf the new value for agconf
     */
    public void setAgconf(Set<Agconf> aAgconf) {
        agconf = aAgconf;
    }

    /**
     * Access method for comtabela.
     *
     * @return the current value of comtabela
     */
    public Set<Comtabela> getComtabela() {
        return comtabela;
    }

    /**
     * Setter method for comtabela.
     *
     * @param aComtabela the new value for comtabela
     */
    public void setComtabela(Set<Comtabela> aComtabela) {
        comtabela = aComtabela;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for confempr3.
     *
     * @return the current value of confempr3
     */
    public Set<Confempr> getConfempr3() {
        return confempr3;
    }

    /**
     * Setter method for confempr3.
     *
     * @param aConfempr3 the new value for confempr3
     */
    public void setConfempr3(Set<Confempr> aConfempr3) {
        confempr3 = aConfempr3;
    }

    /**
     * Access method for confempr2.
     *
     * @return the current value of confempr2
     */
    public Confempr getConfempr2() {
        return confempr2;
    }

    /**
     * Setter method for confempr2.
     *
     * @param aConfempr2 the new value for confempr2
     */
    public void setConfempr2(Confempr aConfempr2) {
        confempr2 = aConfempr2;
    }

    /**
     * Access method for confemprpart.
     *
     * @return the current value of confemprpart
     */
    public Set<Confemprpart> getConfemprpart() {
        return confemprpart;
    }

    /**
     * Setter method for confemprpart.
     *
     * @param aConfemprpart the new value for confemprpart
     */
    public void setConfemprpart(Set<Confemprpart> aConfemprpart) {
        confemprpart = aConfemprpart;
    }

    /**
     * Access method for confplanoconta.
     *
     * @return the current value of confplanoconta
     */
    public Set<Confplanoconta> getConfplanoconta() {
        return confplanoconta;
    }

    /**
     * Setter method for confplanoconta.
     *
     * @param aConfplanoconta the new value for confplanoconta
     */
    public void setConfplanoconta(Set<Confplanoconta> aConfplanoconta) {
        confplanoconta = aConfplanoconta;
    }

    /**
     * Access method for ctbbem.
     *
     * @return the current value of ctbbem
     */
    public Set<Ctbbem> getCtbbem() {
        return ctbbem;
    }

    /**
     * Setter method for ctbbem.
     *
     * @param aCtbbem the new value for ctbbem
     */
    public void setCtbbem(Set<Ctbbem> aCtbbem) {
        ctbbem = aCtbbem;
    }

    /**
     * Access method for ctbfechamento.
     *
     * @return the current value of ctbfechamento
     */
    public Set<Ctbfechamento> getCtbfechamento() {
        return ctbfechamento;
    }

    /**
     * Setter method for ctbfechamento.
     *
     * @param aCtbfechamento the new value for ctbfechamento
     */
    public void setCtbfechamento(Set<Ctbfechamento> aCtbfechamento) {
        ctbfechamento = aCtbfechamento;
    }

    /**
     * Access method for ctblancamento.
     *
     * @return the current value of ctblancamento
     */
    public Set<Ctblancamento> getCtblancamento() {
        return ctblancamento;
    }

    /**
     * Setter method for ctblancamento.
     *
     * @param aCtblancamento the new value for ctblancamento
     */
    public void setCtblancamento(Set<Ctblancamento> aCtblancamento) {
        ctblancamento = aCtblancamento;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Set<Ctbplanoconta> getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Set<Ctbplanoconta> aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ecconf.
     *
     * @return the current value of ecconf
     */
    public Set<Ecconf> getEcconf() {
        return ecconf;
    }

    /**
     * Setter method for ecconf.
     *
     * @param aEcconf the new value for ecconf
     */
    public void setEcconf(Set<Ecconf> aEcconf) {
        ecconf = aEcconf;
    }

    /**
     * Access method for ecdoclancamento.
     *
     * @return the current value of ecdoclancamento
     */
    public Set<Ecdoclancamento> getEcdoclancamento() {
        return ecdoclancamento;
    }

    /**
     * Setter method for ecdoclancamento.
     *
     * @param aEcdoclancamento the new value for ecdoclancamento
     */
    public void setEcdoclancamento(Set<Ecdoclancamento> aEcdoclancamento) {
        ecdoclancamento = aEcdoclancamento;
    }

    /**
     * Access method for fincaixa.
     *
     * @return the current value of fincaixa
     */
    public Set<Fincaixa> getFincaixa() {
        return fincaixa;
    }

    /**
     * Setter method for fincaixa.
     *
     * @param aFincaixa the new value for fincaixa
     */
    public void setFincaixa(Set<Fincaixa> aFincaixa) {
        fincaixa = aFincaixa;
    }

    /**
     * Access method for finconf.
     *
     * @return the current value of finconf
     */
    public Set<Finconf> getFinconf() {
        return finconf;
    }

    /**
     * Setter method for finconf.
     *
     * @param aFinconf the new value for finconf
     */
    public void setFinconf(Set<Finconf> aFinconf) {
        finconf = aFinconf;
    }

    /**
     * Access method for finfluxoplanoconta.
     *
     * @return the current value of finfluxoplanoconta
     */
    public Set<Finfluxoplanoconta> getFinfluxoplanoconta() {
        return finfluxoplanoconta;
    }

    /**
     * Setter method for finfluxoplanoconta.
     *
     * @param aFinfluxoplanoconta the new value for finfluxoplanoconta
     */
    public void setFinfluxoplanoconta(Set<Finfluxoplanoconta> aFinfluxoplanoconta) {
        finfluxoplanoconta = aFinfluxoplanoconta;
    }

    /**
     * Access method for finplanoconta.
     *
     * @return the current value of finplanoconta
     */
    public Set<Finplanoconta> getFinplanoconta() {
        return finplanoconta;
    }

    /**
     * Setter method for finplanoconta.
     *
     * @param aFinplanoconta the new value for finplanoconta
     */
    public void setFinplanoconta(Set<Finplanoconta> aFinplanoconta) {
        finplanoconta = aFinplanoconta;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Set<Folcolaborador> getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Set<Folcolaborador> aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Access method for folhorariotrabalho.
     *
     * @return the current value of folhorariotrabalho
     */
    public Set<Folhorariotrabalho> getFolhorariotrabalho() {
        return folhorariotrabalho;
    }

    /**
     * Setter method for folhorariotrabalho.
     *
     * @param aFolhorariotrabalho the new value for folhorariotrabalho
     */
    public void setFolhorariotrabalho(Set<Folhorariotrabalho> aFolhorariotrabalho) {
        folhorariotrabalho = aFolhorariotrabalho;
    }

    /**
     * Access method for impoptributaria.
     *
     * @return the current value of impoptributaria
     */
    public Set<Impoptributaria> getImpoptributaria() {
        return impoptributaria;
    }

    /**
     * Setter method for impoptributaria.
     *
     * @param aImpoptributaria the new value for impoptributaria
     */
    public void setImpoptributaria(Set<Impoptributaria> aImpoptributaria) {
        impoptributaria = aImpoptributaria;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for opconfentrada.
     *
     * @return the current value of opconfentrada
     */
    public Set<Opconfentrada> getOpconfentrada() {
        return opconfentrada;
    }

    /**
     * Setter method for opconfentrada.
     *
     * @param aOpconfentrada the new value for opconfentrada
     */
    public void setOpconfentrada(Set<Opconfentrada> aOpconfentrada) {
        opconfentrada = aOpconfentrada;
    }

    /**
     * Access method for opconfsaida.
     *
     * @return the current value of opconfsaida
     */
    public Set<Opconfsaida> getOpconfsaida() {
        return opconfsaida;
    }

    /**
     * Setter method for opconfsaida.
     *
     * @param aOpconfsaida the new value for opconfsaida
     */
    public void setOpconfsaida(Set<Opconfsaida> aOpconfsaida) {
        opconfsaida = aOpconfsaida;
    }

    /**
     * Access method for oporc.
     *
     * @return the current value of oporc
     */
    public Set<Oporc> getOporc() {
        return oporc;
    }

    /**
     * Setter method for oporc.
     *
     * @param aOporc the new value for oporc
     */
    public void setOporc(Set<Oporc> aOporc) {
        oporc = aOporc;
    }

    /**
     * Access method for opsolicitacao.
     *
     * @return the current value of opsolicitacao
     */
    public Set<Opsolicitacao> getOpsolicitacao() {
        return opsolicitacao;
    }

    /**
     * Setter method for opsolicitacao.
     *
     * @param aOpsolicitacao the new value for opsolicitacao
     */
    public void setOpsolicitacao(Set<Opsolicitacao> aOpsolicitacao) {
        opsolicitacao = aOpsolicitacao;
    }

    /**
     * Access method for orcplanoconta.
     *
     * @return the current value of orcplanoconta
     */
    public Set<Orcplanoconta> getOrcplanoconta() {
        return orcplanoconta;
    }

    /**
     * Setter method for orcplanoconta.
     *
     * @param aOrcplanoconta the new value for orcplanoconta
     */
    public void setOrcplanoconta(Set<Orcplanoconta> aOrcplanoconta) {
        orcplanoconta = aOrcplanoconta;
    }

    /**
     * Compares the key for this instance with another Confempr.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confempr and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confempr)) {
            return false;
        }
        Confempr that = (Confempr) other;
        if (this.getCodconfempr() != that.getCodconfempr()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confempr.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confempr)) return false;
        return this.equalKeys(other) && ((Confempr)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfempr();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confempr |");
        sb.append(" codconfempr=").append(getCodconfempr());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfempr", Integer.valueOf(getCodconfempr()));
        return ret;
    }

}
