package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CONFEMPRPART")
public class Confemprpart implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfemprpart";

    @Id
    @Column(name="CODCONFEMPRPART", unique=true, nullable=false, precision=10)
    private int codconfemprpart;
    @Column(name="TIPOCONFEMPRPART", nullable=false, precision=5)
    private short tipoconfemprpart;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;

    /** Default constructor. */
    public Confemprpart() {
        super();
    }

    /**
     * Access method for codconfemprpart.
     *
     * @return the current value of codconfemprpart
     */
    public int getCodconfemprpart() {
        return codconfemprpart;
    }

    /**
     * Setter method for codconfemprpart.
     *
     * @param aCodconfemprpart the new value for codconfemprpart
     */
    public void setCodconfemprpart(int aCodconfemprpart) {
        codconfemprpart = aCodconfemprpart;
    }

    /**
     * Access method for tipoconfemprpart.
     *
     * @return the current value of tipoconfemprpart
     */
    public short getTipoconfemprpart() {
        return tipoconfemprpart;
    }

    /**
     * Setter method for tipoconfemprpart.
     *
     * @param aTipoconfemprpart the new value for tipoconfemprpart
     */
    public void setTipoconfemprpart(short aTipoconfemprpart) {
        tipoconfemprpart = aTipoconfemprpart;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Compares the key for this instance with another Confemprpart.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confemprpart and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confemprpart)) {
            return false;
        }
        Confemprpart that = (Confemprpart) other;
        if (this.getCodconfemprpart() != that.getCodconfemprpart()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confemprpart.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confemprpart)) return false;
        return this.equalKeys(other) && ((Confemprpart)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfemprpart();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confemprpart |");
        sb.append(" codconfemprpart=").append(getCodconfemprpart());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfemprpart", Integer.valueOf(getCodconfemprpart()));
        return ret;
    }

}
