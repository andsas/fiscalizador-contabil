package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="CONFINC")
public class Confinc implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "tabelaconfinc";

    @Id
    @Column(name="TABELACONFINC", unique=true, nullable=false, length=60)
    private String tabelaconfinc;
    @Column(name="INC", precision=10)
    private int inc;

    /** Default constructor. */
    public Confinc() {
        super();
    }

    /**
     * Access method for tabelaconfinc.
     *
     * @return the current value of tabelaconfinc
     */
    public String getTabelaconfinc() {
        return tabelaconfinc;
    }

    /**
     * Setter method for tabelaconfinc.
     *
     * @param aTabelaconfinc the new value for tabelaconfinc
     */
    public void setTabelaconfinc(String aTabelaconfinc) {
        tabelaconfinc = aTabelaconfinc;
    }

    /**
     * Access method for inc.
     *
     * @return the current value of inc
     */
    public int getInc() {
        return inc;
    }

    /**
     * Setter method for inc.
     *
     * @param aInc the new value for inc
     */
    public void setInc(int aInc) {
        inc = aInc;
    }

    /**
     * Compares the key for this instance with another Confinc.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confinc and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confinc)) {
            return false;
        }
        Confinc that = (Confinc) other;
        Object myTabelaconfinc = this.getTabelaconfinc();
        Object yourTabelaconfinc = that.getTabelaconfinc();
        if (myTabelaconfinc==null ? yourTabelaconfinc!=null : !myTabelaconfinc.equals(yourTabelaconfinc)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confinc.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confinc)) return false;
        return this.equalKeys(other) && ((Confinc)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getTabelaconfinc() == null) {
            i = 0;
        } else {
            i = getTabelaconfinc().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confinc |");
        sb.append(" tabelaconfinc=").append(getTabelaconfinc());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("tabelaconfinc", getTabelaconfinc());
        return ret;
    }

}
