package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CONFLOG")
public class Conflog implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconflog";

    @Id
    @Column(name="CODCONFLOG", unique=true, nullable=false, precision=10)
    private int codconflog;
    @Column(name="TIPOCONFLOG", nullable=false, precision=5)
    private short tipoconflog;
    @Column(name="DATAHORA", nullable=false)
    private Timestamp datahora;
    @Column(name="NOMETABELA", length=60)
    private String nometabela;
    @Column(name="CODTABELA", length=60)
    private String codtabela;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIO", nullable=false)
    private Confusuario confusuario;

    /** Default constructor. */
    public Conflog() {
        super();
    }

    /**
     * Access method for codconflog.
     *
     * @return the current value of codconflog
     */
    public int getCodconflog() {
        return codconflog;
    }

    /**
     * Setter method for codconflog.
     *
     * @param aCodconflog the new value for codconflog
     */
    public void setCodconflog(int aCodconflog) {
        codconflog = aCodconflog;
    }

    /**
     * Access method for tipoconflog.
     *
     * @return the current value of tipoconflog
     */
    public short getTipoconflog() {
        return tipoconflog;
    }

    /**
     * Setter method for tipoconflog.
     *
     * @param aTipoconflog the new value for tipoconflog
     */
    public void setTipoconflog(short aTipoconflog) {
        tipoconflog = aTipoconflog;
    }

    /**
     * Access method for datahora.
     *
     * @return the current value of datahora
     */
    public Timestamp getDatahora() {
        return datahora;
    }

    /**
     * Setter method for datahora.
     *
     * @param aDatahora the new value for datahora
     */
    public void setDatahora(Timestamp aDatahora) {
        datahora = aDatahora;
    }

    /**
     * Access method for nometabela.
     *
     * @return the current value of nometabela
     */
    public String getNometabela() {
        return nometabela;
    }

    /**
     * Setter method for nometabela.
     *
     * @param aNometabela the new value for nometabela
     */
    public void setNometabela(String aNometabela) {
        nometabela = aNometabela;
    }

    /**
     * Access method for codtabela.
     *
     * @return the current value of codtabela
     */
    public String getCodtabela() {
        return codtabela;
    }

    /**
     * Setter method for codtabela.
     *
     * @param aCodtabela the new value for codtabela
     */
    public void setCodtabela(String aCodtabela) {
        codtabela = aCodtabela;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Compares the key for this instance with another Conflog.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Conflog and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Conflog)) {
            return false;
        }
        Conflog that = (Conflog) other;
        if (this.getCodconflog() != that.getCodconflog()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Conflog.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Conflog)) return false;
        return this.equalKeys(other) && ((Conflog)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconflog();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Conflog |");
        sb.append(" codconflog=").append(getCodconflog());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconflog", Integer.valueOf(getCodconflog()));
        return ret;
    }

}
