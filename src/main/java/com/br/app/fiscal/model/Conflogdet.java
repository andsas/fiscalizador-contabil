package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="CONFLOGDET")
public class Conflogdet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconflogdet";

    @Id
    @Column(name="CODCONFLOGDET", unique=true, nullable=false, precision=10)
    private int codconflogdet;
    @Column(name="CODCONFLOG", precision=10)
    private int codconflog;
    @Column(name="DESCCONFLOG", length=250)
    private String descconflog;
    @Column(name="DESCCOMPLETA")
    private String desccompleta;

    /** Default constructor. */
    public Conflogdet() {
        super();
    }

    /**
     * Access method for codconflogdet.
     *
     * @return the current value of codconflogdet
     */
    public int getCodconflogdet() {
        return codconflogdet;
    }

    /**
     * Setter method for codconflogdet.
     *
     * @param aCodconflogdet the new value for codconflogdet
     */
    public void setCodconflogdet(int aCodconflogdet) {
        codconflogdet = aCodconflogdet;
    }

    /**
     * Access method for codconflog.
     *
     * @return the current value of codconflog
     */
    public int getCodconflog() {
        return codconflog;
    }

    /**
     * Setter method for codconflog.
     *
     * @param aCodconflog the new value for codconflog
     */
    public void setCodconflog(int aCodconflog) {
        codconflog = aCodconflog;
    }

    /**
     * Access method for descconflog.
     *
     * @return the current value of descconflog
     */
    public String getDescconflog() {
        return descconflog;
    }

    /**
     * Setter method for descconflog.
     *
     * @param aDescconflog the new value for descconflog
     */
    public void setDescconflog(String aDescconflog) {
        descconflog = aDescconflog;
    }

    /**
     * Access method for desccompleta.
     *
     * @return the current value of desccompleta
     */
    public String getDesccompleta() {
        return desccompleta;
    }

    /**
     * Setter method for desccompleta.
     *
     * @param aDesccompleta the new value for desccompleta
     */
    public void setDesccompleta(String aDesccompleta) {
        desccompleta = aDesccompleta;
    }

    /**
     * Compares the key for this instance with another Conflogdet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Conflogdet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Conflogdet)) {
            return false;
        }
        Conflogdet that = (Conflogdet) other;
        if (this.getCodconflogdet() != that.getCodconflogdet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Conflogdet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Conflogdet)) return false;
        return this.equalKeys(other) && ((Conflogdet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconflogdet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Conflogdet |");
        sb.append(" codconflogdet=").append(getCodconflogdet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconflogdet", Integer.valueOf(getCodconflogdet()));
        return ret;
    }

}
