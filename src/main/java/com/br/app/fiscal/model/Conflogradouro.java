package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="CONFLOGRADOURO", indexes={@Index(name="conflogradouroConflogradouroIdx1", columnList="NOMETABELA,CODTABELA")})
public class Conflogradouro implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconflogradouro";

    @Id
    @Column(name="CODCONFLOGRADOURO", unique=true, nullable=false, precision=10)
    private int codconflogradouro;
    @Column(name="TIPOCONFLOGRADOURO", nullable=false, precision=5)
    private short tipoconflogradouro;
    @Column(name="LOGRADOURO", nullable=false, length=250)
    private String logradouro;
    @Column(name="BAIRRO", length=60)
    private String bairro;
    @Column(name="NUMERO", precision=10)
    private int numero;
    @Column(name="COMPLEMENTO", length=250)
    private String complemento;
    @Column(name="REFERENCIA")
    private String referencia;
    @Column(name="LATITUDE", precision=15, scale=10)
    private BigDecimal latitude;
    @Column(name="LONGITUDE", precision=15, scale=10)
    private BigDecimal longitude;
    @Column(name="MAPSLINK")
    private String mapslink;
    @Column(name="NOMETABELA", length=60)
    private String nometabela;
    @Column(name="CODTABELA", precision=10)
    private int codtabela;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFCEP", nullable=false)
    private Confcep confcep;
    @OneToMany(mappedBy="conflogradouro")
    private Set<Ctbbemveicinfracao> ctbbemveicinfracao;
    @OneToMany(mappedBy="conflogradouro")
    private Set<Gerimovel> gerimovel;
    @OneToMany(mappedBy="conflogradouro2")
    private Set<Optransacao> optransacao2;
    @OneToMany(mappedBy="conflogradouro3")
    private Set<Optransacao> optransacao3;
    @OneToMany(mappedBy="conflogradouro4")
    private Set<Optransacao> optransacao4;
    @OneToMany(mappedBy="conflogradouro")
    private Set<Optransacao> optransacao;

    /** Default constructor. */
    public Conflogradouro() {
        super();
    }

    /**
     * Access method for codconflogradouro.
     *
     * @return the current value of codconflogradouro
     */
    public int getCodconflogradouro() {
        return codconflogradouro;
    }

    /**
     * Setter method for codconflogradouro.
     *
     * @param aCodconflogradouro the new value for codconflogradouro
     */
    public void setCodconflogradouro(int aCodconflogradouro) {
        codconflogradouro = aCodconflogradouro;
    }

    /**
     * Access method for tipoconflogradouro.
     *
     * @return the current value of tipoconflogradouro
     */
    public short getTipoconflogradouro() {
        return tipoconflogradouro;
    }

    /**
     * Setter method for tipoconflogradouro.
     *
     * @param aTipoconflogradouro the new value for tipoconflogradouro
     */
    public void setTipoconflogradouro(short aTipoconflogradouro) {
        tipoconflogradouro = aTipoconflogradouro;
    }

    /**
     * Access method for logradouro.
     *
     * @return the current value of logradouro
     */
    public String getLogradouro() {
        return logradouro;
    }

    /**
     * Setter method for logradouro.
     *
     * @param aLogradouro the new value for logradouro
     */
    public void setLogradouro(String aLogradouro) {
        logradouro = aLogradouro;
    }

    /**
     * Access method for bairro.
     *
     * @return the current value of bairro
     */
    public String getBairro() {
        return bairro;
    }

    /**
     * Setter method for bairro.
     *
     * @param aBairro the new value for bairro
     */
    public void setBairro(String aBairro) {
        bairro = aBairro;
    }

    /**
     * Access method for numero.
     *
     * @return the current value of numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Setter method for numero.
     *
     * @param aNumero the new value for numero
     */
    public void setNumero(int aNumero) {
        numero = aNumero;
    }

    /**
     * Access method for complemento.
     *
     * @return the current value of complemento
     */
    public String getComplemento() {
        return complemento;
    }

    /**
     * Setter method for complemento.
     *
     * @param aComplemento the new value for complemento
     */
    public void setComplemento(String aComplemento) {
        complemento = aComplemento;
    }

    /**
     * Access method for referencia.
     *
     * @return the current value of referencia
     */
    public String getReferencia() {
        return referencia;
    }

    /**
     * Setter method for referencia.
     *
     * @param aReferencia the new value for referencia
     */
    public void setReferencia(String aReferencia) {
        referencia = aReferencia;
    }

    /**
     * Access method for latitude.
     *
     * @return the current value of latitude
     */
    public BigDecimal getLatitude() {
        return latitude;
    }

    /**
     * Setter method for latitude.
     *
     * @param aLatitude the new value for latitude
     */
    public void setLatitude(BigDecimal aLatitude) {
        latitude = aLatitude;
    }

    /**
     * Access method for longitude.
     *
     * @return the current value of longitude
     */
    public BigDecimal getLongitude() {
        return longitude;
    }

    /**
     * Setter method for longitude.
     *
     * @param aLongitude the new value for longitude
     */
    public void setLongitude(BigDecimal aLongitude) {
        longitude = aLongitude;
    }

    /**
     * Access method for mapslink.
     *
     * @return the current value of mapslink
     */
    public String getMapslink() {
        return mapslink;
    }

    /**
     * Setter method for mapslink.
     *
     * @param aMapslink the new value for mapslink
     */
    public void setMapslink(String aMapslink) {
        mapslink = aMapslink;
    }

    /**
     * Access method for nometabela.
     *
     * @return the current value of nometabela
     */
    public String getNometabela() {
        return nometabela;
    }

    /**
     * Setter method for nometabela.
     *
     * @param aNometabela the new value for nometabela
     */
    public void setNometabela(String aNometabela) {
        nometabela = aNometabela;
    }

    /**
     * Access method for codtabela.
     *
     * @return the current value of codtabela
     */
    public int getCodtabela() {
        return codtabela;
    }

    /**
     * Setter method for codtabela.
     *
     * @param aCodtabela the new value for codtabela
     */
    public void setCodtabela(int aCodtabela) {
        codtabela = aCodtabela;
    }

    /**
     * Access method for confcep.
     *
     * @return the current value of confcep
     */
    public Confcep getConfcep() {
        return confcep;
    }

    /**
     * Setter method for confcep.
     *
     * @param aConfcep the new value for confcep
     */
    public void setConfcep(Confcep aConfcep) {
        confcep = aConfcep;
    }

    /**
     * Access method for ctbbemveicinfracao.
     *
     * @return the current value of ctbbemveicinfracao
     */
    public Set<Ctbbemveicinfracao> getCtbbemveicinfracao() {
        return ctbbemveicinfracao;
    }

    /**
     * Setter method for ctbbemveicinfracao.
     *
     * @param aCtbbemveicinfracao the new value for ctbbemveicinfracao
     */
    public void setCtbbemveicinfracao(Set<Ctbbemveicinfracao> aCtbbemveicinfracao) {
        ctbbemveicinfracao = aCtbbemveicinfracao;
    }

    /**
     * Access method for gerimovel.
     *
     * @return the current value of gerimovel
     */
    public Set<Gerimovel> getGerimovel() {
        return gerimovel;
    }

    /**
     * Setter method for gerimovel.
     *
     * @param aGerimovel the new value for gerimovel
     */
    public void setGerimovel(Set<Gerimovel> aGerimovel) {
        gerimovel = aGerimovel;
    }

    /**
     * Access method for optransacao2.
     *
     * @return the current value of optransacao2
     */
    public Set<Optransacao> getOptransacao2() {
        return optransacao2;
    }

    /**
     * Setter method for optransacao2.
     *
     * @param aOptransacao2 the new value for optransacao2
     */
    public void setOptransacao2(Set<Optransacao> aOptransacao2) {
        optransacao2 = aOptransacao2;
    }

    /**
     * Access method for optransacao3.
     *
     * @return the current value of optransacao3
     */
    public Set<Optransacao> getOptransacao3() {
        return optransacao3;
    }

    /**
     * Setter method for optransacao3.
     *
     * @param aOptransacao3 the new value for optransacao3
     */
    public void setOptransacao3(Set<Optransacao> aOptransacao3) {
        optransacao3 = aOptransacao3;
    }

    /**
     * Access method for optransacao4.
     *
     * @return the current value of optransacao4
     */
    public Set<Optransacao> getOptransacao4() {
        return optransacao4;
    }

    /**
     * Setter method for optransacao4.
     *
     * @param aOptransacao4 the new value for optransacao4
     */
    public void setOptransacao4(Set<Optransacao> aOptransacao4) {
        optransacao4 = aOptransacao4;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Compares the key for this instance with another Conflogradouro.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Conflogradouro and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Conflogradouro)) {
            return false;
        }
        Conflogradouro that = (Conflogradouro) other;
        if (this.getCodconflogradouro() != that.getCodconflogradouro()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Conflogradouro.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Conflogradouro)) return false;
        return this.equalKeys(other) && ((Conflogradouro)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconflogradouro();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Conflogradouro |");
        sb.append(" codconflogradouro=").append(getCodconflogradouro());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconflogradouro", Integer.valueOf(getCodconflogradouro()));
        return ret;
    }

}
