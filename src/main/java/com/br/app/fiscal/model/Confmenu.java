package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="CONFMENU")
public class Confmenu implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfmenu";

    @Id
    @Column(name="CODCONFMENU", unique=true, nullable=false, length=40)
    private String codconfmenu;
    @Column(name="CODCONFPARENTMENU", length=40)
    private String codconfparentmenu;
    @Column(name="NIVEL", precision=5)
    private short nivel;
    @Column(name="NOMECOMPONENTE", length=250)
    private String nomecomponente;
    @Column(name="CLASSEENTIDADE", length=250)
    private String classeentidade;

    /** Default constructor. */
    public Confmenu() {
        super();
    }

    /**
     * Access method for codconfmenu.
     *
     * @return the current value of codconfmenu
     */
    public String getCodconfmenu() {
        return codconfmenu;
    }

    /**
     * Setter method for codconfmenu.
     *
     * @param aCodconfmenu the new value for codconfmenu
     */
    public void setCodconfmenu(String aCodconfmenu) {
        codconfmenu = aCodconfmenu;
    }

    /**
     * Access method for codconfparentmenu.
     *
     * @return the current value of codconfparentmenu
     */
    public String getCodconfparentmenu() {
        return codconfparentmenu;
    }

    /**
     * Setter method for codconfparentmenu.
     *
     * @param aCodconfparentmenu the new value for codconfparentmenu
     */
    public void setCodconfparentmenu(String aCodconfparentmenu) {
        codconfparentmenu = aCodconfparentmenu;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for nomecomponente.
     *
     * @return the current value of nomecomponente
     */
    public String getNomecomponente() {
        return nomecomponente;
    }

    /**
     * Setter method for nomecomponente.
     *
     * @param aNomecomponente the new value for nomecomponente
     */
    public void setNomecomponente(String aNomecomponente) {
        nomecomponente = aNomecomponente;
    }

    /**
     * Access method for classeentidade.
     *
     * @return the current value of classeentidade
     */
    public String getClasseentidade() {
        return classeentidade;
    }

    /**
     * Setter method for classeentidade.
     *
     * @param aClasseentidade the new value for classeentidade
     */
    public void setClasseentidade(String aClasseentidade) {
        classeentidade = aClasseentidade;
    }

    /**
     * Compares the key for this instance with another Confmenu.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confmenu and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confmenu)) {
            return false;
        }
        Confmenu that = (Confmenu) other;
        Object myCodconfmenu = this.getCodconfmenu();
        Object yourCodconfmenu = that.getCodconfmenu();
        if (myCodconfmenu==null ? yourCodconfmenu!=null : !myCodconfmenu.equals(yourCodconfmenu)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confmenu.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confmenu)) return false;
        return this.equalKeys(other) && ((Confmenu)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodconfmenu() == null) {
            i = 0;
        } else {
            i = getCodconfmenu().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confmenu |");
        sb.append(" codconfmenu=").append(getCodconfmenu());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfmenu", getCodconfmenu());
        return ret;
    }

}
