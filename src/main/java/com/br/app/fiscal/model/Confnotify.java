package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="CONFNOTIFY")
public class Confnotify implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfnotify";

    @Id
    @Column(name="CODCONFNOTIFY", unique=true, nullable=false, precision=10)
    private int codconfnotify;
    @Column(name="DESCCONFNOTIFY", nullable=false, length=250)
    private String descconfnotify;
    @Column(name="IMAGEINDEX", nullable=false, precision=5)
    private short imageindex;
    @Column(name="CLASSEENT", length=250)
    private String classeent;
    @Column(name="CLASSEFORM", length=250)
    private String classeform;
    @Column(name="CODENT", length=250)
    private String codent;
    @Column(name="DESCCOMPLETA")
    private String desccompleta;
    @Column(name="CODCONFUSUARIO", length=20)
    private String codconfusuario;
    @Column(name="CODCONFUSUARIOTIPO", precision=10)
    private int codconfusuariotipo;

    /** Default constructor. */
    public Confnotify() {
        super();
    }

    /**
     * Access method for codconfnotify.
     *
     * @return the current value of codconfnotify
     */
    public int getCodconfnotify() {
        return codconfnotify;
    }

    /**
     * Setter method for codconfnotify.
     *
     * @param aCodconfnotify the new value for codconfnotify
     */
    public void setCodconfnotify(int aCodconfnotify) {
        codconfnotify = aCodconfnotify;
    }

    /**
     * Access method for descconfnotify.
     *
     * @return the current value of descconfnotify
     */
    public String getDescconfnotify() {
        return descconfnotify;
    }

    /**
     * Setter method for descconfnotify.
     *
     * @param aDescconfnotify the new value for descconfnotify
     */
    public void setDescconfnotify(String aDescconfnotify) {
        descconfnotify = aDescconfnotify;
    }

    /**
     * Access method for imageindex.
     *
     * @return the current value of imageindex
     */
    public short getImageindex() {
        return imageindex;
    }

    /**
     * Setter method for imageindex.
     *
     * @param aImageindex the new value for imageindex
     */
    public void setImageindex(short aImageindex) {
        imageindex = aImageindex;
    }

    /**
     * Access method for classeent.
     *
     * @return the current value of classeent
     */
    public String getClasseent() {
        return classeent;
    }

    /**
     * Setter method for classeent.
     *
     * @param aClasseent the new value for classeent
     */
    public void setClasseent(String aClasseent) {
        classeent = aClasseent;
    }

    /**
     * Access method for classeform.
     *
     * @return the current value of classeform
     */
    public String getClasseform() {
        return classeform;
    }

    /**
     * Setter method for classeform.
     *
     * @param aClasseform the new value for classeform
     */
    public void setClasseform(String aClasseform) {
        classeform = aClasseform;
    }

    /**
     * Access method for codent.
     *
     * @return the current value of codent
     */
    public String getCodent() {
        return codent;
    }

    /**
     * Setter method for codent.
     *
     * @param aCodent the new value for codent
     */
    public void setCodent(String aCodent) {
        codent = aCodent;
    }

    /**
     * Access method for desccompleta.
     *
     * @return the current value of desccompleta
     */
    public String getDesccompleta() {
        return desccompleta;
    }

    /**
     * Setter method for desccompleta.
     *
     * @param aDesccompleta the new value for desccompleta
     */
    public void setDesccompleta(String aDesccompleta) {
        desccompleta = aDesccompleta;
    }

    /**
     * Access method for codconfusuario.
     *
     * @return the current value of codconfusuario
     */
    public String getCodconfusuario() {
        return codconfusuario;
    }

    /**
     * Setter method for codconfusuario.
     *
     * @param aCodconfusuario the new value for codconfusuario
     */
    public void setCodconfusuario(String aCodconfusuario) {
        codconfusuario = aCodconfusuario;
    }

    /**
     * Access method for codconfusuariotipo.
     *
     * @return the current value of codconfusuariotipo
     */
    public int getCodconfusuariotipo() {
        return codconfusuariotipo;
    }

    /**
     * Setter method for codconfusuariotipo.
     *
     * @param aCodconfusuariotipo the new value for codconfusuariotipo
     */
    public void setCodconfusuariotipo(int aCodconfusuariotipo) {
        codconfusuariotipo = aCodconfusuariotipo;
    }

    /**
     * Compares the key for this instance with another Confnotify.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confnotify and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confnotify)) {
            return false;
        }
        Confnotify that = (Confnotify) other;
        if (this.getCodconfnotify() != that.getCodconfnotify()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confnotify.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confnotify)) return false;
        return this.equalKeys(other) && ((Confnotify)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfnotify();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confnotify |");
        sb.append(" codconfnotify=").append(getCodconfnotify());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfnotify", Integer.valueOf(getCodconfnotify()));
        return ret;
    }

}
