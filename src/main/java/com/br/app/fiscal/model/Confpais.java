package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CONFPAIS")
public class Confpais implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfpais";

    @Id
    @Column(name="CODCONFPAIS", unique=true, nullable=false, precision=10)
    private int codconfpais;
    @Column(name="CODCONFIBGEPAIS", nullable=false, precision=10)
    private int codconfibgepais;
    @Column(name="NOMECONFPAIS", nullable=false, length=250)
    private String nomeconfpais;
    @OneToMany(mappedBy="confpais")
    private Set<Agvistotipo> agvistotipo;
    @OneToMany(mappedBy="confpais")
    private Set<Confuf> confuf;
    @OneToMany(mappedBy="confpais")
    private Set<Finmoeda> finmoeda;

    /** Default constructor. */
    public Confpais() {
        super();
    }

    /**
     * Access method for codconfpais.
     *
     * @return the current value of codconfpais
     */
    public int getCodconfpais() {
        return codconfpais;
    }

    /**
     * Setter method for codconfpais.
     *
     * @param aCodconfpais the new value for codconfpais
     */
    public void setCodconfpais(int aCodconfpais) {
        codconfpais = aCodconfpais;
    }

    /**
     * Access method for codconfibgepais.
     *
     * @return the current value of codconfibgepais
     */
    public int getCodconfibgepais() {
        return codconfibgepais;
    }

    /**
     * Setter method for codconfibgepais.
     *
     * @param aCodconfibgepais the new value for codconfibgepais
     */
    public void setCodconfibgepais(int aCodconfibgepais) {
        codconfibgepais = aCodconfibgepais;
    }

    /**
     * Access method for nomeconfpais.
     *
     * @return the current value of nomeconfpais
     */
    public String getNomeconfpais() {
        return nomeconfpais;
    }

    /**
     * Setter method for nomeconfpais.
     *
     * @param aNomeconfpais the new value for nomeconfpais
     */
    public void setNomeconfpais(String aNomeconfpais) {
        nomeconfpais = aNomeconfpais;
    }

    /**
     * Access method for agvistotipo.
     *
     * @return the current value of agvistotipo
     */
    public Set<Agvistotipo> getAgvistotipo() {
        return agvistotipo;
    }

    /**
     * Setter method for agvistotipo.
     *
     * @param aAgvistotipo the new value for agvistotipo
     */
    public void setAgvistotipo(Set<Agvistotipo> aAgvistotipo) {
        agvistotipo = aAgvistotipo;
    }

    /**
     * Access method for confuf.
     *
     * @return the current value of confuf
     */
    public Set<Confuf> getConfuf() {
        return confuf;
    }

    /**
     * Setter method for confuf.
     *
     * @param aConfuf the new value for confuf
     */
    public void setConfuf(Set<Confuf> aConfuf) {
        confuf = aConfuf;
    }

    /**
     * Access method for finmoeda.
     *
     * @return the current value of finmoeda
     */
    public Set<Finmoeda> getFinmoeda() {
        return finmoeda;
    }

    /**
     * Setter method for finmoeda.
     *
     * @param aFinmoeda the new value for finmoeda
     */
    public void setFinmoeda(Set<Finmoeda> aFinmoeda) {
        finmoeda = aFinmoeda;
    }

    /**
     * Compares the key for this instance with another Confpais.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confpais and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confpais)) {
            return false;
        }
        Confpais that = (Confpais) other;
        if (this.getCodconfpais() != that.getCodconfpais()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confpais.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confpais)) return false;
        return this.equalKeys(other) && ((Confpais)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfpais();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confpais |");
        sb.append(" codconfpais=").append(getCodconfpais());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfpais", Integer.valueOf(getCodconfpais()));
        return ret;
    }

}
