package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CONFPLANOCONTA")
public class Confplanoconta implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfplanoconta";

    @Id
    @Column(name="CODCONFPLANOCONTA", unique=true, nullable=false, precision=10)
    private Integer codconfplanoconta;
    @Column(name="TIPOCONFPLANOCONTA", nullable=false, precision=5)
    private short tipoconfplanoconta;
    @Column(name="MASCARA", nullable=false, length=60)
    private String mascara;
    @Column(name="MASCSTR", length=250)
    private String mascstr;
    @Column(name="NIVEIS", nullable=false, precision=5)
    private short niveis;
    @Column(name="NIVEL1", nullable=false, length=20)
    private String nivel1;
    @Column(name="NIVEL2", nullable=false, length=20)
    private String nivel2;
    @Column(name="NIVEL3", nullable=false, length=20)
    private String nivel3;
    @Column(name="NIVEL4", length=20)
    private String nivel4;
    @Column(name="NIVEL5", length=20)
    private String nivel5;
    @Column(name="NIVEL6", length=20)
    private String nivel6;
    @Column(name="NIVEL7", length=20)
    private String nivel7;
    @Column(name="NIVEL8", length=20)
    private String nivel8;
    @Column(name="NIVEL9", length=20)
    private String nivel9;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;

    /** Default constructor. */
    public Confplanoconta() {
        super();
    }

    /**
     * Access method for codconfplanoconta.
     *
     * @return the current value of codconfplanoconta
     */
    public Integer getCodconfplanoconta() {
        return codconfplanoconta;
    }

    /**
     * Setter method for codconfplanoconta.
     *
     * @param aCodconfplanoconta the new value for codconfplanoconta
     */
    public void setCodconfplanoconta(Integer aCodconfplanoconta) {
        codconfplanoconta = aCodconfplanoconta;
    }

    /**
     * Access method for tipoconfplanoconta.
     *
     * @return the current value of tipoconfplanoconta
     */
    public short getTipoconfplanoconta() {
        return tipoconfplanoconta;
    }

    /**
     * Setter method for tipoconfplanoconta.
     *
     * @param aTipoconfplanoconta the new value for tipoconfplanoconta
     */
    public void setTipoconfplanoconta(short aTipoconfplanoconta) {
        tipoconfplanoconta = aTipoconfplanoconta;
    }

    /**
     * Access method for mascara.
     *
     * @return the current value of mascara
     */
    public String getMascara() {
        return mascara;
    }

    /**
     * Setter method for mascara.
     *
     * @param aMascara the new value for mascara
     */
    public void setMascara(String aMascara) {
        mascara = aMascara;
    }

    /**
     * Access method for mascstr.
     *
     * @return the current value of mascstr
     */
    public String getMascstr() {
        return mascstr;
    }

    /**
     * Setter method for mascstr.
     *
     * @param aMascstr the new value for mascstr
     */
    public void setMascstr(String aMascstr) {
        mascstr = aMascstr;
    }

    /**
     * Access method for niveis.
     *
     * @return the current value of niveis
     */
    public short getNiveis() {
        return niveis;
    }

    /**
     * Setter method for niveis.
     *
     * @param aNiveis the new value for niveis
     */
    public void setNiveis(short aNiveis) {
        niveis = aNiveis;
    }

    /**
     * Access method for nivel1.
     *
     * @return the current value of nivel1
     */
    public String getNivel1() {
        return nivel1;
    }

    /**
     * Setter method for nivel1.
     *
     * @param aNivel1 the new value for nivel1
     */
    public void setNivel1(String aNivel1) {
        nivel1 = aNivel1;
    }

    /**
     * Access method for nivel2.
     *
     * @return the current value of nivel2
     */
    public String getNivel2() {
        return nivel2;
    }

    /**
     * Setter method for nivel2.
     *
     * @param aNivel2 the new value for nivel2
     */
    public void setNivel2(String aNivel2) {
        nivel2 = aNivel2;
    }

    /**
     * Access method for nivel3.
     *
     * @return the current value of nivel3
     */
    public String getNivel3() {
        return nivel3;
    }

    /**
     * Setter method for nivel3.
     *
     * @param aNivel3 the new value for nivel3
     */
    public void setNivel3(String aNivel3) {
        nivel3 = aNivel3;
    }

    /**
     * Access method for nivel4.
     *
     * @return the current value of nivel4
     */
    public String getNivel4() {
        return nivel4;
    }

    /**
     * Setter method for nivel4.
     *
     * @param aNivel4 the new value for nivel4
     */
    public void setNivel4(String aNivel4) {
        nivel4 = aNivel4;
    }

    /**
     * Access method for nivel5.
     *
     * @return the current value of nivel5
     */
    public String getNivel5() {
        return nivel5;
    }

    /**
     * Setter method for nivel5.
     *
     * @param aNivel5 the new value for nivel5
     */
    public void setNivel5(String aNivel5) {
        nivel5 = aNivel5;
    }

    /**
     * Access method for nivel6.
     *
     * @return the current value of nivel6
     */
    public String getNivel6() {
        return nivel6;
    }

    /**
     * Setter method for nivel6.
     *
     * @param aNivel6 the new value for nivel6
     */
    public void setNivel6(String aNivel6) {
        nivel6 = aNivel6;
    }

    /**
     * Access method for nivel7.
     *
     * @return the current value of nivel7
     */
    public String getNivel7() {
        return nivel7;
    }

    /**
     * Setter method for nivel7.
     *
     * @param aNivel7 the new value for nivel7
     */
    public void setNivel7(String aNivel7) {
        nivel7 = aNivel7;
    }

    /**
     * Access method for nivel8.
     *
     * @return the current value of nivel8
     */
    public String getNivel8() {
        return nivel8;
    }

    /**
     * Setter method for nivel8.
     *
     * @param aNivel8 the new value for nivel8
     */
    public void setNivel8(String aNivel8) {
        nivel8 = aNivel8;
    }

    /**
     * Access method for nivel9.
     *
     * @return the current value of nivel9
     */
    public String getNivel9() {
        return nivel9;
    }

    /**
     * Setter method for nivel9.
     *
     * @param aNivel9 the new value for nivel9
     */
    public void setNivel9(String aNivel9) {
        nivel9 = aNivel9;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Compares the key for this instance with another Confplanoconta.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confplanoconta and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confplanoconta)) {
            return false;
        }
        Confplanoconta that = (Confplanoconta) other;
        if (this.getCodconfplanoconta() != that.getCodconfplanoconta()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confplanoconta.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confplanoconta)) return false;
        return this.equalKeys(other) && ((Confplanoconta)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfplanoconta();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confplanoconta |");
        sb.append(" codconfplanoconta=").append(getCodconfplanoconta());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfplanoconta", Integer.valueOf(getCodconfplanoconta()));
        return ret;
    }

}
