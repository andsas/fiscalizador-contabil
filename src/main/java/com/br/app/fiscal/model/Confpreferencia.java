package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="CONFPREFERENCIA")
public class Confpreferencia implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfpreferencia";

    @Id
    @Column(name="CODCONFPREFERENCIA", unique=true, nullable=false, precision=10)
    private int codconfpreferencia;
    @Column(name="DESCCONFPREFERENCIA", nullable=false, length=60)
    private String descconfpreferencia;
    @Column(name="DESCCOMPLETA", nullable=false, length=250)
    private String desccompleta;
    @Column(name="TIPOCONFPREFERENCIA", nullable=false, precision=5)
    private short tipoconfpreferencia;
    @Column(name="VALOR", length=250)
    private String valor;
    @Column(name="CODCONFGRUPOPREFERENCIA", precision=10)
    private int codconfgrupopreferencia;

    /** Default constructor. */
    public Confpreferencia() {
        super();
    }

    /**
     * Access method for codconfpreferencia.
     *
     * @return the current value of codconfpreferencia
     */
    public int getCodconfpreferencia() {
        return codconfpreferencia;
    }

    /**
     * Setter method for codconfpreferencia.
     *
     * @param aCodconfpreferencia the new value for codconfpreferencia
     */
    public void setCodconfpreferencia(int aCodconfpreferencia) {
        codconfpreferencia = aCodconfpreferencia;
    }

    /**
     * Access method for descconfpreferencia.
     *
     * @return the current value of descconfpreferencia
     */
    public String getDescconfpreferencia() {
        return descconfpreferencia;
    }

    /**
     * Setter method for descconfpreferencia.
     *
     * @param aDescconfpreferencia the new value for descconfpreferencia
     */
    public void setDescconfpreferencia(String aDescconfpreferencia) {
        descconfpreferencia = aDescconfpreferencia;
    }

    /**
     * Access method for desccompleta.
     *
     * @return the current value of desccompleta
     */
    public String getDesccompleta() {
        return desccompleta;
    }

    /**
     * Setter method for desccompleta.
     *
     * @param aDesccompleta the new value for desccompleta
     */
    public void setDesccompleta(String aDesccompleta) {
        desccompleta = aDesccompleta;
    }

    /**
     * Access method for tipoconfpreferencia.
     *
     * @return the current value of tipoconfpreferencia
     */
    public short getTipoconfpreferencia() {
        return tipoconfpreferencia;
    }

    /**
     * Setter method for tipoconfpreferencia.
     *
     * @param aTipoconfpreferencia the new value for tipoconfpreferencia
     */
    public void setTipoconfpreferencia(short aTipoconfpreferencia) {
        tipoconfpreferencia = aTipoconfpreferencia;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(String aValor) {
        valor = aValor;
    }

    /**
     * Access method for codconfgrupopreferencia.
     *
     * @return the current value of codconfgrupopreferencia
     */
    public int getCodconfgrupopreferencia() {
        return codconfgrupopreferencia;
    }

    /**
     * Setter method for codconfgrupopreferencia.
     *
     * @param aCodconfgrupopreferencia the new value for codconfgrupopreferencia
     */
    public void setCodconfgrupopreferencia(int aCodconfgrupopreferencia) {
        codconfgrupopreferencia = aCodconfgrupopreferencia;
    }

    /**
     * Compares the key for this instance with another Confpreferencia.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confpreferencia and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confpreferencia)) {
            return false;
        }
        Confpreferencia that = (Confpreferencia) other;
        if (this.getCodconfpreferencia() != that.getCodconfpreferencia()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confpreferencia.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confpreferencia)) return false;
        return this.equalKeys(other) && ((Confpreferencia)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfpreferencia();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confpreferencia |");
        sb.append(" codconfpreferencia=").append(getCodconfpreferencia());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfpreferencia", Integer.valueOf(getCodconfpreferencia()));
        return ret;
    }

}
