package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="CONFPREFERENCIAGRUPO")
public class Confpreferenciagrupo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfpreferenciagrupo";

    @Id
    @Column(name="CODCONFPREFERENCIAGRUPO", unique=true, nullable=false, precision=10)
    private int codconfpreferenciagrupo;
    @Column(name="DESCCONFPREFERENCIAGRUPO", nullable=false, length=250)
    private String descconfpreferenciagrupo;
    @Column(name="CODCONFPARENTPREFERENCIAGRUPO", precision=10)
    private int codconfparentpreferenciagrupo;
    @Column(name="NIVEL", precision=5)
    private short nivel;

    /** Default constructor. */
    public Confpreferenciagrupo() {
        super();
    }

    /**
     * Access method for codconfpreferenciagrupo.
     *
     * @return the current value of codconfpreferenciagrupo
     */
    public int getCodconfpreferenciagrupo() {
        return codconfpreferenciagrupo;
    }

    /**
     * Setter method for codconfpreferenciagrupo.
     *
     * @param aCodconfpreferenciagrupo the new value for codconfpreferenciagrupo
     */
    public void setCodconfpreferenciagrupo(int aCodconfpreferenciagrupo) {
        codconfpreferenciagrupo = aCodconfpreferenciagrupo;
    }

    /**
     * Access method for descconfpreferenciagrupo.
     *
     * @return the current value of descconfpreferenciagrupo
     */
    public String getDescconfpreferenciagrupo() {
        return descconfpreferenciagrupo;
    }

    /**
     * Setter method for descconfpreferenciagrupo.
     *
     * @param aDescconfpreferenciagrupo the new value for descconfpreferenciagrupo
     */
    public void setDescconfpreferenciagrupo(String aDescconfpreferenciagrupo) {
        descconfpreferenciagrupo = aDescconfpreferenciagrupo;
    }

    /**
     * Access method for codconfparentpreferenciagrupo.
     *
     * @return the current value of codconfparentpreferenciagrupo
     */
    public int getCodconfparentpreferenciagrupo() {
        return codconfparentpreferenciagrupo;
    }

    /**
     * Setter method for codconfparentpreferenciagrupo.
     *
     * @param aCodconfparentpreferenciagrupo the new value for codconfparentpreferenciagrupo
     */
    public void setCodconfparentpreferenciagrupo(int aCodconfparentpreferenciagrupo) {
        codconfparentpreferenciagrupo = aCodconfparentpreferenciagrupo;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Compares the key for this instance with another Confpreferenciagrupo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confpreferenciagrupo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confpreferenciagrupo)) {
            return false;
        }
        Confpreferenciagrupo that = (Confpreferenciagrupo) other;
        if (this.getCodconfpreferenciagrupo() != that.getCodconfpreferenciagrupo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confpreferenciagrupo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confpreferenciagrupo)) return false;
        return this.equalKeys(other) && ((Confpreferenciagrupo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfpreferenciagrupo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confpreferenciagrupo |");
        sb.append(" codconfpreferenciagrupo=").append(getCodconfpreferenciagrupo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfpreferenciagrupo", Integer.valueOf(getCodconfpreferenciagrupo()));
        return ret;
    }

}
