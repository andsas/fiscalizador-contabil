package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CONFTASK")
public class Conftask implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconftask";

    @Id
    @Column(name="CODCONFTASK", unique=true, nullable=false, precision=10)
    private int codconftask;
    @Column(name="DESCCONFTASK", nullable=false, length=250)
    private String descconftask;
    @Column(name="CODCONFUSUARIO", nullable=false, length=20)
    private String codconfusuario;
    @Column(name="DATACRIACAO", nullable=false)
    private Timestamp datacriacao;
    @Column(name="DATAINICIAL")
    private Timestamp datainicial;
    @Column(name="DATAFINAL")
    private Timestamp datafinal;
    @Column(name="CODCONFEMPR", nullable=false, precision=10)
    private int codconfempr;
    @Column(name="OBSERVACOES")
    private String observacoes;
    @Column(name="CODTABELA", length=60)
    private String codtabela;
    @Column(name="CODPARENTCONFTASK", precision=10)
    private int codparentconftask;
    @Column(name="DATASILENCIO")
    private Timestamp datasilencio;
    @Column(name="MOTIVOSILENCIO")
    private String motivosilencio;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODTIPOCONFTASK", nullable=false)
    private Conftasktipo conftasktipo;
    @ManyToOne
    @JoinColumn(name="CODSTATUSCONFTASK")
    private Conftaskstatus conftaskstatus;
    @ManyToOne
    @JoinColumn(name="CODCONFDICTAB")
    private Confdictab confdictab;

    /** Default constructor. */
    public Conftask() {
        super();
    }

    /**
     * Access method for codconftask.
     *
     * @return the current value of codconftask
     */
    public int getCodconftask() {
        return codconftask;
    }

    /**
     * Setter method for codconftask.
     *
     * @param aCodconftask the new value for codconftask
     */
    public void setCodconftask(int aCodconftask) {
        codconftask = aCodconftask;
    }

    /**
     * Access method for descconftask.
     *
     * @return the current value of descconftask
     */
    public String getDescconftask() {
        return descconftask;
    }

    /**
     * Setter method for descconftask.
     *
     * @param aDescconftask the new value for descconftask
     */
    public void setDescconftask(String aDescconftask) {
        descconftask = aDescconftask;
    }

    /**
     * Access method for codconfusuario.
     *
     * @return the current value of codconfusuario
     */
    public String getCodconfusuario() {
        return codconfusuario;
    }

    /**
     * Setter method for codconfusuario.
     *
     * @param aCodconfusuario the new value for codconfusuario
     */
    public void setCodconfusuario(String aCodconfusuario) {
        codconfusuario = aCodconfusuario;
    }

    /**
     * Access method for datacriacao.
     *
     * @return the current value of datacriacao
     */
    public Timestamp getDatacriacao() {
        return datacriacao;
    }

    /**
     * Setter method for datacriacao.
     *
     * @param aDatacriacao the new value for datacriacao
     */
    public void setDatacriacao(Timestamp aDatacriacao) {
        datacriacao = aDatacriacao;
    }

    /**
     * Access method for datainicial.
     *
     * @return the current value of datainicial
     */
    public Timestamp getDatainicial() {
        return datainicial;
    }

    /**
     * Setter method for datainicial.
     *
     * @param aDatainicial the new value for datainicial
     */
    public void setDatainicial(Timestamp aDatainicial) {
        datainicial = aDatainicial;
    }

    /**
     * Access method for datafinal.
     *
     * @return the current value of datafinal
     */
    public Timestamp getDatafinal() {
        return datafinal;
    }

    /**
     * Setter method for datafinal.
     *
     * @param aDatafinal the new value for datafinal
     */
    public void setDatafinal(Timestamp aDatafinal) {
        datafinal = aDatafinal;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for observacoes.
     *
     * @return the current value of observacoes
     */
    public String getObservacoes() {
        return observacoes;
    }

    /**
     * Setter method for observacoes.
     *
     * @param aObservacoes the new value for observacoes
     */
    public void setObservacoes(String aObservacoes) {
        observacoes = aObservacoes;
    }

    /**
     * Access method for codtabela.
     *
     * @return the current value of codtabela
     */
    public String getCodtabela() {
        return codtabela;
    }

    /**
     * Setter method for codtabela.
     *
     * @param aCodtabela the new value for codtabela
     */
    public void setCodtabela(String aCodtabela) {
        codtabela = aCodtabela;
    }

    /**
     * Access method for codparentconftask.
     *
     * @return the current value of codparentconftask
     */
    public int getCodparentconftask() {
        return codparentconftask;
    }

    /**
     * Setter method for codparentconftask.
     *
     * @param aCodparentconftask the new value for codparentconftask
     */
    public void setCodparentconftask(int aCodparentconftask) {
        codparentconftask = aCodparentconftask;
    }

    /**
     * Access method for datasilencio.
     *
     * @return the current value of datasilencio
     */
    public Timestamp getDatasilencio() {
        return datasilencio;
    }

    /**
     * Setter method for datasilencio.
     *
     * @param aDatasilencio the new value for datasilencio
     */
    public void setDatasilencio(Timestamp aDatasilencio) {
        datasilencio = aDatasilencio;
    }

    /**
     * Access method for motivosilencio.
     *
     * @return the current value of motivosilencio
     */
    public String getMotivosilencio() {
        return motivosilencio;
    }

    /**
     * Setter method for motivosilencio.
     *
     * @param aMotivosilencio the new value for motivosilencio
     */
    public void setMotivosilencio(String aMotivosilencio) {
        motivosilencio = aMotivosilencio;
    }

    /**
     * Access method for conftasktipo.
     *
     * @return the current value of conftasktipo
     */
    public Conftasktipo getConftasktipo() {
        return conftasktipo;
    }

    /**
     * Setter method for conftasktipo.
     *
     * @param aConftasktipo the new value for conftasktipo
     */
    public void setConftasktipo(Conftasktipo aConftasktipo) {
        conftasktipo = aConftasktipo;
    }

    /**
     * Access method for conftaskstatus.
     *
     * @return the current value of conftaskstatus
     */
    public Conftaskstatus getConftaskstatus() {
        return conftaskstatus;
    }

    /**
     * Setter method for conftaskstatus.
     *
     * @param aConftaskstatus the new value for conftaskstatus
     */
    public void setConftaskstatus(Conftaskstatus aConftaskstatus) {
        conftaskstatus = aConftaskstatus;
    }

    /**
     * Access method for confdictab.
     *
     * @return the current value of confdictab
     */
    public Confdictab getConfdictab() {
        return confdictab;
    }

    /**
     * Setter method for confdictab.
     *
     * @param aConfdictab the new value for confdictab
     */
    public void setConfdictab(Confdictab aConfdictab) {
        confdictab = aConfdictab;
    }

    /**
     * Compares the key for this instance with another Conftask.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Conftask and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Conftask)) {
            return false;
        }
        Conftask that = (Conftask) other;
        if (this.getCodconftask() != that.getCodconftask()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Conftask.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Conftask)) return false;
        return this.equalKeys(other) && ((Conftask)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconftask();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Conftask |");
        sb.append(" codconftask=").append(getCodconftask());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconftask", Integer.valueOf(getCodconftask()));
        return ret;
    }

}
