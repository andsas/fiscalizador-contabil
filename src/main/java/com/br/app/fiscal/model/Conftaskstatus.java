package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CONFTASKSTATUS")
public class Conftaskstatus implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconftaskstatus";

    @Id
    @Column(name="CODCONFTASKSTATUS", unique=true, nullable=false, precision=10)
    private int codconftaskstatus;
    @Column(name="DESCCONFTASKSTATUS", nullable=false, length=250)
    private String descconftaskstatus;
    @Column(name="CORCONFTASKSTATUS", precision=10)
    private int corconftaskstatus;
    @OneToMany(mappedBy="conftaskstatus")
    private Set<Conftask> conftask;

    /** Default constructor. */
    public Conftaskstatus() {
        super();
    }

    /**
     * Access method for codconftaskstatus.
     *
     * @return the current value of codconftaskstatus
     */
    public int getCodconftaskstatus() {
        return codconftaskstatus;
    }

    /**
     * Setter method for codconftaskstatus.
     *
     * @param aCodconftaskstatus the new value for codconftaskstatus
     */
    public void setCodconftaskstatus(int aCodconftaskstatus) {
        codconftaskstatus = aCodconftaskstatus;
    }

    /**
     * Access method for descconftaskstatus.
     *
     * @return the current value of descconftaskstatus
     */
    public String getDescconftaskstatus() {
        return descconftaskstatus;
    }

    /**
     * Setter method for descconftaskstatus.
     *
     * @param aDescconftaskstatus the new value for descconftaskstatus
     */
    public void setDescconftaskstatus(String aDescconftaskstatus) {
        descconftaskstatus = aDescconftaskstatus;
    }

    /**
     * Access method for corconftaskstatus.
     *
     * @return the current value of corconftaskstatus
     */
    public int getCorconftaskstatus() {
        return corconftaskstatus;
    }

    /**
     * Setter method for corconftaskstatus.
     *
     * @param aCorconftaskstatus the new value for corconftaskstatus
     */
    public void setCorconftaskstatus(int aCorconftaskstatus) {
        corconftaskstatus = aCorconftaskstatus;
    }

    /**
     * Access method for conftask.
     *
     * @return the current value of conftask
     */
    public Set<Conftask> getConftask() {
        return conftask;
    }

    /**
     * Setter method for conftask.
     *
     * @param aConftask the new value for conftask
     */
    public void setConftask(Set<Conftask> aConftask) {
        conftask = aConftask;
    }

    /**
     * Compares the key for this instance with another Conftaskstatus.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Conftaskstatus and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Conftaskstatus)) {
            return false;
        }
        Conftaskstatus that = (Conftaskstatus) other;
        if (this.getCodconftaskstatus() != that.getCodconftaskstatus()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Conftaskstatus.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Conftaskstatus)) return false;
        return this.equalKeys(other) && ((Conftaskstatus)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconftaskstatus();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Conftaskstatus |");
        sb.append(" codconftaskstatus=").append(getCodconftaskstatus());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconftaskstatus", Integer.valueOf(getCodconftaskstatus()));
        return ret;
    }

}
