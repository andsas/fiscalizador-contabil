package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CONFTASKTIPO")
public class Conftasktipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconftasktipo";

    @Id
    @Column(name="CODCONFTASKTIPO", unique=true, nullable=false, precision=10)
    private int codconftasktipo;
    @Column(name="DESCCONFTASKTIPO", nullable=false, length=250)
    private String descconftasktipo;
    @Column(name="CORCONFTASKTIPO", precision=10)
    private int corconftasktipo;
    @OneToMany(mappedBy="conftasktipo")
    private Set<Conftask> conftask;

    /** Default constructor. */
    public Conftasktipo() {
        super();
    }

    /**
     * Access method for codconftasktipo.
     *
     * @return the current value of codconftasktipo
     */
    public int getCodconftasktipo() {
        return codconftasktipo;
    }

    /**
     * Setter method for codconftasktipo.
     *
     * @param aCodconftasktipo the new value for codconftasktipo
     */
    public void setCodconftasktipo(int aCodconftasktipo) {
        codconftasktipo = aCodconftasktipo;
    }

    /**
     * Access method for descconftasktipo.
     *
     * @return the current value of descconftasktipo
     */
    public String getDescconftasktipo() {
        return descconftasktipo;
    }

    /**
     * Setter method for descconftasktipo.
     *
     * @param aDescconftasktipo the new value for descconftasktipo
     */
    public void setDescconftasktipo(String aDescconftasktipo) {
        descconftasktipo = aDescconftasktipo;
    }

    /**
     * Access method for corconftasktipo.
     *
     * @return the current value of corconftasktipo
     */
    public int getCorconftasktipo() {
        return corconftasktipo;
    }

    /**
     * Setter method for corconftasktipo.
     *
     * @param aCorconftasktipo the new value for corconftasktipo
     */
    public void setCorconftasktipo(int aCorconftasktipo) {
        corconftasktipo = aCorconftasktipo;
    }

    /**
     * Access method for conftask.
     *
     * @return the current value of conftask
     */
    public Set<Conftask> getConftask() {
        return conftask;
    }

    /**
     * Setter method for conftask.
     *
     * @param aConftask the new value for conftask
     */
    public void setConftask(Set<Conftask> aConftask) {
        conftask = aConftask;
    }

    /**
     * Compares the key for this instance with another Conftasktipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Conftasktipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Conftasktipo)) {
            return false;
        }
        Conftasktipo that = (Conftasktipo) other;
        if (this.getCodconftasktipo() != that.getCodconftasktipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Conftasktipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Conftasktipo)) return false;
        return this.equalKeys(other) && ((Conftasktipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconftasktipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Conftasktipo |");
        sb.append(" codconftasktipo=").append(getCodconftasktipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconftasktipo", Integer.valueOf(getCodconftasktipo()));
        return ret;
    }

}
