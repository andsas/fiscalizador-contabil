package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity(name="CONFUF")
public class Confuf implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfuf";

    @Id
    @Column(name="CODCONFUF", unique=true, nullable=false, length=4)
    private String codconfuf;
    @Column(name="NOMECONFUF", nullable=false, length=250)
    private String nomeconfuf;
    @OneToMany(mappedBy="confuf")
    private Set<Agagentetransp> agagentetransp;
    @OneToMany(mappedBy="confuf")
    private Set<Comtabela> comtabela;
    @OneToMany(mappedBy="confuf")
    private Set<Confcidade> confcidade;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFPAIS", nullable=false)
    private Confpais confpais;
    @OneToMany(mappedBy="confuf")
    private Set<Folcolaborador> folcolaborador;
    @OneToMany(mappedBy="confuf")
    private Set<Folsalminimo> folsalminimo;
    @OneToMany(mappedBy="confuf")
    private Set<Impicmsst> impicmsst;
    @OneToMany(mappedBy="confuf")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToOne(mappedBy="confuf")
    private Impicms impicms;

    /** Default constructor. */
    public Confuf() {
        super();
    }

    /**
     * Access method for codconfuf.
     *
     * @return the current value of codconfuf
     */
    public String getCodconfuf() {
        return codconfuf;
    }

    /**
     * Setter method for codconfuf.
     *
     * @param aCodconfuf the new value for codconfuf
     */
    public void setCodconfuf(String aCodconfuf) {
        codconfuf = aCodconfuf;
    }

    /**
     * Access method for nomeconfuf.
     *
     * @return the current value of nomeconfuf
     */
    public String getNomeconfuf() {
        return nomeconfuf;
    }

    /**
     * Setter method for nomeconfuf.
     *
     * @param aNomeconfuf the new value for nomeconfuf
     */
    public void setNomeconfuf(String aNomeconfuf) {
        nomeconfuf = aNomeconfuf;
    }

    /**
     * Access method for agagentetransp.
     *
     * @return the current value of agagentetransp
     */
    public Set<Agagentetransp> getAgagentetransp() {
        return agagentetransp;
    }

    /**
     * Setter method for agagentetransp.
     *
     * @param aAgagentetransp the new value for agagentetransp
     */
    public void setAgagentetransp(Set<Agagentetransp> aAgagentetransp) {
        agagentetransp = aAgagentetransp;
    }

    /**
     * Access method for comtabela.
     *
     * @return the current value of comtabela
     */
    public Set<Comtabela> getComtabela() {
        return comtabela;
    }

    /**
     * Setter method for comtabela.
     *
     * @param aComtabela the new value for comtabela
     */
    public void setComtabela(Set<Comtabela> aComtabela) {
        comtabela = aComtabela;
    }

    /**
     * Access method for confcidade.
     *
     * @return the current value of confcidade
     */
    public Set<Confcidade> getConfcidade() {
        return confcidade;
    }

    /**
     * Setter method for confcidade.
     *
     * @param aConfcidade the new value for confcidade
     */
    public void setConfcidade(Set<Confcidade> aConfcidade) {
        confcidade = aConfcidade;
    }

    /**
     * Access method for confpais.
     *
     * @return the current value of confpais
     */
    public Confpais getConfpais() {
        return confpais;
    }

    /**
     * Setter method for confpais.
     *
     * @param aConfpais the new value for confpais
     */
    public void setConfpais(Confpais aConfpais) {
        confpais = aConfpais;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Set<Folcolaborador> getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Set<Folcolaborador> aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Access method for folsalminimo.
     *
     * @return the current value of folsalminimo
     */
    public Set<Folsalminimo> getFolsalminimo() {
        return folsalminimo;
    }

    /**
     * Setter method for folsalminimo.
     *
     * @param aFolsalminimo the new value for folsalminimo
     */
    public void setFolsalminimo(Set<Folsalminimo> aFolsalminimo) {
        folsalminimo = aFolsalminimo;
    }

    /**
     * Access method for impicmsst.
     *
     * @return the current value of impicmsst
     */
    public Set<Impicmsst> getImpicmsst() {
        return impicmsst;
    }

    /**
     * Setter method for impicmsst.
     *
     * @param aImpicmsst the new value for impicmsst
     */
    public void setImpicmsst(Set<Impicmsst> aImpicmsst) {
        impicmsst = aImpicmsst;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for impicms.
     *
     * @return the current value of impicms
     */
    public Impicms getImpicms() {
        return impicms;
    }

    /**
     * Setter method for impicms.
     *
     * @param aImpicms the new value for impicms
     */
    public void setImpicms(Impicms aImpicms) {
        impicms = aImpicms;
    }

    /**
     * Compares the key for this instance with another Confuf.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confuf and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confuf)) {
            return false;
        }
        Confuf that = (Confuf) other;
        Object myCodconfuf = this.getCodconfuf();
        Object yourCodconfuf = that.getCodconfuf();
        if (myCodconfuf==null ? yourCodconfuf!=null : !myCodconfuf.equals(yourCodconfuf)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confuf.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confuf)) return false;
        return this.equalKeys(other) && ((Confuf)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodconfuf() == null) {
            i = 0;
        } else {
            i = getCodconfuf().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confuf |");
        sb.append(" codconfuf=").append(getCodconfuf());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfuf", getCodconfuf());
        return ret;
    }

}
