package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CONFUSUARIO")
public class Confusuario implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfusuario";

    @Id
    @Column(name="CODCONFUSUARIO", unique=true, nullable=false, length=20)
    private String codconfusuario;
    @Column(name="NOMECONFUSUARIO", nullable=false, length=250)
    private String nomeconfusuario;
    @Column(name="PASSCONFUSUARIO", length=250)
    private String passconfusuario;
    @Column(name="CONFUSUARIOTIPOCOD", precision=10)
    private int confusuariotipocod;
    @Column(name="ADMINISTRADOR", precision=5)
    private short administrador;
    @Column(name="EMAILHOST", length=250)
    private String emailhost;
    @Column(name="EMAILNOME", length=250)
    private String emailnome;
    @Column(name="EMAILLOGIN", length=250)
    private String emaillogin;
    @Column(name="EMAILSENHA", length=250)
    private String emailsenha;
    @Column(name="EMAILSSL", precision=5)
    private short emailssl;
    @Column(name="EMAILSEG", precision=5)
    private short emailseg;
    @Column(name="EMAILCOPIA", precision=5)
    private short emailcopia;
    @Column(name="EMAILPORTA", precision=5)
    private short emailporta;
    @OneToMany(mappedBy="confusuario")
    private Set<Aglimcreditosol> aglimcreditosol;
    @OneToMany(mappedBy="confusuario2")
    private Set<Aglimcreditosol> aglimcreditosol2;
    @OneToMany(mappedBy="confusuario")
    private Set<Conflog> conflog;
    @OneToMany(mappedBy="confusuario")
    private Set<Crmchamadoevento> crmchamadoevento;
    @OneToMany(mappedBy="confusuario")
    private Set<Crmchamadonota> crmchamadonota;
    @OneToMany(mappedBy="confusuario")
    private Set<Crmpesquisa> crmpesquisa;
    @OneToMany(mappedBy="confusuario")
    private Set<Ctbbem> ctbbem;
    @OneToMany(mappedBy="confusuario")
    private Set<Ctbfechamento> ctbfechamento;
    @OneToMany(mappedBy="confusuario")
    private Set<Ecarquivamento> ecarquivamento;
    @OneToMany(mappedBy="confusuario2")
    private Set<Ecarquivamento> ecarquivamento2;
    @OneToMany(mappedBy="confusuario")
    private Set<Eccheckagenda> eccheckagenda;
    @OneToMany(mappedBy="confusuario2")
    private Set<Eccheckagenda> eccheckagenda2;
    @OneToMany(mappedBy="confusuario")
    private Set<Eccheckstatus> eccheckstatus;
    @OneToMany(mappedBy="confusuario2")
    private Set<Eccheckstatus> eccheckstatus2;
    @OneToMany(mappedBy="confusuario")
    private Set<Oporc> oporc;
    @OneToMany(mappedBy="confusuario")
    private Set<Opsolicitacao> opsolicitacao;
    @OneToMany(mappedBy="confusuario2")
    private Set<Opsolicitacao> opsolicitacao2;
    @OneToMany(mappedBy="confusuario")
    private Set<Parklancamento> parklancamento;
    @OneToMany(mappedBy="confusuario")
    private Set<Prdcordem> prdcordem;
    @OneToMany(mappedBy="confusuario")
    private Set<Projprojeto> projprojeto;
    @OneToMany(mappedBy="confusuario")
    private Set<Projprojetodet> projprojetodet;
    @OneToMany(mappedBy="confusuario")
    private Set<Projprojetousuario> projprojetousuario;
    @OneToMany(mappedBy="confusuario")
    private Set<Segrestricfintipo> segrestricfintipo;
    @OneToMany(mappedBy="confusuario")
    private Set<Segrestricmenu> segrestricmenu;
    @OneToMany(mappedBy="confusuario")
    private Set<Segrestricoptipo> segrestricoptipo;
    @OneToMany(mappedBy="confusuario")
    private Set<Wffilareg> wffilareg;
    @OneToMany(mappedBy="confusuario2")
    private Set<Wffilareg> wffilareg2;
    @OneToMany(mappedBy="confusuario")
    private Set<Wffilaseg> wffilaseg;

    /** Default constructor. */
    public Confusuario() {
        super();
    }

    /**
     * Access method for codconfusuario.
     *
     * @return the current value of codconfusuario
     */
    public String getCodconfusuario() {
        return codconfusuario;
    }

    /**
     * Setter method for codconfusuario.
     *
     * @param aCodconfusuario the new value for codconfusuario
     */
    public void setCodconfusuario(String aCodconfusuario) {
        codconfusuario = aCodconfusuario;
    }

    /**
     * Access method for nomeconfusuario.
     *
     * @return the current value of nomeconfusuario
     */
    public String getNomeconfusuario() {
        return nomeconfusuario;
    }

    /**
     * Setter method for nomeconfusuario.
     *
     * @param aNomeconfusuario the new value for nomeconfusuario
     */
    public void setNomeconfusuario(String aNomeconfusuario) {
        nomeconfusuario = aNomeconfusuario;
    }

    /**
     * Access method for passconfusuario.
     *
     * @return the current value of passconfusuario
     */
    public String getPassconfusuario() {
        return passconfusuario;
    }

    /**
     * Setter method for passconfusuario.
     *
     * @param aPassconfusuario the new value for passconfusuario
     */
    public void setPassconfusuario(String aPassconfusuario) {
        passconfusuario = aPassconfusuario;
    }

    /**
     * Access method for confusuariotipocod.
     *
     * @return the current value of confusuariotipocod
     */
    public int getConfusuariotipocod() {
        return confusuariotipocod;
    }

    /**
     * Setter method for confusuariotipocod.
     *
     * @param aConfusuariotipocod the new value for confusuariotipocod
     */
    public void setConfusuariotipocod(int aConfusuariotipocod) {
        confusuariotipocod = aConfusuariotipocod;
    }

    /**
     * Access method for administrador.
     *
     * @return the current value of administrador
     */
    public short getAdministrador() {
        return administrador;
    }

    /**
     * Setter method for administrador.
     *
     * @param aAdministrador the new value for administrador
     */
    public void setAdministrador(short aAdministrador) {
        administrador = aAdministrador;
    }

    /**
     * Access method for emailhost.
     *
     * @return the current value of emailhost
     */
    public String getEmailhost() {
        return emailhost;
    }

    /**
     * Setter method for emailhost.
     *
     * @param aEmailhost the new value for emailhost
     */
    public void setEmailhost(String aEmailhost) {
        emailhost = aEmailhost;
    }

    /**
     * Access method for emailnome.
     *
     * @return the current value of emailnome
     */
    public String getEmailnome() {
        return emailnome;
    }

    /**
     * Setter method for emailnome.
     *
     * @param aEmailnome the new value for emailnome
     */
    public void setEmailnome(String aEmailnome) {
        emailnome = aEmailnome;
    }

    /**
     * Access method for emaillogin.
     *
     * @return the current value of emaillogin
     */
    public String getEmaillogin() {
        return emaillogin;
    }

    /**
     * Setter method for emaillogin.
     *
     * @param aEmaillogin the new value for emaillogin
     */
    public void setEmaillogin(String aEmaillogin) {
        emaillogin = aEmaillogin;
    }

    /**
     * Access method for emailsenha.
     *
     * @return the current value of emailsenha
     */
    public String getEmailsenha() {
        return emailsenha;
    }

    /**
     * Setter method for emailsenha.
     *
     * @param aEmailsenha the new value for emailsenha
     */
    public void setEmailsenha(String aEmailsenha) {
        emailsenha = aEmailsenha;
    }

    /**
     * Access method for emailssl.
     *
     * @return the current value of emailssl
     */
    public short getEmailssl() {
        return emailssl;
    }

    /**
     * Setter method for emailssl.
     *
     * @param aEmailssl the new value for emailssl
     */
    public void setEmailssl(short aEmailssl) {
        emailssl = aEmailssl;
    }

    /**
     * Access method for emailseg.
     *
     * @return the current value of emailseg
     */
    public short getEmailseg() {
        return emailseg;
    }

    /**
     * Setter method for emailseg.
     *
     * @param aEmailseg the new value for emailseg
     */
    public void setEmailseg(short aEmailseg) {
        emailseg = aEmailseg;
    }

    /**
     * Access method for emailcopia.
     *
     * @return the current value of emailcopia
     */
    public short getEmailcopia() {
        return emailcopia;
    }

    /**
     * Setter method for emailcopia.
     *
     * @param aEmailcopia the new value for emailcopia
     */
    public void setEmailcopia(short aEmailcopia) {
        emailcopia = aEmailcopia;
    }

    /**
     * Access method for emailporta.
     *
     * @return the current value of emailporta
     */
    public short getEmailporta() {
        return emailporta;
    }

    /**
     * Setter method for emailporta.
     *
     * @param aEmailporta the new value for emailporta
     */
    public void setEmailporta(short aEmailporta) {
        emailporta = aEmailporta;
    }

    /**
     * Access method for aglimcreditosol.
     *
     * @return the current value of aglimcreditosol
     */
    public Set<Aglimcreditosol> getAglimcreditosol() {
        return aglimcreditosol;
    }

    /**
     * Setter method for aglimcreditosol.
     *
     * @param aAglimcreditosol the new value for aglimcreditosol
     */
    public void setAglimcreditosol(Set<Aglimcreditosol> aAglimcreditosol) {
        aglimcreditosol = aAglimcreditosol;
    }

    /**
     * Access method for aglimcreditosol2.
     *
     * @return the current value of aglimcreditosol2
     */
    public Set<Aglimcreditosol> getAglimcreditosol2() {
        return aglimcreditosol2;
    }

    /**
     * Setter method for aglimcreditosol2.
     *
     * @param aAglimcreditosol2 the new value for aglimcreditosol2
     */
    public void setAglimcreditosol2(Set<Aglimcreditosol> aAglimcreditosol2) {
        aglimcreditosol2 = aAglimcreditosol2;
    }

    /**
     * Access method for conflog.
     *
     * @return the current value of conflog
     */
    public Set<Conflog> getConflog() {
        return conflog;
    }

    /**
     * Setter method for conflog.
     *
     * @param aConflog the new value for conflog
     */
    public void setConflog(Set<Conflog> aConflog) {
        conflog = aConflog;
    }

    /**
     * Access method for crmchamadoevento.
     *
     * @return the current value of crmchamadoevento
     */
    public Set<Crmchamadoevento> getCrmchamadoevento() {
        return crmchamadoevento;
    }

    /**
     * Setter method for crmchamadoevento.
     *
     * @param aCrmchamadoevento the new value for crmchamadoevento
     */
    public void setCrmchamadoevento(Set<Crmchamadoevento> aCrmchamadoevento) {
        crmchamadoevento = aCrmchamadoevento;
    }

    /**
     * Access method for crmchamadonota.
     *
     * @return the current value of crmchamadonota
     */
    public Set<Crmchamadonota> getCrmchamadonota() {
        return crmchamadonota;
    }

    /**
     * Setter method for crmchamadonota.
     *
     * @param aCrmchamadonota the new value for crmchamadonota
     */
    public void setCrmchamadonota(Set<Crmchamadonota> aCrmchamadonota) {
        crmchamadonota = aCrmchamadonota;
    }

    /**
     * Access method for crmpesquisa.
     *
     * @return the current value of crmpesquisa
     */
    public Set<Crmpesquisa> getCrmpesquisa() {
        return crmpesquisa;
    }

    /**
     * Setter method for crmpesquisa.
     *
     * @param aCrmpesquisa the new value for crmpesquisa
     */
    public void setCrmpesquisa(Set<Crmpesquisa> aCrmpesquisa) {
        crmpesquisa = aCrmpesquisa;
    }

    /**
     * Access method for ctbbem.
     *
     * @return the current value of ctbbem
     */
    public Set<Ctbbem> getCtbbem() {
        return ctbbem;
    }

    /**
     * Setter method for ctbbem.
     *
     * @param aCtbbem the new value for ctbbem
     */
    public void setCtbbem(Set<Ctbbem> aCtbbem) {
        ctbbem = aCtbbem;
    }

    /**
     * Access method for ctbfechamento.
     *
     * @return the current value of ctbfechamento
     */
    public Set<Ctbfechamento> getCtbfechamento() {
        return ctbfechamento;
    }

    /**
     * Setter method for ctbfechamento.
     *
     * @param aCtbfechamento the new value for ctbfechamento
     */
    public void setCtbfechamento(Set<Ctbfechamento> aCtbfechamento) {
        ctbfechamento = aCtbfechamento;
    }

    /**
     * Access method for ecarquivamento.
     *
     * @return the current value of ecarquivamento
     */
    public Set<Ecarquivamento> getEcarquivamento() {
        return ecarquivamento;
    }

    /**
     * Setter method for ecarquivamento.
     *
     * @param aEcarquivamento the new value for ecarquivamento
     */
    public void setEcarquivamento(Set<Ecarquivamento> aEcarquivamento) {
        ecarquivamento = aEcarquivamento;
    }

    /**
     * Access method for ecarquivamento2.
     *
     * @return the current value of ecarquivamento2
     */
    public Set<Ecarquivamento> getEcarquivamento2() {
        return ecarquivamento2;
    }

    /**
     * Setter method for ecarquivamento2.
     *
     * @param aEcarquivamento2 the new value for ecarquivamento2
     */
    public void setEcarquivamento2(Set<Ecarquivamento> aEcarquivamento2) {
        ecarquivamento2 = aEcarquivamento2;
    }

    /**
     * Access method for eccheckagenda.
     *
     * @return the current value of eccheckagenda
     */
    public Set<Eccheckagenda> getEccheckagenda() {
        return eccheckagenda;
    }

    /**
     * Setter method for eccheckagenda.
     *
     * @param aEccheckagenda the new value for eccheckagenda
     */
    public void setEccheckagenda(Set<Eccheckagenda> aEccheckagenda) {
        eccheckagenda = aEccheckagenda;
    }

    /**
     * Access method for eccheckagenda2.
     *
     * @return the current value of eccheckagenda2
     */
    public Set<Eccheckagenda> getEccheckagenda2() {
        return eccheckagenda2;
    }

    /**
     * Setter method for eccheckagenda2.
     *
     * @param aEccheckagenda2 the new value for eccheckagenda2
     */
    public void setEccheckagenda2(Set<Eccheckagenda> aEccheckagenda2) {
        eccheckagenda2 = aEccheckagenda2;
    }

    /**
     * Access method for eccheckstatus.
     *
     * @return the current value of eccheckstatus
     */
    public Set<Eccheckstatus> getEccheckstatus() {
        return eccheckstatus;
    }

    /**
     * Setter method for eccheckstatus.
     *
     * @param aEccheckstatus the new value for eccheckstatus
     */
    public void setEccheckstatus(Set<Eccheckstatus> aEccheckstatus) {
        eccheckstatus = aEccheckstatus;
    }

    /**
     * Access method for eccheckstatus2.
     *
     * @return the current value of eccheckstatus2
     */
    public Set<Eccheckstatus> getEccheckstatus2() {
        return eccheckstatus2;
    }

    /**
     * Setter method for eccheckstatus2.
     *
     * @param aEccheckstatus2 the new value for eccheckstatus2
     */
    public void setEccheckstatus2(Set<Eccheckstatus> aEccheckstatus2) {
        eccheckstatus2 = aEccheckstatus2;
    }

    /**
     * Access method for oporc.
     *
     * @return the current value of oporc
     */
    public Set<Oporc> getOporc() {
        return oporc;
    }

    /**
     * Setter method for oporc.
     *
     * @param aOporc the new value for oporc
     */
    public void setOporc(Set<Oporc> aOporc) {
        oporc = aOporc;
    }

    /**
     * Access method for opsolicitacao.
     *
     * @return the current value of opsolicitacao
     */
    public Set<Opsolicitacao> getOpsolicitacao() {
        return opsolicitacao;
    }

    /**
     * Setter method for opsolicitacao.
     *
     * @param aOpsolicitacao the new value for opsolicitacao
     */
    public void setOpsolicitacao(Set<Opsolicitacao> aOpsolicitacao) {
        opsolicitacao = aOpsolicitacao;
    }

    /**
     * Access method for opsolicitacao2.
     *
     * @return the current value of opsolicitacao2
     */
    public Set<Opsolicitacao> getOpsolicitacao2() {
        return opsolicitacao2;
    }

    /**
     * Setter method for opsolicitacao2.
     *
     * @param aOpsolicitacao2 the new value for opsolicitacao2
     */
    public void setOpsolicitacao2(Set<Opsolicitacao> aOpsolicitacao2) {
        opsolicitacao2 = aOpsolicitacao2;
    }

    /**
     * Access method for parklancamento.
     *
     * @return the current value of parklancamento
     */
    public Set<Parklancamento> getParklancamento() {
        return parklancamento;
    }

    /**
     * Setter method for parklancamento.
     *
     * @param aParklancamento the new value for parklancamento
     */
    public void setParklancamento(Set<Parklancamento> aParklancamento) {
        parklancamento = aParklancamento;
    }

    /**
     * Access method for prdcordem.
     *
     * @return the current value of prdcordem
     */
    public Set<Prdcordem> getPrdcordem() {
        return prdcordem;
    }

    /**
     * Setter method for prdcordem.
     *
     * @param aPrdcordem the new value for prdcordem
     */
    public void setPrdcordem(Set<Prdcordem> aPrdcordem) {
        prdcordem = aPrdcordem;
    }

    /**
     * Access method for projprojeto.
     *
     * @return the current value of projprojeto
     */
    public Set<Projprojeto> getProjprojeto() {
        return projprojeto;
    }

    /**
     * Setter method for projprojeto.
     *
     * @param aProjprojeto the new value for projprojeto
     */
    public void setProjprojeto(Set<Projprojeto> aProjprojeto) {
        projprojeto = aProjprojeto;
    }

    /**
     * Access method for projprojetodet.
     *
     * @return the current value of projprojetodet
     */
    public Set<Projprojetodet> getProjprojetodet() {
        return projprojetodet;
    }

    /**
     * Setter method for projprojetodet.
     *
     * @param aProjprojetodet the new value for projprojetodet
     */
    public void setProjprojetodet(Set<Projprojetodet> aProjprojetodet) {
        projprojetodet = aProjprojetodet;
    }

    /**
     * Access method for projprojetousuario.
     *
     * @return the current value of projprojetousuario
     */
    public Set<Projprojetousuario> getProjprojetousuario() {
        return projprojetousuario;
    }

    /**
     * Setter method for projprojetousuario.
     *
     * @param aProjprojetousuario the new value for projprojetousuario
     */
    public void setProjprojetousuario(Set<Projprojetousuario> aProjprojetousuario) {
        projprojetousuario = aProjprojetousuario;
    }

    /**
     * Access method for segrestricfintipo.
     *
     * @return the current value of segrestricfintipo
     */
    public Set<Segrestricfintipo> getSegrestricfintipo() {
        return segrestricfintipo;
    }

    /**
     * Setter method for segrestricfintipo.
     *
     * @param aSegrestricfintipo the new value for segrestricfintipo
     */
    public void setSegrestricfintipo(Set<Segrestricfintipo> aSegrestricfintipo) {
        segrestricfintipo = aSegrestricfintipo;
    }

    /**
     * Access method for segrestricmenu.
     *
     * @return the current value of segrestricmenu
     */
    public Set<Segrestricmenu> getSegrestricmenu() {
        return segrestricmenu;
    }

    /**
     * Setter method for segrestricmenu.
     *
     * @param aSegrestricmenu the new value for segrestricmenu
     */
    public void setSegrestricmenu(Set<Segrestricmenu> aSegrestricmenu) {
        segrestricmenu = aSegrestricmenu;
    }

    /**
     * Access method for segrestricoptipo.
     *
     * @return the current value of segrestricoptipo
     */
    public Set<Segrestricoptipo> getSegrestricoptipo() {
        return segrestricoptipo;
    }

    /**
     * Setter method for segrestricoptipo.
     *
     * @param aSegrestricoptipo the new value for segrestricoptipo
     */
    public void setSegrestricoptipo(Set<Segrestricoptipo> aSegrestricoptipo) {
        segrestricoptipo = aSegrestricoptipo;
    }

    /**
     * Access method for wffilareg.
     *
     * @return the current value of wffilareg
     */
    public Set<Wffilareg> getWffilareg() {
        return wffilareg;
    }

    /**
     * Setter method for wffilareg.
     *
     * @param aWffilareg the new value for wffilareg
     */
    public void setWffilareg(Set<Wffilareg> aWffilareg) {
        wffilareg = aWffilareg;
    }

    /**
     * Access method for wffilareg2.
     *
     * @return the current value of wffilareg2
     */
    public Set<Wffilareg> getWffilareg2() {
        return wffilareg2;
    }

    /**
     * Setter method for wffilareg2.
     *
     * @param aWffilareg2 the new value for wffilareg2
     */
    public void setWffilareg2(Set<Wffilareg> aWffilareg2) {
        wffilareg2 = aWffilareg2;
    }

    /**
     * Access method for wffilaseg.
     *
     * @return the current value of wffilaseg
     */
    public Set<Wffilaseg> getWffilaseg() {
        return wffilaseg;
    }

    /**
     * Setter method for wffilaseg.
     *
     * @param aWffilaseg the new value for wffilaseg
     */
    public void setWffilaseg(Set<Wffilaseg> aWffilaseg) {
        wffilaseg = aWffilaseg;
    }

    /**
     * Compares the key for this instance with another Confusuario.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confusuario and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confusuario)) {
            return false;
        }
        Confusuario that = (Confusuario) other;
        Object myCodconfusuario = this.getCodconfusuario();
        Object yourCodconfusuario = that.getCodconfusuario();
        if (myCodconfusuario==null ? yourCodconfusuario!=null : !myCodconfusuario.equals(yourCodconfusuario)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confusuario.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confusuario)) return false;
        return this.equalKeys(other) && ((Confusuario)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodconfusuario() == null) {
            i = 0;
        } else {
            i = getCodconfusuario().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confusuario |");
        sb.append(" codconfusuario=").append(getCodconfusuario());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfusuario", getCodconfusuario());
        return ret;
    }

}
