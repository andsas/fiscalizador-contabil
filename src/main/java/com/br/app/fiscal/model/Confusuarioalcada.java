package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CONFUSUARIOALCADA")
public class Confusuarioalcada implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfusuarioalcada";

    @Id
    @Column(name="CODCONFUSUARIOALCADA", unique=true, nullable=false, precision=10)
    private int codconfusuarioalcada;
    @Column(name="DESCCONFUSUARIOALCADA", nullable=false, length=250)
    private String descconfusuarioalcada;
    @OneToMany(mappedBy="confusuarioalcada")
    private Set<Confusuariotipo> confusuariotipo;
    @OneToMany(mappedBy="confusuarioalcada")
    private Set<Segrestricfintipo> segrestricfintipo;
    @OneToMany(mappedBy="confusuarioalcada")
    private Set<Segrestricmenu> segrestricmenu;
    @OneToMany(mappedBy="confusuarioalcada")
    private Set<Segrestricoptipo> segrestricoptipo;
    @OneToMany(mappedBy="confusuarioalcada")
    private Set<Wffilaseg> wffilaseg;

    /** Default constructor. */
    public Confusuarioalcada() {
        super();
    }

    /**
     * Access method for codconfusuarioalcada.
     *
     * @return the current value of codconfusuarioalcada
     */
    public int getCodconfusuarioalcada() {
        return codconfusuarioalcada;
    }

    /**
     * Setter method for codconfusuarioalcada.
     *
     * @param aCodconfusuarioalcada the new value for codconfusuarioalcada
     */
    public void setCodconfusuarioalcada(int aCodconfusuarioalcada) {
        codconfusuarioalcada = aCodconfusuarioalcada;
    }

    /**
     * Access method for descconfusuarioalcada.
     *
     * @return the current value of descconfusuarioalcada
     */
    public String getDescconfusuarioalcada() {
        return descconfusuarioalcada;
    }

    /**
     * Setter method for descconfusuarioalcada.
     *
     * @param aDescconfusuarioalcada the new value for descconfusuarioalcada
     */
    public void setDescconfusuarioalcada(String aDescconfusuarioalcada) {
        descconfusuarioalcada = aDescconfusuarioalcada;
    }

    /**
     * Access method for confusuariotipo.
     *
     * @return the current value of confusuariotipo
     */
    public Set<Confusuariotipo> getConfusuariotipo() {
        return confusuariotipo;
    }

    /**
     * Setter method for confusuariotipo.
     *
     * @param aConfusuariotipo the new value for confusuariotipo
     */
    public void setConfusuariotipo(Set<Confusuariotipo> aConfusuariotipo) {
        confusuariotipo = aConfusuariotipo;
    }

    /**
     * Access method for segrestricfintipo.
     *
     * @return the current value of segrestricfintipo
     */
    public Set<Segrestricfintipo> getSegrestricfintipo() {
        return segrestricfintipo;
    }

    /**
     * Setter method for segrestricfintipo.
     *
     * @param aSegrestricfintipo the new value for segrestricfintipo
     */
    public void setSegrestricfintipo(Set<Segrestricfintipo> aSegrestricfintipo) {
        segrestricfintipo = aSegrestricfintipo;
    }

    /**
     * Access method for segrestricmenu.
     *
     * @return the current value of segrestricmenu
     */
    public Set<Segrestricmenu> getSegrestricmenu() {
        return segrestricmenu;
    }

    /**
     * Setter method for segrestricmenu.
     *
     * @param aSegrestricmenu the new value for segrestricmenu
     */
    public void setSegrestricmenu(Set<Segrestricmenu> aSegrestricmenu) {
        segrestricmenu = aSegrestricmenu;
    }

    /**
     * Access method for segrestricoptipo.
     *
     * @return the current value of segrestricoptipo
     */
    public Set<Segrestricoptipo> getSegrestricoptipo() {
        return segrestricoptipo;
    }

    /**
     * Setter method for segrestricoptipo.
     *
     * @param aSegrestricoptipo the new value for segrestricoptipo
     */
    public void setSegrestricoptipo(Set<Segrestricoptipo> aSegrestricoptipo) {
        segrestricoptipo = aSegrestricoptipo;
    }

    /**
     * Access method for wffilaseg.
     *
     * @return the current value of wffilaseg
     */
    public Set<Wffilaseg> getWffilaseg() {
        return wffilaseg;
    }

    /**
     * Setter method for wffilaseg.
     *
     * @param aWffilaseg the new value for wffilaseg
     */
    public void setWffilaseg(Set<Wffilaseg> aWffilaseg) {
        wffilaseg = aWffilaseg;
    }

    /**
     * Compares the key for this instance with another Confusuarioalcada.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confusuarioalcada and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confusuarioalcada)) {
            return false;
        }
        Confusuarioalcada that = (Confusuarioalcada) other;
        if (this.getCodconfusuarioalcada() != that.getCodconfusuarioalcada()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confusuarioalcada.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confusuarioalcada)) return false;
        return this.equalKeys(other) && ((Confusuarioalcada)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfusuarioalcada();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confusuarioalcada |");
        sb.append(" codconfusuarioalcada=").append(getCodconfusuarioalcada());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfusuarioalcada", Integer.valueOf(getCodconfusuarioalcada()));
        return ret;
    }

}
