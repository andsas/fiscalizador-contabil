package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CONFUSUARIOTIPO")
public class Confusuariotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codconfusuariotipo";

    @Id
    @Column(name="CODCONFUSUARIOTIPO", unique=true, nullable=false, precision=10)
    private int codconfusuariotipo;
    @Column(name="DESCCONFUSUARIOTIPO", nullable=false, length=250)
    private String descconfusuariotipo;
    @Column(name="CORCONFUSUARIOTIPO", nullable=false, precision=10)
    private int corconfusuariotipo;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIOALCADA")
    private Confusuarioalcada confusuarioalcada;
    @OneToMany(mappedBy="confusuariotipo")
    private Set<Opsolicitacao> opsolicitacao;
    @OneToMany(mappedBy="confusuariotipo")
    private Set<Segrestricfintipo> segrestricfintipo;
    @OneToMany(mappedBy="confusuariotipo")
    private Set<Segrestricmenu> segrestricmenu;
    @OneToMany(mappedBy="confusuariotipo")
    private Set<Segrestricoptipo> segrestricoptipo;
    @OneToMany(mappedBy="confusuariotipo")
    private Set<Wffilaseg> wffilaseg;

    /** Default constructor. */
    public Confusuariotipo() {
        super();
    }

    /**
     * Access method for codconfusuariotipo.
     *
     * @return the current value of codconfusuariotipo
     */
    public int getCodconfusuariotipo() {
        return codconfusuariotipo;
    }

    /**
     * Setter method for codconfusuariotipo.
     *
     * @param aCodconfusuariotipo the new value for codconfusuariotipo
     */
    public void setCodconfusuariotipo(int aCodconfusuariotipo) {
        codconfusuariotipo = aCodconfusuariotipo;
    }

    /**
     * Access method for descconfusuariotipo.
     *
     * @return the current value of descconfusuariotipo
     */
    public String getDescconfusuariotipo() {
        return descconfusuariotipo;
    }

    /**
     * Setter method for descconfusuariotipo.
     *
     * @param aDescconfusuariotipo the new value for descconfusuariotipo
     */
    public void setDescconfusuariotipo(String aDescconfusuariotipo) {
        descconfusuariotipo = aDescconfusuariotipo;
    }

    /**
     * Access method for corconfusuariotipo.
     *
     * @return the current value of corconfusuariotipo
     */
    public int getCorconfusuariotipo() {
        return corconfusuariotipo;
    }

    /**
     * Setter method for corconfusuariotipo.
     *
     * @param aCorconfusuariotipo the new value for corconfusuariotipo
     */
    public void setCorconfusuariotipo(int aCorconfusuariotipo) {
        corconfusuariotipo = aCorconfusuariotipo;
    }

    /**
     * Access method for confusuarioalcada.
     *
     * @return the current value of confusuarioalcada
     */
    public Confusuarioalcada getConfusuarioalcada() {
        return confusuarioalcada;
    }

    /**
     * Setter method for confusuarioalcada.
     *
     * @param aConfusuarioalcada the new value for confusuarioalcada
     */
    public void setConfusuarioalcada(Confusuarioalcada aConfusuarioalcada) {
        confusuarioalcada = aConfusuarioalcada;
    }

    /**
     * Access method for opsolicitacao.
     *
     * @return the current value of opsolicitacao
     */
    public Set<Opsolicitacao> getOpsolicitacao() {
        return opsolicitacao;
    }

    /**
     * Setter method for opsolicitacao.
     *
     * @param aOpsolicitacao the new value for opsolicitacao
     */
    public void setOpsolicitacao(Set<Opsolicitacao> aOpsolicitacao) {
        opsolicitacao = aOpsolicitacao;
    }

    /**
     * Access method for segrestricfintipo.
     *
     * @return the current value of segrestricfintipo
     */
    public Set<Segrestricfintipo> getSegrestricfintipo() {
        return segrestricfintipo;
    }

    /**
     * Setter method for segrestricfintipo.
     *
     * @param aSegrestricfintipo the new value for segrestricfintipo
     */
    public void setSegrestricfintipo(Set<Segrestricfintipo> aSegrestricfintipo) {
        segrestricfintipo = aSegrestricfintipo;
    }

    /**
     * Access method for segrestricmenu.
     *
     * @return the current value of segrestricmenu
     */
    public Set<Segrestricmenu> getSegrestricmenu() {
        return segrestricmenu;
    }

    /**
     * Setter method for segrestricmenu.
     *
     * @param aSegrestricmenu the new value for segrestricmenu
     */
    public void setSegrestricmenu(Set<Segrestricmenu> aSegrestricmenu) {
        segrestricmenu = aSegrestricmenu;
    }

    /**
     * Access method for segrestricoptipo.
     *
     * @return the current value of segrestricoptipo
     */
    public Set<Segrestricoptipo> getSegrestricoptipo() {
        return segrestricoptipo;
    }

    /**
     * Setter method for segrestricoptipo.
     *
     * @param aSegrestricoptipo the new value for segrestricoptipo
     */
    public void setSegrestricoptipo(Set<Segrestricoptipo> aSegrestricoptipo) {
        segrestricoptipo = aSegrestricoptipo;
    }

    /**
     * Access method for wffilaseg.
     *
     * @return the current value of wffilaseg
     */
    public Set<Wffilaseg> getWffilaseg() {
        return wffilaseg;
    }

    /**
     * Setter method for wffilaseg.
     *
     * @param aWffilaseg the new value for wffilaseg
     */
    public void setWffilaseg(Set<Wffilaseg> aWffilaseg) {
        wffilaseg = aWffilaseg;
    }

    /**
     * Compares the key for this instance with another Confusuariotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Confusuariotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Confusuariotipo)) {
            return false;
        }
        Confusuariotipo that = (Confusuariotipo) other;
        if (this.getCodconfusuariotipo() != that.getCodconfusuariotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Confusuariotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Confusuariotipo)) return false;
        return this.equalKeys(other) && ((Confusuariotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodconfusuariotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Confusuariotipo |");
        sb.append(" codconfusuariotipo=").append(getCodconfusuariotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codconfusuariotipo", Integer.valueOf(getCodconfusuariotipo()));
        return ret;
    }

}
