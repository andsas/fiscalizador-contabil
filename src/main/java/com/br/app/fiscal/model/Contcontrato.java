package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CONTCONTRATO")
public class Contcontrato implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcontcontrato";

    @Id
    @Column(name="CODCONTCONTRATO", unique=true, nullable=false, precision=10)
    private int codcontcontrato;
    @Column(name="DESCCONTCONTRATO", nullable=false, length=250)
    private String desccontcontrato;
    @Column(name="IDINTERNA", length=40)
    private String idinterna;
    @Column(name="DATAINICIO", nullable=false)
    private Timestamp datainicio;
    @Column(name="DATATERMINO")
    private Timestamp datatermino;
    @Column(name="DATACANCELAMENTO")
    private Timestamp datacancelamento;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @Column(name="CANCELADO", precision=5)
    private short cancelado;
    @Column(name="MOTIVOCANCELAMENTO")
    private String motivocancelamento;
    @Column(name="OBS")
    private String obs;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGCONTRATANTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGCONTRATADO", nullable=false)
    private Agagente agagente2;
    @OneToMany(mappedBy="contcontrato")
    private Set<Contcontratocrmsla> contcontratocrmsla;
    @OneToMany(mappedBy="contcontrato")
    private Set<Crmchamado> crmchamado;
    @OneToMany(mappedBy="contcontrato")
    private Set<Finemprestimo> finemprestimo;

    /** Default constructor. */
    public Contcontrato() {
        super();
    }

    /**
     * Access method for codcontcontrato.
     *
     * @return the current value of codcontcontrato
     */
    public int getCodcontcontrato() {
        return codcontcontrato;
    }

    /**
     * Setter method for codcontcontrato.
     *
     * @param aCodcontcontrato the new value for codcontcontrato
     */
    public void setCodcontcontrato(int aCodcontcontrato) {
        codcontcontrato = aCodcontcontrato;
    }

    /**
     * Access method for desccontcontrato.
     *
     * @return the current value of desccontcontrato
     */
    public String getDesccontcontrato() {
        return desccontcontrato;
    }

    /**
     * Setter method for desccontcontrato.
     *
     * @param aDesccontcontrato the new value for desccontcontrato
     */
    public void setDesccontcontrato(String aDesccontcontrato) {
        desccontcontrato = aDesccontcontrato;
    }

    /**
     * Access method for idinterna.
     *
     * @return the current value of idinterna
     */
    public String getIdinterna() {
        return idinterna;
    }

    /**
     * Setter method for idinterna.
     *
     * @param aIdinterna the new value for idinterna
     */
    public void setIdinterna(String aIdinterna) {
        idinterna = aIdinterna;
    }

    /**
     * Access method for datainicio.
     *
     * @return the current value of datainicio
     */
    public Timestamp getDatainicio() {
        return datainicio;
    }

    /**
     * Setter method for datainicio.
     *
     * @param aDatainicio the new value for datainicio
     */
    public void setDatainicio(Timestamp aDatainicio) {
        datainicio = aDatainicio;
    }

    /**
     * Access method for datatermino.
     *
     * @return the current value of datatermino
     */
    public Timestamp getDatatermino() {
        return datatermino;
    }

    /**
     * Setter method for datatermino.
     *
     * @param aDatatermino the new value for datatermino
     */
    public void setDatatermino(Timestamp aDatatermino) {
        datatermino = aDatatermino;
    }

    /**
     * Access method for datacancelamento.
     *
     * @return the current value of datacancelamento
     */
    public Timestamp getDatacancelamento() {
        return datacancelamento;
    }

    /**
     * Setter method for datacancelamento.
     *
     * @param aDatacancelamento the new value for datacancelamento
     */
    public void setDatacancelamento(Timestamp aDatacancelamento) {
        datacancelamento = aDatacancelamento;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for cancelado.
     *
     * @return the current value of cancelado
     */
    public short getCancelado() {
        return cancelado;
    }

    /**
     * Setter method for cancelado.
     *
     * @param aCancelado the new value for cancelado
     */
    public void setCancelado(short aCancelado) {
        cancelado = aCancelado;
    }

    /**
     * Access method for motivocancelamento.
     *
     * @return the current value of motivocancelamento
     */
    public String getMotivocancelamento() {
        return motivocancelamento;
    }

    /**
     * Setter method for motivocancelamento.
     *
     * @param aMotivocancelamento the new value for motivocancelamento
     */
    public void setMotivocancelamento(String aMotivocancelamento) {
        motivocancelamento = aMotivocancelamento;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Agagente getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Agagente aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for contcontratocrmsla.
     *
     * @return the current value of contcontratocrmsla
     */
    public Set<Contcontratocrmsla> getContcontratocrmsla() {
        return contcontratocrmsla;
    }

    /**
     * Setter method for contcontratocrmsla.
     *
     * @param aContcontratocrmsla the new value for contcontratocrmsla
     */
    public void setContcontratocrmsla(Set<Contcontratocrmsla> aContcontratocrmsla) {
        contcontratocrmsla = aContcontratocrmsla;
    }

    /**
     * Access method for crmchamado.
     *
     * @return the current value of crmchamado
     */
    public Set<Crmchamado> getCrmchamado() {
        return crmchamado;
    }

    /**
     * Setter method for crmchamado.
     *
     * @param aCrmchamado the new value for crmchamado
     */
    public void setCrmchamado(Set<Crmchamado> aCrmchamado) {
        crmchamado = aCrmchamado;
    }

    /**
     * Access method for finemprestimo.
     *
     * @return the current value of finemprestimo
     */
    public Set<Finemprestimo> getFinemprestimo() {
        return finemprestimo;
    }

    /**
     * Setter method for finemprestimo.
     *
     * @param aFinemprestimo the new value for finemprestimo
     */
    public void setFinemprestimo(Set<Finemprestimo> aFinemprestimo) {
        finemprestimo = aFinemprestimo;
    }

    /**
     * Compares the key for this instance with another Contcontrato.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Contcontrato and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Contcontrato)) {
            return false;
        }
        Contcontrato that = (Contcontrato) other;
        if (this.getCodcontcontrato() != that.getCodcontcontrato()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Contcontrato.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Contcontrato)) return false;
        return this.equalKeys(other) && ((Contcontrato)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcontcontrato();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Contcontrato |");
        sb.append(" codcontcontrato=").append(getCodcontcontrato());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcontcontrato", Integer.valueOf(getCodcontcontrato()));
        return ret;
    }

}
