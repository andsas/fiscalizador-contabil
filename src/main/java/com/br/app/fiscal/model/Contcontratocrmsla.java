package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CONTCONTRATOCRMSLA")
public class Contcontratocrmsla implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcontcontratocrmsla";

    @Id
    @Column(name="CODCONTCONTRATOCRMSLA", unique=true, nullable=false, precision=10)
    private int codcontcontratocrmsla;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONTCONTRATO", nullable=false)
    private Contcontrato contcontrato;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCRMSLA", nullable=false)
    private Crmsla crmsla;

    /** Default constructor. */
    public Contcontratocrmsla() {
        super();
    }

    /**
     * Access method for codcontcontratocrmsla.
     *
     * @return the current value of codcontcontratocrmsla
     */
    public int getCodcontcontratocrmsla() {
        return codcontcontratocrmsla;
    }

    /**
     * Setter method for codcontcontratocrmsla.
     *
     * @param aCodcontcontratocrmsla the new value for codcontcontratocrmsla
     */
    public void setCodcontcontratocrmsla(int aCodcontcontratocrmsla) {
        codcontcontratocrmsla = aCodcontcontratocrmsla;
    }

    /**
     * Access method for contcontrato.
     *
     * @return the current value of contcontrato
     */
    public Contcontrato getContcontrato() {
        return contcontrato;
    }

    /**
     * Setter method for contcontrato.
     *
     * @param aContcontrato the new value for contcontrato
     */
    public void setContcontrato(Contcontrato aContcontrato) {
        contcontrato = aContcontrato;
    }

    /**
     * Access method for crmsla.
     *
     * @return the current value of crmsla
     */
    public Crmsla getCrmsla() {
        return crmsla;
    }

    /**
     * Setter method for crmsla.
     *
     * @param aCrmsla the new value for crmsla
     */
    public void setCrmsla(Crmsla aCrmsla) {
        crmsla = aCrmsla;
    }

    /**
     * Compares the key for this instance with another Contcontratocrmsla.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Contcontratocrmsla and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Contcontratocrmsla)) {
            return false;
        }
        Contcontratocrmsla that = (Contcontratocrmsla) other;
        if (this.getCodcontcontratocrmsla() != that.getCodcontcontratocrmsla()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Contcontratocrmsla.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Contcontratocrmsla)) return false;
        return this.equalKeys(other) && ((Contcontratocrmsla)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcontcontratocrmsla();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Contcontratocrmsla |");
        sb.append(" codcontcontratocrmsla=").append(getCodcontcontratocrmsla());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcontcontratocrmsla", Integer.valueOf(getCodcontcontratocrmsla()));
        return ret;
    }

}
