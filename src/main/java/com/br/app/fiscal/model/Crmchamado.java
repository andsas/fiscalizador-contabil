package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CRMCHAMADO")
public class Crmchamado implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmchamado";

    @Id
    @Column(name="CODCRMCHAMADO", unique=true, nullable=false, precision=10)
    private int codcrmchamado;
    @Column(name="DESCCRMCHAMADO", nullable=false, length=250)
    private String desccrmchamado;
    @Column(name="DATA")
    private Timestamp data;
    @Column(name="ULTIMOEVENTODATA")
    private Timestamp ultimoeventodata;
    @Column(name="PRIMEIROEVENTODATA")
    private Timestamp primeiroeventodata;
    @Column(name="IDCRMCHAMADO", length=60)
    private String idcrmchamado;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @Column(name="CODUSUCADASTRO", length=20)
    private String codusucadastro;
    @Column(name="CODUSUATUAL", length=20)
    private String codusuatual;
    @Column(name="FINALIZADO", precision=5)
    private short finalizado;
    @Column(name="CANCELADO", precision=5)
    private short cancelado;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAO")
    private Optransacao optransacao;
    @ManyToOne
    @JoinColumn(name="CODCONTCONTRATO")
    private Contcontrato contcontrato;
    @ManyToOne
    @JoinColumn(name="CODULTIMOEVENTO")
    private Crmevento crmevento;
    @ManyToOne
    @JoinColumn(name="CODPRIMEIROEVENTO")
    private Crmevento crmevento2;
    @OneToMany(mappedBy="crmchamado")
    private Set<Crmchamadoevento> crmchamadoevento;
    @OneToMany(mappedBy="crmchamado")
    private Set<Crmchamadonota> crmchamadonota;

    /** Default constructor. */
    public Crmchamado() {
        super();
    }

    /**
     * Access method for codcrmchamado.
     *
     * @return the current value of codcrmchamado
     */
    public int getCodcrmchamado() {
        return codcrmchamado;
    }

    /**
     * Setter method for codcrmchamado.
     *
     * @param aCodcrmchamado the new value for codcrmchamado
     */
    public void setCodcrmchamado(int aCodcrmchamado) {
        codcrmchamado = aCodcrmchamado;
    }

    /**
     * Access method for desccrmchamado.
     *
     * @return the current value of desccrmchamado
     */
    public String getDesccrmchamado() {
        return desccrmchamado;
    }

    /**
     * Setter method for desccrmchamado.
     *
     * @param aDesccrmchamado the new value for desccrmchamado
     */
    public void setDesccrmchamado(String aDesccrmchamado) {
        desccrmchamado = aDesccrmchamado;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for ultimoeventodata.
     *
     * @return the current value of ultimoeventodata
     */
    public Timestamp getUltimoeventodata() {
        return ultimoeventodata;
    }

    /**
     * Setter method for ultimoeventodata.
     *
     * @param aUltimoeventodata the new value for ultimoeventodata
     */
    public void setUltimoeventodata(Timestamp aUltimoeventodata) {
        ultimoeventodata = aUltimoeventodata;
    }

    /**
     * Access method for primeiroeventodata.
     *
     * @return the current value of primeiroeventodata
     */
    public Timestamp getPrimeiroeventodata() {
        return primeiroeventodata;
    }

    /**
     * Setter method for primeiroeventodata.
     *
     * @param aPrimeiroeventodata the new value for primeiroeventodata
     */
    public void setPrimeiroeventodata(Timestamp aPrimeiroeventodata) {
        primeiroeventodata = aPrimeiroeventodata;
    }

    /**
     * Access method for idcrmchamado.
     *
     * @return the current value of idcrmchamado
     */
    public String getIdcrmchamado() {
        return idcrmchamado;
    }

    /**
     * Setter method for idcrmchamado.
     *
     * @param aIdcrmchamado the new value for idcrmchamado
     */
    public void setIdcrmchamado(String aIdcrmchamado) {
        idcrmchamado = aIdcrmchamado;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for codusucadastro.
     *
     * @return the current value of codusucadastro
     */
    public String getCodusucadastro() {
        return codusucadastro;
    }

    /**
     * Setter method for codusucadastro.
     *
     * @param aCodusucadastro the new value for codusucadastro
     */
    public void setCodusucadastro(String aCodusucadastro) {
        codusucadastro = aCodusucadastro;
    }

    /**
     * Access method for codusuatual.
     *
     * @return the current value of codusuatual
     */
    public String getCodusuatual() {
        return codusuatual;
    }

    /**
     * Setter method for codusuatual.
     *
     * @param aCodusuatual the new value for codusuatual
     */
    public void setCodusuatual(String aCodusuatual) {
        codusuatual = aCodusuatual;
    }

    /**
     * Access method for finalizado.
     *
     * @return the current value of finalizado
     */
    public short getFinalizado() {
        return finalizado;
    }

    /**
     * Setter method for finalizado.
     *
     * @param aFinalizado the new value for finalizado
     */
    public void setFinalizado(short aFinalizado) {
        finalizado = aFinalizado;
    }

    /**
     * Access method for cancelado.
     *
     * @return the current value of cancelado
     */
    public short getCancelado() {
        return cancelado;
    }

    /**
     * Setter method for cancelado.
     *
     * @param aCancelado the new value for cancelado
     */
    public void setCancelado(short aCancelado) {
        cancelado = aCancelado;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for contcontrato.
     *
     * @return the current value of contcontrato
     */
    public Contcontrato getContcontrato() {
        return contcontrato;
    }

    /**
     * Setter method for contcontrato.
     *
     * @param aContcontrato the new value for contcontrato
     */
    public void setContcontrato(Contcontrato aContcontrato) {
        contcontrato = aContcontrato;
    }

    /**
     * Access method for crmevento.
     *
     * @return the current value of crmevento
     */
    public Crmevento getCrmevento() {
        return crmevento;
    }

    /**
     * Setter method for crmevento.
     *
     * @param aCrmevento the new value for crmevento
     */
    public void setCrmevento(Crmevento aCrmevento) {
        crmevento = aCrmevento;
    }

    /**
     * Access method for crmevento2.
     *
     * @return the current value of crmevento2
     */
    public Crmevento getCrmevento2() {
        return crmevento2;
    }

    /**
     * Setter method for crmevento2.
     *
     * @param aCrmevento2 the new value for crmevento2
     */
    public void setCrmevento2(Crmevento aCrmevento2) {
        crmevento2 = aCrmevento2;
    }

    /**
     * Access method for crmchamadoevento.
     *
     * @return the current value of crmchamadoevento
     */
    public Set<Crmchamadoevento> getCrmchamadoevento() {
        return crmchamadoevento;
    }

    /**
     * Setter method for crmchamadoevento.
     *
     * @param aCrmchamadoevento the new value for crmchamadoevento
     */
    public void setCrmchamadoevento(Set<Crmchamadoevento> aCrmchamadoevento) {
        crmchamadoevento = aCrmchamadoevento;
    }

    /**
     * Access method for crmchamadonota.
     *
     * @return the current value of crmchamadonota
     */
    public Set<Crmchamadonota> getCrmchamadonota() {
        return crmchamadonota;
    }

    /**
     * Setter method for crmchamadonota.
     *
     * @param aCrmchamadonota the new value for crmchamadonota
     */
    public void setCrmchamadonota(Set<Crmchamadonota> aCrmchamadonota) {
        crmchamadonota = aCrmchamadonota;
    }

    /**
     * Compares the key for this instance with another Crmchamado.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmchamado and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmchamado)) {
            return false;
        }
        Crmchamado that = (Crmchamado) other;
        if (this.getCodcrmchamado() != that.getCodcrmchamado()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmchamado.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmchamado)) return false;
        return this.equalKeys(other) && ((Crmchamado)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmchamado();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmchamado |");
        sb.append(" codcrmchamado=").append(getCodcrmchamado());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmchamado", Integer.valueOf(getCodcrmchamado()));
        return ret;
    }

}
