package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CRMCHAMADOEVENTO")
public class Crmchamadoevento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmchamadoevento";

    @Id
    @Column(name="CODCRMCHAMADOEVENTO", unique=true, nullable=false, precision=10)
    private int codcrmchamadoevento;
    @Column(name="OBS")
    private String obs;
    @Column(name="DATA")
    private Timestamp data;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCRMCHAMADO", nullable=false)
    private Crmchamado crmchamado;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCRMEVENTO", nullable=false)
    private Crmevento crmevento;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIO")
    private Confusuario confusuario;

    /** Default constructor. */
    public Crmchamadoevento() {
        super();
    }

    /**
     * Access method for codcrmchamadoevento.
     *
     * @return the current value of codcrmchamadoevento
     */
    public int getCodcrmchamadoevento() {
        return codcrmchamadoevento;
    }

    /**
     * Setter method for codcrmchamadoevento.
     *
     * @param aCodcrmchamadoevento the new value for codcrmchamadoevento
     */
    public void setCodcrmchamadoevento(int aCodcrmchamadoevento) {
        codcrmchamadoevento = aCodcrmchamadoevento;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for crmchamado.
     *
     * @return the current value of crmchamado
     */
    public Crmchamado getCrmchamado() {
        return crmchamado;
    }

    /**
     * Setter method for crmchamado.
     *
     * @param aCrmchamado the new value for crmchamado
     */
    public void setCrmchamado(Crmchamado aCrmchamado) {
        crmchamado = aCrmchamado;
    }

    /**
     * Access method for crmevento.
     *
     * @return the current value of crmevento
     */
    public Crmevento getCrmevento() {
        return crmevento;
    }

    /**
     * Setter method for crmevento.
     *
     * @param aCrmevento the new value for crmevento
     */
    public void setCrmevento(Crmevento aCrmevento) {
        crmevento = aCrmevento;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Compares the key for this instance with another Crmchamadoevento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmchamadoevento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmchamadoevento)) {
            return false;
        }
        Crmchamadoevento that = (Crmchamadoevento) other;
        if (this.getCodcrmchamadoevento() != that.getCodcrmchamadoevento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmchamadoevento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmchamadoevento)) return false;
        return this.equalKeys(other) && ((Crmchamadoevento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmchamadoevento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmchamadoevento |");
        sb.append(" codcrmchamadoevento=").append(getCodcrmchamadoevento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmchamadoevento", Integer.valueOf(getCodcrmchamadoevento()));
        return ret;
    }

}
