package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CRMCHAMADONOTA")
public class Crmchamadonota implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmchamadonota";

    @Id
    @Column(name="CODCRMCHAMADONOTA", unique=true, nullable=false, precision=10)
    private int codcrmchamadonota;
    @Column(name="DESCCRMCHAMADONOTA", nullable=false, length=250)
    private String desccrmchamadonota;
    @Column(name="OBS")
    private String obs;
    @Column(name="DATA", nullable=false)
    private Timestamp data;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCRMCHAMADO", nullable=false)
    private Crmchamado crmchamado;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIO", nullable=false)
    private Confusuario confusuario;

    /** Default constructor. */
    public Crmchamadonota() {
        super();
    }

    /**
     * Access method for codcrmchamadonota.
     *
     * @return the current value of codcrmchamadonota
     */
    public int getCodcrmchamadonota() {
        return codcrmchamadonota;
    }

    /**
     * Setter method for codcrmchamadonota.
     *
     * @param aCodcrmchamadonota the new value for codcrmchamadonota
     */
    public void setCodcrmchamadonota(int aCodcrmchamadonota) {
        codcrmchamadonota = aCodcrmchamadonota;
    }

    /**
     * Access method for desccrmchamadonota.
     *
     * @return the current value of desccrmchamadonota
     */
    public String getDesccrmchamadonota() {
        return desccrmchamadonota;
    }

    /**
     * Setter method for desccrmchamadonota.
     *
     * @param aDesccrmchamadonota the new value for desccrmchamadonota
     */
    public void setDesccrmchamadonota(String aDesccrmchamadonota) {
        desccrmchamadonota = aDesccrmchamadonota;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for crmchamado.
     *
     * @return the current value of crmchamado
     */
    public Crmchamado getCrmchamado() {
        return crmchamado;
    }

    /**
     * Setter method for crmchamado.
     *
     * @param aCrmchamado the new value for crmchamado
     */
    public void setCrmchamado(Crmchamado aCrmchamado) {
        crmchamado = aCrmchamado;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Compares the key for this instance with another Crmchamadonota.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmchamadonota and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmchamadonota)) {
            return false;
        }
        Crmchamadonota that = (Crmchamadonota) other;
        if (this.getCodcrmchamadonota() != that.getCodcrmchamadonota()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmchamadonota.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmchamadonota)) return false;
        return this.equalKeys(other) && ((Crmchamadonota)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmchamadonota();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmchamadonota |");
        sb.append(" codcrmchamadonota=").append(getCodcrmchamadonota());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmchamadonota", Integer.valueOf(getCodcrmchamadonota()));
        return ret;
    }

}
