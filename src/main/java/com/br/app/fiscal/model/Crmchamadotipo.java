package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CRMCHAMADOTIPO")
public class Crmchamadotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmchamadotipo";

    @Id
    @Column(name="CODCRMCHAMADOTIPO", unique=true, nullable=false, precision=10)
    private int codcrmchamadotipo;
    @Column(name="DESCCRMCHAMADOTIPO", nullable=false, length=250)
    private String desccrmchamadotipo;
    @OneToMany(mappedBy="crmchamadotipo")
    private Set<Crmsla> crmsla;

    /** Default constructor. */
    public Crmchamadotipo() {
        super();
    }

    /**
     * Access method for codcrmchamadotipo.
     *
     * @return the current value of codcrmchamadotipo
     */
    public int getCodcrmchamadotipo() {
        return codcrmchamadotipo;
    }

    /**
     * Setter method for codcrmchamadotipo.
     *
     * @param aCodcrmchamadotipo the new value for codcrmchamadotipo
     */
    public void setCodcrmchamadotipo(int aCodcrmchamadotipo) {
        codcrmchamadotipo = aCodcrmchamadotipo;
    }

    /**
     * Access method for desccrmchamadotipo.
     *
     * @return the current value of desccrmchamadotipo
     */
    public String getDesccrmchamadotipo() {
        return desccrmchamadotipo;
    }

    /**
     * Setter method for desccrmchamadotipo.
     *
     * @param aDesccrmchamadotipo the new value for desccrmchamadotipo
     */
    public void setDesccrmchamadotipo(String aDesccrmchamadotipo) {
        desccrmchamadotipo = aDesccrmchamadotipo;
    }

    /**
     * Access method for crmsla.
     *
     * @return the current value of crmsla
     */
    public Set<Crmsla> getCrmsla() {
        return crmsla;
    }

    /**
     * Setter method for crmsla.
     *
     * @param aCrmsla the new value for crmsla
     */
    public void setCrmsla(Set<Crmsla> aCrmsla) {
        crmsla = aCrmsla;
    }

    /**
     * Compares the key for this instance with another Crmchamadotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmchamadotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmchamadotipo)) {
            return false;
        }
        Crmchamadotipo that = (Crmchamadotipo) other;
        if (this.getCodcrmchamadotipo() != that.getCodcrmchamadotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmchamadotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmchamadotipo)) return false;
        return this.equalKeys(other) && ((Crmchamadotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmchamadotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmchamadotipo |");
        sb.append(" codcrmchamadotipo=").append(getCodcrmchamadotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmchamadotipo", Integer.valueOf(getCodcrmchamadotipo()));
        return ret;
    }

}
