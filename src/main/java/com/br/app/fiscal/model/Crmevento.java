package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CRMEVENTO")
public class Crmevento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmevento";

    @Id
    @Column(name="CODCRMEVENTO", unique=true, nullable=false, precision=10)
    private int codcrmevento;
    @Column(name="DESCCRMEVENTO", nullable=false, length=250)
    private String desccrmevento;
    @Column(name="INICIARCHAMADO", precision=5)
    private short iniciarchamado;
    @Column(name="FINALIZARCHAMADO", precision=5)
    private short finalizarchamado;
    @Column(name="CANCELARCHAMADO", precision=5)
    private short cancelarchamado;
    @Column(name="PEDIRAUXILIO", precision=5)
    private short pedirauxilio;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @OneToMany(mappedBy="crmevento")
    private Set<Crmchamado> crmchamado;
    @OneToMany(mappedBy="crmevento2")
    private Set<Crmchamado> crmchamado2;
    @OneToMany(mappedBy="crmevento")
    private Set<Crmchamadoevento> crmchamadoevento;

    /** Default constructor. */
    public Crmevento() {
        super();
    }

    /**
     * Access method for codcrmevento.
     *
     * @return the current value of codcrmevento
     */
    public int getCodcrmevento() {
        return codcrmevento;
    }

    /**
     * Setter method for codcrmevento.
     *
     * @param aCodcrmevento the new value for codcrmevento
     */
    public void setCodcrmevento(int aCodcrmevento) {
        codcrmevento = aCodcrmevento;
    }

    /**
     * Access method for desccrmevento.
     *
     * @return the current value of desccrmevento
     */
    public String getDesccrmevento() {
        return desccrmevento;
    }

    /**
     * Setter method for desccrmevento.
     *
     * @param aDesccrmevento the new value for desccrmevento
     */
    public void setDesccrmevento(String aDesccrmevento) {
        desccrmevento = aDesccrmevento;
    }

    /**
     * Access method for iniciarchamado.
     *
     * @return the current value of iniciarchamado
     */
    public short getIniciarchamado() {
        return iniciarchamado;
    }

    /**
     * Setter method for iniciarchamado.
     *
     * @param aIniciarchamado the new value for iniciarchamado
     */
    public void setIniciarchamado(short aIniciarchamado) {
        iniciarchamado = aIniciarchamado;
    }

    /**
     * Access method for finalizarchamado.
     *
     * @return the current value of finalizarchamado
     */
    public short getFinalizarchamado() {
        return finalizarchamado;
    }

    /**
     * Setter method for finalizarchamado.
     *
     * @param aFinalizarchamado the new value for finalizarchamado
     */
    public void setFinalizarchamado(short aFinalizarchamado) {
        finalizarchamado = aFinalizarchamado;
    }

    /**
     * Access method for cancelarchamado.
     *
     * @return the current value of cancelarchamado
     */
    public short getCancelarchamado() {
        return cancelarchamado;
    }

    /**
     * Setter method for cancelarchamado.
     *
     * @param aCancelarchamado the new value for cancelarchamado
     */
    public void setCancelarchamado(short aCancelarchamado) {
        cancelarchamado = aCancelarchamado;
    }

    /**
     * Access method for pedirauxilio.
     *
     * @return the current value of pedirauxilio
     */
    public short getPedirauxilio() {
        return pedirauxilio;
    }

    /**
     * Setter method for pedirauxilio.
     *
     * @param aPedirauxilio the new value for pedirauxilio
     */
    public void setPedirauxilio(short aPedirauxilio) {
        pedirauxilio = aPedirauxilio;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for crmchamado.
     *
     * @return the current value of crmchamado
     */
    public Set<Crmchamado> getCrmchamado() {
        return crmchamado;
    }

    /**
     * Setter method for crmchamado.
     *
     * @param aCrmchamado the new value for crmchamado
     */
    public void setCrmchamado(Set<Crmchamado> aCrmchamado) {
        crmchamado = aCrmchamado;
    }

    /**
     * Access method for crmchamado2.
     *
     * @return the current value of crmchamado2
     */
    public Set<Crmchamado> getCrmchamado2() {
        return crmchamado2;
    }

    /**
     * Setter method for crmchamado2.
     *
     * @param aCrmchamado2 the new value for crmchamado2
     */
    public void setCrmchamado2(Set<Crmchamado> aCrmchamado2) {
        crmchamado2 = aCrmchamado2;
    }

    /**
     * Access method for crmchamadoevento.
     *
     * @return the current value of crmchamadoevento
     */
    public Set<Crmchamadoevento> getCrmchamadoevento() {
        return crmchamadoevento;
    }

    /**
     * Setter method for crmchamadoevento.
     *
     * @param aCrmchamadoevento the new value for crmchamadoevento
     */
    public void setCrmchamadoevento(Set<Crmchamadoevento> aCrmchamadoevento) {
        crmchamadoevento = aCrmchamadoevento;
    }

    /**
     * Compares the key for this instance with another Crmevento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmevento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmevento)) {
            return false;
        }
        Crmevento that = (Crmevento) other;
        if (this.getCodcrmevento() != that.getCodcrmevento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmevento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmevento)) return false;
        return this.equalKeys(other) && ((Crmevento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmevento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmevento |");
        sb.append(" codcrmevento=").append(getCodcrmevento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmevento", Integer.valueOf(getCodcrmevento()));
        return ret;
    }

}
