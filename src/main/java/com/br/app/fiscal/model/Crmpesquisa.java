package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CRMPESQUISA")
public class Crmpesquisa implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmpesquisa";

    @Id
    @Column(name="CODCRMPESQUISA", unique=true, nullable=false, precision=10)
    private int codcrmpesquisa;
    @Column(name="DESCCRMPESQUISA", nullable=false, length=250)
    private String desccrmpesquisa;
    @Column(name="INICIODATA")
    private Timestamp iniciodata;
    @Column(name="FINALDATA")
    private Timestamp finaldata;
    @Column(name="MOTIVO")
    private String motivo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCRMTIPOPESQUISA", nullable=false)
    private Crmpesquisatipo crmpesquisatipo;
    @ManyToOne
    @JoinColumn(name="CODUSURESPONSAVEL")
    private Confusuario confusuario;
    @OneToMany(mappedBy="crmpesquisa")
    private Set<Crmpesquisadet> crmpesquisadet;

    /** Default constructor. */
    public Crmpesquisa() {
        super();
    }

    /**
     * Access method for codcrmpesquisa.
     *
     * @return the current value of codcrmpesquisa
     */
    public int getCodcrmpesquisa() {
        return codcrmpesquisa;
    }

    /**
     * Setter method for codcrmpesquisa.
     *
     * @param aCodcrmpesquisa the new value for codcrmpesquisa
     */
    public void setCodcrmpesquisa(int aCodcrmpesquisa) {
        codcrmpesquisa = aCodcrmpesquisa;
    }

    /**
     * Access method for desccrmpesquisa.
     *
     * @return the current value of desccrmpesquisa
     */
    public String getDesccrmpesquisa() {
        return desccrmpesquisa;
    }

    /**
     * Setter method for desccrmpesquisa.
     *
     * @param aDesccrmpesquisa the new value for desccrmpesquisa
     */
    public void setDesccrmpesquisa(String aDesccrmpesquisa) {
        desccrmpesquisa = aDesccrmpesquisa;
    }

    /**
     * Access method for iniciodata.
     *
     * @return the current value of iniciodata
     */
    public Timestamp getIniciodata() {
        return iniciodata;
    }

    /**
     * Setter method for iniciodata.
     *
     * @param aIniciodata the new value for iniciodata
     */
    public void setIniciodata(Timestamp aIniciodata) {
        iniciodata = aIniciodata;
    }

    /**
     * Access method for finaldata.
     *
     * @return the current value of finaldata
     */
    public Timestamp getFinaldata() {
        return finaldata;
    }

    /**
     * Setter method for finaldata.
     *
     * @param aFinaldata the new value for finaldata
     */
    public void setFinaldata(Timestamp aFinaldata) {
        finaldata = aFinaldata;
    }

    /**
     * Access method for motivo.
     *
     * @return the current value of motivo
     */
    public String getMotivo() {
        return motivo;
    }

    /**
     * Setter method for motivo.
     *
     * @param aMotivo the new value for motivo
     */
    public void setMotivo(String aMotivo) {
        motivo = aMotivo;
    }

    /**
     * Access method for crmpesquisatipo.
     *
     * @return the current value of crmpesquisatipo
     */
    public Crmpesquisatipo getCrmpesquisatipo() {
        return crmpesquisatipo;
    }

    /**
     * Setter method for crmpesquisatipo.
     *
     * @param aCrmpesquisatipo the new value for crmpesquisatipo
     */
    public void setCrmpesquisatipo(Crmpesquisatipo aCrmpesquisatipo) {
        crmpesquisatipo = aCrmpesquisatipo;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for crmpesquisadet.
     *
     * @return the current value of crmpesquisadet
     */
    public Set<Crmpesquisadet> getCrmpesquisadet() {
        return crmpesquisadet;
    }

    /**
     * Setter method for crmpesquisadet.
     *
     * @param aCrmpesquisadet the new value for crmpesquisadet
     */
    public void setCrmpesquisadet(Set<Crmpesquisadet> aCrmpesquisadet) {
        crmpesquisadet = aCrmpesquisadet;
    }

    /**
     * Compares the key for this instance with another Crmpesquisa.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmpesquisa and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmpesquisa)) {
            return false;
        }
        Crmpesquisa that = (Crmpesquisa) other;
        if (this.getCodcrmpesquisa() != that.getCodcrmpesquisa()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmpesquisa.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmpesquisa)) return false;
        return this.equalKeys(other) && ((Crmpesquisa)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmpesquisa();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmpesquisa |");
        sb.append(" codcrmpesquisa=").append(getCodcrmpesquisa());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmpesquisa", Integer.valueOf(getCodcrmpesquisa()));
        return ret;
    }

}
