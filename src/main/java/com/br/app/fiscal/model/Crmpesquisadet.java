package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CRMPESQUISADET")
public class Crmpesquisadet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmpesquisadet";

    @Id
    @Column(name="CODCRMPESQUISADET", unique=true, nullable=false, precision=10)
    private int codcrmpesquisadet;
    @Column(name="DESCCRMPESQUISADET", nullable=false, length=250)
    private String desccrmpesquisadet;
    @Column(name="TIPOCRMPESQUISADET", precision=5)
    private short tipocrmpesquisadet;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCRMPESQUISA", nullable=false)
    private Crmpesquisa crmpesquisa;

    /** Default constructor. */
    public Crmpesquisadet() {
        super();
    }

    /**
     * Access method for codcrmpesquisadet.
     *
     * @return the current value of codcrmpesquisadet
     */
    public int getCodcrmpesquisadet() {
        return codcrmpesquisadet;
    }

    /**
     * Setter method for codcrmpesquisadet.
     *
     * @param aCodcrmpesquisadet the new value for codcrmpesquisadet
     */
    public void setCodcrmpesquisadet(int aCodcrmpesquisadet) {
        codcrmpesquisadet = aCodcrmpesquisadet;
    }

    /**
     * Access method for desccrmpesquisadet.
     *
     * @return the current value of desccrmpesquisadet
     */
    public String getDesccrmpesquisadet() {
        return desccrmpesquisadet;
    }

    /**
     * Setter method for desccrmpesquisadet.
     *
     * @param aDesccrmpesquisadet the new value for desccrmpesquisadet
     */
    public void setDesccrmpesquisadet(String aDesccrmpesquisadet) {
        desccrmpesquisadet = aDesccrmpesquisadet;
    }

    /**
     * Access method for tipocrmpesquisadet.
     *
     * @return the current value of tipocrmpesquisadet
     */
    public short getTipocrmpesquisadet() {
        return tipocrmpesquisadet;
    }

    /**
     * Setter method for tipocrmpesquisadet.
     *
     * @param aTipocrmpesquisadet the new value for tipocrmpesquisadet
     */
    public void setTipocrmpesquisadet(short aTipocrmpesquisadet) {
        tipocrmpesquisadet = aTipocrmpesquisadet;
    }

    /**
     * Access method for crmpesquisa.
     *
     * @return the current value of crmpesquisa
     */
    public Crmpesquisa getCrmpesquisa() {
        return crmpesquisa;
    }

    /**
     * Setter method for crmpesquisa.
     *
     * @param aCrmpesquisa the new value for crmpesquisa
     */
    public void setCrmpesquisa(Crmpesquisa aCrmpesquisa) {
        crmpesquisa = aCrmpesquisa;
    }

    /**
     * Compares the key for this instance with another Crmpesquisadet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmpesquisadet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmpesquisadet)) {
            return false;
        }
        Crmpesquisadet that = (Crmpesquisadet) other;
        if (this.getCodcrmpesquisadet() != that.getCodcrmpesquisadet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmpesquisadet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmpesquisadet)) return false;
        return this.equalKeys(other) && ((Crmpesquisadet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmpesquisadet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmpesquisadet |");
        sb.append(" codcrmpesquisadet=").append(getCodcrmpesquisadet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmpesquisadet", Integer.valueOf(getCodcrmpesquisadet()));
        return ret;
    }

}
