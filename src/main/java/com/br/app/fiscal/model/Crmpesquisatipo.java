package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CRMPESQUISATIPO")
public class Crmpesquisatipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmpesquisatipo";

    @Id
    @Column(name="CODCRMPESQUISATIPO", unique=true, nullable=false, precision=10)
    private int codcrmpesquisatipo;
    @Column(name="DESCCRMPESQUISATIPO", nullable=false, length=250)
    private String desccrmpesquisatipo;
    @Column(name="CORCRMPESQUISATIPO", precision=10)
    private int corcrmpesquisatipo;
    @OneToMany(mappedBy="crmpesquisatipo")
    private Set<Crmpesquisa> crmpesquisa;

    /** Default constructor. */
    public Crmpesquisatipo() {
        super();
    }

    /**
     * Access method for codcrmpesquisatipo.
     *
     * @return the current value of codcrmpesquisatipo
     */
    public int getCodcrmpesquisatipo() {
        return codcrmpesquisatipo;
    }

    /**
     * Setter method for codcrmpesquisatipo.
     *
     * @param aCodcrmpesquisatipo the new value for codcrmpesquisatipo
     */
    public void setCodcrmpesquisatipo(int aCodcrmpesquisatipo) {
        codcrmpesquisatipo = aCodcrmpesquisatipo;
    }

    /**
     * Access method for desccrmpesquisatipo.
     *
     * @return the current value of desccrmpesquisatipo
     */
    public String getDesccrmpesquisatipo() {
        return desccrmpesquisatipo;
    }

    /**
     * Setter method for desccrmpesquisatipo.
     *
     * @param aDesccrmpesquisatipo the new value for desccrmpesquisatipo
     */
    public void setDesccrmpesquisatipo(String aDesccrmpesquisatipo) {
        desccrmpesquisatipo = aDesccrmpesquisatipo;
    }

    /**
     * Access method for corcrmpesquisatipo.
     *
     * @return the current value of corcrmpesquisatipo
     */
    public int getCorcrmpesquisatipo() {
        return corcrmpesquisatipo;
    }

    /**
     * Setter method for corcrmpesquisatipo.
     *
     * @param aCorcrmpesquisatipo the new value for corcrmpesquisatipo
     */
    public void setCorcrmpesquisatipo(int aCorcrmpesquisatipo) {
        corcrmpesquisatipo = aCorcrmpesquisatipo;
    }

    /**
     * Access method for crmpesquisa.
     *
     * @return the current value of crmpesquisa
     */
    public Set<Crmpesquisa> getCrmpesquisa() {
        return crmpesquisa;
    }

    /**
     * Setter method for crmpesquisa.
     *
     * @param aCrmpesquisa the new value for crmpesquisa
     */
    public void setCrmpesquisa(Set<Crmpesquisa> aCrmpesquisa) {
        crmpesquisa = aCrmpesquisa;
    }

    /**
     * Compares the key for this instance with another Crmpesquisatipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmpesquisatipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmpesquisatipo)) {
            return false;
        }
        Crmpesquisatipo that = (Crmpesquisatipo) other;
        if (this.getCodcrmpesquisatipo() != that.getCodcrmpesquisatipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmpesquisatipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmpesquisatipo)) return false;
        return this.equalKeys(other) && ((Crmpesquisatipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmpesquisatipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmpesquisatipo |");
        sb.append(" codcrmpesquisatipo=").append(getCodcrmpesquisatipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmpesquisatipo", Integer.valueOf(getCodcrmpesquisatipo()));
        return ret;
    }

}
