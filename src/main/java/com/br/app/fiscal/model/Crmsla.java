package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CRMSLA")
public class Crmsla implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmsla";

    @Id
    @Column(name="CODCRMSLA", unique=true, nullable=false, precision=10)
    private int codcrmsla;
    @Column(name="DESCCRMSLA", nullable=false, length=250)
    private String desccrmsla;
    @Column(name="OBS")
    private String obs;
    @Column(name="TIPOATENDIMENTO", precision=5)
    private short tipoatendimento;
    @Column(name="TIPOEXCECAOATENDIMENTO", precision=5)
    private short tipoexcecaoatendimento;
    @Column(name="TIPOPRAZOSOLUCAO", precision=5)
    private short tipoprazosolucao;
    @Column(name="TIPOPRAZOEXCECAO", precision=5)
    private short tipoprazoexcecao;
    @OneToMany(mappedBy="crmsla")
    private Set<Contcontratocrmsla> contcontratocrmsla;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCRMCHAMADOTIPO", nullable=false)
    private Crmchamadotipo crmchamadotipo;
    @OneToMany(mappedBy="crmsla")
    private Set<Crmslaconfcontato> crmslaconfcontato;
    @OneToMany(mappedBy="crmsla")
    private Set<Crmslaprodproduto> crmslaprodproduto;

    /** Default constructor. */
    public Crmsla() {
        super();
    }

    /**
     * Access method for codcrmsla.
     *
     * @return the current value of codcrmsla
     */
    public int getCodcrmsla() {
        return codcrmsla;
    }

    /**
     * Setter method for codcrmsla.
     *
     * @param aCodcrmsla the new value for codcrmsla
     */
    public void setCodcrmsla(int aCodcrmsla) {
        codcrmsla = aCodcrmsla;
    }

    /**
     * Access method for desccrmsla.
     *
     * @return the current value of desccrmsla
     */
    public String getDesccrmsla() {
        return desccrmsla;
    }

    /**
     * Setter method for desccrmsla.
     *
     * @param aDesccrmsla the new value for desccrmsla
     */
    public void setDesccrmsla(String aDesccrmsla) {
        desccrmsla = aDesccrmsla;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for tipoatendimento.
     *
     * @return the current value of tipoatendimento
     */
    public short getTipoatendimento() {
        return tipoatendimento;
    }

    /**
     * Setter method for tipoatendimento.
     *
     * @param aTipoatendimento the new value for tipoatendimento
     */
    public void setTipoatendimento(short aTipoatendimento) {
        tipoatendimento = aTipoatendimento;
    }

    /**
     * Access method for tipoexcecaoatendimento.
     *
     * @return the current value of tipoexcecaoatendimento
     */
    public short getTipoexcecaoatendimento() {
        return tipoexcecaoatendimento;
    }

    /**
     * Setter method for tipoexcecaoatendimento.
     *
     * @param aTipoexcecaoatendimento the new value for tipoexcecaoatendimento
     */
    public void setTipoexcecaoatendimento(short aTipoexcecaoatendimento) {
        tipoexcecaoatendimento = aTipoexcecaoatendimento;
    }

    /**
     * Access method for tipoprazosolucao.
     *
     * @return the current value of tipoprazosolucao
     */
    public short getTipoprazosolucao() {
        return tipoprazosolucao;
    }

    /**
     * Setter method for tipoprazosolucao.
     *
     * @param aTipoprazosolucao the new value for tipoprazosolucao
     */
    public void setTipoprazosolucao(short aTipoprazosolucao) {
        tipoprazosolucao = aTipoprazosolucao;
    }

    /**
     * Access method for tipoprazoexcecao.
     *
     * @return the current value of tipoprazoexcecao
     */
    public short getTipoprazoexcecao() {
        return tipoprazoexcecao;
    }

    /**
     * Setter method for tipoprazoexcecao.
     *
     * @param aTipoprazoexcecao the new value for tipoprazoexcecao
     */
    public void setTipoprazoexcecao(short aTipoprazoexcecao) {
        tipoprazoexcecao = aTipoprazoexcecao;
    }

    /**
     * Access method for contcontratocrmsla.
     *
     * @return the current value of contcontratocrmsla
     */
    public Set<Contcontratocrmsla> getContcontratocrmsla() {
        return contcontratocrmsla;
    }

    /**
     * Setter method for contcontratocrmsla.
     *
     * @param aContcontratocrmsla the new value for contcontratocrmsla
     */
    public void setContcontratocrmsla(Set<Contcontratocrmsla> aContcontratocrmsla) {
        contcontratocrmsla = aContcontratocrmsla;
    }

    /**
     * Access method for crmchamadotipo.
     *
     * @return the current value of crmchamadotipo
     */
    public Crmchamadotipo getCrmchamadotipo() {
        return crmchamadotipo;
    }

    /**
     * Setter method for crmchamadotipo.
     *
     * @param aCrmchamadotipo the new value for crmchamadotipo
     */
    public void setCrmchamadotipo(Crmchamadotipo aCrmchamadotipo) {
        crmchamadotipo = aCrmchamadotipo;
    }

    /**
     * Access method for crmslaconfcontato.
     *
     * @return the current value of crmslaconfcontato
     */
    public Set<Crmslaconfcontato> getCrmslaconfcontato() {
        return crmslaconfcontato;
    }

    /**
     * Setter method for crmslaconfcontato.
     *
     * @param aCrmslaconfcontato the new value for crmslaconfcontato
     */
    public void setCrmslaconfcontato(Set<Crmslaconfcontato> aCrmslaconfcontato) {
        crmslaconfcontato = aCrmslaconfcontato;
    }

    /**
     * Access method for crmslaprodproduto.
     *
     * @return the current value of crmslaprodproduto
     */
    public Set<Crmslaprodproduto> getCrmslaprodproduto() {
        return crmslaprodproduto;
    }

    /**
     * Setter method for crmslaprodproduto.
     *
     * @param aCrmslaprodproduto the new value for crmslaprodproduto
     */
    public void setCrmslaprodproduto(Set<Crmslaprodproduto> aCrmslaprodproduto) {
        crmslaprodproduto = aCrmslaprodproduto;
    }

    /**
     * Compares the key for this instance with another Crmsla.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmsla and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmsla)) {
            return false;
        }
        Crmsla that = (Crmsla) other;
        if (this.getCodcrmsla() != that.getCodcrmsla()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmsla.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmsla)) return false;
        return this.equalKeys(other) && ((Crmsla)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmsla();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmsla |");
        sb.append(" codcrmsla=").append(getCodcrmsla());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmsla", Integer.valueOf(getCodcrmsla()));
        return ret;
    }

}
