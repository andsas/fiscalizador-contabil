package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CRMSLACONFCONTATO")
public class Crmslaconfcontato implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmslaconfcontato";

    @Id
    @Column(name="CODCRMSLACONFCONTATO", unique=true, nullable=false, precision=10)
    private int codcrmslaconfcontato;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFCONTATO", nullable=false)
    private Confcontato confcontato;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCRMSLA", nullable=false)
    private Crmsla crmsla;

    /** Default constructor. */
    public Crmslaconfcontato() {
        super();
    }

    /**
     * Access method for codcrmslaconfcontato.
     *
     * @return the current value of codcrmslaconfcontato
     */
    public int getCodcrmslaconfcontato() {
        return codcrmslaconfcontato;
    }

    /**
     * Setter method for codcrmslaconfcontato.
     *
     * @param aCodcrmslaconfcontato the new value for codcrmslaconfcontato
     */
    public void setCodcrmslaconfcontato(int aCodcrmslaconfcontato) {
        codcrmslaconfcontato = aCodcrmslaconfcontato;
    }

    /**
     * Access method for confcontato.
     *
     * @return the current value of confcontato
     */
    public Confcontato getConfcontato() {
        return confcontato;
    }

    /**
     * Setter method for confcontato.
     *
     * @param aConfcontato the new value for confcontato
     */
    public void setConfcontato(Confcontato aConfcontato) {
        confcontato = aConfcontato;
    }

    /**
     * Access method for crmsla.
     *
     * @return the current value of crmsla
     */
    public Crmsla getCrmsla() {
        return crmsla;
    }

    /**
     * Setter method for crmsla.
     *
     * @param aCrmsla the new value for crmsla
     */
    public void setCrmsla(Crmsla aCrmsla) {
        crmsla = aCrmsla;
    }

    /**
     * Compares the key for this instance with another Crmslaconfcontato.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmslaconfcontato and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmslaconfcontato)) {
            return false;
        }
        Crmslaconfcontato that = (Crmslaconfcontato) other;
        if (this.getCodcrmslaconfcontato() != that.getCodcrmslaconfcontato()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmslaconfcontato.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmslaconfcontato)) return false;
        return this.equalKeys(other) && ((Crmslaconfcontato)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmslaconfcontato();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmslaconfcontato |");
        sb.append(" codcrmslaconfcontato=").append(getCodcrmslaconfcontato());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmslaconfcontato", Integer.valueOf(getCodcrmslaconfcontato()));
        return ret;
    }

}
