package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CRMSLAPRODPRODUTO")
public class Crmslaprodproduto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcrmslaprodproduto";

    @Id
    @Column(name="CODCRMSLAPRODPRODUTO", unique=true, nullable=false, precision=10)
    private int codcrmslaprodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCRMSLA", nullable=false)
    private Crmsla crmsla;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;

    /** Default constructor. */
    public Crmslaprodproduto() {
        super();
    }

    /**
     * Access method for codcrmslaprodproduto.
     *
     * @return the current value of codcrmslaprodproduto
     */
    public int getCodcrmslaprodproduto() {
        return codcrmslaprodproduto;
    }

    /**
     * Setter method for codcrmslaprodproduto.
     *
     * @param aCodcrmslaprodproduto the new value for codcrmslaprodproduto
     */
    public void setCodcrmslaprodproduto(int aCodcrmslaprodproduto) {
        codcrmslaprodproduto = aCodcrmslaprodproduto;
    }

    /**
     * Access method for crmsla.
     *
     * @return the current value of crmsla
     */
    public Crmsla getCrmsla() {
        return crmsla;
    }

    /**
     * Setter method for crmsla.
     *
     * @param aCrmsla the new value for crmsla
     */
    public void setCrmsla(Crmsla aCrmsla) {
        crmsla = aCrmsla;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Crmslaprodproduto.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Crmslaprodproduto and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Crmslaprodproduto)) {
            return false;
        }
        Crmslaprodproduto that = (Crmslaprodproduto) other;
        if (this.getCodcrmslaprodproduto() != that.getCodcrmslaprodproduto()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Crmslaprodproduto.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Crmslaprodproduto)) return false;
        return this.equalKeys(other) && ((Crmslaprodproduto)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcrmslaprodproduto();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Crmslaprodproduto |");
        sb.append(" codcrmslaprodproduto=").append(getCodcrmslaprodproduto());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcrmslaprodproduto", Integer.valueOf(getCodcrmslaprodproduto()));
        return ret;
    }

}
