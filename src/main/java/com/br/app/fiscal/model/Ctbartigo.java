package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CTBARTIGO")
public class Ctbartigo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbartigo";

    @Id
    @Column(name="CODCTBARTIGO", unique=true, nullable=false, length=20)
    private String codctbartigo;
    @Column(name="CODCTBCAPITULO", nullable=false, length=20)
    private String codctbcapitulo;
    @Column(name="DESCCTBARTIGO", nullable=false, length=250)
    private String descctbartigo;
    @Column(name="OBS")
    private String obs;
    @Column(name="PENALIDADE")
    private String penalidade;
    @OneToMany(mappedBy="ctbartigo")
    private Set<Ctbveicinfracao> ctbveicinfracao;

    /** Default constructor. */
    public Ctbartigo() {
        super();
    }

    /**
     * Access method for codctbartigo.
     *
     * @return the current value of codctbartigo
     */
    public String getCodctbartigo() {
        return codctbartigo;
    }

    /**
     * Setter method for codctbartigo.
     *
     * @param aCodctbartigo the new value for codctbartigo
     */
    public void setCodctbartigo(String aCodctbartigo) {
        codctbartigo = aCodctbartigo;
    }

    /**
     * Access method for codctbcapitulo.
     *
     * @return the current value of codctbcapitulo
     */
    public String getCodctbcapitulo() {
        return codctbcapitulo;
    }

    /**
     * Setter method for codctbcapitulo.
     *
     * @param aCodctbcapitulo the new value for codctbcapitulo
     */
    public void setCodctbcapitulo(String aCodctbcapitulo) {
        codctbcapitulo = aCodctbcapitulo;
    }

    /**
     * Access method for descctbartigo.
     *
     * @return the current value of descctbartigo
     */
    public String getDescctbartigo() {
        return descctbartigo;
    }

    /**
     * Setter method for descctbartigo.
     *
     * @param aDescctbartigo the new value for descctbartigo
     */
    public void setDescctbartigo(String aDescctbartigo) {
        descctbartigo = aDescctbartigo;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for penalidade.
     *
     * @return the current value of penalidade
     */
    public String getPenalidade() {
        return penalidade;
    }

    /**
     * Setter method for penalidade.
     *
     * @param aPenalidade the new value for penalidade
     */
    public void setPenalidade(String aPenalidade) {
        penalidade = aPenalidade;
    }

    /**
     * Access method for ctbveicinfracao.
     *
     * @return the current value of ctbveicinfracao
     */
    public Set<Ctbveicinfracao> getCtbveicinfracao() {
        return ctbveicinfracao;
    }

    /**
     * Setter method for ctbveicinfracao.
     *
     * @param aCtbveicinfracao the new value for ctbveicinfracao
     */
    public void setCtbveicinfracao(Set<Ctbveicinfracao> aCtbveicinfracao) {
        ctbveicinfracao = aCtbveicinfracao;
    }

    /**
     * Compares the key for this instance with another Ctbartigo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbartigo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbartigo)) {
            return false;
        }
        Ctbartigo that = (Ctbartigo) other;
        Object myCodctbartigo = this.getCodctbartigo();
        Object yourCodctbartigo = that.getCodctbartigo();
        if (myCodctbartigo==null ? yourCodctbartigo!=null : !myCodctbartigo.equals(yourCodctbartigo)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbartigo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbartigo)) return false;
        return this.equalKeys(other) && ((Ctbartigo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodctbartigo() == null) {
            i = 0;
        } else {
            i = getCodctbartigo().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbartigo |");
        sb.append(" codctbartigo=").append(getCodctbartigo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbartigo", getCodctbartigo());
        return ret;
    }

}
