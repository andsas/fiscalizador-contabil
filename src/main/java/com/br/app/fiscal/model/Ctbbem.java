package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CTBBEM")
public class Ctbbem implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbbem";

    @Id
    @Column(name="CODCTBBEM", unique=true, nullable=false, precision=10)
    private int codctbbem;
    @Column(name="DATAENTRADA", nullable=false)
    private Timestamp dataentrada;
    @Column(name="VALORCOMPRA", nullable=false, precision=10)
    private int valorcompra;
    @Column(name="VALORVENAL", precision=15, scale=4)
    private BigDecimal valorvenal;
    @Column(name="VALORMERCADO", precision=15, scale=4)
    private BigDecimal valormercado;
    @Column(name="VALORDEPR", precision=15, scale=4)
    private BigDecimal valordepr;
    @Column(name="VALORVAL", precision=15, scale=4)
    private BigDecimal valorval;
    @Column(name="ULTDEPR")
    private Timestamp ultdepr;
    @Column(name="ULTVAL")
    private Timestamp ultval;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAO", nullable=false)
    private Optransacao optransacao;
    @ManyToOne
    @JoinColumn(name="CODPRODAPOLICE")
    private Prodapolice prodapolice;
    @ManyToOne
    @JoinColumn(name="CODCTBTAXA")
    private Ctbtaxa ctbtaxa;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="CODAGRESPONSAVEL")
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODCONFUSURESPONSAVEL")
    private Confusuario confusuario;
    @OneToMany(mappedBy="ctbbem")
    private Set<Optransacaodet> optransacaodet;

    /** Default constructor. */
    public Ctbbem() {
        super();
    }

    /**
     * Access method for codctbbem.
     *
     * @return the current value of codctbbem
     */
    public int getCodctbbem() {
        return codctbbem;
    }

    /**
     * Setter method for codctbbem.
     *
     * @param aCodctbbem the new value for codctbbem
     */
    public void setCodctbbem(int aCodctbbem) {
        codctbbem = aCodctbbem;
    }

    /**
     * Access method for dataentrada.
     *
     * @return the current value of dataentrada
     */
    public Timestamp getDataentrada() {
        return dataentrada;
    }

    /**
     * Setter method for dataentrada.
     *
     * @param aDataentrada the new value for dataentrada
     */
    public void setDataentrada(Timestamp aDataentrada) {
        dataentrada = aDataentrada;
    }

    /**
     * Access method for valorcompra.
     *
     * @return the current value of valorcompra
     */
    public int getValorcompra() {
        return valorcompra;
    }

    /**
     * Setter method for valorcompra.
     *
     * @param aValorcompra the new value for valorcompra
     */
    public void setValorcompra(int aValorcompra) {
        valorcompra = aValorcompra;
    }

    /**
     * Access method for valorvenal.
     *
     * @return the current value of valorvenal
     */
    public BigDecimal getValorvenal() {
        return valorvenal;
    }

    /**
     * Setter method for valorvenal.
     *
     * @param aValorvenal the new value for valorvenal
     */
    public void setValorvenal(BigDecimal aValorvenal) {
        valorvenal = aValorvenal;
    }

    /**
     * Access method for valormercado.
     *
     * @return the current value of valormercado
     */
    public BigDecimal getValormercado() {
        return valormercado;
    }

    /**
     * Setter method for valormercado.
     *
     * @param aValormercado the new value for valormercado
     */
    public void setValormercado(BigDecimal aValormercado) {
        valormercado = aValormercado;
    }

    /**
     * Access method for valordepr.
     *
     * @return the current value of valordepr
     */
    public BigDecimal getValordepr() {
        return valordepr;
    }

    /**
     * Setter method for valordepr.
     *
     * @param aValordepr the new value for valordepr
     */
    public void setValordepr(BigDecimal aValordepr) {
        valordepr = aValordepr;
    }

    /**
     * Access method for valorval.
     *
     * @return the current value of valorval
     */
    public BigDecimal getValorval() {
        return valorval;
    }

    /**
     * Setter method for valorval.
     *
     * @param aValorval the new value for valorval
     */
    public void setValorval(BigDecimal aValorval) {
        valorval = aValorval;
    }

    /**
     * Access method for ultdepr.
     *
     * @return the current value of ultdepr
     */
    public Timestamp getUltdepr() {
        return ultdepr;
    }

    /**
     * Setter method for ultdepr.
     *
     * @param aUltdepr the new value for ultdepr
     */
    public void setUltdepr(Timestamp aUltdepr) {
        ultdepr = aUltdepr;
    }

    /**
     * Access method for ultval.
     *
     * @return the current value of ultval
     */
    public Timestamp getUltval() {
        return ultval;
    }

    /**
     * Setter method for ultval.
     *
     * @param aUltval the new value for ultval
     */
    public void setUltval(Timestamp aUltval) {
        ultval = aUltval;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for prodapolice.
     *
     * @return the current value of prodapolice
     */
    public Prodapolice getProdapolice() {
        return prodapolice;
    }

    /**
     * Setter method for prodapolice.
     *
     * @param aProdapolice the new value for prodapolice
     */
    public void setProdapolice(Prodapolice aProdapolice) {
        prodapolice = aProdapolice;
    }

    /**
     * Access method for ctbtaxa.
     *
     * @return the current value of ctbtaxa
     */
    public Ctbtaxa getCtbtaxa() {
        return ctbtaxa;
    }

    /**
     * Setter method for ctbtaxa.
     *
     * @param aCtbtaxa the new value for ctbtaxa
     */
    public void setCtbtaxa(Ctbtaxa aCtbtaxa) {
        ctbtaxa = aCtbtaxa;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Compares the key for this instance with another Ctbbem.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbbem and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbbem)) {
            return false;
        }
        Ctbbem that = (Ctbbem) other;
        if (this.getCodctbbem() != that.getCodctbbem()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbbem.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbbem)) return false;
        return this.equalKeys(other) && ((Ctbbem)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbbem();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbbem |");
        sb.append(" codctbbem=").append(getCodctbbem());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbbem", Integer.valueOf(getCodctbbem()));
        return ret;
    }

}
