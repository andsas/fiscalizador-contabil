package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CTBBEMDET")
public class Ctbbemdet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbbemdet";

    @Id
    @Column(name="CODCTBBEMDET", unique=true, nullable=false, precision=10)
    private int codctbbemdet;
    @Column(name="CODCTBBEM", nullable=false, precision=10)
    private int codctbbem;
    @Column(name="TOTAL", nullable=false, precision=15, scale=4)
    private BigDecimal total;
    @Column(name="TIPOCTBBEMDET", nullable=false, precision=5)
    private short tipoctbbemdet;
    @Column(name="DESCCTBBEMDET", nullable=false, length=250)
    private String descctbbemdet;
    @Column(name="DATA", nullable=false)
    private Timestamp data;
    @Column(name="TIPODEBCRED", precision=5)
    private short tipodebcred;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAO")
    private Optransacao optransacao;
    @ManyToOne
    @JoinColumn(name="CTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta2;
    @OneToMany(mappedBy="ctbbemdet")
    private Set<Ctbbemimoapolice> ctbbemimoapolice;
    @OneToMany(mappedBy="ctbbemdet")
    private Set<Ctbbemoutroapolice> ctbbemoutroapolice;
    @OneToMany(mappedBy="ctbbemdet")
    private Set<Ctbbemveicapolice> ctbbemveicapolice;
    @OneToMany(mappedBy="ctbbemdet")
    private Set<Ctbbemveicinfracao> ctbbemveicinfracao;
    @OneToMany(mappedBy="ctbbemdet")
    private Set<Ctbbemveictacografo> ctbbemveictacografo;

    /** Default constructor. */
    public Ctbbemdet() {
        super();
    }

    /**
     * Access method for codctbbemdet.
     *
     * @return the current value of codctbbemdet
     */
    public int getCodctbbemdet() {
        return codctbbemdet;
    }

    /**
     * Setter method for codctbbemdet.
     *
     * @param aCodctbbemdet the new value for codctbbemdet
     */
    public void setCodctbbemdet(int aCodctbbemdet) {
        codctbbemdet = aCodctbbemdet;
    }

    /**
     * Access method for codctbbem.
     *
     * @return the current value of codctbbem
     */
    public int getCodctbbem() {
        return codctbbem;
    }

    /**
     * Setter method for codctbbem.
     *
     * @param aCodctbbem the new value for codctbbem
     */
    public void setCodctbbem(int aCodctbbem) {
        codctbbem = aCodctbbem;
    }

    /**
     * Access method for total.
     *
     * @return the current value of total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Setter method for total.
     *
     * @param aTotal the new value for total
     */
    public void setTotal(BigDecimal aTotal) {
        total = aTotal;
    }

    /**
     * Access method for tipoctbbemdet.
     *
     * @return the current value of tipoctbbemdet
     */
    public short getTipoctbbemdet() {
        return tipoctbbemdet;
    }

    /**
     * Setter method for tipoctbbemdet.
     *
     * @param aTipoctbbemdet the new value for tipoctbbemdet
     */
    public void setTipoctbbemdet(short aTipoctbbemdet) {
        tipoctbbemdet = aTipoctbbemdet;
    }

    /**
     * Access method for descctbbemdet.
     *
     * @return the current value of descctbbemdet
     */
    public String getDescctbbemdet() {
        return descctbbemdet;
    }

    /**
     * Setter method for descctbbemdet.
     *
     * @param aDescctbbemdet the new value for descctbbemdet
     */
    public void setDescctbbemdet(String aDescctbbemdet) {
        descctbbemdet = aDescctbbemdet;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for tipodebcred.
     *
     * @return the current value of tipodebcred
     */
    public short getTipodebcred() {
        return tipodebcred;
    }

    /**
     * Setter method for tipodebcred.
     *
     * @param aTipodebcred the new value for tipodebcred
     */
    public void setTipodebcred(short aTipodebcred) {
        tipodebcred = aTipodebcred;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for ctbbemimoapolice.
     *
     * @return the current value of ctbbemimoapolice
     */
    public Set<Ctbbemimoapolice> getCtbbemimoapolice() {
        return ctbbemimoapolice;
    }

    /**
     * Setter method for ctbbemimoapolice.
     *
     * @param aCtbbemimoapolice the new value for ctbbemimoapolice
     */
    public void setCtbbemimoapolice(Set<Ctbbemimoapolice> aCtbbemimoapolice) {
        ctbbemimoapolice = aCtbbemimoapolice;
    }

    /**
     * Access method for ctbbemoutroapolice.
     *
     * @return the current value of ctbbemoutroapolice
     */
    public Set<Ctbbemoutroapolice> getCtbbemoutroapolice() {
        return ctbbemoutroapolice;
    }

    /**
     * Setter method for ctbbemoutroapolice.
     *
     * @param aCtbbemoutroapolice the new value for ctbbemoutroapolice
     */
    public void setCtbbemoutroapolice(Set<Ctbbemoutroapolice> aCtbbemoutroapolice) {
        ctbbemoutroapolice = aCtbbemoutroapolice;
    }

    /**
     * Access method for ctbbemveicapolice.
     *
     * @return the current value of ctbbemveicapolice
     */
    public Set<Ctbbemveicapolice> getCtbbemveicapolice() {
        return ctbbemveicapolice;
    }

    /**
     * Setter method for ctbbemveicapolice.
     *
     * @param aCtbbemveicapolice the new value for ctbbemveicapolice
     */
    public void setCtbbemveicapolice(Set<Ctbbemveicapolice> aCtbbemveicapolice) {
        ctbbemveicapolice = aCtbbemveicapolice;
    }

    /**
     * Access method for ctbbemveicinfracao.
     *
     * @return the current value of ctbbemveicinfracao
     */
    public Set<Ctbbemveicinfracao> getCtbbemveicinfracao() {
        return ctbbemveicinfracao;
    }

    /**
     * Setter method for ctbbemveicinfracao.
     *
     * @param aCtbbemveicinfracao the new value for ctbbemveicinfracao
     */
    public void setCtbbemveicinfracao(Set<Ctbbemveicinfracao> aCtbbemveicinfracao) {
        ctbbemveicinfracao = aCtbbemveicinfracao;
    }

    /**
     * Access method for ctbbemveictacografo.
     *
     * @return the current value of ctbbemveictacografo
     */
    public Set<Ctbbemveictacografo> getCtbbemveictacografo() {
        return ctbbemveictacografo;
    }

    /**
     * Setter method for ctbbemveictacografo.
     *
     * @param aCtbbemveictacografo the new value for ctbbemveictacografo
     */
    public void setCtbbemveictacografo(Set<Ctbbemveictacografo> aCtbbemveictacografo) {
        ctbbemveictacografo = aCtbbemveictacografo;
    }

    /**
     * Compares the key for this instance with another Ctbbemdet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbbemdet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbbemdet)) {
            return false;
        }
        Ctbbemdet that = (Ctbbemdet) other;
        if (this.getCodctbbemdet() != that.getCodctbbemdet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbbemdet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbbemdet)) return false;
        return this.equalKeys(other) && ((Ctbbemdet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbbemdet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbbemdet |");
        sb.append(" codctbbemdet=").append(getCodctbbemdet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbbemdet", Integer.valueOf(getCodctbbemdet()));
        return ret;
    }

}
