package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CTBBEMIMOAPOLICE")
public class Ctbbemimoapolice implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbbemimoapolice";

    @Id
    @Column(name="CODCTBBEMIMOAPOLICE", unique=true, nullable=false, precision=10)
    private int codctbbemimoapolice;
    @Column(name="DESCCTBBEMIMOAPOLICE", nullable=false, length=250)
    private String descctbbemimoapolice;
    @Column(name="APOLICEINICIAL", nullable=false)
    private Timestamp apoliceinicial;
    @Column(name="APOLICEFINAL", nullable=false)
    private Timestamp apolicefinal;
    @Column(name="APOLICEPGTODATA", nullable=false)
    private Timestamp apolicepgtodata;
    @Column(name="APOLICEPGTOVALOR", nullable=false, precision=15, scale=4)
    private BigDecimal apolicepgtovalor;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGERIMOVEL", nullable=false)
    private Gerimovel gerimovel;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODAPOLICE", nullable=false)
    private Prodapolice prodapolice;
    @ManyToOne
    @JoinColumn(name="CODFINLANCAMENTO")
    private Finlancamento finlancamento;
    @ManyToOne
    @JoinColumn(name="CODCTBBEMDET")
    private Ctbbemdet ctbbemdet;

    /** Default constructor. */
    public Ctbbemimoapolice() {
        super();
    }

    /**
     * Access method for codctbbemimoapolice.
     *
     * @return the current value of codctbbemimoapolice
     */
    public int getCodctbbemimoapolice() {
        return codctbbemimoapolice;
    }

    /**
     * Setter method for codctbbemimoapolice.
     *
     * @param aCodctbbemimoapolice the new value for codctbbemimoapolice
     */
    public void setCodctbbemimoapolice(int aCodctbbemimoapolice) {
        codctbbemimoapolice = aCodctbbemimoapolice;
    }

    /**
     * Access method for descctbbemimoapolice.
     *
     * @return the current value of descctbbemimoapolice
     */
    public String getDescctbbemimoapolice() {
        return descctbbemimoapolice;
    }

    /**
     * Setter method for descctbbemimoapolice.
     *
     * @param aDescctbbemimoapolice the new value for descctbbemimoapolice
     */
    public void setDescctbbemimoapolice(String aDescctbbemimoapolice) {
        descctbbemimoapolice = aDescctbbemimoapolice;
    }

    /**
     * Access method for apoliceinicial.
     *
     * @return the current value of apoliceinicial
     */
    public Timestamp getApoliceinicial() {
        return apoliceinicial;
    }

    /**
     * Setter method for apoliceinicial.
     *
     * @param aApoliceinicial the new value for apoliceinicial
     */
    public void setApoliceinicial(Timestamp aApoliceinicial) {
        apoliceinicial = aApoliceinicial;
    }

    /**
     * Access method for apolicefinal.
     *
     * @return the current value of apolicefinal
     */
    public Timestamp getApolicefinal() {
        return apolicefinal;
    }

    /**
     * Setter method for apolicefinal.
     *
     * @param aApolicefinal the new value for apolicefinal
     */
    public void setApolicefinal(Timestamp aApolicefinal) {
        apolicefinal = aApolicefinal;
    }

    /**
     * Access method for apolicepgtodata.
     *
     * @return the current value of apolicepgtodata
     */
    public Timestamp getApolicepgtodata() {
        return apolicepgtodata;
    }

    /**
     * Setter method for apolicepgtodata.
     *
     * @param aApolicepgtodata the new value for apolicepgtodata
     */
    public void setApolicepgtodata(Timestamp aApolicepgtodata) {
        apolicepgtodata = aApolicepgtodata;
    }

    /**
     * Access method for apolicepgtovalor.
     *
     * @return the current value of apolicepgtovalor
     */
    public BigDecimal getApolicepgtovalor() {
        return apolicepgtovalor;
    }

    /**
     * Setter method for apolicepgtovalor.
     *
     * @param aApolicepgtovalor the new value for apolicepgtovalor
     */
    public void setApolicepgtovalor(BigDecimal aApolicepgtovalor) {
        apolicepgtovalor = aApolicepgtovalor;
    }

    /**
     * Access method for gerimovel.
     *
     * @return the current value of gerimovel
     */
    public Gerimovel getGerimovel() {
        return gerimovel;
    }

    /**
     * Setter method for gerimovel.
     *
     * @param aGerimovel the new value for gerimovel
     */
    public void setGerimovel(Gerimovel aGerimovel) {
        gerimovel = aGerimovel;
    }

    /**
     * Access method for prodapolice.
     *
     * @return the current value of prodapolice
     */
    public Prodapolice getProdapolice() {
        return prodapolice;
    }

    /**
     * Setter method for prodapolice.
     *
     * @param aProdapolice the new value for prodapolice
     */
    public void setProdapolice(Prodapolice aProdapolice) {
        prodapolice = aProdapolice;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Finlancamento getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Finlancamento aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for ctbbemdet.
     *
     * @return the current value of ctbbemdet
     */
    public Ctbbemdet getCtbbemdet() {
        return ctbbemdet;
    }

    /**
     * Setter method for ctbbemdet.
     *
     * @param aCtbbemdet the new value for ctbbemdet
     */
    public void setCtbbemdet(Ctbbemdet aCtbbemdet) {
        ctbbemdet = aCtbbemdet;
    }

    /**
     * Compares the key for this instance with another Ctbbemimoapolice.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbbemimoapolice and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbbemimoapolice)) {
            return false;
        }
        Ctbbemimoapolice that = (Ctbbemimoapolice) other;
        if (this.getCodctbbemimoapolice() != that.getCodctbbemimoapolice()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbbemimoapolice.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbbemimoapolice)) return false;
        return this.equalKeys(other) && ((Ctbbemimoapolice)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbbemimoapolice();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbbemimoapolice |");
        sb.append(" codctbbemimoapolice=").append(getCodctbbemimoapolice());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbbemimoapolice", Integer.valueOf(getCodctbbemimoapolice()));
        return ret;
    }

}
