package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CTBBEMOUTROAPOLICE")
public class Ctbbemoutroapolice implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbbemoutroapolice";

    @Id
    @Column(name="CODCTBBEMOUTROAPOLICE", unique=true, nullable=false, precision=10)
    private int codctbbemoutroapolice;
    @Column(name="DESCCTBBEMOUTROAPOLICE", nullable=false, length=250)
    private String descctbbemoutroapolice;
    @Column(name="APOLICEINICIAL", nullable=false)
    private Timestamp apoliceinicial;
    @Column(name="APOLICEFINAL", nullable=false)
    private Timestamp apolicefinal;
    @Column(name="APOLICEPGTODATA", nullable=false)
    private Timestamp apolicepgtodata;
    @Column(name="APOLICEPGTOVALOR", nullable=false, precision=15, scale=4)
    private BigDecimal apolicepgtovalor;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGEROUTRO", nullable=false)
    private Geroutro geroutro;
    @ManyToOne
    @JoinColumn(name="CODFINLANCAMENTO")
    private Finlancamento finlancamento;
    @ManyToOne
    @JoinColumn(name="CODCTBBEMDET")
    private Ctbbemdet ctbbemdet;

    /** Default constructor. */
    public Ctbbemoutroapolice() {
        super();
    }

    /**
     * Access method for codctbbemoutroapolice.
     *
     * @return the current value of codctbbemoutroapolice
     */
    public int getCodctbbemoutroapolice() {
        return codctbbemoutroapolice;
    }

    /**
     * Setter method for codctbbemoutroapolice.
     *
     * @param aCodctbbemoutroapolice the new value for codctbbemoutroapolice
     */
    public void setCodctbbemoutroapolice(int aCodctbbemoutroapolice) {
        codctbbemoutroapolice = aCodctbbemoutroapolice;
    }

    /**
     * Access method for descctbbemoutroapolice.
     *
     * @return the current value of descctbbemoutroapolice
     */
    public String getDescctbbemoutroapolice() {
        return descctbbemoutroapolice;
    }

    /**
     * Setter method for descctbbemoutroapolice.
     *
     * @param aDescctbbemoutroapolice the new value for descctbbemoutroapolice
     */
    public void setDescctbbemoutroapolice(String aDescctbbemoutroapolice) {
        descctbbemoutroapolice = aDescctbbemoutroapolice;
    }

    /**
     * Access method for apoliceinicial.
     *
     * @return the current value of apoliceinicial
     */
    public Timestamp getApoliceinicial() {
        return apoliceinicial;
    }

    /**
     * Setter method for apoliceinicial.
     *
     * @param aApoliceinicial the new value for apoliceinicial
     */
    public void setApoliceinicial(Timestamp aApoliceinicial) {
        apoliceinicial = aApoliceinicial;
    }

    /**
     * Access method for apolicefinal.
     *
     * @return the current value of apolicefinal
     */
    public Timestamp getApolicefinal() {
        return apolicefinal;
    }

    /**
     * Setter method for apolicefinal.
     *
     * @param aApolicefinal the new value for apolicefinal
     */
    public void setApolicefinal(Timestamp aApolicefinal) {
        apolicefinal = aApolicefinal;
    }

    /**
     * Access method for apolicepgtodata.
     *
     * @return the current value of apolicepgtodata
     */
    public Timestamp getApolicepgtodata() {
        return apolicepgtodata;
    }

    /**
     * Setter method for apolicepgtodata.
     *
     * @param aApolicepgtodata the new value for apolicepgtodata
     */
    public void setApolicepgtodata(Timestamp aApolicepgtodata) {
        apolicepgtodata = aApolicepgtodata;
    }

    /**
     * Access method for apolicepgtovalor.
     *
     * @return the current value of apolicepgtovalor
     */
    public BigDecimal getApolicepgtovalor() {
        return apolicepgtovalor;
    }

    /**
     * Setter method for apolicepgtovalor.
     *
     * @param aApolicepgtovalor the new value for apolicepgtovalor
     */
    public void setApolicepgtovalor(BigDecimal aApolicepgtovalor) {
        apolicepgtovalor = aApolicepgtovalor;
    }

    /**
     * Access method for geroutro.
     *
     * @return the current value of geroutro
     */
    public Geroutro getGeroutro() {
        return geroutro;
    }

    /**
     * Setter method for geroutro.
     *
     * @param aGeroutro the new value for geroutro
     */
    public void setGeroutro(Geroutro aGeroutro) {
        geroutro = aGeroutro;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Finlancamento getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Finlancamento aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for ctbbemdet.
     *
     * @return the current value of ctbbemdet
     */
    public Ctbbemdet getCtbbemdet() {
        return ctbbemdet;
    }

    /**
     * Setter method for ctbbemdet.
     *
     * @param aCtbbemdet the new value for ctbbemdet
     */
    public void setCtbbemdet(Ctbbemdet aCtbbemdet) {
        ctbbemdet = aCtbbemdet;
    }

    /**
     * Compares the key for this instance with another Ctbbemoutroapolice.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbbemoutroapolice and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbbemoutroapolice)) {
            return false;
        }
        Ctbbemoutroapolice that = (Ctbbemoutroapolice) other;
        if (this.getCodctbbemoutroapolice() != that.getCodctbbemoutroapolice()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbbemoutroapolice.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbbemoutroapolice)) return false;
        return this.equalKeys(other) && ((Ctbbemoutroapolice)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbbemoutroapolice();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbbemoutroapolice |");
        sb.append(" codctbbemoutroapolice=").append(getCodctbbemoutroapolice());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbbemoutroapolice", Integer.valueOf(getCodctbbemoutroapolice()));
        return ret;
    }

}
