package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CTBBEMTIPO")
public class Ctbbemtipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbbemtipo";

    @Id
    @Column(name="CODCTBBEMTIPO", unique=true, nullable=false, precision=10)
    private int codctbbemtipo;
    @Column(name="DESCCTBBEMTIPO", nullable=false, length=250)
    private String descctbbemtipo;
    @Column(name="TIPOCTBBEMTIPO", precision=5)
    private short tipoctbbemtipo;
    @OneToMany(mappedBy="ctbbemtipo")
    private Set<Prodapolice> prodapolice;
    @OneToMany(mappedBy="ctbbemtipo")
    private Set<Prodgrupo> prodgrupo;
    @OneToMany(mappedBy="ctbbemtipo")
    private Set<Prodproduto> prodproduto;

    /** Default constructor. */
    public Ctbbemtipo() {
        super();
    }

    /**
     * Access method for codctbbemtipo.
     *
     * @return the current value of codctbbemtipo
     */
    public int getCodctbbemtipo() {
        return codctbbemtipo;
    }

    /**
     * Setter method for codctbbemtipo.
     *
     * @param aCodctbbemtipo the new value for codctbbemtipo
     */
    public void setCodctbbemtipo(int aCodctbbemtipo) {
        codctbbemtipo = aCodctbbemtipo;
    }

    /**
     * Access method for descctbbemtipo.
     *
     * @return the current value of descctbbemtipo
     */
    public String getDescctbbemtipo() {
        return descctbbemtipo;
    }

    /**
     * Setter method for descctbbemtipo.
     *
     * @param aDescctbbemtipo the new value for descctbbemtipo
     */
    public void setDescctbbemtipo(String aDescctbbemtipo) {
        descctbbemtipo = aDescctbbemtipo;
    }

    /**
     * Access method for tipoctbbemtipo.
     *
     * @return the current value of tipoctbbemtipo
     */
    public short getTipoctbbemtipo() {
        return tipoctbbemtipo;
    }

    /**
     * Setter method for tipoctbbemtipo.
     *
     * @param aTipoctbbemtipo the new value for tipoctbbemtipo
     */
    public void setTipoctbbemtipo(short aTipoctbbemtipo) {
        tipoctbbemtipo = aTipoctbbemtipo;
    }

    /**
     * Access method for prodapolice.
     *
     * @return the current value of prodapolice
     */
    public Set<Prodapolice> getProdapolice() {
        return prodapolice;
    }

    /**
     * Setter method for prodapolice.
     *
     * @param aProdapolice the new value for prodapolice
     */
    public void setProdapolice(Set<Prodapolice> aProdapolice) {
        prodapolice = aProdapolice;
    }

    /**
     * Access method for prodgrupo.
     *
     * @return the current value of prodgrupo
     */
    public Set<Prodgrupo> getProdgrupo() {
        return prodgrupo;
    }

    /**
     * Setter method for prodgrupo.
     *
     * @param aProdgrupo the new value for prodgrupo
     */
    public void setProdgrupo(Set<Prodgrupo> aProdgrupo) {
        prodgrupo = aProdgrupo;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Ctbbemtipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbbemtipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbbemtipo)) {
            return false;
        }
        Ctbbemtipo that = (Ctbbemtipo) other;
        if (this.getCodctbbemtipo() != that.getCodctbbemtipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbbemtipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbbemtipo)) return false;
        return this.equalKeys(other) && ((Ctbbemtipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbbemtipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbbemtipo |");
        sb.append(" codctbbemtipo=").append(getCodctbbemtipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbbemtipo", Integer.valueOf(getCodctbbemtipo()));
        return ret;
    }

}
