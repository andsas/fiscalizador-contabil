package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CTBBEMVEICAPOLICE")
public class Ctbbemveicapolice implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbbemveicapolice";

    @Id
    @Column(name="CODCTBBEMVEICAPOLICE", unique=true, nullable=false, precision=10)
    private int codctbbemveicapolice;
    @Column(name="DESCCTBBEMVEICAPOLICE", nullable=false, length=250)
    private String descctbbemveicapolice;
    @Column(name="APOLICEINICIO", nullable=false)
    private Timestamp apoliceinicio;
    @Column(name="APOLICEFINAL", nullable=false)
    private Timestamp apolicefinal;
    @Column(name="APOLICEPGTOVALOR", nullable=false, precision=15, scale=4)
    private BigDecimal apolicepgtovalor;
    @Column(name="APOLICEPGTODATA", nullable=false)
    private Timestamp apolicepgtodata;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODAPOLICE", nullable=false)
    private Prodapolice prodapolice;
    @ManyToOne
    @JoinColumn(name="CODFINLANCAMENTO")
    private Finlancamento finlancamento;
    @ManyToOne
    @JoinColumn(name="CODCTBBEMDET")
    private Ctbbemdet ctbbemdet;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGERVEICULO", nullable=false)
    private Gerveiculo gerveiculo;

    /** Default constructor. */
    public Ctbbemveicapolice() {
        super();
    }

    /**
     * Access method for codctbbemveicapolice.
     *
     * @return the current value of codctbbemveicapolice
     */
    public int getCodctbbemveicapolice() {
        return codctbbemveicapolice;
    }

    /**
     * Setter method for codctbbemveicapolice.
     *
     * @param aCodctbbemveicapolice the new value for codctbbemveicapolice
     */
    public void setCodctbbemveicapolice(int aCodctbbemveicapolice) {
        codctbbemveicapolice = aCodctbbemveicapolice;
    }

    /**
     * Access method for descctbbemveicapolice.
     *
     * @return the current value of descctbbemveicapolice
     */
    public String getDescctbbemveicapolice() {
        return descctbbemveicapolice;
    }

    /**
     * Setter method for descctbbemveicapolice.
     *
     * @param aDescctbbemveicapolice the new value for descctbbemveicapolice
     */
    public void setDescctbbemveicapolice(String aDescctbbemveicapolice) {
        descctbbemveicapolice = aDescctbbemveicapolice;
    }

    /**
     * Access method for apoliceinicio.
     *
     * @return the current value of apoliceinicio
     */
    public Timestamp getApoliceinicio() {
        return apoliceinicio;
    }

    /**
     * Setter method for apoliceinicio.
     *
     * @param aApoliceinicio the new value for apoliceinicio
     */
    public void setApoliceinicio(Timestamp aApoliceinicio) {
        apoliceinicio = aApoliceinicio;
    }

    /**
     * Access method for apolicefinal.
     *
     * @return the current value of apolicefinal
     */
    public Timestamp getApolicefinal() {
        return apolicefinal;
    }

    /**
     * Setter method for apolicefinal.
     *
     * @param aApolicefinal the new value for apolicefinal
     */
    public void setApolicefinal(Timestamp aApolicefinal) {
        apolicefinal = aApolicefinal;
    }

    /**
     * Access method for apolicepgtovalor.
     *
     * @return the current value of apolicepgtovalor
     */
    public BigDecimal getApolicepgtovalor() {
        return apolicepgtovalor;
    }

    /**
     * Setter method for apolicepgtovalor.
     *
     * @param aApolicepgtovalor the new value for apolicepgtovalor
     */
    public void setApolicepgtovalor(BigDecimal aApolicepgtovalor) {
        apolicepgtovalor = aApolicepgtovalor;
    }

    /**
     * Access method for apolicepgtodata.
     *
     * @return the current value of apolicepgtodata
     */
    public Timestamp getApolicepgtodata() {
        return apolicepgtodata;
    }

    /**
     * Setter method for apolicepgtodata.
     *
     * @param aApolicepgtodata the new value for apolicepgtodata
     */
    public void setApolicepgtodata(Timestamp aApolicepgtodata) {
        apolicepgtodata = aApolicepgtodata;
    }

    /**
     * Access method for prodapolice.
     *
     * @return the current value of prodapolice
     */
    public Prodapolice getProdapolice() {
        return prodapolice;
    }

    /**
     * Setter method for prodapolice.
     *
     * @param aProdapolice the new value for prodapolice
     */
    public void setProdapolice(Prodapolice aProdapolice) {
        prodapolice = aProdapolice;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Finlancamento getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Finlancamento aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for ctbbemdet.
     *
     * @return the current value of ctbbemdet
     */
    public Ctbbemdet getCtbbemdet() {
        return ctbbemdet;
    }

    /**
     * Setter method for ctbbemdet.
     *
     * @param aCtbbemdet the new value for ctbbemdet
     */
    public void setCtbbemdet(Ctbbemdet aCtbbemdet) {
        ctbbemdet = aCtbbemdet;
    }

    /**
     * Access method for gerveiculo.
     *
     * @return the current value of gerveiculo
     */
    public Gerveiculo getGerveiculo() {
        return gerveiculo;
    }

    /**
     * Setter method for gerveiculo.
     *
     * @param aGerveiculo the new value for gerveiculo
     */
    public void setGerveiculo(Gerveiculo aGerveiculo) {
        gerveiculo = aGerveiculo;
    }

    /**
     * Compares the key for this instance with another Ctbbemveicapolice.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbbemveicapolice and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbbemveicapolice)) {
            return false;
        }
        Ctbbemveicapolice that = (Ctbbemveicapolice) other;
        if (this.getCodctbbemveicapolice() != that.getCodctbbemveicapolice()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbbemveicapolice.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbbemveicapolice)) return false;
        return this.equalKeys(other) && ((Ctbbemveicapolice)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbbemveicapolice();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbbemveicapolice |");
        sb.append(" codctbbemveicapolice=").append(getCodctbbemveicapolice());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbbemveicapolice", Integer.valueOf(getCodctbbemveicapolice()));
        return ret;
    }

}
