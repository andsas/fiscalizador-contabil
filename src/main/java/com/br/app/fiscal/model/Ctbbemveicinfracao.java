package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CTBBEMVEICINFRACAO")
public class Ctbbemveicinfracao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbbemveicinfracao";

    @Id
    @Column(name="CODCTBBEMVEICINFRACAO", unique=true, nullable=false, precision=10)
    private int codctbbemveicinfracao;
    @Column(name="DESCCTBBEMVEICINFRACAO", nullable=false, length=250)
    private String descctbbemveicinfracao;
    @Column(name="INFRACAOAUTO", length=40)
    private String infracaoauto;
    @Column(name="INFRACAODATA", nullable=false)
    private Timestamp infracaodata;
    @Column(name="INFRACAOTIPO", precision=5)
    private short infracaotipo;
    @Column(name="INFRACAOAIVINCULADO", length=40)
    private String infracaoaivinculado;
    @Column(name="INFRACAOEXPDATA")
    private Timestamp infracaoexpdata;
    @Column(name="AGENTEHABVALIDADE")
    private Timestamp agentehabvalidade;
    @Column(name="PROCDATALIMITE")
    private Timestamp procdatalimite;
    @Column(name="PROCPROTOCOLO", length=40)
    private String procprotocolo;
    @Column(name="PROCVALORCDESC", precision=15, scale=4)
    private BigDecimal procvalorcdesc;
    @Column(name="PROCVALORSDESC", precision=15, scale=4)
    private BigDecimal procvalorsdesc;
    @Column(name="PROCPGTOVALOR", precision=15, scale=4)
    private BigDecimal procpgtovalor;
    @Column(name="PROCPGTODATA")
    private Timestamp procpgtodata;
    @Column(name="INDCONDDATALIMITE")
    private Timestamp indconddatalimite;
    @Column(name="INDCONDPROTOCOLO", length=40)
    private String indcondprotocolo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGERVEICULO", nullable=false)
    private Gerveiculo gerveiculo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCTBVEICINFRACAO", nullable=false)
    private Ctbveicinfracao ctbveicinfracao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFLOGRADOURO", nullable=false)
    private Conflogradouro conflogradouro;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODFINLANCAMENTO")
    private Finlancamento finlancamento;
    @ManyToOne
    @JoinColumn(name="CODCTBBEMDET")
    private Ctbbemdet ctbbemdet;

    /** Default constructor. */
    public Ctbbemveicinfracao() {
        super();
    }

    /**
     * Access method for codctbbemveicinfracao.
     *
     * @return the current value of codctbbemveicinfracao
     */
    public int getCodctbbemveicinfracao() {
        return codctbbemveicinfracao;
    }

    /**
     * Setter method for codctbbemveicinfracao.
     *
     * @param aCodctbbemveicinfracao the new value for codctbbemveicinfracao
     */
    public void setCodctbbemveicinfracao(int aCodctbbemveicinfracao) {
        codctbbemveicinfracao = aCodctbbemveicinfracao;
    }

    /**
     * Access method for descctbbemveicinfracao.
     *
     * @return the current value of descctbbemveicinfracao
     */
    public String getDescctbbemveicinfracao() {
        return descctbbemveicinfracao;
    }

    /**
     * Setter method for descctbbemveicinfracao.
     *
     * @param aDescctbbemveicinfracao the new value for descctbbemveicinfracao
     */
    public void setDescctbbemveicinfracao(String aDescctbbemveicinfracao) {
        descctbbemveicinfracao = aDescctbbemveicinfracao;
    }

    /**
     * Access method for infracaoauto.
     *
     * @return the current value of infracaoauto
     */
    public String getInfracaoauto() {
        return infracaoauto;
    }

    /**
     * Setter method for infracaoauto.
     *
     * @param aInfracaoauto the new value for infracaoauto
     */
    public void setInfracaoauto(String aInfracaoauto) {
        infracaoauto = aInfracaoauto;
    }

    /**
     * Access method for infracaodata.
     *
     * @return the current value of infracaodata
     */
    public Timestamp getInfracaodata() {
        return infracaodata;
    }

    /**
     * Setter method for infracaodata.
     *
     * @param aInfracaodata the new value for infracaodata
     */
    public void setInfracaodata(Timestamp aInfracaodata) {
        infracaodata = aInfracaodata;
    }

    /**
     * Access method for infracaotipo.
     *
     * @return the current value of infracaotipo
     */
    public short getInfracaotipo() {
        return infracaotipo;
    }

    /**
     * Setter method for infracaotipo.
     *
     * @param aInfracaotipo the new value for infracaotipo
     */
    public void setInfracaotipo(short aInfracaotipo) {
        infracaotipo = aInfracaotipo;
    }

    /**
     * Access method for infracaoaivinculado.
     *
     * @return the current value of infracaoaivinculado
     */
    public String getInfracaoaivinculado() {
        return infracaoaivinculado;
    }

    /**
     * Setter method for infracaoaivinculado.
     *
     * @param aInfracaoaivinculado the new value for infracaoaivinculado
     */
    public void setInfracaoaivinculado(String aInfracaoaivinculado) {
        infracaoaivinculado = aInfracaoaivinculado;
    }

    /**
     * Access method for infracaoexpdata.
     *
     * @return the current value of infracaoexpdata
     */
    public Timestamp getInfracaoexpdata() {
        return infracaoexpdata;
    }

    /**
     * Setter method for infracaoexpdata.
     *
     * @param aInfracaoexpdata the new value for infracaoexpdata
     */
    public void setInfracaoexpdata(Timestamp aInfracaoexpdata) {
        infracaoexpdata = aInfracaoexpdata;
    }

    /**
     * Access method for agentehabvalidade.
     *
     * @return the current value of agentehabvalidade
     */
    public Timestamp getAgentehabvalidade() {
        return agentehabvalidade;
    }

    /**
     * Setter method for agentehabvalidade.
     *
     * @param aAgentehabvalidade the new value for agentehabvalidade
     */
    public void setAgentehabvalidade(Timestamp aAgentehabvalidade) {
        agentehabvalidade = aAgentehabvalidade;
    }

    /**
     * Access method for procdatalimite.
     *
     * @return the current value of procdatalimite
     */
    public Timestamp getProcdatalimite() {
        return procdatalimite;
    }

    /**
     * Setter method for procdatalimite.
     *
     * @param aProcdatalimite the new value for procdatalimite
     */
    public void setProcdatalimite(Timestamp aProcdatalimite) {
        procdatalimite = aProcdatalimite;
    }

    /**
     * Access method for procprotocolo.
     *
     * @return the current value of procprotocolo
     */
    public String getProcprotocolo() {
        return procprotocolo;
    }

    /**
     * Setter method for procprotocolo.
     *
     * @param aProcprotocolo the new value for procprotocolo
     */
    public void setProcprotocolo(String aProcprotocolo) {
        procprotocolo = aProcprotocolo;
    }

    /**
     * Access method for procvalorcdesc.
     *
     * @return the current value of procvalorcdesc
     */
    public BigDecimal getProcvalorcdesc() {
        return procvalorcdesc;
    }

    /**
     * Setter method for procvalorcdesc.
     *
     * @param aProcvalorcdesc the new value for procvalorcdesc
     */
    public void setProcvalorcdesc(BigDecimal aProcvalorcdesc) {
        procvalorcdesc = aProcvalorcdesc;
    }

    /**
     * Access method for procvalorsdesc.
     *
     * @return the current value of procvalorsdesc
     */
    public BigDecimal getProcvalorsdesc() {
        return procvalorsdesc;
    }

    /**
     * Setter method for procvalorsdesc.
     *
     * @param aProcvalorsdesc the new value for procvalorsdesc
     */
    public void setProcvalorsdesc(BigDecimal aProcvalorsdesc) {
        procvalorsdesc = aProcvalorsdesc;
    }

    /**
     * Access method for procpgtovalor.
     *
     * @return the current value of procpgtovalor
     */
    public BigDecimal getProcpgtovalor() {
        return procpgtovalor;
    }

    /**
     * Setter method for procpgtovalor.
     *
     * @param aProcpgtovalor the new value for procpgtovalor
     */
    public void setProcpgtovalor(BigDecimal aProcpgtovalor) {
        procpgtovalor = aProcpgtovalor;
    }

    /**
     * Access method for procpgtodata.
     *
     * @return the current value of procpgtodata
     */
    public Timestamp getProcpgtodata() {
        return procpgtodata;
    }

    /**
     * Setter method for procpgtodata.
     *
     * @param aProcpgtodata the new value for procpgtodata
     */
    public void setProcpgtodata(Timestamp aProcpgtodata) {
        procpgtodata = aProcpgtodata;
    }

    /**
     * Access method for indconddatalimite.
     *
     * @return the current value of indconddatalimite
     */
    public Timestamp getIndconddatalimite() {
        return indconddatalimite;
    }

    /**
     * Setter method for indconddatalimite.
     *
     * @param aIndconddatalimite the new value for indconddatalimite
     */
    public void setIndconddatalimite(Timestamp aIndconddatalimite) {
        indconddatalimite = aIndconddatalimite;
    }

    /**
     * Access method for indcondprotocolo.
     *
     * @return the current value of indcondprotocolo
     */
    public String getIndcondprotocolo() {
        return indcondprotocolo;
    }

    /**
     * Setter method for indcondprotocolo.
     *
     * @param aIndcondprotocolo the new value for indcondprotocolo
     */
    public void setIndcondprotocolo(String aIndcondprotocolo) {
        indcondprotocolo = aIndcondprotocolo;
    }

    /**
     * Access method for gerveiculo.
     *
     * @return the current value of gerveiculo
     */
    public Gerveiculo getGerveiculo() {
        return gerveiculo;
    }

    /**
     * Setter method for gerveiculo.
     *
     * @param aGerveiculo the new value for gerveiculo
     */
    public void setGerveiculo(Gerveiculo aGerveiculo) {
        gerveiculo = aGerveiculo;
    }

    /**
     * Access method for ctbveicinfracao.
     *
     * @return the current value of ctbveicinfracao
     */
    public Ctbveicinfracao getCtbveicinfracao() {
        return ctbveicinfracao;
    }

    /**
     * Setter method for ctbveicinfracao.
     *
     * @param aCtbveicinfracao the new value for ctbveicinfracao
     */
    public void setCtbveicinfracao(Ctbveicinfracao aCtbveicinfracao) {
        ctbveicinfracao = aCtbveicinfracao;
    }

    /**
     * Access method for conflogradouro.
     *
     * @return the current value of conflogradouro
     */
    public Conflogradouro getConflogradouro() {
        return conflogradouro;
    }

    /**
     * Setter method for conflogradouro.
     *
     * @param aConflogradouro the new value for conflogradouro
     */
    public void setConflogradouro(Conflogradouro aConflogradouro) {
        conflogradouro = aConflogradouro;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Finlancamento getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Finlancamento aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for ctbbemdet.
     *
     * @return the current value of ctbbemdet
     */
    public Ctbbemdet getCtbbemdet() {
        return ctbbemdet;
    }

    /**
     * Setter method for ctbbemdet.
     *
     * @param aCtbbemdet the new value for ctbbemdet
     */
    public void setCtbbemdet(Ctbbemdet aCtbbemdet) {
        ctbbemdet = aCtbbemdet;
    }

    /**
     * Compares the key for this instance with another Ctbbemveicinfracao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbbemveicinfracao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbbemveicinfracao)) {
            return false;
        }
        Ctbbemveicinfracao that = (Ctbbemveicinfracao) other;
        if (this.getCodctbbemveicinfracao() != that.getCodctbbemveicinfracao()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbbemveicinfracao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbbemveicinfracao)) return false;
        return this.equalKeys(other) && ((Ctbbemveicinfracao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbbemveicinfracao();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbbemveicinfracao |");
        sb.append(" codctbbemveicinfracao=").append(getCodctbbemveicinfracao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbbemveicinfracao", Integer.valueOf(getCodctbbemveicinfracao()));
        return ret;
    }

}
