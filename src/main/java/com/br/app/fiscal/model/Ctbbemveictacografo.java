package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CTBBEMVEICTACOGRAFO")
public class Ctbbemveictacografo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbbemveictacografo";

    @Id
    @Column(name="CODCTBBEMVEICTACOGRAFO", unique=true, nullable=false, precision=10)
    private int codctbbemveictacografo;
    @Column(name="TACONUMSERIE", nullable=false, length=40)
    private String taconumserie;
    @Column(name="TACOAFERICAO", precision=15, scale=4)
    private BigDecimal tacoafericao;
    @Column(name="TACOPRECOAFERICAO", precision=15, scale=4)
    private BigDecimal tacoprecoafericao;
    @Column(name="TACOTAXAINMETRO", nullable=false, precision=15, scale=4)
    private BigDecimal tacotaxainmetro;
    @Column(name="TACODATAPGTOINMETRO", nullable=false)
    private Timestamp tacodatapgtoinmetro;
    @Column(name="TACOCERTIFICADO", length=250)
    private String tacocertificado;
    @Column(name="TACOVALIDADE", nullable=false)
    private Timestamp tacovalidade;
    @Column(name="OBS")
    private String obs;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGERVEICULO", nullable=false)
    private Gerveiculo gerveiculo;
    @ManyToOne
    @JoinColumn(name="TACOCODPRODMARCA")
    private Prodmarca prodmarca;
    @ManyToOne
    @JoinColumn(name="TACOCODPRODMODELO")
    private Prodmodelo prodmodelo;
    @ManyToOne
    @JoinColumn(name="CODFINLANCAMENTO")
    private Finlancamento finlancamento;
    @ManyToOne
    @JoinColumn(name="CODAFERICAOFINLANCAMENTO")
    private Finlancamento finlancamento2;
    @ManyToOne
    @JoinColumn(name="CODCTBBEMDET")
    private Ctbbemdet ctbbemdet;

    /** Default constructor. */
    public Ctbbemveictacografo() {
        super();
    }

    /**
     * Access method for codctbbemveictacografo.
     *
     * @return the current value of codctbbemveictacografo
     */
    public int getCodctbbemveictacografo() {
        return codctbbemveictacografo;
    }

    /**
     * Setter method for codctbbemveictacografo.
     *
     * @param aCodctbbemveictacografo the new value for codctbbemveictacografo
     */
    public void setCodctbbemveictacografo(int aCodctbbemveictacografo) {
        codctbbemveictacografo = aCodctbbemveictacografo;
    }

    /**
     * Access method for taconumserie.
     *
     * @return the current value of taconumserie
     */
    public String getTaconumserie() {
        return taconumserie;
    }

    /**
     * Setter method for taconumserie.
     *
     * @param aTaconumserie the new value for taconumserie
     */
    public void setTaconumserie(String aTaconumserie) {
        taconumserie = aTaconumserie;
    }

    /**
     * Access method for tacoafericao.
     *
     * @return the current value of tacoafericao
     */
    public BigDecimal getTacoafericao() {
        return tacoafericao;
    }

    /**
     * Setter method for tacoafericao.
     *
     * @param aTacoafericao the new value for tacoafericao
     */
    public void setTacoafericao(BigDecimal aTacoafericao) {
        tacoafericao = aTacoafericao;
    }

    /**
     * Access method for tacoprecoafericao.
     *
     * @return the current value of tacoprecoafericao
     */
    public BigDecimal getTacoprecoafericao() {
        return tacoprecoafericao;
    }

    /**
     * Setter method for tacoprecoafericao.
     *
     * @param aTacoprecoafericao the new value for tacoprecoafericao
     */
    public void setTacoprecoafericao(BigDecimal aTacoprecoafericao) {
        tacoprecoafericao = aTacoprecoafericao;
    }

    /**
     * Access method for tacotaxainmetro.
     *
     * @return the current value of tacotaxainmetro
     */
    public BigDecimal getTacotaxainmetro() {
        return tacotaxainmetro;
    }

    /**
     * Setter method for tacotaxainmetro.
     *
     * @param aTacotaxainmetro the new value for tacotaxainmetro
     */
    public void setTacotaxainmetro(BigDecimal aTacotaxainmetro) {
        tacotaxainmetro = aTacotaxainmetro;
    }

    /**
     * Access method for tacodatapgtoinmetro.
     *
     * @return the current value of tacodatapgtoinmetro
     */
    public Timestamp getTacodatapgtoinmetro() {
        return tacodatapgtoinmetro;
    }

    /**
     * Setter method for tacodatapgtoinmetro.
     *
     * @param aTacodatapgtoinmetro the new value for tacodatapgtoinmetro
     */
    public void setTacodatapgtoinmetro(Timestamp aTacodatapgtoinmetro) {
        tacodatapgtoinmetro = aTacodatapgtoinmetro;
    }

    /**
     * Access method for tacocertificado.
     *
     * @return the current value of tacocertificado
     */
    public String getTacocertificado() {
        return tacocertificado;
    }

    /**
     * Setter method for tacocertificado.
     *
     * @param aTacocertificado the new value for tacocertificado
     */
    public void setTacocertificado(String aTacocertificado) {
        tacocertificado = aTacocertificado;
    }

    /**
     * Access method for tacovalidade.
     *
     * @return the current value of tacovalidade
     */
    public Timestamp getTacovalidade() {
        return tacovalidade;
    }

    /**
     * Setter method for tacovalidade.
     *
     * @param aTacovalidade the new value for tacovalidade
     */
    public void setTacovalidade(Timestamp aTacovalidade) {
        tacovalidade = aTacovalidade;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for gerveiculo.
     *
     * @return the current value of gerveiculo
     */
    public Gerveiculo getGerveiculo() {
        return gerveiculo;
    }

    /**
     * Setter method for gerveiculo.
     *
     * @param aGerveiculo the new value for gerveiculo
     */
    public void setGerveiculo(Gerveiculo aGerveiculo) {
        gerveiculo = aGerveiculo;
    }

    /**
     * Access method for prodmarca.
     *
     * @return the current value of prodmarca
     */
    public Prodmarca getProdmarca() {
        return prodmarca;
    }

    /**
     * Setter method for prodmarca.
     *
     * @param aProdmarca the new value for prodmarca
     */
    public void setProdmarca(Prodmarca aProdmarca) {
        prodmarca = aProdmarca;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Prodmodelo getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Prodmodelo aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Finlancamento getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Finlancamento aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for finlancamento2.
     *
     * @return the current value of finlancamento2
     */
    public Finlancamento getFinlancamento2() {
        return finlancamento2;
    }

    /**
     * Setter method for finlancamento2.
     *
     * @param aFinlancamento2 the new value for finlancamento2
     */
    public void setFinlancamento2(Finlancamento aFinlancamento2) {
        finlancamento2 = aFinlancamento2;
    }

    /**
     * Access method for ctbbemdet.
     *
     * @return the current value of ctbbemdet
     */
    public Ctbbemdet getCtbbemdet() {
        return ctbbemdet;
    }

    /**
     * Setter method for ctbbemdet.
     *
     * @param aCtbbemdet the new value for ctbbemdet
     */
    public void setCtbbemdet(Ctbbemdet aCtbbemdet) {
        ctbbemdet = aCtbbemdet;
    }

    /**
     * Compares the key for this instance with another Ctbbemveictacografo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbbemveictacografo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbbemveictacografo)) {
            return false;
        }
        Ctbbemveictacografo that = (Ctbbemveictacografo) other;
        if (this.getCodctbbemveictacografo() != that.getCodctbbemveictacografo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbbemveictacografo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbbemveictacografo)) return false;
        return this.equalKeys(other) && ((Ctbbemveictacografo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbbemveictacografo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbbemveictacografo |");
        sb.append(" codctbbemveictacografo=").append(getCodctbbemveictacografo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbbemveictacografo", Integer.valueOf(getCodctbbemveictacografo()));
        return ret;
    }

}
