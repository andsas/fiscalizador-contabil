package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CTBFECHAMENTO")
public class Ctbfechamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbfechamento";

    @Id
    @Column(name="CODCTBFECHAMENTO", unique=true, nullable=false, precision=10)
    private int codctbfechamento;
    @Column(name="DATAINICIAL", nullable=false)
    private Timestamp datainicial;
    @Column(name="DATAFINAL", nullable=false)
    private Timestamp datafinal;
    @Column(name="OBSERVACOES")
    private String observacoes;
    @Column(name="DEBITOS", precision=15, scale=4)
    private BigDecimal debitos;
    @Column(name="CREDITOS", precision=15, scale=4)
    private BigDecimal creditos;
    @Column(name="SALDO", precision=15, scale=4)
    private BigDecimal saldo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIO", nullable=false)
    private Confusuario confusuario;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="CODAGCONTADOR")
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGRESPONSAVEL", nullable=false)
    private Agagente agagente2;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCTBPLANOCONTA", nullable=false)
    private Ctbplanoconta ctbplanoconta;

    /** Default constructor. */
    public Ctbfechamento() {
        super();
    }

    /**
     * Access method for codctbfechamento.
     *
     * @return the current value of codctbfechamento
     */
    public int getCodctbfechamento() {
        return codctbfechamento;
    }

    /**
     * Setter method for codctbfechamento.
     *
     * @param aCodctbfechamento the new value for codctbfechamento
     */
    public void setCodctbfechamento(int aCodctbfechamento) {
        codctbfechamento = aCodctbfechamento;
    }

    /**
     * Access method for datainicial.
     *
     * @return the current value of datainicial
     */
    public Timestamp getDatainicial() {
        return datainicial;
    }

    /**
     * Setter method for datainicial.
     *
     * @param aDatainicial the new value for datainicial
     */
    public void setDatainicial(Timestamp aDatainicial) {
        datainicial = aDatainicial;
    }

    /**
     * Access method for datafinal.
     *
     * @return the current value of datafinal
     */
    public Timestamp getDatafinal() {
        return datafinal;
    }

    /**
     * Setter method for datafinal.
     *
     * @param aDatafinal the new value for datafinal
     */
    public void setDatafinal(Timestamp aDatafinal) {
        datafinal = aDatafinal;
    }

    /**
     * Access method for observacoes.
     *
     * @return the current value of observacoes
     */
    public String getObservacoes() {
        return observacoes;
    }

    /**
     * Setter method for observacoes.
     *
     * @param aObservacoes the new value for observacoes
     */
    public void setObservacoes(String aObservacoes) {
        observacoes = aObservacoes;
    }

    /**
     * Access method for debitos.
     *
     * @return the current value of debitos
     */
    public BigDecimal getDebitos() {
        return debitos;
    }

    /**
     * Setter method for debitos.
     *
     * @param aDebitos the new value for debitos
     */
    public void setDebitos(BigDecimal aDebitos) {
        debitos = aDebitos;
    }

    /**
     * Access method for creditos.
     *
     * @return the current value of creditos
     */
    public BigDecimal getCreditos() {
        return creditos;
    }

    /**
     * Setter method for creditos.
     *
     * @param aCreditos the new value for creditos
     */
    public void setCreditos(BigDecimal aCreditos) {
        creditos = aCreditos;
    }

    /**
     * Access method for saldo.
     *
     * @return the current value of saldo
     */
    public BigDecimal getSaldo() {
        return saldo;
    }

    /**
     * Setter method for saldo.
     *
     * @param aSaldo the new value for saldo
     */
    public void setSaldo(BigDecimal aSaldo) {
        saldo = aSaldo;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Agagente getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Agagente aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Compares the key for this instance with another Ctbfechamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbfechamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbfechamento)) {
            return false;
        }
        Ctbfechamento that = (Ctbfechamento) other;
        if (this.getCodctbfechamento() != that.getCodctbfechamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbfechamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbfechamento)) return false;
        return this.equalKeys(other) && ((Ctbfechamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbfechamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbfechamento |");
        sb.append(" codctbfechamento=").append(getCodctbfechamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbfechamento", Integer.valueOf(getCodctbfechamento()));
        return ret;
    }

}
