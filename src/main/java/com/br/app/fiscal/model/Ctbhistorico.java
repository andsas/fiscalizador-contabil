package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CTBHISTORICO")
public class Ctbhistorico implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbhistorico";

    @Id
    @Column(name="CODCTBHISTORICO", unique=true, nullable=false, precision=10)
    private int codctbhistorico;
    @Column(name="DESCCTBHISTORICO", nullable=false, length=250)
    private String descctbhistorico;
    @Column(name="TIPOCTBHISTORICO", nullable=false, precision=5)
    private short tipoctbhistorico;
    @Column(name="FORMULA", nullable=false)
    private String formula;
    @OneToMany(mappedBy="ctbhistorico")
    private Set<Ctbplanoconta> ctbplanoconta;
    @OneToMany(mappedBy="ctbhistorico4")
    private Set<Folevento> folevento4;
    @OneToMany(mappedBy="ctbhistorico")
    private Set<Folevento> folevento;
    @OneToMany(mappedBy="ctbhistorico2")
    private Set<Folevento> folevento2;
    @OneToMany(mappedBy="ctbhistorico3")
    private Set<Folevento> folevento3;

    /** Default constructor. */
    public Ctbhistorico() {
        super();
    }

    /**
     * Access method for codctbhistorico.
     *
     * @return the current value of codctbhistorico
     */
    public int getCodctbhistorico() {
        return codctbhistorico;
    }

    /**
     * Setter method for codctbhistorico.
     *
     * @param aCodctbhistorico the new value for codctbhistorico
     */
    public void setCodctbhistorico(int aCodctbhistorico) {
        codctbhistorico = aCodctbhistorico;
    }

    /**
     * Access method for descctbhistorico.
     *
     * @return the current value of descctbhistorico
     */
    public String getDescctbhistorico() {
        return descctbhistorico;
    }

    /**
     * Setter method for descctbhistorico.
     *
     * @param aDescctbhistorico the new value for descctbhistorico
     */
    public void setDescctbhistorico(String aDescctbhistorico) {
        descctbhistorico = aDescctbhistorico;
    }

    /**
     * Access method for tipoctbhistorico.
     *
     * @return the current value of tipoctbhistorico
     */
    public short getTipoctbhistorico() {
        return tipoctbhistorico;
    }

    /**
     * Setter method for tipoctbhistorico.
     *
     * @param aTipoctbhistorico the new value for tipoctbhistorico
     */
    public void setTipoctbhistorico(short aTipoctbhistorico) {
        tipoctbhistorico = aTipoctbhistorico;
    }

    /**
     * Access method for formula.
     *
     * @return the current value of formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Setter method for formula.
     *
     * @param aFormula the new value for formula
     */
    public void setFormula(String aFormula) {
        formula = aFormula;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Set<Ctbplanoconta> getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Set<Ctbplanoconta> aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for folevento4.
     *
     * @return the current value of folevento4
     */
    public Set<Folevento> getFolevento4() {
        return folevento4;
    }

    /**
     * Setter method for folevento4.
     *
     * @param aFolevento4 the new value for folevento4
     */
    public void setFolevento4(Set<Folevento> aFolevento4) {
        folevento4 = aFolevento4;
    }

    /**
     * Access method for folevento.
     *
     * @return the current value of folevento
     */
    public Set<Folevento> getFolevento() {
        return folevento;
    }

    /**
     * Setter method for folevento.
     *
     * @param aFolevento the new value for folevento
     */
    public void setFolevento(Set<Folevento> aFolevento) {
        folevento = aFolevento;
    }

    /**
     * Access method for folevento2.
     *
     * @return the current value of folevento2
     */
    public Set<Folevento> getFolevento2() {
        return folevento2;
    }

    /**
     * Setter method for folevento2.
     *
     * @param aFolevento2 the new value for folevento2
     */
    public void setFolevento2(Set<Folevento> aFolevento2) {
        folevento2 = aFolevento2;
    }

    /**
     * Access method for folevento3.
     *
     * @return the current value of folevento3
     */
    public Set<Folevento> getFolevento3() {
        return folevento3;
    }

    /**
     * Setter method for folevento3.
     *
     * @param aFolevento3 the new value for folevento3
     */
    public void setFolevento3(Set<Folevento> aFolevento3) {
        folevento3 = aFolevento3;
    }

    /**
     * Compares the key for this instance with another Ctbhistorico.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbhistorico and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbhistorico)) {
            return false;
        }
        Ctbhistorico that = (Ctbhistorico) other;
        if (this.getCodctbhistorico() != that.getCodctbhistorico()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbhistorico.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbhistorico)) return false;
        return this.equalKeys(other) && ((Ctbhistorico)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbhistorico();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbhistorico |");
        sb.append(" codctbhistorico=").append(getCodctbhistorico());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbhistorico", Integer.valueOf(getCodctbhistorico()));
        return ret;
    }

}
