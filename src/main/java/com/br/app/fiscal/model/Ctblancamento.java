package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="CTBLANCAMENTO", indexes={@Index(name="ctblancamentoCtblancamentoIdx1", columnList="CODIGOUNICO,CONSOLIDADO")})
public class Ctblancamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctblancamento";

    @Id
    @Column(name="CODCTBLANCAMENTO", unique=true, nullable=false, precision=10)
    private int codctblancamento;
    @Column(name="DESCCTBLANCAMENTO", nullable=false, length=250)
    private String descctblancamento;
    @Column(name="TIPOCTBLANCAMENTO", nullable=false, precision=5)
    private short tipoctblancamento;
    @Column(name="DATA", nullable=false)
    private Timestamp data;
    @Column(name="VALOR", nullable=false, precision=15, scale=4)
    private BigDecimal valor;
    @Column(name="DESCCOMPLETA")
    private String desccompleta;
    @Column(name="CONSOLIDADO", precision=5)
    private short consolidado;
    @Column(name="CODIGOUNICO", length=20)
    private String codigounico;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAO")
    private Optransacao optransacao;
    @ManyToOne
    @JoinColumn(name="CODFINLANCAMENTO")
    private Finlancamento finlancamento;
    @ManyToOne
    @JoinColumn(name="CODCTBPLANOCONTADEB")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CODCTBPLANOCONTACRED")
    private Ctbplanoconta ctbplanoconta2;

    /** Default constructor. */
    public Ctblancamento() {
        super();
    }

    /**
     * Access method for codctblancamento.
     *
     * @return the current value of codctblancamento
     */
    public int getCodctblancamento() {
        return codctblancamento;
    }

    /**
     * Setter method for codctblancamento.
     *
     * @param aCodctblancamento the new value for codctblancamento
     */
    public void setCodctblancamento(int aCodctblancamento) {
        codctblancamento = aCodctblancamento;
    }

    /**
     * Access method for descctblancamento.
     *
     * @return the current value of descctblancamento
     */
    public String getDescctblancamento() {
        return descctblancamento;
    }

    /**
     * Setter method for descctblancamento.
     *
     * @param aDescctblancamento the new value for descctblancamento
     */
    public void setDescctblancamento(String aDescctblancamento) {
        descctblancamento = aDescctblancamento;
    }

    /**
     * Access method for tipoctblancamento.
     *
     * @return the current value of tipoctblancamento
     */
    public short getTipoctblancamento() {
        return tipoctblancamento;
    }

    /**
     * Setter method for tipoctblancamento.
     *
     * @param aTipoctblancamento the new value for tipoctblancamento
     */
    public void setTipoctblancamento(short aTipoctblancamento) {
        tipoctblancamento = aTipoctblancamento;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(BigDecimal aValor) {
        valor = aValor;
    }

    /**
     * Access method for desccompleta.
     *
     * @return the current value of desccompleta
     */
    public String getDesccompleta() {
        return desccompleta;
    }

    /**
     * Setter method for desccompleta.
     *
     * @param aDesccompleta the new value for desccompleta
     */
    public void setDesccompleta(String aDesccompleta) {
        desccompleta = aDesccompleta;
    }

    /**
     * Access method for consolidado.
     *
     * @return the current value of consolidado
     */
    public short getConsolidado() {
        return consolidado;
    }

    /**
     * Setter method for consolidado.
     *
     * @param aConsolidado the new value for consolidado
     */
    public void setConsolidado(short aConsolidado) {
        consolidado = aConsolidado;
    }

    /**
     * Access method for codigounico.
     *
     * @return the current value of codigounico
     */
    public String getCodigounico() {
        return codigounico;
    }

    /**
     * Setter method for codigounico.
     *
     * @param aCodigounico the new value for codigounico
     */
    public void setCodigounico(String aCodigounico) {
        codigounico = aCodigounico;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Finlancamento getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Finlancamento aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Compares the key for this instance with another Ctblancamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctblancamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctblancamento)) {
            return false;
        }
        Ctblancamento that = (Ctblancamento) other;
        if (this.getCodctblancamento() != that.getCodctblancamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctblancamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctblancamento)) return false;
        return this.equalKeys(other) && ((Ctblancamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctblancamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctblancamento |");
        sb.append(" codctblancamento=").append(getCodctblancamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctblancamento", Integer.valueOf(getCodctblancamento()));
        return ret;
    }

}
