package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CTBPLANOCONTA")
public class Ctbplanoconta implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbplanoconta";

    @Id
    @Column(name="CODCTBPLANOCONTA", unique=true, nullable=false, length=20)
    private String codctbplanoconta;
    @Column(name="DESCCTBPLANOCONTA", nullable=false, length=250)
    private String descctbplanoconta;
    @Column(name="NIVEL", precision=5)
    private short nivel;
    @Column(name="CODREDUZIDO", length=20)
    private String codreduzido;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @Column(name="DEBCRED", nullable=false, precision=5)
    private short debcred;
    @Column(name="DEMDVA", precision=5)
    private short demdva;
    @Column(name="DEMDESCDVA", length=250)
    private String demdescdva;
    @Column(name="DEMDMPL", precision=5)
    private short demdmpl;
    @Column(name="DEMDESCDMPL", length=250)
    private String demdescdmpl;
    @Column(name="DEMDFC", precision=5)
    private short demdfc;
    @Column(name="DEMDESCDFC", length=250)
    private String demdescdfc;
    @Column(name="DEMDRE", precision=5)
    private short demdre;
    @Column(name="DEMDESCDRE", length=250)
    private String demdescdre;
    @Column(name="DEMBP", precision=5)
    private short dembp;
    @Column(name="DEMDESCBP", length=250)
    private String demdescbp;
    @Column(name="DATAINICIAL")
    private Timestamp datainicial;
    @Column(name="DATAFINAL")
    private Timestamp datafinal;
    @Column(name="CODCTBPLANOSPEDCONT", length=20)
    private String codctbplanospedcont;
    @Column(name="CODCTBPLANOSUSEP", length=20)
    private String codctbplanosusep;
    @Column(name="CODCTBPLANOCOSIP", length=20)
    private String codctbplanocosip;
    @Column(name="FISRAZAO", precision=5)
    private short fisrazao;
    @Column(name="FISDIARIO", precision=5)
    private short fisdiario;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Agagente> agagente;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Agagente> agagente2;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Agconf> agconf;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Cobrforma> cobrforma;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Cobrforma> cobrforma2;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Ctbbemdet> ctbbemdet;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Ctbbemdet> ctbbemdet2;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Ctbfechamento> ctbfechamento;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Ctblancamento> ctblancamento;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Ctblancamento> ctblancamento2;
    @ManyToOne
    @JoinColumn(name="CODFINTIPO")
    private Fintipo fintipo;
    @OneToMany(mappedBy="ctbplanoconta40")
    private Set<Ctbplanoconta> ctbplanoconta41;
    @ManyToOne
    @JoinColumn(name="CODCTBPLANOTRANSITORIA")
    private Ctbplanoconta ctbplanoconta40;
    @OneToMany(mappedBy="ctbplanoconta42")
    private Set<Ctbplanoconta> ctbplanoconta43;
    @ManyToOne
    @JoinColumn(name="CODCTBPLANOAPURACAO")
    private Ctbplanoconta ctbplanoconta42;
    @ManyToOne
    @JoinColumn(name="CODCTBHISTORICO")
    private Ctbhistorico ctbhistorico;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @OneToMany(mappedBy="ctbplanoconta32")
    private Set<Ctbplanoconta> ctbplanoconta33;
    @ManyToOne
    @JoinColumn(name="CODCTBPARENTPLANOCONTA")
    private Ctbplanoconta ctbplanoconta32;
    @ManyToOne
    @JoinColumn(name="CODFINMOEDA")
    private Finmoeda finmoeda;
    @ManyToOne
    @JoinColumn(name="CODFINFLUXOPLANOCONTA")
    private Finfluxoplanoconta finfluxoplanoconta;
    @ManyToOne
    @JoinColumn(name="CODORCPLANOCONTA")
    private Orcplanoconta orcplanoconta;
    @OneToMany(mappedBy="ctbplanoconta34")
    private Set<Ctbplanoconta> ctbplanoconta35;
    @ManyToOne
    @JoinColumn(name="CODCTBPLANORETIF")
    private Ctbplanoconta ctbplanoconta34;
    @OneToMany(mappedBy="ctbplanoconta36")
    private Set<Ctbplanoconta> ctbplanoconta37;
    @ManyToOne
    @JoinColumn(name="CODCTBPLANOAJUSTE")
    private Ctbplanoconta ctbplanoconta36;
    @OneToMany(mappedBy="ctbplanoconta38")
    private Set<Ctbplanoconta> ctbplanoconta39;
    @ManyToOne
    @JoinColumn(name="CODCTBPLANOCOMPEN")
    private Ctbplanoconta ctbplanoconta38;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Finbaixa> finbaixa;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Finbaixa> finbaixa2;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Fincaixa> fincaixa;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Fincaixa> fincaixa2;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Finjuro> finjuro;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Finjuro> finjuro2;
    @OneToMany(mappedBy="ctbplanoconta3")
    private Set<Finjuro> finjuro3;
    @OneToMany(mappedBy="ctbplanoconta4")
    private Set<Finjuro> finjuro4;
    @OneToMany(mappedBy="ctbplanoconta5")
    private Set<Finjuro> finjuro5;
    @OneToMany(mappedBy="ctbplanoconta6")
    private Set<Finjuro> finjuro6;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Folevento> folevento;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Folevento> folevento2;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Impicms> impicms;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Impicms> impicms2;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Impipi> impipi;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Impipi> impipi2;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Impplanofiscal> impplanofiscal2;
    @OneToMany(mappedBy="ctbplanoconta6")
    private Set<Impplanofiscal> impplanofiscal6;
    @OneToMany(mappedBy="ctbplanoconta5")
    private Set<Impplanofiscal> impplanofiscal5;
    @OneToMany(mappedBy="ctbplanoconta4")
    private Set<Impplanofiscal> impplanofiscal4;
    @OneToMany(mappedBy="ctbplanoconta3")
    private Set<Impplanofiscal> impplanofiscal3;
    @OneToMany(mappedBy="ctbplanoconta8")
    private Set<Impplanofiscal> impplanofiscal8;
    @OneToMany(mappedBy="ctbplanoconta7")
    private Set<Impplanofiscal> impplanofiscal7;
    @OneToMany(mappedBy="ctbplanoconta10")
    private Set<Impplanofiscal> impplanofiscal10;
    @OneToMany(mappedBy="ctbplanoconta9")
    private Set<Impplanofiscal> impplanofiscal9;
    @OneToMany(mappedBy="ctbplanoconta12")
    private Set<Impplanofiscal> impplanofiscal12;
    @OneToMany(mappedBy="ctbplanoconta11")
    private Set<Impplanofiscal> impplanofiscal11;
    @OneToMany(mappedBy="ctbplanoconta14")
    private Set<Impplanofiscal> impplanofiscal14;
    @OneToMany(mappedBy="ctbplanoconta13")
    private Set<Impplanofiscal> impplanofiscal13;
    @OneToMany(mappedBy="ctbplanoconta16")
    private Set<Impplanofiscal> impplanofiscal16;
    @OneToMany(mappedBy="ctbplanoconta15")
    private Set<Impplanofiscal> impplanofiscal15;
    @OneToMany(mappedBy="ctbplanoconta17")
    private Set<Impplanofiscal> impplanofiscal17;
    @OneToMany(mappedBy="ctbplanoconta18")
    private Set<Impplanofiscal> impplanofiscal18;
    @OneToMany(mappedBy="ctbplanoconta19")
    private Set<Impplanofiscal> impplanofiscal19;
    @OneToMany(mappedBy="ctbplanoconta20")
    private Set<Impplanofiscal> impplanofiscal20;
    @OneToMany(mappedBy="ctbplanoconta21")
    private Set<Impplanofiscal> impplanofiscal21;
    @OneToMany(mappedBy="ctbplanoconta22")
    private Set<Impplanofiscal> impplanofiscal22;
    @OneToMany(mappedBy="ctbplanoconta23")
    private Set<Impplanofiscal> impplanofiscal23;
    @OneToMany(mappedBy="ctbplanoconta24")
    private Set<Impplanofiscal> impplanofiscal24;
    @OneToMany(mappedBy="ctbplanoconta25")
    private Set<Impplanofiscal> impplanofiscal25;
    @OneToMany(mappedBy="ctbplanoconta26")
    private Set<Impplanofiscal> impplanofiscal26;
    @OneToMany(mappedBy="ctbplanoconta27")
    private Set<Impplanofiscal> impplanofiscal27;
    @OneToMany(mappedBy="ctbplanoconta28")
    private Set<Impplanofiscal> impplanofiscal28;
    @OneToMany(mappedBy="ctbplanoconta29")
    private Set<Impplanofiscal> impplanofiscal29;
    @OneToMany(mappedBy="ctbplanoconta30")
    private Set<Impplanofiscal> impplanofiscal30;
    @OneToMany(mappedBy="ctbplanoconta31")
    private Set<Impplanofiscal> impplanofiscal31;
    @OneToMany(mappedBy="ctbplanoconta")
    private Set<Opoperacao> opoperacao;
    @OneToMany(mappedBy="ctbplanoconta2")
    private Set<Opoperacao> opoperacao2;

    /** Default constructor. */
    public Ctbplanoconta() {
        super();
    }

    /**
     * Access method for codctbplanoconta.
     *
     * @return the current value of codctbplanoconta
     */
    public String getCodctbplanoconta() {
        return codctbplanoconta;
    }

    /**
     * Setter method for codctbplanoconta.
     *
     * @param aCodctbplanoconta the new value for codctbplanoconta
     */
    public void setCodctbplanoconta(String aCodctbplanoconta) {
        codctbplanoconta = aCodctbplanoconta;
    }

    /**
     * Access method for descctbplanoconta.
     *
     * @return the current value of descctbplanoconta
     */
    public String getDescctbplanoconta() {
        return descctbplanoconta;
    }

    /**
     * Setter method for descctbplanoconta.
     *
     * @param aDescctbplanoconta the new value for descctbplanoconta
     */
    public void setDescctbplanoconta(String aDescctbplanoconta) {
        descctbplanoconta = aDescctbplanoconta;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for codreduzido.
     *
     * @return the current value of codreduzido
     */
    public String getCodreduzido() {
        return codreduzido;
    }

    /**
     * Setter method for codreduzido.
     *
     * @param aCodreduzido the new value for codreduzido
     */
    public void setCodreduzido(String aCodreduzido) {
        codreduzido = aCodreduzido;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for debcred.
     *
     * @return the current value of debcred
     */
    public short getDebcred() {
        return debcred;
    }

    /**
     * Setter method for debcred.
     *
     * @param aDebcred the new value for debcred
     */
    public void setDebcred(short aDebcred) {
        debcred = aDebcred;
    }

    /**
     * Access method for demdva.
     *
     * @return the current value of demdva
     */
    public short getDemdva() {
        return demdva;
    }

    /**
     * Setter method for demdva.
     *
     * @param aDemdva the new value for demdva
     */
    public void setDemdva(short aDemdva) {
        demdva = aDemdva;
    }

    /**
     * Access method for demdescdva.
     *
     * @return the current value of demdescdva
     */
    public String getDemdescdva() {
        return demdescdva;
    }

    /**
     * Setter method for demdescdva.
     *
     * @param aDemdescdva the new value for demdescdva
     */
    public void setDemdescdva(String aDemdescdva) {
        demdescdva = aDemdescdva;
    }

    /**
     * Access method for demdmpl.
     *
     * @return the current value of demdmpl
     */
    public short getDemdmpl() {
        return demdmpl;
    }

    /**
     * Setter method for demdmpl.
     *
     * @param aDemdmpl the new value for demdmpl
     */
    public void setDemdmpl(short aDemdmpl) {
        demdmpl = aDemdmpl;
    }

    /**
     * Access method for demdescdmpl.
     *
     * @return the current value of demdescdmpl
     */
    public String getDemdescdmpl() {
        return demdescdmpl;
    }

    /**
     * Setter method for demdescdmpl.
     *
     * @param aDemdescdmpl the new value for demdescdmpl
     */
    public void setDemdescdmpl(String aDemdescdmpl) {
        demdescdmpl = aDemdescdmpl;
    }

    /**
     * Access method for demdfc.
     *
     * @return the current value of demdfc
     */
    public short getDemdfc() {
        return demdfc;
    }

    /**
     * Setter method for demdfc.
     *
     * @param aDemdfc the new value for demdfc
     */
    public void setDemdfc(short aDemdfc) {
        demdfc = aDemdfc;
    }

    /**
     * Access method for demdescdfc.
     *
     * @return the current value of demdescdfc
     */
    public String getDemdescdfc() {
        return demdescdfc;
    }

    /**
     * Setter method for demdescdfc.
     *
     * @param aDemdescdfc the new value for demdescdfc
     */
    public void setDemdescdfc(String aDemdescdfc) {
        demdescdfc = aDemdescdfc;
    }

    /**
     * Access method for demdre.
     *
     * @return the current value of demdre
     */
    public short getDemdre() {
        return demdre;
    }

    /**
     * Setter method for demdre.
     *
     * @param aDemdre the new value for demdre
     */
    public void setDemdre(short aDemdre) {
        demdre = aDemdre;
    }

    /**
     * Access method for demdescdre.
     *
     * @return the current value of demdescdre
     */
    public String getDemdescdre() {
        return demdescdre;
    }

    /**
     * Setter method for demdescdre.
     *
     * @param aDemdescdre the new value for demdescdre
     */
    public void setDemdescdre(String aDemdescdre) {
        demdescdre = aDemdescdre;
    }

    /**
     * Access method for dembp.
     *
     * @return the current value of dembp
     */
    public short getDembp() {
        return dembp;
    }

    /**
     * Setter method for dembp.
     *
     * @param aDembp the new value for dembp
     */
    public void setDembp(short aDembp) {
        dembp = aDembp;
    }

    /**
     * Access method for demdescbp.
     *
     * @return the current value of demdescbp
     */
    public String getDemdescbp() {
        return demdescbp;
    }

    /**
     * Setter method for demdescbp.
     *
     * @param aDemdescbp the new value for demdescbp
     */
    public void setDemdescbp(String aDemdescbp) {
        demdescbp = aDemdescbp;
    }

    /**
     * Access method for datainicial.
     *
     * @return the current value of datainicial
     */
    public Timestamp getDatainicial() {
        return datainicial;
    }

    /**
     * Setter method for datainicial.
     *
     * @param aDatainicial the new value for datainicial
     */
    public void setDatainicial(Timestamp aDatainicial) {
        datainicial = aDatainicial;
    }

    /**
     * Access method for datafinal.
     *
     * @return the current value of datafinal
     */
    public Timestamp getDatafinal() {
        return datafinal;
    }

    /**
     * Setter method for datafinal.
     *
     * @param aDatafinal the new value for datafinal
     */
    public void setDatafinal(Timestamp aDatafinal) {
        datafinal = aDatafinal;
    }

    /**
     * Access method for codctbplanospedcont.
     *
     * @return the current value of codctbplanospedcont
     */
    public String getCodctbplanospedcont() {
        return codctbplanospedcont;
    }

    /**
     * Setter method for codctbplanospedcont.
     *
     * @param aCodctbplanospedcont the new value for codctbplanospedcont
     */
    public void setCodctbplanospedcont(String aCodctbplanospedcont) {
        codctbplanospedcont = aCodctbplanospedcont;
    }

    /**
     * Access method for codctbplanosusep.
     *
     * @return the current value of codctbplanosusep
     */
    public String getCodctbplanosusep() {
        return codctbplanosusep;
    }

    /**
     * Setter method for codctbplanosusep.
     *
     * @param aCodctbplanosusep the new value for codctbplanosusep
     */
    public void setCodctbplanosusep(String aCodctbplanosusep) {
        codctbplanosusep = aCodctbplanosusep;
    }

    /**
     * Access method for codctbplanocosip.
     *
     * @return the current value of codctbplanocosip
     */
    public String getCodctbplanocosip() {
        return codctbplanocosip;
    }

    /**
     * Setter method for codctbplanocosip.
     *
     * @param aCodctbplanocosip the new value for codctbplanocosip
     */
    public void setCodctbplanocosip(String aCodctbplanocosip) {
        codctbplanocosip = aCodctbplanocosip;
    }

    /**
     * Access method for fisrazao.
     *
     * @return the current value of fisrazao
     */
    public short getFisrazao() {
        return fisrazao;
    }

    /**
     * Setter method for fisrazao.
     *
     * @param aFisrazao the new value for fisrazao
     */
    public void setFisrazao(short aFisrazao) {
        fisrazao = aFisrazao;
    }

    /**
     * Access method for fisdiario.
     *
     * @return the current value of fisdiario
     */
    public short getFisdiario() {
        return fisdiario;
    }

    /**
     * Setter method for fisdiario.
     *
     * @param aFisdiario the new value for fisdiario
     */
    public void setFisdiario(short aFisdiario) {
        fisdiario = aFisdiario;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Set<Agagente> getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Set<Agagente> aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for agconf.
     *
     * @return the current value of agconf
     */
    public Set<Agconf> getAgconf() {
        return agconf;
    }

    /**
     * Setter method for agconf.
     *
     * @param aAgconf the new value for agconf
     */
    public void setAgconf(Set<Agconf> aAgconf) {
        agconf = aAgconf;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Set<Cobrforma> getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Set<Cobrforma> aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Access method for cobrforma2.
     *
     * @return the current value of cobrforma2
     */
    public Set<Cobrforma> getCobrforma2() {
        return cobrforma2;
    }

    /**
     * Setter method for cobrforma2.
     *
     * @param aCobrforma2 the new value for cobrforma2
     */
    public void setCobrforma2(Set<Cobrforma> aCobrforma2) {
        cobrforma2 = aCobrforma2;
    }

    /**
     * Access method for ctbbemdet.
     *
     * @return the current value of ctbbemdet
     */
    public Set<Ctbbemdet> getCtbbemdet() {
        return ctbbemdet;
    }

    /**
     * Setter method for ctbbemdet.
     *
     * @param aCtbbemdet the new value for ctbbemdet
     */
    public void setCtbbemdet(Set<Ctbbemdet> aCtbbemdet) {
        ctbbemdet = aCtbbemdet;
    }

    /**
     * Access method for ctbbemdet2.
     *
     * @return the current value of ctbbemdet2
     */
    public Set<Ctbbemdet> getCtbbemdet2() {
        return ctbbemdet2;
    }

    /**
     * Setter method for ctbbemdet2.
     *
     * @param aCtbbemdet2 the new value for ctbbemdet2
     */
    public void setCtbbemdet2(Set<Ctbbemdet> aCtbbemdet2) {
        ctbbemdet2 = aCtbbemdet2;
    }

    /**
     * Access method for ctbfechamento.
     *
     * @return the current value of ctbfechamento
     */
    public Set<Ctbfechamento> getCtbfechamento() {
        return ctbfechamento;
    }

    /**
     * Setter method for ctbfechamento.
     *
     * @param aCtbfechamento the new value for ctbfechamento
     */
    public void setCtbfechamento(Set<Ctbfechamento> aCtbfechamento) {
        ctbfechamento = aCtbfechamento;
    }

    /**
     * Access method for ctblancamento.
     *
     * @return the current value of ctblancamento
     */
    public Set<Ctblancamento> getCtblancamento() {
        return ctblancamento;
    }

    /**
     * Setter method for ctblancamento.
     *
     * @param aCtblancamento the new value for ctblancamento
     */
    public void setCtblancamento(Set<Ctblancamento> aCtblancamento) {
        ctblancamento = aCtblancamento;
    }

    /**
     * Access method for ctblancamento2.
     *
     * @return the current value of ctblancamento2
     */
    public Set<Ctblancamento> getCtblancamento2() {
        return ctblancamento2;
    }

    /**
     * Setter method for ctblancamento2.
     *
     * @param aCtblancamento2 the new value for ctblancamento2
     */
    public void setCtblancamento2(Set<Ctblancamento> aCtblancamento2) {
        ctblancamento2 = aCtblancamento2;
    }

    /**
     * Access method for fintipo.
     *
     * @return the current value of fintipo
     */
    public Fintipo getFintipo() {
        return fintipo;
    }

    /**
     * Setter method for fintipo.
     *
     * @param aFintipo the new value for fintipo
     */
    public void setFintipo(Fintipo aFintipo) {
        fintipo = aFintipo;
    }

    /**
     * Access method for ctbplanoconta41.
     *
     * @return the current value of ctbplanoconta41
     */
    public Set<Ctbplanoconta> getCtbplanoconta41() {
        return ctbplanoconta41;
    }

    /**
     * Setter method for ctbplanoconta41.
     *
     * @param aCtbplanoconta41 the new value for ctbplanoconta41
     */
    public void setCtbplanoconta41(Set<Ctbplanoconta> aCtbplanoconta41) {
        ctbplanoconta41 = aCtbplanoconta41;
    }

    /**
     * Access method for ctbplanoconta40.
     *
     * @return the current value of ctbplanoconta40
     */
    public Ctbplanoconta getCtbplanoconta40() {
        return ctbplanoconta40;
    }

    /**
     * Setter method for ctbplanoconta40.
     *
     * @param aCtbplanoconta40 the new value for ctbplanoconta40
     */
    public void setCtbplanoconta40(Ctbplanoconta aCtbplanoconta40) {
        ctbplanoconta40 = aCtbplanoconta40;
    }

    /**
     * Access method for ctbplanoconta43.
     *
     * @return the current value of ctbplanoconta43
     */
    public Set<Ctbplanoconta> getCtbplanoconta43() {
        return ctbplanoconta43;
    }

    /**
     * Setter method for ctbplanoconta43.
     *
     * @param aCtbplanoconta43 the new value for ctbplanoconta43
     */
    public void setCtbplanoconta43(Set<Ctbplanoconta> aCtbplanoconta43) {
        ctbplanoconta43 = aCtbplanoconta43;
    }

    /**
     * Access method for ctbplanoconta42.
     *
     * @return the current value of ctbplanoconta42
     */
    public Ctbplanoconta getCtbplanoconta42() {
        return ctbplanoconta42;
    }

    /**
     * Setter method for ctbplanoconta42.
     *
     * @param aCtbplanoconta42 the new value for ctbplanoconta42
     */
    public void setCtbplanoconta42(Ctbplanoconta aCtbplanoconta42) {
        ctbplanoconta42 = aCtbplanoconta42;
    }

    /**
     * Access method for ctbhistorico.
     *
     * @return the current value of ctbhistorico
     */
    public Ctbhistorico getCtbhistorico() {
        return ctbhistorico;
    }

    /**
     * Setter method for ctbhistorico.
     *
     * @param aCtbhistorico the new value for ctbhistorico
     */
    public void setCtbhistorico(Ctbhistorico aCtbhistorico) {
        ctbhistorico = aCtbhistorico;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for ctbplanoconta33.
     *
     * @return the current value of ctbplanoconta33
     */
    public Set<Ctbplanoconta> getCtbplanoconta33() {
        return ctbplanoconta33;
    }

    /**
     * Setter method for ctbplanoconta33.
     *
     * @param aCtbplanoconta33 the new value for ctbplanoconta33
     */
    public void setCtbplanoconta33(Set<Ctbplanoconta> aCtbplanoconta33) {
        ctbplanoconta33 = aCtbplanoconta33;
    }

    /**
     * Access method for ctbplanoconta32.
     *
     * @return the current value of ctbplanoconta32
     */
    public Ctbplanoconta getCtbplanoconta32() {
        return ctbplanoconta32;
    }

    /**
     * Setter method for ctbplanoconta32.
     *
     * @param aCtbplanoconta32 the new value for ctbplanoconta32
     */
    public void setCtbplanoconta32(Ctbplanoconta aCtbplanoconta32) {
        ctbplanoconta32 = aCtbplanoconta32;
    }

    /**
     * Access method for finmoeda.
     *
     * @return the current value of finmoeda
     */
    public Finmoeda getFinmoeda() {
        return finmoeda;
    }

    /**
     * Setter method for finmoeda.
     *
     * @param aFinmoeda the new value for finmoeda
     */
    public void setFinmoeda(Finmoeda aFinmoeda) {
        finmoeda = aFinmoeda;
    }

    /**
     * Access method for finfluxoplanoconta.
     *
     * @return the current value of finfluxoplanoconta
     */
    public Finfluxoplanoconta getFinfluxoplanoconta() {
        return finfluxoplanoconta;
    }

    /**
     * Setter method for finfluxoplanoconta.
     *
     * @param aFinfluxoplanoconta the new value for finfluxoplanoconta
     */
    public void setFinfluxoplanoconta(Finfluxoplanoconta aFinfluxoplanoconta) {
        finfluxoplanoconta = aFinfluxoplanoconta;
    }

    /**
     * Access method for orcplanoconta.
     *
     * @return the current value of orcplanoconta
     */
    public Orcplanoconta getOrcplanoconta() {
        return orcplanoconta;
    }

    /**
     * Setter method for orcplanoconta.
     *
     * @param aOrcplanoconta the new value for orcplanoconta
     */
    public void setOrcplanoconta(Orcplanoconta aOrcplanoconta) {
        orcplanoconta = aOrcplanoconta;
    }

    /**
     * Access method for ctbplanoconta35.
     *
     * @return the current value of ctbplanoconta35
     */
    public Set<Ctbplanoconta> getCtbplanoconta35() {
        return ctbplanoconta35;
    }

    /**
     * Setter method for ctbplanoconta35.
     *
     * @param aCtbplanoconta35 the new value for ctbplanoconta35
     */
    public void setCtbplanoconta35(Set<Ctbplanoconta> aCtbplanoconta35) {
        ctbplanoconta35 = aCtbplanoconta35;
    }

    /**
     * Access method for ctbplanoconta34.
     *
     * @return the current value of ctbplanoconta34
     */
    public Ctbplanoconta getCtbplanoconta34() {
        return ctbplanoconta34;
    }

    /**
     * Setter method for ctbplanoconta34.
     *
     * @param aCtbplanoconta34 the new value for ctbplanoconta34
     */
    public void setCtbplanoconta34(Ctbplanoconta aCtbplanoconta34) {
        ctbplanoconta34 = aCtbplanoconta34;
    }

    /**
     * Access method for ctbplanoconta37.
     *
     * @return the current value of ctbplanoconta37
     */
    public Set<Ctbplanoconta> getCtbplanoconta37() {
        return ctbplanoconta37;
    }

    /**
     * Setter method for ctbplanoconta37.
     *
     * @param aCtbplanoconta37 the new value for ctbplanoconta37
     */
    public void setCtbplanoconta37(Set<Ctbplanoconta> aCtbplanoconta37) {
        ctbplanoconta37 = aCtbplanoconta37;
    }

    /**
     * Access method for ctbplanoconta36.
     *
     * @return the current value of ctbplanoconta36
     */
    public Ctbplanoconta getCtbplanoconta36() {
        return ctbplanoconta36;
    }

    /**
     * Setter method for ctbplanoconta36.
     *
     * @param aCtbplanoconta36 the new value for ctbplanoconta36
     */
    public void setCtbplanoconta36(Ctbplanoconta aCtbplanoconta36) {
        ctbplanoconta36 = aCtbplanoconta36;
    }

    /**
     * Access method for ctbplanoconta39.
     *
     * @return the current value of ctbplanoconta39
     */
    public Set<Ctbplanoconta> getCtbplanoconta39() {
        return ctbplanoconta39;
    }

    /**
     * Setter method for ctbplanoconta39.
     *
     * @param aCtbplanoconta39 the new value for ctbplanoconta39
     */
    public void setCtbplanoconta39(Set<Ctbplanoconta> aCtbplanoconta39) {
        ctbplanoconta39 = aCtbplanoconta39;
    }

    /**
     * Access method for ctbplanoconta38.
     *
     * @return the current value of ctbplanoconta38
     */
    public Ctbplanoconta getCtbplanoconta38() {
        return ctbplanoconta38;
    }

    /**
     * Setter method for ctbplanoconta38.
     *
     * @param aCtbplanoconta38 the new value for ctbplanoconta38
     */
    public void setCtbplanoconta38(Ctbplanoconta aCtbplanoconta38) {
        ctbplanoconta38 = aCtbplanoconta38;
    }

    /**
     * Access method for finbaixa.
     *
     * @return the current value of finbaixa
     */
    public Set<Finbaixa> getFinbaixa() {
        return finbaixa;
    }

    /**
     * Setter method for finbaixa.
     *
     * @param aFinbaixa the new value for finbaixa
     */
    public void setFinbaixa(Set<Finbaixa> aFinbaixa) {
        finbaixa = aFinbaixa;
    }

    /**
     * Access method for finbaixa2.
     *
     * @return the current value of finbaixa2
     */
    public Set<Finbaixa> getFinbaixa2() {
        return finbaixa2;
    }

    /**
     * Setter method for finbaixa2.
     *
     * @param aFinbaixa2 the new value for finbaixa2
     */
    public void setFinbaixa2(Set<Finbaixa> aFinbaixa2) {
        finbaixa2 = aFinbaixa2;
    }

    /**
     * Access method for fincaixa.
     *
     * @return the current value of fincaixa
     */
    public Set<Fincaixa> getFincaixa() {
        return fincaixa;
    }

    /**
     * Setter method for fincaixa.
     *
     * @param aFincaixa the new value for fincaixa
     */
    public void setFincaixa(Set<Fincaixa> aFincaixa) {
        fincaixa = aFincaixa;
    }

    /**
     * Access method for fincaixa2.
     *
     * @return the current value of fincaixa2
     */
    public Set<Fincaixa> getFincaixa2() {
        return fincaixa2;
    }

    /**
     * Setter method for fincaixa2.
     *
     * @param aFincaixa2 the new value for fincaixa2
     */
    public void setFincaixa2(Set<Fincaixa> aFincaixa2) {
        fincaixa2 = aFincaixa2;
    }

    /**
     * Access method for finjuro.
     *
     * @return the current value of finjuro
     */
    public Set<Finjuro> getFinjuro() {
        return finjuro;
    }

    /**
     * Setter method for finjuro.
     *
     * @param aFinjuro the new value for finjuro
     */
    public void setFinjuro(Set<Finjuro> aFinjuro) {
        finjuro = aFinjuro;
    }

    /**
     * Access method for finjuro2.
     *
     * @return the current value of finjuro2
     */
    public Set<Finjuro> getFinjuro2() {
        return finjuro2;
    }

    /**
     * Setter method for finjuro2.
     *
     * @param aFinjuro2 the new value for finjuro2
     */
    public void setFinjuro2(Set<Finjuro> aFinjuro2) {
        finjuro2 = aFinjuro2;
    }

    /**
     * Access method for finjuro3.
     *
     * @return the current value of finjuro3
     */
    public Set<Finjuro> getFinjuro3() {
        return finjuro3;
    }

    /**
     * Setter method for finjuro3.
     *
     * @param aFinjuro3 the new value for finjuro3
     */
    public void setFinjuro3(Set<Finjuro> aFinjuro3) {
        finjuro3 = aFinjuro3;
    }

    /**
     * Access method for finjuro4.
     *
     * @return the current value of finjuro4
     */
    public Set<Finjuro> getFinjuro4() {
        return finjuro4;
    }

    /**
     * Setter method for finjuro4.
     *
     * @param aFinjuro4 the new value for finjuro4
     */
    public void setFinjuro4(Set<Finjuro> aFinjuro4) {
        finjuro4 = aFinjuro4;
    }

    /**
     * Access method for finjuro5.
     *
     * @return the current value of finjuro5
     */
    public Set<Finjuro> getFinjuro5() {
        return finjuro5;
    }

    /**
     * Setter method for finjuro5.
     *
     * @param aFinjuro5 the new value for finjuro5
     */
    public void setFinjuro5(Set<Finjuro> aFinjuro5) {
        finjuro5 = aFinjuro5;
    }

    /**
     * Access method for finjuro6.
     *
     * @return the current value of finjuro6
     */
    public Set<Finjuro> getFinjuro6() {
        return finjuro6;
    }

    /**
     * Setter method for finjuro6.
     *
     * @param aFinjuro6 the new value for finjuro6
     */
    public void setFinjuro6(Set<Finjuro> aFinjuro6) {
        finjuro6 = aFinjuro6;
    }

    /**
     * Access method for folevento.
     *
     * @return the current value of folevento
     */
    public Set<Folevento> getFolevento() {
        return folevento;
    }

    /**
     * Setter method for folevento.
     *
     * @param aFolevento the new value for folevento
     */
    public void setFolevento(Set<Folevento> aFolevento) {
        folevento = aFolevento;
    }

    /**
     * Access method for folevento2.
     *
     * @return the current value of folevento2
     */
    public Set<Folevento> getFolevento2() {
        return folevento2;
    }

    /**
     * Setter method for folevento2.
     *
     * @param aFolevento2 the new value for folevento2
     */
    public void setFolevento2(Set<Folevento> aFolevento2) {
        folevento2 = aFolevento2;
    }

    /**
     * Access method for impicms.
     *
     * @return the current value of impicms
     */
    public Set<Impicms> getImpicms() {
        return impicms;
    }

    /**
     * Setter method for impicms.
     *
     * @param aImpicms the new value for impicms
     */
    public void setImpicms(Set<Impicms> aImpicms) {
        impicms = aImpicms;
    }

    /**
     * Access method for impicms2.
     *
     * @return the current value of impicms2
     */
    public Set<Impicms> getImpicms2() {
        return impicms2;
    }

    /**
     * Setter method for impicms2.
     *
     * @param aImpicms2 the new value for impicms2
     */
    public void setImpicms2(Set<Impicms> aImpicms2) {
        impicms2 = aImpicms2;
    }

    /**
     * Access method for impipi.
     *
     * @return the current value of impipi
     */
    public Set<Impipi> getImpipi() {
        return impipi;
    }

    /**
     * Setter method for impipi.
     *
     * @param aImpipi the new value for impipi
     */
    public void setImpipi(Set<Impipi> aImpipi) {
        impipi = aImpipi;
    }

    /**
     * Access method for impipi2.
     *
     * @return the current value of impipi2
     */
    public Set<Impipi> getImpipi2() {
        return impipi2;
    }

    /**
     * Setter method for impipi2.
     *
     * @param aImpipi2 the new value for impipi2
     */
    public void setImpipi2(Set<Impipi> aImpipi2) {
        impipi2 = aImpipi2;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for impplanofiscal2.
     *
     * @return the current value of impplanofiscal2
     */
    public Set<Impplanofiscal> getImpplanofiscal2() {
        return impplanofiscal2;
    }

    /**
     * Setter method for impplanofiscal2.
     *
     * @param aImpplanofiscal2 the new value for impplanofiscal2
     */
    public void setImpplanofiscal2(Set<Impplanofiscal> aImpplanofiscal2) {
        impplanofiscal2 = aImpplanofiscal2;
    }

    /**
     * Access method for impplanofiscal6.
     *
     * @return the current value of impplanofiscal6
     */
    public Set<Impplanofiscal> getImpplanofiscal6() {
        return impplanofiscal6;
    }

    /**
     * Setter method for impplanofiscal6.
     *
     * @param aImpplanofiscal6 the new value for impplanofiscal6
     */
    public void setImpplanofiscal6(Set<Impplanofiscal> aImpplanofiscal6) {
        impplanofiscal6 = aImpplanofiscal6;
    }

    /**
     * Access method for impplanofiscal5.
     *
     * @return the current value of impplanofiscal5
     */
    public Set<Impplanofiscal> getImpplanofiscal5() {
        return impplanofiscal5;
    }

    /**
     * Setter method for impplanofiscal5.
     *
     * @param aImpplanofiscal5 the new value for impplanofiscal5
     */
    public void setImpplanofiscal5(Set<Impplanofiscal> aImpplanofiscal5) {
        impplanofiscal5 = aImpplanofiscal5;
    }

    /**
     * Access method for impplanofiscal4.
     *
     * @return the current value of impplanofiscal4
     */
    public Set<Impplanofiscal> getImpplanofiscal4() {
        return impplanofiscal4;
    }

    /**
     * Setter method for impplanofiscal4.
     *
     * @param aImpplanofiscal4 the new value for impplanofiscal4
     */
    public void setImpplanofiscal4(Set<Impplanofiscal> aImpplanofiscal4) {
        impplanofiscal4 = aImpplanofiscal4;
    }

    /**
     * Access method for impplanofiscal3.
     *
     * @return the current value of impplanofiscal3
     */
    public Set<Impplanofiscal> getImpplanofiscal3() {
        return impplanofiscal3;
    }

    /**
     * Setter method for impplanofiscal3.
     *
     * @param aImpplanofiscal3 the new value for impplanofiscal3
     */
    public void setImpplanofiscal3(Set<Impplanofiscal> aImpplanofiscal3) {
        impplanofiscal3 = aImpplanofiscal3;
    }

    /**
     * Access method for impplanofiscal8.
     *
     * @return the current value of impplanofiscal8
     */
    public Set<Impplanofiscal> getImpplanofiscal8() {
        return impplanofiscal8;
    }

    /**
     * Setter method for impplanofiscal8.
     *
     * @param aImpplanofiscal8 the new value for impplanofiscal8
     */
    public void setImpplanofiscal8(Set<Impplanofiscal> aImpplanofiscal8) {
        impplanofiscal8 = aImpplanofiscal8;
    }

    /**
     * Access method for impplanofiscal7.
     *
     * @return the current value of impplanofiscal7
     */
    public Set<Impplanofiscal> getImpplanofiscal7() {
        return impplanofiscal7;
    }

    /**
     * Setter method for impplanofiscal7.
     *
     * @param aImpplanofiscal7 the new value for impplanofiscal7
     */
    public void setImpplanofiscal7(Set<Impplanofiscal> aImpplanofiscal7) {
        impplanofiscal7 = aImpplanofiscal7;
    }

    /**
     * Access method for impplanofiscal10.
     *
     * @return the current value of impplanofiscal10
     */
    public Set<Impplanofiscal> getImpplanofiscal10() {
        return impplanofiscal10;
    }

    /**
     * Setter method for impplanofiscal10.
     *
     * @param aImpplanofiscal10 the new value for impplanofiscal10
     */
    public void setImpplanofiscal10(Set<Impplanofiscal> aImpplanofiscal10) {
        impplanofiscal10 = aImpplanofiscal10;
    }

    /**
     * Access method for impplanofiscal9.
     *
     * @return the current value of impplanofiscal9
     */
    public Set<Impplanofiscal> getImpplanofiscal9() {
        return impplanofiscal9;
    }

    /**
     * Setter method for impplanofiscal9.
     *
     * @param aImpplanofiscal9 the new value for impplanofiscal9
     */
    public void setImpplanofiscal9(Set<Impplanofiscal> aImpplanofiscal9) {
        impplanofiscal9 = aImpplanofiscal9;
    }

    /**
     * Access method for impplanofiscal12.
     *
     * @return the current value of impplanofiscal12
     */
    public Set<Impplanofiscal> getImpplanofiscal12() {
        return impplanofiscal12;
    }

    /**
     * Setter method for impplanofiscal12.
     *
     * @param aImpplanofiscal12 the new value for impplanofiscal12
     */
    public void setImpplanofiscal12(Set<Impplanofiscal> aImpplanofiscal12) {
        impplanofiscal12 = aImpplanofiscal12;
    }

    /**
     * Access method for impplanofiscal11.
     *
     * @return the current value of impplanofiscal11
     */
    public Set<Impplanofiscal> getImpplanofiscal11() {
        return impplanofiscal11;
    }

    /**
     * Setter method for impplanofiscal11.
     *
     * @param aImpplanofiscal11 the new value for impplanofiscal11
     */
    public void setImpplanofiscal11(Set<Impplanofiscal> aImpplanofiscal11) {
        impplanofiscal11 = aImpplanofiscal11;
    }

    /**
     * Access method for impplanofiscal14.
     *
     * @return the current value of impplanofiscal14
     */
    public Set<Impplanofiscal> getImpplanofiscal14() {
        return impplanofiscal14;
    }

    /**
     * Setter method for impplanofiscal14.
     *
     * @param aImpplanofiscal14 the new value for impplanofiscal14
     */
    public void setImpplanofiscal14(Set<Impplanofiscal> aImpplanofiscal14) {
        impplanofiscal14 = aImpplanofiscal14;
    }

    /**
     * Access method for impplanofiscal13.
     *
     * @return the current value of impplanofiscal13
     */
    public Set<Impplanofiscal> getImpplanofiscal13() {
        return impplanofiscal13;
    }

    /**
     * Setter method for impplanofiscal13.
     *
     * @param aImpplanofiscal13 the new value for impplanofiscal13
     */
    public void setImpplanofiscal13(Set<Impplanofiscal> aImpplanofiscal13) {
        impplanofiscal13 = aImpplanofiscal13;
    }

    /**
     * Access method for impplanofiscal16.
     *
     * @return the current value of impplanofiscal16
     */
    public Set<Impplanofiscal> getImpplanofiscal16() {
        return impplanofiscal16;
    }

    /**
     * Setter method for impplanofiscal16.
     *
     * @param aImpplanofiscal16 the new value for impplanofiscal16
     */
    public void setImpplanofiscal16(Set<Impplanofiscal> aImpplanofiscal16) {
        impplanofiscal16 = aImpplanofiscal16;
    }

    /**
     * Access method for impplanofiscal15.
     *
     * @return the current value of impplanofiscal15
     */
    public Set<Impplanofiscal> getImpplanofiscal15() {
        return impplanofiscal15;
    }

    /**
     * Setter method for impplanofiscal15.
     *
     * @param aImpplanofiscal15 the new value for impplanofiscal15
     */
    public void setImpplanofiscal15(Set<Impplanofiscal> aImpplanofiscal15) {
        impplanofiscal15 = aImpplanofiscal15;
    }

    /**
     * Access method for impplanofiscal17.
     *
     * @return the current value of impplanofiscal17
     */
    public Set<Impplanofiscal> getImpplanofiscal17() {
        return impplanofiscal17;
    }

    /**
     * Setter method for impplanofiscal17.
     *
     * @param aImpplanofiscal17 the new value for impplanofiscal17
     */
    public void setImpplanofiscal17(Set<Impplanofiscal> aImpplanofiscal17) {
        impplanofiscal17 = aImpplanofiscal17;
    }

    /**
     * Access method for impplanofiscal18.
     *
     * @return the current value of impplanofiscal18
     */
    public Set<Impplanofiscal> getImpplanofiscal18() {
        return impplanofiscal18;
    }

    /**
     * Setter method for impplanofiscal18.
     *
     * @param aImpplanofiscal18 the new value for impplanofiscal18
     */
    public void setImpplanofiscal18(Set<Impplanofiscal> aImpplanofiscal18) {
        impplanofiscal18 = aImpplanofiscal18;
    }

    /**
     * Access method for impplanofiscal19.
     *
     * @return the current value of impplanofiscal19
     */
    public Set<Impplanofiscal> getImpplanofiscal19() {
        return impplanofiscal19;
    }

    /**
     * Setter method for impplanofiscal19.
     *
     * @param aImpplanofiscal19 the new value for impplanofiscal19
     */
    public void setImpplanofiscal19(Set<Impplanofiscal> aImpplanofiscal19) {
        impplanofiscal19 = aImpplanofiscal19;
    }

    /**
     * Access method for impplanofiscal20.
     *
     * @return the current value of impplanofiscal20
     */
    public Set<Impplanofiscal> getImpplanofiscal20() {
        return impplanofiscal20;
    }

    /**
     * Setter method for impplanofiscal20.
     *
     * @param aImpplanofiscal20 the new value for impplanofiscal20
     */
    public void setImpplanofiscal20(Set<Impplanofiscal> aImpplanofiscal20) {
        impplanofiscal20 = aImpplanofiscal20;
    }

    /**
     * Access method for impplanofiscal21.
     *
     * @return the current value of impplanofiscal21
     */
    public Set<Impplanofiscal> getImpplanofiscal21() {
        return impplanofiscal21;
    }

    /**
     * Setter method for impplanofiscal21.
     *
     * @param aImpplanofiscal21 the new value for impplanofiscal21
     */
    public void setImpplanofiscal21(Set<Impplanofiscal> aImpplanofiscal21) {
        impplanofiscal21 = aImpplanofiscal21;
    }

    /**
     * Access method for impplanofiscal22.
     *
     * @return the current value of impplanofiscal22
     */
    public Set<Impplanofiscal> getImpplanofiscal22() {
        return impplanofiscal22;
    }

    /**
     * Setter method for impplanofiscal22.
     *
     * @param aImpplanofiscal22 the new value for impplanofiscal22
     */
    public void setImpplanofiscal22(Set<Impplanofiscal> aImpplanofiscal22) {
        impplanofiscal22 = aImpplanofiscal22;
    }

    /**
     * Access method for impplanofiscal23.
     *
     * @return the current value of impplanofiscal23
     */
    public Set<Impplanofiscal> getImpplanofiscal23() {
        return impplanofiscal23;
    }

    /**
     * Setter method for impplanofiscal23.
     *
     * @param aImpplanofiscal23 the new value for impplanofiscal23
     */
    public void setImpplanofiscal23(Set<Impplanofiscal> aImpplanofiscal23) {
        impplanofiscal23 = aImpplanofiscal23;
    }

    /**
     * Access method for impplanofiscal24.
     *
     * @return the current value of impplanofiscal24
     */
    public Set<Impplanofiscal> getImpplanofiscal24() {
        return impplanofiscal24;
    }

    /**
     * Setter method for impplanofiscal24.
     *
     * @param aImpplanofiscal24 the new value for impplanofiscal24
     */
    public void setImpplanofiscal24(Set<Impplanofiscal> aImpplanofiscal24) {
        impplanofiscal24 = aImpplanofiscal24;
    }

    /**
     * Access method for impplanofiscal25.
     *
     * @return the current value of impplanofiscal25
     */
    public Set<Impplanofiscal> getImpplanofiscal25() {
        return impplanofiscal25;
    }

    /**
     * Setter method for impplanofiscal25.
     *
     * @param aImpplanofiscal25 the new value for impplanofiscal25
     */
    public void setImpplanofiscal25(Set<Impplanofiscal> aImpplanofiscal25) {
        impplanofiscal25 = aImpplanofiscal25;
    }

    /**
     * Access method for impplanofiscal26.
     *
     * @return the current value of impplanofiscal26
     */
    public Set<Impplanofiscal> getImpplanofiscal26() {
        return impplanofiscal26;
    }

    /**
     * Setter method for impplanofiscal26.
     *
     * @param aImpplanofiscal26 the new value for impplanofiscal26
     */
    public void setImpplanofiscal26(Set<Impplanofiscal> aImpplanofiscal26) {
        impplanofiscal26 = aImpplanofiscal26;
    }

    /**
     * Access method for impplanofiscal27.
     *
     * @return the current value of impplanofiscal27
     */
    public Set<Impplanofiscal> getImpplanofiscal27() {
        return impplanofiscal27;
    }

    /**
     * Setter method for impplanofiscal27.
     *
     * @param aImpplanofiscal27 the new value for impplanofiscal27
     */
    public void setImpplanofiscal27(Set<Impplanofiscal> aImpplanofiscal27) {
        impplanofiscal27 = aImpplanofiscal27;
    }

    /**
     * Access method for impplanofiscal28.
     *
     * @return the current value of impplanofiscal28
     */
    public Set<Impplanofiscal> getImpplanofiscal28() {
        return impplanofiscal28;
    }

    /**
     * Setter method for impplanofiscal28.
     *
     * @param aImpplanofiscal28 the new value for impplanofiscal28
     */
    public void setImpplanofiscal28(Set<Impplanofiscal> aImpplanofiscal28) {
        impplanofiscal28 = aImpplanofiscal28;
    }

    /**
     * Access method for impplanofiscal29.
     *
     * @return the current value of impplanofiscal29
     */
    public Set<Impplanofiscal> getImpplanofiscal29() {
        return impplanofiscal29;
    }

    /**
     * Setter method for impplanofiscal29.
     *
     * @param aImpplanofiscal29 the new value for impplanofiscal29
     */
    public void setImpplanofiscal29(Set<Impplanofiscal> aImpplanofiscal29) {
        impplanofiscal29 = aImpplanofiscal29;
    }

    /**
     * Access method for impplanofiscal30.
     *
     * @return the current value of impplanofiscal30
     */
    public Set<Impplanofiscal> getImpplanofiscal30() {
        return impplanofiscal30;
    }

    /**
     * Setter method for impplanofiscal30.
     *
     * @param aImpplanofiscal30 the new value for impplanofiscal30
     */
    public void setImpplanofiscal30(Set<Impplanofiscal> aImpplanofiscal30) {
        impplanofiscal30 = aImpplanofiscal30;
    }

    /**
     * Access method for impplanofiscal31.
     *
     * @return the current value of impplanofiscal31
     */
    public Set<Impplanofiscal> getImpplanofiscal31() {
        return impplanofiscal31;
    }

    /**
     * Setter method for impplanofiscal31.
     *
     * @param aImpplanofiscal31 the new value for impplanofiscal31
     */
    public void setImpplanofiscal31(Set<Impplanofiscal> aImpplanofiscal31) {
        impplanofiscal31 = aImpplanofiscal31;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Set<Opoperacao> getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Set<Opoperacao> aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for opoperacao2.
     *
     * @return the current value of opoperacao2
     */
    public Set<Opoperacao> getOpoperacao2() {
        return opoperacao2;
    }

    /**
     * Setter method for opoperacao2.
     *
     * @param aOpoperacao2 the new value for opoperacao2
     */
    public void setOpoperacao2(Set<Opoperacao> aOpoperacao2) {
        opoperacao2 = aOpoperacao2;
    }

    /**
     * Compares the key for this instance with another Ctbplanoconta.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbplanoconta and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbplanoconta)) {
            return false;
        }
        Ctbplanoconta that = (Ctbplanoconta) other;
        Object myCodctbplanoconta = this.getCodctbplanoconta();
        Object yourCodctbplanoconta = that.getCodctbplanoconta();
        if (myCodctbplanoconta==null ? yourCodctbplanoconta!=null : !myCodctbplanoconta.equals(yourCodctbplanoconta)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbplanoconta.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbplanoconta)) return false;
        return this.equalKeys(other) && ((Ctbplanoconta)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodctbplanoconta() == null) {
            i = 0;
        } else {
            i = getCodctbplanoconta().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbplanoconta |");
        sb.append(" codctbplanoconta=").append(getCodctbplanoconta());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbplanoconta", getCodctbplanoconta());
        return ret;
    }

}
