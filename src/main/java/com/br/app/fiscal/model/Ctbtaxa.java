package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CTBTAXA")
public class Ctbtaxa implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbtaxa";

    @Id
    @Column(name="CODCTBTAXA", unique=true, nullable=false, precision=10)
    private int codctbtaxa;
    @Column(name="DESCCTBTAXA", nullable=false, length=250)
    private String descctbtaxa;
    @Column(name="TIPOCTBTAXA", nullable=false, precision=5)
    private short tipoctbtaxa;
    @Column(name="PERCDEPR", precision=15, scale=4)
    private BigDecimal percdepr;
    @Column(name="PERCVAL", precision=15, scale=4)
    private BigDecimal percval;
    @Column(name="VALORDEPR", precision=15, scale=4)
    private BigDecimal valordepr;
    @Column(name="VALORVAL", precision=15, scale=4)
    private BigDecimal valorval;
    @Column(name="TIPODEPRVAL", precision=5)
    private short tipodeprval;
    @OneToMany(mappedBy="ctbtaxa")
    private Set<Ctbbem> ctbbem;

    /** Default constructor. */
    public Ctbtaxa() {
        super();
    }

    /**
     * Access method for codctbtaxa.
     *
     * @return the current value of codctbtaxa
     */
    public int getCodctbtaxa() {
        return codctbtaxa;
    }

    /**
     * Setter method for codctbtaxa.
     *
     * @param aCodctbtaxa the new value for codctbtaxa
     */
    public void setCodctbtaxa(int aCodctbtaxa) {
        codctbtaxa = aCodctbtaxa;
    }

    /**
     * Access method for descctbtaxa.
     *
     * @return the current value of descctbtaxa
     */
    public String getDescctbtaxa() {
        return descctbtaxa;
    }

    /**
     * Setter method for descctbtaxa.
     *
     * @param aDescctbtaxa the new value for descctbtaxa
     */
    public void setDescctbtaxa(String aDescctbtaxa) {
        descctbtaxa = aDescctbtaxa;
    }

    /**
     * Access method for tipoctbtaxa.
     *
     * @return the current value of tipoctbtaxa
     */
    public short getTipoctbtaxa() {
        return tipoctbtaxa;
    }

    /**
     * Setter method for tipoctbtaxa.
     *
     * @param aTipoctbtaxa the new value for tipoctbtaxa
     */
    public void setTipoctbtaxa(short aTipoctbtaxa) {
        tipoctbtaxa = aTipoctbtaxa;
    }

    /**
     * Access method for percdepr.
     *
     * @return the current value of percdepr
     */
    public BigDecimal getPercdepr() {
        return percdepr;
    }

    /**
     * Setter method for percdepr.
     *
     * @param aPercdepr the new value for percdepr
     */
    public void setPercdepr(BigDecimal aPercdepr) {
        percdepr = aPercdepr;
    }

    /**
     * Access method for percval.
     *
     * @return the current value of percval
     */
    public BigDecimal getPercval() {
        return percval;
    }

    /**
     * Setter method for percval.
     *
     * @param aPercval the new value for percval
     */
    public void setPercval(BigDecimal aPercval) {
        percval = aPercval;
    }

    /**
     * Access method for valordepr.
     *
     * @return the current value of valordepr
     */
    public BigDecimal getValordepr() {
        return valordepr;
    }

    /**
     * Setter method for valordepr.
     *
     * @param aValordepr the new value for valordepr
     */
    public void setValordepr(BigDecimal aValordepr) {
        valordepr = aValordepr;
    }

    /**
     * Access method for valorval.
     *
     * @return the current value of valorval
     */
    public BigDecimal getValorval() {
        return valorval;
    }

    /**
     * Setter method for valorval.
     *
     * @param aValorval the new value for valorval
     */
    public void setValorval(BigDecimal aValorval) {
        valorval = aValorval;
    }

    /**
     * Access method for tipodeprval.
     *
     * @return the current value of tipodeprval
     */
    public short getTipodeprval() {
        return tipodeprval;
    }

    /**
     * Setter method for tipodeprval.
     *
     * @param aTipodeprval the new value for tipodeprval
     */
    public void setTipodeprval(short aTipodeprval) {
        tipodeprval = aTipodeprval;
    }

    /**
     * Access method for ctbbem.
     *
     * @return the current value of ctbbem
     */
    public Set<Ctbbem> getCtbbem() {
        return ctbbem;
    }

    /**
     * Setter method for ctbbem.
     *
     * @param aCtbbem the new value for ctbbem
     */
    public void setCtbbem(Set<Ctbbem> aCtbbem) {
        ctbbem = aCtbbem;
    }

    /**
     * Compares the key for this instance with another Ctbtaxa.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbtaxa and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbtaxa)) {
            return false;
        }
        Ctbtaxa that = (Ctbtaxa) other;
        if (this.getCodctbtaxa() != that.getCodctbtaxa()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbtaxa.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbtaxa)) return false;
        return this.equalKeys(other) && ((Ctbtaxa)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbtaxa();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbtaxa |");
        sb.append(" codctbtaxa=").append(getCodctbtaxa());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbtaxa", Integer.valueOf(getCodctbtaxa()));
        return ret;
    }

}
