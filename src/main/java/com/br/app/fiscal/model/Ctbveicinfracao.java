package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="CTBVEICINFRACAO")
public class Ctbveicinfracao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbveicinfracao";

    @Id
    @Column(name="CODCTBVEICINFRACAO", unique=true, nullable=false, length=20)
    private String codctbveicinfracao;
    @Column(name="DESCCTBVEICINFRACAO", nullable=false, length=250)
    private String descctbveicinfracao;
    @Column(name="OBS")
    private String obs;
    @Column(name="MULTA", precision=15, scale=4)
    private BigDecimal multa;
    @OneToMany(mappedBy="ctbveicinfracao")
    private Set<Ctbbemveicinfracao> ctbbemveicinfracao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCTBARTIGO", nullable=false)
    private Ctbartigo ctbartigo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCTBVEICNATUREZAINFRACAO", nullable=false)
    private Ctbveicinfracaonatureza ctbveicinfracaonatureza;

    /** Default constructor. */
    public Ctbveicinfracao() {
        super();
    }

    /**
     * Access method for codctbveicinfracao.
     *
     * @return the current value of codctbveicinfracao
     */
    public String getCodctbveicinfracao() {
        return codctbveicinfracao;
    }

    /**
     * Setter method for codctbveicinfracao.
     *
     * @param aCodctbveicinfracao the new value for codctbveicinfracao
     */
    public void setCodctbveicinfracao(String aCodctbveicinfracao) {
        codctbveicinfracao = aCodctbveicinfracao;
    }

    /**
     * Access method for descctbveicinfracao.
     *
     * @return the current value of descctbveicinfracao
     */
    public String getDescctbveicinfracao() {
        return descctbveicinfracao;
    }

    /**
     * Setter method for descctbveicinfracao.
     *
     * @param aDescctbveicinfracao the new value for descctbveicinfracao
     */
    public void setDescctbveicinfracao(String aDescctbveicinfracao) {
        descctbveicinfracao = aDescctbveicinfracao;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for multa.
     *
     * @return the current value of multa
     */
    public BigDecimal getMulta() {
        return multa;
    }

    /**
     * Setter method for multa.
     *
     * @param aMulta the new value for multa
     */
    public void setMulta(BigDecimal aMulta) {
        multa = aMulta;
    }

    /**
     * Access method for ctbbemveicinfracao.
     *
     * @return the current value of ctbbemveicinfracao
     */
    public Set<Ctbbemveicinfracao> getCtbbemveicinfracao() {
        return ctbbemveicinfracao;
    }

    /**
     * Setter method for ctbbemveicinfracao.
     *
     * @param aCtbbemveicinfracao the new value for ctbbemveicinfracao
     */
    public void setCtbbemveicinfracao(Set<Ctbbemveicinfracao> aCtbbemveicinfracao) {
        ctbbemveicinfracao = aCtbbemveicinfracao;
    }

    /**
     * Access method for ctbartigo.
     *
     * @return the current value of ctbartigo
     */
    public Ctbartigo getCtbartigo() {
        return ctbartigo;
    }

    /**
     * Setter method for ctbartigo.
     *
     * @param aCtbartigo the new value for ctbartigo
     */
    public void setCtbartigo(Ctbartigo aCtbartigo) {
        ctbartigo = aCtbartigo;
    }

    /**
     * Access method for ctbveicinfracaonatureza.
     *
     * @return the current value of ctbveicinfracaonatureza
     */
    public Ctbveicinfracaonatureza getCtbveicinfracaonatureza() {
        return ctbveicinfracaonatureza;
    }

    /**
     * Setter method for ctbveicinfracaonatureza.
     *
     * @param aCtbveicinfracaonatureza the new value for ctbveicinfracaonatureza
     */
    public void setCtbveicinfracaonatureza(Ctbveicinfracaonatureza aCtbveicinfracaonatureza) {
        ctbveicinfracaonatureza = aCtbveicinfracaonatureza;
    }

    /**
     * Compares the key for this instance with another Ctbveicinfracao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbveicinfracao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbveicinfracao)) {
            return false;
        }
        Ctbveicinfracao that = (Ctbveicinfracao) other;
        Object myCodctbveicinfracao = this.getCodctbveicinfracao();
        Object yourCodctbveicinfracao = that.getCodctbveicinfracao();
        if (myCodctbveicinfracao==null ? yourCodctbveicinfracao!=null : !myCodctbveicinfracao.equals(yourCodctbveicinfracao)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbveicinfracao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbveicinfracao)) return false;
        return this.equalKeys(other) && ((Ctbveicinfracao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodctbveicinfracao() == null) {
            i = 0;
        } else {
            i = getCodctbveicinfracao().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbveicinfracao |");
        sb.append(" codctbveicinfracao=").append(getCodctbveicinfracao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbveicinfracao", getCodctbveicinfracao());
        return ret;
    }

}
