package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CTBVEICINFRACAONATUREZA")
public class Ctbveicinfracaonatureza implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codctbveicinfracaonatureza";

    @Id
    @Column(name="CODCTBVEICINFRACAONATUREZA", unique=true, nullable=false, precision=10)
    private int codctbveicinfracaonatureza;
    @Column(name="DESCCTBVEICINFRACAONATUREZA", nullable=false, length=250)
    private String descctbveicinfracaonatureza;
    @Column(name="PONTOS", nullable=false, precision=10)
    private int pontos;
    @OneToMany(mappedBy="ctbveicinfracaonatureza")
    private Set<Ctbveicinfracao> ctbveicinfracao;

    /** Default constructor. */
    public Ctbveicinfracaonatureza() {
        super();
    }

    /**
     * Access method for codctbveicinfracaonatureza.
     *
     * @return the current value of codctbveicinfracaonatureza
     */
    public int getCodctbveicinfracaonatureza() {
        return codctbveicinfracaonatureza;
    }

    /**
     * Setter method for codctbveicinfracaonatureza.
     *
     * @param aCodctbveicinfracaonatureza the new value for codctbveicinfracaonatureza
     */
    public void setCodctbveicinfracaonatureza(int aCodctbveicinfracaonatureza) {
        codctbveicinfracaonatureza = aCodctbveicinfracaonatureza;
    }

    /**
     * Access method for descctbveicinfracaonatureza.
     *
     * @return the current value of descctbveicinfracaonatureza
     */
    public String getDescctbveicinfracaonatureza() {
        return descctbveicinfracaonatureza;
    }

    /**
     * Setter method for descctbveicinfracaonatureza.
     *
     * @param aDescctbveicinfracaonatureza the new value for descctbveicinfracaonatureza
     */
    public void setDescctbveicinfracaonatureza(String aDescctbveicinfracaonatureza) {
        descctbveicinfracaonatureza = aDescctbveicinfracaonatureza;
    }

    /**
     * Access method for pontos.
     *
     * @return the current value of pontos
     */
    public int getPontos() {
        return pontos;
    }

    /**
     * Setter method for pontos.
     *
     * @param aPontos the new value for pontos
     */
    public void setPontos(int aPontos) {
        pontos = aPontos;
    }

    /**
     * Access method for ctbveicinfracao.
     *
     * @return the current value of ctbveicinfracao
     */
    public Set<Ctbveicinfracao> getCtbveicinfracao() {
        return ctbveicinfracao;
    }

    /**
     * Setter method for ctbveicinfracao.
     *
     * @param aCtbveicinfracao the new value for ctbveicinfracao
     */
    public void setCtbveicinfracao(Set<Ctbveicinfracao> aCtbveicinfracao) {
        ctbveicinfracao = aCtbveicinfracao;
    }

    /**
     * Compares the key for this instance with another Ctbveicinfracaonatureza.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ctbveicinfracaonatureza and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ctbveicinfracaonatureza)) {
            return false;
        }
        Ctbveicinfracaonatureza that = (Ctbveicinfracaonatureza) other;
        if (this.getCodctbveicinfracaonatureza() != that.getCodctbveicinfracaonatureza()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ctbveicinfracaonatureza.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ctbveicinfracaonatureza)) return false;
        return this.equalKeys(other) && ((Ctbveicinfracaonatureza)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodctbveicinfracaonatureza();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ctbveicinfracaonatureza |");
        sb.append(" codctbveicinfracaonatureza=").append(getCodctbveicinfracaonatureza());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codctbveicinfracaonatureza", Integer.valueOf(getCodctbveicinfracaonatureza()));
        return ret;
    }

}
