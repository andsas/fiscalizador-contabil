package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="CUSCENTRO")
public class Cuscentro implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcuscentro";

    @Id
    @Column(name="CODCUSCENTRO", unique=true, nullable=false, length=20)
    private String codcuscentro;
    @Column(name="DESCCUSCENTRO", nullable=false, length=250)
    private String desccuscentro;
    @Column(name="CODPARENTCUSCENTRO", length=20)
    private String codparentcuscentro;
    @Column(name="NIVEL", precision=5)
    private short nivel;

    /** Default constructor. */
    public Cuscentro() {
        super();
    }

    /**
     * Access method for codcuscentro.
     *
     * @return the current value of codcuscentro
     */
    public String getCodcuscentro() {
        return codcuscentro;
    }

    /**
     * Setter method for codcuscentro.
     *
     * @param aCodcuscentro the new value for codcuscentro
     */
    public void setCodcuscentro(String aCodcuscentro) {
        codcuscentro = aCodcuscentro;
    }

    /**
     * Access method for desccuscentro.
     *
     * @return the current value of desccuscentro
     */
    public String getDesccuscentro() {
        return desccuscentro;
    }

    /**
     * Setter method for desccuscentro.
     *
     * @param aDesccuscentro the new value for desccuscentro
     */
    public void setDesccuscentro(String aDesccuscentro) {
        desccuscentro = aDesccuscentro;
    }

    /**
     * Access method for codparentcuscentro.
     *
     * @return the current value of codparentcuscentro
     */
    public String getCodparentcuscentro() {
        return codparentcuscentro;
    }

    /**
     * Setter method for codparentcuscentro.
     *
     * @param aCodparentcuscentro the new value for codparentcuscentro
     */
    public void setCodparentcuscentro(String aCodparentcuscentro) {
        codparentcuscentro = aCodparentcuscentro;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Compares the key for this instance with another Cuscentro.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Cuscentro and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Cuscentro)) {
            return false;
        }
        Cuscentro that = (Cuscentro) other;
        Object myCodcuscentro = this.getCodcuscentro();
        Object yourCodcuscentro = that.getCodcuscentro();
        if (myCodcuscentro==null ? yourCodcuscentro!=null : !myCodcuscentro.equals(yourCodcuscentro)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Cuscentro.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Cuscentro)) return false;
        return this.equalKeys(other) && ((Cuscentro)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodcuscentro() == null) {
            i = 0;
        } else {
            i = getCodcuscentro().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Cuscentro |");
        sb.append(" codcuscentro=").append(getCodcuscentro());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcuscentro", getCodcuscentro());
        return ret;
    }

}
