package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="CUSCUSTO")
public class Cuscusto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcuscusto";

    @Id
    @Column(name="CODCUSCUSTO", unique=true, nullable=false, precision=10)
    private int codcuscusto;
    @Column(name="DESCCUSCUSTO", nullable=false, length=250)
    private String desccuscusto;
    @Column(name="FORMULA", nullable=false)
    private String formula;
    @Column(name="TIPOCUSCUSTO", nullable=false, precision=5)
    private short tipocuscusto;
    @Column(name="ATUALIZAMOEDA", precision=5)
    private short atualizamoeda;
    @Column(name="CODFINMOEDA", length=20)
    private String codfinmoeda;
    @Column(name="FORMULAMOEDA", length=250)
    private String formulamoeda;
    @OneToMany(mappedBy="cuscusto")
    private Set<Cushist> cushist;

    /** Default constructor. */
    public Cuscusto() {
        super();
    }

    /**
     * Access method for codcuscusto.
     *
     * @return the current value of codcuscusto
     */
    public int getCodcuscusto() {
        return codcuscusto;
    }

    /**
     * Setter method for codcuscusto.
     *
     * @param aCodcuscusto the new value for codcuscusto
     */
    public void setCodcuscusto(int aCodcuscusto) {
        codcuscusto = aCodcuscusto;
    }

    /**
     * Access method for desccuscusto.
     *
     * @return the current value of desccuscusto
     */
    public String getDesccuscusto() {
        return desccuscusto;
    }

    /**
     * Setter method for desccuscusto.
     *
     * @param aDesccuscusto the new value for desccuscusto
     */
    public void setDesccuscusto(String aDesccuscusto) {
        desccuscusto = aDesccuscusto;
    }

    /**
     * Access method for formula.
     *
     * @return the current value of formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Setter method for formula.
     *
     * @param aFormula the new value for formula
     */
    public void setFormula(String aFormula) {
        formula = aFormula;
    }

    /**
     * Access method for tipocuscusto.
     *
     * @return the current value of tipocuscusto
     */
    public short getTipocuscusto() {
        return tipocuscusto;
    }

    /**
     * Setter method for tipocuscusto.
     *
     * @param aTipocuscusto the new value for tipocuscusto
     */
    public void setTipocuscusto(short aTipocuscusto) {
        tipocuscusto = aTipocuscusto;
    }

    /**
     * Access method for atualizamoeda.
     *
     * @return the current value of atualizamoeda
     */
    public short getAtualizamoeda() {
        return atualizamoeda;
    }

    /**
     * Setter method for atualizamoeda.
     *
     * @param aAtualizamoeda the new value for atualizamoeda
     */
    public void setAtualizamoeda(short aAtualizamoeda) {
        atualizamoeda = aAtualizamoeda;
    }

    /**
     * Access method for codfinmoeda.
     *
     * @return the current value of codfinmoeda
     */
    public String getCodfinmoeda() {
        return codfinmoeda;
    }

    /**
     * Setter method for codfinmoeda.
     *
     * @param aCodfinmoeda the new value for codfinmoeda
     */
    public void setCodfinmoeda(String aCodfinmoeda) {
        codfinmoeda = aCodfinmoeda;
    }

    /**
     * Access method for formulamoeda.
     *
     * @return the current value of formulamoeda
     */
    public String getFormulamoeda() {
        return formulamoeda;
    }

    /**
     * Setter method for formulamoeda.
     *
     * @param aFormulamoeda the new value for formulamoeda
     */
    public void setFormulamoeda(String aFormulamoeda) {
        formulamoeda = aFormulamoeda;
    }

    /**
     * Access method for cushist.
     *
     * @return the current value of cushist
     */
    public Set<Cushist> getCushist() {
        return cushist;
    }

    /**
     * Setter method for cushist.
     *
     * @param aCushist the new value for cushist
     */
    public void setCushist(Set<Cushist> aCushist) {
        cushist = aCushist;
    }

    /**
     * Compares the key for this instance with another Cuscusto.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Cuscusto and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Cuscusto)) {
            return false;
        }
        Cuscusto that = (Cuscusto) other;
        if (this.getCodcuscusto() != that.getCodcuscusto()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Cuscusto.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Cuscusto)) return false;
        return this.equalKeys(other) && ((Cuscusto)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcuscusto();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Cuscusto |");
        sb.append(" codcuscusto=").append(getCodcuscusto());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcuscusto", Integer.valueOf(getCodcuscusto()));
        return ret;
    }

}
