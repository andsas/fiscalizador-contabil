package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="CUSHIST")
public class Cushist implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codcushist";

    @Id
    @Column(name="CODCUSHIST", unique=true, nullable=false, precision=10)
    private int codcushist;
    @Column(name="DATACUSHIST", nullable=false)
    private Timestamp datacushist;
    @Column(name="FORMULA", nullable=false)
    private String formula;
    @Column(name="CUSTO", nullable=false, precision=15, scale=4)
    private BigDecimal custo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCUSCUSTO", nullable=false)
    private Cuscusto cuscusto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAODET")
    private Optransacaodet optransacaodet;

    /** Default constructor. */
    public Cushist() {
        super();
    }

    /**
     * Access method for codcushist.
     *
     * @return the current value of codcushist
     */
    public int getCodcushist() {
        return codcushist;
    }

    /**
     * Setter method for codcushist.
     *
     * @param aCodcushist the new value for codcushist
     */
    public void setCodcushist(int aCodcushist) {
        codcushist = aCodcushist;
    }

    /**
     * Access method for datacushist.
     *
     * @return the current value of datacushist
     */
    public Timestamp getDatacushist() {
        return datacushist;
    }

    /**
     * Setter method for datacushist.
     *
     * @param aDatacushist the new value for datacushist
     */
    public void setDatacushist(Timestamp aDatacushist) {
        datacushist = aDatacushist;
    }

    /**
     * Access method for formula.
     *
     * @return the current value of formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Setter method for formula.
     *
     * @param aFormula the new value for formula
     */
    public void setFormula(String aFormula) {
        formula = aFormula;
    }

    /**
     * Access method for custo.
     *
     * @return the current value of custo
     */
    public BigDecimal getCusto() {
        return custo;
    }

    /**
     * Setter method for custo.
     *
     * @param aCusto the new value for custo
     */
    public void setCusto(BigDecimal aCusto) {
        custo = aCusto;
    }

    /**
     * Access method for cuscusto.
     *
     * @return the current value of cuscusto
     */
    public Cuscusto getCuscusto() {
        return cuscusto;
    }

    /**
     * Setter method for cuscusto.
     *
     * @param aCuscusto the new value for cuscusto
     */
    public void setCuscusto(Cuscusto aCuscusto) {
        cuscusto = aCuscusto;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Optransacaodet getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Optransacaodet aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Compares the key for this instance with another Cushist.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Cushist and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Cushist)) {
            return false;
        }
        Cushist that = (Cushist) other;
        if (this.getCodcushist() != that.getCodcushist()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Cushist.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Cushist)) return false;
        return this.equalKeys(other) && ((Cushist)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodcushist();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Cushist |");
        sb.append(" codcushist=").append(getCodcushist());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codcushist", Integer.valueOf(getCodcushist()));
        return ret;
    }

}
