package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECARQUIVAMENTO")
public class Ecarquivamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecarquivamento";

    @Id
    @Column(name="CODECARQUIVAMENTO", unique=true, nullable=false, precision=10)
    private int codecarquivamento;
    @Column(name="DESCECARQUIVAMENTO", nullable=false, length=250)
    private String descecarquivamento;
    @Column(name="DATA", nullable=false)
    private Timestamp data;
    @Column(name="OBS")
    private String obs;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @Column(name="ETIQUETA", length=250)
    private String etiqueta;
    @ManyToOne
    @JoinColumn(name="CODGEDARQUIVO")
    private Gedarquivo gedarquivo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECTIPOARQUIVAMENTO", nullable=false)
    private Ecarquivamentotipo ecarquivamentotipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECLOCALARQUIVAMENTO", nullable=false)
    private Ecarquivamentolocal ecarquivamentolocal;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIO", nullable=false)
    private Confusuario confusuario;
    @ManyToOne
    @JoinColumn(name="CODCONFUSURESP")
    private Confusuario confusuario2;

    /** Default constructor. */
    public Ecarquivamento() {
        super();
    }

    /**
     * Access method for codecarquivamento.
     *
     * @return the current value of codecarquivamento
     */
    public int getCodecarquivamento() {
        return codecarquivamento;
    }

    /**
     * Setter method for codecarquivamento.
     *
     * @param aCodecarquivamento the new value for codecarquivamento
     */
    public void setCodecarquivamento(int aCodecarquivamento) {
        codecarquivamento = aCodecarquivamento;
    }

    /**
     * Access method for descecarquivamento.
     *
     * @return the current value of descecarquivamento
     */
    public String getDescecarquivamento() {
        return descecarquivamento;
    }

    /**
     * Setter method for descecarquivamento.
     *
     * @param aDescecarquivamento the new value for descecarquivamento
     */
    public void setDescecarquivamento(String aDescecarquivamento) {
        descecarquivamento = aDescecarquivamento;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for etiqueta.
     *
     * @return the current value of etiqueta
     */
    public String getEtiqueta() {
        return etiqueta;
    }

    /**
     * Setter method for etiqueta.
     *
     * @param aEtiqueta the new value for etiqueta
     */
    public void setEtiqueta(String aEtiqueta) {
        etiqueta = aEtiqueta;
    }

    /**
     * Access method for gedarquivo.
     *
     * @return the current value of gedarquivo
     */
    public Gedarquivo getGedarquivo() {
        return gedarquivo;
    }

    /**
     * Setter method for gedarquivo.
     *
     * @param aGedarquivo the new value for gedarquivo
     */
    public void setGedarquivo(Gedarquivo aGedarquivo) {
        gedarquivo = aGedarquivo;
    }

    /**
     * Access method for ecarquivamentotipo.
     *
     * @return the current value of ecarquivamentotipo
     */
    public Ecarquivamentotipo getEcarquivamentotipo() {
        return ecarquivamentotipo;
    }

    /**
     * Setter method for ecarquivamentotipo.
     *
     * @param aEcarquivamentotipo the new value for ecarquivamentotipo
     */
    public void setEcarquivamentotipo(Ecarquivamentotipo aEcarquivamentotipo) {
        ecarquivamentotipo = aEcarquivamentotipo;
    }

    /**
     * Access method for ecarquivamentolocal.
     *
     * @return the current value of ecarquivamentolocal
     */
    public Ecarquivamentolocal getEcarquivamentolocal() {
        return ecarquivamentolocal;
    }

    /**
     * Setter method for ecarquivamentolocal.
     *
     * @param aEcarquivamentolocal the new value for ecarquivamentolocal
     */
    public void setEcarquivamentolocal(Ecarquivamentolocal aEcarquivamentolocal) {
        ecarquivamentolocal = aEcarquivamentolocal;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for confusuario2.
     *
     * @return the current value of confusuario2
     */
    public Confusuario getConfusuario2() {
        return confusuario2;
    }

    /**
     * Setter method for confusuario2.
     *
     * @param aConfusuario2 the new value for confusuario2
     */
    public void setConfusuario2(Confusuario aConfusuario2) {
        confusuario2 = aConfusuario2;
    }

    /**
     * Compares the key for this instance with another Ecarquivamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecarquivamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecarquivamento)) {
            return false;
        }
        Ecarquivamento that = (Ecarquivamento) other;
        if (this.getCodecarquivamento() != that.getCodecarquivamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecarquivamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecarquivamento)) return false;
        return this.equalKeys(other) && ((Ecarquivamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecarquivamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecarquivamento |");
        sb.append(" codecarquivamento=").append(getCodecarquivamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecarquivamento", Integer.valueOf(getCodecarquivamento()));
        return ret;
    }

}
