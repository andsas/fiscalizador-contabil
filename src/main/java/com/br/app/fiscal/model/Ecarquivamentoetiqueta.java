package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="ECARQUIVAMENTOETIQUETA")
public class Ecarquivamentoetiqueta implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecarquivamentoetiqueta";

    @Id
    @Column(name="CODECARQUIVAMENTOETIQUETA", unique=true, nullable=false, precision=10)
    private int codecarquivamentoetiqueta;
    @Column(name="DESCECARQUIVAMENTOETIQUETA", nullable=false, length=250)
    private String descecarquivamentoetiqueta;
    @Column(name="FORMULA", nullable=false, length=250)
    private String formula;
    @Column(name="DIGITOS", nullable=false, precision=10)
    private int digitos;
    @Column(name="NUMINICIAL", nullable=false, precision=10)
    private int numinicial;
    @Column(name="NUMFINAL", precision=10)
    private int numfinal;
    @Column(name="NUMATUAL", precision=10)
    private int numatual;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;

    /** Default constructor. */
    public Ecarquivamentoetiqueta() {
        super();
    }

    /**
     * Access method for codecarquivamentoetiqueta.
     *
     * @return the current value of codecarquivamentoetiqueta
     */
    public int getCodecarquivamentoetiqueta() {
        return codecarquivamentoetiqueta;
    }

    /**
     * Setter method for codecarquivamentoetiqueta.
     *
     * @param aCodecarquivamentoetiqueta the new value for codecarquivamentoetiqueta
     */
    public void setCodecarquivamentoetiqueta(int aCodecarquivamentoetiqueta) {
        codecarquivamentoetiqueta = aCodecarquivamentoetiqueta;
    }

    /**
     * Access method for descecarquivamentoetiqueta.
     *
     * @return the current value of descecarquivamentoetiqueta
     */
    public String getDescecarquivamentoetiqueta() {
        return descecarquivamentoetiqueta;
    }

    /**
     * Setter method for descecarquivamentoetiqueta.
     *
     * @param aDescecarquivamentoetiqueta the new value for descecarquivamentoetiqueta
     */
    public void setDescecarquivamentoetiqueta(String aDescecarquivamentoetiqueta) {
        descecarquivamentoetiqueta = aDescecarquivamentoetiqueta;
    }

    /**
     * Access method for formula.
     *
     * @return the current value of formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Setter method for formula.
     *
     * @param aFormula the new value for formula
     */
    public void setFormula(String aFormula) {
        formula = aFormula;
    }

    /**
     * Access method for digitos.
     *
     * @return the current value of digitos
     */
    public int getDigitos() {
        return digitos;
    }

    /**
     * Setter method for digitos.
     *
     * @param aDigitos the new value for digitos
     */
    public void setDigitos(int aDigitos) {
        digitos = aDigitos;
    }

    /**
     * Access method for numinicial.
     *
     * @return the current value of numinicial
     */
    public int getNuminicial() {
        return numinicial;
    }

    /**
     * Setter method for numinicial.
     *
     * @param aNuminicial the new value for numinicial
     */
    public void setNuminicial(int aNuminicial) {
        numinicial = aNuminicial;
    }

    /**
     * Access method for numfinal.
     *
     * @return the current value of numfinal
     */
    public int getNumfinal() {
        return numfinal;
    }

    /**
     * Setter method for numfinal.
     *
     * @param aNumfinal the new value for numfinal
     */
    public void setNumfinal(int aNumfinal) {
        numfinal = aNumfinal;
    }

    /**
     * Access method for numatual.
     *
     * @return the current value of numatual
     */
    public int getNumatual() {
        return numatual;
    }

    /**
     * Setter method for numatual.
     *
     * @param aNumatual the new value for numatual
     */
    public void setNumatual(int aNumatual) {
        numatual = aNumatual;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Compares the key for this instance with another Ecarquivamentoetiqueta.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecarquivamentoetiqueta and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecarquivamentoetiqueta)) {
            return false;
        }
        Ecarquivamentoetiqueta that = (Ecarquivamentoetiqueta) other;
        if (this.getCodecarquivamentoetiqueta() != that.getCodecarquivamentoetiqueta()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecarquivamentoetiqueta.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecarquivamentoetiqueta)) return false;
        return this.equalKeys(other) && ((Ecarquivamentoetiqueta)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecarquivamentoetiqueta();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecarquivamentoetiqueta |");
        sb.append(" codecarquivamentoetiqueta=").append(getCodecarquivamentoetiqueta());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecarquivamentoetiqueta", Integer.valueOf(getCodecarquivamentoetiqueta()));
        return ret;
    }

}
