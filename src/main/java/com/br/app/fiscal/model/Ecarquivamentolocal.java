package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECARQUIVAMENTOLOCAL")
public class Ecarquivamentolocal implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecarquivamentolocal";

    @Id
    @Column(name="CODECARQUIVAMENTOLOCAL", unique=true, nullable=false, precision=10)
    private int codecarquivamentolocal;
    @Column(name="DESCECARQUIVAMENTOLOCAL", nullable=false, length=250)
    private String descecarquivamentolocal;
    @Column(name="TIPOECARQUIVAMENTOLOCAL", precision=5)
    private short tipoecarquivamentolocal;
    @Column(name="CAMINHOPASTA")
    private String caminhopasta;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @OneToMany(mappedBy="ecarquivamentolocal")
    private Set<Ecarquivamento> ecarquivamento;

    /** Default constructor. */
    public Ecarquivamentolocal() {
        super();
    }

    /**
     * Access method for codecarquivamentolocal.
     *
     * @return the current value of codecarquivamentolocal
     */
    public int getCodecarquivamentolocal() {
        return codecarquivamentolocal;
    }

    /**
     * Setter method for codecarquivamentolocal.
     *
     * @param aCodecarquivamentolocal the new value for codecarquivamentolocal
     */
    public void setCodecarquivamentolocal(int aCodecarquivamentolocal) {
        codecarquivamentolocal = aCodecarquivamentolocal;
    }

    /**
     * Access method for descecarquivamentolocal.
     *
     * @return the current value of descecarquivamentolocal
     */
    public String getDescecarquivamentolocal() {
        return descecarquivamentolocal;
    }

    /**
     * Setter method for descecarquivamentolocal.
     *
     * @param aDescecarquivamentolocal the new value for descecarquivamentolocal
     */
    public void setDescecarquivamentolocal(String aDescecarquivamentolocal) {
        descecarquivamentolocal = aDescecarquivamentolocal;
    }

    /**
     * Access method for tipoecarquivamentolocal.
     *
     * @return the current value of tipoecarquivamentolocal
     */
    public short getTipoecarquivamentolocal() {
        return tipoecarquivamentolocal;
    }

    /**
     * Setter method for tipoecarquivamentolocal.
     *
     * @param aTipoecarquivamentolocal the new value for tipoecarquivamentolocal
     */
    public void setTipoecarquivamentolocal(short aTipoecarquivamentolocal) {
        tipoecarquivamentolocal = aTipoecarquivamentolocal;
    }

    /**
     * Access method for caminhopasta.
     *
     * @return the current value of caminhopasta
     */
    public String getCaminhopasta() {
        return caminhopasta;
    }

    /**
     * Setter method for caminhopasta.
     *
     * @param aCaminhopasta the new value for caminhopasta
     */
    public void setCaminhopasta(String aCaminhopasta) {
        caminhopasta = aCaminhopasta;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for ecarquivamento.
     *
     * @return the current value of ecarquivamento
     */
    public Set<Ecarquivamento> getEcarquivamento() {
        return ecarquivamento;
    }

    /**
     * Setter method for ecarquivamento.
     *
     * @param aEcarquivamento the new value for ecarquivamento
     */
    public void setEcarquivamento(Set<Ecarquivamento> aEcarquivamento) {
        ecarquivamento = aEcarquivamento;
    }

    /**
     * Compares the key for this instance with another Ecarquivamentolocal.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecarquivamentolocal and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecarquivamentolocal)) {
            return false;
        }
        Ecarquivamentolocal that = (Ecarquivamentolocal) other;
        if (this.getCodecarquivamentolocal() != that.getCodecarquivamentolocal()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecarquivamentolocal.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecarquivamentolocal)) return false;
        return this.equalKeys(other) && ((Ecarquivamentolocal)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecarquivamentolocal();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecarquivamentolocal |");
        sb.append(" codecarquivamentolocal=").append(getCodecarquivamentolocal());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecarquivamentolocal", Integer.valueOf(getCodecarquivamentolocal()));
        return ret;
    }

}
