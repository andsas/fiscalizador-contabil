package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECARQUIVAMENTOTIPO")
public class Ecarquivamentotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecarquivamentotipo";

    @Id
    @Column(name="CODECARQUIVAMENTOTIPO", unique=true, nullable=false, precision=10)
    private int codecarquivamentotipo;
    @Column(name="DESCECARQUIVAMENTOTIPO", nullable=false, length=250)
    private String descecarquivamentotipo;
    @Column(name="TIPOECARQUIVAMENTOTIPO", precision=5)
    private short tipoecarquivamentotipo;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @OneToMany(mappedBy="ecarquivamentotipo")
    private Set<Ecarquivamento> ecarquivamento;

    /** Default constructor. */
    public Ecarquivamentotipo() {
        super();
    }

    /**
     * Access method for codecarquivamentotipo.
     *
     * @return the current value of codecarquivamentotipo
     */
    public int getCodecarquivamentotipo() {
        return codecarquivamentotipo;
    }

    /**
     * Setter method for codecarquivamentotipo.
     *
     * @param aCodecarquivamentotipo the new value for codecarquivamentotipo
     */
    public void setCodecarquivamentotipo(int aCodecarquivamentotipo) {
        codecarquivamentotipo = aCodecarquivamentotipo;
    }

    /**
     * Access method for descecarquivamentotipo.
     *
     * @return the current value of descecarquivamentotipo
     */
    public String getDescecarquivamentotipo() {
        return descecarquivamentotipo;
    }

    /**
     * Setter method for descecarquivamentotipo.
     *
     * @param aDescecarquivamentotipo the new value for descecarquivamentotipo
     */
    public void setDescecarquivamentotipo(String aDescecarquivamentotipo) {
        descecarquivamentotipo = aDescecarquivamentotipo;
    }

    /**
     * Access method for tipoecarquivamentotipo.
     *
     * @return the current value of tipoecarquivamentotipo
     */
    public short getTipoecarquivamentotipo() {
        return tipoecarquivamentotipo;
    }

    /**
     * Setter method for tipoecarquivamentotipo.
     *
     * @param aTipoecarquivamentotipo the new value for tipoecarquivamentotipo
     */
    public void setTipoecarquivamentotipo(short aTipoecarquivamentotipo) {
        tipoecarquivamentotipo = aTipoecarquivamentotipo;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for ecarquivamento.
     *
     * @return the current value of ecarquivamento
     */
    public Set<Ecarquivamento> getEcarquivamento() {
        return ecarquivamento;
    }

    /**
     * Setter method for ecarquivamento.
     *
     * @param aEcarquivamento the new value for ecarquivamento
     */
    public void setEcarquivamento(Set<Ecarquivamento> aEcarquivamento) {
        ecarquivamento = aEcarquivamento;
    }

    /**
     * Compares the key for this instance with another Ecarquivamentotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecarquivamentotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecarquivamentotipo)) {
            return false;
        }
        Ecarquivamentotipo that = (Ecarquivamentotipo) other;
        if (this.getCodecarquivamentotipo() != that.getCodecarquivamentotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecarquivamentotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecarquivamentotipo)) return false;
        return this.equalKeys(other) && ((Ecarquivamentotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecarquivamentotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecarquivamentotipo |");
        sb.append(" codecarquivamentotipo=").append(getCodecarquivamentotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecarquivamentotipo", Integer.valueOf(getCodecarquivamentotipo()));
        return ret;
    }

}
