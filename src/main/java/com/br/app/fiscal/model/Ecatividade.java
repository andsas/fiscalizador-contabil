package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECATIVIDADE")
public class Ecatividade implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecatividade";

    @Id
    @Column(name="CODECATIVIDADE", unique=true, nullable=false, precision=10)
    private int codecatividade;
    @Column(name="DESCECATIVIDADE", nullable=false, length=250)
    private String descecatividade;
    @OneToMany(mappedBy="ecatividade")
    private Set<Eccheckagenda> eccheckagenda;
    @OneToMany(mappedBy="ecatividade")
    private Set<Eccheckstatus> eccheckstatus;
    @OneToMany(mappedBy="ecatividade")
    private Set<Ecdoclancamento> ecdoclancamento;
    @OneToMany(mappedBy="ecatividade")
    private Set<Ecprtabatividadedet> ecprtabatividadedet;

    /** Default constructor. */
    public Ecatividade() {
        super();
    }

    /**
     * Access method for codecatividade.
     *
     * @return the current value of codecatividade
     */
    public int getCodecatividade() {
        return codecatividade;
    }

    /**
     * Setter method for codecatividade.
     *
     * @param aCodecatividade the new value for codecatividade
     */
    public void setCodecatividade(int aCodecatividade) {
        codecatividade = aCodecatividade;
    }

    /**
     * Access method for descecatividade.
     *
     * @return the current value of descecatividade
     */
    public String getDescecatividade() {
        return descecatividade;
    }

    /**
     * Setter method for descecatividade.
     *
     * @param aDescecatividade the new value for descecatividade
     */
    public void setDescecatividade(String aDescecatividade) {
        descecatividade = aDescecatividade;
    }

    /**
     * Access method for eccheckagenda.
     *
     * @return the current value of eccheckagenda
     */
    public Set<Eccheckagenda> getEccheckagenda() {
        return eccheckagenda;
    }

    /**
     * Setter method for eccheckagenda.
     *
     * @param aEccheckagenda the new value for eccheckagenda
     */
    public void setEccheckagenda(Set<Eccheckagenda> aEccheckagenda) {
        eccheckagenda = aEccheckagenda;
    }

    /**
     * Access method for eccheckstatus.
     *
     * @return the current value of eccheckstatus
     */
    public Set<Eccheckstatus> getEccheckstatus() {
        return eccheckstatus;
    }

    /**
     * Setter method for eccheckstatus.
     *
     * @param aEccheckstatus the new value for eccheckstatus
     */
    public void setEccheckstatus(Set<Eccheckstatus> aEccheckstatus) {
        eccheckstatus = aEccheckstatus;
    }

    /**
     * Access method for ecdoclancamento.
     *
     * @return the current value of ecdoclancamento
     */
    public Set<Ecdoclancamento> getEcdoclancamento() {
        return ecdoclancamento;
    }

    /**
     * Setter method for ecdoclancamento.
     *
     * @param aEcdoclancamento the new value for ecdoclancamento
     */
    public void setEcdoclancamento(Set<Ecdoclancamento> aEcdoclancamento) {
        ecdoclancamento = aEcdoclancamento;
    }

    /**
     * Access method for ecprtabatividadedet.
     *
     * @return the current value of ecprtabatividadedet
     */
    public Set<Ecprtabatividadedet> getEcprtabatividadedet() {
        return ecprtabatividadedet;
    }

    /**
     * Setter method for ecprtabatividadedet.
     *
     * @param aEcprtabatividadedet the new value for ecprtabatividadedet
     */
    public void setEcprtabatividadedet(Set<Ecprtabatividadedet> aEcprtabatividadedet) {
        ecprtabatividadedet = aEcprtabatividadedet;
    }

    /**
     * Compares the key for this instance with another Ecatividade.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecatividade and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecatividade)) {
            return false;
        }
        Ecatividade that = (Ecatividade) other;
        if (this.getCodecatividade() != that.getCodecatividade()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecatividade.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecatividade)) return false;
        return this.equalKeys(other) && ((Ecatividade)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecatividade();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecatividade |");
        sb.append(" codecatividade=").append(getCodecatividade());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecatividade", Integer.valueOf(getCodecatividade()));
        return ret;
    }

}
