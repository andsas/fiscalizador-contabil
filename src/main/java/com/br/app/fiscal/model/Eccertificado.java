package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECCERTIFICADO")
public class Eccertificado implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccertificado";

    @Id
    @Column(name="CODECCERTIFICADO", unique=true, nullable=false, precision=10)
    private int codeccertificado;
    @Column(name="DESCECCERTIFICADO", nullable=false, length=250)
    private String desceccertificado;
    @Column(name="SOLICITACAO", length=40)
    private String solicitacao;
    @Column(name="NFSENUMERO", precision=10)
    private int nfsenumero;
    @Column(name="NFSEDATA")
    private Timestamp nfsedata;
    @Column(name="EMISSAODATA", nullable=false)
    private Timestamp emissaodata;
    @Column(name="EXPIRACAODATA", nullable=false)
    private Timestamp expiracaodata;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGCERTIFICADORA", nullable=false)
    private Agagente agagente2;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECRAIZCERTIFICADO", nullable=false)
    private Eccertificadoraiz eccertificadoraiz;
    @ManyToOne
    @JoinColumn(name="CODAGREPRESENTANTE")
    private Agagente agagente3;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECTIPOCERTIFICADO", nullable=false)
    private Eccertificadotipo eccertificadotipo;

    /** Default constructor. */
    public Eccertificado() {
        super();
    }

    /**
     * Access method for codeccertificado.
     *
     * @return the current value of codeccertificado
     */
    public int getCodeccertificado() {
        return codeccertificado;
    }

    /**
     * Setter method for codeccertificado.
     *
     * @param aCodeccertificado the new value for codeccertificado
     */
    public void setCodeccertificado(int aCodeccertificado) {
        codeccertificado = aCodeccertificado;
    }

    /**
     * Access method for desceccertificado.
     *
     * @return the current value of desceccertificado
     */
    public String getDesceccertificado() {
        return desceccertificado;
    }

    /**
     * Setter method for desceccertificado.
     *
     * @param aDesceccertificado the new value for desceccertificado
     */
    public void setDesceccertificado(String aDesceccertificado) {
        desceccertificado = aDesceccertificado;
    }

    /**
     * Access method for solicitacao.
     *
     * @return the current value of solicitacao
     */
    public String getSolicitacao() {
        return solicitacao;
    }

    /**
     * Setter method for solicitacao.
     *
     * @param aSolicitacao the new value for solicitacao
     */
    public void setSolicitacao(String aSolicitacao) {
        solicitacao = aSolicitacao;
    }

    /**
     * Access method for nfsenumero.
     *
     * @return the current value of nfsenumero
     */
    public int getNfsenumero() {
        return nfsenumero;
    }

    /**
     * Setter method for nfsenumero.
     *
     * @param aNfsenumero the new value for nfsenumero
     */
    public void setNfsenumero(int aNfsenumero) {
        nfsenumero = aNfsenumero;
    }

    /**
     * Access method for nfsedata.
     *
     * @return the current value of nfsedata
     */
    public Timestamp getNfsedata() {
        return nfsedata;
    }

    /**
     * Setter method for nfsedata.
     *
     * @param aNfsedata the new value for nfsedata
     */
    public void setNfsedata(Timestamp aNfsedata) {
        nfsedata = aNfsedata;
    }

    /**
     * Access method for emissaodata.
     *
     * @return the current value of emissaodata
     */
    public Timestamp getEmissaodata() {
        return emissaodata;
    }

    /**
     * Setter method for emissaodata.
     *
     * @param aEmissaodata the new value for emissaodata
     */
    public void setEmissaodata(Timestamp aEmissaodata) {
        emissaodata = aEmissaodata;
    }

    /**
     * Access method for expiracaodata.
     *
     * @return the current value of expiracaodata
     */
    public Timestamp getExpiracaodata() {
        return expiracaodata;
    }

    /**
     * Setter method for expiracaodata.
     *
     * @param aExpiracaodata the new value for expiracaodata
     */
    public void setExpiracaodata(Timestamp aExpiracaodata) {
        expiracaodata = aExpiracaodata;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Agagente getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Agagente aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for eccertificadoraiz.
     *
     * @return the current value of eccertificadoraiz
     */
    public Eccertificadoraiz getEccertificadoraiz() {
        return eccertificadoraiz;
    }

    /**
     * Setter method for eccertificadoraiz.
     *
     * @param aEccertificadoraiz the new value for eccertificadoraiz
     */
    public void setEccertificadoraiz(Eccertificadoraiz aEccertificadoraiz) {
        eccertificadoraiz = aEccertificadoraiz;
    }

    /**
     * Access method for agagente3.
     *
     * @return the current value of agagente3
     */
    public Agagente getAgagente3() {
        return agagente3;
    }

    /**
     * Setter method for agagente3.
     *
     * @param aAgagente3 the new value for agagente3
     */
    public void setAgagente3(Agagente aAgagente3) {
        agagente3 = aAgagente3;
    }

    /**
     * Access method for eccertificadotipo.
     *
     * @return the current value of eccertificadotipo
     */
    public Eccertificadotipo getEccertificadotipo() {
        return eccertificadotipo;
    }

    /**
     * Setter method for eccertificadotipo.
     *
     * @param aEccertificadotipo the new value for eccertificadotipo
     */
    public void setEccertificadotipo(Eccertificadotipo aEccertificadotipo) {
        eccertificadotipo = aEccertificadotipo;
    }

    /**
     * Compares the key for this instance with another Eccertificado.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccertificado and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccertificado)) {
            return false;
        }
        Eccertificado that = (Eccertificado) other;
        if (this.getCodeccertificado() != that.getCodeccertificado()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccertificado.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccertificado)) return false;
        return this.equalKeys(other) && ((Eccertificado)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccertificado();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccertificado |");
        sb.append(" codeccertificado=").append(getCodeccertificado());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccertificado", Integer.valueOf(getCodeccertificado()));
        return ret;
    }

}
