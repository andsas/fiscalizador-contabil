package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECCERTIFICADORAIZ")
public class Eccertificadoraiz implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccertificadoraiz";

    @Id
    @Column(name="CODECCERTIFICADORAIZ", unique=true, nullable=false, precision=10)
    private int codeccertificadoraiz;
    @Column(name="DESCECCERTIFICADORAIZ", nullable=false, length=250)
    private String desceccertificadoraiz;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @OneToMany(mappedBy="eccertificadoraiz")
    private Set<Eccertificado> eccertificado;

    /** Default constructor. */
    public Eccertificadoraiz() {
        super();
    }

    /**
     * Access method for codeccertificadoraiz.
     *
     * @return the current value of codeccertificadoraiz
     */
    public int getCodeccertificadoraiz() {
        return codeccertificadoraiz;
    }

    /**
     * Setter method for codeccertificadoraiz.
     *
     * @param aCodeccertificadoraiz the new value for codeccertificadoraiz
     */
    public void setCodeccertificadoraiz(int aCodeccertificadoraiz) {
        codeccertificadoraiz = aCodeccertificadoraiz;
    }

    /**
     * Access method for desceccertificadoraiz.
     *
     * @return the current value of desceccertificadoraiz
     */
    public String getDesceccertificadoraiz() {
        return desceccertificadoraiz;
    }

    /**
     * Setter method for desceccertificadoraiz.
     *
     * @param aDesceccertificadoraiz the new value for desceccertificadoraiz
     */
    public void setDesceccertificadoraiz(String aDesceccertificadoraiz) {
        desceccertificadoraiz = aDesceccertificadoraiz;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for eccertificado.
     *
     * @return the current value of eccertificado
     */
    public Set<Eccertificado> getEccertificado() {
        return eccertificado;
    }

    /**
     * Setter method for eccertificado.
     *
     * @param aEccertificado the new value for eccertificado
     */
    public void setEccertificado(Set<Eccertificado> aEccertificado) {
        eccertificado = aEccertificado;
    }

    /**
     * Compares the key for this instance with another Eccertificadoraiz.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccertificadoraiz and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccertificadoraiz)) {
            return false;
        }
        Eccertificadoraiz that = (Eccertificadoraiz) other;
        if (this.getCodeccertificadoraiz() != that.getCodeccertificadoraiz()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccertificadoraiz.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccertificadoraiz)) return false;
        return this.equalKeys(other) && ((Eccertificadoraiz)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccertificadoraiz();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccertificadoraiz |");
        sb.append(" codeccertificadoraiz=").append(getCodeccertificadoraiz());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccertificadoraiz", Integer.valueOf(getCodeccertificadoraiz()));
        return ret;
    }

}
