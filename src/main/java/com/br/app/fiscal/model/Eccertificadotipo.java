package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECCERTIFICADOTIPO")
public class Eccertificadotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccertificadotipo";

    @Id
    @Column(name="CODECCERTIFICADOTIPO", unique=true, nullable=false, precision=10)
    private int codeccertificadotipo;
    @Column(name="DESCECCERTIFICADOTIPO", nullable=false, length=250)
    private String desceccertificadotipo;
    @Column(name="TIPOMIDIA", precision=5)
    private short tipomidia;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @OneToMany(mappedBy="eccertificadotipo")
    private Set<Eccertificado> eccertificado;

    /** Default constructor. */
    public Eccertificadotipo() {
        super();
    }

    /**
     * Access method for codeccertificadotipo.
     *
     * @return the current value of codeccertificadotipo
     */
    public int getCodeccertificadotipo() {
        return codeccertificadotipo;
    }

    /**
     * Setter method for codeccertificadotipo.
     *
     * @param aCodeccertificadotipo the new value for codeccertificadotipo
     */
    public void setCodeccertificadotipo(int aCodeccertificadotipo) {
        codeccertificadotipo = aCodeccertificadotipo;
    }

    /**
     * Access method for desceccertificadotipo.
     *
     * @return the current value of desceccertificadotipo
     */
    public String getDesceccertificadotipo() {
        return desceccertificadotipo;
    }

    /**
     * Setter method for desceccertificadotipo.
     *
     * @param aDesceccertificadotipo the new value for desceccertificadotipo
     */
    public void setDesceccertificadotipo(String aDesceccertificadotipo) {
        desceccertificadotipo = aDesceccertificadotipo;
    }

    /**
     * Access method for tipomidia.
     *
     * @return the current value of tipomidia
     */
    public short getTipomidia() {
        return tipomidia;
    }

    /**
     * Setter method for tipomidia.
     *
     * @param aTipomidia the new value for tipomidia
     */
    public void setTipomidia(short aTipomidia) {
        tipomidia = aTipomidia;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for eccertificado.
     *
     * @return the current value of eccertificado
     */
    public Set<Eccertificado> getEccertificado() {
        return eccertificado;
    }

    /**
     * Setter method for eccertificado.
     *
     * @param aEccertificado the new value for eccertificado
     */
    public void setEccertificado(Set<Eccertificado> aEccertificado) {
        eccertificado = aEccertificado;
    }

    /**
     * Compares the key for this instance with another Eccertificadotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccertificadotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccertificadotipo)) {
            return false;
        }
        Eccertificadotipo that = (Eccertificadotipo) other;
        if (this.getCodeccertificadotipo() != that.getCodeccertificadotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccertificadotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccertificadotipo)) return false;
        return this.equalKeys(other) && ((Eccertificadotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccertificadotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccertificadotipo |");
        sb.append(" codeccertificadotipo=").append(getCodeccertificadotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccertificadotipo", Integer.valueOf(getCodeccertificadotipo()));
        return ret;
    }

}
