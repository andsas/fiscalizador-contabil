package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECCHECKAGENDA")
public class Eccheckagenda implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccheckagenda";

    @Id
    @Column(name="CODECCHECKAGENDA", unique=true, nullable=false, precision=10)
    private int codeccheckagenda;
    @Column(name="DATA", nullable=false)
    private Timestamp data;
    @Column(name="AGENDAMENTO", nullable=false)
    private Timestamp agendamento;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIO")
    private Confusuario confusuario;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODECDOCTIPO")
    private Ecdoctipo ecdoctipo;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUAGENDAMENTO")
    private Confusuario confusuario2;
    @ManyToOne
    @JoinColumn(name="CODECATIVIDADE")
    private Ecatividade ecatividade;
    @ManyToOne
    @JoinColumn(name="CODECSTATUS")
    private Ecstatus ecstatus;

    /** Default constructor. */
    public Eccheckagenda() {
        super();
    }

    /**
     * Access method for codeccheckagenda.
     *
     * @return the current value of codeccheckagenda
     */
    public int getCodeccheckagenda() {
        return codeccheckagenda;
    }

    /**
     * Setter method for codeccheckagenda.
     *
     * @param aCodeccheckagenda the new value for codeccheckagenda
     */
    public void setCodeccheckagenda(int aCodeccheckagenda) {
        codeccheckagenda = aCodeccheckagenda;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for agendamento.
     *
     * @return the current value of agendamento
     */
    public Timestamp getAgendamento() {
        return agendamento;
    }

    /**
     * Setter method for agendamento.
     *
     * @param aAgendamento the new value for agendamento
     */
    public void setAgendamento(Timestamp aAgendamento) {
        agendamento = aAgendamento;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for ecdoctipo.
     *
     * @return the current value of ecdoctipo
     */
    public Ecdoctipo getEcdoctipo() {
        return ecdoctipo;
    }

    /**
     * Setter method for ecdoctipo.
     *
     * @param aEcdoctipo the new value for ecdoctipo
     */
    public void setEcdoctipo(Ecdoctipo aEcdoctipo) {
        ecdoctipo = aEcdoctipo;
    }

    /**
     * Access method for confusuario2.
     *
     * @return the current value of confusuario2
     */
    public Confusuario getConfusuario2() {
        return confusuario2;
    }

    /**
     * Setter method for confusuario2.
     *
     * @param aConfusuario2 the new value for confusuario2
     */
    public void setConfusuario2(Confusuario aConfusuario2) {
        confusuario2 = aConfusuario2;
    }

    /**
     * Access method for ecatividade.
     *
     * @return the current value of ecatividade
     */
    public Ecatividade getEcatividade() {
        return ecatividade;
    }

    /**
     * Setter method for ecatividade.
     *
     * @param aEcatividade the new value for ecatividade
     */
    public void setEcatividade(Ecatividade aEcatividade) {
        ecatividade = aEcatividade;
    }

    /**
     * Access method for ecstatus.
     *
     * @return the current value of ecstatus
     */
    public Ecstatus getEcstatus() {
        return ecstatus;
    }

    /**
     * Setter method for ecstatus.
     *
     * @param aEcstatus the new value for ecstatus
     */
    public void setEcstatus(Ecstatus aEcstatus) {
        ecstatus = aEcstatus;
    }

    /**
     * Compares the key for this instance with another Eccheckagenda.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccheckagenda and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccheckagenda)) {
            return false;
        }
        Eccheckagenda that = (Eccheckagenda) other;
        if (this.getCodeccheckagenda() != that.getCodeccheckagenda()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccheckagenda.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccheckagenda)) return false;
        return this.equalKeys(other) && ((Eccheckagenda)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccheckagenda();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccheckagenda |");
        sb.append(" codeccheckagenda=").append(getCodeccheckagenda());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccheckagenda", Integer.valueOf(getCodeccheckagenda()));
        return ret;
    }

}
