package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECCHECKMODELO")
public class Eccheckmodelo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccheckmodelo";

    @Id
    @Column(name="CODECCHECKMODELO", unique=true, nullable=false, precision=10)
    private int codeccheckmodelo;
    @Column(name="DESCECCHECKMODELO", nullable=false, length=250)
    private String desceccheckmodelo;
    @OneToMany(mappedBy="eccheckmodelo")
    private Set<Eccheckstatusagente> eccheckstatusagente;

    /** Default constructor. */
    public Eccheckmodelo() {
        super();
    }

    /**
     * Access method for codeccheckmodelo.
     *
     * @return the current value of codeccheckmodelo
     */
    public int getCodeccheckmodelo() {
        return codeccheckmodelo;
    }

    /**
     * Setter method for codeccheckmodelo.
     *
     * @param aCodeccheckmodelo the new value for codeccheckmodelo
     */
    public void setCodeccheckmodelo(int aCodeccheckmodelo) {
        codeccheckmodelo = aCodeccheckmodelo;
    }

    /**
     * Access method for desceccheckmodelo.
     *
     * @return the current value of desceccheckmodelo
     */
    public String getDesceccheckmodelo() {
        return desceccheckmodelo;
    }

    /**
     * Setter method for desceccheckmodelo.
     *
     * @param aDesceccheckmodelo the new value for desceccheckmodelo
     */
    public void setDesceccheckmodelo(String aDesceccheckmodelo) {
        desceccheckmodelo = aDesceccheckmodelo;
    }

    /**
     * Access method for eccheckstatusagente.
     *
     * @return the current value of eccheckstatusagente
     */
    public Set<Eccheckstatusagente> getEccheckstatusagente() {
        return eccheckstatusagente;
    }

    /**
     * Setter method for eccheckstatusagente.
     *
     * @param aEccheckstatusagente the new value for eccheckstatusagente
     */
    public void setEccheckstatusagente(Set<Eccheckstatusagente> aEccheckstatusagente) {
        eccheckstatusagente = aEccheckstatusagente;
    }

    /**
     * Compares the key for this instance with another Eccheckmodelo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccheckmodelo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccheckmodelo)) {
            return false;
        }
        Eccheckmodelo that = (Eccheckmodelo) other;
        if (this.getCodeccheckmodelo() != that.getCodeccheckmodelo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccheckmodelo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccheckmodelo)) return false;
        return this.equalKeys(other) && ((Eccheckmodelo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccheckmodelo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccheckmodelo |");
        sb.append(" codeccheckmodelo=").append(getCodeccheckmodelo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccheckmodelo", Integer.valueOf(getCodeccheckmodelo()));
        return ret;
    }

}
