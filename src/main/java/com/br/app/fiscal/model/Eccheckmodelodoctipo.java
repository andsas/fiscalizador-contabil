package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="ECCHECKMODELODOCTIPO")
public class Eccheckmodelodoctipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccheckmodelodoctipo";

    @Id
    @Column(name="CODECCHECKMODELODOCTIPO", unique=true, nullable=false, precision=10)
    private int codeccheckmodelodoctipo;
    @Column(name="CODECCHECKMODELO", nullable=false, precision=10)
    private int codeccheckmodelo;
    @Column(name="CODECDOCTIPO", nullable=false, length=20)
    private String codecdoctipo;

    /** Default constructor. */
    public Eccheckmodelodoctipo() {
        super();
    }

    /**
     * Access method for codeccheckmodelodoctipo.
     *
     * @return the current value of codeccheckmodelodoctipo
     */
    public int getCodeccheckmodelodoctipo() {
        return codeccheckmodelodoctipo;
    }

    /**
     * Setter method for codeccheckmodelodoctipo.
     *
     * @param aCodeccheckmodelodoctipo the new value for codeccheckmodelodoctipo
     */
    public void setCodeccheckmodelodoctipo(int aCodeccheckmodelodoctipo) {
        codeccheckmodelodoctipo = aCodeccheckmodelodoctipo;
    }

    /**
     * Access method for codeccheckmodelo.
     *
     * @return the current value of codeccheckmodelo
     */
    public int getCodeccheckmodelo() {
        return codeccheckmodelo;
    }

    /**
     * Setter method for codeccheckmodelo.
     *
     * @param aCodeccheckmodelo the new value for codeccheckmodelo
     */
    public void setCodeccheckmodelo(int aCodeccheckmodelo) {
        codeccheckmodelo = aCodeccheckmodelo;
    }

    /**
     * Access method for codecdoctipo.
     *
     * @return the current value of codecdoctipo
     */
    public String getCodecdoctipo() {
        return codecdoctipo;
    }

    /**
     * Setter method for codecdoctipo.
     *
     * @param aCodecdoctipo the new value for codecdoctipo
     */
    public void setCodecdoctipo(String aCodecdoctipo) {
        codecdoctipo = aCodecdoctipo;
    }

    /**
     * Compares the key for this instance with another Eccheckmodelodoctipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccheckmodelodoctipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccheckmodelodoctipo)) {
            return false;
        }
        Eccheckmodelodoctipo that = (Eccheckmodelodoctipo) other;
        if (this.getCodeccheckmodelodoctipo() != that.getCodeccheckmodelodoctipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccheckmodelodoctipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccheckmodelodoctipo)) return false;
        return this.equalKeys(other) && ((Eccheckmodelodoctipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccheckmodelodoctipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccheckmodelodoctipo |");
        sb.append(" codeccheckmodelodoctipo=").append(getCodeccheckmodelodoctipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccheckmodelodoctipo", Integer.valueOf(getCodeccheckmodelodoctipo()));
        return ret;
    }

}
