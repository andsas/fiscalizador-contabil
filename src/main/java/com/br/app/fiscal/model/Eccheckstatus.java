package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECCHECKSTATUS")
public class Eccheckstatus implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccheckstatus";

    @Id
    @Column(name="CODECCHECKSTATUS", unique=true, nullable=false, precision=10)
    private int codeccheckstatus;
    @Column(name="DATA", nullable=false)
    private Timestamp data;
    @Column(name="OBS")
    private String obs;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @Column(name="CODECDOCTIPO", length=20)
    private String codecdoctipo;
    @ManyToOne
    @JoinColumn(name="CODECCHECKSTATUSAGENTE")
    private Eccheckstatusagente eccheckstatusagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIO", nullable=false)
    private Confusuario confusuario;
    @ManyToOne
    @JoinColumn(name="CODCONFADMIN")
    private Confusuario confusuario2;
    @ManyToOne
    @JoinColumn(name="CODECDOCLANCAMENTO")
    private Ecdoclancamento ecdoclancamento;
    @ManyToOne
    @JoinColumn(name="CODECSTATUS")
    private Ecstatus ecstatus;
    @ManyToOne
    @JoinColumn(name="CODECPRTABELA")
    private Ecprtabela ecprtabela;
    @ManyToOne
    @JoinColumn(name="CODECATIVIDADE")
    private Ecatividade ecatividade;
    @ManyToOne
    @JoinColumn(name="CODECPRTABATIVIDADE")
    private Ecprtabatividade ecprtabatividade;
    @ManyToOne
    @JoinColumn(name="CODAGAGENTE")
    private Agagente agagente;

    /** Default constructor. */
    public Eccheckstatus() {
        super();
    }

    /**
     * Access method for codeccheckstatus.
     *
     * @return the current value of codeccheckstatus
     */
    public int getCodeccheckstatus() {
        return codeccheckstatus;
    }

    /**
     * Setter method for codeccheckstatus.
     *
     * @param aCodeccheckstatus the new value for codeccheckstatus
     */
    public void setCodeccheckstatus(int aCodeccheckstatus) {
        codeccheckstatus = aCodeccheckstatus;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for codecdoctipo.
     *
     * @return the current value of codecdoctipo
     */
    public String getCodecdoctipo() {
        return codecdoctipo;
    }

    /**
     * Setter method for codecdoctipo.
     *
     * @param aCodecdoctipo the new value for codecdoctipo
     */
    public void setCodecdoctipo(String aCodecdoctipo) {
        codecdoctipo = aCodecdoctipo;
    }

    /**
     * Access method for eccheckstatusagente.
     *
     * @return the current value of eccheckstatusagente
     */
    public Eccheckstatusagente getEccheckstatusagente() {
        return eccheckstatusagente;
    }

    /**
     * Setter method for eccheckstatusagente.
     *
     * @param aEccheckstatusagente the new value for eccheckstatusagente
     */
    public void setEccheckstatusagente(Eccheckstatusagente aEccheckstatusagente) {
        eccheckstatusagente = aEccheckstatusagente;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for confusuario2.
     *
     * @return the current value of confusuario2
     */
    public Confusuario getConfusuario2() {
        return confusuario2;
    }

    /**
     * Setter method for confusuario2.
     *
     * @param aConfusuario2 the new value for confusuario2
     */
    public void setConfusuario2(Confusuario aConfusuario2) {
        confusuario2 = aConfusuario2;
    }

    /**
     * Access method for ecdoclancamento.
     *
     * @return the current value of ecdoclancamento
     */
    public Ecdoclancamento getEcdoclancamento() {
        return ecdoclancamento;
    }

    /**
     * Setter method for ecdoclancamento.
     *
     * @param aEcdoclancamento the new value for ecdoclancamento
     */
    public void setEcdoclancamento(Ecdoclancamento aEcdoclancamento) {
        ecdoclancamento = aEcdoclancamento;
    }

    /**
     * Access method for ecstatus.
     *
     * @return the current value of ecstatus
     */
    public Ecstatus getEcstatus() {
        return ecstatus;
    }

    /**
     * Setter method for ecstatus.
     *
     * @param aEcstatus the new value for ecstatus
     */
    public void setEcstatus(Ecstatus aEcstatus) {
        ecstatus = aEcstatus;
    }

    /**
     * Access method for ecprtabela.
     *
     * @return the current value of ecprtabela
     */
    public Ecprtabela getEcprtabela() {
        return ecprtabela;
    }

    /**
     * Setter method for ecprtabela.
     *
     * @param aEcprtabela the new value for ecprtabela
     */
    public void setEcprtabela(Ecprtabela aEcprtabela) {
        ecprtabela = aEcprtabela;
    }

    /**
     * Access method for ecatividade.
     *
     * @return the current value of ecatividade
     */
    public Ecatividade getEcatividade() {
        return ecatividade;
    }

    /**
     * Setter method for ecatividade.
     *
     * @param aEcatividade the new value for ecatividade
     */
    public void setEcatividade(Ecatividade aEcatividade) {
        ecatividade = aEcatividade;
    }

    /**
     * Access method for ecprtabatividade.
     *
     * @return the current value of ecprtabatividade
     */
    public Ecprtabatividade getEcprtabatividade() {
        return ecprtabatividade;
    }

    /**
     * Setter method for ecprtabatividade.
     *
     * @param aEcprtabatividade the new value for ecprtabatividade
     */
    public void setEcprtabatividade(Ecprtabatividade aEcprtabatividade) {
        ecprtabatividade = aEcprtabatividade;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Compares the key for this instance with another Eccheckstatus.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccheckstatus and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccheckstatus)) {
            return false;
        }
        Eccheckstatus that = (Eccheckstatus) other;
        if (this.getCodeccheckstatus() != that.getCodeccheckstatus()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccheckstatus.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccheckstatus)) return false;
        return this.equalKeys(other) && ((Eccheckstatus)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccheckstatus();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccheckstatus |");
        sb.append(" codeccheckstatus=").append(getCodeccheckstatus());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccheckstatus", Integer.valueOf(getCodeccheckstatus()));
        return ret;
    }

}
