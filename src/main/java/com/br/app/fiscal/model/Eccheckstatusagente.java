package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="ECCHECKSTATUSAGENTE")
public class Eccheckstatusagente implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccheckstatusagente";

    @Id
    @Column(name="CODECCHECKSTATUSAGENTE", unique=true, nullable=false, precision=10)
    private int codeccheckstatusagente;
    @OneToMany(mappedBy="eccheckstatusagente")
    private Set<Eccheckstatus> eccheckstatus;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECCHECKMODELO", nullable=false)
    private Eccheckmodelo eccheckmodelo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;

    /** Default constructor. */
    public Eccheckstatusagente() {
        super();
    }

    /**
     * Access method for codeccheckstatusagente.
     *
     * @return the current value of codeccheckstatusagente
     */
    public int getCodeccheckstatusagente() {
        return codeccheckstatusagente;
    }

    /**
     * Setter method for codeccheckstatusagente.
     *
     * @param aCodeccheckstatusagente the new value for codeccheckstatusagente
     */
    public void setCodeccheckstatusagente(int aCodeccheckstatusagente) {
        codeccheckstatusagente = aCodeccheckstatusagente;
    }

    /**
     * Access method for eccheckstatus.
     *
     * @return the current value of eccheckstatus
     */
    public Set<Eccheckstatus> getEccheckstatus() {
        return eccheckstatus;
    }

    /**
     * Setter method for eccheckstatus.
     *
     * @param aEccheckstatus the new value for eccheckstatus
     */
    public void setEccheckstatus(Set<Eccheckstatus> aEccheckstatus) {
        eccheckstatus = aEccheckstatus;
    }

    /**
     * Access method for eccheckmodelo.
     *
     * @return the current value of eccheckmodelo
     */
    public Eccheckmodelo getEccheckmodelo() {
        return eccheckmodelo;
    }

    /**
     * Setter method for eccheckmodelo.
     *
     * @param aEccheckmodelo the new value for eccheckmodelo
     */
    public void setEccheckmodelo(Eccheckmodelo aEccheckmodelo) {
        eccheckmodelo = aEccheckmodelo;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Compares the key for this instance with another Eccheckstatusagente.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccheckstatusagente and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccheckstatusagente)) {
            return false;
        }
        Eccheckstatusagente that = (Eccheckstatusagente) other;
        if (this.getCodeccheckstatusagente() != that.getCodeccheckstatusagente()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccheckstatusagente.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccheckstatusagente)) return false;
        return this.equalKeys(other) && ((Eccheckstatusagente)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccheckstatusagente();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccheckstatusagente |");
        sb.append(" codeccheckstatusagente=").append(getCodeccheckstatusagente());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccheckstatusagente", Integer.valueOf(getCodeccheckstatusagente()));
        return ret;
    }

}
