package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECCND")
public class Eccnd implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccnd";

    @Id
    @Column(name="CODECCND", unique=true, nullable=false, precision=10)
    private int codeccnd;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @Column(name="NUMERO", nullable=false, length=60)
    private String numero;
    @Column(name="DESCECCND", nullable=false, length=250)
    private String desceccnd;
    @Column(name="EMISSAODATA", nullable=false)
    private Timestamp emissaodata;
    @Column(name="VENCIMENTODATA", nullable=false)
    private Timestamp vencimentodata;
    @Column(name="LINK", length=250)
    private String link;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGORGAO", nullable=false)
    private Agagente agagente2;

    /** Default constructor. */
    public Eccnd() {
        super();
    }

    /**
     * Access method for codeccnd.
     *
     * @return the current value of codeccnd
     */
    public int getCodeccnd() {
        return codeccnd;
    }

    /**
     * Setter method for codeccnd.
     *
     * @param aCodeccnd the new value for codeccnd
     */
    public void setCodeccnd(int aCodeccnd) {
        codeccnd = aCodeccnd;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for numero.
     *
     * @return the current value of numero
     */
    public String getNumero() {
        return numero;
    }

    /**
     * Setter method for numero.
     *
     * @param aNumero the new value for numero
     */
    public void setNumero(String aNumero) {
        numero = aNumero;
    }

    /**
     * Access method for desceccnd.
     *
     * @return the current value of desceccnd
     */
    public String getDesceccnd() {
        return desceccnd;
    }

    /**
     * Setter method for desceccnd.
     *
     * @param aDesceccnd the new value for desceccnd
     */
    public void setDesceccnd(String aDesceccnd) {
        desceccnd = aDesceccnd;
    }

    /**
     * Access method for emissaodata.
     *
     * @return the current value of emissaodata
     */
    public Timestamp getEmissaodata() {
        return emissaodata;
    }

    /**
     * Setter method for emissaodata.
     *
     * @param aEmissaodata the new value for emissaodata
     */
    public void setEmissaodata(Timestamp aEmissaodata) {
        emissaodata = aEmissaodata;
    }

    /**
     * Access method for vencimentodata.
     *
     * @return the current value of vencimentodata
     */
    public Timestamp getVencimentodata() {
        return vencimentodata;
    }

    /**
     * Setter method for vencimentodata.
     *
     * @param aVencimentodata the new value for vencimentodata
     */
    public void setVencimentodata(Timestamp aVencimentodata) {
        vencimentodata = aVencimentodata;
    }

    /**
     * Access method for link.
     *
     * @return the current value of link
     */
    public String getLink() {
        return link;
    }

    /**
     * Setter method for link.
     *
     * @param aLink the new value for link
     */
    public void setLink(String aLink) {
        link = aLink;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Agagente getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Agagente aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Compares the key for this instance with another Eccnd.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccnd and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccnd)) {
            return false;
        }
        Eccnd that = (Eccnd) other;
        if (this.getCodeccnd() != that.getCodeccnd()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccnd.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccnd)) return false;
        return this.equalKeys(other) && ((Eccnd)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccnd();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccnd |");
        sb.append(" codeccnd=").append(getCodeccnd());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccnd", Integer.valueOf(getCodeccnd()));
        return ret;
    }

}
