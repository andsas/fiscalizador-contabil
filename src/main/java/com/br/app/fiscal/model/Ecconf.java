package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECCONF")
public class Ecconf implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecconf";

    @Id
    @Column(name="CODECCONF", unique=true, nullable=false, precision=10)
    private int codecconf;
    @Column(name="FINTIPOECDOCLANCAMENTO", precision=10)
    private int fintipoecdoclancamento;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;

    /** Default constructor. */
    public Ecconf() {
        super();
    }

    /**
     * Access method for codecconf.
     *
     * @return the current value of codecconf
     */
    public int getCodecconf() {
        return codecconf;
    }

    /**
     * Setter method for codecconf.
     *
     * @param aCodecconf the new value for codecconf
     */
    public void setCodecconf(int aCodecconf) {
        codecconf = aCodecconf;
    }

    /**
     * Access method for fintipoecdoclancamento.
     *
     * @return the current value of fintipoecdoclancamento
     */
    public int getFintipoecdoclancamento() {
        return fintipoecdoclancamento;
    }

    /**
     * Setter method for fintipoecdoclancamento.
     *
     * @param aFintipoecdoclancamento the new value for fintipoecdoclancamento
     */
    public void setFintipoecdoclancamento(int aFintipoecdoclancamento) {
        fintipoecdoclancamento = aFintipoecdoclancamento;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Compares the key for this instance with another Ecconf.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecconf and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecconf)) {
            return false;
        }
        Ecconf that = (Ecconf) other;
        if (this.getCodecconf() != that.getCodecconf()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecconf.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecconf)) return false;
        return this.equalKeys(other) && ((Ecconf)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecconf();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecconf |");
        sb.append(" codecconf=").append(getCodecconf());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecconf", Integer.valueOf(getCodecconf()));
        return ret;
    }

}
