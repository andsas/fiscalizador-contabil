package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECCORRESPLANCAMENTO")
public class Eccorresplancamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codeccorresplancamento";

    @Id
    @Column(name="CODECCORRESPLANCAMENTO", unique=true, nullable=false, precision=10)
    private int codeccorresplancamento;
    @Column(name="DESCECCORRESPLANCAMENTO", nullable=false, length=250)
    private String desceccorresplancamento;
    @Column(name="OBS")
    private String obs;
    @Column(name="DATA", nullable=false)
    private Timestamp data;
    @Column(name="STATUS", precision=5)
    private short status;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGDESTINATARIO", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGREMETENTE", nullable=false)
    private Agagente agagente2;

    /** Default constructor. */
    public Eccorresplancamento() {
        super();
    }

    /**
     * Access method for codeccorresplancamento.
     *
     * @return the current value of codeccorresplancamento
     */
    public int getCodeccorresplancamento() {
        return codeccorresplancamento;
    }

    /**
     * Setter method for codeccorresplancamento.
     *
     * @param aCodeccorresplancamento the new value for codeccorresplancamento
     */
    public void setCodeccorresplancamento(int aCodeccorresplancamento) {
        codeccorresplancamento = aCodeccorresplancamento;
    }

    /**
     * Access method for desceccorresplancamento.
     *
     * @return the current value of desceccorresplancamento
     */
    public String getDesceccorresplancamento() {
        return desceccorresplancamento;
    }

    /**
     * Setter method for desceccorresplancamento.
     *
     * @param aDesceccorresplancamento the new value for desceccorresplancamento
     */
    public void setDesceccorresplancamento(String aDesceccorresplancamento) {
        desceccorresplancamento = aDesceccorresplancamento;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public short getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(short aStatus) {
        status = aStatus;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Agagente getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Agagente aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Compares the key for this instance with another Eccorresplancamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Eccorresplancamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Eccorresplancamento)) {
            return false;
        }
        Eccorresplancamento that = (Eccorresplancamento) other;
        if (this.getCodeccorresplancamento() != that.getCodeccorresplancamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Eccorresplancamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Eccorresplancamento)) return false;
        return this.equalKeys(other) && ((Eccorresplancamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodeccorresplancamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Eccorresplancamento |");
        sb.append(" codeccorresplancamento=").append(getCodeccorresplancamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codeccorresplancamento", Integer.valueOf(getCodeccorresplancamento()));
        return ret;
    }

}
