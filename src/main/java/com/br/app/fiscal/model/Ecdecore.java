package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECDECORE")
public class Ecdecore implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecdecore";

    @Id
    @Column(name="CODECDECORE", unique=true, nullable=false, precision=10)
    private int codecdecore;
    @Column(name="DESCECDECORE", nullable=false, length=250)
    private String descecdecore;
    @Column(name="DATAEMISSAO", nullable=false)
    private Timestamp dataemissao;
    @Column(name="VALORDECLARADO", nullable=false, precision=15, scale=4)
    private BigDecimal valordeclarado;
    @Column(name="DATAPRESTACAO")
    private Timestamp dataprestacao;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECTIPODECORE", nullable=false)
    private Ecdecoretipo ecdecoretipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;

    /** Default constructor. */
    public Ecdecore() {
        super();
    }

    /**
     * Access method for codecdecore.
     *
     * @return the current value of codecdecore
     */
    public int getCodecdecore() {
        return codecdecore;
    }

    /**
     * Setter method for codecdecore.
     *
     * @param aCodecdecore the new value for codecdecore
     */
    public void setCodecdecore(int aCodecdecore) {
        codecdecore = aCodecdecore;
    }

    /**
     * Access method for descecdecore.
     *
     * @return the current value of descecdecore
     */
    public String getDescecdecore() {
        return descecdecore;
    }

    /**
     * Setter method for descecdecore.
     *
     * @param aDescecdecore the new value for descecdecore
     */
    public void setDescecdecore(String aDescecdecore) {
        descecdecore = aDescecdecore;
    }

    /**
     * Access method for dataemissao.
     *
     * @return the current value of dataemissao
     */
    public Timestamp getDataemissao() {
        return dataemissao;
    }

    /**
     * Setter method for dataemissao.
     *
     * @param aDataemissao the new value for dataemissao
     */
    public void setDataemissao(Timestamp aDataemissao) {
        dataemissao = aDataemissao;
    }

    /**
     * Access method for valordeclarado.
     *
     * @return the current value of valordeclarado
     */
    public BigDecimal getValordeclarado() {
        return valordeclarado;
    }

    /**
     * Setter method for valordeclarado.
     *
     * @param aValordeclarado the new value for valordeclarado
     */
    public void setValordeclarado(BigDecimal aValordeclarado) {
        valordeclarado = aValordeclarado;
    }

    /**
     * Access method for dataprestacao.
     *
     * @return the current value of dataprestacao
     */
    public Timestamp getDataprestacao() {
        return dataprestacao;
    }

    /**
     * Setter method for dataprestacao.
     *
     * @param aDataprestacao the new value for dataprestacao
     */
    public void setDataprestacao(Timestamp aDataprestacao) {
        dataprestacao = aDataprestacao;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for ecdecoretipo.
     *
     * @return the current value of ecdecoretipo
     */
    public Ecdecoretipo getEcdecoretipo() {
        return ecdecoretipo;
    }

    /**
     * Setter method for ecdecoretipo.
     *
     * @param aEcdecoretipo the new value for ecdecoretipo
     */
    public void setEcdecoretipo(Ecdecoretipo aEcdecoretipo) {
        ecdecoretipo = aEcdecoretipo;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Compares the key for this instance with another Ecdecore.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecdecore and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecdecore)) {
            return false;
        }
        Ecdecore that = (Ecdecore) other;
        if (this.getCodecdecore() != that.getCodecdecore()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecdecore.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecdecore)) return false;
        return this.equalKeys(other) && ((Ecdecore)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecdecore();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecdecore |");
        sb.append(" codecdecore=").append(getCodecdecore());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecdecore", Integer.valueOf(getCodecdecore()));
        return ret;
    }

}
