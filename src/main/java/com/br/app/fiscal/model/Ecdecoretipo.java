package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECDECORETIPO")
public class Ecdecoretipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecdecoretipo";

    @Id
    @Column(name="CODECDECORETIPO", unique=true, nullable=false, precision=10)
    private int codecdecoretipo;
    @Column(name="DESCECDECORETIPO", nullable=false, length=250)
    private String descecdecoretipo;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @OneToMany(mappedBy="ecdecoretipo")
    private Set<Ecdecore> ecdecore;

    /** Default constructor. */
    public Ecdecoretipo() {
        super();
    }

    /**
     * Access method for codecdecoretipo.
     *
     * @return the current value of codecdecoretipo
     */
    public int getCodecdecoretipo() {
        return codecdecoretipo;
    }

    /**
     * Setter method for codecdecoretipo.
     *
     * @param aCodecdecoretipo the new value for codecdecoretipo
     */
    public void setCodecdecoretipo(int aCodecdecoretipo) {
        codecdecoretipo = aCodecdecoretipo;
    }

    /**
     * Access method for descecdecoretipo.
     *
     * @return the current value of descecdecoretipo
     */
    public String getDescecdecoretipo() {
        return descecdecoretipo;
    }

    /**
     * Setter method for descecdecoretipo.
     *
     * @param aDescecdecoretipo the new value for descecdecoretipo
     */
    public void setDescecdecoretipo(String aDescecdecoretipo) {
        descecdecoretipo = aDescecdecoretipo;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for ecdecore.
     *
     * @return the current value of ecdecore
     */
    public Set<Ecdecore> getEcdecore() {
        return ecdecore;
    }

    /**
     * Setter method for ecdecore.
     *
     * @param aEcdecore the new value for ecdecore
     */
    public void setEcdecore(Set<Ecdecore> aEcdecore) {
        ecdecore = aEcdecore;
    }

    /**
     * Compares the key for this instance with another Ecdecoretipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecdecoretipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecdecoretipo)) {
            return false;
        }
        Ecdecoretipo that = (Ecdecoretipo) other;
        if (this.getCodecdecoretipo() != that.getCodecdecoretipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecdecoretipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecdecoretipo)) return false;
        return this.equalKeys(other) && ((Ecdecoretipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecdecoretipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecdecoretipo |");
        sb.append(" codecdecoretipo=").append(getCodecdecoretipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecdecoretipo", Integer.valueOf(getCodecdecoretipo()));
        return ret;
    }

}
