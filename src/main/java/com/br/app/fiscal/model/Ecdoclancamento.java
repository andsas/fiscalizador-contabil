package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="ECDOCLANCAMENTO")
public class Ecdoclancamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecdoclancamento";

    @Id
    @Column(name="CODECDOCLANCAMENTO", unique=true, nullable=false, precision=10)
    private int codecdoclancamento;
    @Column(name="DESCECDOCLANCAMENTO", nullable=false, length=250)
    private String descecdoclancamento;
    @Column(name="TOTAL", precision=15, scale=4)
    private BigDecimal total;
    @Column(name="OBS")
    private String obs;
    @Column(name="DATA", nullable=false)
    private Timestamp data;
    @Column(name="ATO", length=40)
    private String ato;
    @Column(name="PROTOCOLO", length=60)
    private String protocolo;
    @OneToMany(mappedBy="ecdoclancamento")
    private Set<Eccheckstatus> eccheckstatus;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="CODECDOCTIPO")
    private Ecdoctipo ecdoctipo;
    @ManyToOne
    @JoinColumn(name="CODECPRTABELA")
    private Ecprtabela ecprtabela;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODFINLANCAMENTO")
    private Finlancamento finlancamento;
    @ManyToOne
    @JoinColumn(name="CODECATIVIDADE")
    private Ecatividade ecatividade;
    @ManyToOne
    @JoinColumn(name="CODECPRTABATIVIDADE")
    private Ecprtabatividade ecprtabatividade;

    /** Default constructor. */
    public Ecdoclancamento() {
        super();
    }

    /**
     * Access method for codecdoclancamento.
     *
     * @return the current value of codecdoclancamento
     */
    public int getCodecdoclancamento() {
        return codecdoclancamento;
    }

    /**
     * Setter method for codecdoclancamento.
     *
     * @param aCodecdoclancamento the new value for codecdoclancamento
     */
    public void setCodecdoclancamento(int aCodecdoclancamento) {
        codecdoclancamento = aCodecdoclancamento;
    }

    /**
     * Access method for descecdoclancamento.
     *
     * @return the current value of descecdoclancamento
     */
    public String getDescecdoclancamento() {
        return descecdoclancamento;
    }

    /**
     * Setter method for descecdoclancamento.
     *
     * @param aDescecdoclancamento the new value for descecdoclancamento
     */
    public void setDescecdoclancamento(String aDescecdoclancamento) {
        descecdoclancamento = aDescecdoclancamento;
    }

    /**
     * Access method for total.
     *
     * @return the current value of total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Setter method for total.
     *
     * @param aTotal the new value for total
     */
    public void setTotal(BigDecimal aTotal) {
        total = aTotal;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for ato.
     *
     * @return the current value of ato
     */
    public String getAto() {
        return ato;
    }

    /**
     * Setter method for ato.
     *
     * @param aAto the new value for ato
     */
    public void setAto(String aAto) {
        ato = aAto;
    }

    /**
     * Access method for protocolo.
     *
     * @return the current value of protocolo
     */
    public String getProtocolo() {
        return protocolo;
    }

    /**
     * Setter method for protocolo.
     *
     * @param aProtocolo the new value for protocolo
     */
    public void setProtocolo(String aProtocolo) {
        protocolo = aProtocolo;
    }

    /**
     * Access method for eccheckstatus.
     *
     * @return the current value of eccheckstatus
     */
    public Set<Eccheckstatus> getEccheckstatus() {
        return eccheckstatus;
    }

    /**
     * Setter method for eccheckstatus.
     *
     * @param aEccheckstatus the new value for eccheckstatus
     */
    public void setEccheckstatus(Set<Eccheckstatus> aEccheckstatus) {
        eccheckstatus = aEccheckstatus;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for ecdoctipo.
     *
     * @return the current value of ecdoctipo
     */
    public Ecdoctipo getEcdoctipo() {
        return ecdoctipo;
    }

    /**
     * Setter method for ecdoctipo.
     *
     * @param aEcdoctipo the new value for ecdoctipo
     */
    public void setEcdoctipo(Ecdoctipo aEcdoctipo) {
        ecdoctipo = aEcdoctipo;
    }

    /**
     * Access method for ecprtabela.
     *
     * @return the current value of ecprtabela
     */
    public Ecprtabela getEcprtabela() {
        return ecprtabela;
    }

    /**
     * Setter method for ecprtabela.
     *
     * @param aEcprtabela the new value for ecprtabela
     */
    public void setEcprtabela(Ecprtabela aEcprtabela) {
        ecprtabela = aEcprtabela;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Finlancamento getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Finlancamento aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for ecatividade.
     *
     * @return the current value of ecatividade
     */
    public Ecatividade getEcatividade() {
        return ecatividade;
    }

    /**
     * Setter method for ecatividade.
     *
     * @param aEcatividade the new value for ecatividade
     */
    public void setEcatividade(Ecatividade aEcatividade) {
        ecatividade = aEcatividade;
    }

    /**
     * Access method for ecprtabatividade.
     *
     * @return the current value of ecprtabatividade
     */
    public Ecprtabatividade getEcprtabatividade() {
        return ecprtabatividade;
    }

    /**
     * Setter method for ecprtabatividade.
     *
     * @param aEcprtabatividade the new value for ecprtabatividade
     */
    public void setEcprtabatividade(Ecprtabatividade aEcprtabatividade) {
        ecprtabatividade = aEcprtabatividade;
    }

    /**
     * Compares the key for this instance with another Ecdoclancamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecdoclancamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecdoclancamento)) {
            return false;
        }
        Ecdoclancamento that = (Ecdoclancamento) other;
        if (this.getCodecdoclancamento() != that.getCodecdoclancamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecdoclancamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecdoclancamento)) return false;
        return this.equalKeys(other) && ((Ecdoclancamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecdoclancamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecdoclancamento |");
        sb.append(" codecdoclancamento=").append(getCodecdoclancamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecdoclancamento", Integer.valueOf(getCodecdoclancamento()));
        return ret;
    }

}
