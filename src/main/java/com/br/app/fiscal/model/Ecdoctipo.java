package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECDOCTIPO")
public class Ecdoctipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecdoctipo";

    @Id
    @Column(name="CODECDOCTIPO", unique=true, nullable=false, length=20)
    private String codecdoctipo;
    @Column(name="DESCECDOCTIPO", nullable=false, length=250)
    private String descecdoctipo;
    @Column(name="CODECPARENTDOCTIPO", length=20)
    private String codecparentdoctipo;
    @Column(name="NIVEL", precision=5)
    private short nivel;
    @OneToMany(mappedBy="ecdoctipo")
    private Set<Eccheckagenda> eccheckagenda;
    @OneToMany(mappedBy="ecdoctipo")
    private Set<Ecdoclancamento> ecdoclancamento;
    @OneToMany(mappedBy="ecdoctipo")
    private Set<Ecprtabeladet> ecprtabeladet;

    /** Default constructor. */
    public Ecdoctipo() {
        super();
    }

    /**
     * Access method for codecdoctipo.
     *
     * @return the current value of codecdoctipo
     */
    public String getCodecdoctipo() {
        return codecdoctipo;
    }

    /**
     * Setter method for codecdoctipo.
     *
     * @param aCodecdoctipo the new value for codecdoctipo
     */
    public void setCodecdoctipo(String aCodecdoctipo) {
        codecdoctipo = aCodecdoctipo;
    }

    /**
     * Access method for descecdoctipo.
     *
     * @return the current value of descecdoctipo
     */
    public String getDescecdoctipo() {
        return descecdoctipo;
    }

    /**
     * Setter method for descecdoctipo.
     *
     * @param aDescecdoctipo the new value for descecdoctipo
     */
    public void setDescecdoctipo(String aDescecdoctipo) {
        descecdoctipo = aDescecdoctipo;
    }

    /**
     * Access method for codecparentdoctipo.
     *
     * @return the current value of codecparentdoctipo
     */
    public String getCodecparentdoctipo() {
        return codecparentdoctipo;
    }

    /**
     * Setter method for codecparentdoctipo.
     *
     * @param aCodecparentdoctipo the new value for codecparentdoctipo
     */
    public void setCodecparentdoctipo(String aCodecparentdoctipo) {
        codecparentdoctipo = aCodecparentdoctipo;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for eccheckagenda.
     *
     * @return the current value of eccheckagenda
     */
    public Set<Eccheckagenda> getEccheckagenda() {
        return eccheckagenda;
    }

    /**
     * Setter method for eccheckagenda.
     *
     * @param aEccheckagenda the new value for eccheckagenda
     */
    public void setEccheckagenda(Set<Eccheckagenda> aEccheckagenda) {
        eccheckagenda = aEccheckagenda;
    }

    /**
     * Access method for ecdoclancamento.
     *
     * @return the current value of ecdoclancamento
     */
    public Set<Ecdoclancamento> getEcdoclancamento() {
        return ecdoclancamento;
    }

    /**
     * Setter method for ecdoclancamento.
     *
     * @param aEcdoclancamento the new value for ecdoclancamento
     */
    public void setEcdoclancamento(Set<Ecdoclancamento> aEcdoclancamento) {
        ecdoclancamento = aEcdoclancamento;
    }

    /**
     * Access method for ecprtabeladet.
     *
     * @return the current value of ecprtabeladet
     */
    public Set<Ecprtabeladet> getEcprtabeladet() {
        return ecprtabeladet;
    }

    /**
     * Setter method for ecprtabeladet.
     *
     * @param aEcprtabeladet the new value for ecprtabeladet
     */
    public void setEcprtabeladet(Set<Ecprtabeladet> aEcprtabeladet) {
        ecprtabeladet = aEcprtabeladet;
    }

    /**
     * Compares the key for this instance with another Ecdoctipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecdoctipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecdoctipo)) {
            return false;
        }
        Ecdoctipo that = (Ecdoctipo) other;
        Object myCodecdoctipo = this.getCodecdoctipo();
        Object yourCodecdoctipo = that.getCodecdoctipo();
        if (myCodecdoctipo==null ? yourCodecdoctipo!=null : !myCodecdoctipo.equals(yourCodecdoctipo)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecdoctipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecdoctipo)) return false;
        return this.equalKeys(other) && ((Ecdoctipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodecdoctipo() == null) {
            i = 0;
        } else {
            i = getCodecdoctipo().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecdoctipo |");
        sb.append(" codecdoctipo=").append(getCodecdoctipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecdoctipo", getCodecdoctipo());
        return ret;
    }

}
