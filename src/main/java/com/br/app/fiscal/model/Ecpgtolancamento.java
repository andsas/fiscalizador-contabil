package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECPGTOLANCAMENTO")
public class Ecpgtolancamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecpgtolancamento";

    @Id
    @Column(name="CODECPGTOLANCAMENTO", unique=true, nullable=false, precision=10)
    private int codecpgtolancamento;
    @Column(name="DESCECPGTOLANCAMENTO", nullable=false, length=250)
    private String descecpgtolancamento;
    @Column(name="CODCONFEMPR", nullable=false, precision=10)
    private int codconfempr;
    @Column(name="VALORPAGO", precision=15, scale=4)
    private BigDecimal valorpago;
    @Column(name="VALORINSS", precision=15, scale=4)
    private BigDecimal valorinss;
    @Column(name="VALORISSQN", precision=15, scale=4)
    private BigDecimal valorissqn;
    @Column(name="VALORIRPF", precision=15, scale=4)
    private BigDecimal valorirpf;
    @Column(name="VALORLIQUIDO", precision=15, scale=4)
    private BigDecimal valorliquido;
    @Column(name="PERCINSS", precision=15, scale=4)
    private BigDecimal percinss;
    @Column(name="PERCISSQN", precision=15, scale=4)
    private BigDecimal percissqn;
    @Column(name="PERCIRPF", precision=15, scale=4)
    private BigDecimal percirpf;
    @Column(name="DATA")
    private Timestamp data;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECPGTOTIPO", nullable=false)
    private Ecpgtotipo ecpgtotipo;

    /** Default constructor. */
    public Ecpgtolancamento() {
        super();
    }

    /**
     * Access method for codecpgtolancamento.
     *
     * @return the current value of codecpgtolancamento
     */
    public int getCodecpgtolancamento() {
        return codecpgtolancamento;
    }

    /**
     * Setter method for codecpgtolancamento.
     *
     * @param aCodecpgtolancamento the new value for codecpgtolancamento
     */
    public void setCodecpgtolancamento(int aCodecpgtolancamento) {
        codecpgtolancamento = aCodecpgtolancamento;
    }

    /**
     * Access method for descecpgtolancamento.
     *
     * @return the current value of descecpgtolancamento
     */
    public String getDescecpgtolancamento() {
        return descecpgtolancamento;
    }

    /**
     * Setter method for descecpgtolancamento.
     *
     * @param aDescecpgtolancamento the new value for descecpgtolancamento
     */
    public void setDescecpgtolancamento(String aDescecpgtolancamento) {
        descecpgtolancamento = aDescecpgtolancamento;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for valorpago.
     *
     * @return the current value of valorpago
     */
    public BigDecimal getValorpago() {
        return valorpago;
    }

    /**
     * Setter method for valorpago.
     *
     * @param aValorpago the new value for valorpago
     */
    public void setValorpago(BigDecimal aValorpago) {
        valorpago = aValorpago;
    }

    /**
     * Access method for valorinss.
     *
     * @return the current value of valorinss
     */
    public BigDecimal getValorinss() {
        return valorinss;
    }

    /**
     * Setter method for valorinss.
     *
     * @param aValorinss the new value for valorinss
     */
    public void setValorinss(BigDecimal aValorinss) {
        valorinss = aValorinss;
    }

    /**
     * Access method for valorissqn.
     *
     * @return the current value of valorissqn
     */
    public BigDecimal getValorissqn() {
        return valorissqn;
    }

    /**
     * Setter method for valorissqn.
     *
     * @param aValorissqn the new value for valorissqn
     */
    public void setValorissqn(BigDecimal aValorissqn) {
        valorissqn = aValorissqn;
    }

    /**
     * Access method for valorirpf.
     *
     * @return the current value of valorirpf
     */
    public BigDecimal getValorirpf() {
        return valorirpf;
    }

    /**
     * Setter method for valorirpf.
     *
     * @param aValorirpf the new value for valorirpf
     */
    public void setValorirpf(BigDecimal aValorirpf) {
        valorirpf = aValorirpf;
    }

    /**
     * Access method for valorliquido.
     *
     * @return the current value of valorliquido
     */
    public BigDecimal getValorliquido() {
        return valorliquido;
    }

    /**
     * Setter method for valorliquido.
     *
     * @param aValorliquido the new value for valorliquido
     */
    public void setValorliquido(BigDecimal aValorliquido) {
        valorliquido = aValorliquido;
    }

    /**
     * Access method for percinss.
     *
     * @return the current value of percinss
     */
    public BigDecimal getPercinss() {
        return percinss;
    }

    /**
     * Setter method for percinss.
     *
     * @param aPercinss the new value for percinss
     */
    public void setPercinss(BigDecimal aPercinss) {
        percinss = aPercinss;
    }

    /**
     * Access method for percissqn.
     *
     * @return the current value of percissqn
     */
    public BigDecimal getPercissqn() {
        return percissqn;
    }

    /**
     * Setter method for percissqn.
     *
     * @param aPercissqn the new value for percissqn
     */
    public void setPercissqn(BigDecimal aPercissqn) {
        percissqn = aPercissqn;
    }

    /**
     * Access method for percirpf.
     *
     * @return the current value of percirpf
     */
    public BigDecimal getPercirpf() {
        return percirpf;
    }

    /**
     * Setter method for percirpf.
     *
     * @param aPercirpf the new value for percirpf
     */
    public void setPercirpf(BigDecimal aPercirpf) {
        percirpf = aPercirpf;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for ecpgtotipo.
     *
     * @return the current value of ecpgtotipo
     */
    public Ecpgtotipo getEcpgtotipo() {
        return ecpgtotipo;
    }

    /**
     * Setter method for ecpgtotipo.
     *
     * @param aEcpgtotipo the new value for ecpgtotipo
     */
    public void setEcpgtotipo(Ecpgtotipo aEcpgtotipo) {
        ecpgtotipo = aEcpgtotipo;
    }

    /**
     * Compares the key for this instance with another Ecpgtolancamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecpgtolancamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecpgtolancamento)) {
            return false;
        }
        Ecpgtolancamento that = (Ecpgtolancamento) other;
        if (this.getCodecpgtolancamento() != that.getCodecpgtolancamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecpgtolancamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecpgtolancamento)) return false;
        return this.equalKeys(other) && ((Ecpgtolancamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecpgtolancamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecpgtolancamento |");
        sb.append(" codecpgtolancamento=").append(getCodecpgtolancamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecpgtolancamento", Integer.valueOf(getCodecpgtolancamento()));
        return ret;
    }

}
