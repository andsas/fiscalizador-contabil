package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECPGTOTIPO")
public class Ecpgtotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecpgtotipo";

    @Id
    @Column(name="CODECPGTOTIPO", unique=true, nullable=false, precision=10)
    private int codecpgtotipo;
    @Column(name="DESCECPGTOTIPO", nullable=false, length=250)
    private String descecpgtotipo;
    @Column(name="EMITERECIBO", precision=5)
    private short emiterecibo;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @OneToMany(mappedBy="ecpgtotipo")
    private Set<Ecpgtolancamento> ecpgtolancamento;

    /** Default constructor. */
    public Ecpgtotipo() {
        super();
    }

    /**
     * Access method for codecpgtotipo.
     *
     * @return the current value of codecpgtotipo
     */
    public int getCodecpgtotipo() {
        return codecpgtotipo;
    }

    /**
     * Setter method for codecpgtotipo.
     *
     * @param aCodecpgtotipo the new value for codecpgtotipo
     */
    public void setCodecpgtotipo(int aCodecpgtotipo) {
        codecpgtotipo = aCodecpgtotipo;
    }

    /**
     * Access method for descecpgtotipo.
     *
     * @return the current value of descecpgtotipo
     */
    public String getDescecpgtotipo() {
        return descecpgtotipo;
    }

    /**
     * Setter method for descecpgtotipo.
     *
     * @param aDescecpgtotipo the new value for descecpgtotipo
     */
    public void setDescecpgtotipo(String aDescecpgtotipo) {
        descecpgtotipo = aDescecpgtotipo;
    }

    /**
     * Access method for emiterecibo.
     *
     * @return the current value of emiterecibo
     */
    public short getEmiterecibo() {
        return emiterecibo;
    }

    /**
     * Setter method for emiterecibo.
     *
     * @param aEmiterecibo the new value for emiterecibo
     */
    public void setEmiterecibo(short aEmiterecibo) {
        emiterecibo = aEmiterecibo;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for ecpgtolancamento.
     *
     * @return the current value of ecpgtolancamento
     */
    public Set<Ecpgtolancamento> getEcpgtolancamento() {
        return ecpgtolancamento;
    }

    /**
     * Setter method for ecpgtolancamento.
     *
     * @param aEcpgtolancamento the new value for ecpgtolancamento
     */
    public void setEcpgtolancamento(Set<Ecpgtolancamento> aEcpgtolancamento) {
        ecpgtolancamento = aEcpgtolancamento;
    }

    /**
     * Compares the key for this instance with another Ecpgtotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecpgtotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecpgtotipo)) {
            return false;
        }
        Ecpgtotipo that = (Ecpgtotipo) other;
        if (this.getCodecpgtotipo() != that.getCodecpgtotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecpgtotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecpgtotipo)) return false;
        return this.equalKeys(other) && ((Ecpgtotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecpgtotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecpgtotipo |");
        sb.append(" codecpgtotipo=").append(getCodecpgtotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecpgtotipo", Integer.valueOf(getCodecpgtotipo()));
        return ret;
    }

}
