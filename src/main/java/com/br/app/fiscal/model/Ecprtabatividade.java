package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECPRTABATIVIDADE")
public class Ecprtabatividade implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecprtabatividade";

    @Id
    @Column(name="CODECPRTABATIVIDADE", unique=true, nullable=false, precision=10)
    private int codecprtabatividade;
    @Column(name="DESCECPRTABATIVIDADE", nullable=false, length=250)
    private String descecprtabatividade;
    @Column(name="DATAVALIDADE")
    private Timestamp datavalidade;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @OneToMany(mappedBy="ecprtabatividade")
    private Set<Eccheckstatus> eccheckstatus;
    @OneToMany(mappedBy="ecprtabatividade")
    private Set<Ecdoclancamento> ecdoclancamento;
    @OneToMany(mappedBy="ecprtabatividade")
    private Set<Ecprtabatividadedet> ecprtabatividadedet;

    /** Default constructor. */
    public Ecprtabatividade() {
        super();
    }

    /**
     * Access method for codecprtabatividade.
     *
     * @return the current value of codecprtabatividade
     */
    public int getCodecprtabatividade() {
        return codecprtabatividade;
    }

    /**
     * Setter method for codecprtabatividade.
     *
     * @param aCodecprtabatividade the new value for codecprtabatividade
     */
    public void setCodecprtabatividade(int aCodecprtabatividade) {
        codecprtabatividade = aCodecprtabatividade;
    }

    /**
     * Access method for descecprtabatividade.
     *
     * @return the current value of descecprtabatividade
     */
    public String getDescecprtabatividade() {
        return descecprtabatividade;
    }

    /**
     * Setter method for descecprtabatividade.
     *
     * @param aDescecprtabatividade the new value for descecprtabatividade
     */
    public void setDescecprtabatividade(String aDescecprtabatividade) {
        descecprtabatividade = aDescecprtabatividade;
    }

    /**
     * Access method for datavalidade.
     *
     * @return the current value of datavalidade
     */
    public Timestamp getDatavalidade() {
        return datavalidade;
    }

    /**
     * Setter method for datavalidade.
     *
     * @param aDatavalidade the new value for datavalidade
     */
    public void setDatavalidade(Timestamp aDatavalidade) {
        datavalidade = aDatavalidade;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for eccheckstatus.
     *
     * @return the current value of eccheckstatus
     */
    public Set<Eccheckstatus> getEccheckstatus() {
        return eccheckstatus;
    }

    /**
     * Setter method for eccheckstatus.
     *
     * @param aEccheckstatus the new value for eccheckstatus
     */
    public void setEccheckstatus(Set<Eccheckstatus> aEccheckstatus) {
        eccheckstatus = aEccheckstatus;
    }

    /**
     * Access method for ecdoclancamento.
     *
     * @return the current value of ecdoclancamento
     */
    public Set<Ecdoclancamento> getEcdoclancamento() {
        return ecdoclancamento;
    }

    /**
     * Setter method for ecdoclancamento.
     *
     * @param aEcdoclancamento the new value for ecdoclancamento
     */
    public void setEcdoclancamento(Set<Ecdoclancamento> aEcdoclancamento) {
        ecdoclancamento = aEcdoclancamento;
    }

    /**
     * Access method for ecprtabatividadedet.
     *
     * @return the current value of ecprtabatividadedet
     */
    public Set<Ecprtabatividadedet> getEcprtabatividadedet() {
        return ecprtabatividadedet;
    }

    /**
     * Setter method for ecprtabatividadedet.
     *
     * @param aEcprtabatividadedet the new value for ecprtabatividadedet
     */
    public void setEcprtabatividadedet(Set<Ecprtabatividadedet> aEcprtabatividadedet) {
        ecprtabatividadedet = aEcprtabatividadedet;
    }

    /**
     * Compares the key for this instance with another Ecprtabatividade.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecprtabatividade and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecprtabatividade)) {
            return false;
        }
        Ecprtabatividade that = (Ecprtabatividade) other;
        if (this.getCodecprtabatividade() != that.getCodecprtabatividade()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecprtabatividade.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecprtabatividade)) return false;
        return this.equalKeys(other) && ((Ecprtabatividade)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecprtabatividade();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecprtabatividade |");
        sb.append(" codecprtabatividade=").append(getCodecprtabatividade());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecprtabatividade", Integer.valueOf(getCodecprtabatividade()));
        return ret;
    }

}
