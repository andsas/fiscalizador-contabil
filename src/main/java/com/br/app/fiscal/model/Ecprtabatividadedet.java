package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECPRTABATIVIDADEDET")
public class Ecprtabatividadedet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecprtabatividadedet";

    @Id
    @Column(name="CODECPRTABATIVIDADEDET", unique=true, nullable=false, precision=10)
    private int codecprtabatividadedet;
    @Column(name="PRECO", precision=15, scale=4)
    private BigDecimal preco;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECPRTABATIVIDADE", nullable=false)
    private Ecprtabatividade ecprtabatividade;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECATIVIDADE", nullable=false)
    private Ecatividade ecatividade;

    /** Default constructor. */
    public Ecprtabatividadedet() {
        super();
    }

    /**
     * Access method for codecprtabatividadedet.
     *
     * @return the current value of codecprtabatividadedet
     */
    public int getCodecprtabatividadedet() {
        return codecprtabatividadedet;
    }

    /**
     * Setter method for codecprtabatividadedet.
     *
     * @param aCodecprtabatividadedet the new value for codecprtabatividadedet
     */
    public void setCodecprtabatividadedet(int aCodecprtabatividadedet) {
        codecprtabatividadedet = aCodecprtabatividadedet;
    }

    /**
     * Access method for preco.
     *
     * @return the current value of preco
     */
    public BigDecimal getPreco() {
        return preco;
    }

    /**
     * Setter method for preco.
     *
     * @param aPreco the new value for preco
     */
    public void setPreco(BigDecimal aPreco) {
        preco = aPreco;
    }

    /**
     * Access method for ecprtabatividade.
     *
     * @return the current value of ecprtabatividade
     */
    public Ecprtabatividade getEcprtabatividade() {
        return ecprtabatividade;
    }

    /**
     * Setter method for ecprtabatividade.
     *
     * @param aEcprtabatividade the new value for ecprtabatividade
     */
    public void setEcprtabatividade(Ecprtabatividade aEcprtabatividade) {
        ecprtabatividade = aEcprtabatividade;
    }

    /**
     * Access method for ecatividade.
     *
     * @return the current value of ecatividade
     */
    public Ecatividade getEcatividade() {
        return ecatividade;
    }

    /**
     * Setter method for ecatividade.
     *
     * @param aEcatividade the new value for ecatividade
     */
    public void setEcatividade(Ecatividade aEcatividade) {
        ecatividade = aEcatividade;
    }

    /**
     * Compares the key for this instance with another Ecprtabatividadedet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecprtabatividadedet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecprtabatividadedet)) {
            return false;
        }
        Ecprtabatividadedet that = (Ecprtabatividadedet) other;
        if (this.getCodecprtabatividadedet() != that.getCodecprtabatividadedet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecprtabatividadedet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecprtabatividadedet)) return false;
        return this.equalKeys(other) && ((Ecprtabatividadedet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecprtabatividadedet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecprtabatividadedet |");
        sb.append(" codecprtabatividadedet=").append(getCodecprtabatividadedet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecprtabatividadedet", Integer.valueOf(getCodecprtabatividadedet()));
        return ret;
    }

}
