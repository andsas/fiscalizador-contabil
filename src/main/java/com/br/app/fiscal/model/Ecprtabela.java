package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECPRTABELA")
public class Ecprtabela implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecprtabela";

    @Id
    @Column(name="CODECPRTABELA", unique=true, nullable=false, precision=10)
    private int codecprtabela;
    @Column(name="DESCECPRTABELA", nullable=false, length=250)
    private String descecprtabela;
    @Column(name="DATAVALIDADE", nullable=false)
    private Timestamp datavalidade;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @Column(name="DOCTIPOPRECOFIXO", precision=5)
    private short doctipoprecofixo;
    @OneToMany(mappedBy="ecprtabela")
    private Set<Eccheckstatus> eccheckstatus;
    @OneToMany(mappedBy="ecprtabela")
    private Set<Ecdoclancamento> ecdoclancamento;
    @OneToMany(mappedBy="ecprtabela")
    private Set<Ecprtabeladet> ecprtabeladet;

    /** Default constructor. */
    public Ecprtabela() {
        super();
    }

    /**
     * Access method for codecprtabela.
     *
     * @return the current value of codecprtabela
     */
    public int getCodecprtabela() {
        return codecprtabela;
    }

    /**
     * Setter method for codecprtabela.
     *
     * @param aCodecprtabela the new value for codecprtabela
     */
    public void setCodecprtabela(int aCodecprtabela) {
        codecprtabela = aCodecprtabela;
    }

    /**
     * Access method for descecprtabela.
     *
     * @return the current value of descecprtabela
     */
    public String getDescecprtabela() {
        return descecprtabela;
    }

    /**
     * Setter method for descecprtabela.
     *
     * @param aDescecprtabela the new value for descecprtabela
     */
    public void setDescecprtabela(String aDescecprtabela) {
        descecprtabela = aDescecprtabela;
    }

    /**
     * Access method for datavalidade.
     *
     * @return the current value of datavalidade
     */
    public Timestamp getDatavalidade() {
        return datavalidade;
    }

    /**
     * Setter method for datavalidade.
     *
     * @param aDatavalidade the new value for datavalidade
     */
    public void setDatavalidade(Timestamp aDatavalidade) {
        datavalidade = aDatavalidade;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for doctipoprecofixo.
     *
     * @return the current value of doctipoprecofixo
     */
    public short getDoctipoprecofixo() {
        return doctipoprecofixo;
    }

    /**
     * Setter method for doctipoprecofixo.
     *
     * @param aDoctipoprecofixo the new value for doctipoprecofixo
     */
    public void setDoctipoprecofixo(short aDoctipoprecofixo) {
        doctipoprecofixo = aDoctipoprecofixo;
    }

    /**
     * Access method for eccheckstatus.
     *
     * @return the current value of eccheckstatus
     */
    public Set<Eccheckstatus> getEccheckstatus() {
        return eccheckstatus;
    }

    /**
     * Setter method for eccheckstatus.
     *
     * @param aEccheckstatus the new value for eccheckstatus
     */
    public void setEccheckstatus(Set<Eccheckstatus> aEccheckstatus) {
        eccheckstatus = aEccheckstatus;
    }

    /**
     * Access method for ecdoclancamento.
     *
     * @return the current value of ecdoclancamento
     */
    public Set<Ecdoclancamento> getEcdoclancamento() {
        return ecdoclancamento;
    }

    /**
     * Setter method for ecdoclancamento.
     *
     * @param aEcdoclancamento the new value for ecdoclancamento
     */
    public void setEcdoclancamento(Set<Ecdoclancamento> aEcdoclancamento) {
        ecdoclancamento = aEcdoclancamento;
    }

    /**
     * Access method for ecprtabeladet.
     *
     * @return the current value of ecprtabeladet
     */
    public Set<Ecprtabeladet> getEcprtabeladet() {
        return ecprtabeladet;
    }

    /**
     * Setter method for ecprtabeladet.
     *
     * @param aEcprtabeladet the new value for ecprtabeladet
     */
    public void setEcprtabeladet(Set<Ecprtabeladet> aEcprtabeladet) {
        ecprtabeladet = aEcprtabeladet;
    }

    /**
     * Compares the key for this instance with another Ecprtabela.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecprtabela and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecprtabela)) {
            return false;
        }
        Ecprtabela that = (Ecprtabela) other;
        if (this.getCodecprtabela() != that.getCodecprtabela()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecprtabela.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecprtabela)) return false;
        return this.equalKeys(other) && ((Ecprtabela)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecprtabela();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecprtabela |");
        sb.append(" codecprtabela=").append(getCodecprtabela());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecprtabela", Integer.valueOf(getCodecprtabela()));
        return ret;
    }

}
