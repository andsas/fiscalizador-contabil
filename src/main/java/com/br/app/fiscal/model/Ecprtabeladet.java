package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECPRTABELADET")
public class Ecprtabeladet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecprtabeladet";

    @Id
    @Column(name="CODECPRTABELADET", unique=true, nullable=false, precision=10)
    private int codecprtabeladet;
    @Column(name="FORMULA")
    private String formula;
    @Column(name="PRECO", precision=15, scale=4)
    private BigDecimal preco;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECPRTABELA", nullable=false)
    private Ecprtabela ecprtabela;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECDOCTIPO", nullable=false)
    private Ecdoctipo ecdoctipo;

    /** Default constructor. */
    public Ecprtabeladet() {
        super();
    }

    /**
     * Access method for codecprtabeladet.
     *
     * @return the current value of codecprtabeladet
     */
    public int getCodecprtabeladet() {
        return codecprtabeladet;
    }

    /**
     * Setter method for codecprtabeladet.
     *
     * @param aCodecprtabeladet the new value for codecprtabeladet
     */
    public void setCodecprtabeladet(int aCodecprtabeladet) {
        codecprtabeladet = aCodecprtabeladet;
    }

    /**
     * Access method for formula.
     *
     * @return the current value of formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Setter method for formula.
     *
     * @param aFormula the new value for formula
     */
    public void setFormula(String aFormula) {
        formula = aFormula;
    }

    /**
     * Access method for preco.
     *
     * @return the current value of preco
     */
    public BigDecimal getPreco() {
        return preco;
    }

    /**
     * Setter method for preco.
     *
     * @param aPreco the new value for preco
     */
    public void setPreco(BigDecimal aPreco) {
        preco = aPreco;
    }

    /**
     * Access method for ecprtabela.
     *
     * @return the current value of ecprtabela
     */
    public Ecprtabela getEcprtabela() {
        return ecprtabela;
    }

    /**
     * Setter method for ecprtabela.
     *
     * @param aEcprtabela the new value for ecprtabela
     */
    public void setEcprtabela(Ecprtabela aEcprtabela) {
        ecprtabela = aEcprtabela;
    }

    /**
     * Access method for ecdoctipo.
     *
     * @return the current value of ecdoctipo
     */
    public Ecdoctipo getEcdoctipo() {
        return ecdoctipo;
    }

    /**
     * Setter method for ecdoctipo.
     *
     * @param aEcdoctipo the new value for ecdoctipo
     */
    public void setEcdoctipo(Ecdoctipo aEcdoctipo) {
        ecdoctipo = aEcdoctipo;
    }

    /**
     * Compares the key for this instance with another Ecprtabeladet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecprtabeladet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecprtabeladet)) {
            return false;
        }
        Ecprtabeladet that = (Ecprtabeladet) other;
        if (this.getCodecprtabeladet() != that.getCodecprtabeladet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecprtabeladet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecprtabeladet)) return false;
        return this.equalKeys(other) && ((Ecprtabeladet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecprtabeladet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecprtabeladet |");
        sb.append(" codecprtabeladet=").append(getCodecprtabeladet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecprtabeladet", Integer.valueOf(getCodecprtabeladet()));
        return ret;
    }

}
