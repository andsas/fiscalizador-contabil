package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECSTATUS")
public class Ecstatus implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codecstatus";

    @Id
    @Column(name="CODECSTATUS", unique=true, nullable=false, precision=10)
    private int codecstatus;
    @Column(name="DESCECSTATUS", nullable=false, length=250)
    private String descecstatus;
    @Column(name="NOTUSUARIORESP", precision=5)
    private short notusuarioresp;
    @Column(name="NOTUSUARIOADMIN", precision=5)
    private short notusuarioadmin;
    @Column(name="NOTUSUARIOTIPO", precision=5)
    private short notusuariotipo;
    @Column(name="NOTCONFUSUARIOTIPO", precision=10)
    private int notconfusuariotipo;
    @Column(name="CRIAECDOCLANCAMENTO", precision=5)
    private short criaecdoclancamento;
    @Column(name="NOTCLIENTEEMAIL", precision=5)
    private short notclienteemail;
    @Column(name="NOTCLIENTESMS", precision=5)
    private short notclientesms;
    @Column(name="CORECSTATUS", precision=10)
    private int corecstatus;
    @OneToMany(mappedBy="ecstatus")
    private Set<Eccheckagenda> eccheckagenda;
    @OneToMany(mappedBy="ecstatus")
    private Set<Eccheckstatus> eccheckstatus;

    /** Default constructor. */
    public Ecstatus() {
        super();
    }

    /**
     * Access method for codecstatus.
     *
     * @return the current value of codecstatus
     */
    public int getCodecstatus() {
        return codecstatus;
    }

    /**
     * Setter method for codecstatus.
     *
     * @param aCodecstatus the new value for codecstatus
     */
    public void setCodecstatus(int aCodecstatus) {
        codecstatus = aCodecstatus;
    }

    /**
     * Access method for descecstatus.
     *
     * @return the current value of descecstatus
     */
    public String getDescecstatus() {
        return descecstatus;
    }

    /**
     * Setter method for descecstatus.
     *
     * @param aDescecstatus the new value for descecstatus
     */
    public void setDescecstatus(String aDescecstatus) {
        descecstatus = aDescecstatus;
    }

    /**
     * Access method for notusuarioresp.
     *
     * @return the current value of notusuarioresp
     */
    public short getNotusuarioresp() {
        return notusuarioresp;
    }

    /**
     * Setter method for notusuarioresp.
     *
     * @param aNotusuarioresp the new value for notusuarioresp
     */
    public void setNotusuarioresp(short aNotusuarioresp) {
        notusuarioresp = aNotusuarioresp;
    }

    /**
     * Access method for notusuarioadmin.
     *
     * @return the current value of notusuarioadmin
     */
    public short getNotusuarioadmin() {
        return notusuarioadmin;
    }

    /**
     * Setter method for notusuarioadmin.
     *
     * @param aNotusuarioadmin the new value for notusuarioadmin
     */
    public void setNotusuarioadmin(short aNotusuarioadmin) {
        notusuarioadmin = aNotusuarioadmin;
    }

    /**
     * Access method for notusuariotipo.
     *
     * @return the current value of notusuariotipo
     */
    public short getNotusuariotipo() {
        return notusuariotipo;
    }

    /**
     * Setter method for notusuariotipo.
     *
     * @param aNotusuariotipo the new value for notusuariotipo
     */
    public void setNotusuariotipo(short aNotusuariotipo) {
        notusuariotipo = aNotusuariotipo;
    }

    /**
     * Access method for notconfusuariotipo.
     *
     * @return the current value of notconfusuariotipo
     */
    public int getNotconfusuariotipo() {
        return notconfusuariotipo;
    }

    /**
     * Setter method for notconfusuariotipo.
     *
     * @param aNotconfusuariotipo the new value for notconfusuariotipo
     */
    public void setNotconfusuariotipo(int aNotconfusuariotipo) {
        notconfusuariotipo = aNotconfusuariotipo;
    }

    /**
     * Access method for criaecdoclancamento.
     *
     * @return the current value of criaecdoclancamento
     */
    public short getCriaecdoclancamento() {
        return criaecdoclancamento;
    }

    /**
     * Setter method for criaecdoclancamento.
     *
     * @param aCriaecdoclancamento the new value for criaecdoclancamento
     */
    public void setCriaecdoclancamento(short aCriaecdoclancamento) {
        criaecdoclancamento = aCriaecdoclancamento;
    }

    /**
     * Access method for notclienteemail.
     *
     * @return the current value of notclienteemail
     */
    public short getNotclienteemail() {
        return notclienteemail;
    }

    /**
     * Setter method for notclienteemail.
     *
     * @param aNotclienteemail the new value for notclienteemail
     */
    public void setNotclienteemail(short aNotclienteemail) {
        notclienteemail = aNotclienteemail;
    }

    /**
     * Access method for notclientesms.
     *
     * @return the current value of notclientesms
     */
    public short getNotclientesms() {
        return notclientesms;
    }

    /**
     * Setter method for notclientesms.
     *
     * @param aNotclientesms the new value for notclientesms
     */
    public void setNotclientesms(short aNotclientesms) {
        notclientesms = aNotclientesms;
    }

    /**
     * Access method for corecstatus.
     *
     * @return the current value of corecstatus
     */
    public int getCorecstatus() {
        return corecstatus;
    }

    /**
     * Setter method for corecstatus.
     *
     * @param aCorecstatus the new value for corecstatus
     */
    public void setCorecstatus(int aCorecstatus) {
        corecstatus = aCorecstatus;
    }

    /**
     * Access method for eccheckagenda.
     *
     * @return the current value of eccheckagenda
     */
    public Set<Eccheckagenda> getEccheckagenda() {
        return eccheckagenda;
    }

    /**
     * Setter method for eccheckagenda.
     *
     * @param aEccheckagenda the new value for eccheckagenda
     */
    public void setEccheckagenda(Set<Eccheckagenda> aEccheckagenda) {
        eccheckagenda = aEccheckagenda;
    }

    /**
     * Access method for eccheckstatus.
     *
     * @return the current value of eccheckstatus
     */
    public Set<Eccheckstatus> getEccheckstatus() {
        return eccheckstatus;
    }

    /**
     * Setter method for eccheckstatus.
     *
     * @param aEccheckstatus the new value for eccheckstatus
     */
    public void setEccheckstatus(Set<Eccheckstatus> aEccheckstatus) {
        eccheckstatus = aEccheckstatus;
    }

    /**
     * Compares the key for this instance with another Ecstatus.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ecstatus and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ecstatus)) {
            return false;
        }
        Ecstatus that = (Ecstatus) other;
        if (this.getCodecstatus() != that.getCodecstatus()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ecstatus.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ecstatus)) return false;
        return this.equalKeys(other) && ((Ecstatus)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodecstatus();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ecstatus |");
        sb.append(" codecstatus=").append(getCodecstatus());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codecstatus", Integer.valueOf(getCodecstatus()));
        return ret;
    }

}
