package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="ECTRIBUTACAO")
public class Ectributacao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codectributacao";

    @Id
    @Column(name="CODECTRIBUTACAO", unique=true, nullable=false, precision=10)
    private int codectributacao;
    @Column(name="DESCECTRIBUTACAO", nullable=false, length=250)
    private String descectributacao;
    @Column(name="APURACAO", precision=5)
    private short apuracao;
    @Column(name="VARIACAO", nullable=false, length=40)
    private String variacao;
    @Column(name="PERIODOAPURACAO")
    private Timestamp periodoapuracao;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODECTIPOTRIBUTACAO", nullable=false)
    private Ectributacaotipo ectributacaotipo;

    /** Default constructor. */
    public Ectributacao() {
        super();
    }

    /**
     * Access method for codectributacao.
     *
     * @return the current value of codectributacao
     */
    public int getCodectributacao() {
        return codectributacao;
    }

    /**
     * Setter method for codectributacao.
     *
     * @param aCodectributacao the new value for codectributacao
     */
    public void setCodectributacao(int aCodectributacao) {
        codectributacao = aCodectributacao;
    }

    /**
     * Access method for descectributacao.
     *
     * @return the current value of descectributacao
     */
    public String getDescectributacao() {
        return descectributacao;
    }

    /**
     * Setter method for descectributacao.
     *
     * @param aDescectributacao the new value for descectributacao
     */
    public void setDescectributacao(String aDescectributacao) {
        descectributacao = aDescectributacao;
    }

    /**
     * Access method for apuracao.
     *
     * @return the current value of apuracao
     */
    public short getApuracao() {
        return apuracao;
    }

    /**
     * Setter method for apuracao.
     *
     * @param aApuracao the new value for apuracao
     */
    public void setApuracao(short aApuracao) {
        apuracao = aApuracao;
    }

    /**
     * Access method for variacao.
     *
     * @return the current value of variacao
     */
    public String getVariacao() {
        return variacao;
    }

    /**
     * Setter method for variacao.
     *
     * @param aVariacao the new value for variacao
     */
    public void setVariacao(String aVariacao) {
        variacao = aVariacao;
    }

    /**
     * Access method for periodoapuracao.
     *
     * @return the current value of periodoapuracao
     */
    public Timestamp getPeriodoapuracao() {
        return periodoapuracao;
    }

    /**
     * Setter method for periodoapuracao.
     *
     * @param aPeriodoapuracao the new value for periodoapuracao
     */
    public void setPeriodoapuracao(Timestamp aPeriodoapuracao) {
        periodoapuracao = aPeriodoapuracao;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for ectributacaotipo.
     *
     * @return the current value of ectributacaotipo
     */
    public Ectributacaotipo getEctributacaotipo() {
        return ectributacaotipo;
    }

    /**
     * Setter method for ectributacaotipo.
     *
     * @param aEctributacaotipo the new value for ectributacaotipo
     */
    public void setEctributacaotipo(Ectributacaotipo aEctributacaotipo) {
        ectributacaotipo = aEctributacaotipo;
    }

    /**
     * Compares the key for this instance with another Ectributacao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ectributacao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ectributacao)) {
            return false;
        }
        Ectributacao that = (Ectributacao) other;
        if (this.getCodectributacao() != that.getCodectributacao()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ectributacao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ectributacao)) return false;
        return this.equalKeys(other) && ((Ectributacao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodectributacao();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ectributacao |");
        sb.append(" codectributacao=").append(getCodectributacao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codectributacao", Integer.valueOf(getCodectributacao()));
        return ret;
    }

}
