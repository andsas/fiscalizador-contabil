package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="ECTRIBUTACAOTIPO")
public class Ectributacaotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codectributacaotipo";

    @Id
    @Column(name="CODECTRIBUTACAOTIPO", unique=true, nullable=false, precision=10)
    private int codectributacaotipo;
    @Column(name="DESCECTRIBUTACAOTIPO", nullable=false, length=250)
    private String descectributacaotipo;
    @Column(name="APURACAOPADRAO", precision=5)
    private short apuracaopadrao;
    @OneToMany(mappedBy="ectributacaotipo")
    private Set<Ectributacao> ectributacao;

    /** Default constructor. */
    public Ectributacaotipo() {
        super();
    }

    /**
     * Access method for codectributacaotipo.
     *
     * @return the current value of codectributacaotipo
     */
    public int getCodectributacaotipo() {
        return codectributacaotipo;
    }

    /**
     * Setter method for codectributacaotipo.
     *
     * @param aCodectributacaotipo the new value for codectributacaotipo
     */
    public void setCodectributacaotipo(int aCodectributacaotipo) {
        codectributacaotipo = aCodectributacaotipo;
    }

    /**
     * Access method for descectributacaotipo.
     *
     * @return the current value of descectributacaotipo
     */
    public String getDescectributacaotipo() {
        return descectributacaotipo;
    }

    /**
     * Setter method for descectributacaotipo.
     *
     * @param aDescectributacaotipo the new value for descectributacaotipo
     */
    public void setDescectributacaotipo(String aDescectributacaotipo) {
        descectributacaotipo = aDescectributacaotipo;
    }

    /**
     * Access method for apuracaopadrao.
     *
     * @return the current value of apuracaopadrao
     */
    public short getApuracaopadrao() {
        return apuracaopadrao;
    }

    /**
     * Setter method for apuracaopadrao.
     *
     * @param aApuracaopadrao the new value for apuracaopadrao
     */
    public void setApuracaopadrao(short aApuracaopadrao) {
        apuracaopadrao = aApuracaopadrao;
    }

    /**
     * Access method for ectributacao.
     *
     * @return the current value of ectributacao
     */
    public Set<Ectributacao> getEctributacao() {
        return ectributacao;
    }

    /**
     * Setter method for ectributacao.
     *
     * @param aEctributacao the new value for ectributacao
     */
    public void setEctributacao(Set<Ectributacao> aEctributacao) {
        ectributacao = aEctributacao;
    }

    /**
     * Compares the key for this instance with another Ectributacaotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Ectributacaotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Ectributacaotipo)) {
            return false;
        }
        Ectributacaotipo that = (Ectributacaotipo) other;
        if (this.getCodectributacaotipo() != that.getCodectributacaotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Ectributacaotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Ectributacaotipo)) return false;
        return this.equalKeys(other) && ((Ectributacaotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodectributacaotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Ectributacaotipo |");
        sb.append(" codectributacaotipo=").append(getCodectributacaotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codectributacaotipo", Integer.valueOf(getCodectributacaotipo()));
        return ret;
    }

}
