package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINAGENCIA")
public class Finagencia implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinagencia";

    @Id
    @Column(name="CODFINAGENCIA", unique=true, nullable=false, length=20)
    private String codfinagencia;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFINBANCO", nullable=false)
    private Finbanco finbanco;
    @ManyToOne
    @JoinColumn(name="CODAGAGENCIA")
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODAGRESPONSAVEL")
    private Agagente agagente2;
    @OneToMany(mappedBy="finagencia")
    private Set<Fincheque> fincheque;
    @OneToMany(mappedBy="finagencia")
    private Set<Finconta> finconta;
    @OneToMany(mappedBy="finagencia")
    private Set<Optransacaocobr> optransacaocobr;
    @OneToMany(mappedBy="finagencia2")
    private Set<Optransacaocobr> optransacaocobr2;

    /** Default constructor. */
    public Finagencia() {
        super();
    }

    /**
     * Access method for codfinagencia.
     *
     * @return the current value of codfinagencia
     */
    public String getCodfinagencia() {
        return codfinagencia;
    }

    /**
     * Setter method for codfinagencia.
     *
     * @param aCodfinagencia the new value for codfinagencia
     */
    public void setCodfinagencia(String aCodfinagencia) {
        codfinagencia = aCodfinagencia;
    }

    /**
     * Access method for finbanco.
     *
     * @return the current value of finbanco
     */
    public Finbanco getFinbanco() {
        return finbanco;
    }

    /**
     * Setter method for finbanco.
     *
     * @param aFinbanco the new value for finbanco
     */
    public void setFinbanco(Finbanco aFinbanco) {
        finbanco = aFinbanco;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Agagente getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Agagente aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for fincheque.
     *
     * @return the current value of fincheque
     */
    public Set<Fincheque> getFincheque() {
        return fincheque;
    }

    /**
     * Setter method for fincheque.
     *
     * @param aFincheque the new value for fincheque
     */
    public void setFincheque(Set<Fincheque> aFincheque) {
        fincheque = aFincheque;
    }

    /**
     * Access method for finconta.
     *
     * @return the current value of finconta
     */
    public Set<Finconta> getFinconta() {
        return finconta;
    }

    /**
     * Setter method for finconta.
     *
     * @param aFinconta the new value for finconta
     */
    public void setFinconta(Set<Finconta> aFinconta) {
        finconta = aFinconta;
    }

    /**
     * Access method for optransacaocobr.
     *
     * @return the current value of optransacaocobr
     */
    public Set<Optransacaocobr> getOptransacaocobr() {
        return optransacaocobr;
    }

    /**
     * Setter method for optransacaocobr.
     *
     * @param aOptransacaocobr the new value for optransacaocobr
     */
    public void setOptransacaocobr(Set<Optransacaocobr> aOptransacaocobr) {
        optransacaocobr = aOptransacaocobr;
    }

    /**
     * Access method for optransacaocobr2.
     *
     * @return the current value of optransacaocobr2
     */
    public Set<Optransacaocobr> getOptransacaocobr2() {
        return optransacaocobr2;
    }

    /**
     * Setter method for optransacaocobr2.
     *
     * @param aOptransacaocobr2 the new value for optransacaocobr2
     */
    public void setOptransacaocobr2(Set<Optransacaocobr> aOptransacaocobr2) {
        optransacaocobr2 = aOptransacaocobr2;
    }

    /**
     * Compares the key for this instance with another Finagencia.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finagencia and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finagencia)) {
            return false;
        }
        Finagencia that = (Finagencia) other;
        Object myCodfinagencia = this.getCodfinagencia();
        Object yourCodfinagencia = that.getCodfinagencia();
        if (myCodfinagencia==null ? yourCodfinagencia!=null : !myCodfinagencia.equals(yourCodfinagencia)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finagencia.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finagencia)) return false;
        return this.equalKeys(other) && ((Finagencia)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfinagencia() == null) {
            i = 0;
        } else {
            i = getCodfinagencia().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finagencia |");
        sb.append(" codfinagencia=").append(getCodfinagencia());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinagencia", getCodfinagencia());
        return ret;
    }

}
