package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINBAIXA")
public class Finbaixa implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinbaixa";

    @Id
    @Column(name="CODFINBAIXA", unique=true, nullable=false, precision=10)
    private int codfinbaixa;
    @Column(name="CODFINAGENCIA", length=20)
    private String codfinagencia;
    @Column(name="VALORBAIXA", precision=15, scale=4)
    private BigDecimal valorbaixa;
    @Column(name="VALORCONC", precision=15, scale=4)
    private BigDecimal valorconc;
    @Column(name="VALORDESCONTO", precision=15, scale=4)
    private BigDecimal valordesconto;
    @Column(name="VALORACRESCIMO", precision=15, scale=4)
    private BigDecimal valoracrescimo;
    @Column(name="PERCJURO", length=15)
    private double percjuro;
    @Column(name="PERCDESCONTO", length=15)
    private double percdesconto;
    @Column(name="PERCACRESCIMO", length=15)
    private double percacrescimo;
    @Column(name="PERCMULTA", length=15)
    private double percmulta;
    @Column(name="DATABAIXA")
    private Timestamp databaixa;
    @Column(name="CHQBANCO", length=20)
    private String chqbanco;
    @Column(name="CHQAGENCIA", length=20)
    private String chqagencia;
    @Column(name="CHQNUM", precision=10)
    private int chqnum;
    @Column(name="CHQVALOR", precision=15, scale=4)
    private BigDecimal chqvalor;
    @Column(name="CHQDATA")
    private Date chqdata;
    @Column(name="CHQBOMPARA")
    private Date chqbompara;
    @Column(name="CHQNOME", length=250)
    private String chqnome;
    @Column(name="CHQDOCUMENTO", length=20)
    private String chqdocumento;
    @Column(name="CHQDI", length=60)
    private String chqdi;
    @Column(name="CHQCLIENTEDESDE")
    private Date chqclientedesde;
    @Column(name="CHQFAVORECIDO", length=250)
    private String chqfavorecido;
    @Column(name="CHQFOTO")
    private byte[] chqfoto;
    @Column(name="CARBANDEIRA", precision=5)
    private short carbandeira;
    @Column(name="CARNOME", length=250)
    private String carnome;
    @Column(name="CARNUMERO", length=40)
    private String carnumero;
    @Column(name="CARVALIDADE", length=4)
    private String carvalidade;
    @Column(name="CARSEGURANCAO", precision=10)
    private int carsegurancao;
    @Column(name="STATUS", precision=5)
    private short status;
    @Column(name="INFADIC")
    private String infadic;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFINLANCAMENTO", nullable=false)
    private Finlancamento finlancamento;
    @ManyToOne
    @JoinColumn(name="CODORCPLANOCONTA")
    private Orcplanoconta orcplanoconta;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCOBRFORMA", nullable=false)
    private Cobrforma cobrforma;
    @ManyToOne
    @JoinColumn(name="CODFINBANCO")
    private Finbanco finbanco;
    @ManyToOne
    @JoinColumn(name="CODFINCONTA")
    private Finconta finconta;
    @ManyToOne
    @JoinColumn(name="CTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta2;
    @ManyToOne
    @JoinColumn(name="CODFINPLANOCONTA")
    private Finplanoconta finplanoconta;
    @ManyToOne
    @JoinColumn(name="CODFINFLUXOPLANOCONTA")
    private Finfluxoplanoconta finfluxoplanoconta;
    @OneToMany(mappedBy="finbaixa")
    private Set<Fincheque> fincheque;

    /** Default constructor. */
    public Finbaixa() {
        super();
    }

    /**
     * Access method for codfinbaixa.
     *
     * @return the current value of codfinbaixa
     */
    public int getCodfinbaixa() {
        return codfinbaixa;
    }

    /**
     * Setter method for codfinbaixa.
     *
     * @param aCodfinbaixa the new value for codfinbaixa
     */
    public void setCodfinbaixa(int aCodfinbaixa) {
        codfinbaixa = aCodfinbaixa;
    }

    /**
     * Access method for codfinagencia.
     *
     * @return the current value of codfinagencia
     */
    public String getCodfinagencia() {
        return codfinagencia;
    }

    /**
     * Setter method for codfinagencia.
     *
     * @param aCodfinagencia the new value for codfinagencia
     */
    public void setCodfinagencia(String aCodfinagencia) {
        codfinagencia = aCodfinagencia;
    }

    /**
     * Access method for valorbaixa.
     *
     * @return the current value of valorbaixa
     */
    public BigDecimal getValorbaixa() {
        return valorbaixa;
    }

    /**
     * Setter method for valorbaixa.
     *
     * @param aValorbaixa the new value for valorbaixa
     */
    public void setValorbaixa(BigDecimal aValorbaixa) {
        valorbaixa = aValorbaixa;
    }

    /**
     * Access method for valorconc.
     *
     * @return the current value of valorconc
     */
    public BigDecimal getValorconc() {
        return valorconc;
    }

    /**
     * Setter method for valorconc.
     *
     * @param aValorconc the new value for valorconc
     */
    public void setValorconc(BigDecimal aValorconc) {
        valorconc = aValorconc;
    }

    /**
     * Access method for valordesconto.
     *
     * @return the current value of valordesconto
     */
    public BigDecimal getValordesconto() {
        return valordesconto;
    }

    /**
     * Setter method for valordesconto.
     *
     * @param aValordesconto the new value for valordesconto
     */
    public void setValordesconto(BigDecimal aValordesconto) {
        valordesconto = aValordesconto;
    }

    /**
     * Access method for valoracrescimo.
     *
     * @return the current value of valoracrescimo
     */
    public BigDecimal getValoracrescimo() {
        return valoracrescimo;
    }

    /**
     * Setter method for valoracrescimo.
     *
     * @param aValoracrescimo the new value for valoracrescimo
     */
    public void setValoracrescimo(BigDecimal aValoracrescimo) {
        valoracrescimo = aValoracrescimo;
    }

    /**
     * Access method for percjuro.
     *
     * @return the current value of percjuro
     */
    public double getPercjuro() {
        return percjuro;
    }

    /**
     * Setter method for percjuro.
     *
     * @param aPercjuro the new value for percjuro
     */
    public void setPercjuro(double aPercjuro) {
        percjuro = aPercjuro;
    }

    /**
     * Access method for percdesconto.
     *
     * @return the current value of percdesconto
     */
    public double getPercdesconto() {
        return percdesconto;
    }

    /**
     * Setter method for percdesconto.
     *
     * @param aPercdesconto the new value for percdesconto
     */
    public void setPercdesconto(double aPercdesconto) {
        percdesconto = aPercdesconto;
    }

    /**
     * Access method for percacrescimo.
     *
     * @return the current value of percacrescimo
     */
    public double getPercacrescimo() {
        return percacrescimo;
    }

    /**
     * Setter method for percacrescimo.
     *
     * @param aPercacrescimo the new value for percacrescimo
     */
    public void setPercacrescimo(double aPercacrescimo) {
        percacrescimo = aPercacrescimo;
    }

    /**
     * Access method for percmulta.
     *
     * @return the current value of percmulta
     */
    public double getPercmulta() {
        return percmulta;
    }

    /**
     * Setter method for percmulta.
     *
     * @param aPercmulta the new value for percmulta
     */
    public void setPercmulta(double aPercmulta) {
        percmulta = aPercmulta;
    }

    /**
     * Access method for databaixa.
     *
     * @return the current value of databaixa
     */
    public Timestamp getDatabaixa() {
        return databaixa;
    }

    /**
     * Setter method for databaixa.
     *
     * @param aDatabaixa the new value for databaixa
     */
    public void setDatabaixa(Timestamp aDatabaixa) {
        databaixa = aDatabaixa;
    }

    /**
     * Access method for chqbanco.
     *
     * @return the current value of chqbanco
     */
    public String getChqbanco() {
        return chqbanco;
    }

    /**
     * Setter method for chqbanco.
     *
     * @param aChqbanco the new value for chqbanco
     */
    public void setChqbanco(String aChqbanco) {
        chqbanco = aChqbanco;
    }

    /**
     * Access method for chqagencia.
     *
     * @return the current value of chqagencia
     */
    public String getChqagencia() {
        return chqagencia;
    }

    /**
     * Setter method for chqagencia.
     *
     * @param aChqagencia the new value for chqagencia
     */
    public void setChqagencia(String aChqagencia) {
        chqagencia = aChqagencia;
    }

    /**
     * Access method for chqnum.
     *
     * @return the current value of chqnum
     */
    public int getChqnum() {
        return chqnum;
    }

    /**
     * Setter method for chqnum.
     *
     * @param aChqnum the new value for chqnum
     */
    public void setChqnum(int aChqnum) {
        chqnum = aChqnum;
    }

    /**
     * Access method for chqvalor.
     *
     * @return the current value of chqvalor
     */
    public BigDecimal getChqvalor() {
        return chqvalor;
    }

    /**
     * Setter method for chqvalor.
     *
     * @param aChqvalor the new value for chqvalor
     */
    public void setChqvalor(BigDecimal aChqvalor) {
        chqvalor = aChqvalor;
    }

    /**
     * Access method for chqdata.
     *
     * @return the current value of chqdata
     */
    public Date getChqdata() {
        return chqdata;
    }

    /**
     * Setter method for chqdata.
     *
     * @param aChqdata the new value for chqdata
     */
    public void setChqdata(Date aChqdata) {
        chqdata = aChqdata;
    }

    /**
     * Access method for chqbompara.
     *
     * @return the current value of chqbompara
     */
    public Date getChqbompara() {
        return chqbompara;
    }

    /**
     * Setter method for chqbompara.
     *
     * @param aChqbompara the new value for chqbompara
     */
    public void setChqbompara(Date aChqbompara) {
        chqbompara = aChqbompara;
    }

    /**
     * Access method for chqnome.
     *
     * @return the current value of chqnome
     */
    public String getChqnome() {
        return chqnome;
    }

    /**
     * Setter method for chqnome.
     *
     * @param aChqnome the new value for chqnome
     */
    public void setChqnome(String aChqnome) {
        chqnome = aChqnome;
    }

    /**
     * Access method for chqdocumento.
     *
     * @return the current value of chqdocumento
     */
    public String getChqdocumento() {
        return chqdocumento;
    }

    /**
     * Setter method for chqdocumento.
     *
     * @param aChqdocumento the new value for chqdocumento
     */
    public void setChqdocumento(String aChqdocumento) {
        chqdocumento = aChqdocumento;
    }

    /**
     * Access method for chqdi.
     *
     * @return the current value of chqdi
     */
    public String getChqdi() {
        return chqdi;
    }

    /**
     * Setter method for chqdi.
     *
     * @param aChqdi the new value for chqdi
     */
    public void setChqdi(String aChqdi) {
        chqdi = aChqdi;
    }

    /**
     * Access method for chqclientedesde.
     *
     * @return the current value of chqclientedesde
     */
    public Date getChqclientedesde() {
        return chqclientedesde;
    }

    /**
     * Setter method for chqclientedesde.
     *
     * @param aChqclientedesde the new value for chqclientedesde
     */
    public void setChqclientedesde(Date aChqclientedesde) {
        chqclientedesde = aChqclientedesde;
    }

    /**
     * Access method for chqfavorecido.
     *
     * @return the current value of chqfavorecido
     */
    public String getChqfavorecido() {
        return chqfavorecido;
    }

    /**
     * Setter method for chqfavorecido.
     *
     * @param aChqfavorecido the new value for chqfavorecido
     */
    public void setChqfavorecido(String aChqfavorecido) {
        chqfavorecido = aChqfavorecido;
    }

    /**
     * Access method for chqfoto.
     *
     * @return the current value of chqfoto
     */
    public byte[] getChqfoto() {
        return chqfoto;
    }

    /**
     * Setter method for chqfoto.
     *
     * @param aChqfoto the new value for chqfoto
     */
    public void setChqfoto(byte[] aChqfoto) {
        chqfoto = aChqfoto;
    }

    /**
     * Access method for carbandeira.
     *
     * @return the current value of carbandeira
     */
    public short getCarbandeira() {
        return carbandeira;
    }

    /**
     * Setter method for carbandeira.
     *
     * @param aCarbandeira the new value for carbandeira
     */
    public void setCarbandeira(short aCarbandeira) {
        carbandeira = aCarbandeira;
    }

    /**
     * Access method for carnome.
     *
     * @return the current value of carnome
     */
    public String getCarnome() {
        return carnome;
    }

    /**
     * Setter method for carnome.
     *
     * @param aCarnome the new value for carnome
     */
    public void setCarnome(String aCarnome) {
        carnome = aCarnome;
    }

    /**
     * Access method for carnumero.
     *
     * @return the current value of carnumero
     */
    public String getCarnumero() {
        return carnumero;
    }

    /**
     * Setter method for carnumero.
     *
     * @param aCarnumero the new value for carnumero
     */
    public void setCarnumero(String aCarnumero) {
        carnumero = aCarnumero;
    }

    /**
     * Access method for carvalidade.
     *
     * @return the current value of carvalidade
     */
    public String getCarvalidade() {
        return carvalidade;
    }

    /**
     * Setter method for carvalidade.
     *
     * @param aCarvalidade the new value for carvalidade
     */
    public void setCarvalidade(String aCarvalidade) {
        carvalidade = aCarvalidade;
    }

    /**
     * Access method for carsegurancao.
     *
     * @return the current value of carsegurancao
     */
    public int getCarsegurancao() {
        return carsegurancao;
    }

    /**
     * Setter method for carsegurancao.
     *
     * @param aCarsegurancao the new value for carsegurancao
     */
    public void setCarsegurancao(int aCarsegurancao) {
        carsegurancao = aCarsegurancao;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public short getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(short aStatus) {
        status = aStatus;
    }

    /**
     * Access method for infadic.
     *
     * @return the current value of infadic
     */
    public String getInfadic() {
        return infadic;
    }

    /**
     * Setter method for infadic.
     *
     * @param aInfadic the new value for infadic
     */
    public void setInfadic(String aInfadic) {
        infadic = aInfadic;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Finlancamento getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Finlancamento aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for orcplanoconta.
     *
     * @return the current value of orcplanoconta
     */
    public Orcplanoconta getOrcplanoconta() {
        return orcplanoconta;
    }

    /**
     * Setter method for orcplanoconta.
     *
     * @param aOrcplanoconta the new value for orcplanoconta
     */
    public void setOrcplanoconta(Orcplanoconta aOrcplanoconta) {
        orcplanoconta = aOrcplanoconta;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Cobrforma getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Cobrforma aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Access method for finbanco.
     *
     * @return the current value of finbanco
     */
    public Finbanco getFinbanco() {
        return finbanco;
    }

    /**
     * Setter method for finbanco.
     *
     * @param aFinbanco the new value for finbanco
     */
    public void setFinbanco(Finbanco aFinbanco) {
        finbanco = aFinbanco;
    }

    /**
     * Access method for finconta.
     *
     * @return the current value of finconta
     */
    public Finconta getFinconta() {
        return finconta;
    }

    /**
     * Setter method for finconta.
     *
     * @param aFinconta the new value for finconta
     */
    public void setFinconta(Finconta aFinconta) {
        finconta = aFinconta;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for finplanoconta.
     *
     * @return the current value of finplanoconta
     */
    public Finplanoconta getFinplanoconta() {
        return finplanoconta;
    }

    /**
     * Setter method for finplanoconta.
     *
     * @param aFinplanoconta the new value for finplanoconta
     */
    public void setFinplanoconta(Finplanoconta aFinplanoconta) {
        finplanoconta = aFinplanoconta;
    }

    /**
     * Access method for finfluxoplanoconta.
     *
     * @return the current value of finfluxoplanoconta
     */
    public Finfluxoplanoconta getFinfluxoplanoconta() {
        return finfluxoplanoconta;
    }

    /**
     * Setter method for finfluxoplanoconta.
     *
     * @param aFinfluxoplanoconta the new value for finfluxoplanoconta
     */
    public void setFinfluxoplanoconta(Finfluxoplanoconta aFinfluxoplanoconta) {
        finfluxoplanoconta = aFinfluxoplanoconta;
    }

    /**
     * Access method for fincheque.
     *
     * @return the current value of fincheque
     */
    public Set<Fincheque> getFincheque() {
        return fincheque;
    }

    /**
     * Setter method for fincheque.
     *
     * @param aFincheque the new value for fincheque
     */
    public void setFincheque(Set<Fincheque> aFincheque) {
        fincheque = aFincheque;
    }

    /**
     * Compares the key for this instance with another Finbaixa.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finbaixa and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finbaixa)) {
            return false;
        }
        Finbaixa that = (Finbaixa) other;
        if (this.getCodfinbaixa() != that.getCodfinbaixa()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finbaixa.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finbaixa)) return false;
        return this.equalKeys(other) && ((Finbaixa)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfinbaixa();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finbaixa |");
        sb.append(" codfinbaixa=").append(getCodfinbaixa());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinbaixa", Integer.valueOf(getCodfinbaixa()));
        return ret;
    }

}
