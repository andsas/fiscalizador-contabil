package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FINBANCO")
public class Finbanco implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinbanco";

    @Id
    @Column(name="CODFINBANCO", unique=true, nullable=false, length=20)
    private String codfinbanco;
    @Column(name="NOMEFINBANCO", nullable=false, length=250)
    private String nomefinbanco;
    @OneToMany(mappedBy="finbanco")
    private Set<Finagencia> finagencia;
    @OneToMany(mappedBy="finbanco")
    private Set<Finbaixa> finbaixa;
    @OneToMany(mappedBy="finbanco")
    private Set<Fincheque> fincheque;
    @OneToMany(mappedBy="finbanco")
    private Set<Finconta> finconta;
    @OneToMany(mappedBy="finbanco")
    private Set<Optransacaocobr> optransacaocobr;
    @OneToMany(mappedBy="finbanco2")
    private Set<Optransacaocobr> optransacaocobr2;

    /** Default constructor. */
    public Finbanco() {
        super();
    }

    /**
     * Access method for codfinbanco.
     *
     * @return the current value of codfinbanco
     */
    public String getCodfinbanco() {
        return codfinbanco;
    }

    /**
     * Setter method for codfinbanco.
     *
     * @param aCodfinbanco the new value for codfinbanco
     */
    public void setCodfinbanco(String aCodfinbanco) {
        codfinbanco = aCodfinbanco;
    }

    /**
     * Access method for nomefinbanco.
     *
     * @return the current value of nomefinbanco
     */
    public String getNomefinbanco() {
        return nomefinbanco;
    }

    /**
     * Setter method for nomefinbanco.
     *
     * @param aNomefinbanco the new value for nomefinbanco
     */
    public void setNomefinbanco(String aNomefinbanco) {
        nomefinbanco = aNomefinbanco;
    }

    /**
     * Access method for finagencia.
     *
     * @return the current value of finagencia
     */
    public Set<Finagencia> getFinagencia() {
        return finagencia;
    }

    /**
     * Setter method for finagencia.
     *
     * @param aFinagencia the new value for finagencia
     */
    public void setFinagencia(Set<Finagencia> aFinagencia) {
        finagencia = aFinagencia;
    }

    /**
     * Access method for finbaixa.
     *
     * @return the current value of finbaixa
     */
    public Set<Finbaixa> getFinbaixa() {
        return finbaixa;
    }

    /**
     * Setter method for finbaixa.
     *
     * @param aFinbaixa the new value for finbaixa
     */
    public void setFinbaixa(Set<Finbaixa> aFinbaixa) {
        finbaixa = aFinbaixa;
    }

    /**
     * Access method for fincheque.
     *
     * @return the current value of fincheque
     */
    public Set<Fincheque> getFincheque() {
        return fincheque;
    }

    /**
     * Setter method for fincheque.
     *
     * @param aFincheque the new value for fincheque
     */
    public void setFincheque(Set<Fincheque> aFincheque) {
        fincheque = aFincheque;
    }

    /**
     * Access method for finconta.
     *
     * @return the current value of finconta
     */
    public Set<Finconta> getFinconta() {
        return finconta;
    }

    /**
     * Setter method for finconta.
     *
     * @param aFinconta the new value for finconta
     */
    public void setFinconta(Set<Finconta> aFinconta) {
        finconta = aFinconta;
    }

    /**
     * Access method for optransacaocobr.
     *
     * @return the current value of optransacaocobr
     */
    public Set<Optransacaocobr> getOptransacaocobr() {
        return optransacaocobr;
    }

    /**
     * Setter method for optransacaocobr.
     *
     * @param aOptransacaocobr the new value for optransacaocobr
     */
    public void setOptransacaocobr(Set<Optransacaocobr> aOptransacaocobr) {
        optransacaocobr = aOptransacaocobr;
    }

    /**
     * Access method for optransacaocobr2.
     *
     * @return the current value of optransacaocobr2
     */
    public Set<Optransacaocobr> getOptransacaocobr2() {
        return optransacaocobr2;
    }

    /**
     * Setter method for optransacaocobr2.
     *
     * @param aOptransacaocobr2 the new value for optransacaocobr2
     */
    public void setOptransacaocobr2(Set<Optransacaocobr> aOptransacaocobr2) {
        optransacaocobr2 = aOptransacaocobr2;
    }

    /**
     * Compares the key for this instance with another Finbanco.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finbanco and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finbanco)) {
            return false;
        }
        Finbanco that = (Finbanco) other;
        Object myCodfinbanco = this.getCodfinbanco();
        Object yourCodfinbanco = that.getCodfinbanco();
        if (myCodfinbanco==null ? yourCodfinbanco!=null : !myCodfinbanco.equals(yourCodfinbanco)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finbanco.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finbanco)) return false;
        return this.equalKeys(other) && ((Finbanco)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfinbanco() == null) {
            i = 0;
        } else {
            i = getCodfinbanco().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finbanco |");
        sb.append(" codfinbanco=").append(getCodfinbanco());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinbanco", getCodfinbanco());
        return ret;
    }

}
