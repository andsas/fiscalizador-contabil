package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINCAIXA")
public class Fincaixa implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfincaixa";

    @Id
    @Column(name="CODFINCAIXA", unique=true, nullable=false, precision=10)
    private int codfincaixa;
    @Column(name="DESCFINCAIXA", length=250)
    private String descfincaixa;
    @Column(name="DATACAIXAABERTURA")
    private Date datacaixaabertura;
    @Column(name="DATACAIXAFECHAMENTO")
    private Date datacaixafechamento;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="CTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta2;
    @ManyToOne
    @JoinColumn(name="FINCONTACREDITO")
    private Finplanoconta finplanoconta;
    @ManyToOne
    @JoinColumn(name="FINCONTADEBITO")
    private Finplanoconta finplanoconta2;
    @ManyToOne
    @JoinColumn(name="FLUXOCONTACREDITO")
    private Finfluxoplanoconta finfluxoplanoconta;
    @ManyToOne
    @JoinColumn(name="FLUXOCONTADEBITO")
    private Finfluxoplanoconta finfluxoplanoconta2;
    @ManyToOne
    @JoinColumn(name="ORCCONTACREDITO")
    private Orcplanoconta orcplanoconta;
    @ManyToOne
    @JoinColumn(name="ORCCONTADEBITO")
    private Orcplanoconta orcplanoconta2;
    @OneToMany(mappedBy="fincaixa")
    private Set<Finlancamento> finlancamento;
    @OneToMany(mappedBy="fincaixa")
    private Set<Optransacao> optransacao;

    /** Default constructor. */
    public Fincaixa() {
        super();
    }

    /**
     * Access method for codfincaixa.
     *
     * @return the current value of codfincaixa
     */
    public int getCodfincaixa() {
        return codfincaixa;
    }

    /**
     * Setter method for codfincaixa.
     *
     * @param aCodfincaixa the new value for codfincaixa
     */
    public void setCodfincaixa(int aCodfincaixa) {
        codfincaixa = aCodfincaixa;
    }

    /**
     * Access method for descfincaixa.
     *
     * @return the current value of descfincaixa
     */
    public String getDescfincaixa() {
        return descfincaixa;
    }

    /**
     * Setter method for descfincaixa.
     *
     * @param aDescfincaixa the new value for descfincaixa
     */
    public void setDescfincaixa(String aDescfincaixa) {
        descfincaixa = aDescfincaixa;
    }

    /**
     * Access method for datacaixaabertura.
     *
     * @return the current value of datacaixaabertura
     */
    public Date getDatacaixaabertura() {
        return datacaixaabertura;
    }

    /**
     * Setter method for datacaixaabertura.
     *
     * @param aDatacaixaabertura the new value for datacaixaabertura
     */
    public void setDatacaixaabertura(Date aDatacaixaabertura) {
        datacaixaabertura = aDatacaixaabertura;
    }

    /**
     * Access method for datacaixafechamento.
     *
     * @return the current value of datacaixafechamento
     */
    public Date getDatacaixafechamento() {
        return datacaixafechamento;
    }

    /**
     * Setter method for datacaixafechamento.
     *
     * @param aDatacaixafechamento the new value for datacaixafechamento
     */
    public void setDatacaixafechamento(Date aDatacaixafechamento) {
        datacaixafechamento = aDatacaixafechamento;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for finplanoconta.
     *
     * @return the current value of finplanoconta
     */
    public Finplanoconta getFinplanoconta() {
        return finplanoconta;
    }

    /**
     * Setter method for finplanoconta.
     *
     * @param aFinplanoconta the new value for finplanoconta
     */
    public void setFinplanoconta(Finplanoconta aFinplanoconta) {
        finplanoconta = aFinplanoconta;
    }

    /**
     * Access method for finplanoconta2.
     *
     * @return the current value of finplanoconta2
     */
    public Finplanoconta getFinplanoconta2() {
        return finplanoconta2;
    }

    /**
     * Setter method for finplanoconta2.
     *
     * @param aFinplanoconta2 the new value for finplanoconta2
     */
    public void setFinplanoconta2(Finplanoconta aFinplanoconta2) {
        finplanoconta2 = aFinplanoconta2;
    }

    /**
     * Access method for finfluxoplanoconta.
     *
     * @return the current value of finfluxoplanoconta
     */
    public Finfluxoplanoconta getFinfluxoplanoconta() {
        return finfluxoplanoconta;
    }

    /**
     * Setter method for finfluxoplanoconta.
     *
     * @param aFinfluxoplanoconta the new value for finfluxoplanoconta
     */
    public void setFinfluxoplanoconta(Finfluxoplanoconta aFinfluxoplanoconta) {
        finfluxoplanoconta = aFinfluxoplanoconta;
    }

    /**
     * Access method for finfluxoplanoconta2.
     *
     * @return the current value of finfluxoplanoconta2
     */
    public Finfluxoplanoconta getFinfluxoplanoconta2() {
        return finfluxoplanoconta2;
    }

    /**
     * Setter method for finfluxoplanoconta2.
     *
     * @param aFinfluxoplanoconta2 the new value for finfluxoplanoconta2
     */
    public void setFinfluxoplanoconta2(Finfluxoplanoconta aFinfluxoplanoconta2) {
        finfluxoplanoconta2 = aFinfluxoplanoconta2;
    }

    /**
     * Access method for orcplanoconta.
     *
     * @return the current value of orcplanoconta
     */
    public Orcplanoconta getOrcplanoconta() {
        return orcplanoconta;
    }

    /**
     * Setter method for orcplanoconta.
     *
     * @param aOrcplanoconta the new value for orcplanoconta
     */
    public void setOrcplanoconta(Orcplanoconta aOrcplanoconta) {
        orcplanoconta = aOrcplanoconta;
    }

    /**
     * Access method for orcplanoconta2.
     *
     * @return the current value of orcplanoconta2
     */
    public Orcplanoconta getOrcplanoconta2() {
        return orcplanoconta2;
    }

    /**
     * Setter method for orcplanoconta2.
     *
     * @param aOrcplanoconta2 the new value for orcplanoconta2
     */
    public void setOrcplanoconta2(Orcplanoconta aOrcplanoconta2) {
        orcplanoconta2 = aOrcplanoconta2;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Set<Finlancamento> getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Set<Finlancamento> aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Compares the key for this instance with another Fincaixa.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Fincaixa and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Fincaixa)) {
            return false;
        }
        Fincaixa that = (Fincaixa) other;
        if (this.getCodfincaixa() != that.getCodfincaixa()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Fincaixa.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Fincaixa)) return false;
        return this.equalKeys(other) && ((Fincaixa)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfincaixa();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Fincaixa |");
        sb.append(" codfincaixa=").append(getCodfincaixa());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfincaixa", Integer.valueOf(getCodfincaixa()));
        return ret;
    }

}
