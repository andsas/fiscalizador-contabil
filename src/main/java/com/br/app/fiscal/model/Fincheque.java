package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FINCHEQUE")
public class Fincheque implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfincheque";

    @Id
    @Column(name="CODFINCHEQUE", unique=true, nullable=false, precision=10)
    private int codfincheque;
    @Column(name="TIPOCONCILIACAO", precision=5)
    private short tipoconciliacao;
    @Column(name="CONCILIADO", precision=5)
    private short conciliado;
    @Column(name="CHQNUM", precision=10)
    private int chqnum;
    @Column(name="CHQVALOR", precision=15, scale=4)
    private BigDecimal chqvalor;
    @Column(name="CHQDATA")
    private Date chqdata;
    @Column(name="CHQBOMPARA")
    private Date chqbompara;
    @Column(name="CHQNOME", length=250)
    private String chqnome;
    @Column(name="CHQDOCUMENTO", length=20)
    private String chqdocumento;
    @Column(name="CHQDI", length=60)
    private String chqdi;
    @Column(name="CHQCLIENTEDESDE")
    private Date chqclientedesde;
    @Column(name="CHQFAVORECIDO", length=250)
    private String chqfavorecido;
    @Column(name="CHQFOTO")
    private byte[] chqfoto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFINBAIXA", nullable=false)
    private Finbaixa finbaixa;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAOCOBR", nullable=false)
    private Optransacaocobr optransacaocobr;
    @ManyToOne
    @JoinColumn(name="CHQBANCO")
    private Finbanco finbanco;
    @ManyToOne
    @JoinColumn(name="CHQAGENCIA")
    private Finagencia finagencia;

    /** Default constructor. */
    public Fincheque() {
        super();
    }

    /**
     * Access method for codfincheque.
     *
     * @return the current value of codfincheque
     */
    public int getCodfincheque() {
        return codfincheque;
    }

    /**
     * Setter method for codfincheque.
     *
     * @param aCodfincheque the new value for codfincheque
     */
    public void setCodfincheque(int aCodfincheque) {
        codfincheque = aCodfincheque;
    }

    /**
     * Access method for tipoconciliacao.
     *
     * @return the current value of tipoconciliacao
     */
    public short getTipoconciliacao() {
        return tipoconciliacao;
    }

    /**
     * Setter method for tipoconciliacao.
     *
     * @param aTipoconciliacao the new value for tipoconciliacao
     */
    public void setTipoconciliacao(short aTipoconciliacao) {
        tipoconciliacao = aTipoconciliacao;
    }

    /**
     * Access method for conciliado.
     *
     * @return the current value of conciliado
     */
    public short getConciliado() {
        return conciliado;
    }

    /**
     * Setter method for conciliado.
     *
     * @param aConciliado the new value for conciliado
     */
    public void setConciliado(short aConciliado) {
        conciliado = aConciliado;
    }

    /**
     * Access method for chqnum.
     *
     * @return the current value of chqnum
     */
    public int getChqnum() {
        return chqnum;
    }

    /**
     * Setter method for chqnum.
     *
     * @param aChqnum the new value for chqnum
     */
    public void setChqnum(int aChqnum) {
        chqnum = aChqnum;
    }

    /**
     * Access method for chqvalor.
     *
     * @return the current value of chqvalor
     */
    public BigDecimal getChqvalor() {
        return chqvalor;
    }

    /**
     * Setter method for chqvalor.
     *
     * @param aChqvalor the new value for chqvalor
     */
    public void setChqvalor(BigDecimal aChqvalor) {
        chqvalor = aChqvalor;
    }

    /**
     * Access method for chqdata.
     *
     * @return the current value of chqdata
     */
    public Date getChqdata() {
        return chqdata;
    }

    /**
     * Setter method for chqdata.
     *
     * @param aChqdata the new value for chqdata
     */
    public void setChqdata(Date aChqdata) {
        chqdata = aChqdata;
    }

    /**
     * Access method for chqbompara.
     *
     * @return the current value of chqbompara
     */
    public Date getChqbompara() {
        return chqbompara;
    }

    /**
     * Setter method for chqbompara.
     *
     * @param aChqbompara the new value for chqbompara
     */
    public void setChqbompara(Date aChqbompara) {
        chqbompara = aChqbompara;
    }

    /**
     * Access method for chqnome.
     *
     * @return the current value of chqnome
     */
    public String getChqnome() {
        return chqnome;
    }

    /**
     * Setter method for chqnome.
     *
     * @param aChqnome the new value for chqnome
     */
    public void setChqnome(String aChqnome) {
        chqnome = aChqnome;
    }

    /**
     * Access method for chqdocumento.
     *
     * @return the current value of chqdocumento
     */
    public String getChqdocumento() {
        return chqdocumento;
    }

    /**
     * Setter method for chqdocumento.
     *
     * @param aChqdocumento the new value for chqdocumento
     */
    public void setChqdocumento(String aChqdocumento) {
        chqdocumento = aChqdocumento;
    }

    /**
     * Access method for chqdi.
     *
     * @return the current value of chqdi
     */
    public String getChqdi() {
        return chqdi;
    }

    /**
     * Setter method for chqdi.
     *
     * @param aChqdi the new value for chqdi
     */
    public void setChqdi(String aChqdi) {
        chqdi = aChqdi;
    }

    /**
     * Access method for chqclientedesde.
     *
     * @return the current value of chqclientedesde
     */
    public Date getChqclientedesde() {
        return chqclientedesde;
    }

    /**
     * Setter method for chqclientedesde.
     *
     * @param aChqclientedesde the new value for chqclientedesde
     */
    public void setChqclientedesde(Date aChqclientedesde) {
        chqclientedesde = aChqclientedesde;
    }

    /**
     * Access method for chqfavorecido.
     *
     * @return the current value of chqfavorecido
     */
    public String getChqfavorecido() {
        return chqfavorecido;
    }

    /**
     * Setter method for chqfavorecido.
     *
     * @param aChqfavorecido the new value for chqfavorecido
     */
    public void setChqfavorecido(String aChqfavorecido) {
        chqfavorecido = aChqfavorecido;
    }

    /**
     * Access method for chqfoto.
     *
     * @return the current value of chqfoto
     */
    public byte[] getChqfoto() {
        return chqfoto;
    }

    /**
     * Setter method for chqfoto.
     *
     * @param aChqfoto the new value for chqfoto
     */
    public void setChqfoto(byte[] aChqfoto) {
        chqfoto = aChqfoto;
    }

    /**
     * Access method for finbaixa.
     *
     * @return the current value of finbaixa
     */
    public Finbaixa getFinbaixa() {
        return finbaixa;
    }

    /**
     * Setter method for finbaixa.
     *
     * @param aFinbaixa the new value for finbaixa
     */
    public void setFinbaixa(Finbaixa aFinbaixa) {
        finbaixa = aFinbaixa;
    }

    /**
     * Access method for optransacaocobr.
     *
     * @return the current value of optransacaocobr
     */
    public Optransacaocobr getOptransacaocobr() {
        return optransacaocobr;
    }

    /**
     * Setter method for optransacaocobr.
     *
     * @param aOptransacaocobr the new value for optransacaocobr
     */
    public void setOptransacaocobr(Optransacaocobr aOptransacaocobr) {
        optransacaocobr = aOptransacaocobr;
    }

    /**
     * Access method for finbanco.
     *
     * @return the current value of finbanco
     */
    public Finbanco getFinbanco() {
        return finbanco;
    }

    /**
     * Setter method for finbanco.
     *
     * @param aFinbanco the new value for finbanco
     */
    public void setFinbanco(Finbanco aFinbanco) {
        finbanco = aFinbanco;
    }

    /**
     * Access method for finagencia.
     *
     * @return the current value of finagencia
     */
    public Finagencia getFinagencia() {
        return finagencia;
    }

    /**
     * Setter method for finagencia.
     *
     * @param aFinagencia the new value for finagencia
     */
    public void setFinagencia(Finagencia aFinagencia) {
        finagencia = aFinagencia;
    }

    /**
     * Compares the key for this instance with another Fincheque.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Fincheque and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Fincheque)) {
            return false;
        }
        Fincheque that = (Fincheque) other;
        if (this.getCodfincheque() != that.getCodfincheque()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Fincheque.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Fincheque)) return false;
        return this.equalKeys(other) && ((Fincheque)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfincheque();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Fincheque |");
        sb.append(" codfincheque=").append(getCodfincheque());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfincheque", Integer.valueOf(getCodfincheque()));
        return ret;
    }

}
