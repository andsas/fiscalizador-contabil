package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FINCONF")
public class Finconf implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinconf";

    @Id
    @Column(name="CODFINCONF", unique=true, nullable=false, precision=10)
    private int codfinconf;
    @ManyToOne
    @JoinColumn(name="CODFINTIPOREC")
    private Fintipo fintipo;
    @ManyToOne
    @JoinColumn(name="CODFINTIPOPAG")
    private Fintipo fintipo2;
    @ManyToOne
    @JoinColumn(name="CODFINTIPOCOMI")
    private Fintipo fintipo3;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;

    /** Default constructor. */
    public Finconf() {
        super();
    }

    /**
     * Access method for codfinconf.
     *
     * @return the current value of codfinconf
     */
    public int getCodfinconf() {
        return codfinconf;
    }

    /**
     * Setter method for codfinconf.
     *
     * @param aCodfinconf the new value for codfinconf
     */
    public void setCodfinconf(int aCodfinconf) {
        codfinconf = aCodfinconf;
    }

    /**
     * Access method for fintipo.
     *
     * @return the current value of fintipo
     */
    public Fintipo getFintipo() {
        return fintipo;
    }

    /**
     * Setter method for fintipo.
     *
     * @param aFintipo the new value for fintipo
     */
    public void setFintipo(Fintipo aFintipo) {
        fintipo = aFintipo;
    }

    /**
     * Access method for fintipo2.
     *
     * @return the current value of fintipo2
     */
    public Fintipo getFintipo2() {
        return fintipo2;
    }

    /**
     * Setter method for fintipo2.
     *
     * @param aFintipo2 the new value for fintipo2
     */
    public void setFintipo2(Fintipo aFintipo2) {
        fintipo2 = aFintipo2;
    }

    /**
     * Access method for fintipo3.
     *
     * @return the current value of fintipo3
     */
    public Fintipo getFintipo3() {
        return fintipo3;
    }

    /**
     * Setter method for fintipo3.
     *
     * @param aFintipo3 the new value for fintipo3
     */
    public void setFintipo3(Fintipo aFintipo3) {
        fintipo3 = aFintipo3;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Compares the key for this instance with another Finconf.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finconf and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finconf)) {
            return false;
        }
        Finconf that = (Finconf) other;
        if (this.getCodfinconf() != that.getCodfinconf()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finconf.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finconf)) return false;
        return this.equalKeys(other) && ((Finconf)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfinconf();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finconf |");
        sb.append(" codfinconf=").append(getCodfinconf());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinconf", Integer.valueOf(getCodfinconf()));
        return ret;
    }

}
