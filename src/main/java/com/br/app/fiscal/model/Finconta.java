package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINCONTA")
public class Finconta implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinconta";

    @Id
    @Column(name="CODFINCONTA", unique=true, nullable=false, length=20)
    private String codfinconta;
    @Column(name="LIMITE", precision=15, scale=4)
    private BigDecimal limite;
    @Column(name="BOLREGBANCO", length=40)
    private String bolregbanco;
    @Column(name="BOLCODCEDENTE", length=40)
    private String bolcodcedente;
    @Column(name="BOLCARTEIRA", length=40)
    private String bolcarteira;
    @Column(name="BOLDIASPROTESTO", precision=5)
    private short boldiasprotesto;
    @Column(name="BOLNOSSONUMINI", precision=10)
    private int bolnossonumini;
    @Column(name="BOLNOSSONUMFIM", precision=10)
    private int bolnossonumfim;
    @Column(name="BOLNOSSONUMATUAL", precision=10)
    private int bolnossonumatual;
    @Column(name="BOLLOCALPAGAMENTO")
    private String bollocalpagamento;
    @Column(name="BOLINSTRUCOES")
    private String bolinstrucoes;
    @Column(name="BOLLICENSACOBREBEMX", length=250)
    private String bollicensacobrebemx;
    @Column(name="BOLCONVENIO", length=40)
    private String bolconvenio;
    @Column(name="BOLTIPOEMISSAO", precision=5)
    private short boltipoemissao;
    @OneToMany(mappedBy="finconta")
    private Set<Finbaixa> finbaixa;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFINBANCO", nullable=false)
    private Finbanco finbanco;
    @ManyToOne
    @JoinColumn(name="CODFINAGENCIA")
    private Finagencia finagencia;
    @ManyToOne
    @JoinColumn(name="CODAGCONTARESPONSAVEL")
    private Agagente agagente;
    @OneToMany(mappedBy="finconta")
    private Set<Optransacaocobr> optransacaocobr;

    /** Default constructor. */
    public Finconta() {
        super();
    }

    /**
     * Access method for codfinconta.
     *
     * @return the current value of codfinconta
     */
    public String getCodfinconta() {
        return codfinconta;
    }

    /**
     * Setter method for codfinconta.
     *
     * @param aCodfinconta the new value for codfinconta
     */
    public void setCodfinconta(String aCodfinconta) {
        codfinconta = aCodfinconta;
    }

    /**
     * Access method for limite.
     *
     * @return the current value of limite
     */
    public BigDecimal getLimite() {
        return limite;
    }

    /**
     * Setter method for limite.
     *
     * @param aLimite the new value for limite
     */
    public void setLimite(BigDecimal aLimite) {
        limite = aLimite;
    }

    /**
     * Access method for bolregbanco.
     *
     * @return the current value of bolregbanco
     */
    public String getBolregbanco() {
        return bolregbanco;
    }

    /**
     * Setter method for bolregbanco.
     *
     * @param aBolregbanco the new value for bolregbanco
     */
    public void setBolregbanco(String aBolregbanco) {
        bolregbanco = aBolregbanco;
    }

    /**
     * Access method for bolcodcedente.
     *
     * @return the current value of bolcodcedente
     */
    public String getBolcodcedente() {
        return bolcodcedente;
    }

    /**
     * Setter method for bolcodcedente.
     *
     * @param aBolcodcedente the new value for bolcodcedente
     */
    public void setBolcodcedente(String aBolcodcedente) {
        bolcodcedente = aBolcodcedente;
    }

    /**
     * Access method for bolcarteira.
     *
     * @return the current value of bolcarteira
     */
    public String getBolcarteira() {
        return bolcarteira;
    }

    /**
     * Setter method for bolcarteira.
     *
     * @param aBolcarteira the new value for bolcarteira
     */
    public void setBolcarteira(String aBolcarteira) {
        bolcarteira = aBolcarteira;
    }

    /**
     * Access method for boldiasprotesto.
     *
     * @return the current value of boldiasprotesto
     */
    public short getBoldiasprotesto() {
        return boldiasprotesto;
    }

    /**
     * Setter method for boldiasprotesto.
     *
     * @param aBoldiasprotesto the new value for boldiasprotesto
     */
    public void setBoldiasprotesto(short aBoldiasprotesto) {
        boldiasprotesto = aBoldiasprotesto;
    }

    /**
     * Access method for bolnossonumini.
     *
     * @return the current value of bolnossonumini
     */
    public int getBolnossonumini() {
        return bolnossonumini;
    }

    /**
     * Setter method for bolnossonumini.
     *
     * @param aBolnossonumini the new value for bolnossonumini
     */
    public void setBolnossonumini(int aBolnossonumini) {
        bolnossonumini = aBolnossonumini;
    }

    /**
     * Access method for bolnossonumfim.
     *
     * @return the current value of bolnossonumfim
     */
    public int getBolnossonumfim() {
        return bolnossonumfim;
    }

    /**
     * Setter method for bolnossonumfim.
     *
     * @param aBolnossonumfim the new value for bolnossonumfim
     */
    public void setBolnossonumfim(int aBolnossonumfim) {
        bolnossonumfim = aBolnossonumfim;
    }

    /**
     * Access method for bolnossonumatual.
     *
     * @return the current value of bolnossonumatual
     */
    public int getBolnossonumatual() {
        return bolnossonumatual;
    }

    /**
     * Setter method for bolnossonumatual.
     *
     * @param aBolnossonumatual the new value for bolnossonumatual
     */
    public void setBolnossonumatual(int aBolnossonumatual) {
        bolnossonumatual = aBolnossonumatual;
    }

    /**
     * Access method for bollocalpagamento.
     *
     * @return the current value of bollocalpagamento
     */
    public String getBollocalpagamento() {
        return bollocalpagamento;
    }

    /**
     * Setter method for bollocalpagamento.
     *
     * @param aBollocalpagamento the new value for bollocalpagamento
     */
    public void setBollocalpagamento(String aBollocalpagamento) {
        bollocalpagamento = aBollocalpagamento;
    }

    /**
     * Access method for bolinstrucoes.
     *
     * @return the current value of bolinstrucoes
     */
    public String getBolinstrucoes() {
        return bolinstrucoes;
    }

    /**
     * Setter method for bolinstrucoes.
     *
     * @param aBolinstrucoes the new value for bolinstrucoes
     */
    public void setBolinstrucoes(String aBolinstrucoes) {
        bolinstrucoes = aBolinstrucoes;
    }

    /**
     * Access method for bollicensacobrebemx.
     *
     * @return the current value of bollicensacobrebemx
     */
    public String getBollicensacobrebemx() {
        return bollicensacobrebemx;
    }

    /**
     * Setter method for bollicensacobrebemx.
     *
     * @param aBollicensacobrebemx the new value for bollicensacobrebemx
     */
    public void setBollicensacobrebemx(String aBollicensacobrebemx) {
        bollicensacobrebemx = aBollicensacobrebemx;
    }

    /**
     * Access method for bolconvenio.
     *
     * @return the current value of bolconvenio
     */
    public String getBolconvenio() {
        return bolconvenio;
    }

    /**
     * Setter method for bolconvenio.
     *
     * @param aBolconvenio the new value for bolconvenio
     */
    public void setBolconvenio(String aBolconvenio) {
        bolconvenio = aBolconvenio;
    }

    /**
     * Access method for boltipoemissao.
     *
     * @return the current value of boltipoemissao
     */
    public short getBoltipoemissao() {
        return boltipoemissao;
    }

    /**
     * Setter method for boltipoemissao.
     *
     * @param aBoltipoemissao the new value for boltipoemissao
     */
    public void setBoltipoemissao(short aBoltipoemissao) {
        boltipoemissao = aBoltipoemissao;
    }

    /**
     * Access method for finbaixa.
     *
     * @return the current value of finbaixa
     */
    public Set<Finbaixa> getFinbaixa() {
        return finbaixa;
    }

    /**
     * Setter method for finbaixa.
     *
     * @param aFinbaixa the new value for finbaixa
     */
    public void setFinbaixa(Set<Finbaixa> aFinbaixa) {
        finbaixa = aFinbaixa;
    }

    /**
     * Access method for finbanco.
     *
     * @return the current value of finbanco
     */
    public Finbanco getFinbanco() {
        return finbanco;
    }

    /**
     * Setter method for finbanco.
     *
     * @param aFinbanco the new value for finbanco
     */
    public void setFinbanco(Finbanco aFinbanco) {
        finbanco = aFinbanco;
    }

    /**
     * Access method for finagencia.
     *
     * @return the current value of finagencia
     */
    public Finagencia getFinagencia() {
        return finagencia;
    }

    /**
     * Setter method for finagencia.
     *
     * @param aFinagencia the new value for finagencia
     */
    public void setFinagencia(Finagencia aFinagencia) {
        finagencia = aFinagencia;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for optransacaocobr.
     *
     * @return the current value of optransacaocobr
     */
    public Set<Optransacaocobr> getOptransacaocobr() {
        return optransacaocobr;
    }

    /**
     * Setter method for optransacaocobr.
     *
     * @param aOptransacaocobr the new value for optransacaocobr
     */
    public void setOptransacaocobr(Set<Optransacaocobr> aOptransacaocobr) {
        optransacaocobr = aOptransacaocobr;
    }

    /**
     * Compares the key for this instance with another Finconta.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finconta and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finconta)) {
            return false;
        }
        Finconta that = (Finconta) other;
        Object myCodfinconta = this.getCodfinconta();
        Object yourCodfinconta = that.getCodfinconta();
        if (myCodfinconta==null ? yourCodfinconta!=null : !myCodfinconta.equals(yourCodfinconta)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finconta.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finconta)) return false;
        return this.equalKeys(other) && ((Finconta)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfinconta() == null) {
            i = 0;
        } else {
            i = getCodfinconta().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finconta |");
        sb.append(" codfinconta=").append(getCodfinconta());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinconta", getCodfinconta());
        return ret;
    }

}
