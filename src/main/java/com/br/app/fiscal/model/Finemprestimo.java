package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINEMPRESTIMO")
public class Finemprestimo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinemprestimo";

    @Id
    @Column(name="CODFINEMPRESTIMO", unique=true, nullable=false, precision=10)
    private int codfinemprestimo;
    @Column(name="DESCFINEMPRESTIMO", nullable=false, length=250)
    private String descfinemprestimo;
    @Column(name="DATAINICIO", nullable=false)
    private Timestamp datainicio;
    @Column(name="DATATERMINO", nullable=false)
    private Timestamp datatermino;
    @Column(name="DATAQUITACAO")
    private Timestamp dataquitacao;
    @Column(name="PERCMENSAL", precision=15, scale=4)
    private BigDecimal percmensal;
    @Column(name="PERCIOF", precision=15, scale=4)
    private BigDecimal perciof;
    @Column(name="PERCIRPJ", precision=15, scale=4)
    private BigDecimal percirpj;
    @Column(name="VALORTOTAL", nullable=false, precision=15, scale=4)
    private BigDecimal valortotal;
    @Column(name="VALORPARCELA", nullable=false, precision=15, scale=4)
    private BigDecimal valorparcela;
    @Column(name="MESES", nullable=false, precision=10)
    private int meses;
    @Column(name="DIAVENCIMENTO", nullable=false, precision=10)
    private int diavencimento;
    @Column(name="DATAPRIMPARC")
    private Timestamp dataprimparc;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGCONTRATANTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODCONTCONTRATO")
    private Contcontrato contcontrato;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGCONTRATADO", nullable=false)
    private Agagente agagente2;
    @OneToMany(mappedBy="finemprestimo")
    private Set<Finlancamento> finlancamento;

    /** Default constructor. */
    public Finemprestimo() {
        super();
    }

    /**
     * Access method for codfinemprestimo.
     *
     * @return the current value of codfinemprestimo
     */
    public int getCodfinemprestimo() {
        return codfinemprestimo;
    }

    /**
     * Setter method for codfinemprestimo.
     *
     * @param aCodfinemprestimo the new value for codfinemprestimo
     */
    public void setCodfinemprestimo(int aCodfinemprestimo) {
        codfinemprestimo = aCodfinemprestimo;
    }

    /**
     * Access method for descfinemprestimo.
     *
     * @return the current value of descfinemprestimo
     */
    public String getDescfinemprestimo() {
        return descfinemprestimo;
    }

    /**
     * Setter method for descfinemprestimo.
     *
     * @param aDescfinemprestimo the new value for descfinemprestimo
     */
    public void setDescfinemprestimo(String aDescfinemprestimo) {
        descfinemprestimo = aDescfinemprestimo;
    }

    /**
     * Access method for datainicio.
     *
     * @return the current value of datainicio
     */
    public Timestamp getDatainicio() {
        return datainicio;
    }

    /**
     * Setter method for datainicio.
     *
     * @param aDatainicio the new value for datainicio
     */
    public void setDatainicio(Timestamp aDatainicio) {
        datainicio = aDatainicio;
    }

    /**
     * Access method for datatermino.
     *
     * @return the current value of datatermino
     */
    public Timestamp getDatatermino() {
        return datatermino;
    }

    /**
     * Setter method for datatermino.
     *
     * @param aDatatermino the new value for datatermino
     */
    public void setDatatermino(Timestamp aDatatermino) {
        datatermino = aDatatermino;
    }

    /**
     * Access method for dataquitacao.
     *
     * @return the current value of dataquitacao
     */
    public Timestamp getDataquitacao() {
        return dataquitacao;
    }

    /**
     * Setter method for dataquitacao.
     *
     * @param aDataquitacao the new value for dataquitacao
     */
    public void setDataquitacao(Timestamp aDataquitacao) {
        dataquitacao = aDataquitacao;
    }

    /**
     * Access method for percmensal.
     *
     * @return the current value of percmensal
     */
    public BigDecimal getPercmensal() {
        return percmensal;
    }

    /**
     * Setter method for percmensal.
     *
     * @param aPercmensal the new value for percmensal
     */
    public void setPercmensal(BigDecimal aPercmensal) {
        percmensal = aPercmensal;
    }

    /**
     * Access method for perciof.
     *
     * @return the current value of perciof
     */
    public BigDecimal getPerciof() {
        return perciof;
    }

    /**
     * Setter method for perciof.
     *
     * @param aPerciof the new value for perciof
     */
    public void setPerciof(BigDecimal aPerciof) {
        perciof = aPerciof;
    }

    /**
     * Access method for percirpj.
     *
     * @return the current value of percirpj
     */
    public BigDecimal getPercirpj() {
        return percirpj;
    }

    /**
     * Setter method for percirpj.
     *
     * @param aPercirpj the new value for percirpj
     */
    public void setPercirpj(BigDecimal aPercirpj) {
        percirpj = aPercirpj;
    }

    /**
     * Access method for valortotal.
     *
     * @return the current value of valortotal
     */
    public BigDecimal getValortotal() {
        return valortotal;
    }

    /**
     * Setter method for valortotal.
     *
     * @param aValortotal the new value for valortotal
     */
    public void setValortotal(BigDecimal aValortotal) {
        valortotal = aValortotal;
    }

    /**
     * Access method for valorparcela.
     *
     * @return the current value of valorparcela
     */
    public BigDecimal getValorparcela() {
        return valorparcela;
    }

    /**
     * Setter method for valorparcela.
     *
     * @param aValorparcela the new value for valorparcela
     */
    public void setValorparcela(BigDecimal aValorparcela) {
        valorparcela = aValorparcela;
    }

    /**
     * Access method for meses.
     *
     * @return the current value of meses
     */
    public int getMeses() {
        return meses;
    }

    /**
     * Setter method for meses.
     *
     * @param aMeses the new value for meses
     */
    public void setMeses(int aMeses) {
        meses = aMeses;
    }

    /**
     * Access method for diavencimento.
     *
     * @return the current value of diavencimento
     */
    public int getDiavencimento() {
        return diavencimento;
    }

    /**
     * Setter method for diavencimento.
     *
     * @param aDiavencimento the new value for diavencimento
     */
    public void setDiavencimento(int aDiavencimento) {
        diavencimento = aDiavencimento;
    }

    /**
     * Access method for dataprimparc.
     *
     * @return the current value of dataprimparc
     */
    public Timestamp getDataprimparc() {
        return dataprimparc;
    }

    /**
     * Setter method for dataprimparc.
     *
     * @param aDataprimparc the new value for dataprimparc
     */
    public void setDataprimparc(Timestamp aDataprimparc) {
        dataprimparc = aDataprimparc;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for contcontrato.
     *
     * @return the current value of contcontrato
     */
    public Contcontrato getContcontrato() {
        return contcontrato;
    }

    /**
     * Setter method for contcontrato.
     *
     * @param aContcontrato the new value for contcontrato
     */
    public void setContcontrato(Contcontrato aContcontrato) {
        contcontrato = aContcontrato;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Agagente getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Agagente aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Set<Finlancamento> getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Set<Finlancamento> aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Compares the key for this instance with another Finemprestimo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finemprestimo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finemprestimo)) {
            return false;
        }
        Finemprestimo that = (Finemprestimo) other;
        if (this.getCodfinemprestimo() != that.getCodfinemprestimo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finemprestimo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finemprestimo)) return false;
        return this.equalKeys(other) && ((Finemprestimo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfinemprestimo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finemprestimo |");
        sb.append(" codfinemprestimo=").append(getCodfinemprestimo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinemprestimo", Integer.valueOf(getCodfinemprestimo()));
        return ret;
    }

}
