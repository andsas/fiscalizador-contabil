package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINEVENTO")
public class Finevento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinevento";

    @Id
    @Column(name="CODFINEVENTO", unique=true, nullable=false, precision=10)
    private int codfinevento;
    @Column(name="DESCFINEVENTO", nullable=false, length=250)
    private String descfinevento;
    @Column(name="CORFINEVENTO", precision=10)
    private int corfinevento;
    @Column(name="NUMERO", precision=10)
    private int numero;
    @ManyToOne
    @JoinColumn(name="CODFINTIPO")
    private Fintipo fintipo;
    @OneToMany(mappedBy="finevento")
    private Set<Finlancamento> finlancamento;

    /** Default constructor. */
    public Finevento() {
        super();
    }

    /**
     * Access method for codfinevento.
     *
     * @return the current value of codfinevento
     */
    public int getCodfinevento() {
        return codfinevento;
    }

    /**
     * Setter method for codfinevento.
     *
     * @param aCodfinevento the new value for codfinevento
     */
    public void setCodfinevento(int aCodfinevento) {
        codfinevento = aCodfinevento;
    }

    /**
     * Access method for descfinevento.
     *
     * @return the current value of descfinevento
     */
    public String getDescfinevento() {
        return descfinevento;
    }

    /**
     * Setter method for descfinevento.
     *
     * @param aDescfinevento the new value for descfinevento
     */
    public void setDescfinevento(String aDescfinevento) {
        descfinevento = aDescfinevento;
    }

    /**
     * Access method for corfinevento.
     *
     * @return the current value of corfinevento
     */
    public int getCorfinevento() {
        return corfinevento;
    }

    /**
     * Setter method for corfinevento.
     *
     * @param aCorfinevento the new value for corfinevento
     */
    public void setCorfinevento(int aCorfinevento) {
        corfinevento = aCorfinevento;
    }

    /**
     * Access method for numero.
     *
     * @return the current value of numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Setter method for numero.
     *
     * @param aNumero the new value for numero
     */
    public void setNumero(int aNumero) {
        numero = aNumero;
    }

    /**
     * Access method for fintipo.
     *
     * @return the current value of fintipo
     */
    public Fintipo getFintipo() {
        return fintipo;
    }

    /**
     * Setter method for fintipo.
     *
     * @param aFintipo the new value for fintipo
     */
    public void setFintipo(Fintipo aFintipo) {
        fintipo = aFintipo;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Set<Finlancamento> getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Set<Finlancamento> aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Compares the key for this instance with another Finevento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finevento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finevento)) {
            return false;
        }
        Finevento that = (Finevento) other;
        if (this.getCodfinevento() != that.getCodfinevento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finevento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finevento)) return false;
        return this.equalKeys(other) && ((Finevento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfinevento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finevento |");
        sb.append(" codfinevento=").append(getCodfinevento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinevento", Integer.valueOf(getCodfinevento()));
        return ret;
    }

}
