package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINFLUXOPLANOCONTA")
public class Finfluxoplanoconta implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinfluxoplanoconta";

    @Id
    @Column(name="CODFINFLUXOPLANOCONTA", unique=true, nullable=false, length=20)
    private String codfinfluxoplanoconta;
    @Column(name="DESCFINFLUXOPLANOCONTA", nullable=false, length=250)
    private String descfinfluxoplanoconta;
    @Column(name="NIVEL", precision=10)
    private int nivel;
    @Column(name="CODREDUZIDO", precision=10)
    private int codreduzido;
    @Column(name="DEBCRED", precision=5)
    private short debcred;
    @OneToMany(mappedBy="finfluxoplanoconta")
    private Set<Agagente> agagente;
    @OneToMany(mappedBy="finfluxoplanoconta2")
    private Set<Agagente> agagente2;
    @OneToMany(mappedBy="finfluxoplanoconta")
    private Set<Cobrforma> cobrforma;
    @OneToMany(mappedBy="finfluxoplanoconta2")
    private Set<Cobrforma> cobrforma2;
    @OneToMany(mappedBy="finfluxoplanoconta")
    private Set<Ctbplanoconta> ctbplanoconta;
    @OneToMany(mappedBy="finfluxoplanoconta")
    private Set<Finbaixa> finbaixa;
    @OneToMany(mappedBy="finfluxoplanoconta")
    private Set<Fincaixa> fincaixa;
    @OneToMany(mappedBy="finfluxoplanoconta2")
    private Set<Fincaixa> fincaixa2;
    @OneToMany(mappedBy="finfluxoplanoconta3")
    private Set<Finfluxoplanoconta> finfluxoplanoconta4;
    @ManyToOne
    @JoinColumn(name="CODPARENTFINFLUXOPLANOCONTA")
    private Finfluxoplanoconta finfluxoplanoconta3;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @OneToMany(mappedBy="finfluxoplanoconta")
    private Set<Folevento> folevento;
    @OneToMany(mappedBy="finfluxoplanoconta2")
    private Set<Folevento> folevento2;
    @OneToMany(mappedBy="finfluxoplanoconta")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="finfluxoplanoconta2")
    private Set<Impplanofiscal> impplanofiscal2;

    /** Default constructor. */
    public Finfluxoplanoconta() {
        super();
    }

    /**
     * Access method for codfinfluxoplanoconta.
     *
     * @return the current value of codfinfluxoplanoconta
     */
    public String getCodfinfluxoplanoconta() {
        return codfinfluxoplanoconta;
    }

    /**
     * Setter method for codfinfluxoplanoconta.
     *
     * @param aCodfinfluxoplanoconta the new value for codfinfluxoplanoconta
     */
    public void setCodfinfluxoplanoconta(String aCodfinfluxoplanoconta) {
        codfinfluxoplanoconta = aCodfinfluxoplanoconta;
    }

    /**
     * Access method for descfinfluxoplanoconta.
     *
     * @return the current value of descfinfluxoplanoconta
     */
    public String getDescfinfluxoplanoconta() {
        return descfinfluxoplanoconta;
    }

    /**
     * Setter method for descfinfluxoplanoconta.
     *
     * @param aDescfinfluxoplanoconta the new value for descfinfluxoplanoconta
     */
    public void setDescfinfluxoplanoconta(String aDescfinfluxoplanoconta) {
        descfinfluxoplanoconta = aDescfinfluxoplanoconta;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(int aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for codreduzido.
     *
     * @return the current value of codreduzido
     */
    public int getCodreduzido() {
        return codreduzido;
    }

    /**
     * Setter method for codreduzido.
     *
     * @param aCodreduzido the new value for codreduzido
     */
    public void setCodreduzido(int aCodreduzido) {
        codreduzido = aCodreduzido;
    }

    /**
     * Access method for debcred.
     *
     * @return the current value of debcred
     */
    public short getDebcred() {
        return debcred;
    }

    /**
     * Setter method for debcred.
     *
     * @param aDebcred the new value for debcred
     */
    public void setDebcred(short aDebcred) {
        debcred = aDebcred;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Set<Agagente> getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Set<Agagente> aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Set<Cobrforma> getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Set<Cobrforma> aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Access method for cobrforma2.
     *
     * @return the current value of cobrforma2
     */
    public Set<Cobrforma> getCobrforma2() {
        return cobrforma2;
    }

    /**
     * Setter method for cobrforma2.
     *
     * @param aCobrforma2 the new value for cobrforma2
     */
    public void setCobrforma2(Set<Cobrforma> aCobrforma2) {
        cobrforma2 = aCobrforma2;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Set<Ctbplanoconta> getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Set<Ctbplanoconta> aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for finbaixa.
     *
     * @return the current value of finbaixa
     */
    public Set<Finbaixa> getFinbaixa() {
        return finbaixa;
    }

    /**
     * Setter method for finbaixa.
     *
     * @param aFinbaixa the new value for finbaixa
     */
    public void setFinbaixa(Set<Finbaixa> aFinbaixa) {
        finbaixa = aFinbaixa;
    }

    /**
     * Access method for fincaixa.
     *
     * @return the current value of fincaixa
     */
    public Set<Fincaixa> getFincaixa() {
        return fincaixa;
    }

    /**
     * Setter method for fincaixa.
     *
     * @param aFincaixa the new value for fincaixa
     */
    public void setFincaixa(Set<Fincaixa> aFincaixa) {
        fincaixa = aFincaixa;
    }

    /**
     * Access method for fincaixa2.
     *
     * @return the current value of fincaixa2
     */
    public Set<Fincaixa> getFincaixa2() {
        return fincaixa2;
    }

    /**
     * Setter method for fincaixa2.
     *
     * @param aFincaixa2 the new value for fincaixa2
     */
    public void setFincaixa2(Set<Fincaixa> aFincaixa2) {
        fincaixa2 = aFincaixa2;
    }

    /**
     * Access method for finfluxoplanoconta4.
     *
     * @return the current value of finfluxoplanoconta4
     */
    public Set<Finfluxoplanoconta> getFinfluxoplanoconta4() {
        return finfluxoplanoconta4;
    }

    /**
     * Setter method for finfluxoplanoconta4.
     *
     * @param aFinfluxoplanoconta4 the new value for finfluxoplanoconta4
     */
    public void setFinfluxoplanoconta4(Set<Finfluxoplanoconta> aFinfluxoplanoconta4) {
        finfluxoplanoconta4 = aFinfluxoplanoconta4;
    }

    /**
     * Access method for finfluxoplanoconta3.
     *
     * @return the current value of finfluxoplanoconta3
     */
    public Finfluxoplanoconta getFinfluxoplanoconta3() {
        return finfluxoplanoconta3;
    }

    /**
     * Setter method for finfluxoplanoconta3.
     *
     * @param aFinfluxoplanoconta3 the new value for finfluxoplanoconta3
     */
    public void setFinfluxoplanoconta3(Finfluxoplanoconta aFinfluxoplanoconta3) {
        finfluxoplanoconta3 = aFinfluxoplanoconta3;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for folevento.
     *
     * @return the current value of folevento
     */
    public Set<Folevento> getFolevento() {
        return folevento;
    }

    /**
     * Setter method for folevento.
     *
     * @param aFolevento the new value for folevento
     */
    public void setFolevento(Set<Folevento> aFolevento) {
        folevento = aFolevento;
    }

    /**
     * Access method for folevento2.
     *
     * @return the current value of folevento2
     */
    public Set<Folevento> getFolevento2() {
        return folevento2;
    }

    /**
     * Setter method for folevento2.
     *
     * @param aFolevento2 the new value for folevento2
     */
    public void setFolevento2(Set<Folevento> aFolevento2) {
        folevento2 = aFolevento2;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for impplanofiscal2.
     *
     * @return the current value of impplanofiscal2
     */
    public Set<Impplanofiscal> getImpplanofiscal2() {
        return impplanofiscal2;
    }

    /**
     * Setter method for impplanofiscal2.
     *
     * @param aImpplanofiscal2 the new value for impplanofiscal2
     */
    public void setImpplanofiscal2(Set<Impplanofiscal> aImpplanofiscal2) {
        impplanofiscal2 = aImpplanofiscal2;
    }

    /**
     * Compares the key for this instance with another Finfluxoplanoconta.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finfluxoplanoconta and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finfluxoplanoconta)) {
            return false;
        }
        Finfluxoplanoconta that = (Finfluxoplanoconta) other;
        Object myCodfinfluxoplanoconta = this.getCodfinfluxoplanoconta();
        Object yourCodfinfluxoplanoconta = that.getCodfinfluxoplanoconta();
        if (myCodfinfluxoplanoconta==null ? yourCodfinfluxoplanoconta!=null : !myCodfinfluxoplanoconta.equals(yourCodfinfluxoplanoconta)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finfluxoplanoconta.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finfluxoplanoconta)) return false;
        return this.equalKeys(other) && ((Finfluxoplanoconta)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfinfluxoplanoconta() == null) {
            i = 0;
        } else {
            i = getCodfinfluxoplanoconta().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finfluxoplanoconta |");
        sb.append(" codfinfluxoplanoconta=").append(getCodfinfluxoplanoconta());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinfluxoplanoconta", getCodfinfluxoplanoconta());
        return ret;
    }

}
