package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FINJURO")
public class Finjuro implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinjuro";

    @Id
    @Column(name="CODFINJURO", unique=true, nullable=false, precision=10)
    private int codfinjuro;
    @Column(name="DESCFINJURO", nullable=false, length=250)
    private String descfinjuro;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @Column(name="TIPOFINJURO", nullable=false, precision=5)
    private short tipofinjuro;
    @Column(name="PERCJURO", length=15)
    private double percjuro;
    @Column(name="PERCMORA", length=15)
    private double percmora;
    @Column(name="PERCMULTA", length=15)
    private double percmulta;
    @Column(name="DATAVALIDADE")
    private Timestamp datavalidade;
    @Column(name="NUMDIASMULTA", precision=5)
    private short numdiasmulta;
    @Column(name="NUMDIASMORA", precision=5)
    private short numdiasmora;
    @Column(name="NUMDIASJURO", precision=5)
    private short numdiasjuro;
    @Column(name="PRINCIPAL", precision=5)
    private short principal;
    @ManyToOne
    @JoinColumn(name="JUROCONTADEBITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="JUROCONTACREDITO")
    private Ctbplanoconta ctbplanoconta2;
    @ManyToOne
    @JoinColumn(name="MORACONTACREDITO")
    private Ctbplanoconta ctbplanoconta3;
    @ManyToOne
    @JoinColumn(name="MORACONTADEBITO")
    private Ctbplanoconta ctbplanoconta4;
    @ManyToOne
    @JoinColumn(name="MULTACONTACREDITO")
    private Ctbplanoconta ctbplanoconta5;
    @ManyToOne
    @JoinColumn(name="MULTACONTADEBITO")
    private Ctbplanoconta ctbplanoconta6;

    /** Default constructor. */
    public Finjuro() {
        super();
    }

    /**
     * Access method for codfinjuro.
     *
     * @return the current value of codfinjuro
     */
    public int getCodfinjuro() {
        return codfinjuro;
    }

    /**
     * Setter method for codfinjuro.
     *
     * @param aCodfinjuro the new value for codfinjuro
     */
    public void setCodfinjuro(int aCodfinjuro) {
        codfinjuro = aCodfinjuro;
    }

    /**
     * Access method for descfinjuro.
     *
     * @return the current value of descfinjuro
     */
    public String getDescfinjuro() {
        return descfinjuro;
    }

    /**
     * Setter method for descfinjuro.
     *
     * @param aDescfinjuro the new value for descfinjuro
     */
    public void setDescfinjuro(String aDescfinjuro) {
        descfinjuro = aDescfinjuro;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for tipofinjuro.
     *
     * @return the current value of tipofinjuro
     */
    public short getTipofinjuro() {
        return tipofinjuro;
    }

    /**
     * Setter method for tipofinjuro.
     *
     * @param aTipofinjuro the new value for tipofinjuro
     */
    public void setTipofinjuro(short aTipofinjuro) {
        tipofinjuro = aTipofinjuro;
    }

    /**
     * Access method for percjuro.
     *
     * @return the current value of percjuro
     */
    public double getPercjuro() {
        return percjuro;
    }

    /**
     * Setter method for percjuro.
     *
     * @param aPercjuro the new value for percjuro
     */
    public void setPercjuro(double aPercjuro) {
        percjuro = aPercjuro;
    }

    /**
     * Access method for percmora.
     *
     * @return the current value of percmora
     */
    public double getPercmora() {
        return percmora;
    }

    /**
     * Setter method for percmora.
     *
     * @param aPercmora the new value for percmora
     */
    public void setPercmora(double aPercmora) {
        percmora = aPercmora;
    }

    /**
     * Access method for percmulta.
     *
     * @return the current value of percmulta
     */
    public double getPercmulta() {
        return percmulta;
    }

    /**
     * Setter method for percmulta.
     *
     * @param aPercmulta the new value for percmulta
     */
    public void setPercmulta(double aPercmulta) {
        percmulta = aPercmulta;
    }

    /**
     * Access method for datavalidade.
     *
     * @return the current value of datavalidade
     */
    public Timestamp getDatavalidade() {
        return datavalidade;
    }

    /**
     * Setter method for datavalidade.
     *
     * @param aDatavalidade the new value for datavalidade
     */
    public void setDatavalidade(Timestamp aDatavalidade) {
        datavalidade = aDatavalidade;
    }

    /**
     * Access method for numdiasmulta.
     *
     * @return the current value of numdiasmulta
     */
    public short getNumdiasmulta() {
        return numdiasmulta;
    }

    /**
     * Setter method for numdiasmulta.
     *
     * @param aNumdiasmulta the new value for numdiasmulta
     */
    public void setNumdiasmulta(short aNumdiasmulta) {
        numdiasmulta = aNumdiasmulta;
    }

    /**
     * Access method for numdiasmora.
     *
     * @return the current value of numdiasmora
     */
    public short getNumdiasmora() {
        return numdiasmora;
    }

    /**
     * Setter method for numdiasmora.
     *
     * @param aNumdiasmora the new value for numdiasmora
     */
    public void setNumdiasmora(short aNumdiasmora) {
        numdiasmora = aNumdiasmora;
    }

    /**
     * Access method for numdiasjuro.
     *
     * @return the current value of numdiasjuro
     */
    public short getNumdiasjuro() {
        return numdiasjuro;
    }

    /**
     * Setter method for numdiasjuro.
     *
     * @param aNumdiasjuro the new value for numdiasjuro
     */
    public void setNumdiasjuro(short aNumdiasjuro) {
        numdiasjuro = aNumdiasjuro;
    }

    /**
     * Access method for principal.
     *
     * @return the current value of principal
     */
    public short getPrincipal() {
        return principal;
    }

    /**
     * Setter method for principal.
     *
     * @param aPrincipal the new value for principal
     */
    public void setPrincipal(short aPrincipal) {
        principal = aPrincipal;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for ctbplanoconta3.
     *
     * @return the current value of ctbplanoconta3
     */
    public Ctbplanoconta getCtbplanoconta3() {
        return ctbplanoconta3;
    }

    /**
     * Setter method for ctbplanoconta3.
     *
     * @param aCtbplanoconta3 the new value for ctbplanoconta3
     */
    public void setCtbplanoconta3(Ctbplanoconta aCtbplanoconta3) {
        ctbplanoconta3 = aCtbplanoconta3;
    }

    /**
     * Access method for ctbplanoconta4.
     *
     * @return the current value of ctbplanoconta4
     */
    public Ctbplanoconta getCtbplanoconta4() {
        return ctbplanoconta4;
    }

    /**
     * Setter method for ctbplanoconta4.
     *
     * @param aCtbplanoconta4 the new value for ctbplanoconta4
     */
    public void setCtbplanoconta4(Ctbplanoconta aCtbplanoconta4) {
        ctbplanoconta4 = aCtbplanoconta4;
    }

    /**
     * Access method for ctbplanoconta5.
     *
     * @return the current value of ctbplanoconta5
     */
    public Ctbplanoconta getCtbplanoconta5() {
        return ctbplanoconta5;
    }

    /**
     * Setter method for ctbplanoconta5.
     *
     * @param aCtbplanoconta5 the new value for ctbplanoconta5
     */
    public void setCtbplanoconta5(Ctbplanoconta aCtbplanoconta5) {
        ctbplanoconta5 = aCtbplanoconta5;
    }

    /**
     * Access method for ctbplanoconta6.
     *
     * @return the current value of ctbplanoconta6
     */
    public Ctbplanoconta getCtbplanoconta6() {
        return ctbplanoconta6;
    }

    /**
     * Setter method for ctbplanoconta6.
     *
     * @param aCtbplanoconta6 the new value for ctbplanoconta6
     */
    public void setCtbplanoconta6(Ctbplanoconta aCtbplanoconta6) {
        ctbplanoconta6 = aCtbplanoconta6;
    }

    /**
     * Compares the key for this instance with another Finjuro.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finjuro and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finjuro)) {
            return false;
        }
        Finjuro that = (Finjuro) other;
        if (this.getCodfinjuro() != that.getCodfinjuro()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finjuro.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finjuro)) return false;
        return this.equalKeys(other) && ((Finjuro)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfinjuro();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finjuro |");
        sb.append(" codfinjuro=").append(getCodfinjuro());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinjuro", Integer.valueOf(getCodfinjuro()));
        return ret;
    }

}
