package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINLANCAMENTO")
public class Finlancamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinlancamento";

    @Id
    @Column(name="CODFINLANCAMENTO", unique=true, nullable=false, precision=10)
    private int codfinlancamento;
    @Column(name="DOCFINLANCAMENTO", nullable=false, length=60)
    private String docfinlancamento;
    @Column(name="DATA")
    private Timestamp data;
    @Column(name="EMISSAODATA", nullable=false)
    private Timestamp emissaodata;
    @Column(name="VENCIMENTODATA", nullable=false)
    private Timestamp vencimentodata;
    @Column(name="LIMITEDATA", nullable=false)
    private Timestamp limitedata;
    @Column(name="VALORDOC", nullable=false, precision=15, scale=4)
    private BigDecimal valordoc;
    @Column(name="VALORPAGO", precision=15, scale=4)
    private BigDecimal valorpago;
    @Column(name="VALORCONC", precision=15, scale=4)
    private BigDecimal valorconc;
    @Column(name="VALORDESCONTO", precision=15, scale=4)
    private BigDecimal valordesconto;
    @Column(name="VALORACRESCIMO", precision=15, scale=4)
    private BigDecimal valoracrescimo;
    @Column(name="VALORCORRIGIDO", precision=15, scale=4)
    private BigDecimal valorcorrigido;
    @Column(name="VALORSEMACRESC", precision=15, scale=4)
    private BigDecimal valorsemacresc;
    @Column(name="PERCJURO", length=15)
    private double percjuro;
    @Column(name="PERCMORA", length=15)
    private double percmora;
    @Column(name="PERCMULTA", length=15)
    private double percmulta;
    @Column(name="PERCDESCONTO", length=15)
    private double percdesconto;
    @Column(name="STATUS", precision=5)
    private short status;
    @Column(name="OBS")
    private String obs;
    @OneToMany(mappedBy="finlancamento")
    private Set<Ctbbemimoapolice> ctbbemimoapolice;
    @OneToMany(mappedBy="finlancamento")
    private Set<Ctbbemoutroapolice> ctbbemoutroapolice;
    @OneToMany(mappedBy="finlancamento")
    private Set<Ctbbemveicapolice> ctbbemveicapolice;
    @OneToMany(mappedBy="finlancamento")
    private Set<Ctbbemveicinfracao> ctbbemveicinfracao;
    @OneToMany(mappedBy="finlancamento")
    private Set<Ctbbemveictacografo> ctbbemveictacografo;
    @OneToMany(mappedBy="finlancamento2")
    private Set<Ctbbemveictacografo> ctbbemveictacografo2;
    @OneToMany(mappedBy="finlancamento")
    private Set<Ctblancamento> ctblancamento;
    @OneToMany(mappedBy="finlancamento")
    private Set<Ecdoclancamento> ecdoclancamento;
    @OneToMany(mappedBy="finlancamento")
    private Set<Finbaixa> finbaixa;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFINTIPO", nullable=false)
    private Fintipo fintipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAO")
    private Optransacao optransacao;
    @ManyToOne
    @JoinColumn(name="CODFINCAIXA")
    private Fincaixa fincaixa;
    @ManyToOne
    @JoinColumn(name="CODFINEMPRESTIMO")
    private Finemprestimo finemprestimo;
    @ManyToOne
    @JoinColumn(name="CODFINEVENTO")
    private Finevento finevento;

    /** Default constructor. */
    public Finlancamento() {
        super();
    }

    /**
     * Access method for codfinlancamento.
     *
     * @return the current value of codfinlancamento
     */
    public int getCodfinlancamento() {
        return codfinlancamento;
    }

    /**
     * Setter method for codfinlancamento.
     *
     * @param aCodfinlancamento the new value for codfinlancamento
     */
    public void setCodfinlancamento(int aCodfinlancamento) {
        codfinlancamento = aCodfinlancamento;
    }

    /**
     * Access method for docfinlancamento.
     *
     * @return the current value of docfinlancamento
     */
    public String getDocfinlancamento() {
        return docfinlancamento;
    }

    /**
     * Setter method for docfinlancamento.
     *
     * @param aDocfinlancamento the new value for docfinlancamento
     */
    public void setDocfinlancamento(String aDocfinlancamento) {
        docfinlancamento = aDocfinlancamento;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for emissaodata.
     *
     * @return the current value of emissaodata
     */
    public Timestamp getEmissaodata() {
        return emissaodata;
    }

    /**
     * Setter method for emissaodata.
     *
     * @param aEmissaodata the new value for emissaodata
     */
    public void setEmissaodata(Timestamp aEmissaodata) {
        emissaodata = aEmissaodata;
    }

    /**
     * Access method for vencimentodata.
     *
     * @return the current value of vencimentodata
     */
    public Timestamp getVencimentodata() {
        return vencimentodata;
    }

    /**
     * Setter method for vencimentodata.
     *
     * @param aVencimentodata the new value for vencimentodata
     */
    public void setVencimentodata(Timestamp aVencimentodata) {
        vencimentodata = aVencimentodata;
    }

    /**
     * Access method for limitedata.
     *
     * @return the current value of limitedata
     */
    public Timestamp getLimitedata() {
        return limitedata;
    }

    /**
     * Setter method for limitedata.
     *
     * @param aLimitedata the new value for limitedata
     */
    public void setLimitedata(Timestamp aLimitedata) {
        limitedata = aLimitedata;
    }

    /**
     * Access method for valordoc.
     *
     * @return the current value of valordoc
     */
    public BigDecimal getValordoc() {
        return valordoc;
    }

    /**
     * Setter method for valordoc.
     *
     * @param aValordoc the new value for valordoc
     */
    public void setValordoc(BigDecimal aValordoc) {
        valordoc = aValordoc;
    }

    /**
     * Access method for valorpago.
     *
     * @return the current value of valorpago
     */
    public BigDecimal getValorpago() {
        return valorpago;
    }

    /**
     * Setter method for valorpago.
     *
     * @param aValorpago the new value for valorpago
     */
    public void setValorpago(BigDecimal aValorpago) {
        valorpago = aValorpago;
    }

    /**
     * Access method for valorconc.
     *
     * @return the current value of valorconc
     */
    public BigDecimal getValorconc() {
        return valorconc;
    }

    /**
     * Setter method for valorconc.
     *
     * @param aValorconc the new value for valorconc
     */
    public void setValorconc(BigDecimal aValorconc) {
        valorconc = aValorconc;
    }

    /**
     * Access method for valordesconto.
     *
     * @return the current value of valordesconto
     */
    public BigDecimal getValordesconto() {
        return valordesconto;
    }

    /**
     * Setter method for valordesconto.
     *
     * @param aValordesconto the new value for valordesconto
     */
    public void setValordesconto(BigDecimal aValordesconto) {
        valordesconto = aValordesconto;
    }

    /**
     * Access method for valoracrescimo.
     *
     * @return the current value of valoracrescimo
     */
    public BigDecimal getValoracrescimo() {
        return valoracrescimo;
    }

    /**
     * Setter method for valoracrescimo.
     *
     * @param aValoracrescimo the new value for valoracrescimo
     */
    public void setValoracrescimo(BigDecimal aValoracrescimo) {
        valoracrescimo = aValoracrescimo;
    }

    /**
     * Access method for valorcorrigido.
     *
     * @return the current value of valorcorrigido
     */
    public BigDecimal getValorcorrigido() {
        return valorcorrigido;
    }

    /**
     * Setter method for valorcorrigido.
     *
     * @param aValorcorrigido the new value for valorcorrigido
     */
    public void setValorcorrigido(BigDecimal aValorcorrigido) {
        valorcorrigido = aValorcorrigido;
    }

    /**
     * Access method for valorsemacresc.
     *
     * @return the current value of valorsemacresc
     */
    public BigDecimal getValorsemacresc() {
        return valorsemacresc;
    }

    /**
     * Setter method for valorsemacresc.
     *
     * @param aValorsemacresc the new value for valorsemacresc
     */
    public void setValorsemacresc(BigDecimal aValorsemacresc) {
        valorsemacresc = aValorsemacresc;
    }

    /**
     * Access method for percjuro.
     *
     * @return the current value of percjuro
     */
    public double getPercjuro() {
        return percjuro;
    }

    /**
     * Setter method for percjuro.
     *
     * @param aPercjuro the new value for percjuro
     */
    public void setPercjuro(double aPercjuro) {
        percjuro = aPercjuro;
    }

    /**
     * Access method for percmora.
     *
     * @return the current value of percmora
     */
    public double getPercmora() {
        return percmora;
    }

    /**
     * Setter method for percmora.
     *
     * @param aPercmora the new value for percmora
     */
    public void setPercmora(double aPercmora) {
        percmora = aPercmora;
    }

    /**
     * Access method for percmulta.
     *
     * @return the current value of percmulta
     */
    public double getPercmulta() {
        return percmulta;
    }

    /**
     * Setter method for percmulta.
     *
     * @param aPercmulta the new value for percmulta
     */
    public void setPercmulta(double aPercmulta) {
        percmulta = aPercmulta;
    }

    /**
     * Access method for percdesconto.
     *
     * @return the current value of percdesconto
     */
    public double getPercdesconto() {
        return percdesconto;
    }

    /**
     * Setter method for percdesconto.
     *
     * @param aPercdesconto the new value for percdesconto
     */
    public void setPercdesconto(double aPercdesconto) {
        percdesconto = aPercdesconto;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public short getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(short aStatus) {
        status = aStatus;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for ctbbemimoapolice.
     *
     * @return the current value of ctbbemimoapolice
     */
    public Set<Ctbbemimoapolice> getCtbbemimoapolice() {
        return ctbbemimoapolice;
    }

    /**
     * Setter method for ctbbemimoapolice.
     *
     * @param aCtbbemimoapolice the new value for ctbbemimoapolice
     */
    public void setCtbbemimoapolice(Set<Ctbbemimoapolice> aCtbbemimoapolice) {
        ctbbemimoapolice = aCtbbemimoapolice;
    }

    /**
     * Access method for ctbbemoutroapolice.
     *
     * @return the current value of ctbbemoutroapolice
     */
    public Set<Ctbbemoutroapolice> getCtbbemoutroapolice() {
        return ctbbemoutroapolice;
    }

    /**
     * Setter method for ctbbemoutroapolice.
     *
     * @param aCtbbemoutroapolice the new value for ctbbemoutroapolice
     */
    public void setCtbbemoutroapolice(Set<Ctbbemoutroapolice> aCtbbemoutroapolice) {
        ctbbemoutroapolice = aCtbbemoutroapolice;
    }

    /**
     * Access method for ctbbemveicapolice.
     *
     * @return the current value of ctbbemveicapolice
     */
    public Set<Ctbbemveicapolice> getCtbbemveicapolice() {
        return ctbbemveicapolice;
    }

    /**
     * Setter method for ctbbemveicapolice.
     *
     * @param aCtbbemveicapolice the new value for ctbbemveicapolice
     */
    public void setCtbbemveicapolice(Set<Ctbbemveicapolice> aCtbbemveicapolice) {
        ctbbemveicapolice = aCtbbemveicapolice;
    }

    /**
     * Access method for ctbbemveicinfracao.
     *
     * @return the current value of ctbbemveicinfracao
     */
    public Set<Ctbbemveicinfracao> getCtbbemveicinfracao() {
        return ctbbemveicinfracao;
    }

    /**
     * Setter method for ctbbemveicinfracao.
     *
     * @param aCtbbemveicinfracao the new value for ctbbemveicinfracao
     */
    public void setCtbbemveicinfracao(Set<Ctbbemveicinfracao> aCtbbemveicinfracao) {
        ctbbemveicinfracao = aCtbbemveicinfracao;
    }

    /**
     * Access method for ctbbemveictacografo.
     *
     * @return the current value of ctbbemveictacografo
     */
    public Set<Ctbbemveictacografo> getCtbbemveictacografo() {
        return ctbbemveictacografo;
    }

    /**
     * Setter method for ctbbemveictacografo.
     *
     * @param aCtbbemveictacografo the new value for ctbbemveictacografo
     */
    public void setCtbbemveictacografo(Set<Ctbbemveictacografo> aCtbbemveictacografo) {
        ctbbemveictacografo = aCtbbemveictacografo;
    }

    /**
     * Access method for ctbbemveictacografo2.
     *
     * @return the current value of ctbbemveictacografo2
     */
    public Set<Ctbbemveictacografo> getCtbbemveictacografo2() {
        return ctbbemveictacografo2;
    }

    /**
     * Setter method for ctbbemveictacografo2.
     *
     * @param aCtbbemveictacografo2 the new value for ctbbemveictacografo2
     */
    public void setCtbbemveictacografo2(Set<Ctbbemveictacografo> aCtbbemveictacografo2) {
        ctbbemveictacografo2 = aCtbbemveictacografo2;
    }

    /**
     * Access method for ctblancamento.
     *
     * @return the current value of ctblancamento
     */
    public Set<Ctblancamento> getCtblancamento() {
        return ctblancamento;
    }

    /**
     * Setter method for ctblancamento.
     *
     * @param aCtblancamento the new value for ctblancamento
     */
    public void setCtblancamento(Set<Ctblancamento> aCtblancamento) {
        ctblancamento = aCtblancamento;
    }

    /**
     * Access method for ecdoclancamento.
     *
     * @return the current value of ecdoclancamento
     */
    public Set<Ecdoclancamento> getEcdoclancamento() {
        return ecdoclancamento;
    }

    /**
     * Setter method for ecdoclancamento.
     *
     * @param aEcdoclancamento the new value for ecdoclancamento
     */
    public void setEcdoclancamento(Set<Ecdoclancamento> aEcdoclancamento) {
        ecdoclancamento = aEcdoclancamento;
    }

    /**
     * Access method for finbaixa.
     *
     * @return the current value of finbaixa
     */
    public Set<Finbaixa> getFinbaixa() {
        return finbaixa;
    }

    /**
     * Setter method for finbaixa.
     *
     * @param aFinbaixa the new value for finbaixa
     */
    public void setFinbaixa(Set<Finbaixa> aFinbaixa) {
        finbaixa = aFinbaixa;
    }

    /**
     * Access method for fintipo.
     *
     * @return the current value of fintipo
     */
    public Fintipo getFintipo() {
        return fintipo;
    }

    /**
     * Setter method for fintipo.
     *
     * @param aFintipo the new value for fintipo
     */
    public void setFintipo(Fintipo aFintipo) {
        fintipo = aFintipo;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for fincaixa.
     *
     * @return the current value of fincaixa
     */
    public Fincaixa getFincaixa() {
        return fincaixa;
    }

    /**
     * Setter method for fincaixa.
     *
     * @param aFincaixa the new value for fincaixa
     */
    public void setFincaixa(Fincaixa aFincaixa) {
        fincaixa = aFincaixa;
    }

    /**
     * Access method for finemprestimo.
     *
     * @return the current value of finemprestimo
     */
    public Finemprestimo getFinemprestimo() {
        return finemprestimo;
    }

    /**
     * Setter method for finemprestimo.
     *
     * @param aFinemprestimo the new value for finemprestimo
     */
    public void setFinemprestimo(Finemprestimo aFinemprestimo) {
        finemprestimo = aFinemprestimo;
    }

    /**
     * Access method for finevento.
     *
     * @return the current value of finevento
     */
    public Finevento getFinevento() {
        return finevento;
    }

    /**
     * Setter method for finevento.
     *
     * @param aFinevento the new value for finevento
     */
    public void setFinevento(Finevento aFinevento) {
        finevento = aFinevento;
    }

    /**
     * Compares the key for this instance with another Finlancamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finlancamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finlancamento)) {
            return false;
        }
        Finlancamento that = (Finlancamento) other;
        if (this.getCodfinlancamento() != that.getCodfinlancamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finlancamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finlancamento)) return false;
        return this.equalKeys(other) && ((Finlancamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfinlancamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finlancamento |");
        sb.append(" codfinlancamento=").append(getCodfinlancamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinlancamento", Integer.valueOf(getCodfinlancamento()));
        return ret;
    }

}
