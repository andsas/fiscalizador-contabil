package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FINLEGENDA")
public class Finlegenda implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinlegenda";

    @Id
    @Column(name="CODFINLEGENDA", unique=true, nullable=false, precision=10)
    private int codfinlegenda;
    @Column(name="LEGCORDOCREGULAR", nullable=false, precision=10)
    private int legcordocregular;
    @Column(name="LEGCORDOCVENCIDO", nullable=false, precision=10)
    private int legcordocvencido;
    @Column(name="LEGCORDOCPARCIAL", nullable=false, precision=10)
    private int legcordocparcial;
    @Column(name="LEGCORDOCNVENCIDO", nullable=false, precision=10)
    private int legcordocnvencido;

    /** Default constructor. */
    public Finlegenda() {
        super();
    }

    /**
     * Access method for codfinlegenda.
     *
     * @return the current value of codfinlegenda
     */
    public int getCodfinlegenda() {
        return codfinlegenda;
    }

    /**
     * Setter method for codfinlegenda.
     *
     * @param aCodfinlegenda the new value for codfinlegenda
     */
    public void setCodfinlegenda(int aCodfinlegenda) {
        codfinlegenda = aCodfinlegenda;
    }

    /**
     * Access method for legcordocregular.
     *
     * @return the current value of legcordocregular
     */
    public int getLegcordocregular() {
        return legcordocregular;
    }

    /**
     * Setter method for legcordocregular.
     *
     * @param aLegcordocregular the new value for legcordocregular
     */
    public void setLegcordocregular(int aLegcordocregular) {
        legcordocregular = aLegcordocregular;
    }

    /**
     * Access method for legcordocvencido.
     *
     * @return the current value of legcordocvencido
     */
    public int getLegcordocvencido() {
        return legcordocvencido;
    }

    /**
     * Setter method for legcordocvencido.
     *
     * @param aLegcordocvencido the new value for legcordocvencido
     */
    public void setLegcordocvencido(int aLegcordocvencido) {
        legcordocvencido = aLegcordocvencido;
    }

    /**
     * Access method for legcordocparcial.
     *
     * @return the current value of legcordocparcial
     */
    public int getLegcordocparcial() {
        return legcordocparcial;
    }

    /**
     * Setter method for legcordocparcial.
     *
     * @param aLegcordocparcial the new value for legcordocparcial
     */
    public void setLegcordocparcial(int aLegcordocparcial) {
        legcordocparcial = aLegcordocparcial;
    }

    /**
     * Access method for legcordocnvencido.
     *
     * @return the current value of legcordocnvencido
     */
    public int getLegcordocnvencido() {
        return legcordocnvencido;
    }

    /**
     * Setter method for legcordocnvencido.
     *
     * @param aLegcordocnvencido the new value for legcordocnvencido
     */
    public void setLegcordocnvencido(int aLegcordocnvencido) {
        legcordocnvencido = aLegcordocnvencido;
    }

    /**
     * Compares the key for this instance with another Finlegenda.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finlegenda and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finlegenda)) {
            return false;
        }
        Finlegenda that = (Finlegenda) other;
        if (this.getCodfinlegenda() != that.getCodfinlegenda()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finlegenda.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finlegenda)) return false;
        return this.equalKeys(other) && ((Finlegenda)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfinlegenda();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finlegenda |");
        sb.append(" codfinlegenda=").append(getCodfinlegenda());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinlegenda", Integer.valueOf(getCodfinlegenda()));
        return ret;
    }

}
