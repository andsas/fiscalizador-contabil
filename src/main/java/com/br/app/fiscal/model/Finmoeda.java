package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINMOEDA")
public class Finmoeda implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinmoeda";

    @Id
    @Column(name="CODFINMOEDA", unique=true, nullable=false, length=20)
    private String codfinmoeda;
    @Column(name="DESCFINMOEDA", nullable=false, length=250)
    private String descfinmoeda;
    @Column(name="COTACAODIARIA", precision=5)
    private short cotacaodiaria;
    @OneToMany(mappedBy="finmoeda")
    private Set<Ctbplanoconta> ctbplanoconta;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFPAIS", nullable=false)
    private Confpais confpais;
    @OneToMany(mappedBy="finmoeda")
    private Set<Finmoedacot> finmoedacot;

    /** Default constructor. */
    public Finmoeda() {
        super();
    }

    /**
     * Access method for codfinmoeda.
     *
     * @return the current value of codfinmoeda
     */
    public String getCodfinmoeda() {
        return codfinmoeda;
    }

    /**
     * Setter method for codfinmoeda.
     *
     * @param aCodfinmoeda the new value for codfinmoeda
     */
    public void setCodfinmoeda(String aCodfinmoeda) {
        codfinmoeda = aCodfinmoeda;
    }

    /**
     * Access method for descfinmoeda.
     *
     * @return the current value of descfinmoeda
     */
    public String getDescfinmoeda() {
        return descfinmoeda;
    }

    /**
     * Setter method for descfinmoeda.
     *
     * @param aDescfinmoeda the new value for descfinmoeda
     */
    public void setDescfinmoeda(String aDescfinmoeda) {
        descfinmoeda = aDescfinmoeda;
    }

    /**
     * Access method for cotacaodiaria.
     *
     * @return the current value of cotacaodiaria
     */
    public short getCotacaodiaria() {
        return cotacaodiaria;
    }

    /**
     * Setter method for cotacaodiaria.
     *
     * @param aCotacaodiaria the new value for cotacaodiaria
     */
    public void setCotacaodiaria(short aCotacaodiaria) {
        cotacaodiaria = aCotacaodiaria;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Set<Ctbplanoconta> getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Set<Ctbplanoconta> aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for confpais.
     *
     * @return the current value of confpais
     */
    public Confpais getConfpais() {
        return confpais;
    }

    /**
     * Setter method for confpais.
     *
     * @param aConfpais the new value for confpais
     */
    public void setConfpais(Confpais aConfpais) {
        confpais = aConfpais;
    }

    /**
     * Access method for finmoedacot.
     *
     * @return the current value of finmoedacot
     */
    public Set<Finmoedacot> getFinmoedacot() {
        return finmoedacot;
    }

    /**
     * Setter method for finmoedacot.
     *
     * @param aFinmoedacot the new value for finmoedacot
     */
    public void setFinmoedacot(Set<Finmoedacot> aFinmoedacot) {
        finmoedacot = aFinmoedacot;
    }

    /**
     * Compares the key for this instance with another Finmoeda.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finmoeda and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finmoeda)) {
            return false;
        }
        Finmoeda that = (Finmoeda) other;
        Object myCodfinmoeda = this.getCodfinmoeda();
        Object yourCodfinmoeda = that.getCodfinmoeda();
        if (myCodfinmoeda==null ? yourCodfinmoeda!=null : !myCodfinmoeda.equals(yourCodfinmoeda)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finmoeda.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finmoeda)) return false;
        return this.equalKeys(other) && ((Finmoeda)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfinmoeda() == null) {
            i = 0;
        } else {
            i = getCodfinmoeda().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finmoeda |");
        sb.append(" codfinmoeda=").append(getCodfinmoeda());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinmoeda", getCodfinmoeda());
        return ret;
    }

}
