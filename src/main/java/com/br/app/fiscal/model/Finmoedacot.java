package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FINMOEDACOT")
public class Finmoedacot implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinmoedacot";

    @Id
    @Column(name="CODFINMOEDACOT", unique=true, nullable=false, precision=10)
    private int codfinmoedacot;
    @Column(name="DATA", nullable=false)
    private Date data;
    @Column(name="COTACAO", nullable=false, precision=15, scale=4)
    private BigDecimal cotacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFINMOEDA", nullable=false)
    private Finmoeda finmoeda;

    /** Default constructor. */
    public Finmoedacot() {
        super();
    }

    /**
     * Access method for codfinmoedacot.
     *
     * @return the current value of codfinmoedacot
     */
    public int getCodfinmoedacot() {
        return codfinmoedacot;
    }

    /**
     * Setter method for codfinmoedacot.
     *
     * @param aCodfinmoedacot the new value for codfinmoedacot
     */
    public void setCodfinmoedacot(int aCodfinmoedacot) {
        codfinmoedacot = aCodfinmoedacot;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Date getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Date aData) {
        data = aData;
    }

    /**
     * Access method for cotacao.
     *
     * @return the current value of cotacao
     */
    public BigDecimal getCotacao() {
        return cotacao;
    }

    /**
     * Setter method for cotacao.
     *
     * @param aCotacao the new value for cotacao
     */
    public void setCotacao(BigDecimal aCotacao) {
        cotacao = aCotacao;
    }

    /**
     * Access method for finmoeda.
     *
     * @return the current value of finmoeda
     */
    public Finmoeda getFinmoeda() {
        return finmoeda;
    }

    /**
     * Setter method for finmoeda.
     *
     * @param aFinmoeda the new value for finmoeda
     */
    public void setFinmoeda(Finmoeda aFinmoeda) {
        finmoeda = aFinmoeda;
    }

    /**
     * Compares the key for this instance with another Finmoedacot.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finmoedacot and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finmoedacot)) {
            return false;
        }
        Finmoedacot that = (Finmoedacot) other;
        if (this.getCodfinmoedacot() != that.getCodfinmoedacot()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finmoedacot.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finmoedacot)) return false;
        return this.equalKeys(other) && ((Finmoedacot)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfinmoedacot();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finmoedacot |");
        sb.append(" codfinmoedacot=").append(getCodfinmoedacot());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinmoedacot", Integer.valueOf(getCodfinmoedacot()));
        return ret;
    }

}
