package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINOPERADORA")
public class Finoperadora implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinoperadora";

    @Id
    @Column(name="CODFINOPERADORA", unique=true, nullable=false, precision=10)
    private int codfinoperadora;
    @Column(name="DATAPAGAMENTO", nullable=false, length=20)
    private String datapagamento;
    @Column(name="PERCENTUALCOBRADO", nullable=false, length=15)
    private double percentualcobrado;
    @Column(name="VALORMENSALIDADE", precision=15, scale=4)
    private BigDecimal valormensalidade;
    @OneToMany(mappedBy="finoperadora")
    private Set<Cobrforma> cobrforma;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAO")
    private Optransacao optransacao;

    /** Default constructor. */
    public Finoperadora() {
        super();
    }

    /**
     * Access method for codfinoperadora.
     *
     * @return the current value of codfinoperadora
     */
    public int getCodfinoperadora() {
        return codfinoperadora;
    }

    /**
     * Setter method for codfinoperadora.
     *
     * @param aCodfinoperadora the new value for codfinoperadora
     */
    public void setCodfinoperadora(int aCodfinoperadora) {
        codfinoperadora = aCodfinoperadora;
    }

    /**
     * Access method for datapagamento.
     *
     * @return the current value of datapagamento
     */
    public String getDatapagamento() {
        return datapagamento;
    }

    /**
     * Setter method for datapagamento.
     *
     * @param aDatapagamento the new value for datapagamento
     */
    public void setDatapagamento(String aDatapagamento) {
        datapagamento = aDatapagamento;
    }

    /**
     * Access method for percentualcobrado.
     *
     * @return the current value of percentualcobrado
     */
    public double getPercentualcobrado() {
        return percentualcobrado;
    }

    /**
     * Setter method for percentualcobrado.
     *
     * @param aPercentualcobrado the new value for percentualcobrado
     */
    public void setPercentualcobrado(double aPercentualcobrado) {
        percentualcobrado = aPercentualcobrado;
    }

    /**
     * Access method for valormensalidade.
     *
     * @return the current value of valormensalidade
     */
    public BigDecimal getValormensalidade() {
        return valormensalidade;
    }

    /**
     * Setter method for valormensalidade.
     *
     * @param aValormensalidade the new value for valormensalidade
     */
    public void setValormensalidade(BigDecimal aValormensalidade) {
        valormensalidade = aValormensalidade;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Set<Cobrforma> getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Set<Cobrforma> aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Compares the key for this instance with another Finoperadora.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finoperadora and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finoperadora)) {
            return false;
        }
        Finoperadora that = (Finoperadora) other;
        if (this.getCodfinoperadora() != that.getCodfinoperadora()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finoperadora.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finoperadora)) return false;
        return this.equalKeys(other) && ((Finoperadora)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfinoperadora();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finoperadora |");
        sb.append(" codfinoperadora=").append(getCodfinoperadora());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinoperadora", Integer.valueOf(getCodfinoperadora()));
        return ret;
    }

}
