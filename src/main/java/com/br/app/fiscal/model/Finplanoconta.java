package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FINPLANOCONTA")
public class Finplanoconta implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfinplanoconta";

    @Id
    @Column(name="CODFINPLANOCONTA", unique=true, nullable=false, length=20)
    private String codfinplanoconta;
    @Column(name="DESCFINPLANOCONTA", nullable=false, length=250)
    private String descfinplanoconta;
    @Column(name="NIVEL", precision=10)
    private int nivel;
    @Column(name="DRE", precision=5)
    private short dre;
    @Column(name="DLFC", precision=5)
    private short dlfc;
    @Column(name="CODREDUZIDO", precision=10)
    private int codreduzido;
    @Column(name="DEBCRED", precision=5)
    private short debcred;
    @OneToMany(mappedBy="finplanoconta")
    private Set<Agagente> agagente;
    @OneToMany(mappedBy="finplanoconta2")
    private Set<Agagente> agagente2;
    @OneToMany(mappedBy="finplanoconta")
    private Set<Cobrforma> cobrforma;
    @OneToMany(mappedBy="finplanoconta2")
    private Set<Cobrforma> cobrforma2;
    @OneToMany(mappedBy="finplanoconta")
    private Set<Finbaixa> finbaixa;
    @OneToMany(mappedBy="finplanoconta")
    private Set<Fincaixa> fincaixa;
    @OneToMany(mappedBy="finplanoconta2")
    private Set<Fincaixa> fincaixa2;
    @OneToMany(mappedBy="finplanoconta3")
    private Set<Finplanoconta> finplanoconta4;
    @ManyToOne
    @JoinColumn(name="CODPARENTFINPLANOCONTA")
    private Finplanoconta finplanoconta3;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @OneToMany(mappedBy="finplanoconta")
    private Set<Folevento> folevento;
    @OneToMany(mappedBy="finplanoconta2")
    private Set<Folevento> folevento2;
    @OneToMany(mappedBy="finplanoconta")
    private Set<Impplanofiscal> impplanofiscal;

    /** Default constructor. */
    public Finplanoconta() {
        super();
    }

    /**
     * Access method for codfinplanoconta.
     *
     * @return the current value of codfinplanoconta
     */
    public String getCodfinplanoconta() {
        return codfinplanoconta;
    }

    /**
     * Setter method for codfinplanoconta.
     *
     * @param aCodfinplanoconta the new value for codfinplanoconta
     */
    public void setCodfinplanoconta(String aCodfinplanoconta) {
        codfinplanoconta = aCodfinplanoconta;
    }

    /**
     * Access method for descfinplanoconta.
     *
     * @return the current value of descfinplanoconta
     */
    public String getDescfinplanoconta() {
        return descfinplanoconta;
    }

    /**
     * Setter method for descfinplanoconta.
     *
     * @param aDescfinplanoconta the new value for descfinplanoconta
     */
    public void setDescfinplanoconta(String aDescfinplanoconta) {
        descfinplanoconta = aDescfinplanoconta;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(int aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for dre.
     *
     * @return the current value of dre
     */
    public short getDre() {
        return dre;
    }

    /**
     * Setter method for dre.
     *
     * @param aDre the new value for dre
     */
    public void setDre(short aDre) {
        dre = aDre;
    }

    /**
     * Access method for dlfc.
     *
     * @return the current value of dlfc
     */
    public short getDlfc() {
        return dlfc;
    }

    /**
     * Setter method for dlfc.
     *
     * @param aDlfc the new value for dlfc
     */
    public void setDlfc(short aDlfc) {
        dlfc = aDlfc;
    }

    /**
     * Access method for codreduzido.
     *
     * @return the current value of codreduzido
     */
    public int getCodreduzido() {
        return codreduzido;
    }

    /**
     * Setter method for codreduzido.
     *
     * @param aCodreduzido the new value for codreduzido
     */
    public void setCodreduzido(int aCodreduzido) {
        codreduzido = aCodreduzido;
    }

    /**
     * Access method for debcred.
     *
     * @return the current value of debcred
     */
    public short getDebcred() {
        return debcred;
    }

    /**
     * Setter method for debcred.
     *
     * @param aDebcred the new value for debcred
     */
    public void setDebcred(short aDebcred) {
        debcred = aDebcred;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Set<Agagente> getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Set<Agagente> aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Set<Cobrforma> getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Set<Cobrforma> aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Access method for cobrforma2.
     *
     * @return the current value of cobrforma2
     */
    public Set<Cobrforma> getCobrforma2() {
        return cobrforma2;
    }

    /**
     * Setter method for cobrforma2.
     *
     * @param aCobrforma2 the new value for cobrforma2
     */
    public void setCobrforma2(Set<Cobrforma> aCobrforma2) {
        cobrforma2 = aCobrforma2;
    }

    /**
     * Access method for finbaixa.
     *
     * @return the current value of finbaixa
     */
    public Set<Finbaixa> getFinbaixa() {
        return finbaixa;
    }

    /**
     * Setter method for finbaixa.
     *
     * @param aFinbaixa the new value for finbaixa
     */
    public void setFinbaixa(Set<Finbaixa> aFinbaixa) {
        finbaixa = aFinbaixa;
    }

    /**
     * Access method for fincaixa.
     *
     * @return the current value of fincaixa
     */
    public Set<Fincaixa> getFincaixa() {
        return fincaixa;
    }

    /**
     * Setter method for fincaixa.
     *
     * @param aFincaixa the new value for fincaixa
     */
    public void setFincaixa(Set<Fincaixa> aFincaixa) {
        fincaixa = aFincaixa;
    }

    /**
     * Access method for fincaixa2.
     *
     * @return the current value of fincaixa2
     */
    public Set<Fincaixa> getFincaixa2() {
        return fincaixa2;
    }

    /**
     * Setter method for fincaixa2.
     *
     * @param aFincaixa2 the new value for fincaixa2
     */
    public void setFincaixa2(Set<Fincaixa> aFincaixa2) {
        fincaixa2 = aFincaixa2;
    }

    /**
     * Access method for finplanoconta4.
     *
     * @return the current value of finplanoconta4
     */
    public Set<Finplanoconta> getFinplanoconta4() {
        return finplanoconta4;
    }

    /**
     * Setter method for finplanoconta4.
     *
     * @param aFinplanoconta4 the new value for finplanoconta4
     */
    public void setFinplanoconta4(Set<Finplanoconta> aFinplanoconta4) {
        finplanoconta4 = aFinplanoconta4;
    }

    /**
     * Access method for finplanoconta3.
     *
     * @return the current value of finplanoconta3
     */
    public Finplanoconta getFinplanoconta3() {
        return finplanoconta3;
    }

    /**
     * Setter method for finplanoconta3.
     *
     * @param aFinplanoconta3 the new value for finplanoconta3
     */
    public void setFinplanoconta3(Finplanoconta aFinplanoconta3) {
        finplanoconta3 = aFinplanoconta3;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for folevento.
     *
     * @return the current value of folevento
     */
    public Set<Folevento> getFolevento() {
        return folevento;
    }

    /**
     * Setter method for folevento.
     *
     * @param aFolevento the new value for folevento
     */
    public void setFolevento(Set<Folevento> aFolevento) {
        folevento = aFolevento;
    }

    /**
     * Access method for folevento2.
     *
     * @return the current value of folevento2
     */
    public Set<Folevento> getFolevento2() {
        return folevento2;
    }

    /**
     * Setter method for folevento2.
     *
     * @param aFolevento2 the new value for folevento2
     */
    public void setFolevento2(Set<Folevento> aFolevento2) {
        folevento2 = aFolevento2;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Compares the key for this instance with another Finplanoconta.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Finplanoconta and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Finplanoconta)) {
            return false;
        }
        Finplanoconta that = (Finplanoconta) other;
        Object myCodfinplanoconta = this.getCodfinplanoconta();
        Object yourCodfinplanoconta = that.getCodfinplanoconta();
        if (myCodfinplanoconta==null ? yourCodfinplanoconta!=null : !myCodfinplanoconta.equals(yourCodfinplanoconta)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Finplanoconta.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Finplanoconta)) return false;
        return this.equalKeys(other) && ((Finplanoconta)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfinplanoconta() == null) {
            i = 0;
        } else {
            i = getCodfinplanoconta().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Finplanoconta |");
        sb.append(" codfinplanoconta=").append(getCodfinplanoconta());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfinplanoconta", getCodfinplanoconta());
        return ret;
    }

}
