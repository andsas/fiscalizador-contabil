package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FINTIPO")
public class Fintipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfintipo";

    @Id
    @Column(name="CODFINTIPO", unique=true, nullable=false, precision=10)
    private int codfintipo;
    @Column(name="DESCFINTIPO", nullable=false, length=250)
    private String descfintipo;
    @Column(name="TIPOFINTIPO", nullable=false, precision=5)
    private short tipofintipo;
    @Column(name="LEGENDA", precision=10)
    private int legenda;
    @OneToMany(mappedBy="fintipo")
    private Set<Ctbplanoconta> ctbplanoconta;
    @OneToMany(mappedBy="fintipo")
    private Set<Finconf> finconf;
    @OneToMany(mappedBy="fintipo2")
    private Set<Finconf> finconf2;
    @OneToMany(mappedBy="fintipo3")
    private Set<Finconf> finconf3;
    @OneToMany(mappedBy="fintipo")
    private Set<Finevento> finevento;
    @OneToMany(mappedBy="fintipo")
    private Set<Finlancamento> finlancamento;
    @OneToMany(mappedBy="fintipo")
    private Set<Folevento> folevento;
    @OneToMany(mappedBy="fintipo")
    private Set<Optipo> optipo;
    @OneToMany(mappedBy="fintipo2")
    private Set<Optipo> optipo2;

    /** Default constructor. */
    public Fintipo() {
        super();
    }

    /**
     * Access method for codfintipo.
     *
     * @return the current value of codfintipo
     */
    public int getCodfintipo() {
        return codfintipo;
    }

    /**
     * Setter method for codfintipo.
     *
     * @param aCodfintipo the new value for codfintipo
     */
    public void setCodfintipo(int aCodfintipo) {
        codfintipo = aCodfintipo;
    }

    /**
     * Access method for descfintipo.
     *
     * @return the current value of descfintipo
     */
    public String getDescfintipo() {
        return descfintipo;
    }

    /**
     * Setter method for descfintipo.
     *
     * @param aDescfintipo the new value for descfintipo
     */
    public void setDescfintipo(String aDescfintipo) {
        descfintipo = aDescfintipo;
    }

    /**
     * Access method for tipofintipo.
     *
     * @return the current value of tipofintipo
     */
    public short getTipofintipo() {
        return tipofintipo;
    }

    /**
     * Setter method for tipofintipo.
     *
     * @param aTipofintipo the new value for tipofintipo
     */
    public void setTipofintipo(short aTipofintipo) {
        tipofintipo = aTipofintipo;
    }

    /**
     * Access method for legenda.
     *
     * @return the current value of legenda
     */
    public int getLegenda() {
        return legenda;
    }

    /**
     * Setter method for legenda.
     *
     * @param aLegenda the new value for legenda
     */
    public void setLegenda(int aLegenda) {
        legenda = aLegenda;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Set<Ctbplanoconta> getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Set<Ctbplanoconta> aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for finconf.
     *
     * @return the current value of finconf
     */
    public Set<Finconf> getFinconf() {
        return finconf;
    }

    /**
     * Setter method for finconf.
     *
     * @param aFinconf the new value for finconf
     */
    public void setFinconf(Set<Finconf> aFinconf) {
        finconf = aFinconf;
    }

    /**
     * Access method for finconf2.
     *
     * @return the current value of finconf2
     */
    public Set<Finconf> getFinconf2() {
        return finconf2;
    }

    /**
     * Setter method for finconf2.
     *
     * @param aFinconf2 the new value for finconf2
     */
    public void setFinconf2(Set<Finconf> aFinconf2) {
        finconf2 = aFinconf2;
    }

    /**
     * Access method for finconf3.
     *
     * @return the current value of finconf3
     */
    public Set<Finconf> getFinconf3() {
        return finconf3;
    }

    /**
     * Setter method for finconf3.
     *
     * @param aFinconf3 the new value for finconf3
     */
    public void setFinconf3(Set<Finconf> aFinconf3) {
        finconf3 = aFinconf3;
    }

    /**
     * Access method for finevento.
     *
     * @return the current value of finevento
     */
    public Set<Finevento> getFinevento() {
        return finevento;
    }

    /**
     * Setter method for finevento.
     *
     * @param aFinevento the new value for finevento
     */
    public void setFinevento(Set<Finevento> aFinevento) {
        finevento = aFinevento;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Set<Finlancamento> getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Set<Finlancamento> aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for folevento.
     *
     * @return the current value of folevento
     */
    public Set<Folevento> getFolevento() {
        return folevento;
    }

    /**
     * Setter method for folevento.
     *
     * @param aFolevento the new value for folevento
     */
    public void setFolevento(Set<Folevento> aFolevento) {
        folevento = aFolevento;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Set<Optipo> getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Set<Optipo> aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Access method for optipo2.
     *
     * @return the current value of optipo2
     */
    public Set<Optipo> getOptipo2() {
        return optipo2;
    }

    /**
     * Setter method for optipo2.
     *
     * @param aOptipo2 the new value for optipo2
     */
    public void setOptipo2(Set<Optipo> aOptipo2) {
        optipo2 = aOptipo2;
    }

    /**
     * Compares the key for this instance with another Fintipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Fintipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Fintipo)) {
            return false;
        }
        Fintipo that = (Fintipo) other;
        if (this.getCodfintipo() != that.getCodfintipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Fintipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Fintipo)) return false;
        return this.equalKeys(other) && ((Fintipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfintipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Fintipo |");
        sb.append(" codfintipo=").append(getCodfintipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfintipo", Integer.valueOf(getCodfintipo()));
        return ret;
    }

}
