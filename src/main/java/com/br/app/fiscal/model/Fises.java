package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FISES")
public class Fises implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfises";

    @Id
    @Column(name="CODFISES", unique=true, nullable=false, precision=10)
    private int codfises;
    @Column(name="TIPOFISES", nullable=false, precision=5)
    private short tipofises;
    @Column(name="DOCESPECIE", length=20)
    private String docespecie;
    @Column(name="DOCNUMERO", precision=10)
    private int docnumero;
    @Column(name="DOCSERIE", length=20)
    private String docserie;
    @Column(name="DATAEMISSAO")
    private Timestamp dataemissao;
    @Column(name="DATATRANSACAO")
    private Timestamp datatransacao;
    @Column(name="TOTFRETE", precision=15, scale=4)
    private BigDecimal totfrete;
    @Column(name="TOTSEGURO", precision=15, scale=4)
    private BigDecimal totseguro;
    @Column(name="TOTDESCONTO", precision=15, scale=4)
    private BigDecimal totdesconto;
    @Column(name="TOTDESPESASAC", precision=15, scale=4)
    private BigDecimal totdespesasac;
    @Column(name="TOTPRODUTO", precision=15, scale=4)
    private BigDecimal totproduto;
    @Column(name="TOTNF", precision=15, scale=4)
    private BigDecimal totnf;
    @Column(name="BASEICMS", precision=15, scale=4)
    private BigDecimal baseicms;
    @Column(name="ALIQICMS", precision=15, scale=4)
    private BigDecimal aliqicms;
    @Column(name="IMPOSTOICMS", precision=15, scale=4)
    private BigDecimal impostoicms;
    @Column(name="BASESTICMS", precision=15, scale=4)
    private BigDecimal basesticms;
    @Column(name="IMPOSTOMVA", precision=15, scale=4)
    private BigDecimal impostomva;
    @Column(name="ALIQSTICMS", precision=15, scale=4)
    private BigDecimal aliqsticms;
    @Column(name="IMPOSTOSTICMS", precision=15, scale=4)
    private BigDecimal impostosticms;
    @Column(name="BASEIPI", precision=15, scale=4)
    private BigDecimal baseipi;
    @Column(name="ALIQIPI", precision=15, scale=4)
    private BigDecimal aliqipi;
    @Column(name="IMPOSTOIPI", precision=15, scale=4)
    private BigDecimal impostoipi;
    @Column(name="BASEPIS", precision=15, scale=4)
    private BigDecimal basepis;
    @Column(name="ALIQPIS", precision=15, scale=4)
    private BigDecimal aliqpis;
    @Column(name="IMPOSTOPIS", precision=15, scale=4)
    private BigDecimal impostopis;
    @Column(name="BASECOFINS", precision=15, scale=4)
    private BigDecimal basecofins;
    @Column(name="ALIQCOFINS", precision=15, scale=4)
    private BigDecimal aliqcofins;
    @Column(name="IMPOSTOCOFINS", precision=15, scale=4)
    private BigDecimal impostocofins;
    @Column(name="BASEIRPJ", precision=15, scale=4)
    private BigDecimal baseirpj;
    @Column(name="ALIQIRPJ", precision=15, scale=4)
    private BigDecimal aliqirpj;
    @Column(name="IMPOSTOIRPJ", precision=15, scale=4)
    private BigDecimal impostoirpj;
    @Column(name="BASECSLL", precision=15, scale=4)
    private BigDecimal basecsll;
    @Column(name="ALIQCSLL", precision=15, scale=4)
    private BigDecimal aliqcsll;
    @Column(name="IMPOSTOCSLL", precision=15, scale=4)
    private BigDecimal impostocsll;
    @Column(name="BASEISSQN", precision=15, scale=4)
    private BigDecimal baseissqn;
    @Column(name="ALIQISSQN", precision=15, scale=4)
    private BigDecimal aliqissqn;
    @Column(name="IMPOSTOISSQN", precision=15, scale=4)
    private BigDecimal impostoissqn;
    @Column(name="BASESIMPLES", precision=15, scale=4)
    private BigDecimal basesimples;
    @Column(name="ALIQSIMPLES", precision=15, scale=4)
    private BigDecimal aliqsimples;
    @Column(name="IMPOSTOSIMPLES", precision=15, scale=4)
    private BigDecimal impostosimples;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAO", nullable=false)
    private Optransacao optransacao;
    @ManyToOne
    @JoinColumn(name="CODAGAGENTE")
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODOPOPERACAO")
    private Opoperacao opoperacao;
    @OneToMany(mappedBy="fises")
    private Set<Fisesdet> fisesdet;

    /** Default constructor. */
    public Fises() {
        super();
    }

    /**
     * Access method for codfises.
     *
     * @return the current value of codfises
     */
    public int getCodfises() {
        return codfises;
    }

    /**
     * Setter method for codfises.
     *
     * @param aCodfises the new value for codfises
     */
    public void setCodfises(int aCodfises) {
        codfises = aCodfises;
    }

    /**
     * Access method for tipofises.
     *
     * @return the current value of tipofises
     */
    public short getTipofises() {
        return tipofises;
    }

    /**
     * Setter method for tipofises.
     *
     * @param aTipofises the new value for tipofises
     */
    public void setTipofises(short aTipofises) {
        tipofises = aTipofises;
    }

    /**
     * Access method for docespecie.
     *
     * @return the current value of docespecie
     */
    public String getDocespecie() {
        return docespecie;
    }

    /**
     * Setter method for docespecie.
     *
     * @param aDocespecie the new value for docespecie
     */
    public void setDocespecie(String aDocespecie) {
        docespecie = aDocespecie;
    }

    /**
     * Access method for docnumero.
     *
     * @return the current value of docnumero
     */
    public int getDocnumero() {
        return docnumero;
    }

    /**
     * Setter method for docnumero.
     *
     * @param aDocnumero the new value for docnumero
     */
    public void setDocnumero(int aDocnumero) {
        docnumero = aDocnumero;
    }

    /**
     * Access method for docserie.
     *
     * @return the current value of docserie
     */
    public String getDocserie() {
        return docserie;
    }

    /**
     * Setter method for docserie.
     *
     * @param aDocserie the new value for docserie
     */
    public void setDocserie(String aDocserie) {
        docserie = aDocserie;
    }

    /**
     * Access method for dataemissao.
     *
     * @return the current value of dataemissao
     */
    public Timestamp getDataemissao() {
        return dataemissao;
    }

    /**
     * Setter method for dataemissao.
     *
     * @param aDataemissao the new value for dataemissao
     */
    public void setDataemissao(Timestamp aDataemissao) {
        dataemissao = aDataemissao;
    }

    /**
     * Access method for datatransacao.
     *
     * @return the current value of datatransacao
     */
    public Timestamp getDatatransacao() {
        return datatransacao;
    }

    /**
     * Setter method for datatransacao.
     *
     * @param aDatatransacao the new value for datatransacao
     */
    public void setDatatransacao(Timestamp aDatatransacao) {
        datatransacao = aDatatransacao;
    }

    /**
     * Access method for totfrete.
     *
     * @return the current value of totfrete
     */
    public BigDecimal getTotfrete() {
        return totfrete;
    }

    /**
     * Setter method for totfrete.
     *
     * @param aTotfrete the new value for totfrete
     */
    public void setTotfrete(BigDecimal aTotfrete) {
        totfrete = aTotfrete;
    }

    /**
     * Access method for totseguro.
     *
     * @return the current value of totseguro
     */
    public BigDecimal getTotseguro() {
        return totseguro;
    }

    /**
     * Setter method for totseguro.
     *
     * @param aTotseguro the new value for totseguro
     */
    public void setTotseguro(BigDecimal aTotseguro) {
        totseguro = aTotseguro;
    }

    /**
     * Access method for totdesconto.
     *
     * @return the current value of totdesconto
     */
    public BigDecimal getTotdesconto() {
        return totdesconto;
    }

    /**
     * Setter method for totdesconto.
     *
     * @param aTotdesconto the new value for totdesconto
     */
    public void setTotdesconto(BigDecimal aTotdesconto) {
        totdesconto = aTotdesconto;
    }

    /**
     * Access method for totdespesasac.
     *
     * @return the current value of totdespesasac
     */
    public BigDecimal getTotdespesasac() {
        return totdespesasac;
    }

    /**
     * Setter method for totdespesasac.
     *
     * @param aTotdespesasac the new value for totdespesasac
     */
    public void setTotdespesasac(BigDecimal aTotdespesasac) {
        totdespesasac = aTotdespesasac;
    }

    /**
     * Access method for totproduto.
     *
     * @return the current value of totproduto
     */
    public BigDecimal getTotproduto() {
        return totproduto;
    }

    /**
     * Setter method for totproduto.
     *
     * @param aTotproduto the new value for totproduto
     */
    public void setTotproduto(BigDecimal aTotproduto) {
        totproduto = aTotproduto;
    }

    /**
     * Access method for totnf.
     *
     * @return the current value of totnf
     */
    public BigDecimal getTotnf() {
        return totnf;
    }

    /**
     * Setter method for totnf.
     *
     * @param aTotnf the new value for totnf
     */
    public void setTotnf(BigDecimal aTotnf) {
        totnf = aTotnf;
    }

    /**
     * Access method for baseicms.
     *
     * @return the current value of baseicms
     */
    public BigDecimal getBaseicms() {
        return baseicms;
    }

    /**
     * Setter method for baseicms.
     *
     * @param aBaseicms the new value for baseicms
     */
    public void setBaseicms(BigDecimal aBaseicms) {
        baseicms = aBaseicms;
    }

    /**
     * Access method for aliqicms.
     *
     * @return the current value of aliqicms
     */
    public BigDecimal getAliqicms() {
        return aliqicms;
    }

    /**
     * Setter method for aliqicms.
     *
     * @param aAliqicms the new value for aliqicms
     */
    public void setAliqicms(BigDecimal aAliqicms) {
        aliqicms = aAliqicms;
    }

    /**
     * Access method for impostoicms.
     *
     * @return the current value of impostoicms
     */
    public BigDecimal getImpostoicms() {
        return impostoicms;
    }

    /**
     * Setter method for impostoicms.
     *
     * @param aImpostoicms the new value for impostoicms
     */
    public void setImpostoicms(BigDecimal aImpostoicms) {
        impostoicms = aImpostoicms;
    }

    /**
     * Access method for basesticms.
     *
     * @return the current value of basesticms
     */
    public BigDecimal getBasesticms() {
        return basesticms;
    }

    /**
     * Setter method for basesticms.
     *
     * @param aBasesticms the new value for basesticms
     */
    public void setBasesticms(BigDecimal aBasesticms) {
        basesticms = aBasesticms;
    }

    /**
     * Access method for impostomva.
     *
     * @return the current value of impostomva
     */
    public BigDecimal getImpostomva() {
        return impostomva;
    }

    /**
     * Setter method for impostomva.
     *
     * @param aImpostomva the new value for impostomva
     */
    public void setImpostomva(BigDecimal aImpostomva) {
        impostomva = aImpostomva;
    }

    /**
     * Access method for aliqsticms.
     *
     * @return the current value of aliqsticms
     */
    public BigDecimal getAliqsticms() {
        return aliqsticms;
    }

    /**
     * Setter method for aliqsticms.
     *
     * @param aAliqsticms the new value for aliqsticms
     */
    public void setAliqsticms(BigDecimal aAliqsticms) {
        aliqsticms = aAliqsticms;
    }

    /**
     * Access method for impostosticms.
     *
     * @return the current value of impostosticms
     */
    public BigDecimal getImpostosticms() {
        return impostosticms;
    }

    /**
     * Setter method for impostosticms.
     *
     * @param aImpostosticms the new value for impostosticms
     */
    public void setImpostosticms(BigDecimal aImpostosticms) {
        impostosticms = aImpostosticms;
    }

    /**
     * Access method for baseipi.
     *
     * @return the current value of baseipi
     */
    public BigDecimal getBaseipi() {
        return baseipi;
    }

    /**
     * Setter method for baseipi.
     *
     * @param aBaseipi the new value for baseipi
     */
    public void setBaseipi(BigDecimal aBaseipi) {
        baseipi = aBaseipi;
    }

    /**
     * Access method for aliqipi.
     *
     * @return the current value of aliqipi
     */
    public BigDecimal getAliqipi() {
        return aliqipi;
    }

    /**
     * Setter method for aliqipi.
     *
     * @param aAliqipi the new value for aliqipi
     */
    public void setAliqipi(BigDecimal aAliqipi) {
        aliqipi = aAliqipi;
    }

    /**
     * Access method for impostoipi.
     *
     * @return the current value of impostoipi
     */
    public BigDecimal getImpostoipi() {
        return impostoipi;
    }

    /**
     * Setter method for impostoipi.
     *
     * @param aImpostoipi the new value for impostoipi
     */
    public void setImpostoipi(BigDecimal aImpostoipi) {
        impostoipi = aImpostoipi;
    }

    /**
     * Access method for basepis.
     *
     * @return the current value of basepis
     */
    public BigDecimal getBasepis() {
        return basepis;
    }

    /**
     * Setter method for basepis.
     *
     * @param aBasepis the new value for basepis
     */
    public void setBasepis(BigDecimal aBasepis) {
        basepis = aBasepis;
    }

    /**
     * Access method for aliqpis.
     *
     * @return the current value of aliqpis
     */
    public BigDecimal getAliqpis() {
        return aliqpis;
    }

    /**
     * Setter method for aliqpis.
     *
     * @param aAliqpis the new value for aliqpis
     */
    public void setAliqpis(BigDecimal aAliqpis) {
        aliqpis = aAliqpis;
    }

    /**
     * Access method for impostopis.
     *
     * @return the current value of impostopis
     */
    public BigDecimal getImpostopis() {
        return impostopis;
    }

    /**
     * Setter method for impostopis.
     *
     * @param aImpostopis the new value for impostopis
     */
    public void setImpostopis(BigDecimal aImpostopis) {
        impostopis = aImpostopis;
    }

    /**
     * Access method for basecofins.
     *
     * @return the current value of basecofins
     */
    public BigDecimal getBasecofins() {
        return basecofins;
    }

    /**
     * Setter method for basecofins.
     *
     * @param aBasecofins the new value for basecofins
     */
    public void setBasecofins(BigDecimal aBasecofins) {
        basecofins = aBasecofins;
    }

    /**
     * Access method for aliqcofins.
     *
     * @return the current value of aliqcofins
     */
    public BigDecimal getAliqcofins() {
        return aliqcofins;
    }

    /**
     * Setter method for aliqcofins.
     *
     * @param aAliqcofins the new value for aliqcofins
     */
    public void setAliqcofins(BigDecimal aAliqcofins) {
        aliqcofins = aAliqcofins;
    }

    /**
     * Access method for impostocofins.
     *
     * @return the current value of impostocofins
     */
    public BigDecimal getImpostocofins() {
        return impostocofins;
    }

    /**
     * Setter method for impostocofins.
     *
     * @param aImpostocofins the new value for impostocofins
     */
    public void setImpostocofins(BigDecimal aImpostocofins) {
        impostocofins = aImpostocofins;
    }

    /**
     * Access method for baseirpj.
     *
     * @return the current value of baseirpj
     */
    public BigDecimal getBaseirpj() {
        return baseirpj;
    }

    /**
     * Setter method for baseirpj.
     *
     * @param aBaseirpj the new value for baseirpj
     */
    public void setBaseirpj(BigDecimal aBaseirpj) {
        baseirpj = aBaseirpj;
    }

    /**
     * Access method for aliqirpj.
     *
     * @return the current value of aliqirpj
     */
    public BigDecimal getAliqirpj() {
        return aliqirpj;
    }

    /**
     * Setter method for aliqirpj.
     *
     * @param aAliqirpj the new value for aliqirpj
     */
    public void setAliqirpj(BigDecimal aAliqirpj) {
        aliqirpj = aAliqirpj;
    }

    /**
     * Access method for impostoirpj.
     *
     * @return the current value of impostoirpj
     */
    public BigDecimal getImpostoirpj() {
        return impostoirpj;
    }

    /**
     * Setter method for impostoirpj.
     *
     * @param aImpostoirpj the new value for impostoirpj
     */
    public void setImpostoirpj(BigDecimal aImpostoirpj) {
        impostoirpj = aImpostoirpj;
    }

    /**
     * Access method for basecsll.
     *
     * @return the current value of basecsll
     */
    public BigDecimal getBasecsll() {
        return basecsll;
    }

    /**
     * Setter method for basecsll.
     *
     * @param aBasecsll the new value for basecsll
     */
    public void setBasecsll(BigDecimal aBasecsll) {
        basecsll = aBasecsll;
    }

    /**
     * Access method for aliqcsll.
     *
     * @return the current value of aliqcsll
     */
    public BigDecimal getAliqcsll() {
        return aliqcsll;
    }

    /**
     * Setter method for aliqcsll.
     *
     * @param aAliqcsll the new value for aliqcsll
     */
    public void setAliqcsll(BigDecimal aAliqcsll) {
        aliqcsll = aAliqcsll;
    }

    /**
     * Access method for impostocsll.
     *
     * @return the current value of impostocsll
     */
    public BigDecimal getImpostocsll() {
        return impostocsll;
    }

    /**
     * Setter method for impostocsll.
     *
     * @param aImpostocsll the new value for impostocsll
     */
    public void setImpostocsll(BigDecimal aImpostocsll) {
        impostocsll = aImpostocsll;
    }

    /**
     * Access method for baseissqn.
     *
     * @return the current value of baseissqn
     */
    public BigDecimal getBaseissqn() {
        return baseissqn;
    }

    /**
     * Setter method for baseissqn.
     *
     * @param aBaseissqn the new value for baseissqn
     */
    public void setBaseissqn(BigDecimal aBaseissqn) {
        baseissqn = aBaseissqn;
    }

    /**
     * Access method for aliqissqn.
     *
     * @return the current value of aliqissqn
     */
    public BigDecimal getAliqissqn() {
        return aliqissqn;
    }

    /**
     * Setter method for aliqissqn.
     *
     * @param aAliqissqn the new value for aliqissqn
     */
    public void setAliqissqn(BigDecimal aAliqissqn) {
        aliqissqn = aAliqissqn;
    }

    /**
     * Access method for impostoissqn.
     *
     * @return the current value of impostoissqn
     */
    public BigDecimal getImpostoissqn() {
        return impostoissqn;
    }

    /**
     * Setter method for impostoissqn.
     *
     * @param aImpostoissqn the new value for impostoissqn
     */
    public void setImpostoissqn(BigDecimal aImpostoissqn) {
        impostoissqn = aImpostoissqn;
    }

    /**
     * Access method for basesimples.
     *
     * @return the current value of basesimples
     */
    public BigDecimal getBasesimples() {
        return basesimples;
    }

    /**
     * Setter method for basesimples.
     *
     * @param aBasesimples the new value for basesimples
     */
    public void setBasesimples(BigDecimal aBasesimples) {
        basesimples = aBasesimples;
    }

    /**
     * Access method for aliqsimples.
     *
     * @return the current value of aliqsimples
     */
    public BigDecimal getAliqsimples() {
        return aliqsimples;
    }

    /**
     * Setter method for aliqsimples.
     *
     * @param aAliqsimples the new value for aliqsimples
     */
    public void setAliqsimples(BigDecimal aAliqsimples) {
        aliqsimples = aAliqsimples;
    }

    /**
     * Access method for impostosimples.
     *
     * @return the current value of impostosimples
     */
    public BigDecimal getImpostosimples() {
        return impostosimples;
    }

    /**
     * Setter method for impostosimples.
     *
     * @param aImpostosimples the new value for impostosimples
     */
    public void setImpostosimples(BigDecimal aImpostosimples) {
        impostosimples = aImpostosimples;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Opoperacao getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Opoperacao aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for fisesdet.
     *
     * @return the current value of fisesdet
     */
    public Set<Fisesdet> getFisesdet() {
        return fisesdet;
    }

    /**
     * Setter method for fisesdet.
     *
     * @param aFisesdet the new value for fisesdet
     */
    public void setFisesdet(Set<Fisesdet> aFisesdet) {
        fisesdet = aFisesdet;
    }

    /**
     * Compares the key for this instance with another Fises.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Fises and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Fises)) {
            return false;
        }
        Fises that = (Fises) other;
        if (this.getCodfises() != that.getCodfises()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Fises.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Fises)) return false;
        return this.equalKeys(other) && ((Fises)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfises();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Fises |");
        sb.append(" codfises=").append(getCodfises());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfises", Integer.valueOf(getCodfises()));
        return ret;
    }

}
