package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FISESDET")
public class Fisesdet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfisesdet";

    @Id
    @Column(name="CODFISESDET", unique=true, nullable=false, precision=10)
    private int codfisesdet;
    @Column(name="TOTQUANTIDADE", nullable=false, precision=15, scale=4)
    private BigDecimal totquantidade;
    @Column(name="TOTUNITARIO", nullable=false, precision=15, scale=4)
    private BigDecimal totunitario;
    @Column(name="TOTAL", nullable=false, precision=15, scale=4)
    private BigDecimal total;
    @Column(name="TOTFRETE", precision=15, scale=4)
    private BigDecimal totfrete;
    @Column(name="TOTSEGURO", precision=15, scale=4)
    private BigDecimal totseguro;
    @Column(name="TOTDESCONTO", precision=15, scale=4)
    private BigDecimal totdesconto;
    @Column(name="TOTOUTRAS", precision=15, scale=4)
    private BigDecimal totoutras;
    @Column(name="BASEICMS", precision=15, scale=4)
    private BigDecimal baseicms;
    @Column(name="ALIQICMS", precision=15, scale=4)
    private BigDecimal aliqicms;
    @Column(name="IMPOSTOICMS", precision=15, scale=4)
    private BigDecimal impostoicms;
    @Column(name="BASEST", precision=15, scale=4)
    private BigDecimal basest;
    @Column(name="ALIQST", precision=15, scale=4)
    private BigDecimal aliqst;
    @Column(name="IMPOSTOST", precision=15, scale=4)
    private BigDecimal impostost;
    @Column(name="BASEIPI", precision=15, scale=4)
    private BigDecimal baseipi;
    @Column(name="ALIQIPI", precision=15, scale=4)
    private BigDecimal aliqipi;
    @Column(name="IMPOSTOIPI", precision=15, scale=4)
    private BigDecimal impostoipi;
    @Column(name="BASEPIS", precision=15, scale=4)
    private BigDecimal basepis;
    @Column(name="ALIQPIS", precision=15, scale=4)
    private BigDecimal aliqpis;
    @Column(name="IMPOSTOPIS", precision=15, scale=4)
    private BigDecimal impostopis;
    @Column(name="BASECOFINS", precision=15, scale=4)
    private BigDecimal basecofins;
    @Column(name="ALIQCOFINS", precision=15, scale=4)
    private BigDecimal aliqcofins;
    @Column(name="IMPOSTOCOFINS", precision=15, scale=4)
    private BigDecimal impostocofins;
    @Column(name="BASEIRPJ", precision=15, scale=4)
    private BigDecimal baseirpj;
    @Column(name="ALIQIRPJ", precision=15, scale=4)
    private BigDecimal aliqirpj;
    @Column(name="IMPOSTOIRPJ", precision=15, scale=4)
    private BigDecimal impostoirpj;
    @Column(name="BASECSLL", precision=15, scale=4)
    private BigDecimal basecsll;
    @Column(name="ALIQCSLL", precision=15, scale=4)
    private BigDecimal aliqcsll;
    @Column(name="IMPOSTOCSLL", precision=15, scale=4)
    private BigDecimal impostocsll;
    @Column(name="BASESIMPLES", precision=15, scale=4)
    private BigDecimal basesimples;
    @Column(name="ALIQSIMPLES", precision=15, scale=4)
    private BigDecimal aliqsimples;
    @Column(name="IMPOSTOSIMPLES", precision=15, scale=4)
    private BigDecimal impostosimples;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFISES", nullable=false)
    private Fises fises;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODIMPCST", nullable=false)
    private Impcst impcst;
    @ManyToOne
    @JoinColumn(name="CODPRODNCM")
    private Prodncm prodncm;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPOPERACAO", nullable=false)
    private Opoperacao opoperacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODUNIDADE", nullable=false)
    private Produnidade produnidade;

    /** Default constructor. */
    public Fisesdet() {
        super();
    }

    /**
     * Access method for codfisesdet.
     *
     * @return the current value of codfisesdet
     */
    public int getCodfisesdet() {
        return codfisesdet;
    }

    /**
     * Setter method for codfisesdet.
     *
     * @param aCodfisesdet the new value for codfisesdet
     */
    public void setCodfisesdet(int aCodfisesdet) {
        codfisesdet = aCodfisesdet;
    }

    /**
     * Access method for totquantidade.
     *
     * @return the current value of totquantidade
     */
    public BigDecimal getTotquantidade() {
        return totquantidade;
    }

    /**
     * Setter method for totquantidade.
     *
     * @param aTotquantidade the new value for totquantidade
     */
    public void setTotquantidade(BigDecimal aTotquantidade) {
        totquantidade = aTotquantidade;
    }

    /**
     * Access method for totunitario.
     *
     * @return the current value of totunitario
     */
    public BigDecimal getTotunitario() {
        return totunitario;
    }

    /**
     * Setter method for totunitario.
     *
     * @param aTotunitario the new value for totunitario
     */
    public void setTotunitario(BigDecimal aTotunitario) {
        totunitario = aTotunitario;
    }

    /**
     * Access method for total.
     *
     * @return the current value of total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Setter method for total.
     *
     * @param aTotal the new value for total
     */
    public void setTotal(BigDecimal aTotal) {
        total = aTotal;
    }

    /**
     * Access method for totfrete.
     *
     * @return the current value of totfrete
     */
    public BigDecimal getTotfrete() {
        return totfrete;
    }

    /**
     * Setter method for totfrete.
     *
     * @param aTotfrete the new value for totfrete
     */
    public void setTotfrete(BigDecimal aTotfrete) {
        totfrete = aTotfrete;
    }

    /**
     * Access method for totseguro.
     *
     * @return the current value of totseguro
     */
    public BigDecimal getTotseguro() {
        return totseguro;
    }

    /**
     * Setter method for totseguro.
     *
     * @param aTotseguro the new value for totseguro
     */
    public void setTotseguro(BigDecimal aTotseguro) {
        totseguro = aTotseguro;
    }

    /**
     * Access method for totdesconto.
     *
     * @return the current value of totdesconto
     */
    public BigDecimal getTotdesconto() {
        return totdesconto;
    }

    /**
     * Setter method for totdesconto.
     *
     * @param aTotdesconto the new value for totdesconto
     */
    public void setTotdesconto(BigDecimal aTotdesconto) {
        totdesconto = aTotdesconto;
    }

    /**
     * Access method for totoutras.
     *
     * @return the current value of totoutras
     */
    public BigDecimal getTotoutras() {
        return totoutras;
    }

    /**
     * Setter method for totoutras.
     *
     * @param aTotoutras the new value for totoutras
     */
    public void setTotoutras(BigDecimal aTotoutras) {
        totoutras = aTotoutras;
    }

    /**
     * Access method for baseicms.
     *
     * @return the current value of baseicms
     */
    public BigDecimal getBaseicms() {
        return baseicms;
    }

    /**
     * Setter method for baseicms.
     *
     * @param aBaseicms the new value for baseicms
     */
    public void setBaseicms(BigDecimal aBaseicms) {
        baseicms = aBaseicms;
    }

    /**
     * Access method for aliqicms.
     *
     * @return the current value of aliqicms
     */
    public BigDecimal getAliqicms() {
        return aliqicms;
    }

    /**
     * Setter method for aliqicms.
     *
     * @param aAliqicms the new value for aliqicms
     */
    public void setAliqicms(BigDecimal aAliqicms) {
        aliqicms = aAliqicms;
    }

    /**
     * Access method for impostoicms.
     *
     * @return the current value of impostoicms
     */
    public BigDecimal getImpostoicms() {
        return impostoicms;
    }

    /**
     * Setter method for impostoicms.
     *
     * @param aImpostoicms the new value for impostoicms
     */
    public void setImpostoicms(BigDecimal aImpostoicms) {
        impostoicms = aImpostoicms;
    }

    /**
     * Access method for basest.
     *
     * @return the current value of basest
     */
    public BigDecimal getBasest() {
        return basest;
    }

    /**
     * Setter method for basest.
     *
     * @param aBasest the new value for basest
     */
    public void setBasest(BigDecimal aBasest) {
        basest = aBasest;
    }

    /**
     * Access method for aliqst.
     *
     * @return the current value of aliqst
     */
    public BigDecimal getAliqst() {
        return aliqst;
    }

    /**
     * Setter method for aliqst.
     *
     * @param aAliqst the new value for aliqst
     */
    public void setAliqst(BigDecimal aAliqst) {
        aliqst = aAliqst;
    }

    /**
     * Access method for impostost.
     *
     * @return the current value of impostost
     */
    public BigDecimal getImpostost() {
        return impostost;
    }

    /**
     * Setter method for impostost.
     *
     * @param aImpostost the new value for impostost
     */
    public void setImpostost(BigDecimal aImpostost) {
        impostost = aImpostost;
    }

    /**
     * Access method for baseipi.
     *
     * @return the current value of baseipi
     */
    public BigDecimal getBaseipi() {
        return baseipi;
    }

    /**
     * Setter method for baseipi.
     *
     * @param aBaseipi the new value for baseipi
     */
    public void setBaseipi(BigDecimal aBaseipi) {
        baseipi = aBaseipi;
    }

    /**
     * Access method for aliqipi.
     *
     * @return the current value of aliqipi
     */
    public BigDecimal getAliqipi() {
        return aliqipi;
    }

    /**
     * Setter method for aliqipi.
     *
     * @param aAliqipi the new value for aliqipi
     */
    public void setAliqipi(BigDecimal aAliqipi) {
        aliqipi = aAliqipi;
    }

    /**
     * Access method for impostoipi.
     *
     * @return the current value of impostoipi
     */
    public BigDecimal getImpostoipi() {
        return impostoipi;
    }

    /**
     * Setter method for impostoipi.
     *
     * @param aImpostoipi the new value for impostoipi
     */
    public void setImpostoipi(BigDecimal aImpostoipi) {
        impostoipi = aImpostoipi;
    }

    /**
     * Access method for basepis.
     *
     * @return the current value of basepis
     */
    public BigDecimal getBasepis() {
        return basepis;
    }

    /**
     * Setter method for basepis.
     *
     * @param aBasepis the new value for basepis
     */
    public void setBasepis(BigDecimal aBasepis) {
        basepis = aBasepis;
    }

    /**
     * Access method for aliqpis.
     *
     * @return the current value of aliqpis
     */
    public BigDecimal getAliqpis() {
        return aliqpis;
    }

    /**
     * Setter method for aliqpis.
     *
     * @param aAliqpis the new value for aliqpis
     */
    public void setAliqpis(BigDecimal aAliqpis) {
        aliqpis = aAliqpis;
    }

    /**
     * Access method for impostopis.
     *
     * @return the current value of impostopis
     */
    public BigDecimal getImpostopis() {
        return impostopis;
    }

    /**
     * Setter method for impostopis.
     *
     * @param aImpostopis the new value for impostopis
     */
    public void setImpostopis(BigDecimal aImpostopis) {
        impostopis = aImpostopis;
    }

    /**
     * Access method for basecofins.
     *
     * @return the current value of basecofins
     */
    public BigDecimal getBasecofins() {
        return basecofins;
    }

    /**
     * Setter method for basecofins.
     *
     * @param aBasecofins the new value for basecofins
     */
    public void setBasecofins(BigDecimal aBasecofins) {
        basecofins = aBasecofins;
    }

    /**
     * Access method for aliqcofins.
     *
     * @return the current value of aliqcofins
     */
    public BigDecimal getAliqcofins() {
        return aliqcofins;
    }

    /**
     * Setter method for aliqcofins.
     *
     * @param aAliqcofins the new value for aliqcofins
     */
    public void setAliqcofins(BigDecimal aAliqcofins) {
        aliqcofins = aAliqcofins;
    }

    /**
     * Access method for impostocofins.
     *
     * @return the current value of impostocofins
     */
    public BigDecimal getImpostocofins() {
        return impostocofins;
    }

    /**
     * Setter method for impostocofins.
     *
     * @param aImpostocofins the new value for impostocofins
     */
    public void setImpostocofins(BigDecimal aImpostocofins) {
        impostocofins = aImpostocofins;
    }

    /**
     * Access method for baseirpj.
     *
     * @return the current value of baseirpj
     */
    public BigDecimal getBaseirpj() {
        return baseirpj;
    }

    /**
     * Setter method for baseirpj.
     *
     * @param aBaseirpj the new value for baseirpj
     */
    public void setBaseirpj(BigDecimal aBaseirpj) {
        baseirpj = aBaseirpj;
    }

    /**
     * Access method for aliqirpj.
     *
     * @return the current value of aliqirpj
     */
    public BigDecimal getAliqirpj() {
        return aliqirpj;
    }

    /**
     * Setter method for aliqirpj.
     *
     * @param aAliqirpj the new value for aliqirpj
     */
    public void setAliqirpj(BigDecimal aAliqirpj) {
        aliqirpj = aAliqirpj;
    }

    /**
     * Access method for impostoirpj.
     *
     * @return the current value of impostoirpj
     */
    public BigDecimal getImpostoirpj() {
        return impostoirpj;
    }

    /**
     * Setter method for impostoirpj.
     *
     * @param aImpostoirpj the new value for impostoirpj
     */
    public void setImpostoirpj(BigDecimal aImpostoirpj) {
        impostoirpj = aImpostoirpj;
    }

    /**
     * Access method for basecsll.
     *
     * @return the current value of basecsll
     */
    public BigDecimal getBasecsll() {
        return basecsll;
    }

    /**
     * Setter method for basecsll.
     *
     * @param aBasecsll the new value for basecsll
     */
    public void setBasecsll(BigDecimal aBasecsll) {
        basecsll = aBasecsll;
    }

    /**
     * Access method for aliqcsll.
     *
     * @return the current value of aliqcsll
     */
    public BigDecimal getAliqcsll() {
        return aliqcsll;
    }

    /**
     * Setter method for aliqcsll.
     *
     * @param aAliqcsll the new value for aliqcsll
     */
    public void setAliqcsll(BigDecimal aAliqcsll) {
        aliqcsll = aAliqcsll;
    }

    /**
     * Access method for impostocsll.
     *
     * @return the current value of impostocsll
     */
    public BigDecimal getImpostocsll() {
        return impostocsll;
    }

    /**
     * Setter method for impostocsll.
     *
     * @param aImpostocsll the new value for impostocsll
     */
    public void setImpostocsll(BigDecimal aImpostocsll) {
        impostocsll = aImpostocsll;
    }

    /**
     * Access method for basesimples.
     *
     * @return the current value of basesimples
     */
    public BigDecimal getBasesimples() {
        return basesimples;
    }

    /**
     * Setter method for basesimples.
     *
     * @param aBasesimples the new value for basesimples
     */
    public void setBasesimples(BigDecimal aBasesimples) {
        basesimples = aBasesimples;
    }

    /**
     * Access method for aliqsimples.
     *
     * @return the current value of aliqsimples
     */
    public BigDecimal getAliqsimples() {
        return aliqsimples;
    }

    /**
     * Setter method for aliqsimples.
     *
     * @param aAliqsimples the new value for aliqsimples
     */
    public void setAliqsimples(BigDecimal aAliqsimples) {
        aliqsimples = aAliqsimples;
    }

    /**
     * Access method for impostosimples.
     *
     * @return the current value of impostosimples
     */
    public BigDecimal getImpostosimples() {
        return impostosimples;
    }

    /**
     * Setter method for impostosimples.
     *
     * @param aImpostosimples the new value for impostosimples
     */
    public void setImpostosimples(BigDecimal aImpostosimples) {
        impostosimples = aImpostosimples;
    }

    /**
     * Access method for fises.
     *
     * @return the current value of fises
     */
    public Fises getFises() {
        return fises;
    }

    /**
     * Setter method for fises.
     *
     * @param aFises the new value for fises
     */
    public void setFises(Fises aFises) {
        fises = aFises;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for impcst.
     *
     * @return the current value of impcst
     */
    public Impcst getImpcst() {
        return impcst;
    }

    /**
     * Setter method for impcst.
     *
     * @param aImpcst the new value for impcst
     */
    public void setImpcst(Impcst aImpcst) {
        impcst = aImpcst;
    }

    /**
     * Access method for prodncm.
     *
     * @return the current value of prodncm
     */
    public Prodncm getProdncm() {
        return prodncm;
    }

    /**
     * Setter method for prodncm.
     *
     * @param aProdncm the new value for prodncm
     */
    public void setProdncm(Prodncm aProdncm) {
        prodncm = aProdncm;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Opoperacao getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Opoperacao aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for produnidade.
     *
     * @return the current value of produnidade
     */
    public Produnidade getProdunidade() {
        return produnidade;
    }

    /**
     * Setter method for produnidade.
     *
     * @param aProdunidade the new value for produnidade
     */
    public void setProdunidade(Produnidade aProdunidade) {
        produnidade = aProdunidade;
    }

    /**
     * Compares the key for this instance with another Fisesdet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Fisesdet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Fisesdet)) {
            return false;
        }
        Fisesdet that = (Fisesdet) other;
        if (this.getCodfisesdet() != that.getCodfisesdet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Fisesdet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Fisesdet)) return false;
        return this.equalKeys(other) && ((Fisesdet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfisesdet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Fisesdet |");
        sb.append(" codfisesdet=").append(getCodfisesdet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfisesdet", Integer.valueOf(getCodfisesdet()));
        return ret;
    }

}
