package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FISNATJUR")
public class Fisnatjur implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfisnatjur";

    @Id
    @Column(name="CODFISNATJUR", unique=true, nullable=false, length=20)
    private String codfisnatjur;
    @Column(name="DESCFISNATJUR", nullable=false, length=250)
    private String descfisnatjur;
    @Column(name="REPRESENTANTE", length=40)
    private String representante;
    @Column(name="QUALIFICACAO", length=20)
    private String qualificacao;
    @OneToMany(mappedBy="fisnatjur")
    private Set<Agagente> agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODTIPOFISNATJUR", nullable=false)
    private Fisnatjurtipo fisnatjurtipo;

    /** Default constructor. */
    public Fisnatjur() {
        super();
    }

    /**
     * Access method for codfisnatjur.
     *
     * @return the current value of codfisnatjur
     */
    public String getCodfisnatjur() {
        return codfisnatjur;
    }

    /**
     * Setter method for codfisnatjur.
     *
     * @param aCodfisnatjur the new value for codfisnatjur
     */
    public void setCodfisnatjur(String aCodfisnatjur) {
        codfisnatjur = aCodfisnatjur;
    }

    /**
     * Access method for descfisnatjur.
     *
     * @return the current value of descfisnatjur
     */
    public String getDescfisnatjur() {
        return descfisnatjur;
    }

    /**
     * Setter method for descfisnatjur.
     *
     * @param aDescfisnatjur the new value for descfisnatjur
     */
    public void setDescfisnatjur(String aDescfisnatjur) {
        descfisnatjur = aDescfisnatjur;
    }

    /**
     * Access method for representante.
     *
     * @return the current value of representante
     */
    public String getRepresentante() {
        return representante;
    }

    /**
     * Setter method for representante.
     *
     * @param aRepresentante the new value for representante
     */
    public void setRepresentante(String aRepresentante) {
        representante = aRepresentante;
    }

    /**
     * Access method for qualificacao.
     *
     * @return the current value of qualificacao
     */
    public String getQualificacao() {
        return qualificacao;
    }

    /**
     * Setter method for qualificacao.
     *
     * @param aQualificacao the new value for qualificacao
     */
    public void setQualificacao(String aQualificacao) {
        qualificacao = aQualificacao;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for fisnatjurtipo.
     *
     * @return the current value of fisnatjurtipo
     */
    public Fisnatjurtipo getFisnatjurtipo() {
        return fisnatjurtipo;
    }

    /**
     * Setter method for fisnatjurtipo.
     *
     * @param aFisnatjurtipo the new value for fisnatjurtipo
     */
    public void setFisnatjurtipo(Fisnatjurtipo aFisnatjurtipo) {
        fisnatjurtipo = aFisnatjurtipo;
    }

    /**
     * Compares the key for this instance with another Fisnatjur.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Fisnatjur and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Fisnatjur)) {
            return false;
        }
        Fisnatjur that = (Fisnatjur) other;
        Object myCodfisnatjur = this.getCodfisnatjur();
        Object yourCodfisnatjur = that.getCodfisnatjur();
        if (myCodfisnatjur==null ? yourCodfisnatjur!=null : !myCodfisnatjur.equals(yourCodfisnatjur)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Fisnatjur.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Fisnatjur)) return false;
        return this.equalKeys(other) && ((Fisnatjur)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfisnatjur() == null) {
            i = 0;
        } else {
            i = getCodfisnatjur().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Fisnatjur |");
        sb.append(" codfisnatjur=").append(getCodfisnatjur());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfisnatjur", getCodfisnatjur());
        return ret;
    }

}
