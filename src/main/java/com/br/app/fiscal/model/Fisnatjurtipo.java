package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FISNATJURTIPO")
public class Fisnatjurtipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfisnatjurtipo";

    @Id
    @Column(name="CODFISNATJURTIPO", unique=true, nullable=false, precision=10)
    private int codfisnatjurtipo;
    @Column(name="DESCFISNATJURTIPO", nullable=false, length=250)
    private String descfisnatjurtipo;
    @OneToMany(mappedBy="fisnatjurtipo")
    private Set<Fisnatjur> fisnatjur;

    /** Default constructor. */
    public Fisnatjurtipo() {
        super();
    }

    /**
     * Access method for codfisnatjurtipo.
     *
     * @return the current value of codfisnatjurtipo
     */
    public int getCodfisnatjurtipo() {
        return codfisnatjurtipo;
    }

    /**
     * Setter method for codfisnatjurtipo.
     *
     * @param aCodfisnatjurtipo the new value for codfisnatjurtipo
     */
    public void setCodfisnatjurtipo(int aCodfisnatjurtipo) {
        codfisnatjurtipo = aCodfisnatjurtipo;
    }

    /**
     * Access method for descfisnatjurtipo.
     *
     * @return the current value of descfisnatjurtipo
     */
    public String getDescfisnatjurtipo() {
        return descfisnatjurtipo;
    }

    /**
     * Setter method for descfisnatjurtipo.
     *
     * @param aDescfisnatjurtipo the new value for descfisnatjurtipo
     */
    public void setDescfisnatjurtipo(String aDescfisnatjurtipo) {
        descfisnatjurtipo = aDescfisnatjurtipo;
    }

    /**
     * Access method for fisnatjur.
     *
     * @return the current value of fisnatjur
     */
    public Set<Fisnatjur> getFisnatjur() {
        return fisnatjur;
    }

    /**
     * Setter method for fisnatjur.
     *
     * @param aFisnatjur the new value for fisnatjur
     */
    public void setFisnatjur(Set<Fisnatjur> aFisnatjur) {
        fisnatjur = aFisnatjur;
    }

    /**
     * Compares the key for this instance with another Fisnatjurtipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Fisnatjurtipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Fisnatjurtipo)) {
            return false;
        }
        Fisnatjurtipo that = (Fisnatjurtipo) other;
        if (this.getCodfisnatjurtipo() != that.getCodfisnatjurtipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Fisnatjurtipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Fisnatjurtipo)) return false;
        return this.equalKeys(other) && ((Fisnatjurtipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfisnatjurtipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Fisnatjurtipo |");
        sb.append(" codfisnatjurtipo=").append(getCodfisnatjurtipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfisnatjurtipo", Integer.valueOf(getCodfisnatjurtipo()));
        return ret;
    }

}
