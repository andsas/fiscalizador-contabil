package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FISRECEITA")
public class Fisreceita implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfisreceita";

    @Id
    @Column(name="CODFISRECEITA", unique=true, nullable=false, precision=10)
    private int codfisreceita;
    @Column(name="DESCFISRECEITA", nullable=false, length=250)
    private String descfisreceita;
    @Column(name="TIPOFISRECEITA", precision=5)
    private short tipofisreceita;
    @ManyToOne
    @JoinColumn(name="CODFISTIPORECEITA")
    private Fisreceitatipo fisreceitatipo;

    /** Default constructor. */
    public Fisreceita() {
        super();
    }

    /**
     * Access method for codfisreceita.
     *
     * @return the current value of codfisreceita
     */
    public int getCodfisreceita() {
        return codfisreceita;
    }

    /**
     * Setter method for codfisreceita.
     *
     * @param aCodfisreceita the new value for codfisreceita
     */
    public void setCodfisreceita(int aCodfisreceita) {
        codfisreceita = aCodfisreceita;
    }

    /**
     * Access method for descfisreceita.
     *
     * @return the current value of descfisreceita
     */
    public String getDescfisreceita() {
        return descfisreceita;
    }

    /**
     * Setter method for descfisreceita.
     *
     * @param aDescfisreceita the new value for descfisreceita
     */
    public void setDescfisreceita(String aDescfisreceita) {
        descfisreceita = aDescfisreceita;
    }

    /**
     * Access method for tipofisreceita.
     *
     * @return the current value of tipofisreceita
     */
    public short getTipofisreceita() {
        return tipofisreceita;
    }

    /**
     * Setter method for tipofisreceita.
     *
     * @param aTipofisreceita the new value for tipofisreceita
     */
    public void setTipofisreceita(short aTipofisreceita) {
        tipofisreceita = aTipofisreceita;
    }

    /**
     * Access method for fisreceitatipo.
     *
     * @return the current value of fisreceitatipo
     */
    public Fisreceitatipo getFisreceitatipo() {
        return fisreceitatipo;
    }

    /**
     * Setter method for fisreceitatipo.
     *
     * @param aFisreceitatipo the new value for fisreceitatipo
     */
    public void setFisreceitatipo(Fisreceitatipo aFisreceitatipo) {
        fisreceitatipo = aFisreceitatipo;
    }

    /**
     * Compares the key for this instance with another Fisreceita.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Fisreceita and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Fisreceita)) {
            return false;
        }
        Fisreceita that = (Fisreceita) other;
        if (this.getCodfisreceita() != that.getCodfisreceita()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Fisreceita.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Fisreceita)) return false;
        return this.equalKeys(other) && ((Fisreceita)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfisreceita();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Fisreceita |");
        sb.append(" codfisreceita=").append(getCodfisreceita());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfisreceita", Integer.valueOf(getCodfisreceita()));
        return ret;
    }

}
