package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FISRECEITATIPO")
public class Fisreceitatipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfisreceitatipo";

    @Id
    @Column(name="CODFISRECEITATIPO", unique=true, nullable=false, precision=10)
    private int codfisreceitatipo;
    @Column(name="DESCFISRECEITATIPO", nullable=false, length=250)
    private String descfisreceitatipo;
    @OneToMany(mappedBy="fisreceitatipo")
    private Set<Fisreceita> fisreceita;

    /** Default constructor. */
    public Fisreceitatipo() {
        super();
    }

    /**
     * Access method for codfisreceitatipo.
     *
     * @return the current value of codfisreceitatipo
     */
    public int getCodfisreceitatipo() {
        return codfisreceitatipo;
    }

    /**
     * Setter method for codfisreceitatipo.
     *
     * @param aCodfisreceitatipo the new value for codfisreceitatipo
     */
    public void setCodfisreceitatipo(int aCodfisreceitatipo) {
        codfisreceitatipo = aCodfisreceitatipo;
    }

    /**
     * Access method for descfisreceitatipo.
     *
     * @return the current value of descfisreceitatipo
     */
    public String getDescfisreceitatipo() {
        return descfisreceitatipo;
    }

    /**
     * Setter method for descfisreceitatipo.
     *
     * @param aDescfisreceitatipo the new value for descfisreceitatipo
     */
    public void setDescfisreceitatipo(String aDescfisreceitatipo) {
        descfisreceitatipo = aDescfisreceitatipo;
    }

    /**
     * Access method for fisreceita.
     *
     * @return the current value of fisreceita
     */
    public Set<Fisreceita> getFisreceita() {
        return fisreceita;
    }

    /**
     * Setter method for fisreceita.
     *
     * @param aFisreceita the new value for fisreceita
     */
    public void setFisreceita(Set<Fisreceita> aFisreceita) {
        fisreceita = aFisreceita;
    }

    /**
     * Compares the key for this instance with another Fisreceitatipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Fisreceitatipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Fisreceitatipo)) {
            return false;
        }
        Fisreceitatipo that = (Fisreceitatipo) other;
        if (this.getCodfisreceitatipo() != that.getCodfisreceitatipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Fisreceitatipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Fisreceitatipo)) return false;
        return this.equalKeys(other) && ((Fisreceitatipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfisreceitatipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Fisreceitatipo |");
        sb.append(" codfisreceitatipo=").append(getCodfisreceitatipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfisreceitatipo", Integer.valueOf(getCodfisreceitatipo()));
        return ret;
    }

}
