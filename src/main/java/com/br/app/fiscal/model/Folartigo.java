package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FOLARTIGO")
public class Folartigo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolartigo";

    @Id
    @Column(name="CODFOLARTIGO", unique=true, nullable=false, length=60)
    private String codfolartigo;
    @Column(name="DESCFOLARTIGO", nullable=false)
    private String descfolartigo;
    @OneToMany(mappedBy="folartigo")
    private Set<Folfgtsaliquota> folfgtsaliquota;
    @OneToMany(mappedBy="folartigo2")
    private Set<Folfgtsaliquota> folfgtsaliquota2;
    @OneToMany(mappedBy="folartigo3")
    private Set<Folfgtsaliquota> folfgtsaliquota3;
    @OneToMany(mappedBy="folartigo")
    private Set<Follancamento> follancamento;
    @OneToMany(mappedBy="folartigo")
    private Set<Folsalfamilia> folsalfamilia;
    @OneToMany(mappedBy="folartigo")
    private Set<Folsalminimo> folsalminimo;
    @OneToMany(mappedBy="folartigo")
    private Set<Folsindicato> folsindicato;

    /** Default constructor. */
    public Folartigo() {
        super();
    }

    /**
     * Access method for codfolartigo.
     *
     * @return the current value of codfolartigo
     */
    public String getCodfolartigo() {
        return codfolartigo;
    }

    /**
     * Setter method for codfolartigo.
     *
     * @param aCodfolartigo the new value for codfolartigo
     */
    public void setCodfolartigo(String aCodfolartigo) {
        codfolartigo = aCodfolartigo;
    }

    /**
     * Access method for descfolartigo.
     *
     * @return the current value of descfolartigo
     */
    public String getDescfolartigo() {
        return descfolartigo;
    }

    /**
     * Setter method for descfolartigo.
     *
     * @param aDescfolartigo the new value for descfolartigo
     */
    public void setDescfolartigo(String aDescfolartigo) {
        descfolartigo = aDescfolartigo;
    }

    /**
     * Access method for folfgtsaliquota.
     *
     * @return the current value of folfgtsaliquota
     */
    public Set<Folfgtsaliquota> getFolfgtsaliquota() {
        return folfgtsaliquota;
    }

    /**
     * Setter method for folfgtsaliquota.
     *
     * @param aFolfgtsaliquota the new value for folfgtsaliquota
     */
    public void setFolfgtsaliquota(Set<Folfgtsaliquota> aFolfgtsaliquota) {
        folfgtsaliquota = aFolfgtsaliquota;
    }

    /**
     * Access method for folfgtsaliquota2.
     *
     * @return the current value of folfgtsaliquota2
     */
    public Set<Folfgtsaliquota> getFolfgtsaliquota2() {
        return folfgtsaliquota2;
    }

    /**
     * Setter method for folfgtsaliquota2.
     *
     * @param aFolfgtsaliquota2 the new value for folfgtsaliquota2
     */
    public void setFolfgtsaliquota2(Set<Folfgtsaliquota> aFolfgtsaliquota2) {
        folfgtsaliquota2 = aFolfgtsaliquota2;
    }

    /**
     * Access method for folfgtsaliquota3.
     *
     * @return the current value of folfgtsaliquota3
     */
    public Set<Folfgtsaliquota> getFolfgtsaliquota3() {
        return folfgtsaliquota3;
    }

    /**
     * Setter method for folfgtsaliquota3.
     *
     * @param aFolfgtsaliquota3 the new value for folfgtsaliquota3
     */
    public void setFolfgtsaliquota3(Set<Folfgtsaliquota> aFolfgtsaliquota3) {
        folfgtsaliquota3 = aFolfgtsaliquota3;
    }

    /**
     * Access method for follancamento.
     *
     * @return the current value of follancamento
     */
    public Set<Follancamento> getFollancamento() {
        return follancamento;
    }

    /**
     * Setter method for follancamento.
     *
     * @param aFollancamento the new value for follancamento
     */
    public void setFollancamento(Set<Follancamento> aFollancamento) {
        follancamento = aFollancamento;
    }

    /**
     * Access method for folsalfamilia.
     *
     * @return the current value of folsalfamilia
     */
    public Set<Folsalfamilia> getFolsalfamilia() {
        return folsalfamilia;
    }

    /**
     * Setter method for folsalfamilia.
     *
     * @param aFolsalfamilia the new value for folsalfamilia
     */
    public void setFolsalfamilia(Set<Folsalfamilia> aFolsalfamilia) {
        folsalfamilia = aFolsalfamilia;
    }

    /**
     * Access method for folsalminimo.
     *
     * @return the current value of folsalminimo
     */
    public Set<Folsalminimo> getFolsalminimo() {
        return folsalminimo;
    }

    /**
     * Setter method for folsalminimo.
     *
     * @param aFolsalminimo the new value for folsalminimo
     */
    public void setFolsalminimo(Set<Folsalminimo> aFolsalminimo) {
        folsalminimo = aFolsalminimo;
    }

    /**
     * Access method for folsindicato.
     *
     * @return the current value of folsindicato
     */
    public Set<Folsindicato> getFolsindicato() {
        return folsindicato;
    }

    /**
     * Setter method for folsindicato.
     *
     * @param aFolsindicato the new value for folsindicato
     */
    public void setFolsindicato(Set<Folsindicato> aFolsindicato) {
        folsindicato = aFolsindicato;
    }

    /**
     * Compares the key for this instance with another Folartigo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folartigo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folartigo)) {
            return false;
        }
        Folartigo that = (Folartigo) other;
        Object myCodfolartigo = this.getCodfolartigo();
        Object yourCodfolartigo = that.getCodfolartigo();
        if (myCodfolartigo==null ? yourCodfolartigo!=null : !myCodfolartigo.equals(yourCodfolartigo)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folartigo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folartigo)) return false;
        return this.equalKeys(other) && ((Folartigo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfolartigo() == null) {
            i = 0;
        } else {
            i = getCodfolartigo().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folartigo |");
        sb.append(" codfolartigo=").append(getCodfolartigo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolartigo", getCodfolartigo());
        return ret;
    }

}
