package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLAVISOPREVIO")
public class Folavisoprevio implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolavisoprevio";

    @Id
    @Column(name="CODFOLAVISOPREVIO", unique=true, nullable=false, precision=10)
    private int codfolavisoprevio;
    @Column(name="TEMPOSERVICO", nullable=false, precision=5)
    private short temposervico;
    @Column(name="DIASAVISO", nullable=false, precision=5)
    private short diasaviso;

    /** Default constructor. */
    public Folavisoprevio() {
        super();
    }

    /**
     * Access method for codfolavisoprevio.
     *
     * @return the current value of codfolavisoprevio
     */
    public int getCodfolavisoprevio() {
        return codfolavisoprevio;
    }

    /**
     * Setter method for codfolavisoprevio.
     *
     * @param aCodfolavisoprevio the new value for codfolavisoprevio
     */
    public void setCodfolavisoprevio(int aCodfolavisoprevio) {
        codfolavisoprevio = aCodfolavisoprevio;
    }

    /**
     * Access method for temposervico.
     *
     * @return the current value of temposervico
     */
    public short getTemposervico() {
        return temposervico;
    }

    /**
     * Setter method for temposervico.
     *
     * @param aTemposervico the new value for temposervico
     */
    public void setTemposervico(short aTemposervico) {
        temposervico = aTemposervico;
    }

    /**
     * Access method for diasaviso.
     *
     * @return the current value of diasaviso
     */
    public short getDiasaviso() {
        return diasaviso;
    }

    /**
     * Setter method for diasaviso.
     *
     * @param aDiasaviso the new value for diasaviso
     */
    public void setDiasaviso(short aDiasaviso) {
        diasaviso = aDiasaviso;
    }

    /**
     * Compares the key for this instance with another Folavisoprevio.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folavisoprevio and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folavisoprevio)) {
            return false;
        }
        Folavisoprevio that = (Folavisoprevio) other;
        if (this.getCodfolavisoprevio() != that.getCodfolavisoprevio()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folavisoprevio.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folavisoprevio)) return false;
        return this.equalKeys(other) && ((Folavisoprevio)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolavisoprevio();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folavisoprevio |");
        sb.append(" codfolavisoprevio=").append(getCodfolavisoprevio());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolavisoprevio", Integer.valueOf(getCodfolavisoprevio()));
        return ret;
    }

}
