package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLBENEFICIO")
public class Folbeneficio implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolbeneficio";

    @Id
    @Column(name="CODFOLBENEFICIO", unique=true, nullable=false, precision=10)
    private int codfolbeneficio;
    @Column(name="DESCFOLBENEFICIO", nullable=false, length=250)
    private String descfolbeneficio;
    @Column(name="TIPOFOLBENEFICIO", nullable=false, precision=5)
    private short tipofolbeneficio;
    @Column(name="DESCDIAUTIL", precision=5)
    private short descdiautil;
    @Column(name="VALORDIARIO", precision=15, scale=4)
    private BigDecimal valordiario;
    @Column(name="VALORINTEGRAL", precision=15, scale=4)
    private BigDecimal valorintegral;

    /** Default constructor. */
    public Folbeneficio() {
        super();
    }

    /**
     * Access method for codfolbeneficio.
     *
     * @return the current value of codfolbeneficio
     */
    public int getCodfolbeneficio() {
        return codfolbeneficio;
    }

    /**
     * Setter method for codfolbeneficio.
     *
     * @param aCodfolbeneficio the new value for codfolbeneficio
     */
    public void setCodfolbeneficio(int aCodfolbeneficio) {
        codfolbeneficio = aCodfolbeneficio;
    }

    /**
     * Access method for descfolbeneficio.
     *
     * @return the current value of descfolbeneficio
     */
    public String getDescfolbeneficio() {
        return descfolbeneficio;
    }

    /**
     * Setter method for descfolbeneficio.
     *
     * @param aDescfolbeneficio the new value for descfolbeneficio
     */
    public void setDescfolbeneficio(String aDescfolbeneficio) {
        descfolbeneficio = aDescfolbeneficio;
    }

    /**
     * Access method for tipofolbeneficio.
     *
     * @return the current value of tipofolbeneficio
     */
    public short getTipofolbeneficio() {
        return tipofolbeneficio;
    }

    /**
     * Setter method for tipofolbeneficio.
     *
     * @param aTipofolbeneficio the new value for tipofolbeneficio
     */
    public void setTipofolbeneficio(short aTipofolbeneficio) {
        tipofolbeneficio = aTipofolbeneficio;
    }

    /**
     * Access method for descdiautil.
     *
     * @return the current value of descdiautil
     */
    public short getDescdiautil() {
        return descdiautil;
    }

    /**
     * Setter method for descdiautil.
     *
     * @param aDescdiautil the new value for descdiautil
     */
    public void setDescdiautil(short aDescdiautil) {
        descdiautil = aDescdiautil;
    }

    /**
     * Access method for valordiario.
     *
     * @return the current value of valordiario
     */
    public BigDecimal getValordiario() {
        return valordiario;
    }

    /**
     * Setter method for valordiario.
     *
     * @param aValordiario the new value for valordiario
     */
    public void setValordiario(BigDecimal aValordiario) {
        valordiario = aValordiario;
    }

    /**
     * Access method for valorintegral.
     *
     * @return the current value of valorintegral
     */
    public BigDecimal getValorintegral() {
        return valorintegral;
    }

    /**
     * Setter method for valorintegral.
     *
     * @param aValorintegral the new value for valorintegral
     */
    public void setValorintegral(BigDecimal aValorintegral) {
        valorintegral = aValorintegral;
    }

    /**
     * Compares the key for this instance with another Folbeneficio.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folbeneficio and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folbeneficio)) {
            return false;
        }
        Folbeneficio that = (Folbeneficio) other;
        if (this.getCodfolbeneficio() != that.getCodfolbeneficio()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folbeneficio.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folbeneficio)) return false;
        return this.equalKeys(other) && ((Folbeneficio)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolbeneficio();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folbeneficio |");
        sb.append(" codfolbeneficio=").append(getCodfolbeneficio());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolbeneficio", Integer.valueOf(getCodfolbeneficio()));
        return ret;
    }

}
