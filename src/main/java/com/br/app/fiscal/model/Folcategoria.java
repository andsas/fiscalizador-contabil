package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLCATEGORIA")
public class Folcategoria implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolcategoria";

    @Id
    @Column(name="CODFOLCATEGORIA", unique=true, nullable=false, precision=10)
    private int codfolcategoria;
    @Column(name="DESCFOLCATEGORIA", nullable=false, length=250)
    private String descfolcategoria;

    /** Default constructor. */
    public Folcategoria() {
        super();
    }

    /**
     * Access method for codfolcategoria.
     *
     * @return the current value of codfolcategoria
     */
    public int getCodfolcategoria() {
        return codfolcategoria;
    }

    /**
     * Setter method for codfolcategoria.
     *
     * @param aCodfolcategoria the new value for codfolcategoria
     */
    public void setCodfolcategoria(int aCodfolcategoria) {
        codfolcategoria = aCodfolcategoria;
    }

    /**
     * Access method for descfolcategoria.
     *
     * @return the current value of descfolcategoria
     */
    public String getDescfolcategoria() {
        return descfolcategoria;
    }

    /**
     * Setter method for descfolcategoria.
     *
     * @param aDescfolcategoria the new value for descfolcategoria
     */
    public void setDescfolcategoria(String aDescfolcategoria) {
        descfolcategoria = aDescfolcategoria;
    }

    /**
     * Compares the key for this instance with another Folcategoria.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folcategoria and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folcategoria)) {
            return false;
        }
        Folcategoria that = (Folcategoria) other;
        if (this.getCodfolcategoria() != that.getCodfolcategoria()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folcategoria.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folcategoria)) return false;
        return this.equalKeys(other) && ((Folcategoria)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolcategoria();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folcategoria |");
        sb.append(" codfolcategoria=").append(getCodfolcategoria());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolcategoria", Integer.valueOf(getCodfolcategoria()));
        return ret;
    }

}
