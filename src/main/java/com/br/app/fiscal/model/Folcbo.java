package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FOLCBO")
public class Folcbo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolcbo";

    @Id
    @Column(name="CODFOLCBO", unique=true, nullable=false, length=20)
    private String codfolcbo;
    @Column(name="DESCFOLCBO", nullable=false, length=250)
    private String descfolcbo;
    @OneToMany(mappedBy="folcbo")
    private Set<Folfuncao> folfuncao;

    /** Default constructor. */
    public Folcbo() {
        super();
    }

    /**
     * Access method for codfolcbo.
     *
     * @return the current value of codfolcbo
     */
    public String getCodfolcbo() {
        return codfolcbo;
    }

    /**
     * Setter method for codfolcbo.
     *
     * @param aCodfolcbo the new value for codfolcbo
     */
    public void setCodfolcbo(String aCodfolcbo) {
        codfolcbo = aCodfolcbo;
    }

    /**
     * Access method for descfolcbo.
     *
     * @return the current value of descfolcbo
     */
    public String getDescfolcbo() {
        return descfolcbo;
    }

    /**
     * Setter method for descfolcbo.
     *
     * @param aDescfolcbo the new value for descfolcbo
     */
    public void setDescfolcbo(String aDescfolcbo) {
        descfolcbo = aDescfolcbo;
    }

    /**
     * Access method for folfuncao.
     *
     * @return the current value of folfuncao
     */
    public Set<Folfuncao> getFolfuncao() {
        return folfuncao;
    }

    /**
     * Setter method for folfuncao.
     *
     * @param aFolfuncao the new value for folfuncao
     */
    public void setFolfuncao(Set<Folfuncao> aFolfuncao) {
        folfuncao = aFolfuncao;
    }

    /**
     * Compares the key for this instance with another Folcbo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folcbo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folcbo)) {
            return false;
        }
        Folcbo that = (Folcbo) other;
        Object myCodfolcbo = this.getCodfolcbo();
        Object yourCodfolcbo = that.getCodfolcbo();
        if (myCodfolcbo==null ? yourCodfolcbo!=null : !myCodfolcbo.equals(yourCodfolcbo)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folcbo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folcbo)) return false;
        return this.equalKeys(other) && ((Folcbo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfolcbo() == null) {
            i = 0;
        } else {
            i = getCodfolcbo().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folcbo |");
        sb.append(" codfolcbo=").append(getCodfolcbo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolcbo", getCodfolcbo());
        return ret;
    }

}
