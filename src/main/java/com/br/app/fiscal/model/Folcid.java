package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLCID")
public class Folcid implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolcid";

    @Id
    @Column(name="CODFOLCID", unique=true, nullable=false, length=20)
    private String codfolcid;
    @Column(name="DESCFOLCID", nullable=false, length=250)
    private String descfolcid;

    /** Default constructor. */
    public Folcid() {
        super();
    }

    /**
     * Access method for codfolcid.
     *
     * @return the current value of codfolcid
     */
    public String getCodfolcid() {
        return codfolcid;
    }

    /**
     * Setter method for codfolcid.
     *
     * @param aCodfolcid the new value for codfolcid
     */
    public void setCodfolcid(String aCodfolcid) {
        codfolcid = aCodfolcid;
    }

    /**
     * Access method for descfolcid.
     *
     * @return the current value of descfolcid
     */
    public String getDescfolcid() {
        return descfolcid;
    }

    /**
     * Setter method for descfolcid.
     *
     * @param aDescfolcid the new value for descfolcid
     */
    public void setDescfolcid(String aDescfolcid) {
        descfolcid = aDescfolcid;
    }

    /**
     * Compares the key for this instance with another Folcid.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folcid and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folcid)) {
            return false;
        }
        Folcid that = (Folcid) other;
        Object myCodfolcid = this.getCodfolcid();
        Object yourCodfolcid = that.getCodfolcid();
        if (myCodfolcid==null ? yourCodfolcid!=null : !myCodfolcid.equals(yourCodfolcid)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folcid.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folcid)) return false;
        return this.equalKeys(other) && ((Folcid)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfolcid() == null) {
            i = 0;
        } else {
            i = getCodfolcid().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folcid |");
        sb.append(" codfolcid=").append(getCodfolcid());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolcid", getCodfolcid());
        return ret;
    }

}
