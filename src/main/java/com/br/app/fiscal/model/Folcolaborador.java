package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FOLCOLABORADOR")
public class Folcolaborador implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolcolaborador";

    @Id
    @Column(name="CODFOLCOLABORADOR", unique=true, nullable=false, precision=10)
    private int codfolcolaborador;
    @Column(name="DOCPROFNUMERO", length=40)
    private String docprofnumero;
    @Column(name="DOCPROFEMISSAO")
    private Timestamp docprofemissao;
    @Column(name="DOCPROFVALIDADE")
    private Timestamp docprofvalidade;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODFOLDEFICIENCIATIPO")
    private Foldeficienciatipo foldeficienciatipo;
    @ManyToOne
    @JoinColumn(name="CODFOLGRAUINSTRUCAO")
    private Folgrauinstrucao folgrauinstrucao;
    @ManyToOne
    @JoinColumn(name="CODFOLFUNCAO")
    private Folfuncao folfuncao;
    @ManyToOne
    @JoinColumn(name="DOCPROFUF")
    private Confuf confuf;
    @ManyToOne
    @JoinColumn(name="FILCODAGMAE")
    private Agagente agagente2;
    @ManyToOne
    @JoinColumn(name="FILCODAGPAI")
    private Agagente agagente3;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @OneToMany(mappedBy="folcolaborador")
    private Set<Folcolaboradorconvenio> folcolaboradorconvenio;
    @OneToMany(mappedBy="folcolaborador")
    private Set<Folcolaboradordep> folcolaboradordep;
    @OneToMany(mappedBy="folcolaborador")
    private Set<Folcolaboradoremprant> folcolaboradoremprant;
    @OneToMany(mappedBy="folcolaborador")
    private Set<Follancamento> follancamento;

    /** Default constructor. */
    public Folcolaborador() {
        super();
    }

    /**
     * Access method for codfolcolaborador.
     *
     * @return the current value of codfolcolaborador
     */
    public int getCodfolcolaborador() {
        return codfolcolaborador;
    }

    /**
     * Setter method for codfolcolaborador.
     *
     * @param aCodfolcolaborador the new value for codfolcolaborador
     */
    public void setCodfolcolaborador(int aCodfolcolaborador) {
        codfolcolaborador = aCodfolcolaborador;
    }

    /**
     * Access method for docprofnumero.
     *
     * @return the current value of docprofnumero
     */
    public String getDocprofnumero() {
        return docprofnumero;
    }

    /**
     * Setter method for docprofnumero.
     *
     * @param aDocprofnumero the new value for docprofnumero
     */
    public void setDocprofnumero(String aDocprofnumero) {
        docprofnumero = aDocprofnumero;
    }

    /**
     * Access method for docprofemissao.
     *
     * @return the current value of docprofemissao
     */
    public Timestamp getDocprofemissao() {
        return docprofemissao;
    }

    /**
     * Setter method for docprofemissao.
     *
     * @param aDocprofemissao the new value for docprofemissao
     */
    public void setDocprofemissao(Timestamp aDocprofemissao) {
        docprofemissao = aDocprofemissao;
    }

    /**
     * Access method for docprofvalidade.
     *
     * @return the current value of docprofvalidade
     */
    public Timestamp getDocprofvalidade() {
        return docprofvalidade;
    }

    /**
     * Setter method for docprofvalidade.
     *
     * @param aDocprofvalidade the new value for docprofvalidade
     */
    public void setDocprofvalidade(Timestamp aDocprofvalidade) {
        docprofvalidade = aDocprofvalidade;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for foldeficienciatipo.
     *
     * @return the current value of foldeficienciatipo
     */
    public Foldeficienciatipo getFoldeficienciatipo() {
        return foldeficienciatipo;
    }

    /**
     * Setter method for foldeficienciatipo.
     *
     * @param aFoldeficienciatipo the new value for foldeficienciatipo
     */
    public void setFoldeficienciatipo(Foldeficienciatipo aFoldeficienciatipo) {
        foldeficienciatipo = aFoldeficienciatipo;
    }

    /**
     * Access method for folgrauinstrucao.
     *
     * @return the current value of folgrauinstrucao
     */
    public Folgrauinstrucao getFolgrauinstrucao() {
        return folgrauinstrucao;
    }

    /**
     * Setter method for folgrauinstrucao.
     *
     * @param aFolgrauinstrucao the new value for folgrauinstrucao
     */
    public void setFolgrauinstrucao(Folgrauinstrucao aFolgrauinstrucao) {
        folgrauinstrucao = aFolgrauinstrucao;
    }

    /**
     * Access method for folfuncao.
     *
     * @return the current value of folfuncao
     */
    public Folfuncao getFolfuncao() {
        return folfuncao;
    }

    /**
     * Setter method for folfuncao.
     *
     * @param aFolfuncao the new value for folfuncao
     */
    public void setFolfuncao(Folfuncao aFolfuncao) {
        folfuncao = aFolfuncao;
    }

    /**
     * Access method for confuf.
     *
     * @return the current value of confuf
     */
    public Confuf getConfuf() {
        return confuf;
    }

    /**
     * Setter method for confuf.
     *
     * @param aConfuf the new value for confuf
     */
    public void setConfuf(Confuf aConfuf) {
        confuf = aConfuf;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Agagente getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Agagente aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for agagente3.
     *
     * @return the current value of agagente3
     */
    public Agagente getAgagente3() {
        return agagente3;
    }

    /**
     * Setter method for agagente3.
     *
     * @param aAgagente3 the new value for agagente3
     */
    public void setAgagente3(Agagente aAgagente3) {
        agagente3 = aAgagente3;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for folcolaboradorconvenio.
     *
     * @return the current value of folcolaboradorconvenio
     */
    public Set<Folcolaboradorconvenio> getFolcolaboradorconvenio() {
        return folcolaboradorconvenio;
    }

    /**
     * Setter method for folcolaboradorconvenio.
     *
     * @param aFolcolaboradorconvenio the new value for folcolaboradorconvenio
     */
    public void setFolcolaboradorconvenio(Set<Folcolaboradorconvenio> aFolcolaboradorconvenio) {
        folcolaboradorconvenio = aFolcolaboradorconvenio;
    }

    /**
     * Access method for folcolaboradordep.
     *
     * @return the current value of folcolaboradordep
     */
    public Set<Folcolaboradordep> getFolcolaboradordep() {
        return folcolaboradordep;
    }

    /**
     * Setter method for folcolaboradordep.
     *
     * @param aFolcolaboradordep the new value for folcolaboradordep
     */
    public void setFolcolaboradordep(Set<Folcolaboradordep> aFolcolaboradordep) {
        folcolaboradordep = aFolcolaboradordep;
    }

    /**
     * Access method for folcolaboradoremprant.
     *
     * @return the current value of folcolaboradoremprant
     */
    public Set<Folcolaboradoremprant> getFolcolaboradoremprant() {
        return folcolaboradoremprant;
    }

    /**
     * Setter method for folcolaboradoremprant.
     *
     * @param aFolcolaboradoremprant the new value for folcolaboradoremprant
     */
    public void setFolcolaboradoremprant(Set<Folcolaboradoremprant> aFolcolaboradoremprant) {
        folcolaboradoremprant = aFolcolaboradoremprant;
    }

    /**
     * Access method for follancamento.
     *
     * @return the current value of follancamento
     */
    public Set<Follancamento> getFollancamento() {
        return follancamento;
    }

    /**
     * Setter method for follancamento.
     *
     * @param aFollancamento the new value for follancamento
     */
    public void setFollancamento(Set<Follancamento> aFollancamento) {
        follancamento = aFollancamento;
    }

    /**
     * Compares the key for this instance with another Folcolaborador.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folcolaborador and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folcolaborador)) {
            return false;
        }
        Folcolaborador that = (Folcolaborador) other;
        if (this.getCodfolcolaborador() != that.getCodfolcolaborador()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folcolaborador.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folcolaborador)) return false;
        return this.equalKeys(other) && ((Folcolaborador)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolcolaborador();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folcolaborador |");
        sb.append(" codfolcolaborador=").append(getCodfolcolaborador());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolcolaborador", Integer.valueOf(getCodfolcolaborador()));
        return ret;
    }

}
