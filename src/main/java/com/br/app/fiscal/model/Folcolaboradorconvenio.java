package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLCOLABORADORCONVENIO")
public class Folcolaboradorconvenio implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolcolaboradorconvenio";

    @Id
    @Column(name="CODFOLCOLABORADORCONVENIO", unique=true, nullable=false, precision=10)
    private int codfolcolaboradorconvenio;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLCOLABORADOR", nullable=false)
    private Folcolaborador folcolaborador;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLCONVENIO", nullable=false)
    private Folconvenio folconvenio;

    /** Default constructor. */
    public Folcolaboradorconvenio() {
        super();
    }

    /**
     * Access method for codfolcolaboradorconvenio.
     *
     * @return the current value of codfolcolaboradorconvenio
     */
    public int getCodfolcolaboradorconvenio() {
        return codfolcolaboradorconvenio;
    }

    /**
     * Setter method for codfolcolaboradorconvenio.
     *
     * @param aCodfolcolaboradorconvenio the new value for codfolcolaboradorconvenio
     */
    public void setCodfolcolaboradorconvenio(int aCodfolcolaboradorconvenio) {
        codfolcolaboradorconvenio = aCodfolcolaboradorconvenio;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Folcolaborador getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Folcolaborador aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Access method for folconvenio.
     *
     * @return the current value of folconvenio
     */
    public Folconvenio getFolconvenio() {
        return folconvenio;
    }

    /**
     * Setter method for folconvenio.
     *
     * @param aFolconvenio the new value for folconvenio
     */
    public void setFolconvenio(Folconvenio aFolconvenio) {
        folconvenio = aFolconvenio;
    }

    /**
     * Compares the key for this instance with another Folcolaboradorconvenio.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folcolaboradorconvenio and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folcolaboradorconvenio)) {
            return false;
        }
        Folcolaboradorconvenio that = (Folcolaboradorconvenio) other;
        if (this.getCodfolcolaboradorconvenio() != that.getCodfolcolaboradorconvenio()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folcolaboradorconvenio.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folcolaboradorconvenio)) return false;
        return this.equalKeys(other) && ((Folcolaboradorconvenio)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolcolaboradorconvenio();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folcolaboradorconvenio |");
        sb.append(" codfolcolaboradorconvenio=").append(getCodfolcolaboradorconvenio());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolcolaboradorconvenio", Integer.valueOf(getCodfolcolaboradorconvenio()));
        return ret;
    }

}
