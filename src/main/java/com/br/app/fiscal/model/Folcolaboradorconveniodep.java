package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FOLCOLABORADORCONVENIODEP")
public class Folcolaboradorconveniodep implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolcolaboradorconveniodep";

    @Id
    @Column(name="CODFOLCOLABORADORCONVENIODEP", unique=true, nullable=false, precision=10)
    private int codfolcolaboradorconveniodep;
    @OneToMany(mappedBy="folcolaboradorconveniodep")
    private Set<Folcolaboradorconveniodep> folcolaboradorconveniodepM;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLCOLABORADORDEP", nullable=false)
    private Folcolaboradorconveniodep folcolaboradorconveniodep;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLCONVENIO", nullable=false)
    private Folconvenio folconvenio;

    /** Default constructor. */
    public Folcolaboradorconveniodep() {
        super();
    }

    /**
     * Access method for codfolcolaboradorconveniodep.
     *
     * @return the current value of codfolcolaboradorconveniodep
     */
    public int getCodfolcolaboradorconveniodep() {
        return codfolcolaboradorconveniodep;
    }

    /**
     * Setter method for codfolcolaboradorconveniodep.
     *
     * @param aCodfolcolaboradorconveniodep the new value for codfolcolaboradorconveniodep
     */
    public void setCodfolcolaboradorconveniodep(int aCodfolcolaboradorconveniodep) {
        codfolcolaboradorconveniodep = aCodfolcolaboradorconveniodep;
    }

    /**
     * Access method for folcolaboradorconveniodepM.
     *
     * @return the current value of folcolaboradorconveniodepM
     */
    public Set<Folcolaboradorconveniodep> getFolcolaboradorconveniodepM() {
        return folcolaboradorconveniodepM;
    }

    /**
     * Setter method for folcolaboradorconveniodepM.
     *
     * @param aFolcolaboradorconveniodepM the new value for folcolaboradorconveniodepM
     */
    public void setFolcolaboradorconveniodepM(Set<Folcolaboradorconveniodep> aFolcolaboradorconveniodepM) {
        folcolaboradorconveniodepM = aFolcolaboradorconveniodepM;
    }

    /**
     * Access method for folcolaboradorconveniodep.
     *
     * @return the current value of folcolaboradorconveniodep
     */
    public Folcolaboradorconveniodep getFolcolaboradorconveniodep() {
        return folcolaboradorconveniodep;
    }

    /**
     * Setter method for folcolaboradorconveniodep.
     *
     * @param aFolcolaboradorconveniodep the new value for folcolaboradorconveniodep
     */
    public void setFolcolaboradorconveniodep(Folcolaboradorconveniodep aFolcolaboradorconveniodep) {
        folcolaboradorconveniodep = aFolcolaboradorconveniodep;
    }

    /**
     * Access method for folconvenio.
     *
     * @return the current value of folconvenio
     */
    public Folconvenio getFolconvenio() {
        return folconvenio;
    }

    /**
     * Setter method for folconvenio.
     *
     * @param aFolconvenio the new value for folconvenio
     */
    public void setFolconvenio(Folconvenio aFolconvenio) {
        folconvenio = aFolconvenio;
    }

    /**
     * Compares the key for this instance with another Folcolaboradorconveniodep.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folcolaboradorconveniodep and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folcolaboradorconveniodep)) {
            return false;
        }
        Folcolaboradorconveniodep that = (Folcolaboradorconveniodep) other;
        if (this.getCodfolcolaboradorconveniodep() != that.getCodfolcolaboradorconveniodep()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folcolaboradorconveniodep.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folcolaboradorconveniodep)) return false;
        return this.equalKeys(other) && ((Folcolaboradorconveniodep)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolcolaboradorconveniodep();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folcolaboradorconveniodep |");
        sb.append(" codfolcolaboradorconveniodep=").append(getCodfolcolaboradorconveniodep());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolcolaboradorconveniodep", Integer.valueOf(getCodfolcolaboradorconveniodep()));
        return ret;
    }

}
