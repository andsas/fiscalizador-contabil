package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLCOLABORADORDEP")
public class Folcolaboradordep implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolcolaboradordep";

    @Id
    @Column(name="CODFOLCOLABORADORDEP", unique=true, nullable=false, precision=10)
    private int codfolcolaboradordep;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLCOLABORADOR", nullable=false)
    private Folcolaborador folcolaborador;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLPARENTESCO", nullable=false)
    private Folparentesco folparentesco;

    /** Default constructor. */
    public Folcolaboradordep() {
        super();
    }

    /**
     * Access method for codfolcolaboradordep.
     *
     * @return the current value of codfolcolaboradordep
     */
    public int getCodfolcolaboradordep() {
        return codfolcolaboradordep;
    }

    /**
     * Setter method for codfolcolaboradordep.
     *
     * @param aCodfolcolaboradordep the new value for codfolcolaboradordep
     */
    public void setCodfolcolaboradordep(int aCodfolcolaboradordep) {
        codfolcolaboradordep = aCodfolcolaboradordep;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Folcolaborador getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Folcolaborador aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for folparentesco.
     *
     * @return the current value of folparentesco
     */
    public Folparentesco getFolparentesco() {
        return folparentesco;
    }

    /**
     * Setter method for folparentesco.
     *
     * @param aFolparentesco the new value for folparentesco
     */
    public void setFolparentesco(Folparentesco aFolparentesco) {
        folparentesco = aFolparentesco;
    }

    /**
     * Compares the key for this instance with another Folcolaboradordep.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folcolaboradordep and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folcolaboradordep)) {
            return false;
        }
        Folcolaboradordep that = (Folcolaboradordep) other;
        if (this.getCodfolcolaboradordep() != that.getCodfolcolaboradordep()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folcolaboradordep.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folcolaboradordep)) return false;
        return this.equalKeys(other) && ((Folcolaboradordep)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolcolaboradordep();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folcolaboradordep |");
        sb.append(" codfolcolaboradordep=").append(getCodfolcolaboradordep());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolcolaboradordep", Integer.valueOf(getCodfolcolaboradordep()));
        return ret;
    }

}
