package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLCOLABORADOREMPRANT")
public class Folcolaboradoremprant implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolcolaboradoremprant";

    @Id
    @Column(name="CODFOLCOLABORADOREMPRANT", unique=true, nullable=false, precision=10)
    private int codfolcolaboradoremprant;
    @Column(name="NOMEFOLCOLABORADOREMPRANT", nullable=false, length=250)
    private String nomefolcolaboradoremprant;
    @Column(name="DATAADMISSAO")
    private Timestamp dataadmissao;
    @Column(name="DATADEMISSAO", nullable=false)
    private Timestamp datademissao;
    @Column(name="ULTIMOSALARIO", precision=15, scale=4)
    private BigDecimal ultimosalario;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLCOLABORADOR", nullable=false)
    private Folcolaborador folcolaborador;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLFUNCAO", nullable=false)
    private Folfuncao folfuncao;
    @ManyToOne
    @JoinColumn(name="CODFOLLOCALTRABALHO")
    private Follocaltrabalho follocaltrabalho;

    /** Default constructor. */
    public Folcolaboradoremprant() {
        super();
    }

    /**
     * Access method for codfolcolaboradoremprant.
     *
     * @return the current value of codfolcolaboradoremprant
     */
    public int getCodfolcolaboradoremprant() {
        return codfolcolaboradoremprant;
    }

    /**
     * Setter method for codfolcolaboradoremprant.
     *
     * @param aCodfolcolaboradoremprant the new value for codfolcolaboradoremprant
     */
    public void setCodfolcolaboradoremprant(int aCodfolcolaboradoremprant) {
        codfolcolaboradoremprant = aCodfolcolaboradoremprant;
    }

    /**
     * Access method for nomefolcolaboradoremprant.
     *
     * @return the current value of nomefolcolaboradoremprant
     */
    public String getNomefolcolaboradoremprant() {
        return nomefolcolaboradoremprant;
    }

    /**
     * Setter method for nomefolcolaboradoremprant.
     *
     * @param aNomefolcolaboradoremprant the new value for nomefolcolaboradoremprant
     */
    public void setNomefolcolaboradoremprant(String aNomefolcolaboradoremprant) {
        nomefolcolaboradoremprant = aNomefolcolaboradoremprant;
    }

    /**
     * Access method for dataadmissao.
     *
     * @return the current value of dataadmissao
     */
    public Timestamp getDataadmissao() {
        return dataadmissao;
    }

    /**
     * Setter method for dataadmissao.
     *
     * @param aDataadmissao the new value for dataadmissao
     */
    public void setDataadmissao(Timestamp aDataadmissao) {
        dataadmissao = aDataadmissao;
    }

    /**
     * Access method for datademissao.
     *
     * @return the current value of datademissao
     */
    public Timestamp getDatademissao() {
        return datademissao;
    }

    /**
     * Setter method for datademissao.
     *
     * @param aDatademissao the new value for datademissao
     */
    public void setDatademissao(Timestamp aDatademissao) {
        datademissao = aDatademissao;
    }

    /**
     * Access method for ultimosalario.
     *
     * @return the current value of ultimosalario
     */
    public BigDecimal getUltimosalario() {
        return ultimosalario;
    }

    /**
     * Setter method for ultimosalario.
     *
     * @param aUltimosalario the new value for ultimosalario
     */
    public void setUltimosalario(BigDecimal aUltimosalario) {
        ultimosalario = aUltimosalario;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Folcolaborador getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Folcolaborador aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Access method for folfuncao.
     *
     * @return the current value of folfuncao
     */
    public Folfuncao getFolfuncao() {
        return folfuncao;
    }

    /**
     * Setter method for folfuncao.
     *
     * @param aFolfuncao the new value for folfuncao
     */
    public void setFolfuncao(Folfuncao aFolfuncao) {
        folfuncao = aFolfuncao;
    }

    /**
     * Access method for follocaltrabalho.
     *
     * @return the current value of follocaltrabalho
     */
    public Follocaltrabalho getFollocaltrabalho() {
        return follocaltrabalho;
    }

    /**
     * Setter method for follocaltrabalho.
     *
     * @param aFollocaltrabalho the new value for follocaltrabalho
     */
    public void setFollocaltrabalho(Follocaltrabalho aFollocaltrabalho) {
        follocaltrabalho = aFollocaltrabalho;
    }

    /**
     * Compares the key for this instance with another Folcolaboradoremprant.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folcolaboradoremprant and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folcolaboradoremprant)) {
            return false;
        }
        Folcolaboradoremprant that = (Folcolaboradoremprant) other;
        if (this.getCodfolcolaboradoremprant() != that.getCodfolcolaboradoremprant()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folcolaboradoremprant.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folcolaboradoremprant)) return false;
        return this.equalKeys(other) && ((Folcolaboradoremprant)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolcolaboradoremprant();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folcolaboradoremprant |");
        sb.append(" codfolcolaboradoremprant=").append(getCodfolcolaboradoremprant());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolcolaboradoremprant", Integer.valueOf(getCodfolcolaboradoremprant()));
        return ret;
    }

}
