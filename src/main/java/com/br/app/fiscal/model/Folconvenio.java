package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FOLCONVENIO")
public class Folconvenio implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolconvenio";

    @Id
    @Column(name="CODFOLCONVENIO", unique=true, nullable=false, precision=10)
    private int codfolconvenio;
    @Column(name="DESCFOLCONVENIO", nullable=false, length=250)
    private String descfolconvenio;
    @Column(name="CODOPERADORAANS", length=40)
    private String codoperadoraans;
    @Column(name="EMPRVALORPAGOTITULAR", precision=15, scale=4)
    private BigDecimal emprvalorpagotitular;
    @Column(name="EMPRVALORPAGODEP", precision=15, scale=4)
    private BigDecimal emprvalorpagodep;
    @Column(name="COLABVALORPAGOTITULAR", precision=15, scale=4)
    private BigDecimal colabvalorpagotitular;
    @Column(name="COLABVALORPAGODEP", precision=15, scale=4)
    private BigDecimal colabvalorpagodep;
    @OneToMany(mappedBy="folconvenio")
    private Set<Folcolaboradorconvenio> folcolaboradorconvenio;
    @OneToMany(mappedBy="folconvenio")
    private Set<Folcolaboradorconveniodep> folcolaboradorconveniodep;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTEOPERADORA", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLTIPOCONVENIO", nullable=false)
    private Folconveniotipo folconveniotipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODAPOLICE", nullable=false)
    private Prodapolice prodapolice;

    /** Default constructor. */
    public Folconvenio() {
        super();
    }

    /**
     * Access method for codfolconvenio.
     *
     * @return the current value of codfolconvenio
     */
    public int getCodfolconvenio() {
        return codfolconvenio;
    }

    /**
     * Setter method for codfolconvenio.
     *
     * @param aCodfolconvenio the new value for codfolconvenio
     */
    public void setCodfolconvenio(int aCodfolconvenio) {
        codfolconvenio = aCodfolconvenio;
    }

    /**
     * Access method for descfolconvenio.
     *
     * @return the current value of descfolconvenio
     */
    public String getDescfolconvenio() {
        return descfolconvenio;
    }

    /**
     * Setter method for descfolconvenio.
     *
     * @param aDescfolconvenio the new value for descfolconvenio
     */
    public void setDescfolconvenio(String aDescfolconvenio) {
        descfolconvenio = aDescfolconvenio;
    }

    /**
     * Access method for codoperadoraans.
     *
     * @return the current value of codoperadoraans
     */
    public String getCodoperadoraans() {
        return codoperadoraans;
    }

    /**
     * Setter method for codoperadoraans.
     *
     * @param aCodoperadoraans the new value for codoperadoraans
     */
    public void setCodoperadoraans(String aCodoperadoraans) {
        codoperadoraans = aCodoperadoraans;
    }

    /**
     * Access method for emprvalorpagotitular.
     *
     * @return the current value of emprvalorpagotitular
     */
    public BigDecimal getEmprvalorpagotitular() {
        return emprvalorpagotitular;
    }

    /**
     * Setter method for emprvalorpagotitular.
     *
     * @param aEmprvalorpagotitular the new value for emprvalorpagotitular
     */
    public void setEmprvalorpagotitular(BigDecimal aEmprvalorpagotitular) {
        emprvalorpagotitular = aEmprvalorpagotitular;
    }

    /**
     * Access method for emprvalorpagodep.
     *
     * @return the current value of emprvalorpagodep
     */
    public BigDecimal getEmprvalorpagodep() {
        return emprvalorpagodep;
    }

    /**
     * Setter method for emprvalorpagodep.
     *
     * @param aEmprvalorpagodep the new value for emprvalorpagodep
     */
    public void setEmprvalorpagodep(BigDecimal aEmprvalorpagodep) {
        emprvalorpagodep = aEmprvalorpagodep;
    }

    /**
     * Access method for colabvalorpagotitular.
     *
     * @return the current value of colabvalorpagotitular
     */
    public BigDecimal getColabvalorpagotitular() {
        return colabvalorpagotitular;
    }

    /**
     * Setter method for colabvalorpagotitular.
     *
     * @param aColabvalorpagotitular the new value for colabvalorpagotitular
     */
    public void setColabvalorpagotitular(BigDecimal aColabvalorpagotitular) {
        colabvalorpagotitular = aColabvalorpagotitular;
    }

    /**
     * Access method for colabvalorpagodep.
     *
     * @return the current value of colabvalorpagodep
     */
    public BigDecimal getColabvalorpagodep() {
        return colabvalorpagodep;
    }

    /**
     * Setter method for colabvalorpagodep.
     *
     * @param aColabvalorpagodep the new value for colabvalorpagodep
     */
    public void setColabvalorpagodep(BigDecimal aColabvalorpagodep) {
        colabvalorpagodep = aColabvalorpagodep;
    }

    /**
     * Access method for folcolaboradorconvenio.
     *
     * @return the current value of folcolaboradorconvenio
     */
    public Set<Folcolaboradorconvenio> getFolcolaboradorconvenio() {
        return folcolaboradorconvenio;
    }

    /**
     * Setter method for folcolaboradorconvenio.
     *
     * @param aFolcolaboradorconvenio the new value for folcolaboradorconvenio
     */
    public void setFolcolaboradorconvenio(Set<Folcolaboradorconvenio> aFolcolaboradorconvenio) {
        folcolaboradorconvenio = aFolcolaboradorconvenio;
    }

    /**
     * Access method for folcolaboradorconveniodep.
     *
     * @return the current value of folcolaboradorconveniodep
     */
    public Set<Folcolaboradorconveniodep> getFolcolaboradorconveniodep() {
        return folcolaboradorconveniodep;
    }

    /**
     * Setter method for folcolaboradorconveniodep.
     *
     * @param aFolcolaboradorconveniodep the new value for folcolaboradorconveniodep
     */
    public void setFolcolaboradorconveniodep(Set<Folcolaboradorconveniodep> aFolcolaboradorconveniodep) {
        folcolaboradorconveniodep = aFolcolaboradorconveniodep;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for folconveniotipo.
     *
     * @return the current value of folconveniotipo
     */
    public Folconveniotipo getFolconveniotipo() {
        return folconveniotipo;
    }

    /**
     * Setter method for folconveniotipo.
     *
     * @param aFolconveniotipo the new value for folconveniotipo
     */
    public void setFolconveniotipo(Folconveniotipo aFolconveniotipo) {
        folconveniotipo = aFolconveniotipo;
    }

    /**
     * Access method for prodapolice.
     *
     * @return the current value of prodapolice
     */
    public Prodapolice getProdapolice() {
        return prodapolice;
    }

    /**
     * Setter method for prodapolice.
     *
     * @param aProdapolice the new value for prodapolice
     */
    public void setProdapolice(Prodapolice aProdapolice) {
        prodapolice = aProdapolice;
    }

    /**
     * Compares the key for this instance with another Folconvenio.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folconvenio and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folconvenio)) {
            return false;
        }
        Folconvenio that = (Folconvenio) other;
        if (this.getCodfolconvenio() != that.getCodfolconvenio()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folconvenio.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folconvenio)) return false;
        return this.equalKeys(other) && ((Folconvenio)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolconvenio();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folconvenio |");
        sb.append(" codfolconvenio=").append(getCodfolconvenio());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolconvenio", Integer.valueOf(getCodfolconvenio()));
        return ret;
    }

}
