package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FOLCONVENIOTIPO")
public class Folconveniotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolconveniotipo";

    @Id
    @Column(name="CODFOLCONVENIOTIPO", unique=true, nullable=false, precision=10)
    private int codfolconveniotipo;
    @Column(name="DESCFOLCONVENIOTIPO", nullable=false, length=250)
    private String descfolconveniotipo;
    @OneToMany(mappedBy="folconveniotipo")
    private Set<Folconvenio> folconvenio;
    @ManyToOne
    @JoinColumn(name="CODAGAGENTE")
    private Agagente agagente;

    /** Default constructor. */
    public Folconveniotipo() {
        super();
    }

    /**
     * Access method for codfolconveniotipo.
     *
     * @return the current value of codfolconveniotipo
     */
    public int getCodfolconveniotipo() {
        return codfolconveniotipo;
    }

    /**
     * Setter method for codfolconveniotipo.
     *
     * @param aCodfolconveniotipo the new value for codfolconveniotipo
     */
    public void setCodfolconveniotipo(int aCodfolconveniotipo) {
        codfolconveniotipo = aCodfolconveniotipo;
    }

    /**
     * Access method for descfolconveniotipo.
     *
     * @return the current value of descfolconveniotipo
     */
    public String getDescfolconveniotipo() {
        return descfolconveniotipo;
    }

    /**
     * Setter method for descfolconveniotipo.
     *
     * @param aDescfolconveniotipo the new value for descfolconveniotipo
     */
    public void setDescfolconveniotipo(String aDescfolconveniotipo) {
        descfolconveniotipo = aDescfolconveniotipo;
    }

    /**
     * Access method for folconvenio.
     *
     * @return the current value of folconvenio
     */
    public Set<Folconvenio> getFolconvenio() {
        return folconvenio;
    }

    /**
     * Setter method for folconvenio.
     *
     * @param aFolconvenio the new value for folconvenio
     */
    public void setFolconvenio(Set<Folconvenio> aFolconvenio) {
        folconvenio = aFolconvenio;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Compares the key for this instance with another Folconveniotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folconveniotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folconveniotipo)) {
            return false;
        }
        Folconveniotipo that = (Folconveniotipo) other;
        if (this.getCodfolconveniotipo() != that.getCodfolconveniotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folconveniotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folconveniotipo)) return false;
        return this.equalKeys(other) && ((Folconveniotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolconveniotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folconveniotipo |");
        sb.append(" codfolconveniotipo=").append(getCodfolconveniotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolconveniotipo", Integer.valueOf(getCodfolconveniotipo()));
        return ret;
    }

}
