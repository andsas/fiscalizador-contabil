package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FOLDEFICIENCIATIPO")
public class Foldeficienciatipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfoldeficienciatipo";

    @Id
    @Column(name="CODFOLDEFICIENCIATIPO", unique=true, nullable=false, precision=10)
    private int codfoldeficienciatipo;
    @Column(name="DESCFOLDEFICIENCIATIPO", nullable=false, length=250)
    private String descfoldeficienciatipo;
    @OneToMany(mappedBy="foldeficienciatipo")
    private Set<Folcolaborador> folcolaborador;

    /** Default constructor. */
    public Foldeficienciatipo() {
        super();
    }

    /**
     * Access method for codfoldeficienciatipo.
     *
     * @return the current value of codfoldeficienciatipo
     */
    public int getCodfoldeficienciatipo() {
        return codfoldeficienciatipo;
    }

    /**
     * Setter method for codfoldeficienciatipo.
     *
     * @param aCodfoldeficienciatipo the new value for codfoldeficienciatipo
     */
    public void setCodfoldeficienciatipo(int aCodfoldeficienciatipo) {
        codfoldeficienciatipo = aCodfoldeficienciatipo;
    }

    /**
     * Access method for descfoldeficienciatipo.
     *
     * @return the current value of descfoldeficienciatipo
     */
    public String getDescfoldeficienciatipo() {
        return descfoldeficienciatipo;
    }

    /**
     * Setter method for descfoldeficienciatipo.
     *
     * @param aDescfoldeficienciatipo the new value for descfoldeficienciatipo
     */
    public void setDescfoldeficienciatipo(String aDescfoldeficienciatipo) {
        descfoldeficienciatipo = aDescfoldeficienciatipo;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Set<Folcolaborador> getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Set<Folcolaborador> aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Compares the key for this instance with another Foldeficienciatipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Foldeficienciatipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Foldeficienciatipo)) {
            return false;
        }
        Foldeficienciatipo that = (Foldeficienciatipo) other;
        if (this.getCodfoldeficienciatipo() != that.getCodfoldeficienciatipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Foldeficienciatipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Foldeficienciatipo)) return false;
        return this.equalKeys(other) && ((Foldeficienciatipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfoldeficienciatipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Foldeficienciatipo |");
        sb.append(" codfoldeficienciatipo=").append(getCodfoldeficienciatipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfoldeficienciatipo", Integer.valueOf(getCodfoldeficienciatipo()));
        return ret;
    }

}
