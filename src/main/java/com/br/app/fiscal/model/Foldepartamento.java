package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FOLDEPARTAMENTO")
public class Foldepartamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfoldepartamento";

    @Id
    @Column(name="CODFOLDEPARTAMENTO", unique=true, nullable=false, precision=10)
    private int codfoldepartamento;
    @Column(name="DESCFOLDEPARTAMENTO", nullable=false, length=250)
    private String descfoldepartamento;
    @OneToMany(mappedBy="foldepartamento")
    private Set<Folfuncao> folfuncao;
    @OneToMany(mappedBy="foldepartamento")
    private Set<Follocaltrabalho> follocaltrabalho;

    /** Default constructor. */
    public Foldepartamento() {
        super();
    }

    /**
     * Access method for codfoldepartamento.
     *
     * @return the current value of codfoldepartamento
     */
    public int getCodfoldepartamento() {
        return codfoldepartamento;
    }

    /**
     * Setter method for codfoldepartamento.
     *
     * @param aCodfoldepartamento the new value for codfoldepartamento
     */
    public void setCodfoldepartamento(int aCodfoldepartamento) {
        codfoldepartamento = aCodfoldepartamento;
    }

    /**
     * Access method for descfoldepartamento.
     *
     * @return the current value of descfoldepartamento
     */
    public String getDescfoldepartamento() {
        return descfoldepartamento;
    }

    /**
     * Setter method for descfoldepartamento.
     *
     * @param aDescfoldepartamento the new value for descfoldepartamento
     */
    public void setDescfoldepartamento(String aDescfoldepartamento) {
        descfoldepartamento = aDescfoldepartamento;
    }

    /**
     * Access method for folfuncao.
     *
     * @return the current value of folfuncao
     */
    public Set<Folfuncao> getFolfuncao() {
        return folfuncao;
    }

    /**
     * Setter method for folfuncao.
     *
     * @param aFolfuncao the new value for folfuncao
     */
    public void setFolfuncao(Set<Folfuncao> aFolfuncao) {
        folfuncao = aFolfuncao;
    }

    /**
     * Access method for follocaltrabalho.
     *
     * @return the current value of follocaltrabalho
     */
    public Set<Follocaltrabalho> getFollocaltrabalho() {
        return follocaltrabalho;
    }

    /**
     * Setter method for follocaltrabalho.
     *
     * @param aFollocaltrabalho the new value for follocaltrabalho
     */
    public void setFollocaltrabalho(Set<Follocaltrabalho> aFollocaltrabalho) {
        follocaltrabalho = aFollocaltrabalho;
    }

    /**
     * Compares the key for this instance with another Foldepartamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Foldepartamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Foldepartamento)) {
            return false;
        }
        Foldepartamento that = (Foldepartamento) other;
        if (this.getCodfoldepartamento() != that.getCodfoldepartamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Foldepartamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Foldepartamento)) return false;
        return this.equalKeys(other) && ((Foldepartamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfoldepartamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Foldepartamento |");
        sb.append(" codfoldepartamento=").append(getCodfoldepartamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfoldepartamento", Integer.valueOf(getCodfoldepartamento()));
        return ret;
    }

}
