package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FOLEVENTO")
public class Folevento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolevento";

    @Id
    @Column(name="CODFOLEVENTO", unique=true, nullable=false, precision=10)
    private int codfolevento;
    @Column(name="DESCFOLEVENTO", nullable=false, length=250)
    private String descfolevento;
    @Column(name="CODFOLTIPOEVENTO", nullable=false, precision=10)
    private int codfoltipoevento;
    @Column(name="INCSEFIP", precision=5)
    private short incsefip;
    @Column(name="INCCAGED", precision=5)
    private short inccaged;
    @Column(name="INCGPS", precision=5)
    private short incgps;
    @Column(name="INCRAIS", precision=5)
    private short incrais;
    @Column(name="INCDIRF", precision=5)
    private short incdirf;
    @Column(name="INCESOCIAL", precision=5)
    private short incesocial;
    @Column(name="INCGRRF", precision=5)
    private short incgrrf;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @ManyToOne
    @JoinColumn(name="CODCTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CODORCDEBITO")
    private Orcplanoconta orcplanoconta;
    @ManyToOne
    @JoinColumn(name="CODORCCREDITO")
    private Orcplanoconta orcplanoconta2;
    @ManyToOne
    @JoinColumn(name="CODORCHISTORICO")
    private Ctbhistorico ctbhistorico4;
    @ManyToOne
    @JoinColumn(name="CODFINTIPO")
    private Fintipo fintipo;
    @ManyToOne
    @JoinColumn(name="CODCOBRFORMA")
    private Cobrforma cobrforma;
    @ManyToOne
    @JoinColumn(name="CODCTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta2;
    @ManyToOne
    @JoinColumn(name="CODFINCONTACREDITO")
    private Finplanoconta finplanoconta;
    @ManyToOne
    @JoinColumn(name="CODFINCONTADEBITO")
    private Finplanoconta finplanoconta2;
    @ManyToOne
    @JoinColumn(name="CODFINFLUXOCONTADEBITO")
    private Finfluxoplanoconta finfluxoplanoconta;
    @ManyToOne
    @JoinColumn(name="CODFINFLUXOCONTACREDITO")
    private Finfluxoplanoconta finfluxoplanoconta2;
    @ManyToOne
    @JoinColumn(name="CODCTBHISTORICO")
    private Ctbhistorico ctbhistorico;
    @ManyToOne
    @JoinColumn(name="CODFINHISTORICO")
    private Ctbhistorico ctbhistorico2;
    @ManyToOne
    @JoinColumn(name="CODFINFLUXOHISTORICO")
    private Ctbhistorico ctbhistorico3;
    @OneToMany(mappedBy="folevento")
    private Set<Follancamento> follancamento;

    /** Default constructor. */
    public Folevento() {
        super();
    }

    /**
     * Access method for codfolevento.
     *
     * @return the current value of codfolevento
     */
    public int getCodfolevento() {
        return codfolevento;
    }

    /**
     * Setter method for codfolevento.
     *
     * @param aCodfolevento the new value for codfolevento
     */
    public void setCodfolevento(int aCodfolevento) {
        codfolevento = aCodfolevento;
    }

    /**
     * Access method for descfolevento.
     *
     * @return the current value of descfolevento
     */
    public String getDescfolevento() {
        return descfolevento;
    }

    /**
     * Setter method for descfolevento.
     *
     * @param aDescfolevento the new value for descfolevento
     */
    public void setDescfolevento(String aDescfolevento) {
        descfolevento = aDescfolevento;
    }

    /**
     * Access method for codfoltipoevento.
     *
     * @return the current value of codfoltipoevento
     */
    public int getCodfoltipoevento() {
        return codfoltipoevento;
    }

    /**
     * Setter method for codfoltipoevento.
     *
     * @param aCodfoltipoevento the new value for codfoltipoevento
     */
    public void setCodfoltipoevento(int aCodfoltipoevento) {
        codfoltipoevento = aCodfoltipoevento;
    }

    /**
     * Access method for incsefip.
     *
     * @return the current value of incsefip
     */
    public short getIncsefip() {
        return incsefip;
    }

    /**
     * Setter method for incsefip.
     *
     * @param aIncsefip the new value for incsefip
     */
    public void setIncsefip(short aIncsefip) {
        incsefip = aIncsefip;
    }

    /**
     * Access method for inccaged.
     *
     * @return the current value of inccaged
     */
    public short getInccaged() {
        return inccaged;
    }

    /**
     * Setter method for inccaged.
     *
     * @param aInccaged the new value for inccaged
     */
    public void setInccaged(short aInccaged) {
        inccaged = aInccaged;
    }

    /**
     * Access method for incgps.
     *
     * @return the current value of incgps
     */
    public short getIncgps() {
        return incgps;
    }

    /**
     * Setter method for incgps.
     *
     * @param aIncgps the new value for incgps
     */
    public void setIncgps(short aIncgps) {
        incgps = aIncgps;
    }

    /**
     * Access method for incrais.
     *
     * @return the current value of incrais
     */
    public short getIncrais() {
        return incrais;
    }

    /**
     * Setter method for incrais.
     *
     * @param aIncrais the new value for incrais
     */
    public void setIncrais(short aIncrais) {
        incrais = aIncrais;
    }

    /**
     * Access method for incdirf.
     *
     * @return the current value of incdirf
     */
    public short getIncdirf() {
        return incdirf;
    }

    /**
     * Setter method for incdirf.
     *
     * @param aIncdirf the new value for incdirf
     */
    public void setIncdirf(short aIncdirf) {
        incdirf = aIncdirf;
    }

    /**
     * Access method for incesocial.
     *
     * @return the current value of incesocial
     */
    public short getIncesocial() {
        return incesocial;
    }

    /**
     * Setter method for incesocial.
     *
     * @param aIncesocial the new value for incesocial
     */
    public void setIncesocial(short aIncesocial) {
        incesocial = aIncesocial;
    }

    /**
     * Access method for incgrrf.
     *
     * @return the current value of incgrrf
     */
    public short getIncgrrf() {
        return incgrrf;
    }

    /**
     * Setter method for incgrrf.
     *
     * @param aIncgrrf the new value for incgrrf
     */
    public void setIncgrrf(short aIncgrrf) {
        incgrrf = aIncgrrf;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for orcplanoconta.
     *
     * @return the current value of orcplanoconta
     */
    public Orcplanoconta getOrcplanoconta() {
        return orcplanoconta;
    }

    /**
     * Setter method for orcplanoconta.
     *
     * @param aOrcplanoconta the new value for orcplanoconta
     */
    public void setOrcplanoconta(Orcplanoconta aOrcplanoconta) {
        orcplanoconta = aOrcplanoconta;
    }

    /**
     * Access method for orcplanoconta2.
     *
     * @return the current value of orcplanoconta2
     */
    public Orcplanoconta getOrcplanoconta2() {
        return orcplanoconta2;
    }

    /**
     * Setter method for orcplanoconta2.
     *
     * @param aOrcplanoconta2 the new value for orcplanoconta2
     */
    public void setOrcplanoconta2(Orcplanoconta aOrcplanoconta2) {
        orcplanoconta2 = aOrcplanoconta2;
    }

    /**
     * Access method for ctbhistorico4.
     *
     * @return the current value of ctbhistorico4
     */
    public Ctbhistorico getCtbhistorico4() {
        return ctbhistorico4;
    }

    /**
     * Setter method for ctbhistorico4.
     *
     * @param aCtbhistorico4 the new value for ctbhistorico4
     */
    public void setCtbhistorico4(Ctbhistorico aCtbhistorico4) {
        ctbhistorico4 = aCtbhistorico4;
    }

    /**
     * Access method for fintipo.
     *
     * @return the current value of fintipo
     */
    public Fintipo getFintipo() {
        return fintipo;
    }

    /**
     * Setter method for fintipo.
     *
     * @param aFintipo the new value for fintipo
     */
    public void setFintipo(Fintipo aFintipo) {
        fintipo = aFintipo;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Cobrforma getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Cobrforma aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for finplanoconta.
     *
     * @return the current value of finplanoconta
     */
    public Finplanoconta getFinplanoconta() {
        return finplanoconta;
    }

    /**
     * Setter method for finplanoconta.
     *
     * @param aFinplanoconta the new value for finplanoconta
     */
    public void setFinplanoconta(Finplanoconta aFinplanoconta) {
        finplanoconta = aFinplanoconta;
    }

    /**
     * Access method for finplanoconta2.
     *
     * @return the current value of finplanoconta2
     */
    public Finplanoconta getFinplanoconta2() {
        return finplanoconta2;
    }

    /**
     * Setter method for finplanoconta2.
     *
     * @param aFinplanoconta2 the new value for finplanoconta2
     */
    public void setFinplanoconta2(Finplanoconta aFinplanoconta2) {
        finplanoconta2 = aFinplanoconta2;
    }

    /**
     * Access method for finfluxoplanoconta.
     *
     * @return the current value of finfluxoplanoconta
     */
    public Finfluxoplanoconta getFinfluxoplanoconta() {
        return finfluxoplanoconta;
    }

    /**
     * Setter method for finfluxoplanoconta.
     *
     * @param aFinfluxoplanoconta the new value for finfluxoplanoconta
     */
    public void setFinfluxoplanoconta(Finfluxoplanoconta aFinfluxoplanoconta) {
        finfluxoplanoconta = aFinfluxoplanoconta;
    }

    /**
     * Access method for finfluxoplanoconta2.
     *
     * @return the current value of finfluxoplanoconta2
     */
    public Finfluxoplanoconta getFinfluxoplanoconta2() {
        return finfluxoplanoconta2;
    }

    /**
     * Setter method for finfluxoplanoconta2.
     *
     * @param aFinfluxoplanoconta2 the new value for finfluxoplanoconta2
     */
    public void setFinfluxoplanoconta2(Finfluxoplanoconta aFinfluxoplanoconta2) {
        finfluxoplanoconta2 = aFinfluxoplanoconta2;
    }

    /**
     * Access method for ctbhistorico.
     *
     * @return the current value of ctbhistorico
     */
    public Ctbhistorico getCtbhistorico() {
        return ctbhistorico;
    }

    /**
     * Setter method for ctbhistorico.
     *
     * @param aCtbhistorico the new value for ctbhistorico
     */
    public void setCtbhistorico(Ctbhistorico aCtbhistorico) {
        ctbhistorico = aCtbhistorico;
    }

    /**
     * Access method for ctbhistorico2.
     *
     * @return the current value of ctbhistorico2
     */
    public Ctbhistorico getCtbhistorico2() {
        return ctbhistorico2;
    }

    /**
     * Setter method for ctbhistorico2.
     *
     * @param aCtbhistorico2 the new value for ctbhistorico2
     */
    public void setCtbhistorico2(Ctbhistorico aCtbhistorico2) {
        ctbhistorico2 = aCtbhistorico2;
    }

    /**
     * Access method for ctbhistorico3.
     *
     * @return the current value of ctbhistorico3
     */
    public Ctbhistorico getCtbhistorico3() {
        return ctbhistorico3;
    }

    /**
     * Setter method for ctbhistorico3.
     *
     * @param aCtbhistorico3 the new value for ctbhistorico3
     */
    public void setCtbhistorico3(Ctbhistorico aCtbhistorico3) {
        ctbhistorico3 = aCtbhistorico3;
    }

    /**
     * Access method for follancamento.
     *
     * @return the current value of follancamento
     */
    public Set<Follancamento> getFollancamento() {
        return follancamento;
    }

    /**
     * Setter method for follancamento.
     *
     * @param aFollancamento the new value for follancamento
     */
    public void setFollancamento(Set<Follancamento> aFollancamento) {
        follancamento = aFollancamento;
    }

    /**
     * Compares the key for this instance with another Folevento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folevento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folevento)) {
            return false;
        }
        Folevento that = (Folevento) other;
        if (this.getCodfolevento() != that.getCodfolevento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folevento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folevento)) return false;
        return this.equalKeys(other) && ((Folevento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolevento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folevento |");
        sb.append(" codfolevento=").append(getCodfolevento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolevento", Integer.valueOf(getCodfolevento()));
        return ret;
    }

}
