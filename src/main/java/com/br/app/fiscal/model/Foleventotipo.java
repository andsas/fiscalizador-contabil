package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLEVENTOTIPO")
public class Foleventotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfoleventotipo";

    @Id
    @Column(name="CODFOLEVENTOTIPO", unique=true, nullable=false, precision=10)
    private int codfoleventotipo;
    @Column(name="DESCFOLEVENTOTIPO", nullable=false, length=250)
    private String descfoleventotipo;
    @Column(name="TIPOFOLEVENTOTIPO", precision=5)
    private short tipofoleventotipo;
    @Column(name="BASECALCULO", precision=5)
    private short basecalculo;

    /** Default constructor. */
    public Foleventotipo() {
        super();
    }

    /**
     * Access method for codfoleventotipo.
     *
     * @return the current value of codfoleventotipo
     */
    public int getCodfoleventotipo() {
        return codfoleventotipo;
    }

    /**
     * Setter method for codfoleventotipo.
     *
     * @param aCodfoleventotipo the new value for codfoleventotipo
     */
    public void setCodfoleventotipo(int aCodfoleventotipo) {
        codfoleventotipo = aCodfoleventotipo;
    }

    /**
     * Access method for descfoleventotipo.
     *
     * @return the current value of descfoleventotipo
     */
    public String getDescfoleventotipo() {
        return descfoleventotipo;
    }

    /**
     * Setter method for descfoleventotipo.
     *
     * @param aDescfoleventotipo the new value for descfoleventotipo
     */
    public void setDescfoleventotipo(String aDescfoleventotipo) {
        descfoleventotipo = aDescfoleventotipo;
    }

    /**
     * Access method for tipofoleventotipo.
     *
     * @return the current value of tipofoleventotipo
     */
    public short getTipofoleventotipo() {
        return tipofoleventotipo;
    }

    /**
     * Setter method for tipofoleventotipo.
     *
     * @param aTipofoleventotipo the new value for tipofoleventotipo
     */
    public void setTipofoleventotipo(short aTipofoleventotipo) {
        tipofoleventotipo = aTipofoleventotipo;
    }

    /**
     * Access method for basecalculo.
     *
     * @return the current value of basecalculo
     */
    public short getBasecalculo() {
        return basecalculo;
    }

    /**
     * Setter method for basecalculo.
     *
     * @param aBasecalculo the new value for basecalculo
     */
    public void setBasecalculo(short aBasecalculo) {
        basecalculo = aBasecalculo;
    }

    /**
     * Compares the key for this instance with another Foleventotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Foleventotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Foleventotipo)) {
            return false;
        }
        Foleventotipo that = (Foleventotipo) other;
        if (this.getCodfoleventotipo() != that.getCodfoleventotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Foleventotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Foleventotipo)) return false;
        return this.equalKeys(other) && ((Foleventotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfoleventotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Foleventotipo |");
        sb.append(" codfoleventotipo=").append(getCodfoleventotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfoleventotipo", Integer.valueOf(getCodfoleventotipo()));
        return ret;
    }

}
