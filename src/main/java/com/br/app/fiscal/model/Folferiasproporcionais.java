package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLFERIASPROPORCIONAIS")
public class Folferiasproporcionais implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolferiasproporcionais";

    @Id
    @Column(name="CODFOLFERIASPROPORCIONAIS", unique=true, nullable=false, precision=10)
    private int codfolferiasproporcionais;
    @Column(name="FALTASDE", precision=10)
    private int faltasde;
    @Column(name="FALTASATE", nullable=false, precision=10)
    private int faltasate;
    @Column(name="DIASFERIAS", precision=15, scale=10)
    private BigDecimal diasferias;
    @Column(name="TEMPOSERVICODE", nullable=false, precision=10)
    private int temposervicode;
    @Column(name="TEMPOSERVICOATE", precision=10)
    private int temposervicoate;

    /** Default constructor. */
    public Folferiasproporcionais() {
        super();
    }

    /**
     * Access method for codfolferiasproporcionais.
     *
     * @return the current value of codfolferiasproporcionais
     */
    public int getCodfolferiasproporcionais() {
        return codfolferiasproporcionais;
    }

    /**
     * Setter method for codfolferiasproporcionais.
     *
     * @param aCodfolferiasproporcionais the new value for codfolferiasproporcionais
     */
    public void setCodfolferiasproporcionais(int aCodfolferiasproporcionais) {
        codfolferiasproporcionais = aCodfolferiasproporcionais;
    }

    /**
     * Access method for faltasde.
     *
     * @return the current value of faltasde
     */
    public int getFaltasde() {
        return faltasde;
    }

    /**
     * Setter method for faltasde.
     *
     * @param aFaltasde the new value for faltasde
     */
    public void setFaltasde(int aFaltasde) {
        faltasde = aFaltasde;
    }

    /**
     * Access method for faltasate.
     *
     * @return the current value of faltasate
     */
    public int getFaltasate() {
        return faltasate;
    }

    /**
     * Setter method for faltasate.
     *
     * @param aFaltasate the new value for faltasate
     */
    public void setFaltasate(int aFaltasate) {
        faltasate = aFaltasate;
    }

    /**
     * Access method for diasferias.
     *
     * @return the current value of diasferias
     */
    public BigDecimal getDiasferias() {
        return diasferias;
    }

    /**
     * Setter method for diasferias.
     *
     * @param aDiasferias the new value for diasferias
     */
    public void setDiasferias(BigDecimal aDiasferias) {
        diasferias = aDiasferias;
    }

    /**
     * Access method for temposervicode.
     *
     * @return the current value of temposervicode
     */
    public int getTemposervicode() {
        return temposervicode;
    }

    /**
     * Setter method for temposervicode.
     *
     * @param aTemposervicode the new value for temposervicode
     */
    public void setTemposervicode(int aTemposervicode) {
        temposervicode = aTemposervicode;
    }

    /**
     * Access method for temposervicoate.
     *
     * @return the current value of temposervicoate
     */
    public int getTemposervicoate() {
        return temposervicoate;
    }

    /**
     * Setter method for temposervicoate.
     *
     * @param aTemposervicoate the new value for temposervicoate
     */
    public void setTemposervicoate(int aTemposervicoate) {
        temposervicoate = aTemposervicoate;
    }

    /**
     * Compares the key for this instance with another Folferiasproporcionais.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folferiasproporcionais and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folferiasproporcionais)) {
            return false;
        }
        Folferiasproporcionais that = (Folferiasproporcionais) other;
        if (this.getCodfolferiasproporcionais() != that.getCodfolferiasproporcionais()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folferiasproporcionais.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folferiasproporcionais)) return false;
        return this.equalKeys(other) && ((Folferiasproporcionais)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolferiasproporcionais();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folferiasproporcionais |");
        sb.append(" codfolferiasproporcionais=").append(getCodfolferiasproporcionais());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolferiasproporcionais", Integer.valueOf(getCodfolferiasproporcionais()));
        return ret;
    }

}
