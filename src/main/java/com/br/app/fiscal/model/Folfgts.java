package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLFGTS")
public class Folfgts implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolfgts";

    @Id
    @Column(name="CODFOLFGTS", unique=true, nullable=false, length=20)
    private String codfolfgts;
    @Column(name="DESCFOLFGTS", nullable=false, length=250)
    private String descfolfgts;
    @Column(name="TIPOFOLFGTS", precision=5)
    private short tipofolfgts;

    /** Default constructor. */
    public Folfgts() {
        super();
    }

    /**
     * Access method for codfolfgts.
     *
     * @return the current value of codfolfgts
     */
    public String getCodfolfgts() {
        return codfolfgts;
    }

    /**
     * Setter method for codfolfgts.
     *
     * @param aCodfolfgts the new value for codfolfgts
     */
    public void setCodfolfgts(String aCodfolfgts) {
        codfolfgts = aCodfolfgts;
    }

    /**
     * Access method for descfolfgts.
     *
     * @return the current value of descfolfgts
     */
    public String getDescfolfgts() {
        return descfolfgts;
    }

    /**
     * Setter method for descfolfgts.
     *
     * @param aDescfolfgts the new value for descfolfgts
     */
    public void setDescfolfgts(String aDescfolfgts) {
        descfolfgts = aDescfolfgts;
    }

    /**
     * Access method for tipofolfgts.
     *
     * @return the current value of tipofolfgts
     */
    public short getTipofolfgts() {
        return tipofolfgts;
    }

    /**
     * Setter method for tipofolfgts.
     *
     * @param aTipofolfgts the new value for tipofolfgts
     */
    public void setTipofolfgts(short aTipofolfgts) {
        tipofolfgts = aTipofolfgts;
    }

    /**
     * Compares the key for this instance with another Folfgts.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folfgts and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folfgts)) {
            return false;
        }
        Folfgts that = (Folfgts) other;
        Object myCodfolfgts = this.getCodfolfgts();
        Object yourCodfolfgts = that.getCodfolfgts();
        if (myCodfolfgts==null ? yourCodfolfgts!=null : !myCodfolfgts.equals(yourCodfolfgts)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folfgts.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folfgts)) return false;
        return this.equalKeys(other) && ((Folfgts)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodfolfgts() == null) {
            i = 0;
        } else {
            i = getCodfolfgts().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folfgts |");
        sb.append(" codfolfgts=").append(getCodfolfgts());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolfgts", getCodfolfgts());
        return ret;
    }

}
