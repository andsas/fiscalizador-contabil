package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLFGTSALIQUOTA")
public class Folfgtsaliquota implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolfgtsaliquota";

    @Id
    @Column(name="CODFOLFGTSALIQUOTA", unique=true, nullable=false, precision=10)
    private int codfolfgtsaliquota;
    @Column(name="TIPOPESSOA", precision=5)
    private short tipopessoa;
    @Column(name="FGTSREM", precision=15, scale=4)
    private BigDecimal fgtsrem;
    @Column(name="CSFGTSREM", precision=15, scale=4)
    private BigDecimal csfgtsrem;
    @Column(name="FGTSMULTA", precision=15, scale=4)
    private BigDecimal fgtsmulta;
    @Column(name="CSFGTSMULTA", precision=15, scale=4)
    private BigDecimal csfgtsmulta;
    @Column(name="FGTSREC", precision=15, scale=4)
    private BigDecimal fgtsrec;
    @Column(name="CSFGTSREC", precision=15, scale=4)
    private BigDecimal csfgtsrec;
    @ManyToOne
    @JoinColumn(name="CODFOLARTIGOREM")
    private Folartigo folartigo;
    @ManyToOne
    @JoinColumn(name="CODFOLARTIGOMULTA")
    private Folartigo folartigo2;
    @ManyToOne
    @JoinColumn(name="CODFOLARTIGOREC")
    private Folartigo folartigo3;

    /** Default constructor. */
    public Folfgtsaliquota() {
        super();
    }

    /**
     * Access method for codfolfgtsaliquota.
     *
     * @return the current value of codfolfgtsaliquota
     */
    public int getCodfolfgtsaliquota() {
        return codfolfgtsaliquota;
    }

    /**
     * Setter method for codfolfgtsaliquota.
     *
     * @param aCodfolfgtsaliquota the new value for codfolfgtsaliquota
     */
    public void setCodfolfgtsaliquota(int aCodfolfgtsaliquota) {
        codfolfgtsaliquota = aCodfolfgtsaliquota;
    }

    /**
     * Access method for tipopessoa.
     *
     * @return the current value of tipopessoa
     */
    public short getTipopessoa() {
        return tipopessoa;
    }

    /**
     * Setter method for tipopessoa.
     *
     * @param aTipopessoa the new value for tipopessoa
     */
    public void setTipopessoa(short aTipopessoa) {
        tipopessoa = aTipopessoa;
    }

    /**
     * Access method for fgtsrem.
     *
     * @return the current value of fgtsrem
     */
    public BigDecimal getFgtsrem() {
        return fgtsrem;
    }

    /**
     * Setter method for fgtsrem.
     *
     * @param aFgtsrem the new value for fgtsrem
     */
    public void setFgtsrem(BigDecimal aFgtsrem) {
        fgtsrem = aFgtsrem;
    }

    /**
     * Access method for csfgtsrem.
     *
     * @return the current value of csfgtsrem
     */
    public BigDecimal getCsfgtsrem() {
        return csfgtsrem;
    }

    /**
     * Setter method for csfgtsrem.
     *
     * @param aCsfgtsrem the new value for csfgtsrem
     */
    public void setCsfgtsrem(BigDecimal aCsfgtsrem) {
        csfgtsrem = aCsfgtsrem;
    }

    /**
     * Access method for fgtsmulta.
     *
     * @return the current value of fgtsmulta
     */
    public BigDecimal getFgtsmulta() {
        return fgtsmulta;
    }

    /**
     * Setter method for fgtsmulta.
     *
     * @param aFgtsmulta the new value for fgtsmulta
     */
    public void setFgtsmulta(BigDecimal aFgtsmulta) {
        fgtsmulta = aFgtsmulta;
    }

    /**
     * Access method for csfgtsmulta.
     *
     * @return the current value of csfgtsmulta
     */
    public BigDecimal getCsfgtsmulta() {
        return csfgtsmulta;
    }

    /**
     * Setter method for csfgtsmulta.
     *
     * @param aCsfgtsmulta the new value for csfgtsmulta
     */
    public void setCsfgtsmulta(BigDecimal aCsfgtsmulta) {
        csfgtsmulta = aCsfgtsmulta;
    }

    /**
     * Access method for fgtsrec.
     *
     * @return the current value of fgtsrec
     */
    public BigDecimal getFgtsrec() {
        return fgtsrec;
    }

    /**
     * Setter method for fgtsrec.
     *
     * @param aFgtsrec the new value for fgtsrec
     */
    public void setFgtsrec(BigDecimal aFgtsrec) {
        fgtsrec = aFgtsrec;
    }

    /**
     * Access method for csfgtsrec.
     *
     * @return the current value of csfgtsrec
     */
    public BigDecimal getCsfgtsrec() {
        return csfgtsrec;
    }

    /**
     * Setter method for csfgtsrec.
     *
     * @param aCsfgtsrec the new value for csfgtsrec
     */
    public void setCsfgtsrec(BigDecimal aCsfgtsrec) {
        csfgtsrec = aCsfgtsrec;
    }

    /**
     * Access method for folartigo.
     *
     * @return the current value of folartigo
     */
    public Folartigo getFolartigo() {
        return folartigo;
    }

    /**
     * Setter method for folartigo.
     *
     * @param aFolartigo the new value for folartigo
     */
    public void setFolartigo(Folartigo aFolartigo) {
        folartigo = aFolartigo;
    }

    /**
     * Access method for folartigo2.
     *
     * @return the current value of folartigo2
     */
    public Folartigo getFolartigo2() {
        return folartigo2;
    }

    /**
     * Setter method for folartigo2.
     *
     * @param aFolartigo2 the new value for folartigo2
     */
    public void setFolartigo2(Folartigo aFolartigo2) {
        folartigo2 = aFolartigo2;
    }

    /**
     * Access method for folartigo3.
     *
     * @return the current value of folartigo3
     */
    public Folartigo getFolartigo3() {
        return folartigo3;
    }

    /**
     * Setter method for folartigo3.
     *
     * @param aFolartigo3 the new value for folartigo3
     */
    public void setFolartigo3(Folartigo aFolartigo3) {
        folartigo3 = aFolartigo3;
    }

    /**
     * Compares the key for this instance with another Folfgtsaliquota.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folfgtsaliquota and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folfgtsaliquota)) {
            return false;
        }
        Folfgtsaliquota that = (Folfgtsaliquota) other;
        if (this.getCodfolfgtsaliquota() != that.getCodfolfgtsaliquota()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folfgtsaliquota.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folfgtsaliquota)) return false;
        return this.equalKeys(other) && ((Folfgtsaliquota)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolfgtsaliquota();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folfgtsaliquota |");
        sb.append(" codfolfgtsaliquota=").append(getCodfolfgtsaliquota());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolfgtsaliquota", Integer.valueOf(getCodfolfgtsaliquota()));
        return ret;
    }

}
