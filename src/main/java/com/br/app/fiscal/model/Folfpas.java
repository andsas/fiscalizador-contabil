package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FOLFPAS")
public class Folfpas implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolfpas";

    @Id
    @Column(name="CODFOLFPAS", unique=true, nullable=false, precision=10)
    private int codfolfpas;
    @Column(name="DESCFOLFPAS", nullable=false, length=250)
    private String descfolfpas;
    @Column(name="ALIQUOTA", precision=15, scale=4)
    private BigDecimal aliquota;
    @ManyToOne
    @JoinColumn(name="CODIMPCNAE")
    private Impcnae impcnae;
    @OneToMany(mappedBy="folfpas")
    private Set<Folfpasaliquota> folfpasaliquota;

    /** Default constructor. */
    public Folfpas() {
        super();
    }

    /**
     * Access method for codfolfpas.
     *
     * @return the current value of codfolfpas
     */
    public int getCodfolfpas() {
        return codfolfpas;
    }

    /**
     * Setter method for codfolfpas.
     *
     * @param aCodfolfpas the new value for codfolfpas
     */
    public void setCodfolfpas(int aCodfolfpas) {
        codfolfpas = aCodfolfpas;
    }

    /**
     * Access method for descfolfpas.
     *
     * @return the current value of descfolfpas
     */
    public String getDescfolfpas() {
        return descfolfpas;
    }

    /**
     * Setter method for descfolfpas.
     *
     * @param aDescfolfpas the new value for descfolfpas
     */
    public void setDescfolfpas(String aDescfolfpas) {
        descfolfpas = aDescfolfpas;
    }

    /**
     * Access method for aliquota.
     *
     * @return the current value of aliquota
     */
    public BigDecimal getAliquota() {
        return aliquota;
    }

    /**
     * Setter method for aliquota.
     *
     * @param aAliquota the new value for aliquota
     */
    public void setAliquota(BigDecimal aAliquota) {
        aliquota = aAliquota;
    }

    /**
     * Access method for impcnae.
     *
     * @return the current value of impcnae
     */
    public Impcnae getImpcnae() {
        return impcnae;
    }

    /**
     * Setter method for impcnae.
     *
     * @param aImpcnae the new value for impcnae
     */
    public void setImpcnae(Impcnae aImpcnae) {
        impcnae = aImpcnae;
    }

    /**
     * Access method for folfpasaliquota.
     *
     * @return the current value of folfpasaliquota
     */
    public Set<Folfpasaliquota> getFolfpasaliquota() {
        return folfpasaliquota;
    }

    /**
     * Setter method for folfpasaliquota.
     *
     * @param aFolfpasaliquota the new value for folfpasaliquota
     */
    public void setFolfpasaliquota(Set<Folfpasaliquota> aFolfpasaliquota) {
        folfpasaliquota = aFolfpasaliquota;
    }

    /**
     * Compares the key for this instance with another Folfpas.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folfpas and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folfpas)) {
            return false;
        }
        Folfpas that = (Folfpas) other;
        if (this.getCodfolfpas() != that.getCodfolfpas()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folfpas.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folfpas)) return false;
        return this.equalKeys(other) && ((Folfpas)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolfpas();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folfpas |");
        sb.append(" codfolfpas=").append(getCodfolfpas());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolfpas", Integer.valueOf(getCodfolfpas()));
        return ret;
    }

}
