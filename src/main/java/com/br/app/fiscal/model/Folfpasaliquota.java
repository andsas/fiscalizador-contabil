package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLFPASALIQUOTA")
public class Folfpasaliquota implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolfpasaliquota";

    @Id
    @Column(name="CODFOLFPASALIQUOTA", unique=true, nullable=false, precision=10)
    private int codfolfpasaliquota;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @Column(name="PREVSOCIAL", precision=15, scale=4)
    private BigDecimal prevsocial;
    @Column(name="GILRAT", precision=15, scale=4)
    private BigDecimal gilrat;
    @Column(name="SALARIOEDUCACAO", precision=15, scale=4)
    private BigDecimal salarioeducacao;
    @Column(name="INCRA", precision=15, scale=4)
    private BigDecimal incra;
    @Column(name="SENAI", precision=15, scale=4)
    private BigDecimal senai;
    @Column(name="SESI", precision=15, scale=4)
    private BigDecimal sesi;
    @Column(name="SENAC", precision=15, scale=4)
    private BigDecimal senac;
    @Column(name="SESC", precision=15, scale=4)
    private BigDecimal sesc;
    @Column(name="SEBRAE", precision=15, scale=4)
    private BigDecimal sebrae;
    @Column(name="DPC", precision=15, scale=4)
    private BigDecimal dpc;
    @Column(name="FUNDOAEROVIARIO", precision=15, scale=4)
    private BigDecimal fundoaeroviario;
    @Column(name="SENAR", precision=15, scale=4)
    private BigDecimal senar;
    @Column(name="SEST", precision=15, scale=4)
    private BigDecimal sest;
    @Column(name="SENAT", precision=15, scale=4)
    private BigDecimal senat;
    @Column(name="COOPSES", precision=15, scale=4)
    private BigDecimal coopses;
    @Column(name="DATAINATIVACAO")
    private Timestamp datainativacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLFPAS", nullable=false)
    private Folfpas folfpas;

    /** Default constructor. */
    public Folfpasaliquota() {
        super();
    }

    /**
     * Access method for codfolfpasaliquota.
     *
     * @return the current value of codfolfpasaliquota
     */
    public int getCodfolfpasaliquota() {
        return codfolfpasaliquota;
    }

    /**
     * Setter method for codfolfpasaliquota.
     *
     * @param aCodfolfpasaliquota the new value for codfolfpasaliquota
     */
    public void setCodfolfpasaliquota(int aCodfolfpasaliquota) {
        codfolfpasaliquota = aCodfolfpasaliquota;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for prevsocial.
     *
     * @return the current value of prevsocial
     */
    public BigDecimal getPrevsocial() {
        return prevsocial;
    }

    /**
     * Setter method for prevsocial.
     *
     * @param aPrevsocial the new value for prevsocial
     */
    public void setPrevsocial(BigDecimal aPrevsocial) {
        prevsocial = aPrevsocial;
    }

    /**
     * Access method for gilrat.
     *
     * @return the current value of gilrat
     */
    public BigDecimal getGilrat() {
        return gilrat;
    }

    /**
     * Setter method for gilrat.
     *
     * @param aGilrat the new value for gilrat
     */
    public void setGilrat(BigDecimal aGilrat) {
        gilrat = aGilrat;
    }

    /**
     * Access method for salarioeducacao.
     *
     * @return the current value of salarioeducacao
     */
    public BigDecimal getSalarioeducacao() {
        return salarioeducacao;
    }

    /**
     * Setter method for salarioeducacao.
     *
     * @param aSalarioeducacao the new value for salarioeducacao
     */
    public void setSalarioeducacao(BigDecimal aSalarioeducacao) {
        salarioeducacao = aSalarioeducacao;
    }

    /**
     * Access method for incra.
     *
     * @return the current value of incra
     */
    public BigDecimal getIncra() {
        return incra;
    }

    /**
     * Setter method for incra.
     *
     * @param aIncra the new value for incra
     */
    public void setIncra(BigDecimal aIncra) {
        incra = aIncra;
    }

    /**
     * Access method for senai.
     *
     * @return the current value of senai
     */
    public BigDecimal getSenai() {
        return senai;
    }

    /**
     * Setter method for senai.
     *
     * @param aSenai the new value for senai
     */
    public void setSenai(BigDecimal aSenai) {
        senai = aSenai;
    }

    /**
     * Access method for sesi.
     *
     * @return the current value of sesi
     */
    public BigDecimal getSesi() {
        return sesi;
    }

    /**
     * Setter method for sesi.
     *
     * @param aSesi the new value for sesi
     */
    public void setSesi(BigDecimal aSesi) {
        sesi = aSesi;
    }

    /**
     * Access method for senac.
     *
     * @return the current value of senac
     */
    public BigDecimal getSenac() {
        return senac;
    }

    /**
     * Setter method for senac.
     *
     * @param aSenac the new value for senac
     */
    public void setSenac(BigDecimal aSenac) {
        senac = aSenac;
    }

    /**
     * Access method for sesc.
     *
     * @return the current value of sesc
     */
    public BigDecimal getSesc() {
        return sesc;
    }

    /**
     * Setter method for sesc.
     *
     * @param aSesc the new value for sesc
     */
    public void setSesc(BigDecimal aSesc) {
        sesc = aSesc;
    }

    /**
     * Access method for sebrae.
     *
     * @return the current value of sebrae
     */
    public BigDecimal getSebrae() {
        return sebrae;
    }

    /**
     * Setter method for sebrae.
     *
     * @param aSebrae the new value for sebrae
     */
    public void setSebrae(BigDecimal aSebrae) {
        sebrae = aSebrae;
    }

    /**
     * Access method for dpc.
     *
     * @return the current value of dpc
     */
    public BigDecimal getDpc() {
        return dpc;
    }

    /**
     * Setter method for dpc.
     *
     * @param aDpc the new value for dpc
     */
    public void setDpc(BigDecimal aDpc) {
        dpc = aDpc;
    }

    /**
     * Access method for fundoaeroviario.
     *
     * @return the current value of fundoaeroviario
     */
    public BigDecimal getFundoaeroviario() {
        return fundoaeroviario;
    }

    /**
     * Setter method for fundoaeroviario.
     *
     * @param aFundoaeroviario the new value for fundoaeroviario
     */
    public void setFundoaeroviario(BigDecimal aFundoaeroviario) {
        fundoaeroviario = aFundoaeroviario;
    }

    /**
     * Access method for senar.
     *
     * @return the current value of senar
     */
    public BigDecimal getSenar() {
        return senar;
    }

    /**
     * Setter method for senar.
     *
     * @param aSenar the new value for senar
     */
    public void setSenar(BigDecimal aSenar) {
        senar = aSenar;
    }

    /**
     * Access method for sest.
     *
     * @return the current value of sest
     */
    public BigDecimal getSest() {
        return sest;
    }

    /**
     * Setter method for sest.
     *
     * @param aSest the new value for sest
     */
    public void setSest(BigDecimal aSest) {
        sest = aSest;
    }

    /**
     * Access method for senat.
     *
     * @return the current value of senat
     */
    public BigDecimal getSenat() {
        return senat;
    }

    /**
     * Setter method for senat.
     *
     * @param aSenat the new value for senat
     */
    public void setSenat(BigDecimal aSenat) {
        senat = aSenat;
    }

    /**
     * Access method for coopses.
     *
     * @return the current value of coopses
     */
    public BigDecimal getCoopses() {
        return coopses;
    }

    /**
     * Setter method for coopses.
     *
     * @param aCoopses the new value for coopses
     */
    public void setCoopses(BigDecimal aCoopses) {
        coopses = aCoopses;
    }

    /**
     * Access method for datainativacao.
     *
     * @return the current value of datainativacao
     */
    public Timestamp getDatainativacao() {
        return datainativacao;
    }

    /**
     * Setter method for datainativacao.
     *
     * @param aDatainativacao the new value for datainativacao
     */
    public void setDatainativacao(Timestamp aDatainativacao) {
        datainativacao = aDatainativacao;
    }

    /**
     * Access method for folfpas.
     *
     * @return the current value of folfpas
     */
    public Folfpas getFolfpas() {
        return folfpas;
    }

    /**
     * Setter method for folfpas.
     *
     * @param aFolfpas the new value for folfpas
     */
    public void setFolfpas(Folfpas aFolfpas) {
        folfpas = aFolfpas;
    }

    /**
     * Compares the key for this instance with another Folfpasaliquota.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folfpasaliquota and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folfpasaliquota)) {
            return false;
        }
        Folfpasaliquota that = (Folfpasaliquota) other;
        if (this.getCodfolfpasaliquota() != that.getCodfolfpasaliquota()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folfpasaliquota.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folfpasaliquota)) return false;
        return this.equalKeys(other) && ((Folfpasaliquota)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolfpasaliquota();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folfpasaliquota |");
        sb.append(" codfolfpasaliquota=").append(getCodfolfpasaliquota());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolfpasaliquota", Integer.valueOf(getCodfolfpasaliquota()));
        return ret;
    }

}
