package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FOLFUNCAO")
public class Folfuncao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolfuncao";

    @Id
    @Column(name="CODFOLFUNCAO", unique=true, nullable=false, precision=10)
    private int codfolfuncao;
    @Column(name="DESCFOLFUNCAO", nullable=false, length=250)
    private String descfolfuncao;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @OneToMany(mappedBy="folfuncao")
    private Set<Folcolaborador> folcolaborador;
    @OneToMany(mappedBy="folfuncao")
    private Set<Folcolaboradoremprant> folcolaboradoremprant;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLDEPARTAMENTO", nullable=false)
    private Foldepartamento foldepartamento;
    @ManyToOne
    @JoinColumn(name="CODFOLCBO")
    private Folcbo folcbo;
    @OneToMany(mappedBy="folfuncao")
    private Set<Folhorariotrabalho> folhorariotrabalho;
    @OneToMany(mappedBy="folfuncao")
    private Set<Folsalario> folsalario;

    /** Default constructor. */
    public Folfuncao() {
        super();
    }

    /**
     * Access method for codfolfuncao.
     *
     * @return the current value of codfolfuncao
     */
    public int getCodfolfuncao() {
        return codfolfuncao;
    }

    /**
     * Setter method for codfolfuncao.
     *
     * @param aCodfolfuncao the new value for codfolfuncao
     */
    public void setCodfolfuncao(int aCodfolfuncao) {
        codfolfuncao = aCodfolfuncao;
    }

    /**
     * Access method for descfolfuncao.
     *
     * @return the current value of descfolfuncao
     */
    public String getDescfolfuncao() {
        return descfolfuncao;
    }

    /**
     * Setter method for descfolfuncao.
     *
     * @param aDescfolfuncao the new value for descfolfuncao
     */
    public void setDescfolfuncao(String aDescfolfuncao) {
        descfolfuncao = aDescfolfuncao;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Set<Folcolaborador> getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Set<Folcolaborador> aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Access method for folcolaboradoremprant.
     *
     * @return the current value of folcolaboradoremprant
     */
    public Set<Folcolaboradoremprant> getFolcolaboradoremprant() {
        return folcolaboradoremprant;
    }

    /**
     * Setter method for folcolaboradoremprant.
     *
     * @param aFolcolaboradoremprant the new value for folcolaboradoremprant
     */
    public void setFolcolaboradoremprant(Set<Folcolaboradoremprant> aFolcolaboradoremprant) {
        folcolaboradoremprant = aFolcolaboradoremprant;
    }

    /**
     * Access method for foldepartamento.
     *
     * @return the current value of foldepartamento
     */
    public Foldepartamento getFoldepartamento() {
        return foldepartamento;
    }

    /**
     * Setter method for foldepartamento.
     *
     * @param aFoldepartamento the new value for foldepartamento
     */
    public void setFoldepartamento(Foldepartamento aFoldepartamento) {
        foldepartamento = aFoldepartamento;
    }

    /**
     * Access method for folcbo.
     *
     * @return the current value of folcbo
     */
    public Folcbo getFolcbo() {
        return folcbo;
    }

    /**
     * Setter method for folcbo.
     *
     * @param aFolcbo the new value for folcbo
     */
    public void setFolcbo(Folcbo aFolcbo) {
        folcbo = aFolcbo;
    }

    /**
     * Access method for folhorariotrabalho.
     *
     * @return the current value of folhorariotrabalho
     */
    public Set<Folhorariotrabalho> getFolhorariotrabalho() {
        return folhorariotrabalho;
    }

    /**
     * Setter method for folhorariotrabalho.
     *
     * @param aFolhorariotrabalho the new value for folhorariotrabalho
     */
    public void setFolhorariotrabalho(Set<Folhorariotrabalho> aFolhorariotrabalho) {
        folhorariotrabalho = aFolhorariotrabalho;
    }

    /**
     * Access method for folsalario.
     *
     * @return the current value of folsalario
     */
    public Set<Folsalario> getFolsalario() {
        return folsalario;
    }

    /**
     * Setter method for folsalario.
     *
     * @param aFolsalario the new value for folsalario
     */
    public void setFolsalario(Set<Folsalario> aFolsalario) {
        folsalario = aFolsalario;
    }

    /**
     * Compares the key for this instance with another Folfuncao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folfuncao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folfuncao)) {
            return false;
        }
        Folfuncao that = (Folfuncao) other;
        if (this.getCodfolfuncao() != that.getCodfolfuncao()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folfuncao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folfuncao)) return false;
        return this.equalKeys(other) && ((Folfuncao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolfuncao();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folfuncao |");
        sb.append(" codfolfuncao=").append(getCodfolfuncao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolfuncao", Integer.valueOf(getCodfolfuncao()));
        return ret;
    }

}
