package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FOLGRAUINSTRUCAO")
public class Folgrauinstrucao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolgrauinstrucao";

    @Id
    @Column(name="CODFOLGRAUINSTRUCAO", unique=true, nullable=false, precision=10)
    private int codfolgrauinstrucao;
    @Column(name="DESCFOLGRAUINSTRUCAO", nullable=false, length=250)
    private String descfolgrauinstrucao;
    @OneToMany(mappedBy="folgrauinstrucao")
    private Set<Folcolaborador> folcolaborador;
    @OneToMany(mappedBy="folgrauinstrucao")
    private Set<Folsalario> folsalario;

    /** Default constructor. */
    public Folgrauinstrucao() {
        super();
    }

    /**
     * Access method for codfolgrauinstrucao.
     *
     * @return the current value of codfolgrauinstrucao
     */
    public int getCodfolgrauinstrucao() {
        return codfolgrauinstrucao;
    }

    /**
     * Setter method for codfolgrauinstrucao.
     *
     * @param aCodfolgrauinstrucao the new value for codfolgrauinstrucao
     */
    public void setCodfolgrauinstrucao(int aCodfolgrauinstrucao) {
        codfolgrauinstrucao = aCodfolgrauinstrucao;
    }

    /**
     * Access method for descfolgrauinstrucao.
     *
     * @return the current value of descfolgrauinstrucao
     */
    public String getDescfolgrauinstrucao() {
        return descfolgrauinstrucao;
    }

    /**
     * Setter method for descfolgrauinstrucao.
     *
     * @param aDescfolgrauinstrucao the new value for descfolgrauinstrucao
     */
    public void setDescfolgrauinstrucao(String aDescfolgrauinstrucao) {
        descfolgrauinstrucao = aDescfolgrauinstrucao;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Set<Folcolaborador> getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Set<Folcolaborador> aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Access method for folsalario.
     *
     * @return the current value of folsalario
     */
    public Set<Folsalario> getFolsalario() {
        return folsalario;
    }

    /**
     * Setter method for folsalario.
     *
     * @param aFolsalario the new value for folsalario
     */
    public void setFolsalario(Set<Folsalario> aFolsalario) {
        folsalario = aFolsalario;
    }

    /**
     * Compares the key for this instance with another Folgrauinstrucao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folgrauinstrucao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folgrauinstrucao)) {
            return false;
        }
        Folgrauinstrucao that = (Folgrauinstrucao) other;
        if (this.getCodfolgrauinstrucao() != that.getCodfolgrauinstrucao()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folgrauinstrucao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folgrauinstrucao)) return false;
        return this.equalKeys(other) && ((Folgrauinstrucao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolgrauinstrucao();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folgrauinstrucao |");
        sb.append(" codfolgrauinstrucao=").append(getCodfolgrauinstrucao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolgrauinstrucao", Integer.valueOf(getCodfolgrauinstrucao()));
        return ret;
    }

}
