package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FOLHORARIOTRABALHO")
public class Folhorariotrabalho implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolhorariotrabalho";

    @Id
    @Column(name="CODFOLHORARIOTRABALHO", unique=true, nullable=false, precision=10)
    private int codfolhorariotrabalho;
    @Column(name="DESCFOLHORARIOTRABALHO", nullable=false, length=250)
    private String descfolhorariotrabalho;
    @Column(name="HORASTOTAL", precision=10)
    private int horastotal;
    @Column(name="HORASINICIO")
    private Time horasinicio;
    @Column(name="HORASFINAL")
    private Time horasfinal;
    @Column(name="HORASSEMDEF", precision=5)
    private short horassemdef;
    @Column(name="HORASALMOCO", precision=10)
    private int horasalmoco;
    @Column(name="HORASINTERVALO", precision=10)
    private int horasintervalo;
    @Column(name="HORASQTDEINT", precision=10)
    private int horasqtdeint;
    @Column(name="TRABDOMINGO", precision=5)
    private short trabdomingo;
    @Column(name="TRABSEGUNDA", precision=5)
    private short trabsegunda;
    @Column(name="TRABTERCA", precision=5)
    private short trabterca;
    @Column(name="TRABQUARTA", precision=5)
    private short trabquarta;
    @Column(name="TRABQUINTA", precision=5)
    private short trabquinta;
    @Column(name="TRABSEXTA", precision=5)
    private short trabsexta;
    @Column(name="TRABSABADO", precision=5)
    private short trabsabado;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLFUNCAO", nullable=false)
    private Folfuncao folfuncao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @OneToMany(mappedBy="folhorariotrabalho")
    private Set<Folhorariotrabalhodia> folhorariotrabalhodia;

    /** Default constructor. */
    public Folhorariotrabalho() {
        super();
    }

    /**
     * Access method for codfolhorariotrabalho.
     *
     * @return the current value of codfolhorariotrabalho
     */
    public int getCodfolhorariotrabalho() {
        return codfolhorariotrabalho;
    }

    /**
     * Setter method for codfolhorariotrabalho.
     *
     * @param aCodfolhorariotrabalho the new value for codfolhorariotrabalho
     */
    public void setCodfolhorariotrabalho(int aCodfolhorariotrabalho) {
        codfolhorariotrabalho = aCodfolhorariotrabalho;
    }

    /**
     * Access method for descfolhorariotrabalho.
     *
     * @return the current value of descfolhorariotrabalho
     */
    public String getDescfolhorariotrabalho() {
        return descfolhorariotrabalho;
    }

    /**
     * Setter method for descfolhorariotrabalho.
     *
     * @param aDescfolhorariotrabalho the new value for descfolhorariotrabalho
     */
    public void setDescfolhorariotrabalho(String aDescfolhorariotrabalho) {
        descfolhorariotrabalho = aDescfolhorariotrabalho;
    }

    /**
     * Access method for horastotal.
     *
     * @return the current value of horastotal
     */
    public int getHorastotal() {
        return horastotal;
    }

    /**
     * Setter method for horastotal.
     *
     * @param aHorastotal the new value for horastotal
     */
    public void setHorastotal(int aHorastotal) {
        horastotal = aHorastotal;
    }

    /**
     * Access method for horasinicio.
     *
     * @return the current value of horasinicio
     */
    public Time getHorasinicio() {
        return horasinicio;
    }

    /**
     * Setter method for horasinicio.
     *
     * @param aHorasinicio the new value for horasinicio
     */
    public void setHorasinicio(Time aHorasinicio) {
        horasinicio = aHorasinicio;
    }

    /**
     * Access method for horasfinal.
     *
     * @return the current value of horasfinal
     */
    public Time getHorasfinal() {
        return horasfinal;
    }

    /**
     * Setter method for horasfinal.
     *
     * @param aHorasfinal the new value for horasfinal
     */
    public void setHorasfinal(Time aHorasfinal) {
        horasfinal = aHorasfinal;
    }

    /**
     * Access method for horassemdef.
     *
     * @return the current value of horassemdef
     */
    public short getHorassemdef() {
        return horassemdef;
    }

    /**
     * Setter method for horassemdef.
     *
     * @param aHorassemdef the new value for horassemdef
     */
    public void setHorassemdef(short aHorassemdef) {
        horassemdef = aHorassemdef;
    }

    /**
     * Access method for horasalmoco.
     *
     * @return the current value of horasalmoco
     */
    public int getHorasalmoco() {
        return horasalmoco;
    }

    /**
     * Setter method for horasalmoco.
     *
     * @param aHorasalmoco the new value for horasalmoco
     */
    public void setHorasalmoco(int aHorasalmoco) {
        horasalmoco = aHorasalmoco;
    }

    /**
     * Access method for horasintervalo.
     *
     * @return the current value of horasintervalo
     */
    public int getHorasintervalo() {
        return horasintervalo;
    }

    /**
     * Setter method for horasintervalo.
     *
     * @param aHorasintervalo the new value for horasintervalo
     */
    public void setHorasintervalo(int aHorasintervalo) {
        horasintervalo = aHorasintervalo;
    }

    /**
     * Access method for horasqtdeint.
     *
     * @return the current value of horasqtdeint
     */
    public int getHorasqtdeint() {
        return horasqtdeint;
    }

    /**
     * Setter method for horasqtdeint.
     *
     * @param aHorasqtdeint the new value for horasqtdeint
     */
    public void setHorasqtdeint(int aHorasqtdeint) {
        horasqtdeint = aHorasqtdeint;
    }

    /**
     * Access method for trabdomingo.
     *
     * @return the current value of trabdomingo
     */
    public short getTrabdomingo() {
        return trabdomingo;
    }

    /**
     * Setter method for trabdomingo.
     *
     * @param aTrabdomingo the new value for trabdomingo
     */
    public void setTrabdomingo(short aTrabdomingo) {
        trabdomingo = aTrabdomingo;
    }

    /**
     * Access method for trabsegunda.
     *
     * @return the current value of trabsegunda
     */
    public short getTrabsegunda() {
        return trabsegunda;
    }

    /**
     * Setter method for trabsegunda.
     *
     * @param aTrabsegunda the new value for trabsegunda
     */
    public void setTrabsegunda(short aTrabsegunda) {
        trabsegunda = aTrabsegunda;
    }

    /**
     * Access method for trabterca.
     *
     * @return the current value of trabterca
     */
    public short getTrabterca() {
        return trabterca;
    }

    /**
     * Setter method for trabterca.
     *
     * @param aTrabterca the new value for trabterca
     */
    public void setTrabterca(short aTrabterca) {
        trabterca = aTrabterca;
    }

    /**
     * Access method for trabquarta.
     *
     * @return the current value of trabquarta
     */
    public short getTrabquarta() {
        return trabquarta;
    }

    /**
     * Setter method for trabquarta.
     *
     * @param aTrabquarta the new value for trabquarta
     */
    public void setTrabquarta(short aTrabquarta) {
        trabquarta = aTrabquarta;
    }

    /**
     * Access method for trabquinta.
     *
     * @return the current value of trabquinta
     */
    public short getTrabquinta() {
        return trabquinta;
    }

    /**
     * Setter method for trabquinta.
     *
     * @param aTrabquinta the new value for trabquinta
     */
    public void setTrabquinta(short aTrabquinta) {
        trabquinta = aTrabquinta;
    }

    /**
     * Access method for trabsexta.
     *
     * @return the current value of trabsexta
     */
    public short getTrabsexta() {
        return trabsexta;
    }

    /**
     * Setter method for trabsexta.
     *
     * @param aTrabsexta the new value for trabsexta
     */
    public void setTrabsexta(short aTrabsexta) {
        trabsexta = aTrabsexta;
    }

    /**
     * Access method for trabsabado.
     *
     * @return the current value of trabsabado
     */
    public short getTrabsabado() {
        return trabsabado;
    }

    /**
     * Setter method for trabsabado.
     *
     * @param aTrabsabado the new value for trabsabado
     */
    public void setTrabsabado(short aTrabsabado) {
        trabsabado = aTrabsabado;
    }

    /**
     * Access method for folfuncao.
     *
     * @return the current value of folfuncao
     */
    public Folfuncao getFolfuncao() {
        return folfuncao;
    }

    /**
     * Setter method for folfuncao.
     *
     * @param aFolfuncao the new value for folfuncao
     */
    public void setFolfuncao(Folfuncao aFolfuncao) {
        folfuncao = aFolfuncao;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for folhorariotrabalhodia.
     *
     * @return the current value of folhorariotrabalhodia
     */
    public Set<Folhorariotrabalhodia> getFolhorariotrabalhodia() {
        return folhorariotrabalhodia;
    }

    /**
     * Setter method for folhorariotrabalhodia.
     *
     * @param aFolhorariotrabalhodia the new value for folhorariotrabalhodia
     */
    public void setFolhorariotrabalhodia(Set<Folhorariotrabalhodia> aFolhorariotrabalhodia) {
        folhorariotrabalhodia = aFolhorariotrabalhodia;
    }

    /**
     * Compares the key for this instance with another Folhorariotrabalho.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folhorariotrabalho and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folhorariotrabalho)) {
            return false;
        }
        Folhorariotrabalho that = (Folhorariotrabalho) other;
        if (this.getCodfolhorariotrabalho() != that.getCodfolhorariotrabalho()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folhorariotrabalho.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folhorariotrabalho)) return false;
        return this.equalKeys(other) && ((Folhorariotrabalho)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolhorariotrabalho();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folhorariotrabalho |");
        sb.append(" codfolhorariotrabalho=").append(getCodfolhorariotrabalho());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolhorariotrabalho", Integer.valueOf(getCodfolhorariotrabalho()));
        return ret;
    }

}
