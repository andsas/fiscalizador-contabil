package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLHORARIOTRABALHODIA")
public class Folhorariotrabalhodia implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolhorariotrabalhodia";

    @Id
    @Column(name="CODFOLHORARIOTRABALHODIA", unique=true, nullable=false, precision=10)
    private int codfolhorariotrabalhodia;
    @Column(name="DIA", nullable=false, precision=10)
    private int dia;
    @Column(name="HORASINICIO")
    private Time horasinicio;
    @Column(name="HORASFINAL")
    private Time horasfinal;
    @Column(name="HORASALMOCO", precision=10)
    private int horasalmoco;
    @Column(name="HORASINTERVALO", precision=10)
    private int horasintervalo;
    @Column(name="HORASQTDEINT", precision=10)
    private int horasqtdeint;
    @Column(name="HORASSEMDEF", precision=5)
    private short horassemdef;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLHORARIOTRABALHO", nullable=false)
    private Folhorariotrabalho folhorariotrabalho;

    /** Default constructor. */
    public Folhorariotrabalhodia() {
        super();
    }

    /**
     * Access method for codfolhorariotrabalhodia.
     *
     * @return the current value of codfolhorariotrabalhodia
     */
    public int getCodfolhorariotrabalhodia() {
        return codfolhorariotrabalhodia;
    }

    /**
     * Setter method for codfolhorariotrabalhodia.
     *
     * @param aCodfolhorariotrabalhodia the new value for codfolhorariotrabalhodia
     */
    public void setCodfolhorariotrabalhodia(int aCodfolhorariotrabalhodia) {
        codfolhorariotrabalhodia = aCodfolhorariotrabalhodia;
    }

    /**
     * Access method for dia.
     *
     * @return the current value of dia
     */
    public int getDia() {
        return dia;
    }

    /**
     * Setter method for dia.
     *
     * @param aDia the new value for dia
     */
    public void setDia(int aDia) {
        dia = aDia;
    }

    /**
     * Access method for horasinicio.
     *
     * @return the current value of horasinicio
     */
    public Time getHorasinicio() {
        return horasinicio;
    }

    /**
     * Setter method for horasinicio.
     *
     * @param aHorasinicio the new value for horasinicio
     */
    public void setHorasinicio(Time aHorasinicio) {
        horasinicio = aHorasinicio;
    }

    /**
     * Access method for horasfinal.
     *
     * @return the current value of horasfinal
     */
    public Time getHorasfinal() {
        return horasfinal;
    }

    /**
     * Setter method for horasfinal.
     *
     * @param aHorasfinal the new value for horasfinal
     */
    public void setHorasfinal(Time aHorasfinal) {
        horasfinal = aHorasfinal;
    }

    /**
     * Access method for horasalmoco.
     *
     * @return the current value of horasalmoco
     */
    public int getHorasalmoco() {
        return horasalmoco;
    }

    /**
     * Setter method for horasalmoco.
     *
     * @param aHorasalmoco the new value for horasalmoco
     */
    public void setHorasalmoco(int aHorasalmoco) {
        horasalmoco = aHorasalmoco;
    }

    /**
     * Access method for horasintervalo.
     *
     * @return the current value of horasintervalo
     */
    public int getHorasintervalo() {
        return horasintervalo;
    }

    /**
     * Setter method for horasintervalo.
     *
     * @param aHorasintervalo the new value for horasintervalo
     */
    public void setHorasintervalo(int aHorasintervalo) {
        horasintervalo = aHorasintervalo;
    }

    /**
     * Access method for horasqtdeint.
     *
     * @return the current value of horasqtdeint
     */
    public int getHorasqtdeint() {
        return horasqtdeint;
    }

    /**
     * Setter method for horasqtdeint.
     *
     * @param aHorasqtdeint the new value for horasqtdeint
     */
    public void setHorasqtdeint(int aHorasqtdeint) {
        horasqtdeint = aHorasqtdeint;
    }

    /**
     * Access method for horassemdef.
     *
     * @return the current value of horassemdef
     */
    public short getHorassemdef() {
        return horassemdef;
    }

    /**
     * Setter method for horassemdef.
     *
     * @param aHorassemdef the new value for horassemdef
     */
    public void setHorassemdef(short aHorassemdef) {
        horassemdef = aHorassemdef;
    }

    /**
     * Access method for folhorariotrabalho.
     *
     * @return the current value of folhorariotrabalho
     */
    public Folhorariotrabalho getFolhorariotrabalho() {
        return folhorariotrabalho;
    }

    /**
     * Setter method for folhorariotrabalho.
     *
     * @param aFolhorariotrabalho the new value for folhorariotrabalho
     */
    public void setFolhorariotrabalho(Folhorariotrabalho aFolhorariotrabalho) {
        folhorariotrabalho = aFolhorariotrabalho;
    }

    /**
     * Compares the key for this instance with another Folhorariotrabalhodia.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folhorariotrabalhodia and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folhorariotrabalhodia)) {
            return false;
        }
        Folhorariotrabalhodia that = (Folhorariotrabalhodia) other;
        if (this.getCodfolhorariotrabalhodia() != that.getCodfolhorariotrabalhodia()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folhorariotrabalhodia.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folhorariotrabalhodia)) return false;
        return this.equalKeys(other) && ((Folhorariotrabalhodia)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolhorariotrabalhodia();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folhorariotrabalhodia |");
        sb.append(" codfolhorariotrabalhodia=").append(getCodfolhorariotrabalhodia());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolhorariotrabalhodia", Integer.valueOf(getCodfolhorariotrabalhodia()));
        return ret;
    }

}
