package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLLANCAMENTO")
public class Follancamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfollancamento";

    @Id
    @Column(name="CODFOLLANCAMENTO", unique=true, nullable=false, precision=10)
    private int codfollancamento;
    @Column(name="DESCFOLLANCAMENTO", nullable=false, length=250)
    private String descfollancamento;
    @Column(name="DATALANCAMENTO", nullable=false)
    private Timestamp datalancamento;
    @Column(name="VALORLANCAMENTO", precision=15, scale=4)
    private BigDecimal valorlancamento;
    @Column(name="VALORDESCONTO", precision=15, scale=4)
    private BigDecimal valordesconto;
    @Column(name="OBSERVACOES", precision=10)
    private int observacoes;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLEVENTO", nullable=false)
    private Folevento folevento;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLCOLABORADOR", nullable=false)
    private Folcolaborador folcolaborador;
    @ManyToOne
    @JoinColumn(name="CODFOLARTIGO")
    private Folartigo folartigo;

    /** Default constructor. */
    public Follancamento() {
        super();
    }

    /**
     * Access method for codfollancamento.
     *
     * @return the current value of codfollancamento
     */
    public int getCodfollancamento() {
        return codfollancamento;
    }

    /**
     * Setter method for codfollancamento.
     *
     * @param aCodfollancamento the new value for codfollancamento
     */
    public void setCodfollancamento(int aCodfollancamento) {
        codfollancamento = aCodfollancamento;
    }

    /**
     * Access method for descfollancamento.
     *
     * @return the current value of descfollancamento
     */
    public String getDescfollancamento() {
        return descfollancamento;
    }

    /**
     * Setter method for descfollancamento.
     *
     * @param aDescfollancamento the new value for descfollancamento
     */
    public void setDescfollancamento(String aDescfollancamento) {
        descfollancamento = aDescfollancamento;
    }

    /**
     * Access method for datalancamento.
     *
     * @return the current value of datalancamento
     */
    public Timestamp getDatalancamento() {
        return datalancamento;
    }

    /**
     * Setter method for datalancamento.
     *
     * @param aDatalancamento the new value for datalancamento
     */
    public void setDatalancamento(Timestamp aDatalancamento) {
        datalancamento = aDatalancamento;
    }

    /**
     * Access method for valorlancamento.
     *
     * @return the current value of valorlancamento
     */
    public BigDecimal getValorlancamento() {
        return valorlancamento;
    }

    /**
     * Setter method for valorlancamento.
     *
     * @param aValorlancamento the new value for valorlancamento
     */
    public void setValorlancamento(BigDecimal aValorlancamento) {
        valorlancamento = aValorlancamento;
    }

    /**
     * Access method for valordesconto.
     *
     * @return the current value of valordesconto
     */
    public BigDecimal getValordesconto() {
        return valordesconto;
    }

    /**
     * Setter method for valordesconto.
     *
     * @param aValordesconto the new value for valordesconto
     */
    public void setValordesconto(BigDecimal aValordesconto) {
        valordesconto = aValordesconto;
    }

    /**
     * Access method for observacoes.
     *
     * @return the current value of observacoes
     */
    public int getObservacoes() {
        return observacoes;
    }

    /**
     * Setter method for observacoes.
     *
     * @param aObservacoes the new value for observacoes
     */
    public void setObservacoes(int aObservacoes) {
        observacoes = aObservacoes;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for folevento.
     *
     * @return the current value of folevento
     */
    public Folevento getFolevento() {
        return folevento;
    }

    /**
     * Setter method for folevento.
     *
     * @param aFolevento the new value for folevento
     */
    public void setFolevento(Folevento aFolevento) {
        folevento = aFolevento;
    }

    /**
     * Access method for folcolaborador.
     *
     * @return the current value of folcolaborador
     */
    public Folcolaborador getFolcolaborador() {
        return folcolaborador;
    }

    /**
     * Setter method for folcolaborador.
     *
     * @param aFolcolaborador the new value for folcolaborador
     */
    public void setFolcolaborador(Folcolaborador aFolcolaborador) {
        folcolaborador = aFolcolaborador;
    }

    /**
     * Access method for folartigo.
     *
     * @return the current value of folartigo
     */
    public Folartigo getFolartigo() {
        return folartigo;
    }

    /**
     * Setter method for folartigo.
     *
     * @param aFolartigo the new value for folartigo
     */
    public void setFolartigo(Folartigo aFolartigo) {
        folartigo = aFolartigo;
    }

    /**
     * Compares the key for this instance with another Follancamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Follancamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Follancamento)) {
            return false;
        }
        Follancamento that = (Follancamento) other;
        if (this.getCodfollancamento() != that.getCodfollancamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Follancamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Follancamento)) return false;
        return this.equalKeys(other) && ((Follancamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfollancamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Follancamento |");
        sb.append(" codfollancamento=").append(getCodfollancamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfollancamento", Integer.valueOf(getCodfollancamento()));
        return ret;
    }

}
