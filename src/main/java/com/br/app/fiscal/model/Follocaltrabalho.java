package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="FOLLOCALTRABALHO")
public class Follocaltrabalho implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfollocaltrabalho";

    @Id
    @Column(name="CODFOLLOCALTRABALHO", unique=true, nullable=false, precision=10)
    private int codfollocaltrabalho;
    @Column(name="DESCFOLLOCALTRABALHO", nullable=false, length=250)
    private String descfollocaltrabalho;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @OneToMany(mappedBy="follocaltrabalho")
    private Set<Folcolaboradoremprant> folcolaboradoremprant;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLDEPARTAMENTO", nullable=false)
    private Foldepartamento foldepartamento;

    /** Default constructor. */
    public Follocaltrabalho() {
        super();
    }

    /**
     * Access method for codfollocaltrabalho.
     *
     * @return the current value of codfollocaltrabalho
     */
    public int getCodfollocaltrabalho() {
        return codfollocaltrabalho;
    }

    /**
     * Setter method for codfollocaltrabalho.
     *
     * @param aCodfollocaltrabalho the new value for codfollocaltrabalho
     */
    public void setCodfollocaltrabalho(int aCodfollocaltrabalho) {
        codfollocaltrabalho = aCodfollocaltrabalho;
    }

    /**
     * Access method for descfollocaltrabalho.
     *
     * @return the current value of descfollocaltrabalho
     */
    public String getDescfollocaltrabalho() {
        return descfollocaltrabalho;
    }

    /**
     * Setter method for descfollocaltrabalho.
     *
     * @param aDescfollocaltrabalho the new value for descfollocaltrabalho
     */
    public void setDescfollocaltrabalho(String aDescfollocaltrabalho) {
        descfollocaltrabalho = aDescfollocaltrabalho;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for folcolaboradoremprant.
     *
     * @return the current value of folcolaboradoremprant
     */
    public Set<Folcolaboradoremprant> getFolcolaboradoremprant() {
        return folcolaboradoremprant;
    }

    /**
     * Setter method for folcolaboradoremprant.
     *
     * @param aFolcolaboradoremprant the new value for folcolaboradoremprant
     */
    public void setFolcolaboradoremprant(Set<Folcolaboradoremprant> aFolcolaboradoremprant) {
        folcolaboradoremprant = aFolcolaboradoremprant;
    }

    /**
     * Access method for foldepartamento.
     *
     * @return the current value of foldepartamento
     */
    public Foldepartamento getFoldepartamento() {
        return foldepartamento;
    }

    /**
     * Setter method for foldepartamento.
     *
     * @param aFoldepartamento the new value for foldepartamento
     */
    public void setFoldepartamento(Foldepartamento aFoldepartamento) {
        foldepartamento = aFoldepartamento;
    }

    /**
     * Compares the key for this instance with another Follocaltrabalho.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Follocaltrabalho and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Follocaltrabalho)) {
            return false;
        }
        Follocaltrabalho that = (Follocaltrabalho) other;
        if (this.getCodfollocaltrabalho() != that.getCodfollocaltrabalho()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Follocaltrabalho.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Follocaltrabalho)) return false;
        return this.equalKeys(other) && ((Follocaltrabalho)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfollocaltrabalho();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Follocaltrabalho |");
        sb.append(" codfollocaltrabalho=").append(getCodfollocaltrabalho());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfollocaltrabalho", Integer.valueOf(getCodfollocaltrabalho()));
        return ret;
    }

}
