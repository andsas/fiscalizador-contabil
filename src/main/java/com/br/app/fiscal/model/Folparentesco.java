package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FOLPARENTESCO")
public class Folparentesco implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolparentesco";

    @Id
    @Column(name="CODFOLPARENTESCO", unique=true, nullable=false, precision=10)
    private int codfolparentesco;
    @Column(name="DESCFOLPARENTESCO", nullable=false, length=250)
    private String descfolparentesco;
    @OneToMany(mappedBy="folparentesco")
    private Set<Folcolaboradordep> folcolaboradordep;

    /** Default constructor. */
    public Folparentesco() {
        super();
    }

    /**
     * Access method for codfolparentesco.
     *
     * @return the current value of codfolparentesco
     */
    public int getCodfolparentesco() {
        return codfolparentesco;
    }

    /**
     * Setter method for codfolparentesco.
     *
     * @param aCodfolparentesco the new value for codfolparentesco
     */
    public void setCodfolparentesco(int aCodfolparentesco) {
        codfolparentesco = aCodfolparentesco;
    }

    /**
     * Access method for descfolparentesco.
     *
     * @return the current value of descfolparentesco
     */
    public String getDescfolparentesco() {
        return descfolparentesco;
    }

    /**
     * Setter method for descfolparentesco.
     *
     * @param aDescfolparentesco the new value for descfolparentesco
     */
    public void setDescfolparentesco(String aDescfolparentesco) {
        descfolparentesco = aDescfolparentesco;
    }

    /**
     * Access method for folcolaboradordep.
     *
     * @return the current value of folcolaboradordep
     */
    public Set<Folcolaboradordep> getFolcolaboradordep() {
        return folcolaboradordep;
    }

    /**
     * Setter method for folcolaboradordep.
     *
     * @param aFolcolaboradordep the new value for folcolaboradordep
     */
    public void setFolcolaboradordep(Set<Folcolaboradordep> aFolcolaboradordep) {
        folcolaboradordep = aFolcolaboradordep;
    }

    /**
     * Compares the key for this instance with another Folparentesco.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folparentesco and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folparentesco)) {
            return false;
        }
        Folparentesco that = (Folparentesco) other;
        if (this.getCodfolparentesco() != that.getCodfolparentesco()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folparentesco.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folparentesco)) return false;
        return this.equalKeys(other) && ((Folparentesco)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolparentesco();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folparentesco |");
        sb.append(" codfolparentesco=").append(getCodfolparentesco());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolparentesco", Integer.valueOf(getCodfolparentesco()));
        return ret;
    }

}
