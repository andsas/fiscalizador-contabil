package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLPREVBEN")
public class Folprevben implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolprevben";

    @Id
    @Column(name="CODFOLPREVBEN", unique=true, nullable=false, precision=10)
    private int codfolprevben;
    @Column(name="DESCFOLPREVBEN", nullable=false, length=250)
    private String descfolprevben;

    /** Default constructor. */
    public Folprevben() {
        super();
    }

    /**
     * Access method for codfolprevben.
     *
     * @return the current value of codfolprevben
     */
    public int getCodfolprevben() {
        return codfolprevben;
    }

    /**
     * Setter method for codfolprevben.
     *
     * @param aCodfolprevben the new value for codfolprevben
     */
    public void setCodfolprevben(int aCodfolprevben) {
        codfolprevben = aCodfolprevben;
    }

    /**
     * Access method for descfolprevben.
     *
     * @return the current value of descfolprevben
     */
    public String getDescfolprevben() {
        return descfolprevben;
    }

    /**
     * Setter method for descfolprevben.
     *
     * @param aDescfolprevben the new value for descfolprevben
     */
    public void setDescfolprevben(String aDescfolprevben) {
        descfolprevben = aDescfolprevben;
    }

    /**
     * Compares the key for this instance with another Folprevben.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folprevben and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folprevben)) {
            return false;
        }
        Folprevben that = (Folprevben) other;
        if (this.getCodfolprevben() != that.getCodfolprevben()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folprevben.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folprevben)) return false;
        return this.equalKeys(other) && ((Folprevben)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolprevben();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folprevben |");
        sb.append(" codfolprevben=").append(getCodfolprevben());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolprevben", Integer.valueOf(getCodfolprevben()));
        return ret;
    }

}
