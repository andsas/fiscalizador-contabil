package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLSALARIO")
public class Folsalario implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolsalario";

    @Id
    @Column(name="CODFOLSALARIO", unique=true, nullable=false, precision=10)
    private int codfolsalario;
    @Column(name="INICIAL", precision=15, scale=4)
    private BigDecimal inicial;
    @Column(name="FINAL", precision=15, scale=4)
    private BigDecimal final_;
    @Column(name="OBSERVACOES")
    private String observacoes;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLTIPOSALARIO", nullable=false)
    private Folsalariotipo folsalariotipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODFOLFUNCAO", nullable=false)
    private Folfuncao folfuncao;
    @ManyToOne
    @JoinColumn(name="CODFOLGRAUINSTRUCAO")
    private Folgrauinstrucao folgrauinstrucao;

    /** Default constructor. */
    public Folsalario() {
        super();
    }

    /**
     * Access method for codfolsalario.
     *
     * @return the current value of codfolsalario
     */
    public int getCodfolsalario() {
        return codfolsalario;
    }

    /**
     * Setter method for codfolsalario.
     *
     * @param aCodfolsalario the new value for codfolsalario
     */
    public void setCodfolsalario(int aCodfolsalario) {
        codfolsalario = aCodfolsalario;
    }

    /**
     * Access method for inicial.
     *
     * @return the current value of inicial
     */
    public BigDecimal getInicial() {
        return inicial;
    }

    /**
     * Setter method for inicial.
     *
     * @param aInicial the new value for inicial
     */
    public void setInicial(BigDecimal aInicial) {
        inicial = aInicial;
    }

    /**
     * Access method for final_.
     *
     * @return the current value of final_
     */
    public BigDecimal getFinal_() {
        return final_;
    }

    /**
     * Setter method for final_.
     *
     * @param aFinal_ the new value for final_
     */
    public void setFinal_(BigDecimal aFinal_) {
        final_ = aFinal_;
    }

    /**
     * Access method for observacoes.
     *
     * @return the current value of observacoes
     */
    public String getObservacoes() {
        return observacoes;
    }

    /**
     * Setter method for observacoes.
     *
     * @param aObservacoes the new value for observacoes
     */
    public void setObservacoes(String aObservacoes) {
        observacoes = aObservacoes;
    }

    /**
     * Access method for folsalariotipo.
     *
     * @return the current value of folsalariotipo
     */
    public Folsalariotipo getFolsalariotipo() {
        return folsalariotipo;
    }

    /**
     * Setter method for folsalariotipo.
     *
     * @param aFolsalariotipo the new value for folsalariotipo
     */
    public void setFolsalariotipo(Folsalariotipo aFolsalariotipo) {
        folsalariotipo = aFolsalariotipo;
    }

    /**
     * Access method for folfuncao.
     *
     * @return the current value of folfuncao
     */
    public Folfuncao getFolfuncao() {
        return folfuncao;
    }

    /**
     * Setter method for folfuncao.
     *
     * @param aFolfuncao the new value for folfuncao
     */
    public void setFolfuncao(Folfuncao aFolfuncao) {
        folfuncao = aFolfuncao;
    }

    /**
     * Access method for folgrauinstrucao.
     *
     * @return the current value of folgrauinstrucao
     */
    public Folgrauinstrucao getFolgrauinstrucao() {
        return folgrauinstrucao;
    }

    /**
     * Setter method for folgrauinstrucao.
     *
     * @param aFolgrauinstrucao the new value for folgrauinstrucao
     */
    public void setFolgrauinstrucao(Folgrauinstrucao aFolgrauinstrucao) {
        folgrauinstrucao = aFolgrauinstrucao;
    }

    /**
     * Compares the key for this instance with another Folsalario.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folsalario and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folsalario)) {
            return false;
        }
        Folsalario that = (Folsalario) other;
        if (this.getCodfolsalario() != that.getCodfolsalario()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folsalario.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folsalario)) return false;
        return this.equalKeys(other) && ((Folsalario)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolsalario();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folsalario |");
        sb.append(" codfolsalario=").append(getCodfolsalario());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolsalario", Integer.valueOf(getCodfolsalario()));
        return ret;
    }

}
