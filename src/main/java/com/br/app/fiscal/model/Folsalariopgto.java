package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLSALARIOPGTO")
public class Folsalariopgto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolsalariopgto";

    @Id
    @Column(name="CODFOLSALARIOPGTO", unique=true, nullable=false, precision=10)
    private int codfolsalariopgto;
    @Column(name="DESCFOLSALARIOPGTO", nullable=false, length=250)
    private String descfolsalariopgto;
    @Column(name="DIAPARCELA1", precision=10)
    private int diaparcela1;
    @Column(name="DIAPARCELA2", precision=10)
    private int diaparcela2;
    @Column(name="QUINTODIAUTIL", precision=5)
    private short quintodiautil;

    /** Default constructor. */
    public Folsalariopgto() {
        super();
    }

    /**
     * Access method for codfolsalariopgto.
     *
     * @return the current value of codfolsalariopgto
     */
    public int getCodfolsalariopgto() {
        return codfolsalariopgto;
    }

    /**
     * Setter method for codfolsalariopgto.
     *
     * @param aCodfolsalariopgto the new value for codfolsalariopgto
     */
    public void setCodfolsalariopgto(int aCodfolsalariopgto) {
        codfolsalariopgto = aCodfolsalariopgto;
    }

    /**
     * Access method for descfolsalariopgto.
     *
     * @return the current value of descfolsalariopgto
     */
    public String getDescfolsalariopgto() {
        return descfolsalariopgto;
    }

    /**
     * Setter method for descfolsalariopgto.
     *
     * @param aDescfolsalariopgto the new value for descfolsalariopgto
     */
    public void setDescfolsalariopgto(String aDescfolsalariopgto) {
        descfolsalariopgto = aDescfolsalariopgto;
    }

    /**
     * Access method for diaparcela1.
     *
     * @return the current value of diaparcela1
     */
    public int getDiaparcela1() {
        return diaparcela1;
    }

    /**
     * Setter method for diaparcela1.
     *
     * @param aDiaparcela1 the new value for diaparcela1
     */
    public void setDiaparcela1(int aDiaparcela1) {
        diaparcela1 = aDiaparcela1;
    }

    /**
     * Access method for diaparcela2.
     *
     * @return the current value of diaparcela2
     */
    public int getDiaparcela2() {
        return diaparcela2;
    }

    /**
     * Setter method for diaparcela2.
     *
     * @param aDiaparcela2 the new value for diaparcela2
     */
    public void setDiaparcela2(int aDiaparcela2) {
        diaparcela2 = aDiaparcela2;
    }

    /**
     * Access method for quintodiautil.
     *
     * @return the current value of quintodiautil
     */
    public short getQuintodiautil() {
        return quintodiautil;
    }

    /**
     * Setter method for quintodiautil.
     *
     * @param aQuintodiautil the new value for quintodiautil
     */
    public void setQuintodiautil(short aQuintodiautil) {
        quintodiautil = aQuintodiautil;
    }

    /**
     * Compares the key for this instance with another Folsalariopgto.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folsalariopgto and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folsalariopgto)) {
            return false;
        }
        Folsalariopgto that = (Folsalariopgto) other;
        if (this.getCodfolsalariopgto() != that.getCodfolsalariopgto()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folsalariopgto.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folsalariopgto)) return false;
        return this.equalKeys(other) && ((Folsalariopgto)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolsalariopgto();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folsalariopgto |");
        sb.append(" codfolsalariopgto=").append(getCodfolsalariopgto());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolsalariopgto", Integer.valueOf(getCodfolsalariopgto()));
        return ret;
    }

}
