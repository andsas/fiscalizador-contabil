package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="FOLSALARIOTIPO")
public class Folsalariotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolsalariotipo";

    @Id
    @Column(name="CODFOLSALARIOTIPO", unique=true, nullable=false, precision=10)
    private int codfolsalariotipo;
    @Column(name="DESCFOLSALARIOTIPO", nullable=false, length=250)
    private String descfolsalariotipo;
    @OneToMany(mappedBy="folsalariotipo")
    private Set<Folsalario> folsalario;

    /** Default constructor. */
    public Folsalariotipo() {
        super();
    }

    /**
     * Access method for codfolsalariotipo.
     *
     * @return the current value of codfolsalariotipo
     */
    public int getCodfolsalariotipo() {
        return codfolsalariotipo;
    }

    /**
     * Setter method for codfolsalariotipo.
     *
     * @param aCodfolsalariotipo the new value for codfolsalariotipo
     */
    public void setCodfolsalariotipo(int aCodfolsalariotipo) {
        codfolsalariotipo = aCodfolsalariotipo;
    }

    /**
     * Access method for descfolsalariotipo.
     *
     * @return the current value of descfolsalariotipo
     */
    public String getDescfolsalariotipo() {
        return descfolsalariotipo;
    }

    /**
     * Setter method for descfolsalariotipo.
     *
     * @param aDescfolsalariotipo the new value for descfolsalariotipo
     */
    public void setDescfolsalariotipo(String aDescfolsalariotipo) {
        descfolsalariotipo = aDescfolsalariotipo;
    }

    /**
     * Access method for folsalario.
     *
     * @return the current value of folsalario
     */
    public Set<Folsalario> getFolsalario() {
        return folsalario;
    }

    /**
     * Setter method for folsalario.
     *
     * @param aFolsalario the new value for folsalario
     */
    public void setFolsalario(Set<Folsalario> aFolsalario) {
        folsalario = aFolsalario;
    }

    /**
     * Compares the key for this instance with another Folsalariotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folsalariotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folsalariotipo)) {
            return false;
        }
        Folsalariotipo that = (Folsalariotipo) other;
        if (this.getCodfolsalariotipo() != that.getCodfolsalariotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folsalariotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folsalariotipo)) return false;
        return this.equalKeys(other) && ((Folsalariotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolsalariotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folsalariotipo |");
        sb.append(" codfolsalariotipo=").append(getCodfolsalariotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolsalariotipo", Integer.valueOf(getCodfolsalariotipo()));
        return ret;
    }

}
