package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLSALFAMILIA")
public class Folsalfamilia implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolsalfamilia";

    @Id
    @Column(name="CODFOLSALFAMILIA", unique=true, nullable=false, precision=10)
    private int codfolsalfamilia;
    @Column(name="DESCFOLSALFAMILIA", nullable=false, length=250)
    private String descfolsalfamilia;
    @Column(name="VALOR", nullable=false, precision=15, scale=4)
    private BigDecimal valor;
    @ManyToOne
    @JoinColumn(name="CODFOLARTIGO")
    private Folartigo folartigo;

    /** Default constructor. */
    public Folsalfamilia() {
        super();
    }

    /**
     * Access method for codfolsalfamilia.
     *
     * @return the current value of codfolsalfamilia
     */
    public int getCodfolsalfamilia() {
        return codfolsalfamilia;
    }

    /**
     * Setter method for codfolsalfamilia.
     *
     * @param aCodfolsalfamilia the new value for codfolsalfamilia
     */
    public void setCodfolsalfamilia(int aCodfolsalfamilia) {
        codfolsalfamilia = aCodfolsalfamilia;
    }

    /**
     * Access method for descfolsalfamilia.
     *
     * @return the current value of descfolsalfamilia
     */
    public String getDescfolsalfamilia() {
        return descfolsalfamilia;
    }

    /**
     * Setter method for descfolsalfamilia.
     *
     * @param aDescfolsalfamilia the new value for descfolsalfamilia
     */
    public void setDescfolsalfamilia(String aDescfolsalfamilia) {
        descfolsalfamilia = aDescfolsalfamilia;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(BigDecimal aValor) {
        valor = aValor;
    }

    /**
     * Access method for folartigo.
     *
     * @return the current value of folartigo
     */
    public Folartigo getFolartigo() {
        return folartigo;
    }

    /**
     * Setter method for folartigo.
     *
     * @param aFolartigo the new value for folartigo
     */
    public void setFolartigo(Folartigo aFolartigo) {
        folartigo = aFolartigo;
    }

    /**
     * Compares the key for this instance with another Folsalfamilia.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folsalfamilia and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folsalfamilia)) {
            return false;
        }
        Folsalfamilia that = (Folsalfamilia) other;
        if (this.getCodfolsalfamilia() != that.getCodfolsalfamilia()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folsalfamilia.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folsalfamilia)) return false;
        return this.equalKeys(other) && ((Folsalfamilia)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolsalfamilia();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folsalfamilia |");
        sb.append(" codfolsalfamilia=").append(getCodfolsalfamilia());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolsalfamilia", Integer.valueOf(getCodfolsalfamilia()));
        return ret;
    }

}
