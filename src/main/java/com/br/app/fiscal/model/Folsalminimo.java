package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLSALMINIMO")
public class Folsalminimo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolsalminimo";

    @Id
    @Column(name="CODFOLSALMINIMO", unique=true, nullable=false, precision=10)
    private int codfolsalminimo;
    @Column(name="DESCFOLSALMINIMO", nullable=false, length=250)
    private String descfolsalminimo;
    @Column(name="VALOR", nullable=false, precision=15, scale=4)
    private BigDecimal valor;
    @Column(name="DATAATUALIZACAO", nullable=false)
    private Timestamp dataatualizacao;
    @ManyToOne
    @JoinColumn(name="CODFOLARTIGO")
    private Folartigo folartigo;
    @ManyToOne
    @JoinColumn(name="CODCONFUF")
    private Confuf confuf;

    /** Default constructor. */
    public Folsalminimo() {
        super();
    }

    /**
     * Access method for codfolsalminimo.
     *
     * @return the current value of codfolsalminimo
     */
    public int getCodfolsalminimo() {
        return codfolsalminimo;
    }

    /**
     * Setter method for codfolsalminimo.
     *
     * @param aCodfolsalminimo the new value for codfolsalminimo
     */
    public void setCodfolsalminimo(int aCodfolsalminimo) {
        codfolsalminimo = aCodfolsalminimo;
    }

    /**
     * Access method for descfolsalminimo.
     *
     * @return the current value of descfolsalminimo
     */
    public String getDescfolsalminimo() {
        return descfolsalminimo;
    }

    /**
     * Setter method for descfolsalminimo.
     *
     * @param aDescfolsalminimo the new value for descfolsalminimo
     */
    public void setDescfolsalminimo(String aDescfolsalminimo) {
        descfolsalminimo = aDescfolsalminimo;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(BigDecimal aValor) {
        valor = aValor;
    }

    /**
     * Access method for dataatualizacao.
     *
     * @return the current value of dataatualizacao
     */
    public Timestamp getDataatualizacao() {
        return dataatualizacao;
    }

    /**
     * Setter method for dataatualizacao.
     *
     * @param aDataatualizacao the new value for dataatualizacao
     */
    public void setDataatualizacao(Timestamp aDataatualizacao) {
        dataatualizacao = aDataatualizacao;
    }

    /**
     * Access method for folartigo.
     *
     * @return the current value of folartigo
     */
    public Folartigo getFolartigo() {
        return folartigo;
    }

    /**
     * Setter method for folartigo.
     *
     * @param aFolartigo the new value for folartigo
     */
    public void setFolartigo(Folartigo aFolartigo) {
        folartigo = aFolartigo;
    }

    /**
     * Access method for confuf.
     *
     * @return the current value of confuf
     */
    public Confuf getConfuf() {
        return confuf;
    }

    /**
     * Setter method for confuf.
     *
     * @param aConfuf the new value for confuf
     */
    public void setConfuf(Confuf aConfuf) {
        confuf = aConfuf;
    }

    /**
     * Compares the key for this instance with another Folsalminimo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folsalminimo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folsalminimo)) {
            return false;
        }
        Folsalminimo that = (Folsalminimo) other;
        if (this.getCodfolsalminimo() != that.getCodfolsalminimo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folsalminimo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folsalminimo)) return false;
        return this.equalKeys(other) && ((Folsalminimo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolsalminimo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folsalminimo |");
        sb.append(" codfolsalminimo=").append(getCodfolsalminimo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolsalminimo", Integer.valueOf(getCodfolsalminimo()));
        return ret;
    }

}
