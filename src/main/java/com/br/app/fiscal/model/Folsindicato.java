package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="FOLSINDICATO")
public class Folsindicato implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolsindicato";

    @Id
    @Column(name="CODFOLSINDICATO", unique=true, nullable=false, precision=10)
    private int codfolsindicato;
    @Column(name="NOMEFOLSINDICATO", nullable=false, length=250)
    private String nomefolsindicato;
    @Column(name="REGISTRO", length=20)
    private String registro;
    @ManyToOne
    @JoinColumn(name="CODFOLARTIGO")
    private Folartigo folartigo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;

    /** Default constructor. */
    public Folsindicato() {
        super();
    }

    /**
     * Access method for codfolsindicato.
     *
     * @return the current value of codfolsindicato
     */
    public int getCodfolsindicato() {
        return codfolsindicato;
    }

    /**
     * Setter method for codfolsindicato.
     *
     * @param aCodfolsindicato the new value for codfolsindicato
     */
    public void setCodfolsindicato(int aCodfolsindicato) {
        codfolsindicato = aCodfolsindicato;
    }

    /**
     * Access method for nomefolsindicato.
     *
     * @return the current value of nomefolsindicato
     */
    public String getNomefolsindicato() {
        return nomefolsindicato;
    }

    /**
     * Setter method for nomefolsindicato.
     *
     * @param aNomefolsindicato the new value for nomefolsindicato
     */
    public void setNomefolsindicato(String aNomefolsindicato) {
        nomefolsindicato = aNomefolsindicato;
    }

    /**
     * Access method for registro.
     *
     * @return the current value of registro
     */
    public String getRegistro() {
        return registro;
    }

    /**
     * Setter method for registro.
     *
     * @param aRegistro the new value for registro
     */
    public void setRegistro(String aRegistro) {
        registro = aRegistro;
    }

    /**
     * Access method for folartigo.
     *
     * @return the current value of folartigo
     */
    public Folartigo getFolartigo() {
        return folartigo;
    }

    /**
     * Setter method for folartigo.
     *
     * @param aFolartigo the new value for folartigo
     */
    public void setFolartigo(Folartigo aFolartigo) {
        folartigo = aFolartigo;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Compares the key for this instance with another Folsindicato.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folsindicato and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folsindicato)) {
            return false;
        }
        Folsindicato that = (Folsindicato) other;
        if (this.getCodfolsindicato() != that.getCodfolsindicato()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folsindicato.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folsindicato)) return false;
        return this.equalKeys(other) && ((Folsindicato)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolsindicato();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folsindicato |");
        sb.append(" codfolsindicato=").append(getCodfolsindicato());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolsindicato", Integer.valueOf(getCodfolsindicato()));
        return ret;
    }

}
