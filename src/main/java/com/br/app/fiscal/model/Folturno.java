package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Time;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="FOLTURNO")
public class Folturno implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codfolturno";

    @Id
    @Column(name="CODFOLTURNO", unique=true, nullable=false, precision=10)
    private int codfolturno;
    @Column(name="DESCFOLTURNO", nullable=false, length=250)
    private String descfolturno;
    @Column(name="HORARIOINICIAL")
    private Time horarioinicial;
    @Column(name="HORARIOFINAL")
    private Time horariofinal;
    @Column(name="DESCRICAOHORARIO", length=250)
    private String descricaohorario;

    /** Default constructor. */
    public Folturno() {
        super();
    }

    /**
     * Access method for codfolturno.
     *
     * @return the current value of codfolturno
     */
    public int getCodfolturno() {
        return codfolturno;
    }

    /**
     * Setter method for codfolturno.
     *
     * @param aCodfolturno the new value for codfolturno
     */
    public void setCodfolturno(int aCodfolturno) {
        codfolturno = aCodfolturno;
    }

    /**
     * Access method for descfolturno.
     *
     * @return the current value of descfolturno
     */
    public String getDescfolturno() {
        return descfolturno;
    }

    /**
     * Setter method for descfolturno.
     *
     * @param aDescfolturno the new value for descfolturno
     */
    public void setDescfolturno(String aDescfolturno) {
        descfolturno = aDescfolturno;
    }

    /**
     * Access method for horarioinicial.
     *
     * @return the current value of horarioinicial
     */
    public Time getHorarioinicial() {
        return horarioinicial;
    }

    /**
     * Setter method for horarioinicial.
     *
     * @param aHorarioinicial the new value for horarioinicial
     */
    public void setHorarioinicial(Time aHorarioinicial) {
        horarioinicial = aHorarioinicial;
    }

    /**
     * Access method for horariofinal.
     *
     * @return the current value of horariofinal
     */
    public Time getHorariofinal() {
        return horariofinal;
    }

    /**
     * Setter method for horariofinal.
     *
     * @param aHorariofinal the new value for horariofinal
     */
    public void setHorariofinal(Time aHorariofinal) {
        horariofinal = aHorariofinal;
    }

    /**
     * Access method for descricaohorario.
     *
     * @return the current value of descricaohorario
     */
    public String getDescricaohorario() {
        return descricaohorario;
    }

    /**
     * Setter method for descricaohorario.
     *
     * @param aDescricaohorario the new value for descricaohorario
     */
    public void setDescricaohorario(String aDescricaohorario) {
        descricaohorario = aDescricaohorario;
    }

    /**
     * Compares the key for this instance with another Folturno.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Folturno and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Folturno)) {
            return false;
        }
        Folturno that = (Folturno) other;
        if (this.getCodfolturno() != that.getCodfolturno()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Folturno.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Folturno)) return false;
        return this.equalKeys(other) && ((Folturno)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodfolturno();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Folturno |");
        sb.append(" codfolturno=").append(getCodfolturno());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codfolturno", Integer.valueOf(getCodfolturno()));
        return ret;
    }

}
