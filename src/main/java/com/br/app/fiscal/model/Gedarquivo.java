package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="GEDARQUIVO")
public class Gedarquivo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgedarquivo";

    @Id
    @Column(name="CODGEDARQUIVO", unique=true, nullable=false, precision=10)
    private int codgedarquivo;
    @Column(name="DESCGEDARQUIVO", nullable=false, length=250)
    private String descgedarquivo;
    @Column(name="NOMETABELA", nullable=false, length=60)
    private String nometabela;
    @Column(name="CODTABELA", nullable=false, length=60)
    private String codtabela;
    @Column(name="PATHARQUIVO", length=250)
    private String patharquivo;
    @Column(name="BLOBARQUIVO")
    private byte[] blobarquivo;
    @OneToMany(mappedBy="gedarquivo")
    private Set<Ecarquivamento> ecarquivamento;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGEDTIPO", nullable=false)
    private Gedarquivotipo gedarquivotipo;
    @OneToMany(mappedBy="gedarquivo")
    private Set<Gedarquivoversao> gedarquivoversao;

    /** Default constructor. */
    public Gedarquivo() {
        super();
    }

    /**
     * Access method for codgedarquivo.
     *
     * @return the current value of codgedarquivo
     */
    public int getCodgedarquivo() {
        return codgedarquivo;
    }

    /**
     * Setter method for codgedarquivo.
     *
     * @param aCodgedarquivo the new value for codgedarquivo
     */
    public void setCodgedarquivo(int aCodgedarquivo) {
        codgedarquivo = aCodgedarquivo;
    }

    /**
     * Access method for descgedarquivo.
     *
     * @return the current value of descgedarquivo
     */
    public String getDescgedarquivo() {
        return descgedarquivo;
    }

    /**
     * Setter method for descgedarquivo.
     *
     * @param aDescgedarquivo the new value for descgedarquivo
     */
    public void setDescgedarquivo(String aDescgedarquivo) {
        descgedarquivo = aDescgedarquivo;
    }

    /**
     * Access method for nometabela.
     *
     * @return the current value of nometabela
     */
    public String getNometabela() {
        return nometabela;
    }

    /**
     * Setter method for nometabela.
     *
     * @param aNometabela the new value for nometabela
     */
    public void setNometabela(String aNometabela) {
        nometabela = aNometabela;
    }

    /**
     * Access method for codtabela.
     *
     * @return the current value of codtabela
     */
    public String getCodtabela() {
        return codtabela;
    }

    /**
     * Setter method for codtabela.
     *
     * @param aCodtabela the new value for codtabela
     */
    public void setCodtabela(String aCodtabela) {
        codtabela = aCodtabela;
    }

    /**
     * Access method for patharquivo.
     *
     * @return the current value of patharquivo
     */
    public String getPatharquivo() {
        return patharquivo;
    }

    /**
     * Setter method for patharquivo.
     *
     * @param aPatharquivo the new value for patharquivo
     */
    public void setPatharquivo(String aPatharquivo) {
        patharquivo = aPatharquivo;
    }

    /**
     * Access method for blobarquivo.
     *
     * @return the current value of blobarquivo
     */
    public byte[] getBlobarquivo() {
        return blobarquivo;
    }

    /**
     * Setter method for blobarquivo.
     *
     * @param aBlobarquivo the new value for blobarquivo
     */
    public void setBlobarquivo(byte[] aBlobarquivo) {
        blobarquivo = aBlobarquivo;
    }

    /**
     * Access method for ecarquivamento.
     *
     * @return the current value of ecarquivamento
     */
    public Set<Ecarquivamento> getEcarquivamento() {
        return ecarquivamento;
    }

    /**
     * Setter method for ecarquivamento.
     *
     * @param aEcarquivamento the new value for ecarquivamento
     */
    public void setEcarquivamento(Set<Ecarquivamento> aEcarquivamento) {
        ecarquivamento = aEcarquivamento;
    }

    /**
     * Access method for gedarquivotipo.
     *
     * @return the current value of gedarquivotipo
     */
    public Gedarquivotipo getGedarquivotipo() {
        return gedarquivotipo;
    }

    /**
     * Setter method for gedarquivotipo.
     *
     * @param aGedarquivotipo the new value for gedarquivotipo
     */
    public void setGedarquivotipo(Gedarquivotipo aGedarquivotipo) {
        gedarquivotipo = aGedarquivotipo;
    }

    /**
     * Access method for gedarquivoversao.
     *
     * @return the current value of gedarquivoversao
     */
    public Set<Gedarquivoversao> getGedarquivoversao() {
        return gedarquivoversao;
    }

    /**
     * Setter method for gedarquivoversao.
     *
     * @param aGedarquivoversao the new value for gedarquivoversao
     */
    public void setGedarquivoversao(Set<Gedarquivoversao> aGedarquivoversao) {
        gedarquivoversao = aGedarquivoversao;
    }

    /**
     * Compares the key for this instance with another Gedarquivo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Gedarquivo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Gedarquivo)) {
            return false;
        }
        Gedarquivo that = (Gedarquivo) other;
        if (this.getCodgedarquivo() != that.getCodgedarquivo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Gedarquivo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gedarquivo)) return false;
        return this.equalKeys(other) && ((Gedarquivo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgedarquivo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Gedarquivo |");
        sb.append(" codgedarquivo=").append(getCodgedarquivo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgedarquivo", Integer.valueOf(getCodgedarquivo()));
        return ret;
    }

}
