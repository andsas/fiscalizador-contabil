package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="GEDARQUIVOTIPO")
public class Gedarquivotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgedarquivotipo";

    @Id
    @Column(name="CODGEDARQUIVOTIPO", unique=true, nullable=false, precision=10)
    private int codgedarquivotipo;
    @Column(name="DESCGEDARQUIVOTIPO", nullable=false, length=250)
    private String descgedarquivotipo;
    @Column(name="TIPOGEDARQUIVOTIPO", precision=5)
    private short tipogedarquivotipo;
    @OneToMany(mappedBy="gedarquivotipo")
    private Set<Gedarquivo> gedarquivo;

    /** Default constructor. */
    public Gedarquivotipo() {
        super();
    }

    /**
     * Access method for codgedarquivotipo.
     *
     * @return the current value of codgedarquivotipo
     */
    public int getCodgedarquivotipo() {
        return codgedarquivotipo;
    }

    /**
     * Setter method for codgedarquivotipo.
     *
     * @param aCodgedarquivotipo the new value for codgedarquivotipo
     */
    public void setCodgedarquivotipo(int aCodgedarquivotipo) {
        codgedarquivotipo = aCodgedarquivotipo;
    }

    /**
     * Access method for descgedarquivotipo.
     *
     * @return the current value of descgedarquivotipo
     */
    public String getDescgedarquivotipo() {
        return descgedarquivotipo;
    }

    /**
     * Setter method for descgedarquivotipo.
     *
     * @param aDescgedarquivotipo the new value for descgedarquivotipo
     */
    public void setDescgedarquivotipo(String aDescgedarquivotipo) {
        descgedarquivotipo = aDescgedarquivotipo;
    }

    /**
     * Access method for tipogedarquivotipo.
     *
     * @return the current value of tipogedarquivotipo
     */
    public short getTipogedarquivotipo() {
        return tipogedarquivotipo;
    }

    /**
     * Setter method for tipogedarquivotipo.
     *
     * @param aTipogedarquivotipo the new value for tipogedarquivotipo
     */
    public void setTipogedarquivotipo(short aTipogedarquivotipo) {
        tipogedarquivotipo = aTipogedarquivotipo;
    }

    /**
     * Access method for gedarquivo.
     *
     * @return the current value of gedarquivo
     */
    public Set<Gedarquivo> getGedarquivo() {
        return gedarquivo;
    }

    /**
     * Setter method for gedarquivo.
     *
     * @param aGedarquivo the new value for gedarquivo
     */
    public void setGedarquivo(Set<Gedarquivo> aGedarquivo) {
        gedarquivo = aGedarquivo;
    }

    /**
     * Compares the key for this instance with another Gedarquivotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Gedarquivotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Gedarquivotipo)) {
            return false;
        }
        Gedarquivotipo that = (Gedarquivotipo) other;
        if (this.getCodgedarquivotipo() != that.getCodgedarquivotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Gedarquivotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gedarquivotipo)) return false;
        return this.equalKeys(other) && ((Gedarquivotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgedarquivotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Gedarquivotipo |");
        sb.append(" codgedarquivotipo=").append(getCodgedarquivotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgedarquivotipo", Integer.valueOf(getCodgedarquivotipo()));
        return ret;
    }

}
