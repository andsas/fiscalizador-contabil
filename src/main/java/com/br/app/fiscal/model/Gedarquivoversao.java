package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="GEDARQUIVOVERSAO")
public class Gedarquivoversao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgedarquivoversao";

    @Id
    @Column(name="CODGEDARQUIVOVERSAO", unique=true, nullable=false, precision=10)
    private int codgedarquivoversao;
    @Column(name="CODCONFUSUARIOCAD", nullable=false, length=20)
    private String codconfusuariocad;
    @Column(name="CODCONFUSUARIOALT", length=20)
    private String codconfusuarioalt;
    @Column(name="CODCONFUSUARIODEL", length=20)
    private String codconfusuariodel;
    @Column(name="DATACAD", nullable=false)
    private Timestamp datacad;
    @Column(name="DATAALT")
    private Timestamp dataalt;
    @Column(name="DATAEXC")
    private Timestamp dataexc;
    @Column(name="INCGEDARQUIVOVERSAO", nullable=false, precision=10)
    private int incgedarquivoversao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGEDARQUIVO", nullable=false)
    private Gedarquivo gedarquivo;

    /** Default constructor. */
    public Gedarquivoversao() {
        super();
    }

    /**
     * Access method for codgedarquivoversao.
     *
     * @return the current value of codgedarquivoversao
     */
    public int getCodgedarquivoversao() {
        return codgedarquivoversao;
    }

    /**
     * Setter method for codgedarquivoversao.
     *
     * @param aCodgedarquivoversao the new value for codgedarquivoversao
     */
    public void setCodgedarquivoversao(int aCodgedarquivoversao) {
        codgedarquivoversao = aCodgedarquivoversao;
    }

    /**
     * Access method for codconfusuariocad.
     *
     * @return the current value of codconfusuariocad
     */
    public String getCodconfusuariocad() {
        return codconfusuariocad;
    }

    /**
     * Setter method for codconfusuariocad.
     *
     * @param aCodconfusuariocad the new value for codconfusuariocad
     */
    public void setCodconfusuariocad(String aCodconfusuariocad) {
        codconfusuariocad = aCodconfusuariocad;
    }

    /**
     * Access method for codconfusuarioalt.
     *
     * @return the current value of codconfusuarioalt
     */
    public String getCodconfusuarioalt() {
        return codconfusuarioalt;
    }

    /**
     * Setter method for codconfusuarioalt.
     *
     * @param aCodconfusuarioalt the new value for codconfusuarioalt
     */
    public void setCodconfusuarioalt(String aCodconfusuarioalt) {
        codconfusuarioalt = aCodconfusuarioalt;
    }

    /**
     * Access method for codconfusuariodel.
     *
     * @return the current value of codconfusuariodel
     */
    public String getCodconfusuariodel() {
        return codconfusuariodel;
    }

    /**
     * Setter method for codconfusuariodel.
     *
     * @param aCodconfusuariodel the new value for codconfusuariodel
     */
    public void setCodconfusuariodel(String aCodconfusuariodel) {
        codconfusuariodel = aCodconfusuariodel;
    }

    /**
     * Access method for datacad.
     *
     * @return the current value of datacad
     */
    public Timestamp getDatacad() {
        return datacad;
    }

    /**
     * Setter method for datacad.
     *
     * @param aDatacad the new value for datacad
     */
    public void setDatacad(Timestamp aDatacad) {
        datacad = aDatacad;
    }

    /**
     * Access method for dataalt.
     *
     * @return the current value of dataalt
     */
    public Timestamp getDataalt() {
        return dataalt;
    }

    /**
     * Setter method for dataalt.
     *
     * @param aDataalt the new value for dataalt
     */
    public void setDataalt(Timestamp aDataalt) {
        dataalt = aDataalt;
    }

    /**
     * Access method for dataexc.
     *
     * @return the current value of dataexc
     */
    public Timestamp getDataexc() {
        return dataexc;
    }

    /**
     * Setter method for dataexc.
     *
     * @param aDataexc the new value for dataexc
     */
    public void setDataexc(Timestamp aDataexc) {
        dataexc = aDataexc;
    }

    /**
     * Access method for incgedarquivoversao.
     *
     * @return the current value of incgedarquivoversao
     */
    public int getIncgedarquivoversao() {
        return incgedarquivoversao;
    }

    /**
     * Setter method for incgedarquivoversao.
     *
     * @param aIncgedarquivoversao the new value for incgedarquivoversao
     */
    public void setIncgedarquivoversao(int aIncgedarquivoversao) {
        incgedarquivoversao = aIncgedarquivoversao;
    }

    /**
     * Access method for gedarquivo.
     *
     * @return the current value of gedarquivo
     */
    public Gedarquivo getGedarquivo() {
        return gedarquivo;
    }

    /**
     * Setter method for gedarquivo.
     *
     * @param aGedarquivo the new value for gedarquivo
     */
    public void setGedarquivo(Gedarquivo aGedarquivo) {
        gedarquivo = aGedarquivo;
    }

    /**
     * Compares the key for this instance with another Gedarquivoversao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Gedarquivoversao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Gedarquivoversao)) {
            return false;
        }
        Gedarquivoversao that = (Gedarquivoversao) other;
        if (this.getCodgedarquivoversao() != that.getCodgedarquivoversao()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Gedarquivoversao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gedarquivoversao)) return false;
        return this.equalKeys(other) && ((Gedarquivoversao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgedarquivoversao();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Gedarquivoversao |");
        sb.append(" codgedarquivoversao=").append(getCodgedarquivoversao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgedarquivoversao", Integer.valueOf(getCodgedarquivoversao()));
        return ret;
    }

}
