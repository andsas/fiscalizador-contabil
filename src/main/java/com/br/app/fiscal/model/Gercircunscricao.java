package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="GERCIRCUNSCRICAO")
public class Gercircunscricao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgercircunscricao";

    @Id
    @Column(name="CODGERCIRCUNSCRICAO", unique=true, nullable=false, precision=10)
    private int codgercircunscricao;
    @Column(name="CODAGAGENTE", nullable=false, precision=10)
    private int codagagente;
    @Column(name="NOMEGERCIRCUNSCRICAO", nullable=false, length=250)
    private String nomegercircunscricao;
    @OneToMany(mappedBy="gercircunscricao")
    private Set<Gerimovel> gerimovel;

    /** Default constructor. */
    public Gercircunscricao() {
        super();
    }

    /**
     * Access method for codgercircunscricao.
     *
     * @return the current value of codgercircunscricao
     */
    public int getCodgercircunscricao() {
        return codgercircunscricao;
    }

    /**
     * Setter method for codgercircunscricao.
     *
     * @param aCodgercircunscricao the new value for codgercircunscricao
     */
    public void setCodgercircunscricao(int aCodgercircunscricao) {
        codgercircunscricao = aCodgercircunscricao;
    }

    /**
     * Access method for codagagente.
     *
     * @return the current value of codagagente
     */
    public int getCodagagente() {
        return codagagente;
    }

    /**
     * Setter method for codagagente.
     *
     * @param aCodagagente the new value for codagagente
     */
    public void setCodagagente(int aCodagagente) {
        codagagente = aCodagagente;
    }

    /**
     * Access method for nomegercircunscricao.
     *
     * @return the current value of nomegercircunscricao
     */
    public String getNomegercircunscricao() {
        return nomegercircunscricao;
    }

    /**
     * Setter method for nomegercircunscricao.
     *
     * @param aNomegercircunscricao the new value for nomegercircunscricao
     */
    public void setNomegercircunscricao(String aNomegercircunscricao) {
        nomegercircunscricao = aNomegercircunscricao;
    }

    /**
     * Access method for gerimovel.
     *
     * @return the current value of gerimovel
     */
    public Set<Gerimovel> getGerimovel() {
        return gerimovel;
    }

    /**
     * Setter method for gerimovel.
     *
     * @param aGerimovel the new value for gerimovel
     */
    public void setGerimovel(Set<Gerimovel> aGerimovel) {
        gerimovel = aGerimovel;
    }

    /**
     * Compares the key for this instance with another Gercircunscricao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Gercircunscricao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Gercircunscricao)) {
            return false;
        }
        Gercircunscricao that = (Gercircunscricao) other;
        if (this.getCodgercircunscricao() != that.getCodgercircunscricao()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Gercircunscricao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gercircunscricao)) return false;
        return this.equalKeys(other) && ((Gercircunscricao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgercircunscricao();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Gercircunscricao |");
        sb.append(" codgercircunscricao=").append(getCodgercircunscricao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgercircunscricao", Integer.valueOf(getCodgercircunscricao()));
        return ret;
    }

}
