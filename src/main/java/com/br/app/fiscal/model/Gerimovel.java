package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="GERIMOVEL")
public class Gerimovel implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgerimovel";

    @Id
    @Column(name="CODGERIMOVEL", unique=true, nullable=false, precision=10)
    private int codgerimovel;
    @Column(name="DESCGERIMOVEL", nullable=false, length=250)
    private String descgerimovel;
    @Column(name="TIPOGERIMOVEL", nullable=false, precision=5)
    private short tipogerimovel;
    @Column(name="REGISTRO", nullable=false, length=40)
    private String registro;
    @Column(name="TRANSCRICAO", length=40)
    private String transcricao;
    @Column(name="DESCCOMPLETA")
    private String desccompleta;
    @Column(name="DATACONSTRUCAO")
    private Timestamp dataconstrucao;
    @Column(name="CODPRODMODELO", precision=10)
    private int codprodmodelo;
    @Column(name="INSCFISCAL", length=40)
    private String inscfiscal;
    @Column(name="INSCIMOBILIARIA", length=40)
    private String inscimobiliaria;
    @Column(name="INSCINCRA", length=40)
    private String inscincra;
    @Column(name="INSCCAFIR", length=40)
    private String insccafir;
    @Column(name="LOCQUADRA", length=40)
    private String locquadra;
    @Column(name="LOCLOTE", length=40)
    private String loclote;
    @Column(name="LOCFRACAOIDEAL", precision=15, scale=4)
    private BigDecimal locfracaoideal;
    @Column(name="LOCAREAM2", precision=15, scale=4)
    private BigDecimal locaream2;
    @Column(name="LOCAREAALQUEIRE", precision=15, scale=4)
    private BigDecimal locareaalqueire;
    @Column(name="LOCAREAEDTOTAL", precision=15, scale=4)
    private BigDecimal locareaedtotal;
    @Column(name="LOCAREAEDUTIL", precision=15, scale=4)
    private BigDecimal locareaedutil;
    @Column(name="VALORAQUISICAO", precision=15, scale=4)
    private BigDecimal valoraquisicao;
    @Column(name="VALORCONTABIL", precision=15, scale=4)
    private BigDecimal valorcontabil;
    @Column(name="VALORVENAL", precision=15, scale=4)
    private BigDecimal valorvenal;
    @Column(name="VALORMERCADO", precision=15, scale=4)
    private BigDecimal valormercado;
    @OneToMany(mappedBy="gerimovel")
    private Set<Ctbbemimoapolice> ctbbemimoapolice;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFLOGRADOURO", nullable=false)
    private Conflogradouro conflogradouro;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODPRODPRODUTO")
    private Prodproduto prodproduto;
    @ManyToOne
    @JoinColumn(name="CODGERCIRCUNSCRICAO")
    private Gercircunscricao gercircunscricao;
    @OneToMany(mappedBy="gerimovel")
    private Set<Gerimovelcarac> gerimovelcarac;

    /** Default constructor. */
    public Gerimovel() {
        super();
    }

    /**
     * Access method for codgerimovel.
     *
     * @return the current value of codgerimovel
     */
    public int getCodgerimovel() {
        return codgerimovel;
    }

    /**
     * Setter method for codgerimovel.
     *
     * @param aCodgerimovel the new value for codgerimovel
     */
    public void setCodgerimovel(int aCodgerimovel) {
        codgerimovel = aCodgerimovel;
    }

    /**
     * Access method for descgerimovel.
     *
     * @return the current value of descgerimovel
     */
    public String getDescgerimovel() {
        return descgerimovel;
    }

    /**
     * Setter method for descgerimovel.
     *
     * @param aDescgerimovel the new value for descgerimovel
     */
    public void setDescgerimovel(String aDescgerimovel) {
        descgerimovel = aDescgerimovel;
    }

    /**
     * Access method for tipogerimovel.
     *
     * @return the current value of tipogerimovel
     */
    public short getTipogerimovel() {
        return tipogerimovel;
    }

    /**
     * Setter method for tipogerimovel.
     *
     * @param aTipogerimovel the new value for tipogerimovel
     */
    public void setTipogerimovel(short aTipogerimovel) {
        tipogerimovel = aTipogerimovel;
    }

    /**
     * Access method for registro.
     *
     * @return the current value of registro
     */
    public String getRegistro() {
        return registro;
    }

    /**
     * Setter method for registro.
     *
     * @param aRegistro the new value for registro
     */
    public void setRegistro(String aRegistro) {
        registro = aRegistro;
    }

    /**
     * Access method for transcricao.
     *
     * @return the current value of transcricao
     */
    public String getTranscricao() {
        return transcricao;
    }

    /**
     * Setter method for transcricao.
     *
     * @param aTranscricao the new value for transcricao
     */
    public void setTranscricao(String aTranscricao) {
        transcricao = aTranscricao;
    }

    /**
     * Access method for desccompleta.
     *
     * @return the current value of desccompleta
     */
    public String getDesccompleta() {
        return desccompleta;
    }

    /**
     * Setter method for desccompleta.
     *
     * @param aDesccompleta the new value for desccompleta
     */
    public void setDesccompleta(String aDesccompleta) {
        desccompleta = aDesccompleta;
    }

    /**
     * Access method for dataconstrucao.
     *
     * @return the current value of dataconstrucao
     */
    public Timestamp getDataconstrucao() {
        return dataconstrucao;
    }

    /**
     * Setter method for dataconstrucao.
     *
     * @param aDataconstrucao the new value for dataconstrucao
     */
    public void setDataconstrucao(Timestamp aDataconstrucao) {
        dataconstrucao = aDataconstrucao;
    }

    /**
     * Access method for codprodmodelo.
     *
     * @return the current value of codprodmodelo
     */
    public int getCodprodmodelo() {
        return codprodmodelo;
    }

    /**
     * Setter method for codprodmodelo.
     *
     * @param aCodprodmodelo the new value for codprodmodelo
     */
    public void setCodprodmodelo(int aCodprodmodelo) {
        codprodmodelo = aCodprodmodelo;
    }

    /**
     * Access method for inscfiscal.
     *
     * @return the current value of inscfiscal
     */
    public String getInscfiscal() {
        return inscfiscal;
    }

    /**
     * Setter method for inscfiscal.
     *
     * @param aInscfiscal the new value for inscfiscal
     */
    public void setInscfiscal(String aInscfiscal) {
        inscfiscal = aInscfiscal;
    }

    /**
     * Access method for inscimobiliaria.
     *
     * @return the current value of inscimobiliaria
     */
    public String getInscimobiliaria() {
        return inscimobiliaria;
    }

    /**
     * Setter method for inscimobiliaria.
     *
     * @param aInscimobiliaria the new value for inscimobiliaria
     */
    public void setInscimobiliaria(String aInscimobiliaria) {
        inscimobiliaria = aInscimobiliaria;
    }

    /**
     * Access method for inscincra.
     *
     * @return the current value of inscincra
     */
    public String getInscincra() {
        return inscincra;
    }

    /**
     * Setter method for inscincra.
     *
     * @param aInscincra the new value for inscincra
     */
    public void setInscincra(String aInscincra) {
        inscincra = aInscincra;
    }

    /**
     * Access method for insccafir.
     *
     * @return the current value of insccafir
     */
    public String getInsccafir() {
        return insccafir;
    }

    /**
     * Setter method for insccafir.
     *
     * @param aInsccafir the new value for insccafir
     */
    public void setInsccafir(String aInsccafir) {
        insccafir = aInsccafir;
    }

    /**
     * Access method for locquadra.
     *
     * @return the current value of locquadra
     */
    public String getLocquadra() {
        return locquadra;
    }

    /**
     * Setter method for locquadra.
     *
     * @param aLocquadra the new value for locquadra
     */
    public void setLocquadra(String aLocquadra) {
        locquadra = aLocquadra;
    }

    /**
     * Access method for loclote.
     *
     * @return the current value of loclote
     */
    public String getLoclote() {
        return loclote;
    }

    /**
     * Setter method for loclote.
     *
     * @param aLoclote the new value for loclote
     */
    public void setLoclote(String aLoclote) {
        loclote = aLoclote;
    }

    /**
     * Access method for locfracaoideal.
     *
     * @return the current value of locfracaoideal
     */
    public BigDecimal getLocfracaoideal() {
        return locfracaoideal;
    }

    /**
     * Setter method for locfracaoideal.
     *
     * @param aLocfracaoideal the new value for locfracaoideal
     */
    public void setLocfracaoideal(BigDecimal aLocfracaoideal) {
        locfracaoideal = aLocfracaoideal;
    }

    /**
     * Access method for locaream2.
     *
     * @return the current value of locaream2
     */
    public BigDecimal getLocaream2() {
        return locaream2;
    }

    /**
     * Setter method for locaream2.
     *
     * @param aLocaream2 the new value for locaream2
     */
    public void setLocaream2(BigDecimal aLocaream2) {
        locaream2 = aLocaream2;
    }

    /**
     * Access method for locareaalqueire.
     *
     * @return the current value of locareaalqueire
     */
    public BigDecimal getLocareaalqueire() {
        return locareaalqueire;
    }

    /**
     * Setter method for locareaalqueire.
     *
     * @param aLocareaalqueire the new value for locareaalqueire
     */
    public void setLocareaalqueire(BigDecimal aLocareaalqueire) {
        locareaalqueire = aLocareaalqueire;
    }

    /**
     * Access method for locareaedtotal.
     *
     * @return the current value of locareaedtotal
     */
    public BigDecimal getLocareaedtotal() {
        return locareaedtotal;
    }

    /**
     * Setter method for locareaedtotal.
     *
     * @param aLocareaedtotal the new value for locareaedtotal
     */
    public void setLocareaedtotal(BigDecimal aLocareaedtotal) {
        locareaedtotal = aLocareaedtotal;
    }

    /**
     * Access method for locareaedutil.
     *
     * @return the current value of locareaedutil
     */
    public BigDecimal getLocareaedutil() {
        return locareaedutil;
    }

    /**
     * Setter method for locareaedutil.
     *
     * @param aLocareaedutil the new value for locareaedutil
     */
    public void setLocareaedutil(BigDecimal aLocareaedutil) {
        locareaedutil = aLocareaedutil;
    }

    /**
     * Access method for valoraquisicao.
     *
     * @return the current value of valoraquisicao
     */
    public BigDecimal getValoraquisicao() {
        return valoraquisicao;
    }

    /**
     * Setter method for valoraquisicao.
     *
     * @param aValoraquisicao the new value for valoraquisicao
     */
    public void setValoraquisicao(BigDecimal aValoraquisicao) {
        valoraquisicao = aValoraquisicao;
    }

    /**
     * Access method for valorcontabil.
     *
     * @return the current value of valorcontabil
     */
    public BigDecimal getValorcontabil() {
        return valorcontabil;
    }

    /**
     * Setter method for valorcontabil.
     *
     * @param aValorcontabil the new value for valorcontabil
     */
    public void setValorcontabil(BigDecimal aValorcontabil) {
        valorcontabil = aValorcontabil;
    }

    /**
     * Access method for valorvenal.
     *
     * @return the current value of valorvenal
     */
    public BigDecimal getValorvenal() {
        return valorvenal;
    }

    /**
     * Setter method for valorvenal.
     *
     * @param aValorvenal the new value for valorvenal
     */
    public void setValorvenal(BigDecimal aValorvenal) {
        valorvenal = aValorvenal;
    }

    /**
     * Access method for valormercado.
     *
     * @return the current value of valormercado
     */
    public BigDecimal getValormercado() {
        return valormercado;
    }

    /**
     * Setter method for valormercado.
     *
     * @param aValormercado the new value for valormercado
     */
    public void setValormercado(BigDecimal aValormercado) {
        valormercado = aValormercado;
    }

    /**
     * Access method for ctbbemimoapolice.
     *
     * @return the current value of ctbbemimoapolice
     */
    public Set<Ctbbemimoapolice> getCtbbemimoapolice() {
        return ctbbemimoapolice;
    }

    /**
     * Setter method for ctbbemimoapolice.
     *
     * @param aCtbbemimoapolice the new value for ctbbemimoapolice
     */
    public void setCtbbemimoapolice(Set<Ctbbemimoapolice> aCtbbemimoapolice) {
        ctbbemimoapolice = aCtbbemimoapolice;
    }

    /**
     * Access method for conflogradouro.
     *
     * @return the current value of conflogradouro
     */
    public Conflogradouro getConflogradouro() {
        return conflogradouro;
    }

    /**
     * Setter method for conflogradouro.
     *
     * @param aConflogradouro the new value for conflogradouro
     */
    public void setConflogradouro(Conflogradouro aConflogradouro) {
        conflogradouro = aConflogradouro;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for gercircunscricao.
     *
     * @return the current value of gercircunscricao
     */
    public Gercircunscricao getGercircunscricao() {
        return gercircunscricao;
    }

    /**
     * Setter method for gercircunscricao.
     *
     * @param aGercircunscricao the new value for gercircunscricao
     */
    public void setGercircunscricao(Gercircunscricao aGercircunscricao) {
        gercircunscricao = aGercircunscricao;
    }

    /**
     * Access method for gerimovelcarac.
     *
     * @return the current value of gerimovelcarac
     */
    public Set<Gerimovelcarac> getGerimovelcarac() {
        return gerimovelcarac;
    }

    /**
     * Setter method for gerimovelcarac.
     *
     * @param aGerimovelcarac the new value for gerimovelcarac
     */
    public void setGerimovelcarac(Set<Gerimovelcarac> aGerimovelcarac) {
        gerimovelcarac = aGerimovelcarac;
    }

    /**
     * Compares the key for this instance with another Gerimovel.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Gerimovel and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Gerimovel)) {
            return false;
        }
        Gerimovel that = (Gerimovel) other;
        if (this.getCodgerimovel() != that.getCodgerimovel()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Gerimovel.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gerimovel)) return false;
        return this.equalKeys(other) && ((Gerimovel)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgerimovel();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Gerimovel |");
        sb.append(" codgerimovel=").append(getCodgerimovel());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgerimovel", Integer.valueOf(getCodgerimovel()));
        return ret;
    }

}
