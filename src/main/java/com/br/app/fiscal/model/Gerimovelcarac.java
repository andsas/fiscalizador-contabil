package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="GERIMOVELCARAC")
public class Gerimovelcarac implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgerimovelcarac";

    @Id
    @Column(name="CODGERIMOVELCARAC", unique=true, nullable=false, precision=10)
    private int codgerimovelcarac;
    @Column(name="VALOR", nullable=false, length=250)
    private String valor;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGERIMOVEL", nullable=false)
    private Gerimovel gerimovel;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODMODELOCARAC", nullable=false)
    private Prodmodelocarac prodmodelocarac;

    /** Default constructor. */
    public Gerimovelcarac() {
        super();
    }

    /**
     * Access method for codgerimovelcarac.
     *
     * @return the current value of codgerimovelcarac
     */
    public int getCodgerimovelcarac() {
        return codgerimovelcarac;
    }

    /**
     * Setter method for codgerimovelcarac.
     *
     * @param aCodgerimovelcarac the new value for codgerimovelcarac
     */
    public void setCodgerimovelcarac(int aCodgerimovelcarac) {
        codgerimovelcarac = aCodgerimovelcarac;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(String aValor) {
        valor = aValor;
    }

    /**
     * Access method for gerimovel.
     *
     * @return the current value of gerimovel
     */
    public Gerimovel getGerimovel() {
        return gerimovel;
    }

    /**
     * Setter method for gerimovel.
     *
     * @param aGerimovel the new value for gerimovel
     */
    public void setGerimovel(Gerimovel aGerimovel) {
        gerimovel = aGerimovel;
    }

    /**
     * Access method for prodmodelocarac.
     *
     * @return the current value of prodmodelocarac
     */
    public Prodmodelocarac getProdmodelocarac() {
        return prodmodelocarac;
    }

    /**
     * Setter method for prodmodelocarac.
     *
     * @param aProdmodelocarac the new value for prodmodelocarac
     */
    public void setProdmodelocarac(Prodmodelocarac aProdmodelocarac) {
        prodmodelocarac = aProdmodelocarac;
    }

    /**
     * Compares the key for this instance with another Gerimovelcarac.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Gerimovelcarac and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Gerimovelcarac)) {
            return false;
        }
        Gerimovelcarac that = (Gerimovelcarac) other;
        if (this.getCodgerimovelcarac() != that.getCodgerimovelcarac()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Gerimovelcarac.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gerimovelcarac)) return false;
        return this.equalKeys(other) && ((Gerimovelcarac)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgerimovelcarac();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Gerimovelcarac |");
        sb.append(" codgerimovelcarac=").append(getCodgerimovelcarac());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgerimovelcarac", Integer.valueOf(getCodgerimovelcarac()));
        return ret;
    }

}
