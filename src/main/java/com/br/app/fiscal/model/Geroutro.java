package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="GEROUTRO")
public class Geroutro implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgeroutro";

    @Id
    @Column(name="CODGEROUTRO", unique=true, nullable=false, precision=10)
    private int codgeroutro;
    @Column(name="DESCGEROUTRO", nullable=false, length=250)
    private String descgeroutro;
    @Column(name="NUMSERIE", nullable=false, length=60)
    private String numserie;
    @OneToMany(mappedBy="geroutro")
    private Set<Ctbbemoutroapolice> ctbbemoutroapolice;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGERTIPOOUTRO", nullable=false)
    private Geroutrotipo geroutrotipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODMARCA", nullable=false)
    private Prodmarca prodmarca;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODMODELO", nullable=false)
    private Prodmodelo prodmodelo;
    @ManyToOne
    @JoinColumn(name="CODPRODPRODUTO")
    private Prodproduto prodproduto;
    @OneToMany(mappedBy="geroutro")
    private Set<Geroutrocarac> geroutrocarac;

    /** Default constructor. */
    public Geroutro() {
        super();
    }

    /**
     * Access method for codgeroutro.
     *
     * @return the current value of codgeroutro
     */
    public int getCodgeroutro() {
        return codgeroutro;
    }

    /**
     * Setter method for codgeroutro.
     *
     * @param aCodgeroutro the new value for codgeroutro
     */
    public void setCodgeroutro(int aCodgeroutro) {
        codgeroutro = aCodgeroutro;
    }

    /**
     * Access method for descgeroutro.
     *
     * @return the current value of descgeroutro
     */
    public String getDescgeroutro() {
        return descgeroutro;
    }

    /**
     * Setter method for descgeroutro.
     *
     * @param aDescgeroutro the new value for descgeroutro
     */
    public void setDescgeroutro(String aDescgeroutro) {
        descgeroutro = aDescgeroutro;
    }

    /**
     * Access method for numserie.
     *
     * @return the current value of numserie
     */
    public String getNumserie() {
        return numserie;
    }

    /**
     * Setter method for numserie.
     *
     * @param aNumserie the new value for numserie
     */
    public void setNumserie(String aNumserie) {
        numserie = aNumserie;
    }

    /**
     * Access method for ctbbemoutroapolice.
     *
     * @return the current value of ctbbemoutroapolice
     */
    public Set<Ctbbemoutroapolice> getCtbbemoutroapolice() {
        return ctbbemoutroapolice;
    }

    /**
     * Setter method for ctbbemoutroapolice.
     *
     * @param aCtbbemoutroapolice the new value for ctbbemoutroapolice
     */
    public void setCtbbemoutroapolice(Set<Ctbbemoutroapolice> aCtbbemoutroapolice) {
        ctbbemoutroapolice = aCtbbemoutroapolice;
    }

    /**
     * Access method for geroutrotipo.
     *
     * @return the current value of geroutrotipo
     */
    public Geroutrotipo getGeroutrotipo() {
        return geroutrotipo;
    }

    /**
     * Setter method for geroutrotipo.
     *
     * @param aGeroutrotipo the new value for geroutrotipo
     */
    public void setGeroutrotipo(Geroutrotipo aGeroutrotipo) {
        geroutrotipo = aGeroutrotipo;
    }

    /**
     * Access method for prodmarca.
     *
     * @return the current value of prodmarca
     */
    public Prodmarca getProdmarca() {
        return prodmarca;
    }

    /**
     * Setter method for prodmarca.
     *
     * @param aProdmarca the new value for prodmarca
     */
    public void setProdmarca(Prodmarca aProdmarca) {
        prodmarca = aProdmarca;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Prodmodelo getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Prodmodelo aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for geroutrocarac.
     *
     * @return the current value of geroutrocarac
     */
    public Set<Geroutrocarac> getGeroutrocarac() {
        return geroutrocarac;
    }

    /**
     * Setter method for geroutrocarac.
     *
     * @param aGeroutrocarac the new value for geroutrocarac
     */
    public void setGeroutrocarac(Set<Geroutrocarac> aGeroutrocarac) {
        geroutrocarac = aGeroutrocarac;
    }

    /**
     * Compares the key for this instance with another Geroutro.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Geroutro and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Geroutro)) {
            return false;
        }
        Geroutro that = (Geroutro) other;
        if (this.getCodgeroutro() != that.getCodgeroutro()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Geroutro.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Geroutro)) return false;
        return this.equalKeys(other) && ((Geroutro)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgeroutro();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Geroutro |");
        sb.append(" codgeroutro=").append(getCodgeroutro());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgeroutro", Integer.valueOf(getCodgeroutro()));
        return ret;
    }

}
