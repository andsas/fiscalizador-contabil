package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="GEROUTROCARAC")
public class Geroutrocarac implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgeroutrocarac";

    @Id
    @Column(name="CODGEROUTROCARAC", unique=true, nullable=false, precision=10)
    private int codgeroutrocarac;
    @Column(name="VALOR", precision=10)
    private int valor;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGEROUTRO", nullable=false)
    private Geroutro geroutro;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODMODELOCARAC", nullable=false)
    private Prodmodelocarac prodmodelocarac;

    /** Default constructor. */
    public Geroutrocarac() {
        super();
    }

    /**
     * Access method for codgeroutrocarac.
     *
     * @return the current value of codgeroutrocarac
     */
    public int getCodgeroutrocarac() {
        return codgeroutrocarac;
    }

    /**
     * Setter method for codgeroutrocarac.
     *
     * @param aCodgeroutrocarac the new value for codgeroutrocarac
     */
    public void setCodgeroutrocarac(int aCodgeroutrocarac) {
        codgeroutrocarac = aCodgeroutrocarac;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public int getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(int aValor) {
        valor = aValor;
    }

    /**
     * Access method for geroutro.
     *
     * @return the current value of geroutro
     */
    public Geroutro getGeroutro() {
        return geroutro;
    }

    /**
     * Setter method for geroutro.
     *
     * @param aGeroutro the new value for geroutro
     */
    public void setGeroutro(Geroutro aGeroutro) {
        geroutro = aGeroutro;
    }

    /**
     * Access method for prodmodelocarac.
     *
     * @return the current value of prodmodelocarac
     */
    public Prodmodelocarac getProdmodelocarac() {
        return prodmodelocarac;
    }

    /**
     * Setter method for prodmodelocarac.
     *
     * @param aProdmodelocarac the new value for prodmodelocarac
     */
    public void setProdmodelocarac(Prodmodelocarac aProdmodelocarac) {
        prodmodelocarac = aProdmodelocarac;
    }

    /**
     * Compares the key for this instance with another Geroutrocarac.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Geroutrocarac and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Geroutrocarac)) {
            return false;
        }
        Geroutrocarac that = (Geroutrocarac) other;
        if (this.getCodgeroutrocarac() != that.getCodgeroutrocarac()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Geroutrocarac.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Geroutrocarac)) return false;
        return this.equalKeys(other) && ((Geroutrocarac)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgeroutrocarac();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Geroutrocarac |");
        sb.append(" codgeroutrocarac=").append(getCodgeroutrocarac());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgeroutrocarac", Integer.valueOf(getCodgeroutrocarac()));
        return ret;
    }

}
