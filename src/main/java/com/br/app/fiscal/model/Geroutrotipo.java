package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="GEROUTROTIPO")
public class Geroutrotipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgeroutrotipo";

    @Id
    @Column(name="CODGEROUTROTIPO", unique=true, nullable=false, precision=10)
    private int codgeroutrotipo;
    @Column(name="DESCGEROUTROTIPO", nullable=false, length=250)
    private String descgeroutrotipo;
    @OneToMany(mappedBy="geroutrotipo")
    private Set<Geroutro> geroutro;

    /** Default constructor. */
    public Geroutrotipo() {
        super();
    }

    /**
     * Access method for codgeroutrotipo.
     *
     * @return the current value of codgeroutrotipo
     */
    public int getCodgeroutrotipo() {
        return codgeroutrotipo;
    }

    /**
     * Setter method for codgeroutrotipo.
     *
     * @param aCodgeroutrotipo the new value for codgeroutrotipo
     */
    public void setCodgeroutrotipo(int aCodgeroutrotipo) {
        codgeroutrotipo = aCodgeroutrotipo;
    }

    /**
     * Access method for descgeroutrotipo.
     *
     * @return the current value of descgeroutrotipo
     */
    public String getDescgeroutrotipo() {
        return descgeroutrotipo;
    }

    /**
     * Setter method for descgeroutrotipo.
     *
     * @param aDescgeroutrotipo the new value for descgeroutrotipo
     */
    public void setDescgeroutrotipo(String aDescgeroutrotipo) {
        descgeroutrotipo = aDescgeroutrotipo;
    }

    /**
     * Access method for geroutro.
     *
     * @return the current value of geroutro
     */
    public Set<Geroutro> getGeroutro() {
        return geroutro;
    }

    /**
     * Setter method for geroutro.
     *
     * @param aGeroutro the new value for geroutro
     */
    public void setGeroutro(Set<Geroutro> aGeroutro) {
        geroutro = aGeroutro;
    }

    /**
     * Compares the key for this instance with another Geroutrotipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Geroutrotipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Geroutrotipo)) {
            return false;
        }
        Geroutrotipo that = (Geroutrotipo) other;
        if (this.getCodgeroutrotipo() != that.getCodgeroutrotipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Geroutrotipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Geroutrotipo)) return false;
        return this.equalKeys(other) && ((Geroutrotipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgeroutrotipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Geroutrotipo |");
        sb.append(" codgeroutrotipo=").append(getCodgeroutrotipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgeroutrotipo", Integer.valueOf(getCodgeroutrotipo()));
        return ret;
    }

}
