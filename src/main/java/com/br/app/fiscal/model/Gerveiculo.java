package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="GERVEICULO")
public class Gerveiculo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgerveiculo";

    @Id
    @Column(name="CODGERVEICULO", unique=true, nullable=false, precision=10)
    private int codgerveiculo;
    @Column(name="DESCGERVEICULO", nullable=false, length=250)
    private String descgerveiculo;
    @Column(name="TIPOGERVEICULO", nullable=false, precision=5)
    private short tipogerveiculo;
    @Column(name="RODANOFABR", nullable=false, precision=10)
    private int rodanofabr;
    @Column(name="RODANOMOD", nullable=false, precision=10)
    private int rodanomod;
    @Column(name="RODRENAVAM", length=20)
    private String rodrenavam;
    @Column(name="RODIPVAULT", precision=15, scale=4)
    private BigDecimal rodipvault;
    @Column(name="RODIPVAVENC", precision=5)
    private short rodipvavenc;
    @Column(name="RODTIPOPNEUSDIA", length=20)
    private String rodtipopneusdia;
    @Column(name="RODTIPOPNEUSTRA", length=20)
    private String rodtipopneustra;
    @Column(name="RODTIPOPNEUSEIXO", length=20)
    private String rodtipopneuseixo;
    @Column(name="RODPLACA", length=20)
    private String rodplaca;
    @Column(name="RODKM", precision=10)
    private int rodkm;
    @Column(name="RODFINANCIADO", precision=5)
    private short rodfinanciado;
    @OneToMany(mappedBy="gerveiculo")
    private Set<Ctbbemveicapolice> ctbbemveicapolice;
    @OneToMany(mappedBy="gerveiculo")
    private Set<Ctbbemveicinfracao> ctbbemveicinfracao;
    @OneToMany(mappedBy="gerveiculo")
    private Set<Ctbbemveictacografo> ctbbemveictacografo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="RODCODMODELO")
    private Prodmodelo prodmodelo;
    @ManyToOne
    @JoinColumn(name="CODPRODPRODUTO")
    private Prodproduto prodproduto;
    @OneToMany(mappedBy="gerveiculo")
    private Set<Gerveiculocarac> gerveiculocarac;
    @OneToMany(mappedBy="gerveiculo")
    private Set<Optransacao> optransacao;
    @OneToMany(mappedBy="gerveiculo")
    private Set<Parklancamento> parklancamento;

    /** Default constructor. */
    public Gerveiculo() {
        super();
    }

    /**
     * Access method for codgerveiculo.
     *
     * @return the current value of codgerveiculo
     */
    public int getCodgerveiculo() {
        return codgerveiculo;
    }

    /**
     * Setter method for codgerveiculo.
     *
     * @param aCodgerveiculo the new value for codgerveiculo
     */
    public void setCodgerveiculo(int aCodgerveiculo) {
        codgerveiculo = aCodgerveiculo;
    }

    /**
     * Access method for descgerveiculo.
     *
     * @return the current value of descgerveiculo
     */
    public String getDescgerveiculo() {
        return descgerveiculo;
    }

    /**
     * Setter method for descgerveiculo.
     *
     * @param aDescgerveiculo the new value for descgerveiculo
     */
    public void setDescgerveiculo(String aDescgerveiculo) {
        descgerveiculo = aDescgerveiculo;
    }

    /**
     * Access method for tipogerveiculo.
     *
     * @return the current value of tipogerveiculo
     */
    public short getTipogerveiculo() {
        return tipogerveiculo;
    }

    /**
     * Setter method for tipogerveiculo.
     *
     * @param aTipogerveiculo the new value for tipogerveiculo
     */
    public void setTipogerveiculo(short aTipogerveiculo) {
        tipogerveiculo = aTipogerveiculo;
    }

    /**
     * Access method for rodanofabr.
     *
     * @return the current value of rodanofabr
     */
    public int getRodanofabr() {
        return rodanofabr;
    }

    /**
     * Setter method for rodanofabr.
     *
     * @param aRodanofabr the new value for rodanofabr
     */
    public void setRodanofabr(int aRodanofabr) {
        rodanofabr = aRodanofabr;
    }

    /**
     * Access method for rodanomod.
     *
     * @return the current value of rodanomod
     */
    public int getRodanomod() {
        return rodanomod;
    }

    /**
     * Setter method for rodanomod.
     *
     * @param aRodanomod the new value for rodanomod
     */
    public void setRodanomod(int aRodanomod) {
        rodanomod = aRodanomod;
    }

    /**
     * Access method for rodrenavam.
     *
     * @return the current value of rodrenavam
     */
    public String getRodrenavam() {
        return rodrenavam;
    }

    /**
     * Setter method for rodrenavam.
     *
     * @param aRodrenavam the new value for rodrenavam
     */
    public void setRodrenavam(String aRodrenavam) {
        rodrenavam = aRodrenavam;
    }

    /**
     * Access method for rodipvault.
     *
     * @return the current value of rodipvault
     */
    public BigDecimal getRodipvault() {
        return rodipvault;
    }

    /**
     * Setter method for rodipvault.
     *
     * @param aRodipvault the new value for rodipvault
     */
    public void setRodipvault(BigDecimal aRodipvault) {
        rodipvault = aRodipvault;
    }

    /**
     * Access method for rodipvavenc.
     *
     * @return the current value of rodipvavenc
     */
    public short getRodipvavenc() {
        return rodipvavenc;
    }

    /**
     * Setter method for rodipvavenc.
     *
     * @param aRodipvavenc the new value for rodipvavenc
     */
    public void setRodipvavenc(short aRodipvavenc) {
        rodipvavenc = aRodipvavenc;
    }

    /**
     * Access method for rodtipopneusdia.
     *
     * @return the current value of rodtipopneusdia
     */
    public String getRodtipopneusdia() {
        return rodtipopneusdia;
    }

    /**
     * Setter method for rodtipopneusdia.
     *
     * @param aRodtipopneusdia the new value for rodtipopneusdia
     */
    public void setRodtipopneusdia(String aRodtipopneusdia) {
        rodtipopneusdia = aRodtipopneusdia;
    }

    /**
     * Access method for rodtipopneustra.
     *
     * @return the current value of rodtipopneustra
     */
    public String getRodtipopneustra() {
        return rodtipopneustra;
    }

    /**
     * Setter method for rodtipopneustra.
     *
     * @param aRodtipopneustra the new value for rodtipopneustra
     */
    public void setRodtipopneustra(String aRodtipopneustra) {
        rodtipopneustra = aRodtipopneustra;
    }

    /**
     * Access method for rodtipopneuseixo.
     *
     * @return the current value of rodtipopneuseixo
     */
    public String getRodtipopneuseixo() {
        return rodtipopneuseixo;
    }

    /**
     * Setter method for rodtipopneuseixo.
     *
     * @param aRodtipopneuseixo the new value for rodtipopneuseixo
     */
    public void setRodtipopneuseixo(String aRodtipopneuseixo) {
        rodtipopneuseixo = aRodtipopneuseixo;
    }

    /**
     * Access method for rodplaca.
     *
     * @return the current value of rodplaca
     */
    public String getRodplaca() {
        return rodplaca;
    }

    /**
     * Setter method for rodplaca.
     *
     * @param aRodplaca the new value for rodplaca
     */
    public void setRodplaca(String aRodplaca) {
        rodplaca = aRodplaca;
    }

    /**
     * Access method for rodkm.
     *
     * @return the current value of rodkm
     */
    public int getRodkm() {
        return rodkm;
    }

    /**
     * Setter method for rodkm.
     *
     * @param aRodkm the new value for rodkm
     */
    public void setRodkm(int aRodkm) {
        rodkm = aRodkm;
    }

    /**
     * Access method for rodfinanciado.
     *
     * @return the current value of rodfinanciado
     */
    public short getRodfinanciado() {
        return rodfinanciado;
    }

    /**
     * Setter method for rodfinanciado.
     *
     * @param aRodfinanciado the new value for rodfinanciado
     */
    public void setRodfinanciado(short aRodfinanciado) {
        rodfinanciado = aRodfinanciado;
    }

    /**
     * Access method for ctbbemveicapolice.
     *
     * @return the current value of ctbbemveicapolice
     */
    public Set<Ctbbemveicapolice> getCtbbemveicapolice() {
        return ctbbemveicapolice;
    }

    /**
     * Setter method for ctbbemveicapolice.
     *
     * @param aCtbbemveicapolice the new value for ctbbemveicapolice
     */
    public void setCtbbemveicapolice(Set<Ctbbemveicapolice> aCtbbemveicapolice) {
        ctbbemveicapolice = aCtbbemveicapolice;
    }

    /**
     * Access method for ctbbemveicinfracao.
     *
     * @return the current value of ctbbemveicinfracao
     */
    public Set<Ctbbemveicinfracao> getCtbbemveicinfracao() {
        return ctbbemveicinfracao;
    }

    /**
     * Setter method for ctbbemveicinfracao.
     *
     * @param aCtbbemveicinfracao the new value for ctbbemveicinfracao
     */
    public void setCtbbemveicinfracao(Set<Ctbbemveicinfracao> aCtbbemveicinfracao) {
        ctbbemveicinfracao = aCtbbemveicinfracao;
    }

    /**
     * Access method for ctbbemveictacografo.
     *
     * @return the current value of ctbbemveictacografo
     */
    public Set<Ctbbemveictacografo> getCtbbemveictacografo() {
        return ctbbemveictacografo;
    }

    /**
     * Setter method for ctbbemveictacografo.
     *
     * @param aCtbbemveictacografo the new value for ctbbemveictacografo
     */
    public void setCtbbemveictacografo(Set<Ctbbemveictacografo> aCtbbemveictacografo) {
        ctbbemveictacografo = aCtbbemveictacografo;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Prodmodelo getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Prodmodelo aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for gerveiculocarac.
     *
     * @return the current value of gerveiculocarac
     */
    public Set<Gerveiculocarac> getGerveiculocarac() {
        return gerveiculocarac;
    }

    /**
     * Setter method for gerveiculocarac.
     *
     * @param aGerveiculocarac the new value for gerveiculocarac
     */
    public void setGerveiculocarac(Set<Gerveiculocarac> aGerveiculocarac) {
        gerveiculocarac = aGerveiculocarac;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for parklancamento.
     *
     * @return the current value of parklancamento
     */
    public Set<Parklancamento> getParklancamento() {
        return parklancamento;
    }

    /**
     * Setter method for parklancamento.
     *
     * @param aParklancamento the new value for parklancamento
     */
    public void setParklancamento(Set<Parklancamento> aParklancamento) {
        parklancamento = aParklancamento;
    }

    /**
     * Compares the key for this instance with another Gerveiculo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Gerveiculo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Gerveiculo)) {
            return false;
        }
        Gerveiculo that = (Gerveiculo) other;
        if (this.getCodgerveiculo() != that.getCodgerveiculo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Gerveiculo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gerveiculo)) return false;
        return this.equalKeys(other) && ((Gerveiculo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgerveiculo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Gerveiculo |");
        sb.append(" codgerveiculo=").append(getCodgerveiculo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgerveiculo", Integer.valueOf(getCodgerveiculo()));
        return ret;
    }

}
