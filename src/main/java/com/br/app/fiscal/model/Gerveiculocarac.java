package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="GERVEICULOCARAC")
public class Gerveiculocarac implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codgerveiculocarac";

    @Id
    @Column(name="CODGERVEICULOCARAC", unique=true, nullable=false, precision=10)
    private int codgerveiculocarac;
    @Column(name="VALOR", nullable=false, length=250)
    private String valor;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODGERVEICULO", nullable=false)
    private Gerveiculo gerveiculo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODMODELOCARAC", nullable=false)
    private Prodmodelocarac prodmodelocarac;

    /** Default constructor. */
    public Gerveiculocarac() {
        super();
    }

    /**
     * Access method for codgerveiculocarac.
     *
     * @return the current value of codgerveiculocarac
     */
    public int getCodgerveiculocarac() {
        return codgerveiculocarac;
    }

    /**
     * Setter method for codgerveiculocarac.
     *
     * @param aCodgerveiculocarac the new value for codgerveiculocarac
     */
    public void setCodgerveiculocarac(int aCodgerveiculocarac) {
        codgerveiculocarac = aCodgerveiculocarac;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(String aValor) {
        valor = aValor;
    }

    /**
     * Access method for gerveiculo.
     *
     * @return the current value of gerveiculo
     */
    public Gerveiculo getGerveiculo() {
        return gerveiculo;
    }

    /**
     * Setter method for gerveiculo.
     *
     * @param aGerveiculo the new value for gerveiculo
     */
    public void setGerveiculo(Gerveiculo aGerveiculo) {
        gerveiculo = aGerveiculo;
    }

    /**
     * Access method for prodmodelocarac.
     *
     * @return the current value of prodmodelocarac
     */
    public Prodmodelocarac getProdmodelocarac() {
        return prodmodelocarac;
    }

    /**
     * Setter method for prodmodelocarac.
     *
     * @param aProdmodelocarac the new value for prodmodelocarac
     */
    public void setProdmodelocarac(Prodmodelocarac aProdmodelocarac) {
        prodmodelocarac = aProdmodelocarac;
    }

    /**
     * Compares the key for this instance with another Gerveiculocarac.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Gerveiculocarac and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Gerveiculocarac)) {
            return false;
        }
        Gerveiculocarac that = (Gerveiculocarac) other;
        if (this.getCodgerveiculocarac() != that.getCodgerveiculocarac()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Gerveiculocarac.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Gerveiculocarac)) return false;
        return this.equalKeys(other) && ((Gerveiculocarac)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodgerveiculocarac();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Gerveiculocarac |");
        sb.append(" codgerveiculocarac=").append(getCodgerveiculocarac());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codgerveiculocarac", Integer.valueOf(getCodgerveiculocarac()));
        return ret;
    }

}
