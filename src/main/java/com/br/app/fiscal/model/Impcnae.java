package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="IMPCNAE")
public class Impcnae implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpcnae";

    @Id
    @Column(name="CODIMPCNAE", unique=true, nullable=false, precision=10)
    private int codimpcnae;
    @Column(name="DENOMINACAO", length=250)
    private String denominacao;
    @Column(name="SECAO", length=4)
    private String secao;
    @Column(name="DIVISAO", length=4)
    private String divisao;
    @Column(name="GRUPO", length=4)
    private String grupo;
    @Column(name="CLASSE", length=20)
    private String classe;
    @Column(name="SUBCLASSE", length=20)
    private String subclasse;
    @OneToMany(mappedBy="impcnae")
    private Set<Agagenteimpcnae> agagenteimpcnae;
    @OneToMany(mappedBy="impcnae")
    private Set<Folfpas> folfpas;
    @OneToMany(mappedBy="impcnae")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="impcnae")
    private Set<Prodproduto> prodproduto;

    /** Default constructor. */
    public Impcnae() {
        super();
    }

    /**
     * Access method for codimpcnae.
     *
     * @return the current value of codimpcnae
     */
    public int getCodimpcnae() {
        return codimpcnae;
    }

    /**
     * Setter method for codimpcnae.
     *
     * @param aCodimpcnae the new value for codimpcnae
     */
    public void setCodimpcnae(int aCodimpcnae) {
        codimpcnae = aCodimpcnae;
    }

    /**
     * Access method for denominacao.
     *
     * @return the current value of denominacao
     */
    public String getDenominacao() {
        return denominacao;
    }

    /**
     * Setter method for denominacao.
     *
     * @param aDenominacao the new value for denominacao
     */
    public void setDenominacao(String aDenominacao) {
        denominacao = aDenominacao;
    }

    /**
     * Access method for secao.
     *
     * @return the current value of secao
     */
    public String getSecao() {
        return secao;
    }

    /**
     * Setter method for secao.
     *
     * @param aSecao the new value for secao
     */
    public void setSecao(String aSecao) {
        secao = aSecao;
    }

    /**
     * Access method for divisao.
     *
     * @return the current value of divisao
     */
    public String getDivisao() {
        return divisao;
    }

    /**
     * Setter method for divisao.
     *
     * @param aDivisao the new value for divisao
     */
    public void setDivisao(String aDivisao) {
        divisao = aDivisao;
    }

    /**
     * Access method for grupo.
     *
     * @return the current value of grupo
     */
    public String getGrupo() {
        return grupo;
    }

    /**
     * Setter method for grupo.
     *
     * @param aGrupo the new value for grupo
     */
    public void setGrupo(String aGrupo) {
        grupo = aGrupo;
    }

    /**
     * Access method for classe.
     *
     * @return the current value of classe
     */
    public String getClasse() {
        return classe;
    }

    /**
     * Setter method for classe.
     *
     * @param aClasse the new value for classe
     */
    public void setClasse(String aClasse) {
        classe = aClasse;
    }

    /**
     * Access method for subclasse.
     *
     * @return the current value of subclasse
     */
    public String getSubclasse() {
        return subclasse;
    }

    /**
     * Setter method for subclasse.
     *
     * @param aSubclasse the new value for subclasse
     */
    public void setSubclasse(String aSubclasse) {
        subclasse = aSubclasse;
    }

    /**
     * Access method for agagenteimpcnae.
     *
     * @return the current value of agagenteimpcnae
     */
    public Set<Agagenteimpcnae> getAgagenteimpcnae() {
        return agagenteimpcnae;
    }

    /**
     * Setter method for agagenteimpcnae.
     *
     * @param aAgagenteimpcnae the new value for agagenteimpcnae
     */
    public void setAgagenteimpcnae(Set<Agagenteimpcnae> aAgagenteimpcnae) {
        agagenteimpcnae = aAgagenteimpcnae;
    }

    /**
     * Access method for folfpas.
     *
     * @return the current value of folfpas
     */
    public Set<Folfpas> getFolfpas() {
        return folfpas;
    }

    /**
     * Setter method for folfpas.
     *
     * @param aFolfpas the new value for folfpas
     */
    public void setFolfpas(Set<Folfpas> aFolfpas) {
        folfpas = aFolfpas;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Impcnae.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impcnae and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impcnae)) {
            return false;
        }
        Impcnae that = (Impcnae) other;
        if (this.getCodimpcnae() != that.getCodimpcnae()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impcnae.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impcnae)) return false;
        return this.equalKeys(other) && ((Impcnae)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpcnae();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impcnae |");
        sb.append(" codimpcnae=").append(getCodimpcnae());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpcnae", Integer.valueOf(getCodimpcnae()));
        return ret;
    }

}
