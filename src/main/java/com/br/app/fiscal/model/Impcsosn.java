package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="IMPCSOSN")
public class Impcsosn implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpcsosn";

    @Id
    @Column(name="CODIMPCSOSN", unique=true, nullable=false, precision=10)
    private int codimpcsosn;
    @Column(name="DESCCSOSN", nullable=false, length=250)
    private String desccsosn;
    @Column(name="CSOSN", nullable=false, length=4)
    private String csosn;
    @OneToMany(mappedBy="impcsosn")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="impcsosn2")
    private Set<Impplanofiscal> impplanofiscal2;
    @OneToMany(mappedBy="impcsosn3")
    private Set<Impplanofiscal> impplanofiscal3;
    @OneToMany(mappedBy="impcsosn4")
    private Set<Impplanofiscal> impplanofiscal4;

    /** Default constructor. */
    public Impcsosn() {
        super();
    }

    /**
     * Access method for codimpcsosn.
     *
     * @return the current value of codimpcsosn
     */
    public int getCodimpcsosn() {
        return codimpcsosn;
    }

    /**
     * Setter method for codimpcsosn.
     *
     * @param aCodimpcsosn the new value for codimpcsosn
     */
    public void setCodimpcsosn(int aCodimpcsosn) {
        codimpcsosn = aCodimpcsosn;
    }

    /**
     * Access method for desccsosn.
     *
     * @return the current value of desccsosn
     */
    public String getDesccsosn() {
        return desccsosn;
    }

    /**
     * Setter method for desccsosn.
     *
     * @param aDesccsosn the new value for desccsosn
     */
    public void setDesccsosn(String aDesccsosn) {
        desccsosn = aDesccsosn;
    }

    /**
     * Access method for csosn.
     *
     * @return the current value of csosn
     */
    public String getCsosn() {
        return csosn;
    }

    /**
     * Setter method for csosn.
     *
     * @param aCsosn the new value for csosn
     */
    public void setCsosn(String aCsosn) {
        csosn = aCsosn;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for impplanofiscal2.
     *
     * @return the current value of impplanofiscal2
     */
    public Set<Impplanofiscal> getImpplanofiscal2() {
        return impplanofiscal2;
    }

    /**
     * Setter method for impplanofiscal2.
     *
     * @param aImpplanofiscal2 the new value for impplanofiscal2
     */
    public void setImpplanofiscal2(Set<Impplanofiscal> aImpplanofiscal2) {
        impplanofiscal2 = aImpplanofiscal2;
    }

    /**
     * Access method for impplanofiscal3.
     *
     * @return the current value of impplanofiscal3
     */
    public Set<Impplanofiscal> getImpplanofiscal3() {
        return impplanofiscal3;
    }

    /**
     * Setter method for impplanofiscal3.
     *
     * @param aImpplanofiscal3 the new value for impplanofiscal3
     */
    public void setImpplanofiscal3(Set<Impplanofiscal> aImpplanofiscal3) {
        impplanofiscal3 = aImpplanofiscal3;
    }

    /**
     * Access method for impplanofiscal4.
     *
     * @return the current value of impplanofiscal4
     */
    public Set<Impplanofiscal> getImpplanofiscal4() {
        return impplanofiscal4;
    }

    /**
     * Setter method for impplanofiscal4.
     *
     * @param aImpplanofiscal4 the new value for impplanofiscal4
     */
    public void setImpplanofiscal4(Set<Impplanofiscal> aImpplanofiscal4) {
        impplanofiscal4 = aImpplanofiscal4;
    }

    /**
     * Compares the key for this instance with another Impcsosn.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impcsosn and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impcsosn)) {
            return false;
        }
        Impcsosn that = (Impcsosn) other;
        if (this.getCodimpcsosn() != that.getCodimpcsosn()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impcsosn.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impcsosn)) return false;
        return this.equalKeys(other) && ((Impcsosn)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpcsosn();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impcsosn |");
        sb.append(" codimpcsosn=").append(getCodimpcsosn());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpcsosn", Integer.valueOf(getCodimpcsosn()));
        return ret;
    }

}
