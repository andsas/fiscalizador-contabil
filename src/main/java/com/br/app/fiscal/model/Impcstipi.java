package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="IMPCSTIPI")
public class Impcstipi implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpcstipi";

    @Id
    @Column(name="CODIMPCSTIPI", unique=true, nullable=false, precision=10)
    private int codimpcstipi;
    @Column(name="DESCIMPCSTIPI", nullable=false, length=250)
    private String descimpcstipi;
    @Column(name="CST", nullable=false, length=4)
    private String cst;
    @OneToMany(mappedBy="impcstipi2")
    private Set<Impplanofiscal> impplanofiscal2;
    @OneToMany(mappedBy="impcstipi")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="impcstipi")
    private Set<Optransacaodet> optransacaodet;

    /** Default constructor. */
    public Impcstipi() {
        super();
    }

    /**
     * Access method for codimpcstipi.
     *
     * @return the current value of codimpcstipi
     */
    public int getCodimpcstipi() {
        return codimpcstipi;
    }

    /**
     * Setter method for codimpcstipi.
     *
     * @param aCodimpcstipi the new value for codimpcstipi
     */
    public void setCodimpcstipi(int aCodimpcstipi) {
        codimpcstipi = aCodimpcstipi;
    }

    /**
     * Access method for descimpcstipi.
     *
     * @return the current value of descimpcstipi
     */
    public String getDescimpcstipi() {
        return descimpcstipi;
    }

    /**
     * Setter method for descimpcstipi.
     *
     * @param aDescimpcstipi the new value for descimpcstipi
     */
    public void setDescimpcstipi(String aDescimpcstipi) {
        descimpcstipi = aDescimpcstipi;
    }

    /**
     * Access method for cst.
     *
     * @return the current value of cst
     */
    public String getCst() {
        return cst;
    }

    /**
     * Setter method for cst.
     *
     * @param aCst the new value for cst
     */
    public void setCst(String aCst) {
        cst = aCst;
    }

    /**
     * Access method for impplanofiscal2.
     *
     * @return the current value of impplanofiscal2
     */
    public Set<Impplanofiscal> getImpplanofiscal2() {
        return impplanofiscal2;
    }

    /**
     * Setter method for impplanofiscal2.
     *
     * @param aImpplanofiscal2 the new value for impplanofiscal2
     */
    public void setImpplanofiscal2(Set<Impplanofiscal> aImpplanofiscal2) {
        impplanofiscal2 = aImpplanofiscal2;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Compares the key for this instance with another Impcstipi.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impcstipi and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impcstipi)) {
            return false;
        }
        Impcstipi that = (Impcstipi) other;
        if (this.getCodimpcstipi() != that.getCodimpcstipi()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impcstipi.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impcstipi)) return false;
        return this.equalKeys(other) && ((Impcstipi)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpcstipi();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impcstipi |");
        sb.append(" codimpcstipi=").append(getCodimpcstipi());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpcstipi", Integer.valueOf(getCodimpcstipi()));
        return ret;
    }

}
