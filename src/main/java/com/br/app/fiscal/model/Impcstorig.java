package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="IMPCSTORIG")
public class Impcstorig implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpcstorig";

    @Id
    @Column(name="CODIMPCSTORIG", unique=true, nullable=false, precision=10)
    private int codimpcstorig;
    @Column(name="DESCIMPCSTORIG", nullable=false, length=250)
    private String descimpcstorig;
    @Column(name="CODIGOORIGEM", nullable=false, precision=5)
    private short codigoorigem;
    @OneToMany(mappedBy="impcstorig")
    private Set<Impplanofiscal> impplanofiscal;

    /** Default constructor. */
    public Impcstorig() {
        super();
    }

    /**
     * Access method for codimpcstorig.
     *
     * @return the current value of codimpcstorig
     */
    public int getCodimpcstorig() {
        return codimpcstorig;
    }

    /**
     * Setter method for codimpcstorig.
     *
     * @param aCodimpcstorig the new value for codimpcstorig
     */
    public void setCodimpcstorig(int aCodimpcstorig) {
        codimpcstorig = aCodimpcstorig;
    }

    /**
     * Access method for descimpcstorig.
     *
     * @return the current value of descimpcstorig
     */
    public String getDescimpcstorig() {
        return descimpcstorig;
    }

    /**
     * Setter method for descimpcstorig.
     *
     * @param aDescimpcstorig the new value for descimpcstorig
     */
    public void setDescimpcstorig(String aDescimpcstorig) {
        descimpcstorig = aDescimpcstorig;
    }

    /**
     * Access method for codigoorigem.
     *
     * @return the current value of codigoorigem
     */
    public short getCodigoorigem() {
        return codigoorigem;
    }

    /**
     * Setter method for codigoorigem.
     *
     * @param aCodigoorigem the new value for codigoorigem
     */
    public void setCodigoorigem(short aCodigoorigem) {
        codigoorigem = aCodigoorigem;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Compares the key for this instance with another Impcstorig.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impcstorig and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impcstorig)) {
            return false;
        }
        Impcstorig that = (Impcstorig) other;
        if (this.getCodimpcstorig() != that.getCodimpcstorig()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impcstorig.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impcstorig)) return false;
        return this.equalKeys(other) && ((Impcstorig)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpcstorig();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impcstorig |");
        sb.append(" codimpcstorig=").append(getCodimpcstorig());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpcstorig", Integer.valueOf(getCodimpcstorig()));
        return ret;
    }

}
