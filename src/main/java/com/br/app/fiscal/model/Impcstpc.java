package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="IMPCSTPC")
public class Impcstpc implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpcstpc";

    @Id
    @Column(name="CODIMPCSTPC", unique=true, nullable=false, precision=10)
    private int codimpcstpc;
    @Column(name="DESCIMPCSTPC", nullable=false, length=250)
    private String descimpcstpc;
    @Column(name="CST", nullable=false, length=4)
    private String cst;
    @OneToMany(mappedBy="impcstpc")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="impcstpc2")
    private Set<Impplanofiscal> impplanofiscal2;
    @OneToMany(mappedBy="impcstpc3")
    private Set<Impplanofiscal> impplanofiscal3;
    @OneToMany(mappedBy="impcstpc4")
    private Set<Impplanofiscal> impplanofiscal4;
    @OneToMany(mappedBy="impcstpc")
    private Set<Optransacaodet> optransacaodet;
    @OneToMany(mappedBy="impcstpc2")
    private Set<Optransacaodet> optransacaodet2;

    /** Default constructor. */
    public Impcstpc() {
        super();
    }

    /**
     * Access method for codimpcstpc.
     *
     * @return the current value of codimpcstpc
     */
    public int getCodimpcstpc() {
        return codimpcstpc;
    }

    /**
     * Setter method for codimpcstpc.
     *
     * @param aCodimpcstpc the new value for codimpcstpc
     */
    public void setCodimpcstpc(int aCodimpcstpc) {
        codimpcstpc = aCodimpcstpc;
    }

    /**
     * Access method for descimpcstpc.
     *
     * @return the current value of descimpcstpc
     */
    public String getDescimpcstpc() {
        return descimpcstpc;
    }

    /**
     * Setter method for descimpcstpc.
     *
     * @param aDescimpcstpc the new value for descimpcstpc
     */
    public void setDescimpcstpc(String aDescimpcstpc) {
        descimpcstpc = aDescimpcstpc;
    }

    /**
     * Access method for cst.
     *
     * @return the current value of cst
     */
    public String getCst() {
        return cst;
    }

    /**
     * Setter method for cst.
     *
     * @param aCst the new value for cst
     */
    public void setCst(String aCst) {
        cst = aCst;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for impplanofiscal2.
     *
     * @return the current value of impplanofiscal2
     */
    public Set<Impplanofiscal> getImpplanofiscal2() {
        return impplanofiscal2;
    }

    /**
     * Setter method for impplanofiscal2.
     *
     * @param aImpplanofiscal2 the new value for impplanofiscal2
     */
    public void setImpplanofiscal2(Set<Impplanofiscal> aImpplanofiscal2) {
        impplanofiscal2 = aImpplanofiscal2;
    }

    /**
     * Access method for impplanofiscal3.
     *
     * @return the current value of impplanofiscal3
     */
    public Set<Impplanofiscal> getImpplanofiscal3() {
        return impplanofiscal3;
    }

    /**
     * Setter method for impplanofiscal3.
     *
     * @param aImpplanofiscal3 the new value for impplanofiscal3
     */
    public void setImpplanofiscal3(Set<Impplanofiscal> aImpplanofiscal3) {
        impplanofiscal3 = aImpplanofiscal3;
    }

    /**
     * Access method for impplanofiscal4.
     *
     * @return the current value of impplanofiscal4
     */
    public Set<Impplanofiscal> getImpplanofiscal4() {
        return impplanofiscal4;
    }

    /**
     * Setter method for impplanofiscal4.
     *
     * @param aImpplanofiscal4 the new value for impplanofiscal4
     */
    public void setImpplanofiscal4(Set<Impplanofiscal> aImpplanofiscal4) {
        impplanofiscal4 = aImpplanofiscal4;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Access method for optransacaodet2.
     *
     * @return the current value of optransacaodet2
     */
    public Set<Optransacaodet> getOptransacaodet2() {
        return optransacaodet2;
    }

    /**
     * Setter method for optransacaodet2.
     *
     * @param aOptransacaodet2 the new value for optransacaodet2
     */
    public void setOptransacaodet2(Set<Optransacaodet> aOptransacaodet2) {
        optransacaodet2 = aOptransacaodet2;
    }

    /**
     * Compares the key for this instance with another Impcstpc.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impcstpc and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impcstpc)) {
            return false;
        }
        Impcstpc that = (Impcstpc) other;
        if (this.getCodimpcstpc() != that.getCodimpcstpc()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impcstpc.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impcstpc)) return false;
        return this.equalKeys(other) && ((Impcstpc)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpcstpc();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impcstpc |");
        sb.append(" codimpcstpc=").append(getCodimpcstpc());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpcstpc", Integer.valueOf(getCodimpcstpc()));
        return ret;
    }

}
