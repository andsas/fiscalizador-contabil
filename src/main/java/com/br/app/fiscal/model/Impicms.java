package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
//import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity(name="IMPICMS")
public class Impicms implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpicms";

    @Id
    @Column(name="CODIMPICMS", unique=true, nullable=false, precision=10)
    private int codimpicms;
    @Column(name="ALIQICMS", nullable=false, length=15)
    private double aliqicms;
    @ManyToOne
    @JoinColumn(name="CTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta2;
//    @OneToOne(optional=false, mappedBy="impicms")
//    @JoinColumns({
//    	@JoinColumn(name="CODCONFUFOR", nullable=false), 
//    	@JoinColumn(name="CODCONFUFDES", nullable=false)
//    })
    @OneToOne(optional=false)
    @JoinColumn(name="CODCONFUFOR", nullable=false)
    private Confuf confuf;

    /** Default constructor. */
    public Impicms() {
        super();
    }

    /**
     * Access method for codimpicms.
     *
     * @return the current value of codimpicms
     */
    public int getCodimpicms() {
        return codimpicms;
    }

    /**
     * Setter method for codimpicms.
     *
     * @param aCodimpicms the new value for codimpicms
     */
    public void setCodimpicms(int aCodimpicms) {
        codimpicms = aCodimpicms;
    }

    /**
     * Access method for aliqicms.
     *
     * @return the current value of aliqicms
     */
    public double getAliqicms() {
        return aliqicms;
    }

    /**
     * Setter method for aliqicms.
     *
     * @param aAliqicms the new value for aliqicms
     */
    public void setAliqicms(double aAliqicms) {
        aliqicms = aAliqicms;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for confuf.
     *
     * @return the current value of confuf
     */
    public Confuf getConfuf() {
        return confuf;
    }

    /**
     * Setter method for confuf.
     *
     * @param aConfuf the new value for confuf
     */
    public void setConfuf(Confuf aConfuf) {
        confuf = aConfuf;
    }

    /**
     * Compares the key for this instance with another Impicms.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impicms and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impicms)) {
            return false;
        }
        Impicms that = (Impicms) other;
        if (this.getCodimpicms() != that.getCodimpicms()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impicms.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impicms)) return false;
        return this.equalKeys(other) && ((Impicms)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpicms();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impicms |");
        sb.append(" codimpicms=").append(getCodimpicms());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpicms", Integer.valueOf(getCodimpicms()));
        return ret;
    }

}
