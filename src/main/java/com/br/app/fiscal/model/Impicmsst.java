package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="IMPICMSST")
public class Impicmsst implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpicmsst";

    @Id
    @Column(name="CODIMPICMSST", unique=true, nullable=false, precision=10)
    private int codimpicmsst;
    @Column(name="ALIQMVA", length=15)
    private double aliqmva;
    @Column(name="ALIQICMS", length=15)
    private double aliqicms;
    @Column(name="ALIQREDICMSST", length=15)
    private double aliqredicmsst;
    @Column(name="ALIQREDMVASIMPLES", length=15)
    private double aliqredmvasimples;
    @Column(name="ALIQREDUTORICMS", precision=5)
    private short aliqredutoricms;
    @Column(name="ALIQREDUZIDA", length=15)
    private double aliqreduzida;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODIMPIPI", nullable=false)
    private Impipi impipi;
    @ManyToOne
    @JoinColumn(name="CODCONFUF")
    private Confuf confuf;

    /** Default constructor. */
    public Impicmsst() {
        super();
    }

    /**
     * Access method for codimpicmsst.
     *
     * @return the current value of codimpicmsst
     */
    public int getCodimpicmsst() {
        return codimpicmsst;
    }

    /**
     * Setter method for codimpicmsst.
     *
     * @param aCodimpicmsst the new value for codimpicmsst
     */
    public void setCodimpicmsst(int aCodimpicmsst) {
        codimpicmsst = aCodimpicmsst;
    }

    /**
     * Access method for aliqmva.
     *
     * @return the current value of aliqmva
     */
    public double getAliqmva() {
        return aliqmva;
    }

    /**
     * Setter method for aliqmva.
     *
     * @param aAliqmva the new value for aliqmva
     */
    public void setAliqmva(double aAliqmva) {
        aliqmva = aAliqmva;
    }

    /**
     * Access method for aliqicms.
     *
     * @return the current value of aliqicms
     */
    public double getAliqicms() {
        return aliqicms;
    }

    /**
     * Setter method for aliqicms.
     *
     * @param aAliqicms the new value for aliqicms
     */
    public void setAliqicms(double aAliqicms) {
        aliqicms = aAliqicms;
    }

    /**
     * Access method for aliqredicmsst.
     *
     * @return the current value of aliqredicmsst
     */
    public double getAliqredicmsst() {
        return aliqredicmsst;
    }

    /**
     * Setter method for aliqredicmsst.
     *
     * @param aAliqredicmsst the new value for aliqredicmsst
     */
    public void setAliqredicmsst(double aAliqredicmsst) {
        aliqredicmsst = aAliqredicmsst;
    }

    /**
     * Access method for aliqredmvasimples.
     *
     * @return the current value of aliqredmvasimples
     */
    public double getAliqredmvasimples() {
        return aliqredmvasimples;
    }

    /**
     * Setter method for aliqredmvasimples.
     *
     * @param aAliqredmvasimples the new value for aliqredmvasimples
     */
    public void setAliqredmvasimples(double aAliqredmvasimples) {
        aliqredmvasimples = aAliqredmvasimples;
    }

    /**
     * Access method for aliqredutoricms.
     *
     * @return the current value of aliqredutoricms
     */
    public short getAliqredutoricms() {
        return aliqredutoricms;
    }

    /**
     * Setter method for aliqredutoricms.
     *
     * @param aAliqredutoricms the new value for aliqredutoricms
     */
    public void setAliqredutoricms(short aAliqredutoricms) {
        aliqredutoricms = aAliqredutoricms;
    }

    /**
     * Access method for aliqreduzida.
     *
     * @return the current value of aliqreduzida
     */
    public double getAliqreduzida() {
        return aliqreduzida;
    }

    /**
     * Setter method for aliqreduzida.
     *
     * @param aAliqreduzida the new value for aliqreduzida
     */
    public void setAliqreduzida(double aAliqreduzida) {
        aliqreduzida = aAliqreduzida;
    }

    /**
     * Access method for impipi.
     *
     * @return the current value of impipi
     */
    public Impipi getImpipi() {
        return impipi;
    }

    /**
     * Setter method for impipi.
     *
     * @param aImpipi the new value for impipi
     */
    public void setImpipi(Impipi aImpipi) {
        impipi = aImpipi;
    }

    /**
     * Access method for confuf.
     *
     * @return the current value of confuf
     */
    public Confuf getConfuf() {
        return confuf;
    }

    /**
     * Setter method for confuf.
     *
     * @param aConfuf the new value for confuf
     */
    public void setConfuf(Confuf aConfuf) {
        confuf = aConfuf;
    }

    /**
     * Compares the key for this instance with another Impicmsst.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impicmsst and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impicmsst)) {
            return false;
        }
        Impicmsst that = (Impicmsst) other;
        if (this.getCodimpicmsst() != that.getCodimpicmsst()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impicmsst.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impicmsst)) return false;
        return this.equalKeys(other) && ((Impicmsst)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpicmsst();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impicmsst |");
        sb.append(" codimpicmsst=").append(getCodimpicmsst());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpicmsst", Integer.valueOf(getCodimpicmsst()));
        return ret;
    }

}
