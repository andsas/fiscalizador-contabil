package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="IMPIPI")
public class Impipi implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpipi";

    @Id
    @Column(name="CODIMPIPI", unique=true, nullable=false, precision=10)
    private int codimpipi;
    @Column(name="EXCECAO", length=4)
    private String excecao;
    @Column(name="CODTABIBPT", precision=10)
    private int codtabibpt;
    @Column(name="ALIQNACIMPIPI", nullable=false, length=15)
    private double aliqnacimpipi;
    @Column(name="ALIQINTIMPIPI", nullable=false, length=15)
    private double aliqintimpipi;
    @Column(name="ALIQPISIMP", length=15)
    private double aliqpisimp;
    @Column(name="ALIQCOFINSIMP", length=15)
    private double aliqcofinsimp;
    @Column(name="ALIQIMPIPI", precision=15, scale=4)
    private BigDecimal aliqimpipi;
    @OneToMany(mappedBy="impipi")
    private Set<Impicmsst> impicmsst;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODNCM", nullable=false)
    private Prodncm prodncm;
    @ManyToOne
    @JoinColumn(name="CTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta2;

    /** Default constructor. */
    public Impipi() {
        super();
    }

    /**
     * Access method for codimpipi.
     *
     * @return the current value of codimpipi
     */
    public int getCodimpipi() {
        return codimpipi;
    }

    /**
     * Setter method for codimpipi.
     *
     * @param aCodimpipi the new value for codimpipi
     */
    public void setCodimpipi(int aCodimpipi) {
        codimpipi = aCodimpipi;
    }

    /**
     * Access method for excecao.
     *
     * @return the current value of excecao
     */
    public String getExcecao() {
        return excecao;
    }

    /**
     * Setter method for excecao.
     *
     * @param aExcecao the new value for excecao
     */
    public void setExcecao(String aExcecao) {
        excecao = aExcecao;
    }

    /**
     * Access method for codtabibpt.
     *
     * @return the current value of codtabibpt
     */
    public int getCodtabibpt() {
        return codtabibpt;
    }

    /**
     * Setter method for codtabibpt.
     *
     * @param aCodtabibpt the new value for codtabibpt
     */
    public void setCodtabibpt(int aCodtabibpt) {
        codtabibpt = aCodtabibpt;
    }

    /**
     * Access method for aliqnacimpipi.
     *
     * @return the current value of aliqnacimpipi
     */
    public double getAliqnacimpipi() {
        return aliqnacimpipi;
    }

    /**
     * Setter method for aliqnacimpipi.
     *
     * @param aAliqnacimpipi the new value for aliqnacimpipi
     */
    public void setAliqnacimpipi(double aAliqnacimpipi) {
        aliqnacimpipi = aAliqnacimpipi;
    }

    /**
     * Access method for aliqintimpipi.
     *
     * @return the current value of aliqintimpipi
     */
    public double getAliqintimpipi() {
        return aliqintimpipi;
    }

    /**
     * Setter method for aliqintimpipi.
     *
     * @param aAliqintimpipi the new value for aliqintimpipi
     */
    public void setAliqintimpipi(double aAliqintimpipi) {
        aliqintimpipi = aAliqintimpipi;
    }

    /**
     * Access method for aliqpisimp.
     *
     * @return the current value of aliqpisimp
     */
    public double getAliqpisimp() {
        return aliqpisimp;
    }

    /**
     * Setter method for aliqpisimp.
     *
     * @param aAliqpisimp the new value for aliqpisimp
     */
    public void setAliqpisimp(double aAliqpisimp) {
        aliqpisimp = aAliqpisimp;
    }

    /**
     * Access method for aliqcofinsimp.
     *
     * @return the current value of aliqcofinsimp
     */
    public double getAliqcofinsimp() {
        return aliqcofinsimp;
    }

    /**
     * Setter method for aliqcofinsimp.
     *
     * @param aAliqcofinsimp the new value for aliqcofinsimp
     */
    public void setAliqcofinsimp(double aAliqcofinsimp) {
        aliqcofinsimp = aAliqcofinsimp;
    }

    /**
     * Access method for aliqimpipi.
     *
     * @return the current value of aliqimpipi
     */
    public BigDecimal getAliqimpipi() {
        return aliqimpipi;
    }

    /**
     * Setter method for aliqimpipi.
     *
     * @param aAliqimpipi the new value for aliqimpipi
     */
    public void setAliqimpipi(BigDecimal aAliqimpipi) {
        aliqimpipi = aAliqimpipi;
    }

    /**
     * Access method for impicmsst.
     *
     * @return the current value of impicmsst
     */
    public Set<Impicmsst> getImpicmsst() {
        return impicmsst;
    }

    /**
     * Setter method for impicmsst.
     *
     * @param aImpicmsst the new value for impicmsst
     */
    public void setImpicmsst(Set<Impicmsst> aImpicmsst) {
        impicmsst = aImpicmsst;
    }

    /**
     * Access method for prodncm.
     *
     * @return the current value of prodncm
     */
    public Prodncm getProdncm() {
        return prodncm;
    }

    /**
     * Setter method for prodncm.
     *
     * @param aProdncm the new value for prodncm
     */
    public void setProdncm(Prodncm aProdncm) {
        prodncm = aProdncm;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Compares the key for this instance with another Impipi.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impipi and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impipi)) {
            return false;
        }
        Impipi that = (Impipi) other;
        if (this.getCodimpipi() != that.getCodimpipi()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impipi.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impipi)) return false;
        return this.equalKeys(other) && ((Impipi)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpipi();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impipi |");
        sb.append(" codimpipi=").append(getCodimpipi());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpipi", Integer.valueOf(getCodimpipi()));
        return ret;
    }

}
