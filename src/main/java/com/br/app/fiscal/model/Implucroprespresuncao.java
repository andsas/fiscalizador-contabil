package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="IMPLUCROPRESPRESUNCAO")
public class Implucroprespresuncao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimplucroprespresuncao";

    @Id
    @Column(name="CODIMPLUCROPRESPRESUNCAO", unique=true, nullable=false, precision=10)
    private int codimplucroprespresuncao;
    @Column(name="DESCIMPLUCROPRESPRESUNCAO", nullable=false, length=250)
    private String descimplucroprespresuncao;
    @Column(name="ALIQUOTA", nullable=false, length=15)
    private double aliquota;
    @Column(name="RECEITABRTATE", precision=15, scale=4)
    private BigDecimal receitabrtate;

    /** Default constructor. */
    public Implucroprespresuncao() {
        super();
    }

    /**
     * Access method for codimplucroprespresuncao.
     *
     * @return the current value of codimplucroprespresuncao
     */
    public int getCodimplucroprespresuncao() {
        return codimplucroprespresuncao;
    }

    /**
     * Setter method for codimplucroprespresuncao.
     *
     * @param aCodimplucroprespresuncao the new value for codimplucroprespresuncao
     */
    public void setCodimplucroprespresuncao(int aCodimplucroprespresuncao) {
        codimplucroprespresuncao = aCodimplucroprespresuncao;
    }

    /**
     * Access method for descimplucroprespresuncao.
     *
     * @return the current value of descimplucroprespresuncao
     */
    public String getDescimplucroprespresuncao() {
        return descimplucroprespresuncao;
    }

    /**
     * Setter method for descimplucroprespresuncao.
     *
     * @param aDescimplucroprespresuncao the new value for descimplucroprespresuncao
     */
    public void setDescimplucroprespresuncao(String aDescimplucroprespresuncao) {
        descimplucroprespresuncao = aDescimplucroprespresuncao;
    }

    /**
     * Access method for aliquota.
     *
     * @return the current value of aliquota
     */
    public double getAliquota() {
        return aliquota;
    }

    /**
     * Setter method for aliquota.
     *
     * @param aAliquota the new value for aliquota
     */
    public void setAliquota(double aAliquota) {
        aliquota = aAliquota;
    }

    /**
     * Access method for receitabrtate.
     *
     * @return the current value of receitabrtate
     */
    public BigDecimal getReceitabrtate() {
        return receitabrtate;
    }

    /**
     * Setter method for receitabrtate.
     *
     * @param aReceitabrtate the new value for receitabrtate
     */
    public void setReceitabrtate(BigDecimal aReceitabrtate) {
        receitabrtate = aReceitabrtate;
    }

    /**
     * Compares the key for this instance with another Implucroprespresuncao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Implucroprespresuncao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Implucroprespresuncao)) {
            return false;
        }
        Implucroprespresuncao that = (Implucroprespresuncao) other;
        if (this.getCodimplucroprespresuncao() != that.getCodimplucroprespresuncao()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Implucroprespresuncao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Implucroprespresuncao)) return false;
        return this.equalKeys(other) && ((Implucroprespresuncao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimplucroprespresuncao();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Implucroprespresuncao |");
        sb.append(" codimplucroprespresuncao=").append(getCodimplucroprespresuncao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimplucroprespresuncao", Integer.valueOf(getCodimplucroprespresuncao()));
        return ret;
    }

}
