package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="IMPOBS")
public class Impobs implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpobs";

    @Id
    @Column(name="CODIMPOBS", unique=true, nullable=false, precision=10)
    private int codimpobs;
    @Column(name="OBS", nullable=false)
    private String obs;
    @OneToMany(mappedBy="impobs")
    private Set<Opdocmodelo> opdocmodelo;
    @OneToMany(mappedBy="impobs")
    private Set<Optipo> optipo;
    @OneToMany(mappedBy="impobs")
    private Set<Optransacao> optransacao;
    @OneToMany(mappedBy="impobs2")
    private Set<Optransacao> optransacao2;
    @OneToMany(mappedBy="impobs3")
    private Set<Optransacao> optransacao3;
    @OneToMany(mappedBy="impobs")
    private Set<Optransacaodet> optransacaodet;

    /** Default constructor. */
    public Impobs() {
        super();
    }

    /**
     * Access method for codimpobs.
     *
     * @return the current value of codimpobs
     */
    public int getCodimpobs() {
        return codimpobs;
    }

    /**
     * Setter method for codimpobs.
     *
     * @param aCodimpobs the new value for codimpobs
     */
    public void setCodimpobs(int aCodimpobs) {
        codimpobs = aCodimpobs;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for opdocmodelo.
     *
     * @return the current value of opdocmodelo
     */
    public Set<Opdocmodelo> getOpdocmodelo() {
        return opdocmodelo;
    }

    /**
     * Setter method for opdocmodelo.
     *
     * @param aOpdocmodelo the new value for opdocmodelo
     */
    public void setOpdocmodelo(Set<Opdocmodelo> aOpdocmodelo) {
        opdocmodelo = aOpdocmodelo;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Set<Optipo> getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Set<Optipo> aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for optransacao2.
     *
     * @return the current value of optransacao2
     */
    public Set<Optransacao> getOptransacao2() {
        return optransacao2;
    }

    /**
     * Setter method for optransacao2.
     *
     * @param aOptransacao2 the new value for optransacao2
     */
    public void setOptransacao2(Set<Optransacao> aOptransacao2) {
        optransacao2 = aOptransacao2;
    }

    /**
     * Access method for optransacao3.
     *
     * @return the current value of optransacao3
     */
    public Set<Optransacao> getOptransacao3() {
        return optransacao3;
    }

    /**
     * Setter method for optransacao3.
     *
     * @param aOptransacao3 the new value for optransacao3
     */
    public void setOptransacao3(Set<Optransacao> aOptransacao3) {
        optransacao3 = aOptransacao3;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Compares the key for this instance with another Impobs.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impobs and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impobs)) {
            return false;
        }
        Impobs that = (Impobs) other;
        if (this.getCodimpobs() != that.getCodimpobs()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impobs.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impobs)) return false;
        return this.equalKeys(other) && ((Impobs)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpobs();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impobs |");
        sb.append(" codimpobs=").append(getCodimpobs());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpobs", Integer.valueOf(getCodimpobs()));
        return ret;
    }

}
