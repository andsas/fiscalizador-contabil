package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="IMPOPTRIBUTARIA")
public class Impoptributaria implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpoptributaria";

    @Id
    @Column(name="CODIMPOPTRIBUTARIA", unique=true, nullable=false, precision=10)
    private int codimpoptributaria;
    @Column(name="TIPOIMPOPTRIBUTARIA", nullable=false, precision=5)
    private short tipoimpoptributaria;
    @Column(name="ALIQPIS", precision=15, scale=4)
    private BigDecimal aliqpis;
    @Column(name="ALIQCOFINS", precision=15, scale=4)
    private BigDecimal aliqcofins;
    @Column(name="ALIQCSLL", precision=15, scale=4)
    private BigDecimal aliqcsll;
    @Column(name="ALIQIRPJ", precision=15, scale=4)
    private BigDecimal aliqirpj;
    @Column(name="ALIQADIRPJ", precision=15, scale=4)
    private BigDecimal aliqadirpj;
    @Column(name="OPCAORECOLHIMENTO", nullable=false, precision=5)
    private short opcaorecolhimento;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;

    /** Default constructor. */
    public Impoptributaria() {
        super();
    }

    /**
     * Access method for codimpoptributaria.
     *
     * @return the current value of codimpoptributaria
     */
    public int getCodimpoptributaria() {
        return codimpoptributaria;
    }

    /**
     * Setter method for codimpoptributaria.
     *
     * @param aCodimpoptributaria the new value for codimpoptributaria
     */
    public void setCodimpoptributaria(int aCodimpoptributaria) {
        codimpoptributaria = aCodimpoptributaria;
    }

    /**
     * Access method for tipoimpoptributaria.
     *
     * @return the current value of tipoimpoptributaria
     */
    public short getTipoimpoptributaria() {
        return tipoimpoptributaria;
    }

    /**
     * Setter method for tipoimpoptributaria.
     *
     * @param aTipoimpoptributaria the new value for tipoimpoptributaria
     */
    public void setTipoimpoptributaria(short aTipoimpoptributaria) {
        tipoimpoptributaria = aTipoimpoptributaria;
    }

    /**
     * Access method for aliqpis.
     *
     * @return the current value of aliqpis
     */
    public BigDecimal getAliqpis() {
        return aliqpis;
    }

    /**
     * Setter method for aliqpis.
     *
     * @param aAliqpis the new value for aliqpis
     */
    public void setAliqpis(BigDecimal aAliqpis) {
        aliqpis = aAliqpis;
    }

    /**
     * Access method for aliqcofins.
     *
     * @return the current value of aliqcofins
     */
    public BigDecimal getAliqcofins() {
        return aliqcofins;
    }

    /**
     * Setter method for aliqcofins.
     *
     * @param aAliqcofins the new value for aliqcofins
     */
    public void setAliqcofins(BigDecimal aAliqcofins) {
        aliqcofins = aAliqcofins;
    }

    /**
     * Access method for aliqcsll.
     *
     * @return the current value of aliqcsll
     */
    public BigDecimal getAliqcsll() {
        return aliqcsll;
    }

    /**
     * Setter method for aliqcsll.
     *
     * @param aAliqcsll the new value for aliqcsll
     */
    public void setAliqcsll(BigDecimal aAliqcsll) {
        aliqcsll = aAliqcsll;
    }

    /**
     * Access method for aliqirpj.
     *
     * @return the current value of aliqirpj
     */
    public BigDecimal getAliqirpj() {
        return aliqirpj;
    }

    /**
     * Setter method for aliqirpj.
     *
     * @param aAliqirpj the new value for aliqirpj
     */
    public void setAliqirpj(BigDecimal aAliqirpj) {
        aliqirpj = aAliqirpj;
    }

    /**
     * Access method for aliqadirpj.
     *
     * @return the current value of aliqadirpj
     */
    public BigDecimal getAliqadirpj() {
        return aliqadirpj;
    }

    /**
     * Setter method for aliqadirpj.
     *
     * @param aAliqadirpj the new value for aliqadirpj
     */
    public void setAliqadirpj(BigDecimal aAliqadirpj) {
        aliqadirpj = aAliqadirpj;
    }

    /**
     * Access method for opcaorecolhimento.
     *
     * @return the current value of opcaorecolhimento
     */
    public short getOpcaorecolhimento() {
        return opcaorecolhimento;
    }

    /**
     * Setter method for opcaorecolhimento.
     *
     * @param aOpcaorecolhimento the new value for opcaorecolhimento
     */
    public void setOpcaorecolhimento(short aOpcaorecolhimento) {
        opcaorecolhimento = aOpcaorecolhimento;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Compares the key for this instance with another Impoptributaria.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impoptributaria and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impoptributaria)) {
            return false;
        }
        Impoptributaria that = (Impoptributaria) other;
        if (this.getCodimpoptributaria() != that.getCodimpoptributaria()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impoptributaria.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impoptributaria)) return false;
        return this.equalKeys(other) && ((Impoptributaria)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpoptributaria();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impoptributaria |");
        sb.append(" codimpoptributaria=").append(getCodimpoptributaria());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpoptributaria", Integer.valueOf(getCodimpoptributaria()));
        return ret;
    }

}
