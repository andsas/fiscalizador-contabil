package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="IMPPLANOFISCAL")
public class Impplanofiscal implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpplanofiscal";

    @Id
    @Column(name="CODIMPPLANOFISCAL", unique=true, nullable=false, precision=10)
    private int codimpplanofiscal;
    @Column(name="DESCIMPPLANOFISCAL", nullable=false, length=250)
    private String descimpplanofiscal;
    @Column(name="CODAGTIPO", precision=10)
    private int codagtipo;
    @Column(name="CODAGGRUPO", length=20)
    private String codaggrupo;
    @Column(name="CODAGAGENTE", precision=10)
    private int codagagente;
    @Column(name="FEDIPIDENTROCALCULA", precision=5)
    private short fedipidentrocalcula;
    @Column(name="FEDIPIFORACALCULA", precision=5)
    private short fedipiforacalcula;
    @Column(name="ESTICMSDENTROCALCULA", precision=5)
    private short esticmsdentrocalcula;
    @Column(name="ESTICMSFORACALCULA", precision=5)
    private short esticmsforacalcula;
    @Column(name="ESTMVACALCULA", precision=5)
    private short estmvacalcula;
    @Column(name="ESTMVADENTRO", precision=15, scale=4)
    private BigDecimal estmvadentro;
    @Column(name="ESTMVAFORA", precision=15, scale=4)
    private BigDecimal estmvafora;
    @Column(name="ESTMVADIFALIQUOTA", precision=5)
    private short estmvadifaliquota;
    @Column(name="ESTIPVACALCULA", precision=5)
    private short estipvacalcula;
    @Column(name="ESTSTBASECALCFORA", precision=15, scale=4)
    private BigDecimal eststbasecalcfora;
    @Column(name="ESTSTBASECALCDENTRO", precision=15, scale=4)
    private BigDecimal eststbasecalcdentro;
    @Column(name="ESTSTPERCFORA", precision=15, scale=4)
    private BigDecimal eststpercfora;
    @Column(name="ESTSTPERCDENTRO", precision=15, scale=4)
    private BigDecimal eststpercdentro;
    @Column(name="MUNIPTUCALCULA", precision=5)
    private short muniptucalcula;
    @Column(name="MUNITBICALCULA", precision=5)
    private short munitbicalcula;
    @Column(name="MUNISSCALCULA", precision=5)
    private short munisscalcula;
    @Column(name="MUNISSPERC", precision=15, scale=4)
    private BigDecimal munissperc;
    @Column(name="FEDPISDENTROPERC", precision=15, scale=4)
    private BigDecimal fedpisdentroperc;
    @Column(name="FEDPISDENTROBASECALC", precision=15, scale=4)
    private BigDecimal fedpisdentrobasecalc;
    @Column(name="FEDCOFINSDENTROPERC", precision=15, scale=4)
    private BigDecimal fedcofinsdentroperc;
    @Column(name="FEDCOFINSDENTROBASECALC", precision=15, scale=4)
    private BigDecimal fedcofinsdentrobasecalc;
    @Column(name="FEDPISFORAPERC", precision=15, scale=4)
    private BigDecimal fedpisforaperc;
    @Column(name="FEDPISFORABASECALC", precision=15, scale=4)
    private BigDecimal fedpisforabasecalc;
    @Column(name="FEDCOFINSFORAPERC", precision=15, scale=4)
    private BigDecimal fedcofinsforaperc;
    @Column(name="FEDCOFINSFORABASECALC", precision=15, scale=4)
    private BigDecimal fedcofinsforabasecalc;
    @Column(name="ESTSTCALCULAFORA", precision=5)
    private short eststcalculafora;
    @Column(name="ESTSTCALCULADENTRO", precision=5)
    private short eststcalculadentro;
    @Column(name="OPCAOAGENTECONSFINAL", precision=5)
    private short opcaoagenteconsfinal;
    @Column(name="OPCAOAGENTECOMERCIO", precision=5)
    private short opcaoagentecomercio;
    @Column(name="OPCAOAGENTESUBSTTRIB", precision=5)
    private short opcaoagentesubsttrib;
    @Column(name="FEDPISDENTROCALCULA", precision=5)
    private short fedpisdentrocalcula;
    @Column(name="FEDCOFINSDENTROCALCULA", precision=5)
    private short fedcofinsdentrocalcula;
    @Column(name="FEDPISFORACALCULA", precision=5)
    private short fedpisforacalcula;
    @Column(name="FEDCOFINSFORACALCULA", precision=5)
    private short fedcofinsforacalcula;
    @Column(name="ESTICMSALIQDENTRO", precision=15, scale=4)
    private BigDecimal esticmsaliqdentro;
    @Column(name="ESTICMSALIQFORA", precision=15, scale=4)
    private BigDecimal esticmsaliqfora;
    @Column(name="ESTICMSREDBASECALCDENTRO", precision=15, scale=4)
    private BigDecimal esticmsredbasecalcdentro;
    @Column(name="ESTICMSREDBASECALCFORA", precision=15, scale=4)
    private BigDecimal esticmsredbasecalcfora;
    @Column(name="MUNEXIGIBILIDADEISS", precision=5)
    private short munexigibilidadeiss;
    @Column(name="ESTIPVAALIQ", precision=15, scale=4)
    private BigDecimal estipvaaliq;
    @Column(name="ESTIPVADESCAVISTA", precision=15, scale=4)
    private BigDecimal estipvadescavista;
    @Column(name="ESTIPVAGERAANUAL", precision=5)
    private short estipvageraanual;
    @Column(name="ESTIPVAMESPRIMPARC", precision=10)
    private int estipvamesprimparc;
    @Column(name="ESTIPVAPARCELAS", precision=10)
    private int estipvaparcelas;
    @Column(name="ESTIPVADPVAT", precision=5)
    private short estipvadpvat;
    @Column(name="ESTIPVAVALORDPVAT", precision=15, scale=4)
    private BigDecimal estipvavalordpvat;
    @Column(name="ESTIPVALIC", precision=5)
    private short estipvalic;
    @Column(name="ESTIPVAVALORLIC", precision=15, scale=4)
    private BigDecimal estipvavalorlic;
    @Column(name="MUNIPTUALIQ", precision=15, scale=4)
    private BigDecimal muniptualiq;
    @Column(name="MUNIPTUDESCAVISTA", precision=15, scale=4)
    private BigDecimal muniptudescavista;
    @Column(name="MUNIPTUGERAANUAL", precision=5)
    private short muniptugeraanual;
    @Column(name="MUNIPTUMESPRIMPARC", precision=10)
    private int muniptumesprimparc;
    @Column(name="MUNIPTUPARCELAS", precision=10)
    private int muniptuparcelas;
    @ManyToOne
    @JoinColumn(name="CODCONFEMPR")
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="FEDCODIPICSTFORA")
    private Impcstipi impcstipi2;
    @ManyToOne
    @JoinColumn(name="ESTCODICMSCSTDENTRO")
    private Impcst impcst;
    @ManyToOne
    @JoinColumn(name="ESTCODICMSCSTFORA")
    private Impcst impcst2;
    @ManyToOne
    @JoinColumn(name="FEDPISDENTROCST")
    private Impcstpc impcstpc;
    @ManyToOne
    @JoinColumn(name="FEDPISFORACST")
    private Impcstpc impcstpc2;
    @ManyToOne
    @JoinColumn(name="FEDCOFINSDENTROCST")
    private Impcstpc impcstpc3;
    @ManyToOne
    @JoinColumn(name="FEDCOFINSFORACST")
    private Impcstpc impcstpc4;
    @ManyToOne
    @JoinColumn(name="CODIMPCNAE")
    private Impcnae impcnae;
    @ManyToOne
    @JoinColumn(name="ESTSTCSTFORA")
    private Impcst impcst3;
    @ManyToOne
    @JoinColumn(name="ESTSTCSTDENTRO")
    private Impcst impcst4;
    @ManyToOne
    @JoinColumn(name="CODCONFUF")
    private Confuf confuf;
    @ManyToOne
    @JoinColumn(name="CODOPTIPO")
    private Optipo optipo;
    @ManyToOne
    @JoinColumn(name="CODOPOPERACAO")
    private Opoperacao opoperacao;
    @ManyToOne
    @JoinColumn(name="ESTCODICMSCSOSNDENTRO")
    private Impcsosn impcsosn;
    @ManyToOne
    @JoinColumn(name="ESTCODICMSCSOSNFORA")
    private Impcsosn impcsosn2;
    @ManyToOne
    @JoinColumn(name="ESTSTCSOSNDENTRO")
    private Impcsosn impcsosn3;
    @ManyToOne
    @JoinColumn(name="ESTSTCSOSNFORA")
    private Impcsosn impcsosn4;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODIMPCSTORIG", nullable=false)
    private Impcstorig impcstorig;
    @ManyToOne
    @JoinColumn(name="ICMSCONTADEBITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="ICMSCONTACREDITO")
    private Ctbplanoconta ctbplanoconta2;
    @ManyToOne
    @JoinColumn(name="IPICONTACREDITO")
    private Ctbplanoconta ctbplanoconta6;
    @ManyToOne
    @JoinColumn(name="CODCONFCIDADE")
    private Confcidade confcidade;
    @ManyToOne
    @JoinColumn(name="IPICONTADEBITO")
    private Ctbplanoconta ctbplanoconta5;
    @ManyToOne
    @JoinColumn(name="STCONTACREDITO")
    private Ctbplanoconta ctbplanoconta4;
    @ManyToOne
    @JoinColumn(name="STCONTADEBITO")
    private Ctbplanoconta ctbplanoconta3;
    @ManyToOne
    @JoinColumn(name="PISCONTACREDITO")
    private Ctbplanoconta ctbplanoconta8;
    @ManyToOne
    @JoinColumn(name="PISCONTADEBITO")
    private Ctbplanoconta ctbplanoconta7;
    @ManyToOne
    @JoinColumn(name="COFINSCONTACREDITO")
    private Ctbplanoconta ctbplanoconta10;
    @ManyToOne
    @JoinColumn(name="COFINSCONTADEBITO")
    private Ctbplanoconta ctbplanoconta9;
    @ManyToOne
    @JoinColumn(name="MVACONTACREDITO")
    private Ctbplanoconta ctbplanoconta12;
    @ManyToOne
    @JoinColumn(name="MVACONTADEBITO")
    private Ctbplanoconta ctbplanoconta11;
    @ManyToOne
    @JoinColumn(name="IPTUCONTACREDITO")
    private Ctbplanoconta ctbplanoconta14;
    @ManyToOne
    @JoinColumn(name="CODPRODGRUPO")
    private Prodgrupo prodgrupo;
    @ManyToOne
    @JoinColumn(name="IPTUCONTADEBITO")
    private Ctbplanoconta ctbplanoconta13;
    @ManyToOne
    @JoinColumn(name="ITBICONTACREDITO")
    private Ctbplanoconta ctbplanoconta16;
    @ManyToOne
    @JoinColumn(name="ITBICONTADEBITO")
    private Ctbplanoconta ctbplanoconta15;
    @ManyToOne
    @JoinColumn(name="CTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta17;
    @ManyToOne
    @JoinColumn(name="CTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta18;
    @ManyToOne
    @JoinColumn(name="FINCONTACREDITO")
    private Ctbplanoconta ctbplanoconta19;
    @ManyToOne
    @JoinColumn(name="FINCONTADEBITO")
    private Finplanoconta finplanoconta;
    @ManyToOne
    @JoinColumn(name="FLUXOCONTACREDITO")
    private Finfluxoplanoconta finfluxoplanoconta;
    @ManyToOne
    @JoinColumn(name="FLUXOCONTADEBITO")
    private Finfluxoplanoconta finfluxoplanoconta2;
    @ManyToOne
    @JoinColumn(name="ORCCONTACREDITO")
    private Orcplanoconta orcplanoconta;
    @ManyToOne
    @JoinColumn(name="CODPRODNCM")
    private Prodncm prodncm;
    @ManyToOne
    @JoinColumn(name="ORCCONTADEBITO")
    private Orcplanoconta orcplanoconta2;
    @ManyToOne
    @JoinColumn(name="CTBITEMCONTACREDITO")
    private Ctbplanoconta ctbplanoconta20;
    @ManyToOne
    @JoinColumn(name="CTBITEMCONTADEBITO")
    private Ctbplanoconta ctbplanoconta21;
    @ManyToOne
    @JoinColumn(name="FRETECONTACREDITO")
    private Ctbplanoconta ctbplanoconta22;
    @ManyToOne
    @JoinColumn(name="FRETECONTADEBITO")
    private Ctbplanoconta ctbplanoconta23;
    @ManyToOne
    @JoinColumn(name="ISSCONTACREDITO")
    private Ctbplanoconta ctbplanoconta24;
    @ManyToOne
    @JoinColumn(name="ISSCONTADEBITO")
    private Ctbplanoconta ctbplanoconta25;
    @ManyToOne
    @JoinColumn(name="DESCONTOCONTACREDITO")
    private Ctbplanoconta ctbplanoconta26;
    @ManyToOne
    @JoinColumn(name="DESCONTOCONTADEBITO")
    private Ctbplanoconta ctbplanoconta27;
    @ManyToOne
    @JoinColumn(name="OUTRASCONTACREDITO")
    private Ctbplanoconta ctbplanoconta28;
    @ManyToOne
    @JoinColumn(name="CODPRODMARCA")
    private Prodmarca prodmarca;
    @ManyToOne
    @JoinColumn(name="OUTRASCONTADEBITO")
    private Ctbplanoconta ctbplanoconta29;
    @ManyToOne
    @JoinColumn(name="CSLLCONTADEBITO")
    private Ctbplanoconta ctbplanoconta30;
    @ManyToOne
    @JoinColumn(name="CSLLCONTACREDITO")
    private Ctbplanoconta ctbplanoconta31;
    @ManyToOne
    @JoinColumn(name="CODPRODESPECIE")
    private Prodespecie prodespecie;
    @ManyToOne
    @JoinColumn(name="CODPRODPRODUTO")
    private Prodproduto prodproduto;
    @ManyToOne
    @JoinColumn(name="FEDCODIPICSTDENTRO")
    private Impcstipi impcstipi;
    @OneToMany(mappedBy="impplanofiscal")
    private Set<Optransacaodet> optransacaodet;

    /** Default constructor. */
    public Impplanofiscal() {
        super();
    }

    /**
     * Access method for codimpplanofiscal.
     *
     * @return the current value of codimpplanofiscal
     */
    public int getCodimpplanofiscal() {
        return codimpplanofiscal;
    }

    /**
     * Setter method for codimpplanofiscal.
     *
     * @param aCodimpplanofiscal the new value for codimpplanofiscal
     */
    public void setCodimpplanofiscal(int aCodimpplanofiscal) {
        codimpplanofiscal = aCodimpplanofiscal;
    }

    /**
     * Access method for descimpplanofiscal.
     *
     * @return the current value of descimpplanofiscal
     */
    public String getDescimpplanofiscal() {
        return descimpplanofiscal;
    }

    /**
     * Setter method for descimpplanofiscal.
     *
     * @param aDescimpplanofiscal the new value for descimpplanofiscal
     */
    public void setDescimpplanofiscal(String aDescimpplanofiscal) {
        descimpplanofiscal = aDescimpplanofiscal;
    }

    /**
     * Access method for codagtipo.
     *
     * @return the current value of codagtipo
     */
    public int getCodagtipo() {
        return codagtipo;
    }

    /**
     * Setter method for codagtipo.
     *
     * @param aCodagtipo the new value for codagtipo
     */
    public void setCodagtipo(int aCodagtipo) {
        codagtipo = aCodagtipo;
    }

    /**
     * Access method for codaggrupo.
     *
     * @return the current value of codaggrupo
     */
    public String getCodaggrupo() {
        return codaggrupo;
    }

    /**
     * Setter method for codaggrupo.
     *
     * @param aCodaggrupo the new value for codaggrupo
     */
    public void setCodaggrupo(String aCodaggrupo) {
        codaggrupo = aCodaggrupo;
    }

    /**
     * Access method for codagagente.
     *
     * @return the current value of codagagente
     */
    public int getCodagagente() {
        return codagagente;
    }

    /**
     * Setter method for codagagente.
     *
     * @param aCodagagente the new value for codagagente
     */
    public void setCodagagente(int aCodagagente) {
        codagagente = aCodagagente;
    }

    /**
     * Access method for fedipidentrocalcula.
     *
     * @return the current value of fedipidentrocalcula
     */
    public short getFedipidentrocalcula() {
        return fedipidentrocalcula;
    }

    /**
     * Setter method for fedipidentrocalcula.
     *
     * @param aFedipidentrocalcula the new value for fedipidentrocalcula
     */
    public void setFedipidentrocalcula(short aFedipidentrocalcula) {
        fedipidentrocalcula = aFedipidentrocalcula;
    }

    /**
     * Access method for fedipiforacalcula.
     *
     * @return the current value of fedipiforacalcula
     */
    public short getFedipiforacalcula() {
        return fedipiforacalcula;
    }

    /**
     * Setter method for fedipiforacalcula.
     *
     * @param aFedipiforacalcula the new value for fedipiforacalcula
     */
    public void setFedipiforacalcula(short aFedipiforacalcula) {
        fedipiforacalcula = aFedipiforacalcula;
    }

    /**
     * Access method for esticmsdentrocalcula.
     *
     * @return the current value of esticmsdentrocalcula
     */
    public short getEsticmsdentrocalcula() {
        return esticmsdentrocalcula;
    }

    /**
     * Setter method for esticmsdentrocalcula.
     *
     * @param aEsticmsdentrocalcula the new value for esticmsdentrocalcula
     */
    public void setEsticmsdentrocalcula(short aEsticmsdentrocalcula) {
        esticmsdentrocalcula = aEsticmsdentrocalcula;
    }

    /**
     * Access method for esticmsforacalcula.
     *
     * @return the current value of esticmsforacalcula
     */
    public short getEsticmsforacalcula() {
        return esticmsforacalcula;
    }

    /**
     * Setter method for esticmsforacalcula.
     *
     * @param aEsticmsforacalcula the new value for esticmsforacalcula
     */
    public void setEsticmsforacalcula(short aEsticmsforacalcula) {
        esticmsforacalcula = aEsticmsforacalcula;
    }

    /**
     * Access method for estmvacalcula.
     *
     * @return the current value of estmvacalcula
     */
    public short getEstmvacalcula() {
        return estmvacalcula;
    }

    /**
     * Setter method for estmvacalcula.
     *
     * @param aEstmvacalcula the new value for estmvacalcula
     */
    public void setEstmvacalcula(short aEstmvacalcula) {
        estmvacalcula = aEstmvacalcula;
    }

    /**
     * Access method for estmvadentro.
     *
     * @return the current value of estmvadentro
     */
    public BigDecimal getEstmvadentro() {
        return estmvadentro;
    }

    /**
     * Setter method for estmvadentro.
     *
     * @param aEstmvadentro the new value for estmvadentro
     */
    public void setEstmvadentro(BigDecimal aEstmvadentro) {
        estmvadentro = aEstmvadentro;
    }

    /**
     * Access method for estmvafora.
     *
     * @return the current value of estmvafora
     */
    public BigDecimal getEstmvafora() {
        return estmvafora;
    }

    /**
     * Setter method for estmvafora.
     *
     * @param aEstmvafora the new value for estmvafora
     */
    public void setEstmvafora(BigDecimal aEstmvafora) {
        estmvafora = aEstmvafora;
    }

    /**
     * Access method for estmvadifaliquota.
     *
     * @return the current value of estmvadifaliquota
     */
    public short getEstmvadifaliquota() {
        return estmvadifaliquota;
    }

    /**
     * Setter method for estmvadifaliquota.
     *
     * @param aEstmvadifaliquota the new value for estmvadifaliquota
     */
    public void setEstmvadifaliquota(short aEstmvadifaliquota) {
        estmvadifaliquota = aEstmvadifaliquota;
    }

    /**
     * Access method for estipvacalcula.
     *
     * @return the current value of estipvacalcula
     */
    public short getEstipvacalcula() {
        return estipvacalcula;
    }

    /**
     * Setter method for estipvacalcula.
     *
     * @param aEstipvacalcula the new value for estipvacalcula
     */
    public void setEstipvacalcula(short aEstipvacalcula) {
        estipvacalcula = aEstipvacalcula;
    }

    /**
     * Access method for eststbasecalcfora.
     *
     * @return the current value of eststbasecalcfora
     */
    public BigDecimal getEststbasecalcfora() {
        return eststbasecalcfora;
    }

    /**
     * Setter method for eststbasecalcfora.
     *
     * @param aEststbasecalcfora the new value for eststbasecalcfora
     */
    public void setEststbasecalcfora(BigDecimal aEststbasecalcfora) {
        eststbasecalcfora = aEststbasecalcfora;
    }

    /**
     * Access method for eststbasecalcdentro.
     *
     * @return the current value of eststbasecalcdentro
     */
    public BigDecimal getEststbasecalcdentro() {
        return eststbasecalcdentro;
    }

    /**
     * Setter method for eststbasecalcdentro.
     *
     * @param aEststbasecalcdentro the new value for eststbasecalcdentro
     */
    public void setEststbasecalcdentro(BigDecimal aEststbasecalcdentro) {
        eststbasecalcdentro = aEststbasecalcdentro;
    }

    /**
     * Access method for eststpercfora.
     *
     * @return the current value of eststpercfora
     */
    public BigDecimal getEststpercfora() {
        return eststpercfora;
    }

    /**
     * Setter method for eststpercfora.
     *
     * @param aEststpercfora the new value for eststpercfora
     */
    public void setEststpercfora(BigDecimal aEststpercfora) {
        eststpercfora = aEststpercfora;
    }

    /**
     * Access method for eststpercdentro.
     *
     * @return the current value of eststpercdentro
     */
    public BigDecimal getEststpercdentro() {
        return eststpercdentro;
    }

    /**
     * Setter method for eststpercdentro.
     *
     * @param aEststpercdentro the new value for eststpercdentro
     */
    public void setEststpercdentro(BigDecimal aEststpercdentro) {
        eststpercdentro = aEststpercdentro;
    }

    /**
     * Access method for muniptucalcula.
     *
     * @return the current value of muniptucalcula
     */
    public short getMuniptucalcula() {
        return muniptucalcula;
    }

    /**
     * Setter method for muniptucalcula.
     *
     * @param aMuniptucalcula the new value for muniptucalcula
     */
    public void setMuniptucalcula(short aMuniptucalcula) {
        muniptucalcula = aMuniptucalcula;
    }

    /**
     * Access method for munitbicalcula.
     *
     * @return the current value of munitbicalcula
     */
    public short getMunitbicalcula() {
        return munitbicalcula;
    }

    /**
     * Setter method for munitbicalcula.
     *
     * @param aMunitbicalcula the new value for munitbicalcula
     */
    public void setMunitbicalcula(short aMunitbicalcula) {
        munitbicalcula = aMunitbicalcula;
    }

    /**
     * Access method for munisscalcula.
     *
     * @return the current value of munisscalcula
     */
    public short getMunisscalcula() {
        return munisscalcula;
    }

    /**
     * Setter method for munisscalcula.
     *
     * @param aMunisscalcula the new value for munisscalcula
     */
    public void setMunisscalcula(short aMunisscalcula) {
        munisscalcula = aMunisscalcula;
    }

    /**
     * Access method for munissperc.
     *
     * @return the current value of munissperc
     */
    public BigDecimal getMunissperc() {
        return munissperc;
    }

    /**
     * Setter method for munissperc.
     *
     * @param aMunissperc the new value for munissperc
     */
    public void setMunissperc(BigDecimal aMunissperc) {
        munissperc = aMunissperc;
    }

    /**
     * Access method for fedpisdentroperc.
     *
     * @return the current value of fedpisdentroperc
     */
    public BigDecimal getFedpisdentroperc() {
        return fedpisdentroperc;
    }

    /**
     * Setter method for fedpisdentroperc.
     *
     * @param aFedpisdentroperc the new value for fedpisdentroperc
     */
    public void setFedpisdentroperc(BigDecimal aFedpisdentroperc) {
        fedpisdentroperc = aFedpisdentroperc;
    }

    /**
     * Access method for fedpisdentrobasecalc.
     *
     * @return the current value of fedpisdentrobasecalc
     */
    public BigDecimal getFedpisdentrobasecalc() {
        return fedpisdentrobasecalc;
    }

    /**
     * Setter method for fedpisdentrobasecalc.
     *
     * @param aFedpisdentrobasecalc the new value for fedpisdentrobasecalc
     */
    public void setFedpisdentrobasecalc(BigDecimal aFedpisdentrobasecalc) {
        fedpisdentrobasecalc = aFedpisdentrobasecalc;
    }

    /**
     * Access method for fedcofinsdentroperc.
     *
     * @return the current value of fedcofinsdentroperc
     */
    public BigDecimal getFedcofinsdentroperc() {
        return fedcofinsdentroperc;
    }

    /**
     * Setter method for fedcofinsdentroperc.
     *
     * @param aFedcofinsdentroperc the new value for fedcofinsdentroperc
     */
    public void setFedcofinsdentroperc(BigDecimal aFedcofinsdentroperc) {
        fedcofinsdentroperc = aFedcofinsdentroperc;
    }

    /**
     * Access method for fedcofinsdentrobasecalc.
     *
     * @return the current value of fedcofinsdentrobasecalc
     */
    public BigDecimal getFedcofinsdentrobasecalc() {
        return fedcofinsdentrobasecalc;
    }

    /**
     * Setter method for fedcofinsdentrobasecalc.
     *
     * @param aFedcofinsdentrobasecalc the new value for fedcofinsdentrobasecalc
     */
    public void setFedcofinsdentrobasecalc(BigDecimal aFedcofinsdentrobasecalc) {
        fedcofinsdentrobasecalc = aFedcofinsdentrobasecalc;
    }

    /**
     * Access method for fedpisforaperc.
     *
     * @return the current value of fedpisforaperc
     */
    public BigDecimal getFedpisforaperc() {
        return fedpisforaperc;
    }

    /**
     * Setter method for fedpisforaperc.
     *
     * @param aFedpisforaperc the new value for fedpisforaperc
     */
    public void setFedpisforaperc(BigDecimal aFedpisforaperc) {
        fedpisforaperc = aFedpisforaperc;
    }

    /**
     * Access method for fedpisforabasecalc.
     *
     * @return the current value of fedpisforabasecalc
     */
    public BigDecimal getFedpisforabasecalc() {
        return fedpisforabasecalc;
    }

    /**
     * Setter method for fedpisforabasecalc.
     *
     * @param aFedpisforabasecalc the new value for fedpisforabasecalc
     */
    public void setFedpisforabasecalc(BigDecimal aFedpisforabasecalc) {
        fedpisforabasecalc = aFedpisforabasecalc;
    }

    /**
     * Access method for fedcofinsforaperc.
     *
     * @return the current value of fedcofinsforaperc
     */
    public BigDecimal getFedcofinsforaperc() {
        return fedcofinsforaperc;
    }

    /**
     * Setter method for fedcofinsforaperc.
     *
     * @param aFedcofinsforaperc the new value for fedcofinsforaperc
     */
    public void setFedcofinsforaperc(BigDecimal aFedcofinsforaperc) {
        fedcofinsforaperc = aFedcofinsforaperc;
    }

    /**
     * Access method for fedcofinsforabasecalc.
     *
     * @return the current value of fedcofinsforabasecalc
     */
    public BigDecimal getFedcofinsforabasecalc() {
        return fedcofinsforabasecalc;
    }

    /**
     * Setter method for fedcofinsforabasecalc.
     *
     * @param aFedcofinsforabasecalc the new value for fedcofinsforabasecalc
     */
    public void setFedcofinsforabasecalc(BigDecimal aFedcofinsforabasecalc) {
        fedcofinsforabasecalc = aFedcofinsforabasecalc;
    }

    /**
     * Access method for eststcalculafora.
     *
     * @return the current value of eststcalculafora
     */
    public short getEststcalculafora() {
        return eststcalculafora;
    }

    /**
     * Setter method for eststcalculafora.
     *
     * @param aEststcalculafora the new value for eststcalculafora
     */
    public void setEststcalculafora(short aEststcalculafora) {
        eststcalculafora = aEststcalculafora;
    }

    /**
     * Access method for eststcalculadentro.
     *
     * @return the current value of eststcalculadentro
     */
    public short getEststcalculadentro() {
        return eststcalculadentro;
    }

    /**
     * Setter method for eststcalculadentro.
     *
     * @param aEststcalculadentro the new value for eststcalculadentro
     */
    public void setEststcalculadentro(short aEststcalculadentro) {
        eststcalculadentro = aEststcalculadentro;
    }

    /**
     * Access method for opcaoagenteconsfinal.
     *
     * @return the current value of opcaoagenteconsfinal
     */
    public short getOpcaoagenteconsfinal() {
        return opcaoagenteconsfinal;
    }

    /**
     * Setter method for opcaoagenteconsfinal.
     *
     * @param aOpcaoagenteconsfinal the new value for opcaoagenteconsfinal
     */
    public void setOpcaoagenteconsfinal(short aOpcaoagenteconsfinal) {
        opcaoagenteconsfinal = aOpcaoagenteconsfinal;
    }

    /**
     * Access method for opcaoagentecomercio.
     *
     * @return the current value of opcaoagentecomercio
     */
    public short getOpcaoagentecomercio() {
        return opcaoagentecomercio;
    }

    /**
     * Setter method for opcaoagentecomercio.
     *
     * @param aOpcaoagentecomercio the new value for opcaoagentecomercio
     */
    public void setOpcaoagentecomercio(short aOpcaoagentecomercio) {
        opcaoagentecomercio = aOpcaoagentecomercio;
    }

    /**
     * Access method for opcaoagentesubsttrib.
     *
     * @return the current value of opcaoagentesubsttrib
     */
    public short getOpcaoagentesubsttrib() {
        return opcaoagentesubsttrib;
    }

    /**
     * Setter method for opcaoagentesubsttrib.
     *
     * @param aOpcaoagentesubsttrib the new value for opcaoagentesubsttrib
     */
    public void setOpcaoagentesubsttrib(short aOpcaoagentesubsttrib) {
        opcaoagentesubsttrib = aOpcaoagentesubsttrib;
    }

    /**
     * Access method for fedpisdentrocalcula.
     *
     * @return the current value of fedpisdentrocalcula
     */
    public short getFedpisdentrocalcula() {
        return fedpisdentrocalcula;
    }

    /**
     * Setter method for fedpisdentrocalcula.
     *
     * @param aFedpisdentrocalcula the new value for fedpisdentrocalcula
     */
    public void setFedpisdentrocalcula(short aFedpisdentrocalcula) {
        fedpisdentrocalcula = aFedpisdentrocalcula;
    }

    /**
     * Access method for fedcofinsdentrocalcula.
     *
     * @return the current value of fedcofinsdentrocalcula
     */
    public short getFedcofinsdentrocalcula() {
        return fedcofinsdentrocalcula;
    }

    /**
     * Setter method for fedcofinsdentrocalcula.
     *
     * @param aFedcofinsdentrocalcula the new value for fedcofinsdentrocalcula
     */
    public void setFedcofinsdentrocalcula(short aFedcofinsdentrocalcula) {
        fedcofinsdentrocalcula = aFedcofinsdentrocalcula;
    }

    /**
     * Access method for fedpisforacalcula.
     *
     * @return the current value of fedpisforacalcula
     */
    public short getFedpisforacalcula() {
        return fedpisforacalcula;
    }

    /**
     * Setter method for fedpisforacalcula.
     *
     * @param aFedpisforacalcula the new value for fedpisforacalcula
     */
    public void setFedpisforacalcula(short aFedpisforacalcula) {
        fedpisforacalcula = aFedpisforacalcula;
    }

    /**
     * Access method for fedcofinsforacalcula.
     *
     * @return the current value of fedcofinsforacalcula
     */
    public short getFedcofinsforacalcula() {
        return fedcofinsforacalcula;
    }

    /**
     * Setter method for fedcofinsforacalcula.
     *
     * @param aFedcofinsforacalcula the new value for fedcofinsforacalcula
     */
    public void setFedcofinsforacalcula(short aFedcofinsforacalcula) {
        fedcofinsforacalcula = aFedcofinsforacalcula;
    }

    /**
     * Access method for esticmsaliqdentro.
     *
     * @return the current value of esticmsaliqdentro
     */
    public BigDecimal getEsticmsaliqdentro() {
        return esticmsaliqdentro;
    }

    /**
     * Setter method for esticmsaliqdentro.
     *
     * @param aEsticmsaliqdentro the new value for esticmsaliqdentro
     */
    public void setEsticmsaliqdentro(BigDecimal aEsticmsaliqdentro) {
        esticmsaliqdentro = aEsticmsaliqdentro;
    }

    /**
     * Access method for esticmsaliqfora.
     *
     * @return the current value of esticmsaliqfora
     */
    public BigDecimal getEsticmsaliqfora() {
        return esticmsaliqfora;
    }

    /**
     * Setter method for esticmsaliqfora.
     *
     * @param aEsticmsaliqfora the new value for esticmsaliqfora
     */
    public void setEsticmsaliqfora(BigDecimal aEsticmsaliqfora) {
        esticmsaliqfora = aEsticmsaliqfora;
    }

    /**
     * Access method for esticmsredbasecalcdentro.
     *
     * @return the current value of esticmsredbasecalcdentro
     */
    public BigDecimal getEsticmsredbasecalcdentro() {
        return esticmsredbasecalcdentro;
    }

    /**
     * Setter method for esticmsredbasecalcdentro.
     *
     * @param aEsticmsredbasecalcdentro the new value for esticmsredbasecalcdentro
     */
    public void setEsticmsredbasecalcdentro(BigDecimal aEsticmsredbasecalcdentro) {
        esticmsredbasecalcdentro = aEsticmsredbasecalcdentro;
    }

    /**
     * Access method for esticmsredbasecalcfora.
     *
     * @return the current value of esticmsredbasecalcfora
     */
    public BigDecimal getEsticmsredbasecalcfora() {
        return esticmsredbasecalcfora;
    }

    /**
     * Setter method for esticmsredbasecalcfora.
     *
     * @param aEsticmsredbasecalcfora the new value for esticmsredbasecalcfora
     */
    public void setEsticmsredbasecalcfora(BigDecimal aEsticmsredbasecalcfora) {
        esticmsredbasecalcfora = aEsticmsredbasecalcfora;
    }

    /**
     * Access method for munexigibilidadeiss.
     *
     * @return the current value of munexigibilidadeiss
     */
    public short getMunexigibilidadeiss() {
        return munexigibilidadeiss;
    }

    /**
     * Setter method for munexigibilidadeiss.
     *
     * @param aMunexigibilidadeiss the new value for munexigibilidadeiss
     */
    public void setMunexigibilidadeiss(short aMunexigibilidadeiss) {
        munexigibilidadeiss = aMunexigibilidadeiss;
    }

    /**
     * Access method for estipvaaliq.
     *
     * @return the current value of estipvaaliq
     */
    public BigDecimal getEstipvaaliq() {
        return estipvaaliq;
    }

    /**
     * Setter method for estipvaaliq.
     *
     * @param aEstipvaaliq the new value for estipvaaliq
     */
    public void setEstipvaaliq(BigDecimal aEstipvaaliq) {
        estipvaaliq = aEstipvaaliq;
    }

    /**
     * Access method for estipvadescavista.
     *
     * @return the current value of estipvadescavista
     */
    public BigDecimal getEstipvadescavista() {
        return estipvadescavista;
    }

    /**
     * Setter method for estipvadescavista.
     *
     * @param aEstipvadescavista the new value for estipvadescavista
     */
    public void setEstipvadescavista(BigDecimal aEstipvadescavista) {
        estipvadescavista = aEstipvadescavista;
    }

    /**
     * Access method for estipvageraanual.
     *
     * @return the current value of estipvageraanual
     */
    public short getEstipvageraanual() {
        return estipvageraanual;
    }

    /**
     * Setter method for estipvageraanual.
     *
     * @param aEstipvageraanual the new value for estipvageraanual
     */
    public void setEstipvageraanual(short aEstipvageraanual) {
        estipvageraanual = aEstipvageraanual;
    }

    /**
     * Access method for estipvamesprimparc.
     *
     * @return the current value of estipvamesprimparc
     */
    public int getEstipvamesprimparc() {
        return estipvamesprimparc;
    }

    /**
     * Setter method for estipvamesprimparc.
     *
     * @param aEstipvamesprimparc the new value for estipvamesprimparc
     */
    public void setEstipvamesprimparc(int aEstipvamesprimparc) {
        estipvamesprimparc = aEstipvamesprimparc;
    }

    /**
     * Access method for estipvaparcelas.
     *
     * @return the current value of estipvaparcelas
     */
    public int getEstipvaparcelas() {
        return estipvaparcelas;
    }

    /**
     * Setter method for estipvaparcelas.
     *
     * @param aEstipvaparcelas the new value for estipvaparcelas
     */
    public void setEstipvaparcelas(int aEstipvaparcelas) {
        estipvaparcelas = aEstipvaparcelas;
    }

    /**
     * Access method for estipvadpvat.
     *
     * @return the current value of estipvadpvat
     */
    public short getEstipvadpvat() {
        return estipvadpvat;
    }

    /**
     * Setter method for estipvadpvat.
     *
     * @param aEstipvadpvat the new value for estipvadpvat
     */
    public void setEstipvadpvat(short aEstipvadpvat) {
        estipvadpvat = aEstipvadpvat;
    }

    /**
     * Access method for estipvavalordpvat.
     *
     * @return the current value of estipvavalordpvat
     */
    public BigDecimal getEstipvavalordpvat() {
        return estipvavalordpvat;
    }

    /**
     * Setter method for estipvavalordpvat.
     *
     * @param aEstipvavalordpvat the new value for estipvavalordpvat
     */
    public void setEstipvavalordpvat(BigDecimal aEstipvavalordpvat) {
        estipvavalordpvat = aEstipvavalordpvat;
    }

    /**
     * Access method for estipvalic.
     *
     * @return the current value of estipvalic
     */
    public short getEstipvalic() {
        return estipvalic;
    }

    /**
     * Setter method for estipvalic.
     *
     * @param aEstipvalic the new value for estipvalic
     */
    public void setEstipvalic(short aEstipvalic) {
        estipvalic = aEstipvalic;
    }

    /**
     * Access method for estipvavalorlic.
     *
     * @return the current value of estipvavalorlic
     */
    public BigDecimal getEstipvavalorlic() {
        return estipvavalorlic;
    }

    /**
     * Setter method for estipvavalorlic.
     *
     * @param aEstipvavalorlic the new value for estipvavalorlic
     */
    public void setEstipvavalorlic(BigDecimal aEstipvavalorlic) {
        estipvavalorlic = aEstipvavalorlic;
    }

    /**
     * Access method for muniptualiq.
     *
     * @return the current value of muniptualiq
     */
    public BigDecimal getMuniptualiq() {
        return muniptualiq;
    }

    /**
     * Setter method for muniptualiq.
     *
     * @param aMuniptualiq the new value for muniptualiq
     */
    public void setMuniptualiq(BigDecimal aMuniptualiq) {
        muniptualiq = aMuniptualiq;
    }

    /**
     * Access method for muniptudescavista.
     *
     * @return the current value of muniptudescavista
     */
    public BigDecimal getMuniptudescavista() {
        return muniptudescavista;
    }

    /**
     * Setter method for muniptudescavista.
     *
     * @param aMuniptudescavista the new value for muniptudescavista
     */
    public void setMuniptudescavista(BigDecimal aMuniptudescavista) {
        muniptudescavista = aMuniptudescavista;
    }

    /**
     * Access method for muniptugeraanual.
     *
     * @return the current value of muniptugeraanual
     */
    public short getMuniptugeraanual() {
        return muniptugeraanual;
    }

    /**
     * Setter method for muniptugeraanual.
     *
     * @param aMuniptugeraanual the new value for muniptugeraanual
     */
    public void setMuniptugeraanual(short aMuniptugeraanual) {
        muniptugeraanual = aMuniptugeraanual;
    }

    /**
     * Access method for muniptumesprimparc.
     *
     * @return the current value of muniptumesprimparc
     */
    public int getMuniptumesprimparc() {
        return muniptumesprimparc;
    }

    /**
     * Setter method for muniptumesprimparc.
     *
     * @param aMuniptumesprimparc the new value for muniptumesprimparc
     */
    public void setMuniptumesprimparc(int aMuniptumesprimparc) {
        muniptumesprimparc = aMuniptumesprimparc;
    }

    /**
     * Access method for muniptuparcelas.
     *
     * @return the current value of muniptuparcelas
     */
    public int getMuniptuparcelas() {
        return muniptuparcelas;
    }

    /**
     * Setter method for muniptuparcelas.
     *
     * @param aMuniptuparcelas the new value for muniptuparcelas
     */
    public void setMuniptuparcelas(int aMuniptuparcelas) {
        muniptuparcelas = aMuniptuparcelas;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for impcstipi2.
     *
     * @return the current value of impcstipi2
     */
    public Impcstipi getImpcstipi2() {
        return impcstipi2;
    }

    /**
     * Setter method for impcstipi2.
     *
     * @param aImpcstipi2 the new value for impcstipi2
     */
    public void setImpcstipi2(Impcstipi aImpcstipi2) {
        impcstipi2 = aImpcstipi2;
    }

    /**
     * Access method for impcst.
     *
     * @return the current value of impcst
     */
    public Impcst getImpcst() {
        return impcst;
    }

    /**
     * Setter method for impcst.
     *
     * @param aImpcst the new value for impcst
     */
    public void setImpcst(Impcst aImpcst) {
        impcst = aImpcst;
    }

    /**
     * Access method for impcst2.
     *
     * @return the current value of impcst2
     */
    public Impcst getImpcst2() {
        return impcst2;
    }

    /**
     * Setter method for impcst2.
     *
     * @param aImpcst2 the new value for impcst2
     */
    public void setImpcst2(Impcst aImpcst2) {
        impcst2 = aImpcst2;
    }

    /**
     * Access method for impcstpc.
     *
     * @return the current value of impcstpc
     */
    public Impcstpc getImpcstpc() {
        return impcstpc;
    }

    /**
     * Setter method for impcstpc.
     *
     * @param aImpcstpc the new value for impcstpc
     */
    public void setImpcstpc(Impcstpc aImpcstpc) {
        impcstpc = aImpcstpc;
    }

    /**
     * Access method for impcstpc2.
     *
     * @return the current value of impcstpc2
     */
    public Impcstpc getImpcstpc2() {
        return impcstpc2;
    }

    /**
     * Setter method for impcstpc2.
     *
     * @param aImpcstpc2 the new value for impcstpc2
     */
    public void setImpcstpc2(Impcstpc aImpcstpc2) {
        impcstpc2 = aImpcstpc2;
    }

    /**
     * Access method for impcstpc3.
     *
     * @return the current value of impcstpc3
     */
    public Impcstpc getImpcstpc3() {
        return impcstpc3;
    }

    /**
     * Setter method for impcstpc3.
     *
     * @param aImpcstpc3 the new value for impcstpc3
     */
    public void setImpcstpc3(Impcstpc aImpcstpc3) {
        impcstpc3 = aImpcstpc3;
    }

    /**
     * Access method for impcstpc4.
     *
     * @return the current value of impcstpc4
     */
    public Impcstpc getImpcstpc4() {
        return impcstpc4;
    }

    /**
     * Setter method for impcstpc4.
     *
     * @param aImpcstpc4 the new value for impcstpc4
     */
    public void setImpcstpc4(Impcstpc aImpcstpc4) {
        impcstpc4 = aImpcstpc4;
    }

    /**
     * Access method for impcnae.
     *
     * @return the current value of impcnae
     */
    public Impcnae getImpcnae() {
        return impcnae;
    }

    /**
     * Setter method for impcnae.
     *
     * @param aImpcnae the new value for impcnae
     */
    public void setImpcnae(Impcnae aImpcnae) {
        impcnae = aImpcnae;
    }

    /**
     * Access method for impcst3.
     *
     * @return the current value of impcst3
     */
    public Impcst getImpcst3() {
        return impcst3;
    }

    /**
     * Setter method for impcst3.
     *
     * @param aImpcst3 the new value for impcst3
     */
    public void setImpcst3(Impcst aImpcst3) {
        impcst3 = aImpcst3;
    }

    /**
     * Access method for impcst4.
     *
     * @return the current value of impcst4
     */
    public Impcst getImpcst4() {
        return impcst4;
    }

    /**
     * Setter method for impcst4.
     *
     * @param aImpcst4 the new value for impcst4
     */
    public void setImpcst4(Impcst aImpcst4) {
        impcst4 = aImpcst4;
    }

    /**
     * Access method for confuf.
     *
     * @return the current value of confuf
     */
    public Confuf getConfuf() {
        return confuf;
    }

    /**
     * Setter method for confuf.
     *
     * @param aConfuf the new value for confuf
     */
    public void setConfuf(Confuf aConfuf) {
        confuf = aConfuf;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Optipo getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Optipo aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Opoperacao getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Opoperacao aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for impcsosn.
     *
     * @return the current value of impcsosn
     */
    public Impcsosn getImpcsosn() {
        return impcsosn;
    }

    /**
     * Setter method for impcsosn.
     *
     * @param aImpcsosn the new value for impcsosn
     */
    public void setImpcsosn(Impcsosn aImpcsosn) {
        impcsosn = aImpcsosn;
    }

    /**
     * Access method for impcsosn2.
     *
     * @return the current value of impcsosn2
     */
    public Impcsosn getImpcsosn2() {
        return impcsosn2;
    }

    /**
     * Setter method for impcsosn2.
     *
     * @param aImpcsosn2 the new value for impcsosn2
     */
    public void setImpcsosn2(Impcsosn aImpcsosn2) {
        impcsosn2 = aImpcsosn2;
    }

    /**
     * Access method for impcsosn3.
     *
     * @return the current value of impcsosn3
     */
    public Impcsosn getImpcsosn3() {
        return impcsosn3;
    }

    /**
     * Setter method for impcsosn3.
     *
     * @param aImpcsosn3 the new value for impcsosn3
     */
    public void setImpcsosn3(Impcsosn aImpcsosn3) {
        impcsosn3 = aImpcsosn3;
    }

    /**
     * Access method for impcsosn4.
     *
     * @return the current value of impcsosn4
     */
    public Impcsosn getImpcsosn4() {
        return impcsosn4;
    }

    /**
     * Setter method for impcsosn4.
     *
     * @param aImpcsosn4 the new value for impcsosn4
     */
    public void setImpcsosn4(Impcsosn aImpcsosn4) {
        impcsosn4 = aImpcsosn4;
    }

    /**
     * Access method for impcstorig.
     *
     * @return the current value of impcstorig
     */
    public Impcstorig getImpcstorig() {
        return impcstorig;
    }

    /**
     * Setter method for impcstorig.
     *
     * @param aImpcstorig the new value for impcstorig
     */
    public void setImpcstorig(Impcstorig aImpcstorig) {
        impcstorig = aImpcstorig;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for ctbplanoconta6.
     *
     * @return the current value of ctbplanoconta6
     */
    public Ctbplanoconta getCtbplanoconta6() {
        return ctbplanoconta6;
    }

    /**
     * Setter method for ctbplanoconta6.
     *
     * @param aCtbplanoconta6 the new value for ctbplanoconta6
     */
    public void setCtbplanoconta6(Ctbplanoconta aCtbplanoconta6) {
        ctbplanoconta6 = aCtbplanoconta6;
    }

    /**
     * Access method for confcidade.
     *
     * @return the current value of confcidade
     */
    public Confcidade getConfcidade() {
        return confcidade;
    }

    /**
     * Setter method for confcidade.
     *
     * @param aConfcidade the new value for confcidade
     */
    public void setConfcidade(Confcidade aConfcidade) {
        confcidade = aConfcidade;
    }

    /**
     * Access method for ctbplanoconta5.
     *
     * @return the current value of ctbplanoconta5
     */
    public Ctbplanoconta getCtbplanoconta5() {
        return ctbplanoconta5;
    }

    /**
     * Setter method for ctbplanoconta5.
     *
     * @param aCtbplanoconta5 the new value for ctbplanoconta5
     */
    public void setCtbplanoconta5(Ctbplanoconta aCtbplanoconta5) {
        ctbplanoconta5 = aCtbplanoconta5;
    }

    /**
     * Access method for ctbplanoconta4.
     *
     * @return the current value of ctbplanoconta4
     */
    public Ctbplanoconta getCtbplanoconta4() {
        return ctbplanoconta4;
    }

    /**
     * Setter method for ctbplanoconta4.
     *
     * @param aCtbplanoconta4 the new value for ctbplanoconta4
     */
    public void setCtbplanoconta4(Ctbplanoconta aCtbplanoconta4) {
        ctbplanoconta4 = aCtbplanoconta4;
    }

    /**
     * Access method for ctbplanoconta3.
     *
     * @return the current value of ctbplanoconta3
     */
    public Ctbplanoconta getCtbplanoconta3() {
        return ctbplanoconta3;
    }

    /**
     * Setter method for ctbplanoconta3.
     *
     * @param aCtbplanoconta3 the new value for ctbplanoconta3
     */
    public void setCtbplanoconta3(Ctbplanoconta aCtbplanoconta3) {
        ctbplanoconta3 = aCtbplanoconta3;
    }

    /**
     * Access method for ctbplanoconta8.
     *
     * @return the current value of ctbplanoconta8
     */
    public Ctbplanoconta getCtbplanoconta8() {
        return ctbplanoconta8;
    }

    /**
     * Setter method for ctbplanoconta8.
     *
     * @param aCtbplanoconta8 the new value for ctbplanoconta8
     */
    public void setCtbplanoconta8(Ctbplanoconta aCtbplanoconta8) {
        ctbplanoconta8 = aCtbplanoconta8;
    }

    /**
     * Access method for ctbplanoconta7.
     *
     * @return the current value of ctbplanoconta7
     */
    public Ctbplanoconta getCtbplanoconta7() {
        return ctbplanoconta7;
    }

    /**
     * Setter method for ctbplanoconta7.
     *
     * @param aCtbplanoconta7 the new value for ctbplanoconta7
     */
    public void setCtbplanoconta7(Ctbplanoconta aCtbplanoconta7) {
        ctbplanoconta7 = aCtbplanoconta7;
    }

    /**
     * Access method for ctbplanoconta10.
     *
     * @return the current value of ctbplanoconta10
     */
    public Ctbplanoconta getCtbplanoconta10() {
        return ctbplanoconta10;
    }

    /**
     * Setter method for ctbplanoconta10.
     *
     * @param aCtbplanoconta10 the new value for ctbplanoconta10
     */
    public void setCtbplanoconta10(Ctbplanoconta aCtbplanoconta10) {
        ctbplanoconta10 = aCtbplanoconta10;
    }

    /**
     * Access method for ctbplanoconta9.
     *
     * @return the current value of ctbplanoconta9
     */
    public Ctbplanoconta getCtbplanoconta9() {
        return ctbplanoconta9;
    }

    /**
     * Setter method for ctbplanoconta9.
     *
     * @param aCtbplanoconta9 the new value for ctbplanoconta9
     */
    public void setCtbplanoconta9(Ctbplanoconta aCtbplanoconta9) {
        ctbplanoconta9 = aCtbplanoconta9;
    }

    /**
     * Access method for ctbplanoconta12.
     *
     * @return the current value of ctbplanoconta12
     */
    public Ctbplanoconta getCtbplanoconta12() {
        return ctbplanoconta12;
    }

    /**
     * Setter method for ctbplanoconta12.
     *
     * @param aCtbplanoconta12 the new value for ctbplanoconta12
     */
    public void setCtbplanoconta12(Ctbplanoconta aCtbplanoconta12) {
        ctbplanoconta12 = aCtbplanoconta12;
    }

    /**
     * Access method for ctbplanoconta11.
     *
     * @return the current value of ctbplanoconta11
     */
    public Ctbplanoconta getCtbplanoconta11() {
        return ctbplanoconta11;
    }

    /**
     * Setter method for ctbplanoconta11.
     *
     * @param aCtbplanoconta11 the new value for ctbplanoconta11
     */
    public void setCtbplanoconta11(Ctbplanoconta aCtbplanoconta11) {
        ctbplanoconta11 = aCtbplanoconta11;
    }

    /**
     * Access method for ctbplanoconta14.
     *
     * @return the current value of ctbplanoconta14
     */
    public Ctbplanoconta getCtbplanoconta14() {
        return ctbplanoconta14;
    }

    /**
     * Setter method for ctbplanoconta14.
     *
     * @param aCtbplanoconta14 the new value for ctbplanoconta14
     */
    public void setCtbplanoconta14(Ctbplanoconta aCtbplanoconta14) {
        ctbplanoconta14 = aCtbplanoconta14;
    }

    /**
     * Access method for prodgrupo.
     *
     * @return the current value of prodgrupo
     */
    public Prodgrupo getProdgrupo() {
        return prodgrupo;
    }

    /**
     * Setter method for prodgrupo.
     *
     * @param aProdgrupo the new value for prodgrupo
     */
    public void setProdgrupo(Prodgrupo aProdgrupo) {
        prodgrupo = aProdgrupo;
    }

    /**
     * Access method for ctbplanoconta13.
     *
     * @return the current value of ctbplanoconta13
     */
    public Ctbplanoconta getCtbplanoconta13() {
        return ctbplanoconta13;
    }

    /**
     * Setter method for ctbplanoconta13.
     *
     * @param aCtbplanoconta13 the new value for ctbplanoconta13
     */
    public void setCtbplanoconta13(Ctbplanoconta aCtbplanoconta13) {
        ctbplanoconta13 = aCtbplanoconta13;
    }

    /**
     * Access method for ctbplanoconta16.
     *
     * @return the current value of ctbplanoconta16
     */
    public Ctbplanoconta getCtbplanoconta16() {
        return ctbplanoconta16;
    }

    /**
     * Setter method for ctbplanoconta16.
     *
     * @param aCtbplanoconta16 the new value for ctbplanoconta16
     */
    public void setCtbplanoconta16(Ctbplanoconta aCtbplanoconta16) {
        ctbplanoconta16 = aCtbplanoconta16;
    }

    /**
     * Access method for ctbplanoconta15.
     *
     * @return the current value of ctbplanoconta15
     */
    public Ctbplanoconta getCtbplanoconta15() {
        return ctbplanoconta15;
    }

    /**
     * Setter method for ctbplanoconta15.
     *
     * @param aCtbplanoconta15 the new value for ctbplanoconta15
     */
    public void setCtbplanoconta15(Ctbplanoconta aCtbplanoconta15) {
        ctbplanoconta15 = aCtbplanoconta15;
    }

    /**
     * Access method for ctbplanoconta17.
     *
     * @return the current value of ctbplanoconta17
     */
    public Ctbplanoconta getCtbplanoconta17() {
        return ctbplanoconta17;
    }

    /**
     * Setter method for ctbplanoconta17.
     *
     * @param aCtbplanoconta17 the new value for ctbplanoconta17
     */
    public void setCtbplanoconta17(Ctbplanoconta aCtbplanoconta17) {
        ctbplanoconta17 = aCtbplanoconta17;
    }

    /**
     * Access method for ctbplanoconta18.
     *
     * @return the current value of ctbplanoconta18
     */
    public Ctbplanoconta getCtbplanoconta18() {
        return ctbplanoconta18;
    }

    /**
     * Setter method for ctbplanoconta18.
     *
     * @param aCtbplanoconta18 the new value for ctbplanoconta18
     */
    public void setCtbplanoconta18(Ctbplanoconta aCtbplanoconta18) {
        ctbplanoconta18 = aCtbplanoconta18;
    }

    /**
     * Access method for ctbplanoconta19.
     *
     * @return the current value of ctbplanoconta19
     */
    public Ctbplanoconta getCtbplanoconta19() {
        return ctbplanoconta19;
    }

    /**
     * Setter method for ctbplanoconta19.
     *
     * @param aCtbplanoconta19 the new value for ctbplanoconta19
     */
    public void setCtbplanoconta19(Ctbplanoconta aCtbplanoconta19) {
        ctbplanoconta19 = aCtbplanoconta19;
    }

    /**
     * Access method for finplanoconta.
     *
     * @return the current value of finplanoconta
     */
    public Finplanoconta getFinplanoconta() {
        return finplanoconta;
    }

    /**
     * Setter method for finplanoconta.
     *
     * @param aFinplanoconta the new value for finplanoconta
     */
    public void setFinplanoconta(Finplanoconta aFinplanoconta) {
        finplanoconta = aFinplanoconta;
    }

    /**
     * Access method for finfluxoplanoconta.
     *
     * @return the current value of finfluxoplanoconta
     */
    public Finfluxoplanoconta getFinfluxoplanoconta() {
        return finfluxoplanoconta;
    }

    /**
     * Setter method for finfluxoplanoconta.
     *
     * @param aFinfluxoplanoconta the new value for finfluxoplanoconta
     */
    public void setFinfluxoplanoconta(Finfluxoplanoconta aFinfluxoplanoconta) {
        finfluxoplanoconta = aFinfluxoplanoconta;
    }

    /**
     * Access method for finfluxoplanoconta2.
     *
     * @return the current value of finfluxoplanoconta2
     */
    public Finfluxoplanoconta getFinfluxoplanoconta2() {
        return finfluxoplanoconta2;
    }

    /**
     * Setter method for finfluxoplanoconta2.
     *
     * @param aFinfluxoplanoconta2 the new value for finfluxoplanoconta2
     */
    public void setFinfluxoplanoconta2(Finfluxoplanoconta aFinfluxoplanoconta2) {
        finfluxoplanoconta2 = aFinfluxoplanoconta2;
    }

    /**
     * Access method for orcplanoconta.
     *
     * @return the current value of orcplanoconta
     */
    public Orcplanoconta getOrcplanoconta() {
        return orcplanoconta;
    }

    /**
     * Setter method for orcplanoconta.
     *
     * @param aOrcplanoconta the new value for orcplanoconta
     */
    public void setOrcplanoconta(Orcplanoconta aOrcplanoconta) {
        orcplanoconta = aOrcplanoconta;
    }

    /**
     * Access method for prodncm.
     *
     * @return the current value of prodncm
     */
    public Prodncm getProdncm() {
        return prodncm;
    }

    /**
     * Setter method for prodncm.
     *
     * @param aProdncm the new value for prodncm
     */
    public void setProdncm(Prodncm aProdncm) {
        prodncm = aProdncm;
    }

    /**
     * Access method for orcplanoconta2.
     *
     * @return the current value of orcplanoconta2
     */
    public Orcplanoconta getOrcplanoconta2() {
        return orcplanoconta2;
    }

    /**
     * Setter method for orcplanoconta2.
     *
     * @param aOrcplanoconta2 the new value for orcplanoconta2
     */
    public void setOrcplanoconta2(Orcplanoconta aOrcplanoconta2) {
        orcplanoconta2 = aOrcplanoconta2;
    }

    /**
     * Access method for ctbplanoconta20.
     *
     * @return the current value of ctbplanoconta20
     */
    public Ctbplanoconta getCtbplanoconta20() {
        return ctbplanoconta20;
    }

    /**
     * Setter method for ctbplanoconta20.
     *
     * @param aCtbplanoconta20 the new value for ctbplanoconta20
     */
    public void setCtbplanoconta20(Ctbplanoconta aCtbplanoconta20) {
        ctbplanoconta20 = aCtbplanoconta20;
    }

    /**
     * Access method for ctbplanoconta21.
     *
     * @return the current value of ctbplanoconta21
     */
    public Ctbplanoconta getCtbplanoconta21() {
        return ctbplanoconta21;
    }

    /**
     * Setter method for ctbplanoconta21.
     *
     * @param aCtbplanoconta21 the new value for ctbplanoconta21
     */
    public void setCtbplanoconta21(Ctbplanoconta aCtbplanoconta21) {
        ctbplanoconta21 = aCtbplanoconta21;
    }

    /**
     * Access method for ctbplanoconta22.
     *
     * @return the current value of ctbplanoconta22
     */
    public Ctbplanoconta getCtbplanoconta22() {
        return ctbplanoconta22;
    }

    /**
     * Setter method for ctbplanoconta22.
     *
     * @param aCtbplanoconta22 the new value for ctbplanoconta22
     */
    public void setCtbplanoconta22(Ctbplanoconta aCtbplanoconta22) {
        ctbplanoconta22 = aCtbplanoconta22;
    }

    /**
     * Access method for ctbplanoconta23.
     *
     * @return the current value of ctbplanoconta23
     */
    public Ctbplanoconta getCtbplanoconta23() {
        return ctbplanoconta23;
    }

    /**
     * Setter method for ctbplanoconta23.
     *
     * @param aCtbplanoconta23 the new value for ctbplanoconta23
     */
    public void setCtbplanoconta23(Ctbplanoconta aCtbplanoconta23) {
        ctbplanoconta23 = aCtbplanoconta23;
    }

    /**
     * Access method for ctbplanoconta24.
     *
     * @return the current value of ctbplanoconta24
     */
    public Ctbplanoconta getCtbplanoconta24() {
        return ctbplanoconta24;
    }

    /**
     * Setter method for ctbplanoconta24.
     *
     * @param aCtbplanoconta24 the new value for ctbplanoconta24
     */
    public void setCtbplanoconta24(Ctbplanoconta aCtbplanoconta24) {
        ctbplanoconta24 = aCtbplanoconta24;
    }

    /**
     * Access method for ctbplanoconta25.
     *
     * @return the current value of ctbplanoconta25
     */
    public Ctbplanoconta getCtbplanoconta25() {
        return ctbplanoconta25;
    }

    /**
     * Setter method for ctbplanoconta25.
     *
     * @param aCtbplanoconta25 the new value for ctbplanoconta25
     */
    public void setCtbplanoconta25(Ctbplanoconta aCtbplanoconta25) {
        ctbplanoconta25 = aCtbplanoconta25;
    }

    /**
     * Access method for ctbplanoconta26.
     *
     * @return the current value of ctbplanoconta26
     */
    public Ctbplanoconta getCtbplanoconta26() {
        return ctbplanoconta26;
    }

    /**
     * Setter method for ctbplanoconta26.
     *
     * @param aCtbplanoconta26 the new value for ctbplanoconta26
     */
    public void setCtbplanoconta26(Ctbplanoconta aCtbplanoconta26) {
        ctbplanoconta26 = aCtbplanoconta26;
    }

    /**
     * Access method for ctbplanoconta27.
     *
     * @return the current value of ctbplanoconta27
     */
    public Ctbplanoconta getCtbplanoconta27() {
        return ctbplanoconta27;
    }

    /**
     * Setter method for ctbplanoconta27.
     *
     * @param aCtbplanoconta27 the new value for ctbplanoconta27
     */
    public void setCtbplanoconta27(Ctbplanoconta aCtbplanoconta27) {
        ctbplanoconta27 = aCtbplanoconta27;
    }

    /**
     * Access method for ctbplanoconta28.
     *
     * @return the current value of ctbplanoconta28
     */
    public Ctbplanoconta getCtbplanoconta28() {
        return ctbplanoconta28;
    }

    /**
     * Setter method for ctbplanoconta28.
     *
     * @param aCtbplanoconta28 the new value for ctbplanoconta28
     */
    public void setCtbplanoconta28(Ctbplanoconta aCtbplanoconta28) {
        ctbplanoconta28 = aCtbplanoconta28;
    }

    /**
     * Access method for prodmarca.
     *
     * @return the current value of prodmarca
     */
    public Prodmarca getProdmarca() {
        return prodmarca;
    }

    /**
     * Setter method for prodmarca.
     *
     * @param aProdmarca the new value for prodmarca
     */
    public void setProdmarca(Prodmarca aProdmarca) {
        prodmarca = aProdmarca;
    }

    /**
     * Access method for ctbplanoconta29.
     *
     * @return the current value of ctbplanoconta29
     */
    public Ctbplanoconta getCtbplanoconta29() {
        return ctbplanoconta29;
    }

    /**
     * Setter method for ctbplanoconta29.
     *
     * @param aCtbplanoconta29 the new value for ctbplanoconta29
     */
    public void setCtbplanoconta29(Ctbplanoconta aCtbplanoconta29) {
        ctbplanoconta29 = aCtbplanoconta29;
    }

    /**
     * Access method for ctbplanoconta30.
     *
     * @return the current value of ctbplanoconta30
     */
    public Ctbplanoconta getCtbplanoconta30() {
        return ctbplanoconta30;
    }

    /**
     * Setter method for ctbplanoconta30.
     *
     * @param aCtbplanoconta30 the new value for ctbplanoconta30
     */
    public void setCtbplanoconta30(Ctbplanoconta aCtbplanoconta30) {
        ctbplanoconta30 = aCtbplanoconta30;
    }

    /**
     * Access method for ctbplanoconta31.
     *
     * @return the current value of ctbplanoconta31
     */
    public Ctbplanoconta getCtbplanoconta31() {
        return ctbplanoconta31;
    }

    /**
     * Setter method for ctbplanoconta31.
     *
     * @param aCtbplanoconta31 the new value for ctbplanoconta31
     */
    public void setCtbplanoconta31(Ctbplanoconta aCtbplanoconta31) {
        ctbplanoconta31 = aCtbplanoconta31;
    }

    /**
     * Access method for prodespecie.
     *
     * @return the current value of prodespecie
     */
    public Prodespecie getProdespecie() {
        return prodespecie;
    }

    /**
     * Setter method for prodespecie.
     *
     * @param aProdespecie the new value for prodespecie
     */
    public void setProdespecie(Prodespecie aProdespecie) {
        prodespecie = aProdespecie;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for impcstipi.
     *
     * @return the current value of impcstipi
     */
    public Impcstipi getImpcstipi() {
        return impcstipi;
    }

    /**
     * Setter method for impcstipi.
     *
     * @param aImpcstipi the new value for impcstipi
     */
    public void setImpcstipi(Impcstipi aImpcstipi) {
        impcstipi = aImpcstipi;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Compares the key for this instance with another Impplanofiscal.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impplanofiscal and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impplanofiscal)) {
            return false;
        }
        Impplanofiscal that = (Impplanofiscal) other;
        if (this.getCodimpplanofiscal() != that.getCodimpplanofiscal()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impplanofiscal.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impplanofiscal)) return false;
        return this.equalKeys(other) && ((Impplanofiscal)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpplanofiscal();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impplanofiscal |");
        sb.append(" codimpplanofiscal=").append(getCodimpplanofiscal());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpplanofiscal", Integer.valueOf(getCodimpplanofiscal()));
        return ret;
    }

}
