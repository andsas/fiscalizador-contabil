package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="IMPSIMPLES")
public class Impsimples implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpsimples";

    @Id
    @Column(name="CODIMPSIMPLES", unique=true, nullable=false, precision=10)
    private int codimpsimples;
    @Column(name="DESCIMPSIMPLES", nullable=false, length=250)
    private String descimpsimples;
    @Column(name="TIPOIMPSIMPLES", nullable=false, precision=5)
    private short tipoimpsimples;
    @OneToMany(mappedBy="impsimples")
    private Set<Impsimplesdet> impsimplesdet;

    /** Default constructor. */
    public Impsimples() {
        super();
    }

    /**
     * Access method for codimpsimples.
     *
     * @return the current value of codimpsimples
     */
    public int getCodimpsimples() {
        return codimpsimples;
    }

    /**
     * Setter method for codimpsimples.
     *
     * @param aCodimpsimples the new value for codimpsimples
     */
    public void setCodimpsimples(int aCodimpsimples) {
        codimpsimples = aCodimpsimples;
    }

    /**
     * Access method for descimpsimples.
     *
     * @return the current value of descimpsimples
     */
    public String getDescimpsimples() {
        return descimpsimples;
    }

    /**
     * Setter method for descimpsimples.
     *
     * @param aDescimpsimples the new value for descimpsimples
     */
    public void setDescimpsimples(String aDescimpsimples) {
        descimpsimples = aDescimpsimples;
    }

    /**
     * Access method for tipoimpsimples.
     *
     * @return the current value of tipoimpsimples
     */
    public short getTipoimpsimples() {
        return tipoimpsimples;
    }

    /**
     * Setter method for tipoimpsimples.
     *
     * @param aTipoimpsimples the new value for tipoimpsimples
     */
    public void setTipoimpsimples(short aTipoimpsimples) {
        tipoimpsimples = aTipoimpsimples;
    }

    /**
     * Access method for impsimplesdet.
     *
     * @return the current value of impsimplesdet
     */
    public Set<Impsimplesdet> getImpsimplesdet() {
        return impsimplesdet;
    }

    /**
     * Setter method for impsimplesdet.
     *
     * @param aImpsimplesdet the new value for impsimplesdet
     */
    public void setImpsimplesdet(Set<Impsimplesdet> aImpsimplesdet) {
        impsimplesdet = aImpsimplesdet;
    }

    /**
     * Compares the key for this instance with another Impsimples.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impsimples and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impsimples)) {
            return false;
        }
        Impsimples that = (Impsimples) other;
        if (this.getCodimpsimples() != that.getCodimpsimples()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impsimples.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impsimples)) return false;
        return this.equalKeys(other) && ((Impsimples)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpsimples();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impsimples |");
        sb.append(" codimpsimples=").append(getCodimpsimples());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpsimples", Integer.valueOf(getCodimpsimples()));
        return ret;
    }

}
