package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="IMPSIMPLESDET")
public class Impsimplesdet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimpsimplesdet";

    @Id
    @Column(name="CODIMPSIMPLESDET", unique=true, nullable=false, precision=10)
    private int codimpsimplesdet;
    @Column(name="RECEITABRTDE", nullable=false, precision=15, scale=4)
    private BigDecimal receitabrtde;
    @Column(name="RECEITABRTATE", nullable=false, precision=15, scale=4)
    private BigDecimal receitabrtate;
    @Column(name="ALIQUOTA", length=15)
    private double aliquota;
    @Column(name="PERCIRPJ", length=15)
    private double percirpj;
    @Column(name="PERCCSLL", length=15)
    private double perccsll;
    @Column(name="PERCCOFINS", length=15)
    private double perccofins;
    @Column(name="PERCPIS", length=15)
    private double percpis;
    @Column(name="PERCCPP", length=15)
    private double perccpp;
    @Column(name="PERCICMS", length=15)
    private double percicms;
    @Column(name="PERCIPI", length=15)
    private double percipi;
    @Column(name="PERCISS", length=15)
    private double perciss;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODIMPSIMPLES", nullable=false)
    private Impsimples impsimples;

    /** Default constructor. */
    public Impsimplesdet() {
        super();
    }

    /**
     * Access method for codimpsimplesdet.
     *
     * @return the current value of codimpsimplesdet
     */
    public int getCodimpsimplesdet() {
        return codimpsimplesdet;
    }

    /**
     * Setter method for codimpsimplesdet.
     *
     * @param aCodimpsimplesdet the new value for codimpsimplesdet
     */
    public void setCodimpsimplesdet(int aCodimpsimplesdet) {
        codimpsimplesdet = aCodimpsimplesdet;
    }

    /**
     * Access method for receitabrtde.
     *
     * @return the current value of receitabrtde
     */
    public BigDecimal getReceitabrtde() {
        return receitabrtde;
    }

    /**
     * Setter method for receitabrtde.
     *
     * @param aReceitabrtde the new value for receitabrtde
     */
    public void setReceitabrtde(BigDecimal aReceitabrtde) {
        receitabrtde = aReceitabrtde;
    }

    /**
     * Access method for receitabrtate.
     *
     * @return the current value of receitabrtate
     */
    public BigDecimal getReceitabrtate() {
        return receitabrtate;
    }

    /**
     * Setter method for receitabrtate.
     *
     * @param aReceitabrtate the new value for receitabrtate
     */
    public void setReceitabrtate(BigDecimal aReceitabrtate) {
        receitabrtate = aReceitabrtate;
    }

    /**
     * Access method for aliquota.
     *
     * @return the current value of aliquota
     */
    public double getAliquota() {
        return aliquota;
    }

    /**
     * Setter method for aliquota.
     *
     * @param aAliquota the new value for aliquota
     */
    public void setAliquota(double aAliquota) {
        aliquota = aAliquota;
    }

    /**
     * Access method for percirpj.
     *
     * @return the current value of percirpj
     */
    public double getPercirpj() {
        return percirpj;
    }

    /**
     * Setter method for percirpj.
     *
     * @param aPercirpj the new value for percirpj
     */
    public void setPercirpj(double aPercirpj) {
        percirpj = aPercirpj;
    }

    /**
     * Access method for perccsll.
     *
     * @return the current value of perccsll
     */
    public double getPerccsll() {
        return perccsll;
    }

    /**
     * Setter method for perccsll.
     *
     * @param aPerccsll the new value for perccsll
     */
    public void setPerccsll(double aPerccsll) {
        perccsll = aPerccsll;
    }

    /**
     * Access method for perccofins.
     *
     * @return the current value of perccofins
     */
    public double getPerccofins() {
        return perccofins;
    }

    /**
     * Setter method for perccofins.
     *
     * @param aPerccofins the new value for perccofins
     */
    public void setPerccofins(double aPerccofins) {
        perccofins = aPerccofins;
    }

    /**
     * Access method for percpis.
     *
     * @return the current value of percpis
     */
    public double getPercpis() {
        return percpis;
    }

    /**
     * Setter method for percpis.
     *
     * @param aPercpis the new value for percpis
     */
    public void setPercpis(double aPercpis) {
        percpis = aPercpis;
    }

    /**
     * Access method for perccpp.
     *
     * @return the current value of perccpp
     */
    public double getPerccpp() {
        return perccpp;
    }

    /**
     * Setter method for perccpp.
     *
     * @param aPerccpp the new value for perccpp
     */
    public void setPerccpp(double aPerccpp) {
        perccpp = aPerccpp;
    }

    /**
     * Access method for percicms.
     *
     * @return the current value of percicms
     */
    public double getPercicms() {
        return percicms;
    }

    /**
     * Setter method for percicms.
     *
     * @param aPercicms the new value for percicms
     */
    public void setPercicms(double aPercicms) {
        percicms = aPercicms;
    }

    /**
     * Access method for percipi.
     *
     * @return the current value of percipi
     */
    public double getPercipi() {
        return percipi;
    }

    /**
     * Setter method for percipi.
     *
     * @param aPercipi the new value for percipi
     */
    public void setPercipi(double aPercipi) {
        percipi = aPercipi;
    }

    /**
     * Access method for perciss.
     *
     * @return the current value of perciss
     */
    public double getPerciss() {
        return perciss;
    }

    /**
     * Setter method for perciss.
     *
     * @param aPerciss the new value for perciss
     */
    public void setPerciss(double aPerciss) {
        perciss = aPerciss;
    }

    /**
     * Access method for impsimples.
     *
     * @return the current value of impsimples
     */
    public Impsimples getImpsimples() {
        return impsimples;
    }

    /**
     * Setter method for impsimples.
     *
     * @param aImpsimples the new value for impsimples
     */
    public void setImpsimples(Impsimples aImpsimples) {
        impsimples = aImpsimples;
    }

    /**
     * Compares the key for this instance with another Impsimplesdet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Impsimplesdet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Impsimplesdet)) {
            return false;
        }
        Impsimplesdet that = (Impsimplesdet) other;
        if (this.getCodimpsimplesdet() != that.getCodimpsimplesdet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Impsimplesdet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Impsimplesdet)) return false;
        return this.equalKeys(other) && ((Impsimplesdet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimpsimplesdet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Impsimplesdet |");
        sb.append(" codimpsimplesdet=").append(getCodimpsimplesdet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimpsimplesdet", Integer.valueOf(getCodimpsimplesdet()));
        return ret;
    }

}
