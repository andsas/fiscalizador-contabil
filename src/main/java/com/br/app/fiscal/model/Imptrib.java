package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="IMPTRIB")
public class Imptrib implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimptrib";

    @Id
    @Column(name="CODIMPTRIB", unique=true, nullable=false, precision=10)
    private int codimptrib;
    @Column(name="DESCIMPTRIB", nullable=false, length=250)
    private String descimptrib;
    @Column(name="ORIGEMIMPTRIB", nullable=false, precision=5)
    private short origemimptrib;

    /** Default constructor. */
    public Imptrib() {
        super();
    }

    /**
     * Access method for codimptrib.
     *
     * @return the current value of codimptrib
     */
    public int getCodimptrib() {
        return codimptrib;
    }

    /**
     * Setter method for codimptrib.
     *
     * @param aCodimptrib the new value for codimptrib
     */
    public void setCodimptrib(int aCodimptrib) {
        codimptrib = aCodimptrib;
    }

    /**
     * Access method for descimptrib.
     *
     * @return the current value of descimptrib
     */
    public String getDescimptrib() {
        return descimptrib;
    }

    /**
     * Setter method for descimptrib.
     *
     * @param aDescimptrib the new value for descimptrib
     */
    public void setDescimptrib(String aDescimptrib) {
        descimptrib = aDescimptrib;
    }

    /**
     * Access method for origemimptrib.
     *
     * @return the current value of origemimptrib
     */
    public short getOrigemimptrib() {
        return origemimptrib;
    }

    /**
     * Setter method for origemimptrib.
     *
     * @param aOrigemimptrib the new value for origemimptrib
     */
    public void setOrigemimptrib(short aOrigemimptrib) {
        origemimptrib = aOrigemimptrib;
    }

    /**
     * Compares the key for this instance with another Imptrib.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Imptrib and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Imptrib)) {
            return false;
        }
        Imptrib that = (Imptrib) other;
        if (this.getCodimptrib() != that.getCodimptrib()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Imptrib.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Imptrib)) return false;
        return this.equalKeys(other) && ((Imptrib)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimptrib();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Imptrib |");
        sb.append(" codimptrib=").append(getCodimptrib());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimptrib", Integer.valueOf(getCodimptrib()));
        return ret;
    }

}
