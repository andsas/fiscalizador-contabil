package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="IMPTRIBDET")
public class Imptribdet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codimptribdet";

    @Id
    @Column(name="CODIMPTRIBDET", unique=true, nullable=false, precision=10)
    private int codimptribdet;
    @Column(name="CODIMPTRIB", precision=10)
    private int codimptrib;
    @Column(name="CODCONFUF", length=4)
    private String codconfuf;
    @Column(name="LUCROPRES", precision=15, scale=4)
    private BigDecimal lucropres;
    @Column(name="ISENCAOINDFINAL", precision=5)
    private short isencaoindfinal;
    @Column(name="ALIQISS", length=15)
    private double aliqiss;
    @Column(name="ALIQREDBASEICMSDENTRO", length=15)
    private double aliqredbaseicmsdentro;
    @Column(name="ALIQESPICMSDENTRO", length=15)
    private double aliqespicmsdentro;
    @Column(name="CSTICMSDENTRO", length=4)
    private String csticmsdentro;
    @Column(name="ALIQREDBASEICMSFORA", length=15)
    private double aliqredbaseicmsfora;

    /** Default constructor. */
    public Imptribdet() {
        super();
    }

    /**
     * Access method for codimptribdet.
     *
     * @return the current value of codimptribdet
     */
    public int getCodimptribdet() {
        return codimptribdet;
    }

    /**
     * Setter method for codimptribdet.
     *
     * @param aCodimptribdet the new value for codimptribdet
     */
    public void setCodimptribdet(int aCodimptribdet) {
        codimptribdet = aCodimptribdet;
    }

    /**
     * Access method for codimptrib.
     *
     * @return the current value of codimptrib
     */
    public int getCodimptrib() {
        return codimptrib;
    }

    /**
     * Setter method for codimptrib.
     *
     * @param aCodimptrib the new value for codimptrib
     */
    public void setCodimptrib(int aCodimptrib) {
        codimptrib = aCodimptrib;
    }

    /**
     * Access method for codconfuf.
     *
     * @return the current value of codconfuf
     */
    public String getCodconfuf() {
        return codconfuf;
    }

    /**
     * Setter method for codconfuf.
     *
     * @param aCodconfuf the new value for codconfuf
     */
    public void setCodconfuf(String aCodconfuf) {
        codconfuf = aCodconfuf;
    }

    /**
     * Access method for lucropres.
     *
     * @return the current value of lucropres
     */
    public BigDecimal getLucropres() {
        return lucropres;
    }

    /**
     * Setter method for lucropres.
     *
     * @param aLucropres the new value for lucropres
     */
    public void setLucropres(BigDecimal aLucropres) {
        lucropres = aLucropres;
    }

    /**
     * Access method for isencaoindfinal.
     *
     * @return the current value of isencaoindfinal
     */
    public short getIsencaoindfinal() {
        return isencaoindfinal;
    }

    /**
     * Setter method for isencaoindfinal.
     *
     * @param aIsencaoindfinal the new value for isencaoindfinal
     */
    public void setIsencaoindfinal(short aIsencaoindfinal) {
        isencaoindfinal = aIsencaoindfinal;
    }

    /**
     * Access method for aliqiss.
     *
     * @return the current value of aliqiss
     */
    public double getAliqiss() {
        return aliqiss;
    }

    /**
     * Setter method for aliqiss.
     *
     * @param aAliqiss the new value for aliqiss
     */
    public void setAliqiss(double aAliqiss) {
        aliqiss = aAliqiss;
    }

    /**
     * Access method for aliqredbaseicmsdentro.
     *
     * @return the current value of aliqredbaseicmsdentro
     */
    public double getAliqredbaseicmsdentro() {
        return aliqredbaseicmsdentro;
    }

    /**
     * Setter method for aliqredbaseicmsdentro.
     *
     * @param aAliqredbaseicmsdentro the new value for aliqredbaseicmsdentro
     */
    public void setAliqredbaseicmsdentro(double aAliqredbaseicmsdentro) {
        aliqredbaseicmsdentro = aAliqredbaseicmsdentro;
    }

    /**
     * Access method for aliqespicmsdentro.
     *
     * @return the current value of aliqespicmsdentro
     */
    public double getAliqespicmsdentro() {
        return aliqespicmsdentro;
    }

    /**
     * Setter method for aliqespicmsdentro.
     *
     * @param aAliqespicmsdentro the new value for aliqespicmsdentro
     */
    public void setAliqespicmsdentro(double aAliqespicmsdentro) {
        aliqespicmsdentro = aAliqespicmsdentro;
    }

    /**
     * Access method for csticmsdentro.
     *
     * @return the current value of csticmsdentro
     */
    public String getCsticmsdentro() {
        return csticmsdentro;
    }

    /**
     * Setter method for csticmsdentro.
     *
     * @param aCsticmsdentro the new value for csticmsdentro
     */
    public void setCsticmsdentro(String aCsticmsdentro) {
        csticmsdentro = aCsticmsdentro;
    }

    /**
     * Access method for aliqredbaseicmsfora.
     *
     * @return the current value of aliqredbaseicmsfora
     */
    public double getAliqredbaseicmsfora() {
        return aliqredbaseicmsfora;
    }

    /**
     * Setter method for aliqredbaseicmsfora.
     *
     * @param aAliqredbaseicmsfora the new value for aliqredbaseicmsfora
     */
    public void setAliqredbaseicmsfora(double aAliqredbaseicmsfora) {
        aliqredbaseicmsfora = aAliqredbaseicmsfora;
    }

    /**
     * Compares the key for this instance with another Imptribdet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Imptribdet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Imptribdet)) {
            return false;
        }
        Imptribdet that = (Imptribdet) other;
        if (this.getCodimptribdet() != that.getCodimptribdet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Imptribdet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Imptribdet)) return false;
        return this.equalKeys(other) && ((Imptribdet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodimptribdet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Imptribdet |");
        sb.append(" codimptribdet=").append(getCodimptribdet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codimptribdet", Integer.valueOf(getCodimptribdet()));
        return ret;
    }

}
