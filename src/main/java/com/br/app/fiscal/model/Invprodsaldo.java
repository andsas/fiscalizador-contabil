package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="INVPRODSALDO")
public class Invprodsaldo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codinvprodsaldo";

    @Id
    @Column(name="CODINVPRODSALDO", unique=true, nullable=false, precision=10)
    private int codinvprodsaldo;
    @Column(name="SALDO", precision=15, scale=10)
    private BigDecimal saldo;
    @Column(name="CUSTOMEDIO", precision=15, scale=4)
    private BigDecimal customedio;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODINVSALDO", nullable=false)
    private Invsaldo invsaldo;

    /** Default constructor. */
    public Invprodsaldo() {
        super();
    }

    /**
     * Access method for codinvprodsaldo.
     *
     * @return the current value of codinvprodsaldo
     */
    public int getCodinvprodsaldo() {
        return codinvprodsaldo;
    }

    /**
     * Setter method for codinvprodsaldo.
     *
     * @param aCodinvprodsaldo the new value for codinvprodsaldo
     */
    public void setCodinvprodsaldo(int aCodinvprodsaldo) {
        codinvprodsaldo = aCodinvprodsaldo;
    }

    /**
     * Access method for saldo.
     *
     * @return the current value of saldo
     */
    public BigDecimal getSaldo() {
        return saldo;
    }

    /**
     * Setter method for saldo.
     *
     * @param aSaldo the new value for saldo
     */
    public void setSaldo(BigDecimal aSaldo) {
        saldo = aSaldo;
    }

    /**
     * Access method for customedio.
     *
     * @return the current value of customedio
     */
    public BigDecimal getCustomedio() {
        return customedio;
    }

    /**
     * Setter method for customedio.
     *
     * @param aCustomedio the new value for customedio
     */
    public void setCustomedio(BigDecimal aCustomedio) {
        customedio = aCustomedio;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for invsaldo.
     *
     * @return the current value of invsaldo
     */
    public Invsaldo getInvsaldo() {
        return invsaldo;
    }

    /**
     * Setter method for invsaldo.
     *
     * @param aInvsaldo the new value for invsaldo
     */
    public void setInvsaldo(Invsaldo aInvsaldo) {
        invsaldo = aInvsaldo;
    }

    /**
     * Compares the key for this instance with another Invprodsaldo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Invprodsaldo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Invprodsaldo)) {
            return false;
        }
        Invprodsaldo that = (Invprodsaldo) other;
        if (this.getCodinvprodsaldo() != that.getCodinvprodsaldo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Invprodsaldo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Invprodsaldo)) return false;
        return this.equalKeys(other) && ((Invprodsaldo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodinvprodsaldo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Invprodsaldo |");
        sb.append(" codinvprodsaldo=").append(getCodinvprodsaldo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codinvprodsaldo", Integer.valueOf(getCodinvprodsaldo()));
        return ret;
    }

}
