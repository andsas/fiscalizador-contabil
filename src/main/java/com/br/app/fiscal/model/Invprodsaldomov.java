package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="INVPRODSALDOMOV")
public class Invprodsaldomov implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codinvprodsaldomov";

    @Id
    @Column(name="CODINVPRODSALDOMOV", unique=true, nullable=false, precision=10)
    private int codinvprodsaldomov;
    @Column(name="DESCINVPRODSALDOMOV", nullable=false, length=250)
    private String descinvprodsaldomov;
    @Column(name="TIPOINVPRODSALDOMOV", precision=5)
    private short tipoinvprodsaldomov;
    @Column(name="QUANTIDADE", nullable=false, precision=15, scale=10)
    private BigDecimal quantidade;
    @Column(name="ORIGEM", length=20)
    private String origem;
    @Column(name="CODORIGEM", precision=10)
    private int codorigem;
    @Column(name="VALORUNITARIO", precision=15, scale=4)
    private BigDecimal valorunitario;
    @Column(name="TOTAL", precision=15, scale=4)
    private BigDecimal total;
    @Column(name="DATA")
    private Timestamp data;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODINVSALDO", nullable=false)
    private Invsaldo invsaldo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;

    /** Default constructor. */
    public Invprodsaldomov() {
        super();
    }

    /**
     * Access method for codinvprodsaldomov.
     *
     * @return the current value of codinvprodsaldomov
     */
    public int getCodinvprodsaldomov() {
        return codinvprodsaldomov;
    }

    /**
     * Setter method for codinvprodsaldomov.
     *
     * @param aCodinvprodsaldomov the new value for codinvprodsaldomov
     */
    public void setCodinvprodsaldomov(int aCodinvprodsaldomov) {
        codinvprodsaldomov = aCodinvprodsaldomov;
    }

    /**
     * Access method for descinvprodsaldomov.
     *
     * @return the current value of descinvprodsaldomov
     */
    public String getDescinvprodsaldomov() {
        return descinvprodsaldomov;
    }

    /**
     * Setter method for descinvprodsaldomov.
     *
     * @param aDescinvprodsaldomov the new value for descinvprodsaldomov
     */
    public void setDescinvprodsaldomov(String aDescinvprodsaldomov) {
        descinvprodsaldomov = aDescinvprodsaldomov;
    }

    /**
     * Access method for tipoinvprodsaldomov.
     *
     * @return the current value of tipoinvprodsaldomov
     */
    public short getTipoinvprodsaldomov() {
        return tipoinvprodsaldomov;
    }

    /**
     * Setter method for tipoinvprodsaldomov.
     *
     * @param aTipoinvprodsaldomov the new value for tipoinvprodsaldomov
     */
    public void setTipoinvprodsaldomov(short aTipoinvprodsaldomov) {
        tipoinvprodsaldomov = aTipoinvprodsaldomov;
    }

    /**
     * Access method for quantidade.
     *
     * @return the current value of quantidade
     */
    public BigDecimal getQuantidade() {
        return quantidade;
    }

    /**
     * Setter method for quantidade.
     *
     * @param aQuantidade the new value for quantidade
     */
    public void setQuantidade(BigDecimal aQuantidade) {
        quantidade = aQuantidade;
    }

    /**
     * Access method for origem.
     *
     * @return the current value of origem
     */
    public String getOrigem() {
        return origem;
    }

    /**
     * Setter method for origem.
     *
     * @param aOrigem the new value for origem
     */
    public void setOrigem(String aOrigem) {
        origem = aOrigem;
    }

    /**
     * Access method for codorigem.
     *
     * @return the current value of codorigem
     */
    public int getCodorigem() {
        return codorigem;
    }

    /**
     * Setter method for codorigem.
     *
     * @param aCodorigem the new value for codorigem
     */
    public void setCodorigem(int aCodorigem) {
        codorigem = aCodorigem;
    }

    /**
     * Access method for valorunitario.
     *
     * @return the current value of valorunitario
     */
    public BigDecimal getValorunitario() {
        return valorunitario;
    }

    /**
     * Setter method for valorunitario.
     *
     * @param aValorunitario the new value for valorunitario
     */
    public void setValorunitario(BigDecimal aValorunitario) {
        valorunitario = aValorunitario;
    }

    /**
     * Access method for total.
     *
     * @return the current value of total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Setter method for total.
     *
     * @param aTotal the new value for total
     */
    public void setTotal(BigDecimal aTotal) {
        total = aTotal;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for invsaldo.
     *
     * @return the current value of invsaldo
     */
    public Invsaldo getInvsaldo() {
        return invsaldo;
    }

    /**
     * Setter method for invsaldo.
     *
     * @param aInvsaldo the new value for invsaldo
     */
    public void setInvsaldo(Invsaldo aInvsaldo) {
        invsaldo = aInvsaldo;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Invprodsaldomov.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Invprodsaldomov and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Invprodsaldomov)) {
            return false;
        }
        Invprodsaldomov that = (Invprodsaldomov) other;
        if (this.getCodinvprodsaldomov() != that.getCodinvprodsaldomov()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Invprodsaldomov.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Invprodsaldomov)) return false;
        return this.equalKeys(other) && ((Invprodsaldomov)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodinvprodsaldomov();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Invprodsaldomov |");
        sb.append(" codinvprodsaldomov=").append(getCodinvprodsaldomov());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codinvprodsaldomov", Integer.valueOf(getCodinvprodsaldomov()));
        return ret;
    }

}
