package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="INVSALDO")
public class Invsaldo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codinvsaldo";

    @Id
    @Column(name="CODINVSALDO", unique=true, nullable=false, precision=10)
    private int codinvsaldo;
    @Column(name="DESCINVSALDO", nullable=false, length=250)
    private String descinvsaldo;
    @OneToMany(mappedBy="invsaldo")
    private Set<Invprodsaldo> invprodsaldo;
    @OneToMany(mappedBy="invsaldo")
    private Set<Invprodsaldomov> invprodsaldomov;
    @OneToMany(mappedBy="invsaldo")
    private Set<Optipo> optipo;
    @OneToMany(mappedBy="invsaldo")
    private Set<Optiposaldo> optiposaldo;
    @OneToMany(mappedBy="invsaldo")
    private Set<Prodminmax> prodminmax;

    /** Default constructor. */
    public Invsaldo() {
        super();
    }

    /**
     * Access method for codinvsaldo.
     *
     * @return the current value of codinvsaldo
     */
    public int getCodinvsaldo() {
        return codinvsaldo;
    }

    /**
     * Setter method for codinvsaldo.
     *
     * @param aCodinvsaldo the new value for codinvsaldo
     */
    public void setCodinvsaldo(int aCodinvsaldo) {
        codinvsaldo = aCodinvsaldo;
    }

    /**
     * Access method for descinvsaldo.
     *
     * @return the current value of descinvsaldo
     */
    public String getDescinvsaldo() {
        return descinvsaldo;
    }

    /**
     * Setter method for descinvsaldo.
     *
     * @param aDescinvsaldo the new value for descinvsaldo
     */
    public void setDescinvsaldo(String aDescinvsaldo) {
        descinvsaldo = aDescinvsaldo;
    }

    /**
     * Access method for invprodsaldo.
     *
     * @return the current value of invprodsaldo
     */
    public Set<Invprodsaldo> getInvprodsaldo() {
        return invprodsaldo;
    }

    /**
     * Setter method for invprodsaldo.
     *
     * @param aInvprodsaldo the new value for invprodsaldo
     */
    public void setInvprodsaldo(Set<Invprodsaldo> aInvprodsaldo) {
        invprodsaldo = aInvprodsaldo;
    }

    /**
     * Access method for invprodsaldomov.
     *
     * @return the current value of invprodsaldomov
     */
    public Set<Invprodsaldomov> getInvprodsaldomov() {
        return invprodsaldomov;
    }

    /**
     * Setter method for invprodsaldomov.
     *
     * @param aInvprodsaldomov the new value for invprodsaldomov
     */
    public void setInvprodsaldomov(Set<Invprodsaldomov> aInvprodsaldomov) {
        invprodsaldomov = aInvprodsaldomov;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Set<Optipo> getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Set<Optipo> aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Access method for optiposaldo.
     *
     * @return the current value of optiposaldo
     */
    public Set<Optiposaldo> getOptiposaldo() {
        return optiposaldo;
    }

    /**
     * Setter method for optiposaldo.
     *
     * @param aOptiposaldo the new value for optiposaldo
     */
    public void setOptiposaldo(Set<Optiposaldo> aOptiposaldo) {
        optiposaldo = aOptiposaldo;
    }

    /**
     * Access method for prodminmax.
     *
     * @return the current value of prodminmax
     */
    public Set<Prodminmax> getProdminmax() {
        return prodminmax;
    }

    /**
     * Setter method for prodminmax.
     *
     * @param aProdminmax the new value for prodminmax
     */
    public void setProdminmax(Set<Prodminmax> aProdminmax) {
        prodminmax = aProdminmax;
    }

    /**
     * Compares the key for this instance with another Invsaldo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Invsaldo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Invsaldo)) {
            return false;
        }
        Invsaldo that = (Invsaldo) other;
        if (this.getCodinvsaldo() != that.getCodinvsaldo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Invsaldo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Invsaldo)) return false;
        return this.equalKeys(other) && ((Invsaldo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodinvsaldo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Invsaldo |");
        sb.append(" codinvsaldo=").append(getCodinvsaldo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codinvsaldo", Integer.valueOf(getCodinvsaldo()));
        return ret;
    }

}
