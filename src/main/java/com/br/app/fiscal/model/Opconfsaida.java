package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPCONFSAIDA")
public class Opconfsaida implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopconfsaida";

    @Id
    @Column(name="CODOPCONFSAIDA", unique=true, nullable=false, precision=10)
    private int codopconfsaida;
    @Column(name="CDQUANTIDADE", precision=10)
    private int cdquantidade;
    @Column(name="CDUNITARIO", precision=10)
    private int cdunitario;
    @Column(name="CDTOTAL", precision=10)
    private int cdtotal;
    @Column(name="CDIMPOSTOSVALOR", precision=10)
    private int cdimpostosvalor;
    @Column(name="CDIMPOSTOSPERC", precision=10)
    private int cdimpostosperc;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="CODOPTIPOPEDIDOVENDA")
    private Optipo optipo;
    @ManyToOne
    @JoinColumn(name="CODOPTIPOORCAMENTOVENDA")
    private Optipo optipo2;
    @ManyToOne
    @JoinColumn(name="CODOPTIPOVENDA")
    private Optipo optipo3;
    @ManyToOne
    @JoinColumn(name="CODOPDOCSERIENFSE")
    private Opdocserie opdocserie;
    @ManyToOne
    @JoinColumn(name="ORCRAPIDOCODAGTIPO")
    private Agtipo agtipo;
    @ManyToOne
    @JoinColumn(name="CODOPOPERACAOATENDIMENTO")
    private Opoperacao opoperacao;

    /** Default constructor. */
    public Opconfsaida() {
        super();
    }

    /**
     * Access method for codopconfsaida.
     *
     * @return the current value of codopconfsaida
     */
    public int getCodopconfsaida() {
        return codopconfsaida;
    }

    /**
     * Setter method for codopconfsaida.
     *
     * @param aCodopconfsaida the new value for codopconfsaida
     */
    public void setCodopconfsaida(int aCodopconfsaida) {
        codopconfsaida = aCodopconfsaida;
    }

    /**
     * Access method for cdquantidade.
     *
     * @return the current value of cdquantidade
     */
    public int getCdquantidade() {
        return cdquantidade;
    }

    /**
     * Setter method for cdquantidade.
     *
     * @param aCdquantidade the new value for cdquantidade
     */
    public void setCdquantidade(int aCdquantidade) {
        cdquantidade = aCdquantidade;
    }

    /**
     * Access method for cdunitario.
     *
     * @return the current value of cdunitario
     */
    public int getCdunitario() {
        return cdunitario;
    }

    /**
     * Setter method for cdunitario.
     *
     * @param aCdunitario the new value for cdunitario
     */
    public void setCdunitario(int aCdunitario) {
        cdunitario = aCdunitario;
    }

    /**
     * Access method for cdtotal.
     *
     * @return the current value of cdtotal
     */
    public int getCdtotal() {
        return cdtotal;
    }

    /**
     * Setter method for cdtotal.
     *
     * @param aCdtotal the new value for cdtotal
     */
    public void setCdtotal(int aCdtotal) {
        cdtotal = aCdtotal;
    }

    /**
     * Access method for cdimpostosvalor.
     *
     * @return the current value of cdimpostosvalor
     */
    public int getCdimpostosvalor() {
        return cdimpostosvalor;
    }

    /**
     * Setter method for cdimpostosvalor.
     *
     * @param aCdimpostosvalor the new value for cdimpostosvalor
     */
    public void setCdimpostosvalor(int aCdimpostosvalor) {
        cdimpostosvalor = aCdimpostosvalor;
    }

    /**
     * Access method for cdimpostosperc.
     *
     * @return the current value of cdimpostosperc
     */
    public int getCdimpostosperc() {
        return cdimpostosperc;
    }

    /**
     * Setter method for cdimpostosperc.
     *
     * @param aCdimpostosperc the new value for cdimpostosperc
     */
    public void setCdimpostosperc(int aCdimpostosperc) {
        cdimpostosperc = aCdimpostosperc;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Optipo getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Optipo aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Access method for optipo2.
     *
     * @return the current value of optipo2
     */
    public Optipo getOptipo2() {
        return optipo2;
    }

    /**
     * Setter method for optipo2.
     *
     * @param aOptipo2 the new value for optipo2
     */
    public void setOptipo2(Optipo aOptipo2) {
        optipo2 = aOptipo2;
    }

    /**
     * Access method for optipo3.
     *
     * @return the current value of optipo3
     */
    public Optipo getOptipo3() {
        return optipo3;
    }

    /**
     * Setter method for optipo3.
     *
     * @param aOptipo3 the new value for optipo3
     */
    public void setOptipo3(Optipo aOptipo3) {
        optipo3 = aOptipo3;
    }

    /**
     * Access method for opdocserie.
     *
     * @return the current value of opdocserie
     */
    public Opdocserie getOpdocserie() {
        return opdocserie;
    }

    /**
     * Setter method for opdocserie.
     *
     * @param aOpdocserie the new value for opdocserie
     */
    public void setOpdocserie(Opdocserie aOpdocserie) {
        opdocserie = aOpdocserie;
    }

    /**
     * Access method for agtipo.
     *
     * @return the current value of agtipo
     */
    public Agtipo getAgtipo() {
        return agtipo;
    }

    /**
     * Setter method for agtipo.
     *
     * @param aAgtipo the new value for agtipo
     */
    public void setAgtipo(Agtipo aAgtipo) {
        agtipo = aAgtipo;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Opoperacao getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Opoperacao aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Compares the key for this instance with another Opconfsaida.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opconfsaida and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opconfsaida)) {
            return false;
        }
        Opconfsaida that = (Opconfsaida) other;
        if (this.getCodopconfsaida() != that.getCodopconfsaida()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opconfsaida.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opconfsaida)) return false;
        return this.equalKeys(other) && ((Opconfsaida)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodopconfsaida();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opconfsaida |");
        sb.append(" codopconfsaida=").append(getCodopconfsaida());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopconfsaida", Integer.valueOf(getCodopconfsaida()));
        return ret;
    }

}
