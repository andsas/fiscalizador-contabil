package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPDOCMODELO")
public class Opdocmodelo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopdocmodelo";

    @Id
    @Column(name="CODOPDOCMODELO", unique=true, nullable=false, length=20)
    private String codopdocmodelo;
    @Column(name="DESCOPDOCMODELO", nullable=false, length=250)
    private String descopdocmodelo;
    @Column(name="APLICACAO")
    private String aplicacao;
    @ManyToOne
    @JoinColumn(name="CODIMPOBS")
    private Impobs impobs;
    @OneToMany(mappedBy="opdocmodelo")
    private Set<Opdocserie> opdocserie;

    /** Default constructor. */
    public Opdocmodelo() {
        super();
    }

    /**
     * Access method for codopdocmodelo.
     *
     * @return the current value of codopdocmodelo
     */
    public String getCodopdocmodelo() {
        return codopdocmodelo;
    }

    /**
     * Setter method for codopdocmodelo.
     *
     * @param aCodopdocmodelo the new value for codopdocmodelo
     */
    public void setCodopdocmodelo(String aCodopdocmodelo) {
        codopdocmodelo = aCodopdocmodelo;
    }

    /**
     * Access method for descopdocmodelo.
     *
     * @return the current value of descopdocmodelo
     */
    public String getDescopdocmodelo() {
        return descopdocmodelo;
    }

    /**
     * Setter method for descopdocmodelo.
     *
     * @param aDescopdocmodelo the new value for descopdocmodelo
     */
    public void setDescopdocmodelo(String aDescopdocmodelo) {
        descopdocmodelo = aDescopdocmodelo;
    }

    /**
     * Access method for aplicacao.
     *
     * @return the current value of aplicacao
     */
    public String getAplicacao() {
        return aplicacao;
    }

    /**
     * Setter method for aplicacao.
     *
     * @param aAplicacao the new value for aplicacao
     */
    public void setAplicacao(String aAplicacao) {
        aplicacao = aAplicacao;
    }

    /**
     * Access method for impobs.
     *
     * @return the current value of impobs
     */
    public Impobs getImpobs() {
        return impobs;
    }

    /**
     * Setter method for impobs.
     *
     * @param aImpobs the new value for impobs
     */
    public void setImpobs(Impobs aImpobs) {
        impobs = aImpobs;
    }

    /**
     * Access method for opdocserie.
     *
     * @return the current value of opdocserie
     */
    public Set<Opdocserie> getOpdocserie() {
        return opdocserie;
    }

    /**
     * Setter method for opdocserie.
     *
     * @param aOpdocserie the new value for opdocserie
     */
    public void setOpdocserie(Set<Opdocserie> aOpdocserie) {
        opdocserie = aOpdocserie;
    }

    /**
     * Compares the key for this instance with another Opdocmodelo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opdocmodelo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opdocmodelo)) {
            return false;
        }
        Opdocmodelo that = (Opdocmodelo) other;
        Object myCodopdocmodelo = this.getCodopdocmodelo();
        Object yourCodopdocmodelo = that.getCodopdocmodelo();
        if (myCodopdocmodelo==null ? yourCodopdocmodelo!=null : !myCodopdocmodelo.equals(yourCodopdocmodelo)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opdocmodelo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opdocmodelo)) return false;
        return this.equalKeys(other) && ((Opdocmodelo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodopdocmodelo() == null) {
            i = 0;
        } else {
            i = getCodopdocmodelo().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opdocmodelo |");
        sb.append(" codopdocmodelo=").append(getCodopdocmodelo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopdocmodelo", getCodopdocmodelo());
        return ret;
    }

}
