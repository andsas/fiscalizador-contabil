package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPDOCSERIE")
public class Opdocserie implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopdocserie";

    @Id
    @Column(name="CODOPDOCSERIE", unique=true, nullable=false, length=20)
    private String codopdocserie;
    @Column(name="DESCOPDOCSERIE", nullable=false, length=250)
    private String descopdocserie;
    @Column(name="NUMINI", nullable=false, precision=10)
    private int numini;
    @Column(name="NUMFIM", precision=10)
    private int numfim;
    @Column(name="DATAVALIDADE")
    private Date datavalidade;
    @Column(name="TIPOOPDOCSERIEAVISO", precision=5)
    private short tipoopdocserieaviso;
    @Column(name="NUMATUAL", precision=10)
    private int numatual;
    @Column(name="PRINCIPAL", precision=5)
    private short principal;
    @OneToMany(mappedBy="opdocserie")
    private Set<Opconfsaida> opconfsaida;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPDOCMODELO", nullable=false)
    private Opdocmodelo opdocmodelo;
    @OneToMany(mappedBy="opdocserie")
    private Set<Opnfeinut> opnfeinut;
    @OneToMany(mappedBy="opdocserie")
    private Set<Optransacao> optransacao;

    /** Default constructor. */
    public Opdocserie() {
        super();
    }

    /**
     * Access method for codopdocserie.
     *
     * @return the current value of codopdocserie
     */
    public String getCodopdocserie() {
        return codopdocserie;
    }

    /**
     * Setter method for codopdocserie.
     *
     * @param aCodopdocserie the new value for codopdocserie
     */
    public void setCodopdocserie(String aCodopdocserie) {
        codopdocserie = aCodopdocserie;
    }

    /**
     * Access method for descopdocserie.
     *
     * @return the current value of descopdocserie
     */
    public String getDescopdocserie() {
        return descopdocserie;
    }

    /**
     * Setter method for descopdocserie.
     *
     * @param aDescopdocserie the new value for descopdocserie
     */
    public void setDescopdocserie(String aDescopdocserie) {
        descopdocserie = aDescopdocserie;
    }

    /**
     * Access method for numini.
     *
     * @return the current value of numini
     */
    public int getNumini() {
        return numini;
    }

    /**
     * Setter method for numini.
     *
     * @param aNumini the new value for numini
     */
    public void setNumini(int aNumini) {
        numini = aNumini;
    }

    /**
     * Access method for numfim.
     *
     * @return the current value of numfim
     */
    public int getNumfim() {
        return numfim;
    }

    /**
     * Setter method for numfim.
     *
     * @param aNumfim the new value for numfim
     */
    public void setNumfim(int aNumfim) {
        numfim = aNumfim;
    }

    /**
     * Access method for datavalidade.
     *
     * @return the current value of datavalidade
     */
    public Date getDatavalidade() {
        return datavalidade;
    }

    /**
     * Setter method for datavalidade.
     *
     * @param aDatavalidade the new value for datavalidade
     */
    public void setDatavalidade(Date aDatavalidade) {
        datavalidade = aDatavalidade;
    }

    /**
     * Access method for tipoopdocserieaviso.
     *
     * @return the current value of tipoopdocserieaviso
     */
    public short getTipoopdocserieaviso() {
        return tipoopdocserieaviso;
    }

    /**
     * Setter method for tipoopdocserieaviso.
     *
     * @param aTipoopdocserieaviso the new value for tipoopdocserieaviso
     */
    public void setTipoopdocserieaviso(short aTipoopdocserieaviso) {
        tipoopdocserieaviso = aTipoopdocserieaviso;
    }

    /**
     * Access method for numatual.
     *
     * @return the current value of numatual
     */
    public int getNumatual() {
        return numatual;
    }

    /**
     * Setter method for numatual.
     *
     * @param aNumatual the new value for numatual
     */
    public void setNumatual(int aNumatual) {
        numatual = aNumatual;
    }

    /**
     * Access method for principal.
     *
     * @return the current value of principal
     */
    public short getPrincipal() {
        return principal;
    }

    /**
     * Setter method for principal.
     *
     * @param aPrincipal the new value for principal
     */
    public void setPrincipal(short aPrincipal) {
        principal = aPrincipal;
    }

    /**
     * Access method for opconfsaida.
     *
     * @return the current value of opconfsaida
     */
    public Set<Opconfsaida> getOpconfsaida() {
        return opconfsaida;
    }

    /**
     * Setter method for opconfsaida.
     *
     * @param aOpconfsaida the new value for opconfsaida
     */
    public void setOpconfsaida(Set<Opconfsaida> aOpconfsaida) {
        opconfsaida = aOpconfsaida;
    }

    /**
     * Access method for opdocmodelo.
     *
     * @return the current value of opdocmodelo
     */
    public Opdocmodelo getOpdocmodelo() {
        return opdocmodelo;
    }

    /**
     * Setter method for opdocmodelo.
     *
     * @param aOpdocmodelo the new value for opdocmodelo
     */
    public void setOpdocmodelo(Opdocmodelo aOpdocmodelo) {
        opdocmodelo = aOpdocmodelo;
    }

    /**
     * Access method for opnfeinut.
     *
     * @return the current value of opnfeinut
     */
    public Set<Opnfeinut> getOpnfeinut() {
        return opnfeinut;
    }

    /**
     * Setter method for opnfeinut.
     *
     * @param aOpnfeinut the new value for opnfeinut
     */
    public void setOpnfeinut(Set<Opnfeinut> aOpnfeinut) {
        opnfeinut = aOpnfeinut;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Compares the key for this instance with another Opdocserie.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opdocserie and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opdocserie)) {
            return false;
        }
        Opdocserie that = (Opdocserie) other;
        Object myCodopdocserie = this.getCodopdocserie();
        Object yourCodopdocserie = that.getCodopdocserie();
        if (myCodopdocserie==null ? yourCodopdocserie!=null : !myCodopdocserie.equals(yourCodopdocserie)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opdocserie.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opdocserie)) return false;
        return this.equalKeys(other) && ((Opdocserie)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodopdocserie() == null) {
            i = 0;
        } else {
            i = getCodopdocserie().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opdocserie |");
        sb.append(" codopdocserie=").append(getCodopdocserie());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopdocserie", getCodopdocserie());
        return ret;
    }

}
