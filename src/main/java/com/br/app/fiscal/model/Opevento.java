package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPEVENTO")
public class Opevento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopevento";

    @Id
    @Column(name="CODOPEVENTO", unique=true, nullable=false, precision=10)
    private int codopevento;
    @Column(name="DESCOPEVENTO", nullable=false, length=250)
    private String descopevento;
    @Column(name="GERARTRANSACAO", precision=5)
    private short gerartransacao;
    @Column(name="FINALIZARTRANSACAO", precision=5)
    private short finalizartransacao;
    @Column(name="NFEENVIAR", precision=5)
    private short nfeenviar;
    @Column(name="NFECANCELAR", precision=5)
    private short nfecancelar;
    @Column(name="COROPEVENTO", precision=10)
    private int coropevento;
    @Column(name="NUMERO", precision=10)
    private int numero;
    @ManyToOne
    @JoinColumn(name="CODOPOPERACAOGERAR")
    private Opoperacao opoperacao;
    @ManyToOne
    @JoinColumn(name="CODOPTIPO")
    private Optipo optipo;

    /** Default constructor. */
    public Opevento() {
        super();
    }

    /**
     * Access method for codopevento.
     *
     * @return the current value of codopevento
     */
    public int getCodopevento() {
        return codopevento;
    }

    /**
     * Setter method for codopevento.
     *
     * @param aCodopevento the new value for codopevento
     */
    public void setCodopevento(int aCodopevento) {
        codopevento = aCodopevento;
    }

    /**
     * Access method for descopevento.
     *
     * @return the current value of descopevento
     */
    public String getDescopevento() {
        return descopevento;
    }

    /**
     * Setter method for descopevento.
     *
     * @param aDescopevento the new value for descopevento
     */
    public void setDescopevento(String aDescopevento) {
        descopevento = aDescopevento;
    }

    /**
     * Access method for gerartransacao.
     *
     * @return the current value of gerartransacao
     */
    public short getGerartransacao() {
        return gerartransacao;
    }

    /**
     * Setter method for gerartransacao.
     *
     * @param aGerartransacao the new value for gerartransacao
     */
    public void setGerartransacao(short aGerartransacao) {
        gerartransacao = aGerartransacao;
    }

    /**
     * Access method for finalizartransacao.
     *
     * @return the current value of finalizartransacao
     */
    public short getFinalizartransacao() {
        return finalizartransacao;
    }

    /**
     * Setter method for finalizartransacao.
     *
     * @param aFinalizartransacao the new value for finalizartransacao
     */
    public void setFinalizartransacao(short aFinalizartransacao) {
        finalizartransacao = aFinalizartransacao;
    }

    /**
     * Access method for nfeenviar.
     *
     * @return the current value of nfeenviar
     */
    public short getNfeenviar() {
        return nfeenviar;
    }

    /**
     * Setter method for nfeenviar.
     *
     * @param aNfeenviar the new value for nfeenviar
     */
    public void setNfeenviar(short aNfeenviar) {
        nfeenviar = aNfeenviar;
    }

    /**
     * Access method for nfecancelar.
     *
     * @return the current value of nfecancelar
     */
    public short getNfecancelar() {
        return nfecancelar;
    }

    /**
     * Setter method for nfecancelar.
     *
     * @param aNfecancelar the new value for nfecancelar
     */
    public void setNfecancelar(short aNfecancelar) {
        nfecancelar = aNfecancelar;
    }

    /**
     * Access method for coropevento.
     *
     * @return the current value of coropevento
     */
    public int getCoropevento() {
        return coropevento;
    }

    /**
     * Setter method for coropevento.
     *
     * @param aCoropevento the new value for coropevento
     */
    public void setCoropevento(int aCoropevento) {
        coropevento = aCoropevento;
    }

    /**
     * Access method for numero.
     *
     * @return the current value of numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Setter method for numero.
     *
     * @param aNumero the new value for numero
     */
    public void setNumero(int aNumero) {
        numero = aNumero;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Opoperacao getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Opoperacao aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Optipo getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Optipo aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Compares the key for this instance with another Opevento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opevento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opevento)) {
            return false;
        }
        Opevento that = (Opevento) other;
        if (this.getCodopevento() != that.getCodopevento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opevento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opevento)) return false;
        return this.equalKeys(other) && ((Opevento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodopevento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opevento |");
        sb.append(" codopevento=").append(getCodopevento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopevento", Integer.valueOf(getCodopevento()));
        return ret;
    }

}
