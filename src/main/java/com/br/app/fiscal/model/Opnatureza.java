package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="OPNATUREZA")
public class Opnatureza implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopnatureza";

    @Id
    @Column(name="CODOPNATUREZA", unique=true, nullable=false, precision=10)
    private int codopnatureza;
    @Column(name="DESCOPNATUREZA", nullable=false, length=250)
    private String descopnatureza;
    @Column(name="TIPOOPNATUREZA", nullable=false, precision=5)
    private short tipoopnatureza;
    @OneToMany(mappedBy="opnatureza")
    private Set<Optipo> optipo;

    /** Default constructor. */
    public Opnatureza() {
        super();
    }

    /**
     * Access method for codopnatureza.
     *
     * @return the current value of codopnatureza
     */
    public int getCodopnatureza() {
        return codopnatureza;
    }

    /**
     * Setter method for codopnatureza.
     *
     * @param aCodopnatureza the new value for codopnatureza
     */
    public void setCodopnatureza(int aCodopnatureza) {
        codopnatureza = aCodopnatureza;
    }

    /**
     * Access method for descopnatureza.
     *
     * @return the current value of descopnatureza
     */
    public String getDescopnatureza() {
        return descopnatureza;
    }

    /**
     * Setter method for descopnatureza.
     *
     * @param aDescopnatureza the new value for descopnatureza
     */
    public void setDescopnatureza(String aDescopnatureza) {
        descopnatureza = aDescopnatureza;
    }

    /**
     * Access method for tipoopnatureza.
     *
     * @return the current value of tipoopnatureza
     */
    public short getTipoopnatureza() {
        return tipoopnatureza;
    }

    /**
     * Setter method for tipoopnatureza.
     *
     * @param aTipoopnatureza the new value for tipoopnatureza
     */
    public void setTipoopnatureza(short aTipoopnatureza) {
        tipoopnatureza = aTipoopnatureza;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Set<Optipo> getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Set<Optipo> aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Compares the key for this instance with another Opnatureza.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opnatureza and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opnatureza)) {
            return false;
        }
        Opnatureza that = (Opnatureza) other;
        if (this.getCodopnatureza() != that.getCodopnatureza()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opnatureza.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opnatureza)) return false;
        return this.equalKeys(other) && ((Opnatureza)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodopnatureza();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opnatureza |");
        sb.append(" codopnatureza=").append(getCodopnatureza());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopnatureza", Integer.valueOf(getCodopnatureza()));
        return ret;
    }

}
