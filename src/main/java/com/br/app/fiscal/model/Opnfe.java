package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPNFE")
public class Opnfe implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopnfe";

    @Id
    @Column(name="CODOPNFE", unique=true, nullable=false, length=60)
    private String codopnfe;
    @Column(name="RECIBO", nullable=false, length=40)
    private String recibo;
    @Column(name="CSTAT", precision=10)
    private int cstat;
    @Column(name="XMLENVIO")
    private String xmlenvio;
    @Column(name="XMLCANCELAMENTO")
    private String xmlcancelamento;
    @Column(name="XMLCONSULTA")
    private String xmlconsulta;
    @Column(name="TTT")
    private byte[] ttt;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAO")
    private Optransacao optransacao;
    @OneToMany(mappedBy="opnfe")
    private Set<Opnfedet> opnfedet;

    /** Default constructor. */
    public Opnfe() {
        super();
    }

    /**
     * Access method for codopnfe.
     *
     * @return the current value of codopnfe
     */
    public String getCodopnfe() {
        return codopnfe;
    }

    /**
     * Setter method for codopnfe.
     *
     * @param aCodopnfe the new value for codopnfe
     */
    public void setCodopnfe(String aCodopnfe) {
        codopnfe = aCodopnfe;
    }

    /**
     * Access method for recibo.
     *
     * @return the current value of recibo
     */
    public String getRecibo() {
        return recibo;
    }

    /**
     * Setter method for recibo.
     *
     * @param aRecibo the new value for recibo
     */
    public void setRecibo(String aRecibo) {
        recibo = aRecibo;
    }

    /**
     * Access method for cstat.
     *
     * @return the current value of cstat
     */
    public int getCstat() {
        return cstat;
    }

    /**
     * Setter method for cstat.
     *
     * @param aCstat the new value for cstat
     */
    public void setCstat(int aCstat) {
        cstat = aCstat;
    }

    /**
     * Access method for xmlenvio.
     *
     * @return the current value of xmlenvio
     */
    public String getXmlenvio() {
        return xmlenvio;
    }

    /**
     * Setter method for xmlenvio.
     *
     * @param aXmlenvio the new value for xmlenvio
     */
    public void setXmlenvio(String aXmlenvio) {
        xmlenvio = aXmlenvio;
    }

    /**
     * Access method for xmlcancelamento.
     *
     * @return the current value of xmlcancelamento
     */
    public String getXmlcancelamento() {
        return xmlcancelamento;
    }

    /**
     * Setter method for xmlcancelamento.
     *
     * @param aXmlcancelamento the new value for xmlcancelamento
     */
    public void setXmlcancelamento(String aXmlcancelamento) {
        xmlcancelamento = aXmlcancelamento;
    }

    /**
     * Access method for xmlconsulta.
     *
     * @return the current value of xmlconsulta
     */
    public String getXmlconsulta() {
        return xmlconsulta;
    }

    /**
     * Setter method for xmlconsulta.
     *
     * @param aXmlconsulta the new value for xmlconsulta
     */
    public void setXmlconsulta(String aXmlconsulta) {
        xmlconsulta = aXmlconsulta;
    }

    /**
     * Access method for ttt.
     *
     * @return the current value of ttt
     */
    public byte[] getTtt() {
        return ttt;
    }

    /**
     * Setter method for ttt.
     *
     * @param aTtt the new value for ttt
     */
    public void setTtt(byte[] aTtt) {
        ttt = aTtt;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for opnfedet.
     *
     * @return the current value of opnfedet
     */
    public Set<Opnfedet> getOpnfedet() {
        return opnfedet;
    }

    /**
     * Setter method for opnfedet.
     *
     * @param aOpnfedet the new value for opnfedet
     */
    public void setOpnfedet(Set<Opnfedet> aOpnfedet) {
        opnfedet = aOpnfedet;
    }

    /**
     * Compares the key for this instance with another Opnfe.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opnfe and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opnfe)) {
            return false;
        }
        Opnfe that = (Opnfe) other;
        Object myCodopnfe = this.getCodopnfe();
        Object yourCodopnfe = that.getCodopnfe();
        if (myCodopnfe==null ? yourCodopnfe!=null : !myCodopnfe.equals(yourCodopnfe)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opnfe.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opnfe)) return false;
        return this.equalKeys(other) && ((Opnfe)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodopnfe() == null) {
            i = 0;
        } else {
            i = getCodopnfe().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opnfe |");
        sb.append(" codopnfe=").append(getCodopnfe());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopnfe", getCodopnfe());
        return ret;
    }

}
