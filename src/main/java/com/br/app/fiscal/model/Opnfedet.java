package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPNFEDET")
public class Opnfedet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopnfedet";

    @Id
    @Column(name="CODOPNFEDET", unique=true, nullable=false, precision=10)
    private int codopnfedet;
    @Column(name="TIPOOPNFEDET", nullable=false, precision=5)
    private short tipoopnfedet;
    @Column(name="DESCOPNFEDET", nullable=false, length=250)
    private String descopnfedet;
    @Column(name="XML", nullable=false)
    private String xml;
    @Column(name="CSTAT", precision=10)
    private int cstat;
    @Column(name="MSG")
    private String msg;
    @Column(name="CONTROLELOTE", length=40)
    private String controlelote;
    @Column(name="PROTOCOLO", length=60)
    private String protocolo;
    @Column(name="CANCJUSTIFICATIVA", length=250)
    private String cancjustificativa;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPNFE", nullable=false)
    private Opnfe opnfe;

    /** Default constructor. */
    public Opnfedet() {
        super();
    }

    /**
     * Access method for codopnfedet.
     *
     * @return the current value of codopnfedet
     */
    public int getCodopnfedet() {
        return codopnfedet;
    }

    /**
     * Setter method for codopnfedet.
     *
     * @param aCodopnfedet the new value for codopnfedet
     */
    public void setCodopnfedet(int aCodopnfedet) {
        codopnfedet = aCodopnfedet;
    }

    /**
     * Access method for tipoopnfedet.
     *
     * @return the current value of tipoopnfedet
     */
    public short getTipoopnfedet() {
        return tipoopnfedet;
    }

    /**
     * Setter method for tipoopnfedet.
     *
     * @param aTipoopnfedet the new value for tipoopnfedet
     */
    public void setTipoopnfedet(short aTipoopnfedet) {
        tipoopnfedet = aTipoopnfedet;
    }

    /**
     * Access method for descopnfedet.
     *
     * @return the current value of descopnfedet
     */
    public String getDescopnfedet() {
        return descopnfedet;
    }

    /**
     * Setter method for descopnfedet.
     *
     * @param aDescopnfedet the new value for descopnfedet
     */
    public void setDescopnfedet(String aDescopnfedet) {
        descopnfedet = aDescopnfedet;
    }

    /**
     * Access method for xml.
     *
     * @return the current value of xml
     */
    public String getXml() {
        return xml;
    }

    /**
     * Setter method for xml.
     *
     * @param aXml the new value for xml
     */
    public void setXml(String aXml) {
        xml = aXml;
    }

    /**
     * Access method for cstat.
     *
     * @return the current value of cstat
     */
    public int getCstat() {
        return cstat;
    }

    /**
     * Setter method for cstat.
     *
     * @param aCstat the new value for cstat
     */
    public void setCstat(int aCstat) {
        cstat = aCstat;
    }

    /**
     * Access method for msg.
     *
     * @return the current value of msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * Setter method for msg.
     *
     * @param aMsg the new value for msg
     */
    public void setMsg(String aMsg) {
        msg = aMsg;
    }

    /**
     * Access method for controlelote.
     *
     * @return the current value of controlelote
     */
    public String getControlelote() {
        return controlelote;
    }

    /**
     * Setter method for controlelote.
     *
     * @param aControlelote the new value for controlelote
     */
    public void setControlelote(String aControlelote) {
        controlelote = aControlelote;
    }

    /**
     * Access method for protocolo.
     *
     * @return the current value of protocolo
     */
    public String getProtocolo() {
        return protocolo;
    }

    /**
     * Setter method for protocolo.
     *
     * @param aProtocolo the new value for protocolo
     */
    public void setProtocolo(String aProtocolo) {
        protocolo = aProtocolo;
    }

    /**
     * Access method for cancjustificativa.
     *
     * @return the current value of cancjustificativa
     */
    public String getCancjustificativa() {
        return cancjustificativa;
    }

    /**
     * Setter method for cancjustificativa.
     *
     * @param aCancjustificativa the new value for cancjustificativa
     */
    public void setCancjustificativa(String aCancjustificativa) {
        cancjustificativa = aCancjustificativa;
    }

    /**
     * Access method for opnfe.
     *
     * @return the current value of opnfe
     */
    public Opnfe getOpnfe() {
        return opnfe;
    }

    /**
     * Setter method for opnfe.
     *
     * @param aOpnfe the new value for opnfe
     */
    public void setOpnfe(Opnfe aOpnfe) {
        opnfe = aOpnfe;
    }

    /**
     * Compares the key for this instance with another Opnfedet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opnfedet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opnfedet)) {
            return false;
        }
        Opnfedet that = (Opnfedet) other;
        if (this.getCodopnfedet() != that.getCodopnfedet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opnfedet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opnfedet)) return false;
        return this.equalKeys(other) && ((Opnfedet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodopnfedet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opnfedet |");
        sb.append(" codopnfedet=").append(getCodopnfedet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopnfedet", Integer.valueOf(getCodopnfedet()));
        return ret;
    }

}
