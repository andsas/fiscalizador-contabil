package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPNFEIMPORTA")
public class Opnfeimporta implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopnfeimporta";

    @Id
    @Column(name="CODOPNFEIMPORTA", unique=true, nullable=false, precision=10)
    private int codopnfeimporta;
    @Column(name="REMNOME", nullable=false, length=250)
    private String remnome;
    @Column(name="REMCNPJ", nullable=false, length=20)
    private String remcnpj;
    @Column(name="NFNUM", nullable=false, precision=10)
    private int nfnum;
    @Column(name="NFCHAVE", nullable=false, length=250)
    private String nfchave;
    @Column(name="ICMSTOT", precision=15, scale=4)
    private BigDecimal icmstot;
    @Column(name="XML", nullable=false)
    private String xml;
    @Column(name="DATAIMPORTA", nullable=false)
    private Timestamp dataimporta;
    @Column(name="DATAOPTRANSACAO")
    private Timestamp dataoptransacao;
    @Column(name="NFEIMPORTADA", precision=5)
    private short nfeimportada;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAO")
    private Optransacao optransacao;

    /** Default constructor. */
    public Opnfeimporta() {
        super();
    }

    /**
     * Access method for codopnfeimporta.
     *
     * @return the current value of codopnfeimporta
     */
    public int getCodopnfeimporta() {
        return codopnfeimporta;
    }

    /**
     * Setter method for codopnfeimporta.
     *
     * @param aCodopnfeimporta the new value for codopnfeimporta
     */
    public void setCodopnfeimporta(int aCodopnfeimporta) {
        codopnfeimporta = aCodopnfeimporta;
    }

    /**
     * Access method for remnome.
     *
     * @return the current value of remnome
     */
    public String getRemnome() {
        return remnome;
    }

    /**
     * Setter method for remnome.
     *
     * @param aRemnome the new value for remnome
     */
    public void setRemnome(String aRemnome) {
        remnome = aRemnome;
    }

    /**
     * Access method for remcnpj.
     *
     * @return the current value of remcnpj
     */
    public String getRemcnpj() {
        return remcnpj;
    }

    /**
     * Setter method for remcnpj.
     *
     * @param aRemcnpj the new value for remcnpj
     */
    public void setRemcnpj(String aRemcnpj) {
        remcnpj = aRemcnpj;
    }

    /**
     * Access method for nfnum.
     *
     * @return the current value of nfnum
     */
    public int getNfnum() {
        return nfnum;
    }

    /**
     * Setter method for nfnum.
     *
     * @param aNfnum the new value for nfnum
     */
    public void setNfnum(int aNfnum) {
        nfnum = aNfnum;
    }

    /**
     * Access method for nfchave.
     *
     * @return the current value of nfchave
     */
    public String getNfchave() {
        return nfchave;
    }

    /**
     * Setter method for nfchave.
     *
     * @param aNfchave the new value for nfchave
     */
    public void setNfchave(String aNfchave) {
        nfchave = aNfchave;
    }

    /**
     * Access method for icmstot.
     *
     * @return the current value of icmstot
     */
    public BigDecimal getIcmstot() {
        return icmstot;
    }

    /**
     * Setter method for icmstot.
     *
     * @param aIcmstot the new value for icmstot
     */
    public void setIcmstot(BigDecimal aIcmstot) {
        icmstot = aIcmstot;
    }

    /**
     * Access method for xml.
     *
     * @return the current value of xml
     */
    public String getXml() {
        return xml;
    }

    /**
     * Setter method for xml.
     *
     * @param aXml the new value for xml
     */
    public void setXml(String aXml) {
        xml = aXml;
    }

    /**
     * Access method for dataimporta.
     *
     * @return the current value of dataimporta
     */
    public Timestamp getDataimporta() {
        return dataimporta;
    }

    /**
     * Setter method for dataimporta.
     *
     * @param aDataimporta the new value for dataimporta
     */
    public void setDataimporta(Timestamp aDataimporta) {
        dataimporta = aDataimporta;
    }

    /**
     * Access method for dataoptransacao.
     *
     * @return the current value of dataoptransacao
     */
    public Timestamp getDataoptransacao() {
        return dataoptransacao;
    }

    /**
     * Setter method for dataoptransacao.
     *
     * @param aDataoptransacao the new value for dataoptransacao
     */
    public void setDataoptransacao(Timestamp aDataoptransacao) {
        dataoptransacao = aDataoptransacao;
    }

    /**
     * Access method for nfeimportada.
     *
     * @return the current value of nfeimportada
     */
    public short getNfeimportada() {
        return nfeimportada;
    }

    /**
     * Setter method for nfeimportada.
     *
     * @param aNfeimportada the new value for nfeimportada
     */
    public void setNfeimportada(short aNfeimportada) {
        nfeimportada = aNfeimportada;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Compares the key for this instance with another Opnfeimporta.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opnfeimporta and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opnfeimporta)) {
            return false;
        }
        Opnfeimporta that = (Opnfeimporta) other;
        if (this.getCodopnfeimporta() != that.getCodopnfeimporta()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opnfeimporta.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opnfeimporta)) return false;
        return this.equalKeys(other) && ((Opnfeimporta)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodopnfeimporta();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opnfeimporta |");
        sb.append(" codopnfeimporta=").append(getCodopnfeimporta());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopnfeimporta", Integer.valueOf(getCodopnfeimporta()));
        return ret;
    }

}
