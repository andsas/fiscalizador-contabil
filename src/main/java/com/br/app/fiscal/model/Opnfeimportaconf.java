package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="OPNFEIMPORTACONF")
public class Opnfeimportaconf implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopnfeimportaconf";

    @Id
    @Column(name="CODOPNFEIMPORTACONF", unique=true, nullable=false, precision=10)
    private int codopnfeimportaconf;
    @Column(name="CODAGTIPO", precision=10)
    private int codagtipo;
    @Column(name="CODAGGRUPO", length=20)
    private String codaggrupo;
    @Column(name="CODPRODGRUPO", length=20)
    private String codprodgrupo;
    @Column(name="CODOPTIPO", precision=10)
    private int codoptipo;
    @Column(name="ULTNSUCONSNFES", length=20)
    private String ultnsuconsnfes;
    @Column(name="CODCOBRFORMA", precision=10)
    private int codcobrforma;

    /** Default constructor. */
    public Opnfeimportaconf() {
        super();
    }

    /**
     * Access method for codopnfeimportaconf.
     *
     * @return the current value of codopnfeimportaconf
     */
    public int getCodopnfeimportaconf() {
        return codopnfeimportaconf;
    }

    /**
     * Setter method for codopnfeimportaconf.
     *
     * @param aCodopnfeimportaconf the new value for codopnfeimportaconf
     */
    public void setCodopnfeimportaconf(int aCodopnfeimportaconf) {
        codopnfeimportaconf = aCodopnfeimportaconf;
    }

    /**
     * Access method for codagtipo.
     *
     * @return the current value of codagtipo
     */
    public int getCodagtipo() {
        return codagtipo;
    }

    /**
     * Setter method for codagtipo.
     *
     * @param aCodagtipo the new value for codagtipo
     */
    public void setCodagtipo(int aCodagtipo) {
        codagtipo = aCodagtipo;
    }

    /**
     * Access method for codaggrupo.
     *
     * @return the current value of codaggrupo
     */
    public String getCodaggrupo() {
        return codaggrupo;
    }

    /**
     * Setter method for codaggrupo.
     *
     * @param aCodaggrupo the new value for codaggrupo
     */
    public void setCodaggrupo(String aCodaggrupo) {
        codaggrupo = aCodaggrupo;
    }

    /**
     * Access method for codprodgrupo.
     *
     * @return the current value of codprodgrupo
     */
    public String getCodprodgrupo() {
        return codprodgrupo;
    }

    /**
     * Setter method for codprodgrupo.
     *
     * @param aCodprodgrupo the new value for codprodgrupo
     */
    public void setCodprodgrupo(String aCodprodgrupo) {
        codprodgrupo = aCodprodgrupo;
    }

    /**
     * Access method for codoptipo.
     *
     * @return the current value of codoptipo
     */
    public int getCodoptipo() {
        return codoptipo;
    }

    /**
     * Setter method for codoptipo.
     *
     * @param aCodoptipo the new value for codoptipo
     */
    public void setCodoptipo(int aCodoptipo) {
        codoptipo = aCodoptipo;
    }

    /**
     * Access method for ultnsuconsnfes.
     *
     * @return the current value of ultnsuconsnfes
     */
    public String getUltnsuconsnfes() {
        return ultnsuconsnfes;
    }

    /**
     * Setter method for ultnsuconsnfes.
     *
     * @param aUltnsuconsnfes the new value for ultnsuconsnfes
     */
    public void setUltnsuconsnfes(String aUltnsuconsnfes) {
        ultnsuconsnfes = aUltnsuconsnfes;
    }

    /**
     * Access method for codcobrforma.
     *
     * @return the current value of codcobrforma
     */
    public int getCodcobrforma() {
        return codcobrforma;
    }

    /**
     * Setter method for codcobrforma.
     *
     * @param aCodcobrforma the new value for codcobrforma
     */
    public void setCodcobrforma(int aCodcobrforma) {
        codcobrforma = aCodcobrforma;
    }

    /**
     * Compares the key for this instance with another Opnfeimportaconf.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opnfeimportaconf and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opnfeimportaconf)) {
            return false;
        }
        Opnfeimportaconf that = (Opnfeimportaconf) other;
        if (this.getCodopnfeimportaconf() != that.getCodopnfeimportaconf()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opnfeimportaconf.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opnfeimportaconf)) return false;
        return this.equalKeys(other) && ((Opnfeimportaconf)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodopnfeimportaconf();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opnfeimportaconf |");
        sb.append(" codopnfeimportaconf=").append(getCodopnfeimportaconf());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopnfeimportaconf", Integer.valueOf(getCodopnfeimportaconf()));
        return ret;
    }

}
