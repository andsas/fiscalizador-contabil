package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPNFEINUT")
public class Opnfeinut implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopnfeinut";

    @Id
    @Column(name="CODOPNFEINUT", unique=true, nullable=false, precision=10)
    private int codopnfeinut;
    @Column(name="NUMINI", nullable=false, precision=10)
    private int numini;
    @Column(name="NUMFIM", nullable=false, precision=10)
    private int numfim;
    @Column(name="JUSTIFICATIVA", nullable=false)
    private byte[] justificativa;
    @Column(name="CSTAT", precision=10)
    private int cstat;
    @Column(name="XML")
    private byte[] xml;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPDOCSERIE", nullable=false)
    private Opdocserie opdocserie;

    /** Default constructor. */
    public Opnfeinut() {
        super();
    }

    /**
     * Access method for codopnfeinut.
     *
     * @return the current value of codopnfeinut
     */
    public int getCodopnfeinut() {
        return codopnfeinut;
    }

    /**
     * Setter method for codopnfeinut.
     *
     * @param aCodopnfeinut the new value for codopnfeinut
     */
    public void setCodopnfeinut(int aCodopnfeinut) {
        codopnfeinut = aCodopnfeinut;
    }

    /**
     * Access method for numini.
     *
     * @return the current value of numini
     */
    public int getNumini() {
        return numini;
    }

    /**
     * Setter method for numini.
     *
     * @param aNumini the new value for numini
     */
    public void setNumini(int aNumini) {
        numini = aNumini;
    }

    /**
     * Access method for numfim.
     *
     * @return the current value of numfim
     */
    public int getNumfim() {
        return numfim;
    }

    /**
     * Setter method for numfim.
     *
     * @param aNumfim the new value for numfim
     */
    public void setNumfim(int aNumfim) {
        numfim = aNumfim;
    }

    /**
     * Access method for justificativa.
     *
     * @return the current value of justificativa
     */
    public byte[] getJustificativa() {
        return justificativa;
    }

    /**
     * Setter method for justificativa.
     *
     * @param aJustificativa the new value for justificativa
     */
    public void setJustificativa(byte[] aJustificativa) {
        justificativa = aJustificativa;
    }

    /**
     * Access method for cstat.
     *
     * @return the current value of cstat
     */
    public int getCstat() {
        return cstat;
    }

    /**
     * Setter method for cstat.
     *
     * @param aCstat the new value for cstat
     */
    public void setCstat(int aCstat) {
        cstat = aCstat;
    }

    /**
     * Access method for xml.
     *
     * @return the current value of xml
     */
    public byte[] getXml() {
        return xml;
    }

    /**
     * Setter method for xml.
     *
     * @param aXml the new value for xml
     */
    public void setXml(byte[] aXml) {
        xml = aXml;
    }

    /**
     * Access method for opdocserie.
     *
     * @return the current value of opdocserie
     */
    public Opdocserie getOpdocserie() {
        return opdocserie;
    }

    /**
     * Setter method for opdocserie.
     *
     * @param aOpdocserie the new value for opdocserie
     */
    public void setOpdocserie(Opdocserie aOpdocserie) {
        opdocserie = aOpdocserie;
    }

    /**
     * Compares the key for this instance with another Opnfeinut.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opnfeinut and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opnfeinut)) {
            return false;
        }
        Opnfeinut that = (Opnfeinut) other;
        if (this.getCodopnfeinut() != that.getCodopnfeinut()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opnfeinut.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opnfeinut)) return false;
        return this.equalKeys(other) && ((Opnfeinut)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodopnfeinut();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opnfeinut |");
        sb.append(" codopnfeinut=").append(getCodopnfeinut());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopnfeinut", Integer.valueOf(getCodopnfeinut()));
        return ret;
    }

}
