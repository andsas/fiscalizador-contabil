package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPOPERACAO")
public class Opoperacao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopoperacao";

    @Id
    @Column(name="CODOPOPERACAO", unique=true, nullable=false, length=20)
    private String codopoperacao;
    @Column(name="DESCOPOPERACAO", nullable=false, length=250)
    private String descopoperacao;
    @Column(name="APLICACAO")
    private String aplicacao;
    @Column(name="FAVORITA", precision=5)
    private short favorita;
    @OneToMany(mappedBy="opoperacao")
    private Set<Agagente> agagente;
    @OneToMany(mappedBy="opoperacao")
    private Set<Fises> fises;
    @OneToMany(mappedBy="opoperacao")
    private Set<Fisesdet> fisesdet;
    @OneToMany(mappedBy="opoperacao")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="opoperacao")
    private Set<Opconfentrada> opconfentrada;
    @OneToMany(mappedBy="opoperacao")
    private Set<Opconfsaida> opconfsaida;
    @OneToMany(mappedBy="opoperacao")
    private Set<Opevento> opevento;
    @OneToMany(mappedBy="opoperacao2")
    private Set<Opoperacao> opoperacao3;
    @ManyToOne
    @JoinColumn(name="CODIMPORTAOPOPERACAO")
    private Opoperacao opoperacao2;
    @ManyToOne
    @JoinColumn(name="CODOPTIPO")
    private Optipo optipo;
    @ManyToOne
    @JoinColumn(name="CTBCONTACREDITO")
    private Ctbplanoconta ctbplanoconta;
    @ManyToOne
    @JoinColumn(name="CTBCONTADEBITO")
    private Ctbplanoconta ctbplanoconta2;
    @OneToMany(mappedBy="opoperacao")
    private Set<Optransacao> optransacao;
    @OneToMany(mappedBy="opoperacao")
    private Set<Optransacaodet> optransacaodet;
    @OneToMany(mappedBy="opoperacao")
    private Set<Prdcplanoetapa> prdcplanoetapa;

    /** Default constructor. */
    public Opoperacao() {
        super();
    }

    /**
     * Access method for codopoperacao.
     *
     * @return the current value of codopoperacao
     */
    public String getCodopoperacao() {
        return codopoperacao;
    }

    /**
     * Setter method for codopoperacao.
     *
     * @param aCodopoperacao the new value for codopoperacao
     */
    public void setCodopoperacao(String aCodopoperacao) {
        codopoperacao = aCodopoperacao;
    }

    /**
     * Access method for descopoperacao.
     *
     * @return the current value of descopoperacao
     */
    public String getDescopoperacao() {
        return descopoperacao;
    }

    /**
     * Setter method for descopoperacao.
     *
     * @param aDescopoperacao the new value for descopoperacao
     */
    public void setDescopoperacao(String aDescopoperacao) {
        descopoperacao = aDescopoperacao;
    }

    /**
     * Access method for aplicacao.
     *
     * @return the current value of aplicacao
     */
    public String getAplicacao() {
        return aplicacao;
    }

    /**
     * Setter method for aplicacao.
     *
     * @param aAplicacao the new value for aplicacao
     */
    public void setAplicacao(String aAplicacao) {
        aplicacao = aAplicacao;
    }

    /**
     * Access method for favorita.
     *
     * @return the current value of favorita
     */
    public short getFavorita() {
        return favorita;
    }

    /**
     * Setter method for favorita.
     *
     * @param aFavorita the new value for favorita
     */
    public void setFavorita(short aFavorita) {
        favorita = aFavorita;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for fises.
     *
     * @return the current value of fises
     */
    public Set<Fises> getFises() {
        return fises;
    }

    /**
     * Setter method for fises.
     *
     * @param aFises the new value for fises
     */
    public void setFises(Set<Fises> aFises) {
        fises = aFises;
    }

    /**
     * Access method for fisesdet.
     *
     * @return the current value of fisesdet
     */
    public Set<Fisesdet> getFisesdet() {
        return fisesdet;
    }

    /**
     * Setter method for fisesdet.
     *
     * @param aFisesdet the new value for fisesdet
     */
    public void setFisesdet(Set<Fisesdet> aFisesdet) {
        fisesdet = aFisesdet;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for opconfentrada.
     *
     * @return the current value of opconfentrada
     */
    public Set<Opconfentrada> getOpconfentrada() {
        return opconfentrada;
    }

    /**
     * Setter method for opconfentrada.
     *
     * @param aOpconfentrada the new value for opconfentrada
     */
    public void setOpconfentrada(Set<Opconfentrada> aOpconfentrada) {
        opconfentrada = aOpconfentrada;
    }

    /**
     * Access method for opconfsaida.
     *
     * @return the current value of opconfsaida
     */
    public Set<Opconfsaida> getOpconfsaida() {
        return opconfsaida;
    }

    /**
     * Setter method for opconfsaida.
     *
     * @param aOpconfsaida the new value for opconfsaida
     */
    public void setOpconfsaida(Set<Opconfsaida> aOpconfsaida) {
        opconfsaida = aOpconfsaida;
    }

    /**
     * Access method for opevento.
     *
     * @return the current value of opevento
     */
    public Set<Opevento> getOpevento() {
        return opevento;
    }

    /**
     * Setter method for opevento.
     *
     * @param aOpevento the new value for opevento
     */
    public void setOpevento(Set<Opevento> aOpevento) {
        opevento = aOpevento;
    }

    /**
     * Access method for opoperacao3.
     *
     * @return the current value of opoperacao3
     */
    public Set<Opoperacao> getOpoperacao3() {
        return opoperacao3;
    }

    /**
     * Setter method for opoperacao3.
     *
     * @param aOpoperacao3 the new value for opoperacao3
     */
    public void setOpoperacao3(Set<Opoperacao> aOpoperacao3) {
        opoperacao3 = aOpoperacao3;
    }

    /**
     * Access method for opoperacao2.
     *
     * @return the current value of opoperacao2
     */
    public Opoperacao getOpoperacao2() {
        return opoperacao2;
    }

    /**
     * Setter method for opoperacao2.
     *
     * @param aOpoperacao2 the new value for opoperacao2
     */
    public void setOpoperacao2(Opoperacao aOpoperacao2) {
        opoperacao2 = aOpoperacao2;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Optipo getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Optipo aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Ctbplanoconta getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Ctbplanoconta aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for ctbplanoconta2.
     *
     * @return the current value of ctbplanoconta2
     */
    public Ctbplanoconta getCtbplanoconta2() {
        return ctbplanoconta2;
    }

    /**
     * Setter method for ctbplanoconta2.
     *
     * @param aCtbplanoconta2 the new value for ctbplanoconta2
     */
    public void setCtbplanoconta2(Ctbplanoconta aCtbplanoconta2) {
        ctbplanoconta2 = aCtbplanoconta2;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Access method for prdcplanoetapa.
     *
     * @return the current value of prdcplanoetapa
     */
    public Set<Prdcplanoetapa> getPrdcplanoetapa() {
        return prdcplanoetapa;
    }

    /**
     * Setter method for prdcplanoetapa.
     *
     * @param aPrdcplanoetapa the new value for prdcplanoetapa
     */
    public void setPrdcplanoetapa(Set<Prdcplanoetapa> aPrdcplanoetapa) {
        prdcplanoetapa = aPrdcplanoetapa;
    }

    /**
     * Compares the key for this instance with another Opoperacao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opoperacao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opoperacao)) {
            return false;
        }
        Opoperacao that = (Opoperacao) other;
        Object myCodopoperacao = this.getCodopoperacao();
        Object yourCodopoperacao = that.getCodopoperacao();
        if (myCodopoperacao==null ? yourCodopoperacao!=null : !myCodopoperacao.equals(yourCodopoperacao)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opoperacao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opoperacao)) return false;
        return this.equalKeys(other) && ((Opoperacao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodopoperacao() == null) {
            i = 0;
        } else {
            i = getCodopoperacao().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opoperacao |");
        sb.append(" codopoperacao=").append(getCodopoperacao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopoperacao", getCodopoperacao());
        return ret;
    }

}
