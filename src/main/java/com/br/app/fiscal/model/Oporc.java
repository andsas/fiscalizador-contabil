package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPORC")
public class Oporc implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoporc";

    @Id
    @Column(name="CODOPORC", unique=true, nullable=false, precision=10)
    private int codoporc;
    @Column(name="DATA")
    private Date data;
    @Column(name="TOTAL", precision=15, scale=4)
    private BigDecimal total;
    @Column(name="AGAGENTEGANHADOR", precision=10)
    private int agagenteganhador;
    @ManyToOne
    @JoinColumn(name="CODOPSOLICITACAO")
    private Opsolicitacao opsolicitacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIO", nullable=false)
    private Confusuario confusuario;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @OneToMany(mappedBy="oporc")
    private Set<Oporcdet> oporcdet;
    @OneToMany(mappedBy="oporc")
    private Set<Optransacao> optransacao;

    /** Default constructor. */
    public Oporc() {
        super();
    }

    /**
     * Access method for codoporc.
     *
     * @return the current value of codoporc
     */
    public int getCodoporc() {
        return codoporc;
    }

    /**
     * Setter method for codoporc.
     *
     * @param aCodoporc the new value for codoporc
     */
    public void setCodoporc(int aCodoporc) {
        codoporc = aCodoporc;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Date getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Date aData) {
        data = aData;
    }

    /**
     * Access method for total.
     *
     * @return the current value of total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Setter method for total.
     *
     * @param aTotal the new value for total
     */
    public void setTotal(BigDecimal aTotal) {
        total = aTotal;
    }

    /**
     * Access method for agagenteganhador.
     *
     * @return the current value of agagenteganhador
     */
    public int getAgagenteganhador() {
        return agagenteganhador;
    }

    /**
     * Setter method for agagenteganhador.
     *
     * @param aAgagenteganhador the new value for agagenteganhador
     */
    public void setAgagenteganhador(int aAgagenteganhador) {
        agagenteganhador = aAgagenteganhador;
    }

    /**
     * Access method for opsolicitacao.
     *
     * @return the current value of opsolicitacao
     */
    public Opsolicitacao getOpsolicitacao() {
        return opsolicitacao;
    }

    /**
     * Setter method for opsolicitacao.
     *
     * @param aOpsolicitacao the new value for opsolicitacao
     */
    public void setOpsolicitacao(Opsolicitacao aOpsolicitacao) {
        opsolicitacao = aOpsolicitacao;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for oporcdet.
     *
     * @return the current value of oporcdet
     */
    public Set<Oporcdet> getOporcdet() {
        return oporcdet;
    }

    /**
     * Setter method for oporcdet.
     *
     * @param aOporcdet the new value for oporcdet
     */
    public void setOporcdet(Set<Oporcdet> aOporcdet) {
        oporcdet = aOporcdet;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Compares the key for this instance with another Oporc.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Oporc and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Oporc)) {
            return false;
        }
        Oporc that = (Oporc) other;
        if (this.getCodoporc() != that.getCodoporc()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Oporc.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Oporc)) return false;
        return this.equalKeys(other) && ((Oporc)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoporc();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Oporc |");
        sb.append(" codoporc=").append(getCodoporc());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoporc", Integer.valueOf(getCodoporc()));
        return ret;
    }

}
