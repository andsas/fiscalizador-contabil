package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPORCDET")
public class Oporcdet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoporcdet";

    @Id
    @Column(name="CODOPORCDET", unique=true, nullable=false, precision=10)
    private int codoporcdet;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPORC", nullable=false)
    private Oporc oporc;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @OneToMany(mappedBy="oporcdet")
    private Set<Oporcdetag> oporcdetag;

    /** Default constructor. */
    public Oporcdet() {
        super();
    }

    /**
     * Access method for codoporcdet.
     *
     * @return the current value of codoporcdet
     */
    public int getCodoporcdet() {
        return codoporcdet;
    }

    /**
     * Setter method for codoporcdet.
     *
     * @param aCodoporcdet the new value for codoporcdet
     */
    public void setCodoporcdet(int aCodoporcdet) {
        codoporcdet = aCodoporcdet;
    }

    /**
     * Access method for oporc.
     *
     * @return the current value of oporc
     */
    public Oporc getOporc() {
        return oporc;
    }

    /**
     * Setter method for oporc.
     *
     * @param aOporc the new value for oporc
     */
    public void setOporc(Oporc aOporc) {
        oporc = aOporc;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for oporcdetag.
     *
     * @return the current value of oporcdetag
     */
    public Set<Oporcdetag> getOporcdetag() {
        return oporcdetag;
    }

    /**
     * Setter method for oporcdetag.
     *
     * @param aOporcdetag the new value for oporcdetag
     */
    public void setOporcdetag(Set<Oporcdetag> aOporcdetag) {
        oporcdetag = aOporcdetag;
    }

    /**
     * Compares the key for this instance with another Oporcdet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Oporcdet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Oporcdet)) {
            return false;
        }
        Oporcdet that = (Oporcdet) other;
        if (this.getCodoporcdet() != that.getCodoporcdet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Oporcdet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Oporcdet)) return false;
        return this.equalKeys(other) && ((Oporcdet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoporcdet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Oporcdet |");
        sb.append(" codoporcdet=").append(getCodoporcdet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoporcdet", Integer.valueOf(getCodoporcdet()));
        return ret;
    }

}
