package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPORCDETAG")
public class Oporcdetag implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoporcdetag";

    @Id
    @Column(name="CODOPORCDETAG", unique=true, nullable=false, precision=10)
    private int codoporcdetag;
    @Column(name="GANHADOR", precision=5)
    private short ganhador;
    @Column(name="QUANTIDADE", precision=15, scale=4)
    private BigDecimal quantidade;
    @Column(name="PRECO", precision=15, scale=4)
    private BigDecimal preco;
    @Column(name="VALIDADE", precision=5)
    private short validade;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPORCDET", nullable=false)
    private Oporcdet oporcdet;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;

    /** Default constructor. */
    public Oporcdetag() {
        super();
    }

    /**
     * Access method for codoporcdetag.
     *
     * @return the current value of codoporcdetag
     */
    public int getCodoporcdetag() {
        return codoporcdetag;
    }

    /**
     * Setter method for codoporcdetag.
     *
     * @param aCodoporcdetag the new value for codoporcdetag
     */
    public void setCodoporcdetag(int aCodoporcdetag) {
        codoporcdetag = aCodoporcdetag;
    }

    /**
     * Access method for ganhador.
     *
     * @return the current value of ganhador
     */
    public short getGanhador() {
        return ganhador;
    }

    /**
     * Setter method for ganhador.
     *
     * @param aGanhador the new value for ganhador
     */
    public void setGanhador(short aGanhador) {
        ganhador = aGanhador;
    }

    /**
     * Access method for quantidade.
     *
     * @return the current value of quantidade
     */
    public BigDecimal getQuantidade() {
        return quantidade;
    }

    /**
     * Setter method for quantidade.
     *
     * @param aQuantidade the new value for quantidade
     */
    public void setQuantidade(BigDecimal aQuantidade) {
        quantidade = aQuantidade;
    }

    /**
     * Access method for preco.
     *
     * @return the current value of preco
     */
    public BigDecimal getPreco() {
        return preco;
    }

    /**
     * Setter method for preco.
     *
     * @param aPreco the new value for preco
     */
    public void setPreco(BigDecimal aPreco) {
        preco = aPreco;
    }

    /**
     * Access method for validade.
     *
     * @return the current value of validade
     */
    public short getValidade() {
        return validade;
    }

    /**
     * Setter method for validade.
     *
     * @param aValidade the new value for validade
     */
    public void setValidade(short aValidade) {
        validade = aValidade;
    }

    /**
     * Access method for oporcdet.
     *
     * @return the current value of oporcdet
     */
    public Oporcdet getOporcdet() {
        return oporcdet;
    }

    /**
     * Setter method for oporcdet.
     *
     * @param aOporcdet the new value for oporcdet
     */
    public void setOporcdet(Oporcdet aOporcdet) {
        oporcdet = aOporcdet;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Compares the key for this instance with another Oporcdetag.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Oporcdetag and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Oporcdetag)) {
            return false;
        }
        Oporcdetag that = (Oporcdetag) other;
        if (this.getCodoporcdetag() != that.getCodoporcdetag()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Oporcdetag.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Oporcdetag)) return false;
        return this.equalKeys(other) && ((Oporcdetag)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoporcdetag();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Oporcdetag |");
        sb.append(" codoporcdetag=").append(getCodoporcdetag());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoporcdetag", Integer.valueOf(getCodoporcdetag()));
        return ret;
    }

}
