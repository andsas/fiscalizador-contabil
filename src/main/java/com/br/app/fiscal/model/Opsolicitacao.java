package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPSOLICITACAO")
public class Opsolicitacao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopsolicitacao";

    @Id
    @Column(name="CODOPSOLICITACAO", unique=true, nullable=false, precision=10)
    private int codopsolicitacao;
    @Column(name="DATA", nullable=false)
    private Date data;
    @Column(name="DESCOPSOLICITACAO", nullable=false, length=250)
    private String descopsolicitacao;
    @Column(name="TIPOOPSOLICITACAO", precision=5)
    private short tipoopsolicitacao;
    @Column(name="STATUS", precision=5)
    private short status;
    @OneToMany(mappedBy="opsolicitacao")
    private Set<Oporc> oporc;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIO", nullable=false)
    private Confusuario confusuario;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAO")
    private Optransacao optransacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;
    @ManyToOne
    @JoinColumn(name="CODCONFAUTUSUARIO")
    private Confusuario confusuario2;
    @ManyToOne
    @JoinColumn(name="CODCONFAUTTIPOUSUARIO")
    private Confusuariotipo confusuariotipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTIPO", nullable=false)
    private Optipo optipo;
    @OneToMany(mappedBy="opsolicitacao")
    private Set<Opsolicitacaodet> opsolicitacaodet;

    /** Default constructor. */
    public Opsolicitacao() {
        super();
    }

    /**
     * Access method for codopsolicitacao.
     *
     * @return the current value of codopsolicitacao
     */
    public int getCodopsolicitacao() {
        return codopsolicitacao;
    }

    /**
     * Setter method for codopsolicitacao.
     *
     * @param aCodopsolicitacao the new value for codopsolicitacao
     */
    public void setCodopsolicitacao(int aCodopsolicitacao) {
        codopsolicitacao = aCodopsolicitacao;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Date getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Date aData) {
        data = aData;
    }

    /**
     * Access method for descopsolicitacao.
     *
     * @return the current value of descopsolicitacao
     */
    public String getDescopsolicitacao() {
        return descopsolicitacao;
    }

    /**
     * Setter method for descopsolicitacao.
     *
     * @param aDescopsolicitacao the new value for descopsolicitacao
     */
    public void setDescopsolicitacao(String aDescopsolicitacao) {
        descopsolicitacao = aDescopsolicitacao;
    }

    /**
     * Access method for tipoopsolicitacao.
     *
     * @return the current value of tipoopsolicitacao
     */
    public short getTipoopsolicitacao() {
        return tipoopsolicitacao;
    }

    /**
     * Setter method for tipoopsolicitacao.
     *
     * @param aTipoopsolicitacao the new value for tipoopsolicitacao
     */
    public void setTipoopsolicitacao(short aTipoopsolicitacao) {
        tipoopsolicitacao = aTipoopsolicitacao;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public short getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(short aStatus) {
        status = aStatus;
    }

    /**
     * Access method for oporc.
     *
     * @return the current value of oporc
     */
    public Set<Oporc> getOporc() {
        return oporc;
    }

    /**
     * Setter method for oporc.
     *
     * @param aOporc the new value for oporc
     */
    public void setOporc(Set<Oporc> aOporc) {
        oporc = aOporc;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Access method for confusuario2.
     *
     * @return the current value of confusuario2
     */
    public Confusuario getConfusuario2() {
        return confusuario2;
    }

    /**
     * Setter method for confusuario2.
     *
     * @param aConfusuario2 the new value for confusuario2
     */
    public void setConfusuario2(Confusuario aConfusuario2) {
        confusuario2 = aConfusuario2;
    }

    /**
     * Access method for confusuariotipo.
     *
     * @return the current value of confusuariotipo
     */
    public Confusuariotipo getConfusuariotipo() {
        return confusuariotipo;
    }

    /**
     * Setter method for confusuariotipo.
     *
     * @param aConfusuariotipo the new value for confusuariotipo
     */
    public void setConfusuariotipo(Confusuariotipo aConfusuariotipo) {
        confusuariotipo = aConfusuariotipo;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Optipo getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Optipo aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Access method for opsolicitacaodet.
     *
     * @return the current value of opsolicitacaodet
     */
    public Set<Opsolicitacaodet> getOpsolicitacaodet() {
        return opsolicitacaodet;
    }

    /**
     * Setter method for opsolicitacaodet.
     *
     * @param aOpsolicitacaodet the new value for opsolicitacaodet
     */
    public void setOpsolicitacaodet(Set<Opsolicitacaodet> aOpsolicitacaodet) {
        opsolicitacaodet = aOpsolicitacaodet;
    }

    /**
     * Compares the key for this instance with another Opsolicitacao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opsolicitacao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opsolicitacao)) {
            return false;
        }
        Opsolicitacao that = (Opsolicitacao) other;
        if (this.getCodopsolicitacao() != that.getCodopsolicitacao()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opsolicitacao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opsolicitacao)) return false;
        return this.equalKeys(other) && ((Opsolicitacao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodopsolicitacao();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opsolicitacao |");
        sb.append(" codopsolicitacao=").append(getCodopsolicitacao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopsolicitacao", Integer.valueOf(getCodopsolicitacao()));
        return ret;
    }

}
