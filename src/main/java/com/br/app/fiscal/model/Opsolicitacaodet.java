package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPSOLICITACAODET")
public class Opsolicitacaodet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codopsolicitacaodet";

    @Id
    @Column(name="CODOPSOLICITACAODET", unique=true, nullable=false, precision=10)
    private int codopsolicitacaodet;
    @Column(name="QUANTIDADE", precision=15, scale=4)
    private BigDecimal quantidade;
    @Column(name="DESCOPSOLICITACAODET", length=250)
    private String descopsolicitacaodet;
    @Column(name="VALOR", precision=15, scale=4)
    private BigDecimal valor;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPSOLICITACAO", nullable=false)
    private Opsolicitacao opsolicitacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAODET")
    private Optransacaodet optransacaodet;

    /** Default constructor. */
    public Opsolicitacaodet() {
        super();
    }

    /**
     * Access method for codopsolicitacaodet.
     *
     * @return the current value of codopsolicitacaodet
     */
    public int getCodopsolicitacaodet() {
        return codopsolicitacaodet;
    }

    /**
     * Setter method for codopsolicitacaodet.
     *
     * @param aCodopsolicitacaodet the new value for codopsolicitacaodet
     */
    public void setCodopsolicitacaodet(int aCodopsolicitacaodet) {
        codopsolicitacaodet = aCodopsolicitacaodet;
    }

    /**
     * Access method for quantidade.
     *
     * @return the current value of quantidade
     */
    public BigDecimal getQuantidade() {
        return quantidade;
    }

    /**
     * Setter method for quantidade.
     *
     * @param aQuantidade the new value for quantidade
     */
    public void setQuantidade(BigDecimal aQuantidade) {
        quantidade = aQuantidade;
    }

    /**
     * Access method for descopsolicitacaodet.
     *
     * @return the current value of descopsolicitacaodet
     */
    public String getDescopsolicitacaodet() {
        return descopsolicitacaodet;
    }

    /**
     * Setter method for descopsolicitacaodet.
     *
     * @param aDescopsolicitacaodet the new value for descopsolicitacaodet
     */
    public void setDescopsolicitacaodet(String aDescopsolicitacaodet) {
        descopsolicitacaodet = aDescopsolicitacaodet;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(BigDecimal aValor) {
        valor = aValor;
    }

    /**
     * Access method for opsolicitacao.
     *
     * @return the current value of opsolicitacao
     */
    public Opsolicitacao getOpsolicitacao() {
        return opsolicitacao;
    }

    /**
     * Setter method for opsolicitacao.
     *
     * @param aOpsolicitacao the new value for opsolicitacao
     */
    public void setOpsolicitacao(Opsolicitacao aOpsolicitacao) {
        opsolicitacao = aOpsolicitacao;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Optransacaodet getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Optransacaodet aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Compares the key for this instance with another Opsolicitacaodet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Opsolicitacaodet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Opsolicitacaodet)) {
            return false;
        }
        Opsolicitacaodet that = (Opsolicitacaodet) other;
        if (this.getCodopsolicitacaodet() != that.getCodopsolicitacaodet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Opsolicitacaodet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Opsolicitacaodet)) return false;
        return this.equalKeys(other) && ((Opsolicitacaodet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodopsolicitacaodet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Opsolicitacaodet |");
        sb.append(" codopsolicitacaodet=").append(getCodopsolicitacaodet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codopsolicitacaodet", Integer.valueOf(getCodopsolicitacaodet()));
        return ret;
    }

}
