package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPTIPO")
public class Optipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoptipo";

    @Id
    @Column(name="CODOPTIPO", unique=true, nullable=false, precision=10)
    private int codoptipo;
    @Column(name="DESCOPTIPO", nullable=false, length=250)
    private String descoptipo;
    @Column(name="LEGENDA", nullable=false, precision=10)
    private int legenda;
    @Column(name="OPINVENTARIO", precision=5)
    private short opinventario;
    @Column(name="OPLANCAMENTO", precision=5)
    private short oplancamento;
    @Column(name="OPFINALIZAR", precision=5)
    private short opfinalizar;
    @Column(name="OPCOMISSAO", precision=5)
    private short opcomissao;
    @Column(name="PCPINICIA", precision=5)
    private short pcpinicia;
    @OneToMany(mappedBy="optipo")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="optipo")
    private Set<Opconfentrada> opconfentrada;
    @OneToMany(mappedBy="optipo2")
    private Set<Opconfentrada> opconfentrada2;
    @OneToMany(mappedBy="optipo")
    private Set<Opconfsaida> opconfsaida;
    @OneToMany(mappedBy="optipo2")
    private Set<Opconfsaida> opconfsaida2;
    @OneToMany(mappedBy="optipo3")
    private Set<Opconfsaida> opconfsaida3;
    @OneToMany(mappedBy="optipo")
    private Set<Opevento> opevento;
    @OneToMany(mappedBy="optipo")
    private Set<Opoperacao> opoperacao;
    @OneToMany(mappedBy="optipo")
    private Set<Opsolicitacao> opsolicitacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPNATUREZA", nullable=false)
    private Opnatureza opnatureza;
    @ManyToOne
    @JoinColumn(name="CODFINTIPO")
    private Fintipo fintipo;
    @ManyToOne
    @JoinColumn(name="CODCOMTABELA")
    private Comtabela comtabela;
    @ManyToOne
    @JoinColumn(name="CODFINCOMTIPO")
    private Fintipo fintipo2;
    @ManyToOne
    @JoinColumn(name="CODINVSALDO")
    private Invsaldo invsaldo;
    @ManyToOne
    @JoinColumn(name="CODIMPOBS")
    private Impobs impobs;
    @ManyToOne
    @JoinColumn(name="CODPRDCPLANO")
    private Prdcplano prdcplano;
    @OneToMany(mappedBy="optipo")
    private Set<Optiposaldo> optiposaldo;

    /** Default constructor. */
    public Optipo() {
        super();
    }

    /**
     * Access method for codoptipo.
     *
     * @return the current value of codoptipo
     */
    public int getCodoptipo() {
        return codoptipo;
    }

    /**
     * Setter method for codoptipo.
     *
     * @param aCodoptipo the new value for codoptipo
     */
    public void setCodoptipo(int aCodoptipo) {
        codoptipo = aCodoptipo;
    }

    /**
     * Access method for descoptipo.
     *
     * @return the current value of descoptipo
     */
    public String getDescoptipo() {
        return descoptipo;
    }

    /**
     * Setter method for descoptipo.
     *
     * @param aDescoptipo the new value for descoptipo
     */
    public void setDescoptipo(String aDescoptipo) {
        descoptipo = aDescoptipo;
    }

    /**
     * Access method for legenda.
     *
     * @return the current value of legenda
     */
    public int getLegenda() {
        return legenda;
    }

    /**
     * Setter method for legenda.
     *
     * @param aLegenda the new value for legenda
     */
    public void setLegenda(int aLegenda) {
        legenda = aLegenda;
    }

    /**
     * Access method for opinventario.
     *
     * @return the current value of opinventario
     */
    public short getOpinventario() {
        return opinventario;
    }

    /**
     * Setter method for opinventario.
     *
     * @param aOpinventario the new value for opinventario
     */
    public void setOpinventario(short aOpinventario) {
        opinventario = aOpinventario;
    }

    /**
     * Access method for oplancamento.
     *
     * @return the current value of oplancamento
     */
    public short getOplancamento() {
        return oplancamento;
    }

    /**
     * Setter method for oplancamento.
     *
     * @param aOplancamento the new value for oplancamento
     */
    public void setOplancamento(short aOplancamento) {
        oplancamento = aOplancamento;
    }

    /**
     * Access method for opfinalizar.
     *
     * @return the current value of opfinalizar
     */
    public short getOpfinalizar() {
        return opfinalizar;
    }

    /**
     * Setter method for opfinalizar.
     *
     * @param aOpfinalizar the new value for opfinalizar
     */
    public void setOpfinalizar(short aOpfinalizar) {
        opfinalizar = aOpfinalizar;
    }

    /**
     * Access method for opcomissao.
     *
     * @return the current value of opcomissao
     */
    public short getOpcomissao() {
        return opcomissao;
    }

    /**
     * Setter method for opcomissao.
     *
     * @param aOpcomissao the new value for opcomissao
     */
    public void setOpcomissao(short aOpcomissao) {
        opcomissao = aOpcomissao;
    }

    /**
     * Access method for pcpinicia.
     *
     * @return the current value of pcpinicia
     */
    public short getPcpinicia() {
        return pcpinicia;
    }

    /**
     * Setter method for pcpinicia.
     *
     * @param aPcpinicia the new value for pcpinicia
     */
    public void setPcpinicia(short aPcpinicia) {
        pcpinicia = aPcpinicia;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for opconfentrada.
     *
     * @return the current value of opconfentrada
     */
    public Set<Opconfentrada> getOpconfentrada() {
        return opconfentrada;
    }

    /**
     * Setter method for opconfentrada.
     *
     * @param aOpconfentrada the new value for opconfentrada
     */
    public void setOpconfentrada(Set<Opconfentrada> aOpconfentrada) {
        opconfentrada = aOpconfentrada;
    }

    /**
     * Access method for opconfentrada2.
     *
     * @return the current value of opconfentrada2
     */
    public Set<Opconfentrada> getOpconfentrada2() {
        return opconfentrada2;
    }

    /**
     * Setter method for opconfentrada2.
     *
     * @param aOpconfentrada2 the new value for opconfentrada2
     */
    public void setOpconfentrada2(Set<Opconfentrada> aOpconfentrada2) {
        opconfentrada2 = aOpconfentrada2;
    }

    /**
     * Access method for opconfsaida.
     *
     * @return the current value of opconfsaida
     */
    public Set<Opconfsaida> getOpconfsaida() {
        return opconfsaida;
    }

    /**
     * Setter method for opconfsaida.
     *
     * @param aOpconfsaida the new value for opconfsaida
     */
    public void setOpconfsaida(Set<Opconfsaida> aOpconfsaida) {
        opconfsaida = aOpconfsaida;
    }

    /**
     * Access method for opconfsaida2.
     *
     * @return the current value of opconfsaida2
     */
    public Set<Opconfsaida> getOpconfsaida2() {
        return opconfsaida2;
    }

    /**
     * Setter method for opconfsaida2.
     *
     * @param aOpconfsaida2 the new value for opconfsaida2
     */
    public void setOpconfsaida2(Set<Opconfsaida> aOpconfsaida2) {
        opconfsaida2 = aOpconfsaida2;
    }

    /**
     * Access method for opconfsaida3.
     *
     * @return the current value of opconfsaida3
     */
    public Set<Opconfsaida> getOpconfsaida3() {
        return opconfsaida3;
    }

    /**
     * Setter method for opconfsaida3.
     *
     * @param aOpconfsaida3 the new value for opconfsaida3
     */
    public void setOpconfsaida3(Set<Opconfsaida> aOpconfsaida3) {
        opconfsaida3 = aOpconfsaida3;
    }

    /**
     * Access method for opevento.
     *
     * @return the current value of opevento
     */
    public Set<Opevento> getOpevento() {
        return opevento;
    }

    /**
     * Setter method for opevento.
     *
     * @param aOpevento the new value for opevento
     */
    public void setOpevento(Set<Opevento> aOpevento) {
        opevento = aOpevento;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Set<Opoperacao> getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Set<Opoperacao> aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for opsolicitacao.
     *
     * @return the current value of opsolicitacao
     */
    public Set<Opsolicitacao> getOpsolicitacao() {
        return opsolicitacao;
    }

    /**
     * Setter method for opsolicitacao.
     *
     * @param aOpsolicitacao the new value for opsolicitacao
     */
    public void setOpsolicitacao(Set<Opsolicitacao> aOpsolicitacao) {
        opsolicitacao = aOpsolicitacao;
    }

    /**
     * Access method for opnatureza.
     *
     * @return the current value of opnatureza
     */
    public Opnatureza getOpnatureza() {
        return opnatureza;
    }

    /**
     * Setter method for opnatureza.
     *
     * @param aOpnatureza the new value for opnatureza
     */
    public void setOpnatureza(Opnatureza aOpnatureza) {
        opnatureza = aOpnatureza;
    }

    /**
     * Access method for fintipo.
     *
     * @return the current value of fintipo
     */
    public Fintipo getFintipo() {
        return fintipo;
    }

    /**
     * Setter method for fintipo.
     *
     * @param aFintipo the new value for fintipo
     */
    public void setFintipo(Fintipo aFintipo) {
        fintipo = aFintipo;
    }

    /**
     * Access method for comtabela.
     *
     * @return the current value of comtabela
     */
    public Comtabela getComtabela() {
        return comtabela;
    }

    /**
     * Setter method for comtabela.
     *
     * @param aComtabela the new value for comtabela
     */
    public void setComtabela(Comtabela aComtabela) {
        comtabela = aComtabela;
    }

    /**
     * Access method for fintipo2.
     *
     * @return the current value of fintipo2
     */
    public Fintipo getFintipo2() {
        return fintipo2;
    }

    /**
     * Setter method for fintipo2.
     *
     * @param aFintipo2 the new value for fintipo2
     */
    public void setFintipo2(Fintipo aFintipo2) {
        fintipo2 = aFintipo2;
    }

    /**
     * Access method for invsaldo.
     *
     * @return the current value of invsaldo
     */
    public Invsaldo getInvsaldo() {
        return invsaldo;
    }

    /**
     * Setter method for invsaldo.
     *
     * @param aInvsaldo the new value for invsaldo
     */
    public void setInvsaldo(Invsaldo aInvsaldo) {
        invsaldo = aInvsaldo;
    }

    /**
     * Access method for impobs.
     *
     * @return the current value of impobs
     */
    public Impobs getImpobs() {
        return impobs;
    }

    /**
     * Setter method for impobs.
     *
     * @param aImpobs the new value for impobs
     */
    public void setImpobs(Impobs aImpobs) {
        impobs = aImpobs;
    }

    /**
     * Access method for prdcplano.
     *
     * @return the current value of prdcplano
     */
    public Prdcplano getPrdcplano() {
        return prdcplano;
    }

    /**
     * Setter method for prdcplano.
     *
     * @param aPrdcplano the new value for prdcplano
     */
    public void setPrdcplano(Prdcplano aPrdcplano) {
        prdcplano = aPrdcplano;
    }

    /**
     * Access method for optiposaldo.
     *
     * @return the current value of optiposaldo
     */
    public Set<Optiposaldo> getOptiposaldo() {
        return optiposaldo;
    }

    /**
     * Setter method for optiposaldo.
     *
     * @param aOptiposaldo the new value for optiposaldo
     */
    public void setOptiposaldo(Set<Optiposaldo> aOptiposaldo) {
        optiposaldo = aOptiposaldo;
    }

    /**
     * Compares the key for this instance with another Optipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Optipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Optipo)) {
            return false;
        }
        Optipo that = (Optipo) other;
        if (this.getCodoptipo() != that.getCodoptipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Optipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Optipo)) return false;
        return this.equalKeys(other) && ((Optipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoptipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Optipo |");
        sb.append(" codoptipo=").append(getCodoptipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoptipo", Integer.valueOf(getCodoptipo()));
        return ret;
    }

}
