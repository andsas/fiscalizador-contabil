package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPTIPOSALDO")
public class Optiposaldo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoptiposaldo";

    @Id
    @Column(name="CODOPTIPOSALDO", unique=true, nullable=false, precision=10)
    private int codoptiposaldo;
    @Column(name="TIPOOPTIPOSALDO", precision=5)
    private short tipooptiposaldo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTIPO", nullable=false)
    private Optipo optipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODINVSALDO", nullable=false)
    private Invsaldo invsaldo;

    /** Default constructor. */
    public Optiposaldo() {
        super();
    }

    /**
     * Access method for codoptiposaldo.
     *
     * @return the current value of codoptiposaldo
     */
    public int getCodoptiposaldo() {
        return codoptiposaldo;
    }

    /**
     * Setter method for codoptiposaldo.
     *
     * @param aCodoptiposaldo the new value for codoptiposaldo
     */
    public void setCodoptiposaldo(int aCodoptiposaldo) {
        codoptiposaldo = aCodoptiposaldo;
    }

    /**
     * Access method for tipooptiposaldo.
     *
     * @return the current value of tipooptiposaldo
     */
    public short getTipooptiposaldo() {
        return tipooptiposaldo;
    }

    /**
     * Setter method for tipooptiposaldo.
     *
     * @param aTipooptiposaldo the new value for tipooptiposaldo
     */
    public void setTipooptiposaldo(short aTipooptiposaldo) {
        tipooptiposaldo = aTipooptiposaldo;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Optipo getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Optipo aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Access method for invsaldo.
     *
     * @return the current value of invsaldo
     */
    public Invsaldo getInvsaldo() {
        return invsaldo;
    }

    /**
     * Setter method for invsaldo.
     *
     * @param aInvsaldo the new value for invsaldo
     */
    public void setInvsaldo(Invsaldo aInvsaldo) {
        invsaldo = aInvsaldo;
    }

    /**
     * Compares the key for this instance with another Optiposaldo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Optiposaldo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Optiposaldo)) {
            return false;
        }
        Optiposaldo that = (Optiposaldo) other;
        if (this.getCodoptiposaldo() != that.getCodoptiposaldo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Optiposaldo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Optiposaldo)) return false;
        return this.equalKeys(other) && ((Optiposaldo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoptiposaldo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Optiposaldo |");
        sb.append(" codoptiposaldo=").append(getCodoptiposaldo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoptiposaldo", Integer.valueOf(getCodoptiposaldo()));
        return ret;
    }

}
