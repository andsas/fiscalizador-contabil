package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPTRANSACAO")
public class Optransacao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoptransacao";

    @Id
    @Column(name="CODOPTRANSACAO", unique=true, nullable=false, precision=10)
    private int codoptransacao;
    @Column(name="ID", length=40)
    private String id;
    @Column(name="SITUACAO", nullable=false, precision=5)
    private short situacao;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @Column(name="CODCONFLOGRADOUROEMPR", precision=10)
    private int codconflogradouroempr;
    @Column(name="CODAGRETIRADA", precision=10)
    private int codagretirada;
    @Column(name="CODAGENTREGA", precision=10)
    private int codagentrega;
    @Column(name="CODAGTRANSPORTADORA", precision=10)
    private int codagtransportadora;
    @Column(name="CODAGVENDEDOR", precision=10)
    private int codagvendedor;
    @Column(name="CODCONFLOGRADOUROVENDEDOR", precision=10)
    private int codconflogradourovendedor;
    @Column(name="TIPOINDPAG", nullable=false, precision=5)
    private short tipoindpag;
    @Column(name="TIPODESTINO", nullable=false, precision=5)
    private short tipodestino;
    @Column(name="TIPOIMPRESSAO", nullable=false, precision=5)
    private short tipoimpressao;
    @Column(name="TIPOEMISSAO", nullable=false, precision=5)
    private short tipoemissao;
    @Column(name="TIPOAMBIENTE", nullable=false, precision=5)
    private short tipoambiente;
    @Column(name="TIPOFINALIDADE", nullable=false, precision=5)
    private short tipofinalidade;
    @Column(name="TIPOCONSUMIDORFINAL", nullable=false, precision=5)
    private short tipoconsumidorfinal;
    @Column(name="TIPOPRESENCIAL", nullable=false, precision=5)
    private short tipopresencial;
    @Column(name="TIPOAPLICATIVOEMISSAO", nullable=false, precision=5)
    private short tipoaplicativoemissao;
    @Column(name="DATATRANSACAO", nullable=false)
    private Timestamp datatransacao;
    @Column(name="DATAEMISSAO", nullable=false)
    private Timestamp dataemissao;
    @Column(name="DATAMOVIMENTO", nullable=false)
    private Timestamp datamovimento;
    @Column(name="EXPEMBCODCONFUF", length=4)
    private String expembcodconfuf;
    @Column(name="EXPEMBLOCAL", length=250)
    private String expemblocal;
    @Column(name="EXPSAIDAPAISCODCONFUF", length=4)
    private String expsaidapaiscodconfuf;
    @Column(name="EXPLOCALEXPORTA", length=250)
    private String explocalexporta;
    @Column(name="EXPLOCALDESPACHO", length=250)
    private String explocaldespacho;
    @Column(name="COMPRAPUBEMPENHO", length=60)
    private String comprapubempenho;
    @Column(name="COMPRAPUBPEDIDO", length=60)
    private String comprapubpedido;
    @Column(name="COMPRAPUBCONTRATO", length=60)
    private String comprapubcontrato;
    @Column(name="ICMSTOTBC", precision=15, scale=4)
    private BigDecimal icmstotbc;
    @Column(name="ICMSTOTICMS", precision=15, scale=4)
    private BigDecimal icmstoticms;
    @Column(name="ICMSTOTDESONICMS", precision=15, scale=4)
    private BigDecimal icmstotdesonicms;
    @Column(name="ICMSTOTBASEST", precision=15, scale=4)
    private BigDecimal icmstotbasest;
    @Column(name="ICMSTOTST", precision=15, scale=4)
    private BigDecimal icmstotst;
    @Column(name="ICMSTOTPROD", precision=15, scale=4)
    private BigDecimal icmstotprod;
    @Column(name="ICMSTOTFRETE", precision=15, scale=4)
    private BigDecimal icmstotfrete;
    @Column(name="ICMSTOTSEG", precision=15, scale=4)
    private BigDecimal icmstotseg;
    @Column(name="ICMSTOTDESC", precision=15, scale=4)
    private BigDecimal icmstotdesc;
    @Column(name="ICMSTOTII", precision=15, scale=4)
    private BigDecimal icmstotii;
    @Column(name="ICMSTOTIPI", precision=15, scale=4)
    private BigDecimal icmstotipi;
    @Column(name="ICMSTOTPIS", precision=15, scale=4)
    private BigDecimal icmstotpis;
    @Column(name="ICMSTOTCOFINS", precision=15, scale=4)
    private BigDecimal icmstotcofins;
    @Column(name="ICMSTOTOUTRO", precision=15, scale=4)
    private BigDecimal icmstotoutro;
    @Column(name="ICMSTOTNF", precision=15, scale=4)
    private BigDecimal icmstotnf;
    @Column(name="ICMSTOTTOTTRIB", precision=15, scale=4)
    private BigDecimal icmstottottrib;
    @Column(name="ISSQNTOTSERV", precision=15, scale=4)
    private BigDecimal issqntotserv;
    @Column(name="ISSQNTOTBC", precision=15, scale=4)
    private BigDecimal issqntotbc;
    @Column(name="ISSQNTOTISS", precision=15, scale=4)
    private BigDecimal issqntotiss;
    @Column(name="ISSQNTOTPIS", precision=15, scale=4)
    private BigDecimal issqntotpis;
    @Column(name="ISSQNTOTCOFINS", precision=15, scale=4)
    private BigDecimal issqntotcofins;
    @Column(name="ISSQNTOTDCOMPET", precision=15, scale=4)
    private BigDecimal issqntotdcompet;
    @Column(name="ISSQNTOTDEDUCAO", precision=15, scale=4)
    private BigDecimal issqntotdeducao;
    @Column(name="ISSQNTOTOUTRO", precision=15, scale=4)
    private BigDecimal issqntotoutro;
    @Column(name="ISSQNTOTDESCINCOND", precision=15, scale=4)
    private BigDecimal issqntotdescincond;
    @Column(name="ISSQNTOTDESCCOND", precision=15, scale=4)
    private BigDecimal issqntotdesccond;
    @Column(name="RETTRIBRETPIS", precision=15, scale=4)
    private BigDecimal rettribretpis;
    @Column(name="RETTRIBRETCOFINS", precision=15, scale=4)
    private BigDecimal rettribretcofins;
    @Column(name="RETTRIBRETCSLL", precision=15, scale=4)
    private BigDecimal rettribretcsll;
    @Column(name="RETTRIBBCIRRF", precision=15, scale=4)
    private BigDecimal rettribbcirrf;
    @Column(name="RETTRIBIRRF", precision=15, scale=4)
    private BigDecimal rettribirrf;
    @Column(name="RETTRIBBCRETPREV", precision=15, scale=4)
    private BigDecimal rettribbcretprev;
    @Column(name="RETTRIBRETPREV", precision=15, scale=4)
    private BigDecimal rettribretprev;
    @Column(name="COBRFATURA", length=250)
    private String cobrfatura;
    @Column(name="COBRVALORORIGINAL", precision=15, scale=4)
    private BigDecimal cobrvalororiginal;
    @Column(name="COBRVALORLIQ", precision=15, scale=4)
    private BigDecimal cobrvalorliq;
    @Column(name="COBRVALORDESC", precision=15, scale=4)
    private BigDecimal cobrvalordesc;
    @Column(name="NFECHAVE", length=60)
    private String nfechave;
    @Column(name="NFECSTAT", precision=10)
    private int nfecstat;
    @Column(name="NFENF", precision=10)
    private int nfenf;
    @Column(name="NFEPROTOCOLO", length=60)
    private String nfeprotocolo;
    @Column(name="NFECONTROLELOTE", precision=10)
    private int nfecontrolelote;
    @Column(name="TRANSPTIPOFRETE", precision=5)
    private short transptipofrete;
    @Column(name="TRANSPPLACA", length=40)
    private String transpplaca;
    @Column(name="TRANSPUFPLACA", length=4)
    private String transpufplaca;
    @Column(name="TRANSPRNTC", precision=10)
    private int transprntc;
    @Column(name="TRANSPVAGAO", length=40)
    private String transpvagao;
    @Column(name="TRANSPBALSA", length=40)
    private String transpbalsa;
    @Column(name="TRANSPIDAERONAVE", length=40)
    private String transpidaeronave;
    @Column(name="TRANSPPESOLIQ", precision=15, scale=4)
    private BigDecimal transppesoliq;
    @Column(name="TRANSPPESOBRT", precision=15, scale=4)
    private BigDecimal transppesobrt;
    @Column(name="NFEINFADICIONALMANUAL")
    private String nfeinfadicionalmanual;
    @Column(name="NFSERPS", precision=10)
    private int nfserps;
    @Column(name="NFSELOTE", precision=10)
    private int nfselote;
    @Column(name="NFSEPROTOCOLO", length=40)
    private String nfseprotocolo;
    @Column(name="NFSEVERIFICACAO", length=40)
    private String nfseverificacao;
    @OneToMany(mappedBy="optransacao")
    private Set<Crmchamado> crmchamado;
    @OneToMany(mappedBy="optransacao")
    private Set<Ctbbem> ctbbem;
    @OneToMany(mappedBy="optransacao")
    private Set<Ctbbemdet> ctbbemdet;
    @OneToMany(mappedBy="optransacao")
    private Set<Ctblancamento> ctblancamento;
    @OneToMany(mappedBy="optransacao")
    private Set<Finlancamento> finlancamento;
    @OneToMany(mappedBy="optransacao")
    private Set<Finoperadora> finoperadora;
    @OneToMany(mappedBy="optransacao")
    private Set<Fises> fises;
    @OneToMany(mappedBy="optransacao")
    private Set<Opnfe> opnfe;
    @OneToMany(mappedBy="optransacao")
    private Set<Opnfeimporta> opnfeimporta;
    @OneToMany(mappedBy="optransacao")
    private Set<Opsolicitacao> opsolicitacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPOPERACAO", nullable=false)
    private Opoperacao opoperacao;
    @ManyToOne
    @JoinColumn(name="CODOPEVENTOULTIMO")
    private Optransacaoevento optransacaoevento;
    @ManyToOne
    @JoinColumn(name="CODCONFLOGRADOURORETIRADA")
    private Conflogradouro conflogradouro2;
    @ManyToOne
    @JoinColumn(name="CODCONFLOGRADOUROENTREGA")
    private Conflogradouro conflogradouro3;
    @ManyToOne
    @JoinColumn(name="CODCONFLOGRADOUROTRANSP")
    private Conflogradouro conflogradouro4;
    @ManyToOne
    @JoinColumn(name="CODGERVEICULO")
    private Gerveiculo gerveiculo;
    @ManyToOne
    @JoinColumn(name="CODIMPOBSFISCO")
    private Impobs impobs;
    @ManyToOne
    @JoinColumn(name="CODIMPOBSADICIONAL")
    private Impobs impobs2;
    @ManyToOne
    @JoinColumn(name="CODIMPOBSJUSTIFICATIVA")
    private Impobs impobs3;
    @ManyToOne
    @JoinColumn(name="CODPRTABELA")
    private Prtabela prtabela;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODCONFLOGRADOUROAGENTE")
    private Conflogradouro conflogradouro;
    @ManyToOne
    @JoinColumn(name="CODFINCAIXA")
    private Fincaixa fincaixa;
    @ManyToOne
    @JoinColumn(name="CODOPORC")
    private Oporc oporc;
    @ManyToOne
    @JoinColumn(name="CODPRDCORDEM")
    private Prdcordem prdcordem2;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPDOCSERIE", nullable=false)
    private Opdocserie opdocserie;
    @ManyToOne
    @JoinColumn(name="CODCONFCIDADEFG")
    private Confcidade confcidade;
    @OneToMany(mappedBy="optransacao")
    private Set<Optransacaocobr> optransacaocobr;
    @OneToMany(mappedBy="optransacao")
    private Set<Optransacaodet> optransacaodet;
    @OneToMany(mappedBy="optransacao2")
    private Set<Optransacaodet> optransacaodet2;
    @OneToMany(mappedBy="optransacao")
    private Set<Optransacaoresp> optransacaoresp;
    @OneToMany(mappedBy="optransacao")
    private Set<Optransacaovinc> optransacaovinc;
    @OneToMany(mappedBy="optransacao2")
    private Set<Optransacaovinc> optransacaovinc2;
    @OneToMany(mappedBy="optransacao")
    private Set<Prdcordem> prdcordem;

    /** Default constructor. */
    public Optransacao() {
        super();
    }

    /**
     * Access method for codoptransacao.
     *
     * @return the current value of codoptransacao
     */
    public int getCodoptransacao() {
        return codoptransacao;
    }

    /**
     * Setter method for codoptransacao.
     *
     * @param aCodoptransacao the new value for codoptransacao
     */
    public void setCodoptransacao(int aCodoptransacao) {
        codoptransacao = aCodoptransacao;
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(String aId) {
        id = aId;
    }

    /**
     * Access method for situacao.
     *
     * @return the current value of situacao
     */
    public short getSituacao() {
        return situacao;
    }

    /**
     * Setter method for situacao.
     *
     * @param aSituacao the new value for situacao
     */
    public void setSituacao(short aSituacao) {
        situacao = aSituacao;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for codconflogradouroempr.
     *
     * @return the current value of codconflogradouroempr
     */
    public int getCodconflogradouroempr() {
        return codconflogradouroempr;
    }

    /**
     * Setter method for codconflogradouroempr.
     *
     * @param aCodconflogradouroempr the new value for codconflogradouroempr
     */
    public void setCodconflogradouroempr(int aCodconflogradouroempr) {
        codconflogradouroempr = aCodconflogradouroempr;
    }

    /**
     * Access method for codagretirada.
     *
     * @return the current value of codagretirada
     */
    public int getCodagretirada() {
        return codagretirada;
    }

    /**
     * Setter method for codagretirada.
     *
     * @param aCodagretirada the new value for codagretirada
     */
    public void setCodagretirada(int aCodagretirada) {
        codagretirada = aCodagretirada;
    }

    /**
     * Access method for codagentrega.
     *
     * @return the current value of codagentrega
     */
    public int getCodagentrega() {
        return codagentrega;
    }

    /**
     * Setter method for codagentrega.
     *
     * @param aCodagentrega the new value for codagentrega
     */
    public void setCodagentrega(int aCodagentrega) {
        codagentrega = aCodagentrega;
    }

    /**
     * Access method for codagtransportadora.
     *
     * @return the current value of codagtransportadora
     */
    public int getCodagtransportadora() {
        return codagtransportadora;
    }

    /**
     * Setter method for codagtransportadora.
     *
     * @param aCodagtransportadora the new value for codagtransportadora
     */
    public void setCodagtransportadora(int aCodagtransportadora) {
        codagtransportadora = aCodagtransportadora;
    }

    /**
     * Access method for codagvendedor.
     *
     * @return the current value of codagvendedor
     */
    public int getCodagvendedor() {
        return codagvendedor;
    }

    /**
     * Setter method for codagvendedor.
     *
     * @param aCodagvendedor the new value for codagvendedor
     */
    public void setCodagvendedor(int aCodagvendedor) {
        codagvendedor = aCodagvendedor;
    }

    /**
     * Access method for codconflogradourovendedor.
     *
     * @return the current value of codconflogradourovendedor
     */
    public int getCodconflogradourovendedor() {
        return codconflogradourovendedor;
    }

    /**
     * Setter method for codconflogradourovendedor.
     *
     * @param aCodconflogradourovendedor the new value for codconflogradourovendedor
     */
    public void setCodconflogradourovendedor(int aCodconflogradourovendedor) {
        codconflogradourovendedor = aCodconflogradourovendedor;
    }

    /**
     * Access method for tipoindpag.
     *
     * @return the current value of tipoindpag
     */
    public short getTipoindpag() {
        return tipoindpag;
    }

    /**
     * Setter method for tipoindpag.
     *
     * @param aTipoindpag the new value for tipoindpag
     */
    public void setTipoindpag(short aTipoindpag) {
        tipoindpag = aTipoindpag;
    }

    /**
     * Access method for tipodestino.
     *
     * @return the current value of tipodestino
     */
    public short getTipodestino() {
        return tipodestino;
    }

    /**
     * Setter method for tipodestino.
     *
     * @param aTipodestino the new value for tipodestino
     */
    public void setTipodestino(short aTipodestino) {
        tipodestino = aTipodestino;
    }

    /**
     * Access method for tipoimpressao.
     *
     * @return the current value of tipoimpressao
     */
    public short getTipoimpressao() {
        return tipoimpressao;
    }

    /**
     * Setter method for tipoimpressao.
     *
     * @param aTipoimpressao the new value for tipoimpressao
     */
    public void setTipoimpressao(short aTipoimpressao) {
        tipoimpressao = aTipoimpressao;
    }

    /**
     * Access method for tipoemissao.
     *
     * @return the current value of tipoemissao
     */
    public short getTipoemissao() {
        return tipoemissao;
    }

    /**
     * Setter method for tipoemissao.
     *
     * @param aTipoemissao the new value for tipoemissao
     */
    public void setTipoemissao(short aTipoemissao) {
        tipoemissao = aTipoemissao;
    }

    /**
     * Access method for tipoambiente.
     *
     * @return the current value of tipoambiente
     */
    public short getTipoambiente() {
        return tipoambiente;
    }

    /**
     * Setter method for tipoambiente.
     *
     * @param aTipoambiente the new value for tipoambiente
     */
    public void setTipoambiente(short aTipoambiente) {
        tipoambiente = aTipoambiente;
    }

    /**
     * Access method for tipofinalidade.
     *
     * @return the current value of tipofinalidade
     */
    public short getTipofinalidade() {
        return tipofinalidade;
    }

    /**
     * Setter method for tipofinalidade.
     *
     * @param aTipofinalidade the new value for tipofinalidade
     */
    public void setTipofinalidade(short aTipofinalidade) {
        tipofinalidade = aTipofinalidade;
    }

    /**
     * Access method for tipoconsumidorfinal.
     *
     * @return the current value of tipoconsumidorfinal
     */
    public short getTipoconsumidorfinal() {
        return tipoconsumidorfinal;
    }

    /**
     * Setter method for tipoconsumidorfinal.
     *
     * @param aTipoconsumidorfinal the new value for tipoconsumidorfinal
     */
    public void setTipoconsumidorfinal(short aTipoconsumidorfinal) {
        tipoconsumidorfinal = aTipoconsumidorfinal;
    }

    /**
     * Access method for tipopresencial.
     *
     * @return the current value of tipopresencial
     */
    public short getTipopresencial() {
        return tipopresencial;
    }

    /**
     * Setter method for tipopresencial.
     *
     * @param aTipopresencial the new value for tipopresencial
     */
    public void setTipopresencial(short aTipopresencial) {
        tipopresencial = aTipopresencial;
    }

    /**
     * Access method for tipoaplicativoemissao.
     *
     * @return the current value of tipoaplicativoemissao
     */
    public short getTipoaplicativoemissao() {
        return tipoaplicativoemissao;
    }

    /**
     * Setter method for tipoaplicativoemissao.
     *
     * @param aTipoaplicativoemissao the new value for tipoaplicativoemissao
     */
    public void setTipoaplicativoemissao(short aTipoaplicativoemissao) {
        tipoaplicativoemissao = aTipoaplicativoemissao;
    }

    /**
     * Access method for datatransacao.
     *
     * @return the current value of datatransacao
     */
    public Timestamp getDatatransacao() {
        return datatransacao;
    }

    /**
     * Setter method for datatransacao.
     *
     * @param aDatatransacao the new value for datatransacao
     */
    public void setDatatransacao(Timestamp aDatatransacao) {
        datatransacao = aDatatransacao;
    }

    /**
     * Access method for dataemissao.
     *
     * @return the current value of dataemissao
     */
    public Timestamp getDataemissao() {
        return dataemissao;
    }

    /**
     * Setter method for dataemissao.
     *
     * @param aDataemissao the new value for dataemissao
     */
    public void setDataemissao(Timestamp aDataemissao) {
        dataemissao = aDataemissao;
    }

    /**
     * Access method for datamovimento.
     *
     * @return the current value of datamovimento
     */
    public Timestamp getDatamovimento() {
        return datamovimento;
    }

    /**
     * Setter method for datamovimento.
     *
     * @param aDatamovimento the new value for datamovimento
     */
    public void setDatamovimento(Timestamp aDatamovimento) {
        datamovimento = aDatamovimento;
    }

    /**
     * Access method for expembcodconfuf.
     *
     * @return the current value of expembcodconfuf
     */
    public String getExpembcodconfuf() {
        return expembcodconfuf;
    }

    /**
     * Setter method for expembcodconfuf.
     *
     * @param aExpembcodconfuf the new value for expembcodconfuf
     */
    public void setExpembcodconfuf(String aExpembcodconfuf) {
        expembcodconfuf = aExpembcodconfuf;
    }

    /**
     * Access method for expemblocal.
     *
     * @return the current value of expemblocal
     */
    public String getExpemblocal() {
        return expemblocal;
    }

    /**
     * Setter method for expemblocal.
     *
     * @param aExpemblocal the new value for expemblocal
     */
    public void setExpemblocal(String aExpemblocal) {
        expemblocal = aExpemblocal;
    }

    /**
     * Access method for expsaidapaiscodconfuf.
     *
     * @return the current value of expsaidapaiscodconfuf
     */
    public String getExpsaidapaiscodconfuf() {
        return expsaidapaiscodconfuf;
    }

    /**
     * Setter method for expsaidapaiscodconfuf.
     *
     * @param aExpsaidapaiscodconfuf the new value for expsaidapaiscodconfuf
     */
    public void setExpsaidapaiscodconfuf(String aExpsaidapaiscodconfuf) {
        expsaidapaiscodconfuf = aExpsaidapaiscodconfuf;
    }

    /**
     * Access method for explocalexporta.
     *
     * @return the current value of explocalexporta
     */
    public String getExplocalexporta() {
        return explocalexporta;
    }

    /**
     * Setter method for explocalexporta.
     *
     * @param aExplocalexporta the new value for explocalexporta
     */
    public void setExplocalexporta(String aExplocalexporta) {
        explocalexporta = aExplocalexporta;
    }

    /**
     * Access method for explocaldespacho.
     *
     * @return the current value of explocaldespacho
     */
    public String getExplocaldespacho() {
        return explocaldespacho;
    }

    /**
     * Setter method for explocaldespacho.
     *
     * @param aExplocaldespacho the new value for explocaldespacho
     */
    public void setExplocaldespacho(String aExplocaldespacho) {
        explocaldespacho = aExplocaldespacho;
    }

    /**
     * Access method for comprapubempenho.
     *
     * @return the current value of comprapubempenho
     */
    public String getComprapubempenho() {
        return comprapubempenho;
    }

    /**
     * Setter method for comprapubempenho.
     *
     * @param aComprapubempenho the new value for comprapubempenho
     */
    public void setComprapubempenho(String aComprapubempenho) {
        comprapubempenho = aComprapubempenho;
    }

    /**
     * Access method for comprapubpedido.
     *
     * @return the current value of comprapubpedido
     */
    public String getComprapubpedido() {
        return comprapubpedido;
    }

    /**
     * Setter method for comprapubpedido.
     *
     * @param aComprapubpedido the new value for comprapubpedido
     */
    public void setComprapubpedido(String aComprapubpedido) {
        comprapubpedido = aComprapubpedido;
    }

    /**
     * Access method for comprapubcontrato.
     *
     * @return the current value of comprapubcontrato
     */
    public String getComprapubcontrato() {
        return comprapubcontrato;
    }

    /**
     * Setter method for comprapubcontrato.
     *
     * @param aComprapubcontrato the new value for comprapubcontrato
     */
    public void setComprapubcontrato(String aComprapubcontrato) {
        comprapubcontrato = aComprapubcontrato;
    }

    /**
     * Access method for icmstotbc.
     *
     * @return the current value of icmstotbc
     */
    public BigDecimal getIcmstotbc() {
        return icmstotbc;
    }

    /**
     * Setter method for icmstotbc.
     *
     * @param aIcmstotbc the new value for icmstotbc
     */
    public void setIcmstotbc(BigDecimal aIcmstotbc) {
        icmstotbc = aIcmstotbc;
    }

    /**
     * Access method for icmstoticms.
     *
     * @return the current value of icmstoticms
     */
    public BigDecimal getIcmstoticms() {
        return icmstoticms;
    }

    /**
     * Setter method for icmstoticms.
     *
     * @param aIcmstoticms the new value for icmstoticms
     */
    public void setIcmstoticms(BigDecimal aIcmstoticms) {
        icmstoticms = aIcmstoticms;
    }

    /**
     * Access method for icmstotdesonicms.
     *
     * @return the current value of icmstotdesonicms
     */
    public BigDecimal getIcmstotdesonicms() {
        return icmstotdesonicms;
    }

    /**
     * Setter method for icmstotdesonicms.
     *
     * @param aIcmstotdesonicms the new value for icmstotdesonicms
     */
    public void setIcmstotdesonicms(BigDecimal aIcmstotdesonicms) {
        icmstotdesonicms = aIcmstotdesonicms;
    }

    /**
     * Access method for icmstotbasest.
     *
     * @return the current value of icmstotbasest
     */
    public BigDecimal getIcmstotbasest() {
        return icmstotbasest;
    }

    /**
     * Setter method for icmstotbasest.
     *
     * @param aIcmstotbasest the new value for icmstotbasest
     */
    public void setIcmstotbasest(BigDecimal aIcmstotbasest) {
        icmstotbasest = aIcmstotbasest;
    }

    /**
     * Access method for icmstotst.
     *
     * @return the current value of icmstotst
     */
    public BigDecimal getIcmstotst() {
        return icmstotst;
    }

    /**
     * Setter method for icmstotst.
     *
     * @param aIcmstotst the new value for icmstotst
     */
    public void setIcmstotst(BigDecimal aIcmstotst) {
        icmstotst = aIcmstotst;
    }

    /**
     * Access method for icmstotprod.
     *
     * @return the current value of icmstotprod
     */
    public BigDecimal getIcmstotprod() {
        return icmstotprod;
    }

    /**
     * Setter method for icmstotprod.
     *
     * @param aIcmstotprod the new value for icmstotprod
     */
    public void setIcmstotprod(BigDecimal aIcmstotprod) {
        icmstotprod = aIcmstotprod;
    }

    /**
     * Access method for icmstotfrete.
     *
     * @return the current value of icmstotfrete
     */
    public BigDecimal getIcmstotfrete() {
        return icmstotfrete;
    }

    /**
     * Setter method for icmstotfrete.
     *
     * @param aIcmstotfrete the new value for icmstotfrete
     */
    public void setIcmstotfrete(BigDecimal aIcmstotfrete) {
        icmstotfrete = aIcmstotfrete;
    }

    /**
     * Access method for icmstotseg.
     *
     * @return the current value of icmstotseg
     */
    public BigDecimal getIcmstotseg() {
        return icmstotseg;
    }

    /**
     * Setter method for icmstotseg.
     *
     * @param aIcmstotseg the new value for icmstotseg
     */
    public void setIcmstotseg(BigDecimal aIcmstotseg) {
        icmstotseg = aIcmstotseg;
    }

    /**
     * Access method for icmstotdesc.
     *
     * @return the current value of icmstotdesc
     */
    public BigDecimal getIcmstotdesc() {
        return icmstotdesc;
    }

    /**
     * Setter method for icmstotdesc.
     *
     * @param aIcmstotdesc the new value for icmstotdesc
     */
    public void setIcmstotdesc(BigDecimal aIcmstotdesc) {
        icmstotdesc = aIcmstotdesc;
    }

    /**
     * Access method for icmstotii.
     *
     * @return the current value of icmstotii
     */
    public BigDecimal getIcmstotii() {
        return icmstotii;
    }

    /**
     * Setter method for icmstotii.
     *
     * @param aIcmstotii the new value for icmstotii
     */
    public void setIcmstotii(BigDecimal aIcmstotii) {
        icmstotii = aIcmstotii;
    }

    /**
     * Access method for icmstotipi.
     *
     * @return the current value of icmstotipi
     */
    public BigDecimal getIcmstotipi() {
        return icmstotipi;
    }

    /**
     * Setter method for icmstotipi.
     *
     * @param aIcmstotipi the new value for icmstotipi
     */
    public void setIcmstotipi(BigDecimal aIcmstotipi) {
        icmstotipi = aIcmstotipi;
    }

    /**
     * Access method for icmstotpis.
     *
     * @return the current value of icmstotpis
     */
    public BigDecimal getIcmstotpis() {
        return icmstotpis;
    }

    /**
     * Setter method for icmstotpis.
     *
     * @param aIcmstotpis the new value for icmstotpis
     */
    public void setIcmstotpis(BigDecimal aIcmstotpis) {
        icmstotpis = aIcmstotpis;
    }

    /**
     * Access method for icmstotcofins.
     *
     * @return the current value of icmstotcofins
     */
    public BigDecimal getIcmstotcofins() {
        return icmstotcofins;
    }

    /**
     * Setter method for icmstotcofins.
     *
     * @param aIcmstotcofins the new value for icmstotcofins
     */
    public void setIcmstotcofins(BigDecimal aIcmstotcofins) {
        icmstotcofins = aIcmstotcofins;
    }

    /**
     * Access method for icmstotoutro.
     *
     * @return the current value of icmstotoutro
     */
    public BigDecimal getIcmstotoutro() {
        return icmstotoutro;
    }

    /**
     * Setter method for icmstotoutro.
     *
     * @param aIcmstotoutro the new value for icmstotoutro
     */
    public void setIcmstotoutro(BigDecimal aIcmstotoutro) {
        icmstotoutro = aIcmstotoutro;
    }

    /**
     * Access method for icmstotnf.
     *
     * @return the current value of icmstotnf
     */
    public BigDecimal getIcmstotnf() {
        return icmstotnf;
    }

    /**
     * Setter method for icmstotnf.
     *
     * @param aIcmstotnf the new value for icmstotnf
     */
    public void setIcmstotnf(BigDecimal aIcmstotnf) {
        icmstotnf = aIcmstotnf;
    }

    /**
     * Access method for icmstottottrib.
     *
     * @return the current value of icmstottottrib
     */
    public BigDecimal getIcmstottottrib() {
        return icmstottottrib;
    }

    /**
     * Setter method for icmstottottrib.
     *
     * @param aIcmstottottrib the new value for icmstottottrib
     */
    public void setIcmstottottrib(BigDecimal aIcmstottottrib) {
        icmstottottrib = aIcmstottottrib;
    }

    /**
     * Access method for issqntotserv.
     *
     * @return the current value of issqntotserv
     */
    public BigDecimal getIssqntotserv() {
        return issqntotserv;
    }

    /**
     * Setter method for issqntotserv.
     *
     * @param aIssqntotserv the new value for issqntotserv
     */
    public void setIssqntotserv(BigDecimal aIssqntotserv) {
        issqntotserv = aIssqntotserv;
    }

    /**
     * Access method for issqntotbc.
     *
     * @return the current value of issqntotbc
     */
    public BigDecimal getIssqntotbc() {
        return issqntotbc;
    }

    /**
     * Setter method for issqntotbc.
     *
     * @param aIssqntotbc the new value for issqntotbc
     */
    public void setIssqntotbc(BigDecimal aIssqntotbc) {
        issqntotbc = aIssqntotbc;
    }

    /**
     * Access method for issqntotiss.
     *
     * @return the current value of issqntotiss
     */
    public BigDecimal getIssqntotiss() {
        return issqntotiss;
    }

    /**
     * Setter method for issqntotiss.
     *
     * @param aIssqntotiss the new value for issqntotiss
     */
    public void setIssqntotiss(BigDecimal aIssqntotiss) {
        issqntotiss = aIssqntotiss;
    }

    /**
     * Access method for issqntotpis.
     *
     * @return the current value of issqntotpis
     */
    public BigDecimal getIssqntotpis() {
        return issqntotpis;
    }

    /**
     * Setter method for issqntotpis.
     *
     * @param aIssqntotpis the new value for issqntotpis
     */
    public void setIssqntotpis(BigDecimal aIssqntotpis) {
        issqntotpis = aIssqntotpis;
    }

    /**
     * Access method for issqntotcofins.
     *
     * @return the current value of issqntotcofins
     */
    public BigDecimal getIssqntotcofins() {
        return issqntotcofins;
    }

    /**
     * Setter method for issqntotcofins.
     *
     * @param aIssqntotcofins the new value for issqntotcofins
     */
    public void setIssqntotcofins(BigDecimal aIssqntotcofins) {
        issqntotcofins = aIssqntotcofins;
    }

    /**
     * Access method for issqntotdcompet.
     *
     * @return the current value of issqntotdcompet
     */
    public BigDecimal getIssqntotdcompet() {
        return issqntotdcompet;
    }

    /**
     * Setter method for issqntotdcompet.
     *
     * @param aIssqntotdcompet the new value for issqntotdcompet
     */
    public void setIssqntotdcompet(BigDecimal aIssqntotdcompet) {
        issqntotdcompet = aIssqntotdcompet;
    }

    /**
     * Access method for issqntotdeducao.
     *
     * @return the current value of issqntotdeducao
     */
    public BigDecimal getIssqntotdeducao() {
        return issqntotdeducao;
    }

    /**
     * Setter method for issqntotdeducao.
     *
     * @param aIssqntotdeducao the new value for issqntotdeducao
     */
    public void setIssqntotdeducao(BigDecimal aIssqntotdeducao) {
        issqntotdeducao = aIssqntotdeducao;
    }

    /**
     * Access method for issqntotoutro.
     *
     * @return the current value of issqntotoutro
     */
    public BigDecimal getIssqntotoutro() {
        return issqntotoutro;
    }

    /**
     * Setter method for issqntotoutro.
     *
     * @param aIssqntotoutro the new value for issqntotoutro
     */
    public void setIssqntotoutro(BigDecimal aIssqntotoutro) {
        issqntotoutro = aIssqntotoutro;
    }

    /**
     * Access method for issqntotdescincond.
     *
     * @return the current value of issqntotdescincond
     */
    public BigDecimal getIssqntotdescincond() {
        return issqntotdescincond;
    }

    /**
     * Setter method for issqntotdescincond.
     *
     * @param aIssqntotdescincond the new value for issqntotdescincond
     */
    public void setIssqntotdescincond(BigDecimal aIssqntotdescincond) {
        issqntotdescincond = aIssqntotdescincond;
    }

    /**
     * Access method for issqntotdesccond.
     *
     * @return the current value of issqntotdesccond
     */
    public BigDecimal getIssqntotdesccond() {
        return issqntotdesccond;
    }

    /**
     * Setter method for issqntotdesccond.
     *
     * @param aIssqntotdesccond the new value for issqntotdesccond
     */
    public void setIssqntotdesccond(BigDecimal aIssqntotdesccond) {
        issqntotdesccond = aIssqntotdesccond;
    }

    /**
     * Access method for rettribretpis.
     *
     * @return the current value of rettribretpis
     */
    public BigDecimal getRettribretpis() {
        return rettribretpis;
    }

    /**
     * Setter method for rettribretpis.
     *
     * @param aRettribretpis the new value for rettribretpis
     */
    public void setRettribretpis(BigDecimal aRettribretpis) {
        rettribretpis = aRettribretpis;
    }

    /**
     * Access method for rettribretcofins.
     *
     * @return the current value of rettribretcofins
     */
    public BigDecimal getRettribretcofins() {
        return rettribretcofins;
    }

    /**
     * Setter method for rettribretcofins.
     *
     * @param aRettribretcofins the new value for rettribretcofins
     */
    public void setRettribretcofins(BigDecimal aRettribretcofins) {
        rettribretcofins = aRettribretcofins;
    }

    /**
     * Access method for rettribretcsll.
     *
     * @return the current value of rettribretcsll
     */
    public BigDecimal getRettribretcsll() {
        return rettribretcsll;
    }

    /**
     * Setter method for rettribretcsll.
     *
     * @param aRettribretcsll the new value for rettribretcsll
     */
    public void setRettribretcsll(BigDecimal aRettribretcsll) {
        rettribretcsll = aRettribretcsll;
    }

    /**
     * Access method for rettribbcirrf.
     *
     * @return the current value of rettribbcirrf
     */
    public BigDecimal getRettribbcirrf() {
        return rettribbcirrf;
    }

    /**
     * Setter method for rettribbcirrf.
     *
     * @param aRettribbcirrf the new value for rettribbcirrf
     */
    public void setRettribbcirrf(BigDecimal aRettribbcirrf) {
        rettribbcirrf = aRettribbcirrf;
    }

    /**
     * Access method for rettribirrf.
     *
     * @return the current value of rettribirrf
     */
    public BigDecimal getRettribirrf() {
        return rettribirrf;
    }

    /**
     * Setter method for rettribirrf.
     *
     * @param aRettribirrf the new value for rettribirrf
     */
    public void setRettribirrf(BigDecimal aRettribirrf) {
        rettribirrf = aRettribirrf;
    }

    /**
     * Access method for rettribbcretprev.
     *
     * @return the current value of rettribbcretprev
     */
    public BigDecimal getRettribbcretprev() {
        return rettribbcretprev;
    }

    /**
     * Setter method for rettribbcretprev.
     *
     * @param aRettribbcretprev the new value for rettribbcretprev
     */
    public void setRettribbcretprev(BigDecimal aRettribbcretprev) {
        rettribbcretprev = aRettribbcretprev;
    }

    /**
     * Access method for rettribretprev.
     *
     * @return the current value of rettribretprev
     */
    public BigDecimal getRettribretprev() {
        return rettribretprev;
    }

    /**
     * Setter method for rettribretprev.
     *
     * @param aRettribretprev the new value for rettribretprev
     */
    public void setRettribretprev(BigDecimal aRettribretprev) {
        rettribretprev = aRettribretprev;
    }

    /**
     * Access method for cobrfatura.
     *
     * @return the current value of cobrfatura
     */
    public String getCobrfatura() {
        return cobrfatura;
    }

    /**
     * Setter method for cobrfatura.
     *
     * @param aCobrfatura the new value for cobrfatura
     */
    public void setCobrfatura(String aCobrfatura) {
        cobrfatura = aCobrfatura;
    }

    /**
     * Access method for cobrvalororiginal.
     *
     * @return the current value of cobrvalororiginal
     */
    public BigDecimal getCobrvalororiginal() {
        return cobrvalororiginal;
    }

    /**
     * Setter method for cobrvalororiginal.
     *
     * @param aCobrvalororiginal the new value for cobrvalororiginal
     */
    public void setCobrvalororiginal(BigDecimal aCobrvalororiginal) {
        cobrvalororiginal = aCobrvalororiginal;
    }

    /**
     * Access method for cobrvalorliq.
     *
     * @return the current value of cobrvalorliq
     */
    public BigDecimal getCobrvalorliq() {
        return cobrvalorliq;
    }

    /**
     * Setter method for cobrvalorliq.
     *
     * @param aCobrvalorliq the new value for cobrvalorliq
     */
    public void setCobrvalorliq(BigDecimal aCobrvalorliq) {
        cobrvalorliq = aCobrvalorliq;
    }

    /**
     * Access method for cobrvalordesc.
     *
     * @return the current value of cobrvalordesc
     */
    public BigDecimal getCobrvalordesc() {
        return cobrvalordesc;
    }

    /**
     * Setter method for cobrvalordesc.
     *
     * @param aCobrvalordesc the new value for cobrvalordesc
     */
    public void setCobrvalordesc(BigDecimal aCobrvalordesc) {
        cobrvalordesc = aCobrvalordesc;
    }

    /**
     * Access method for nfechave.
     *
     * @return the current value of nfechave
     */
    public String getNfechave() {
        return nfechave;
    }

    /**
     * Setter method for nfechave.
     *
     * @param aNfechave the new value for nfechave
     */
    public void setNfechave(String aNfechave) {
        nfechave = aNfechave;
    }

    /**
     * Access method for nfecstat.
     *
     * @return the current value of nfecstat
     */
    public int getNfecstat() {
        return nfecstat;
    }

    /**
     * Setter method for nfecstat.
     *
     * @param aNfecstat the new value for nfecstat
     */
    public void setNfecstat(int aNfecstat) {
        nfecstat = aNfecstat;
    }

    /**
     * Access method for nfenf.
     *
     * @return the current value of nfenf
     */
    public int getNfenf() {
        return nfenf;
    }

    /**
     * Setter method for nfenf.
     *
     * @param aNfenf the new value for nfenf
     */
    public void setNfenf(int aNfenf) {
        nfenf = aNfenf;
    }

    /**
     * Access method for nfeprotocolo.
     *
     * @return the current value of nfeprotocolo
     */
    public String getNfeprotocolo() {
        return nfeprotocolo;
    }

    /**
     * Setter method for nfeprotocolo.
     *
     * @param aNfeprotocolo the new value for nfeprotocolo
     */
    public void setNfeprotocolo(String aNfeprotocolo) {
        nfeprotocolo = aNfeprotocolo;
    }

    /**
     * Access method for nfecontrolelote.
     *
     * @return the current value of nfecontrolelote
     */
    public int getNfecontrolelote() {
        return nfecontrolelote;
    }

    /**
     * Setter method for nfecontrolelote.
     *
     * @param aNfecontrolelote the new value for nfecontrolelote
     */
    public void setNfecontrolelote(int aNfecontrolelote) {
        nfecontrolelote = aNfecontrolelote;
    }

    /**
     * Access method for transptipofrete.
     *
     * @return the current value of transptipofrete
     */
    public short getTransptipofrete() {
        return transptipofrete;
    }

    /**
     * Setter method for transptipofrete.
     *
     * @param aTransptipofrete the new value for transptipofrete
     */
    public void setTransptipofrete(short aTransptipofrete) {
        transptipofrete = aTransptipofrete;
    }

    /**
     * Access method for transpplaca.
     *
     * @return the current value of transpplaca
     */
    public String getTranspplaca() {
        return transpplaca;
    }

    /**
     * Setter method for transpplaca.
     *
     * @param aTranspplaca the new value for transpplaca
     */
    public void setTranspplaca(String aTranspplaca) {
        transpplaca = aTranspplaca;
    }

    /**
     * Access method for transpufplaca.
     *
     * @return the current value of transpufplaca
     */
    public String getTranspufplaca() {
        return transpufplaca;
    }

    /**
     * Setter method for transpufplaca.
     *
     * @param aTranspufplaca the new value for transpufplaca
     */
    public void setTranspufplaca(String aTranspufplaca) {
        transpufplaca = aTranspufplaca;
    }

    /**
     * Access method for transprntc.
     *
     * @return the current value of transprntc
     */
    public int getTransprntc() {
        return transprntc;
    }

    /**
     * Setter method for transprntc.
     *
     * @param aTransprntc the new value for transprntc
     */
    public void setTransprntc(int aTransprntc) {
        transprntc = aTransprntc;
    }

    /**
     * Access method for transpvagao.
     *
     * @return the current value of transpvagao
     */
    public String getTranspvagao() {
        return transpvagao;
    }

    /**
     * Setter method for transpvagao.
     *
     * @param aTranspvagao the new value for transpvagao
     */
    public void setTranspvagao(String aTranspvagao) {
        transpvagao = aTranspvagao;
    }

    /**
     * Access method for transpbalsa.
     *
     * @return the current value of transpbalsa
     */
    public String getTranspbalsa() {
        return transpbalsa;
    }

    /**
     * Setter method for transpbalsa.
     *
     * @param aTranspbalsa the new value for transpbalsa
     */
    public void setTranspbalsa(String aTranspbalsa) {
        transpbalsa = aTranspbalsa;
    }

    /**
     * Access method for transpidaeronave.
     *
     * @return the current value of transpidaeronave
     */
    public String getTranspidaeronave() {
        return transpidaeronave;
    }

    /**
     * Setter method for transpidaeronave.
     *
     * @param aTranspidaeronave the new value for transpidaeronave
     */
    public void setTranspidaeronave(String aTranspidaeronave) {
        transpidaeronave = aTranspidaeronave;
    }

    /**
     * Access method for transppesoliq.
     *
     * @return the current value of transppesoliq
     */
    public BigDecimal getTransppesoliq() {
        return transppesoliq;
    }

    /**
     * Setter method for transppesoliq.
     *
     * @param aTransppesoliq the new value for transppesoliq
     */
    public void setTransppesoliq(BigDecimal aTransppesoliq) {
        transppesoliq = aTransppesoliq;
    }

    /**
     * Access method for transppesobrt.
     *
     * @return the current value of transppesobrt
     */
    public BigDecimal getTransppesobrt() {
        return transppesobrt;
    }

    /**
     * Setter method for transppesobrt.
     *
     * @param aTransppesobrt the new value for transppesobrt
     */
    public void setTransppesobrt(BigDecimal aTransppesobrt) {
        transppesobrt = aTransppesobrt;
    }

    /**
     * Access method for nfeinfadicionalmanual.
     *
     * @return the current value of nfeinfadicionalmanual
     */
    public String getNfeinfadicionalmanual() {
        return nfeinfadicionalmanual;
    }

    /**
     * Setter method for nfeinfadicionalmanual.
     *
     * @param aNfeinfadicionalmanual the new value for nfeinfadicionalmanual
     */
    public void setNfeinfadicionalmanual(String aNfeinfadicionalmanual) {
        nfeinfadicionalmanual = aNfeinfadicionalmanual;
    }

    /**
     * Access method for nfserps.
     *
     * @return the current value of nfserps
     */
    public int getNfserps() {
        return nfserps;
    }

    /**
     * Setter method for nfserps.
     *
     * @param aNfserps the new value for nfserps
     */
    public void setNfserps(int aNfserps) {
        nfserps = aNfserps;
    }

    /**
     * Access method for nfselote.
     *
     * @return the current value of nfselote
     */
    public int getNfselote() {
        return nfselote;
    }

    /**
     * Setter method for nfselote.
     *
     * @param aNfselote the new value for nfselote
     */
    public void setNfselote(int aNfselote) {
        nfselote = aNfselote;
    }

    /**
     * Access method for nfseprotocolo.
     *
     * @return the current value of nfseprotocolo
     */
    public String getNfseprotocolo() {
        return nfseprotocolo;
    }

    /**
     * Setter method for nfseprotocolo.
     *
     * @param aNfseprotocolo the new value for nfseprotocolo
     */
    public void setNfseprotocolo(String aNfseprotocolo) {
        nfseprotocolo = aNfseprotocolo;
    }

    /**
     * Access method for nfseverificacao.
     *
     * @return the current value of nfseverificacao
     */
    public String getNfseverificacao() {
        return nfseverificacao;
    }

    /**
     * Setter method for nfseverificacao.
     *
     * @param aNfseverificacao the new value for nfseverificacao
     */
    public void setNfseverificacao(String aNfseverificacao) {
        nfseverificacao = aNfseverificacao;
    }

    /**
     * Access method for crmchamado.
     *
     * @return the current value of crmchamado
     */
    public Set<Crmchamado> getCrmchamado() {
        return crmchamado;
    }

    /**
     * Setter method for crmchamado.
     *
     * @param aCrmchamado the new value for crmchamado
     */
    public void setCrmchamado(Set<Crmchamado> aCrmchamado) {
        crmchamado = aCrmchamado;
    }

    /**
     * Access method for ctbbem.
     *
     * @return the current value of ctbbem
     */
    public Set<Ctbbem> getCtbbem() {
        return ctbbem;
    }

    /**
     * Setter method for ctbbem.
     *
     * @param aCtbbem the new value for ctbbem
     */
    public void setCtbbem(Set<Ctbbem> aCtbbem) {
        ctbbem = aCtbbem;
    }

    /**
     * Access method for ctbbemdet.
     *
     * @return the current value of ctbbemdet
     */
    public Set<Ctbbemdet> getCtbbemdet() {
        return ctbbemdet;
    }

    /**
     * Setter method for ctbbemdet.
     *
     * @param aCtbbemdet the new value for ctbbemdet
     */
    public void setCtbbemdet(Set<Ctbbemdet> aCtbbemdet) {
        ctbbemdet = aCtbbemdet;
    }

    /**
     * Access method for ctblancamento.
     *
     * @return the current value of ctblancamento
     */
    public Set<Ctblancamento> getCtblancamento() {
        return ctblancamento;
    }

    /**
     * Setter method for ctblancamento.
     *
     * @param aCtblancamento the new value for ctblancamento
     */
    public void setCtblancamento(Set<Ctblancamento> aCtblancamento) {
        ctblancamento = aCtblancamento;
    }

    /**
     * Access method for finlancamento.
     *
     * @return the current value of finlancamento
     */
    public Set<Finlancamento> getFinlancamento() {
        return finlancamento;
    }

    /**
     * Setter method for finlancamento.
     *
     * @param aFinlancamento the new value for finlancamento
     */
    public void setFinlancamento(Set<Finlancamento> aFinlancamento) {
        finlancamento = aFinlancamento;
    }

    /**
     * Access method for finoperadora.
     *
     * @return the current value of finoperadora
     */
    public Set<Finoperadora> getFinoperadora() {
        return finoperadora;
    }

    /**
     * Setter method for finoperadora.
     *
     * @param aFinoperadora the new value for finoperadora
     */
    public void setFinoperadora(Set<Finoperadora> aFinoperadora) {
        finoperadora = aFinoperadora;
    }

    /**
     * Access method for fises.
     *
     * @return the current value of fises
     */
    public Set<Fises> getFises() {
        return fises;
    }

    /**
     * Setter method for fises.
     *
     * @param aFises the new value for fises
     */
    public void setFises(Set<Fises> aFises) {
        fises = aFises;
    }

    /**
     * Access method for opnfe.
     *
     * @return the current value of opnfe
     */
    public Set<Opnfe> getOpnfe() {
        return opnfe;
    }

    /**
     * Setter method for opnfe.
     *
     * @param aOpnfe the new value for opnfe
     */
    public void setOpnfe(Set<Opnfe> aOpnfe) {
        opnfe = aOpnfe;
    }

    /**
     * Access method for opnfeimporta.
     *
     * @return the current value of opnfeimporta
     */
    public Set<Opnfeimporta> getOpnfeimporta() {
        return opnfeimporta;
    }

    /**
     * Setter method for opnfeimporta.
     *
     * @param aOpnfeimporta the new value for opnfeimporta
     */
    public void setOpnfeimporta(Set<Opnfeimporta> aOpnfeimporta) {
        opnfeimporta = aOpnfeimporta;
    }

    /**
     * Access method for opsolicitacao.
     *
     * @return the current value of opsolicitacao
     */
    public Set<Opsolicitacao> getOpsolicitacao() {
        return opsolicitacao;
    }

    /**
     * Setter method for opsolicitacao.
     *
     * @param aOpsolicitacao the new value for opsolicitacao
     */
    public void setOpsolicitacao(Set<Opsolicitacao> aOpsolicitacao) {
        opsolicitacao = aOpsolicitacao;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Opoperacao getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Opoperacao aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for optransacaoevento.
     *
     * @return the current value of optransacaoevento
     */
    public Optransacaoevento getOptransacaoevento() {
        return optransacaoevento;
    }

    /**
     * Setter method for optransacaoevento.
     *
     * @param aOptransacaoevento the new value for optransacaoevento
     */
    public void setOptransacaoevento(Optransacaoevento aOptransacaoevento) {
        optransacaoevento = aOptransacaoevento;
    }

    /**
     * Access method for conflogradouro2.
     *
     * @return the current value of conflogradouro2
     */
    public Conflogradouro getConflogradouro2() {
        return conflogradouro2;
    }

    /**
     * Setter method for conflogradouro2.
     *
     * @param aConflogradouro2 the new value for conflogradouro2
     */
    public void setConflogradouro2(Conflogradouro aConflogradouro2) {
        conflogradouro2 = aConflogradouro2;
    }

    /**
     * Access method for conflogradouro3.
     *
     * @return the current value of conflogradouro3
     */
    public Conflogradouro getConflogradouro3() {
        return conflogradouro3;
    }

    /**
     * Setter method for conflogradouro3.
     *
     * @param aConflogradouro3 the new value for conflogradouro3
     */
    public void setConflogradouro3(Conflogradouro aConflogradouro3) {
        conflogradouro3 = aConflogradouro3;
    }

    /**
     * Access method for conflogradouro4.
     *
     * @return the current value of conflogradouro4
     */
    public Conflogradouro getConflogradouro4() {
        return conflogradouro4;
    }

    /**
     * Setter method for conflogradouro4.
     *
     * @param aConflogradouro4 the new value for conflogradouro4
     */
    public void setConflogradouro4(Conflogradouro aConflogradouro4) {
        conflogradouro4 = aConflogradouro4;
    }

    /**
     * Access method for gerveiculo.
     *
     * @return the current value of gerveiculo
     */
    public Gerveiculo getGerveiculo() {
        return gerveiculo;
    }

    /**
     * Setter method for gerveiculo.
     *
     * @param aGerveiculo the new value for gerveiculo
     */
    public void setGerveiculo(Gerveiculo aGerveiculo) {
        gerveiculo = aGerveiculo;
    }

    /**
     * Access method for impobs.
     *
     * @return the current value of impobs
     */
    public Impobs getImpobs() {
        return impobs;
    }

    /**
     * Setter method for impobs.
     *
     * @param aImpobs the new value for impobs
     */
    public void setImpobs(Impobs aImpobs) {
        impobs = aImpobs;
    }

    /**
     * Access method for impobs2.
     *
     * @return the current value of impobs2
     */
    public Impobs getImpobs2() {
        return impobs2;
    }

    /**
     * Setter method for impobs2.
     *
     * @param aImpobs2 the new value for impobs2
     */
    public void setImpobs2(Impobs aImpobs2) {
        impobs2 = aImpobs2;
    }

    /**
     * Access method for impobs3.
     *
     * @return the current value of impobs3
     */
    public Impobs getImpobs3() {
        return impobs3;
    }

    /**
     * Setter method for impobs3.
     *
     * @param aImpobs3 the new value for impobs3
     */
    public void setImpobs3(Impobs aImpobs3) {
        impobs3 = aImpobs3;
    }

    /**
     * Access method for prtabela.
     *
     * @return the current value of prtabela
     */
    public Prtabela getPrtabela() {
        return prtabela;
    }

    /**
     * Setter method for prtabela.
     *
     * @param aPrtabela the new value for prtabela
     */
    public void setPrtabela(Prtabela aPrtabela) {
        prtabela = aPrtabela;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for conflogradouro.
     *
     * @return the current value of conflogradouro
     */
    public Conflogradouro getConflogradouro() {
        return conflogradouro;
    }

    /**
     * Setter method for conflogradouro.
     *
     * @param aConflogradouro the new value for conflogradouro
     */
    public void setConflogradouro(Conflogradouro aConflogradouro) {
        conflogradouro = aConflogradouro;
    }

    /**
     * Access method for fincaixa.
     *
     * @return the current value of fincaixa
     */
    public Fincaixa getFincaixa() {
        return fincaixa;
    }

    /**
     * Setter method for fincaixa.
     *
     * @param aFincaixa the new value for fincaixa
     */
    public void setFincaixa(Fincaixa aFincaixa) {
        fincaixa = aFincaixa;
    }

    /**
     * Access method for oporc.
     *
     * @return the current value of oporc
     */
    public Oporc getOporc() {
        return oporc;
    }

    /**
     * Setter method for oporc.
     *
     * @param aOporc the new value for oporc
     */
    public void setOporc(Oporc aOporc) {
        oporc = aOporc;
    }

    /**
     * Access method for prdcordem2.
     *
     * @return the current value of prdcordem2
     */
    public Prdcordem getPrdcordem2() {
        return prdcordem2;
    }

    /**
     * Setter method for prdcordem2.
     *
     * @param aPrdcordem2 the new value for prdcordem2
     */
    public void setPrdcordem2(Prdcordem aPrdcordem2) {
        prdcordem2 = aPrdcordem2;
    }

    /**
     * Access method for opdocserie.
     *
     * @return the current value of opdocserie
     */
    public Opdocserie getOpdocserie() {
        return opdocserie;
    }

    /**
     * Setter method for opdocserie.
     *
     * @param aOpdocserie the new value for opdocserie
     */
    public void setOpdocserie(Opdocserie aOpdocserie) {
        opdocserie = aOpdocserie;
    }

    /**
     * Access method for confcidade.
     *
     * @return the current value of confcidade
     */
    public Confcidade getConfcidade() {
        return confcidade;
    }

    /**
     * Setter method for confcidade.
     *
     * @param aConfcidade the new value for confcidade
     */
    public void setConfcidade(Confcidade aConfcidade) {
        confcidade = aConfcidade;
    }

    /**
     * Access method for optransacaocobr.
     *
     * @return the current value of optransacaocobr
     */
    public Set<Optransacaocobr> getOptransacaocobr() {
        return optransacaocobr;
    }

    /**
     * Setter method for optransacaocobr.
     *
     * @param aOptransacaocobr the new value for optransacaocobr
     */
    public void setOptransacaocobr(Set<Optransacaocobr> aOptransacaocobr) {
        optransacaocobr = aOptransacaocobr;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Access method for optransacaodet2.
     *
     * @return the current value of optransacaodet2
     */
    public Set<Optransacaodet> getOptransacaodet2() {
        return optransacaodet2;
    }

    /**
     * Setter method for optransacaodet2.
     *
     * @param aOptransacaodet2 the new value for optransacaodet2
     */
    public void setOptransacaodet2(Set<Optransacaodet> aOptransacaodet2) {
        optransacaodet2 = aOptransacaodet2;
    }

    /**
     * Access method for optransacaoresp.
     *
     * @return the current value of optransacaoresp
     */
    public Set<Optransacaoresp> getOptransacaoresp() {
        return optransacaoresp;
    }

    /**
     * Setter method for optransacaoresp.
     *
     * @param aOptransacaoresp the new value for optransacaoresp
     */
    public void setOptransacaoresp(Set<Optransacaoresp> aOptransacaoresp) {
        optransacaoresp = aOptransacaoresp;
    }

    /**
     * Access method for optransacaovinc.
     *
     * @return the current value of optransacaovinc
     */
    public Set<Optransacaovinc> getOptransacaovinc() {
        return optransacaovinc;
    }

    /**
     * Setter method for optransacaovinc.
     *
     * @param aOptransacaovinc the new value for optransacaovinc
     */
    public void setOptransacaovinc(Set<Optransacaovinc> aOptransacaovinc) {
        optransacaovinc = aOptransacaovinc;
    }

    /**
     * Access method for optransacaovinc2.
     *
     * @return the current value of optransacaovinc2
     */
    public Set<Optransacaovinc> getOptransacaovinc2() {
        return optransacaovinc2;
    }

    /**
     * Setter method for optransacaovinc2.
     *
     * @param aOptransacaovinc2 the new value for optransacaovinc2
     */
    public void setOptransacaovinc2(Set<Optransacaovinc> aOptransacaovinc2) {
        optransacaovinc2 = aOptransacaovinc2;
    }

    /**
     * Access method for prdcordem.
     *
     * @return the current value of prdcordem
     */
    public Set<Prdcordem> getPrdcordem() {
        return prdcordem;
    }

    /**
     * Setter method for prdcordem.
     *
     * @param aPrdcordem the new value for prdcordem
     */
    public void setPrdcordem(Set<Prdcordem> aPrdcordem) {
        prdcordem = aPrdcordem;
    }

    /**
     * Compares the key for this instance with another Optransacao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Optransacao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Optransacao)) {
            return false;
        }
        Optransacao that = (Optransacao) other;
        if (this.getCodoptransacao() != that.getCodoptransacao()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Optransacao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Optransacao)) return false;
        return this.equalKeys(other) && ((Optransacao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoptransacao();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Optransacao |");
        sb.append(" codoptransacao=").append(getCodoptransacao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoptransacao", Integer.valueOf(getCodoptransacao()));
        return ret;
    }

}
