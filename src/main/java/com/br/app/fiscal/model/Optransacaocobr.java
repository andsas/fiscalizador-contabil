package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPTRANSACAOCOBR")
public class Optransacaocobr implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoptransacaocobr";

    @Id
    @Column(name="CODOPTRANSACAOCOBR", unique=true, nullable=false, precision=10)
    private int codoptransacaocobr;
    @Column(name="TOTAL", nullable=false, precision=15, scale=4)
    private BigDecimal total;
    @Column(name="NUMPARCELA", nullable=false, precision=5)
    private short numparcela;
    @Column(name="PERCPARCELA", nullable=false, length=15)
    private double percparcela;
    @Column(name="CHQNUM", precision=10)
    private int chqnum;
    @Column(name="CHQVALOR", precision=15, scale=4)
    private BigDecimal chqvalor;
    @Column(name="CHQDATA")
    private Date chqdata;
    @Column(name="CHQBOMPARA")
    private Date chqbompara;
    @Column(name="CHQNOME", length=250)
    private String chqnome;
    @Column(name="CHQDOCUMENTO", length=20)
    private String chqdocumento;
    @Column(name="CHQDI", length=60)
    private String chqdi;
    @Column(name="CHQCLIENTEDESDE")
    private Date chqclientedesde;
    @Column(name="CHQFAVORECIDO", length=250)
    private String chqfavorecido;
    @Column(name="CHQFOTO")
    private byte[] chqfoto;
    @Column(name="CARBANDEIRA", precision=5)
    private short carbandeira;
    @Column(name="CARNOME", length=250)
    private String carnome;
    @Column(name="CARNUMERO", length=40)
    private String carnumero;
    @Column(name="CARVALIDADE", length=4)
    private String carvalidade;
    @Column(name="CARSEGURANCAO", precision=10)
    private int carsegurancao;
    @Column(name="DATA")
    private Date data;
    @Column(name="VENCIMENTO")
    private Date vencimento;
    @OneToMany(mappedBy="optransacaocobr")
    private Set<Fincheque> fincheque;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAO", nullable=false)
    private Optransacao optransacao;
    @ManyToOne
    @JoinColumn(name="CODCOBRFORMA")
    private Cobrforma cobrforma;
    @ManyToOne
    @JoinColumn(name="CODCOBRPLANO")
    private Cobrplano cobrplano;
    @ManyToOne
    @JoinColumn(name="CODFINBANCO")
    private Finbanco finbanco;
    @ManyToOne
    @JoinColumn(name="CODFINAGENCIA")
    private Finagencia finagencia;
    @ManyToOne
    @JoinColumn(name="CODFINCONTA")
    private Finconta finconta;
    @ManyToOne
    @JoinColumn(name="CHQBANCO")
    private Finbanco finbanco2;
    @ManyToOne
    @JoinColumn(name="CHQAGENCIA")
    private Finagencia finagencia2;

    /** Default constructor. */
    public Optransacaocobr() {
        super();
    }

    /**
     * Access method for codoptransacaocobr.
     *
     * @return the current value of codoptransacaocobr
     */
    public int getCodoptransacaocobr() {
        return codoptransacaocobr;
    }

    /**
     * Setter method for codoptransacaocobr.
     *
     * @param aCodoptransacaocobr the new value for codoptransacaocobr
     */
    public void setCodoptransacaocobr(int aCodoptransacaocobr) {
        codoptransacaocobr = aCodoptransacaocobr;
    }

    /**
     * Access method for total.
     *
     * @return the current value of total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Setter method for total.
     *
     * @param aTotal the new value for total
     */
    public void setTotal(BigDecimal aTotal) {
        total = aTotal;
    }

    /**
     * Access method for numparcela.
     *
     * @return the current value of numparcela
     */
    public short getNumparcela() {
        return numparcela;
    }

    /**
     * Setter method for numparcela.
     *
     * @param aNumparcela the new value for numparcela
     */
    public void setNumparcela(short aNumparcela) {
        numparcela = aNumparcela;
    }

    /**
     * Access method for percparcela.
     *
     * @return the current value of percparcela
     */
    public double getPercparcela() {
        return percparcela;
    }

    /**
     * Setter method for percparcela.
     *
     * @param aPercparcela the new value for percparcela
     */
    public void setPercparcela(double aPercparcela) {
        percparcela = aPercparcela;
    }

    /**
     * Access method for chqnum.
     *
     * @return the current value of chqnum
     */
    public int getChqnum() {
        return chqnum;
    }

    /**
     * Setter method for chqnum.
     *
     * @param aChqnum the new value for chqnum
     */
    public void setChqnum(int aChqnum) {
        chqnum = aChqnum;
    }

    /**
     * Access method for chqvalor.
     *
     * @return the current value of chqvalor
     */
    public BigDecimal getChqvalor() {
        return chqvalor;
    }

    /**
     * Setter method for chqvalor.
     *
     * @param aChqvalor the new value for chqvalor
     */
    public void setChqvalor(BigDecimal aChqvalor) {
        chqvalor = aChqvalor;
    }

    /**
     * Access method for chqdata.
     *
     * @return the current value of chqdata
     */
    public Date getChqdata() {
        return chqdata;
    }

    /**
     * Setter method for chqdata.
     *
     * @param aChqdata the new value for chqdata
     */
    public void setChqdata(Date aChqdata) {
        chqdata = aChqdata;
    }

    /**
     * Access method for chqbompara.
     *
     * @return the current value of chqbompara
     */
    public Date getChqbompara() {
        return chqbompara;
    }

    /**
     * Setter method for chqbompara.
     *
     * @param aChqbompara the new value for chqbompara
     */
    public void setChqbompara(Date aChqbompara) {
        chqbompara = aChqbompara;
    }

    /**
     * Access method for chqnome.
     *
     * @return the current value of chqnome
     */
    public String getChqnome() {
        return chqnome;
    }

    /**
     * Setter method for chqnome.
     *
     * @param aChqnome the new value for chqnome
     */
    public void setChqnome(String aChqnome) {
        chqnome = aChqnome;
    }

    /**
     * Access method for chqdocumento.
     *
     * @return the current value of chqdocumento
     */
    public String getChqdocumento() {
        return chqdocumento;
    }

    /**
     * Setter method for chqdocumento.
     *
     * @param aChqdocumento the new value for chqdocumento
     */
    public void setChqdocumento(String aChqdocumento) {
        chqdocumento = aChqdocumento;
    }

    /**
     * Access method for chqdi.
     *
     * @return the current value of chqdi
     */
    public String getChqdi() {
        return chqdi;
    }

    /**
     * Setter method for chqdi.
     *
     * @param aChqdi the new value for chqdi
     */
    public void setChqdi(String aChqdi) {
        chqdi = aChqdi;
    }

    /**
     * Access method for chqclientedesde.
     *
     * @return the current value of chqclientedesde
     */
    public Date getChqclientedesde() {
        return chqclientedesde;
    }

    /**
     * Setter method for chqclientedesde.
     *
     * @param aChqclientedesde the new value for chqclientedesde
     */
    public void setChqclientedesde(Date aChqclientedesde) {
        chqclientedesde = aChqclientedesde;
    }

    /**
     * Access method for chqfavorecido.
     *
     * @return the current value of chqfavorecido
     */
    public String getChqfavorecido() {
        return chqfavorecido;
    }

    /**
     * Setter method for chqfavorecido.
     *
     * @param aChqfavorecido the new value for chqfavorecido
     */
    public void setChqfavorecido(String aChqfavorecido) {
        chqfavorecido = aChqfavorecido;
    }

    /**
     * Access method for chqfoto.
     *
     * @return the current value of chqfoto
     */
    public byte[] getChqfoto() {
        return chqfoto;
    }

    /**
     * Setter method for chqfoto.
     *
     * @param aChqfoto the new value for chqfoto
     */
    public void setChqfoto(byte[] aChqfoto) {
        chqfoto = aChqfoto;
    }

    /**
     * Access method for carbandeira.
     *
     * @return the current value of carbandeira
     */
    public short getCarbandeira() {
        return carbandeira;
    }

    /**
     * Setter method for carbandeira.
     *
     * @param aCarbandeira the new value for carbandeira
     */
    public void setCarbandeira(short aCarbandeira) {
        carbandeira = aCarbandeira;
    }

    /**
     * Access method for carnome.
     *
     * @return the current value of carnome
     */
    public String getCarnome() {
        return carnome;
    }

    /**
     * Setter method for carnome.
     *
     * @param aCarnome the new value for carnome
     */
    public void setCarnome(String aCarnome) {
        carnome = aCarnome;
    }

    /**
     * Access method for carnumero.
     *
     * @return the current value of carnumero
     */
    public String getCarnumero() {
        return carnumero;
    }

    /**
     * Setter method for carnumero.
     *
     * @param aCarnumero the new value for carnumero
     */
    public void setCarnumero(String aCarnumero) {
        carnumero = aCarnumero;
    }

    /**
     * Access method for carvalidade.
     *
     * @return the current value of carvalidade
     */
    public String getCarvalidade() {
        return carvalidade;
    }

    /**
     * Setter method for carvalidade.
     *
     * @param aCarvalidade the new value for carvalidade
     */
    public void setCarvalidade(String aCarvalidade) {
        carvalidade = aCarvalidade;
    }

    /**
     * Access method for carsegurancao.
     *
     * @return the current value of carsegurancao
     */
    public int getCarsegurancao() {
        return carsegurancao;
    }

    /**
     * Setter method for carsegurancao.
     *
     * @param aCarsegurancao the new value for carsegurancao
     */
    public void setCarsegurancao(int aCarsegurancao) {
        carsegurancao = aCarsegurancao;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Date getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Date aData) {
        data = aData;
    }

    /**
     * Access method for vencimento.
     *
     * @return the current value of vencimento
     */
    public Date getVencimento() {
        return vencimento;
    }

    /**
     * Setter method for vencimento.
     *
     * @param aVencimento the new value for vencimento
     */
    public void setVencimento(Date aVencimento) {
        vencimento = aVencimento;
    }

    /**
     * Access method for fincheque.
     *
     * @return the current value of fincheque
     */
    public Set<Fincheque> getFincheque() {
        return fincheque;
    }

    /**
     * Setter method for fincheque.
     *
     * @param aFincheque the new value for fincheque
     */
    public void setFincheque(Set<Fincheque> aFincheque) {
        fincheque = aFincheque;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Cobrforma getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Cobrforma aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Access method for cobrplano.
     *
     * @return the current value of cobrplano
     */
    public Cobrplano getCobrplano() {
        return cobrplano;
    }

    /**
     * Setter method for cobrplano.
     *
     * @param aCobrplano the new value for cobrplano
     */
    public void setCobrplano(Cobrplano aCobrplano) {
        cobrplano = aCobrplano;
    }

    /**
     * Access method for finbanco.
     *
     * @return the current value of finbanco
     */
    public Finbanco getFinbanco() {
        return finbanco;
    }

    /**
     * Setter method for finbanco.
     *
     * @param aFinbanco the new value for finbanco
     */
    public void setFinbanco(Finbanco aFinbanco) {
        finbanco = aFinbanco;
    }

    /**
     * Access method for finagencia.
     *
     * @return the current value of finagencia
     */
    public Finagencia getFinagencia() {
        return finagencia;
    }

    /**
     * Setter method for finagencia.
     *
     * @param aFinagencia the new value for finagencia
     */
    public void setFinagencia(Finagencia aFinagencia) {
        finagencia = aFinagencia;
    }

    /**
     * Access method for finconta.
     *
     * @return the current value of finconta
     */
    public Finconta getFinconta() {
        return finconta;
    }

    /**
     * Setter method for finconta.
     *
     * @param aFinconta the new value for finconta
     */
    public void setFinconta(Finconta aFinconta) {
        finconta = aFinconta;
    }

    /**
     * Access method for finbanco2.
     *
     * @return the current value of finbanco2
     */
    public Finbanco getFinbanco2() {
        return finbanco2;
    }

    /**
     * Setter method for finbanco2.
     *
     * @param aFinbanco2 the new value for finbanco2
     */
    public void setFinbanco2(Finbanco aFinbanco2) {
        finbanco2 = aFinbanco2;
    }

    /**
     * Access method for finagencia2.
     *
     * @return the current value of finagencia2
     */
    public Finagencia getFinagencia2() {
        return finagencia2;
    }

    /**
     * Setter method for finagencia2.
     *
     * @param aFinagencia2 the new value for finagencia2
     */
    public void setFinagencia2(Finagencia aFinagencia2) {
        finagencia2 = aFinagencia2;
    }

    /**
     * Compares the key for this instance with another Optransacaocobr.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Optransacaocobr and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Optransacaocobr)) {
            return false;
        }
        Optransacaocobr that = (Optransacaocobr) other;
        if (this.getCodoptransacaocobr() != that.getCodoptransacaocobr()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Optransacaocobr.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Optransacaocobr)) return false;
        return this.equalKeys(other) && ((Optransacaocobr)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoptransacaocobr();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Optransacaocobr |");
        sb.append(" codoptransacaocobr=").append(getCodoptransacaocobr());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoptransacaocobr", Integer.valueOf(getCodoptransacaocobr()));
        return ret;
    }

}
