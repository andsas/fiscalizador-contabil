package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="OPTRANSACAODET")
public class Optransacaodet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoptransacaodet";

    @Id
    @Column(name="CODOPTRANSACAODET", unique=true, nullable=false, precision=10)
    private int codoptransacaodet;
    @Column(name="CODITEM", precision=10)
    private int coditem;
    @Column(name="TIPOTOTALIZADOR", nullable=false, precision=5)
    private short tipototalizador;
    @Column(name="TOTQUANTIDADE", nullable=false, precision=15, scale=4)
    private BigDecimal totquantidade;
    @Column(name="TOTUNITARIO", nullable=false, precision=15, scale=4)
    private BigDecimal totunitario;
    @Column(name="TOTFRETE", precision=15, scale=4)
    private BigDecimal totfrete;
    @Column(name="TOTSEGURO", precision=15, scale=4)
    private BigDecimal totseguro;
    @Column(name="TOTACRESCIMO", precision=15, scale=4)
    private BigDecimal totacrescimo;
    @Column(name="TOTDESCONTO", precision=15, scale=4)
    private BigDecimal totdesconto;
    @Column(name="TOTOUTRO", precision=15, scale=4)
    private BigDecimal totoutro;
    @Column(name="TOTBRUTO", precision=15, scale=4)
    private BigDecimal totbruto;
    @Column(name="TOTAL", nullable=false, precision=15, scale=4)
    private BigDecimal total;
    @Column(name="TRIBCODPRODUNIDADE", nullable=false, length=20)
    private String tribcodprodunidade;
    @Column(name="TRIBQUANTIDADE", nullable=false, precision=15, scale=4)
    private BigDecimal tribquantidade;
    @Column(name="TRIBUNITARIO", nullable=false, precision=15, scale=4)
    private BigDecimal tribunitario;
    @Column(name="TRIBFRETE", precision=15, scale=4)
    private BigDecimal tribfrete;
    @Column(name="TRIBSEGURO", precision=15, scale=4)
    private BigDecimal tribseguro;
    @Column(name="TRIBACRESCIMO", precision=15, scale=4)
    private BigDecimal tribacrescimo;
    @Column(name="TRIBDESCONTO", precision=15, scale=4)
    private BigDecimal tribdesconto;
    @Column(name="TRIBOUTRO", precision=15, scale=4)
    private BigDecimal triboutro;
    @Column(name="IMPOSTOTOTTRIB", precision=15, scale=4)
    private BigDecimal impostotottrib;
    @Column(name="IMPOSTOICMS", precision=15, scale=4)
    private BigDecimal impostoicms;
    @Column(name="IMPOSTOIPI", precision=15, scale=4)
    private BigDecimal impostoipi;
    @Column(name="IMPOSTOII", precision=15, scale=4)
    private BigDecimal impostoii;
    @Column(name="IMPOSTOPIS", precision=15, scale=4)
    private BigDecimal impostopis;
    @Column(name="IMPOSTOSTPIS", precision=15, scale=4)
    private BigDecimal impostostpis;
    @Column(name="IMPOSTOCOFINS", precision=15, scale=4)
    private BigDecimal impostocofins;
    @Column(name="IMPOSTOSTCOFINS", precision=15, scale=4)
    private BigDecimal impostostcofins;
    @Column(name="IMPOSTOISSQN", precision=15, scale=4)
    private BigDecimal impostoissqn;
    @Column(name="TOTICMS", precision=15, scale=4)
    private BigDecimal toticms;
    @Column(name="TOTIPI", precision=15, scale=4)
    private BigDecimal totipi;
    @Column(name="TOTII", precision=15, scale=4)
    private BigDecimal totii;
    @Column(name="TOTPIS", precision=15, scale=4)
    private BigDecimal totpis;
    @Column(name="TOTSTPIS", precision=15, scale=4)
    private BigDecimal totstpis;
    @Column(name="TOTCOFINS", precision=15, scale=4)
    private BigDecimal totcofins;
    @Column(name="TOTSTCOFINS", precision=15, scale=4)
    private BigDecimal totstcofins;
    @Column(name="TOTISSQN", precision=15, scale=4)
    private BigDecimal totissqn;
    @Column(name="BASEICMS", precision=15, scale=4)
    private BigDecimal baseicms;
    @Column(name="BASEIPI", precision=15, scale=4)
    private BigDecimal baseipi;
    @Column(name="BASEII", precision=15, scale=4)
    private BigDecimal baseii;
    @Column(name="BASEPIS", precision=15, scale=4)
    private BigDecimal basepis;
    @Column(name="BASECOFINS", precision=15, scale=4)
    private BigDecimal basecofins;
    @Column(name="BASESTPIS", precision=15, scale=4)
    private BigDecimal basestpis;
    @Column(name="BASESTCOFINS", precision=15, scale=4)
    private BigDecimal basestcofins;
    @Column(name="BASEISSQN", precision=15, scale=4)
    private BigDecimal baseissqn;
    @Column(name="TOTST", precision=15, scale=4)
    private BigDecimal totst;
    @Column(name="IMPOSTOST", precision=15, scale=4)
    private BigDecimal impostost;
    @Column(name="BASEST", precision=15, scale=4)
    private BigDecimal basest;
    @Column(name="SERVICO", precision=5)
    private short servico;
    @Column(name="TOTINSS", precision=15, scale=4)
    private BigDecimal totinss;
    @Column(name="TOTDEDUCOES", precision=15, scale=4)
    private BigDecimal totdeducoes;
    @Column(name="TOTIR", precision=15, scale=4)
    private BigDecimal totir;
    @Column(name="TOTCSLL", precision=15, scale=4)
    private BigDecimal totcsll;
    @Column(name="BASEINSS", precision=15, scale=4)
    private BigDecimal baseinss;
    @Column(name="BASEIR", precision=15, scale=4)
    private BigDecimal baseir;
    @Column(name="BASECSLL", precision=15, scale=4)
    private BigDecimal basecsll;
    @Column(name="IMPOSTOINSS", precision=15, scale=4)
    private BigDecimal impostoinss;
    @Column(name="IMPOSTOIR", precision=15, scale=4)
    private BigDecimal impostoir;
    @Column(name="IMPOSTOCSLL", precision=15, scale=4)
    private BigDecimal impostocsll;
    @Column(name="TOTDESCCONDICIONADO", precision=15, scale=4)
    private BigDecimal totdesccondicionado;
    @Column(name="TOTDESCINCOND", precision=15, scale=4)
    private BigDecimal totdescincond;
    @Column(name="INFADIC")
    private String infadic;
    @Column(name="PRECOVENDA", precision=15, scale=4)
    private BigDecimal precovenda;
    @Column(name="TOTVOLUME", precision=15, scale=10)
    private BigDecimal totvolume;
    @OneToMany(mappedBy="optransacaodet")
    private Set<Cushist> cushist;
    @OneToMany(mappedBy="optransacaodet")
    private Set<Opsolicitacaodet> opsolicitacaodet;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAO", nullable=false)
    private Optransacao optransacao;
    @ManyToOne
    @JoinColumn(name="CODCSTICMS")
    private Impcst impcst;
    @ManyToOne
    @JoinColumn(name="CODCSTST")
    private Impcst impcst2;
    @ManyToOne
    @JoinColumn(name="CODCSTPIS")
    private Impcstpc impcstpc;
    @ManyToOne
    @JoinColumn(name="CODCSTCOFINS")
    private Impcstpc impcstpc2;
    @ManyToOne
    @JoinColumn(name="CODIMPPLANOFISCAL")
    private Impplanofiscal impplanofiscal;
    @ManyToOne
    @JoinColumn(name="CODPRODMODELO")
    private Prodmodelo prodmodelo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPOPERACAO", nullable=false)
    private Opoperacao opoperacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODUNIDADE", nullable=false)
    private Produnidade produnidade;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAOPEDIDO")
    private Optransacao optransacao2;
    @OneToMany(mappedBy="optransacaodet3")
    private Set<Optransacaodet> optransacaodet4;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAODETPEDIDO")
    private Optransacaodet optransacaodet3;
    @ManyToOne
    @JoinColumn(name="CODIMPOBS")
    private Impobs impobs;
    @ManyToOne
    @JoinColumn(name="CODCTBBEM")
    private Ctbbem ctbbem;
    @ManyToOne
    @JoinColumn(name="CODCSTIPI")
    private Impcstipi impcstipi;
    @OneToMany(mappedBy="optransacaodet")
    private Set<Optransacaodetprodcarac> optransacaodetprodcarac;
    @OneToMany(mappedBy="optransacaodet")
    private Set<Prdcordemetapaproduto> prdcordemetapaproduto;

    /** Default constructor. */
    public Optransacaodet() {
        super();
    }

    /**
     * Access method for codoptransacaodet.
     *
     * @return the current value of codoptransacaodet
     */
    public int getCodoptransacaodet() {
        return codoptransacaodet;
    }

    /**
     * Setter method for codoptransacaodet.
     *
     * @param aCodoptransacaodet the new value for codoptransacaodet
     */
    public void setCodoptransacaodet(int aCodoptransacaodet) {
        codoptransacaodet = aCodoptransacaodet;
    }

    /**
     * Access method for coditem.
     *
     * @return the current value of coditem
     */
    public int getCoditem() {
        return coditem;
    }

    /**
     * Setter method for coditem.
     *
     * @param aCoditem the new value for coditem
     */
    public void setCoditem(int aCoditem) {
        coditem = aCoditem;
    }

    /**
     * Access method for tipototalizador.
     *
     * @return the current value of tipototalizador
     */
    public short getTipototalizador() {
        return tipototalizador;
    }

    /**
     * Setter method for tipototalizador.
     *
     * @param aTipototalizador the new value for tipototalizador
     */
    public void setTipototalizador(short aTipototalizador) {
        tipototalizador = aTipototalizador;
    }

    /**
     * Access method for totquantidade.
     *
     * @return the current value of totquantidade
     */
    public BigDecimal getTotquantidade() {
        return totquantidade;
    }

    /**
     * Setter method for totquantidade.
     *
     * @param aTotquantidade the new value for totquantidade
     */
    public void setTotquantidade(BigDecimal aTotquantidade) {
        totquantidade = aTotquantidade;
    }

    /**
     * Access method for totunitario.
     *
     * @return the current value of totunitario
     */
    public BigDecimal getTotunitario() {
        return totunitario;
    }

    /**
     * Setter method for totunitario.
     *
     * @param aTotunitario the new value for totunitario
     */
    public void setTotunitario(BigDecimal aTotunitario) {
        totunitario = aTotunitario;
    }

    /**
     * Access method for totfrete.
     *
     * @return the current value of totfrete
     */
    public BigDecimal getTotfrete() {
        return totfrete;
    }

    /**
     * Setter method for totfrete.
     *
     * @param aTotfrete the new value for totfrete
     */
    public void setTotfrete(BigDecimal aTotfrete) {
        totfrete = aTotfrete;
    }

    /**
     * Access method for totseguro.
     *
     * @return the current value of totseguro
     */
    public BigDecimal getTotseguro() {
        return totseguro;
    }

    /**
     * Setter method for totseguro.
     *
     * @param aTotseguro the new value for totseguro
     */
    public void setTotseguro(BigDecimal aTotseguro) {
        totseguro = aTotseguro;
    }

    /**
     * Access method for totacrescimo.
     *
     * @return the current value of totacrescimo
     */
    public BigDecimal getTotacrescimo() {
        return totacrescimo;
    }

    /**
     * Setter method for totacrescimo.
     *
     * @param aTotacrescimo the new value for totacrescimo
     */
    public void setTotacrescimo(BigDecimal aTotacrescimo) {
        totacrescimo = aTotacrescimo;
    }

    /**
     * Access method for totdesconto.
     *
     * @return the current value of totdesconto
     */
    public BigDecimal getTotdesconto() {
        return totdesconto;
    }

    /**
     * Setter method for totdesconto.
     *
     * @param aTotdesconto the new value for totdesconto
     */
    public void setTotdesconto(BigDecimal aTotdesconto) {
        totdesconto = aTotdesconto;
    }

    /**
     * Access method for totoutro.
     *
     * @return the current value of totoutro
     */
    public BigDecimal getTotoutro() {
        return totoutro;
    }

    /**
     * Setter method for totoutro.
     *
     * @param aTotoutro the new value for totoutro
     */
    public void setTotoutro(BigDecimal aTotoutro) {
        totoutro = aTotoutro;
    }

    /**
     * Access method for totbruto.
     *
     * @return the current value of totbruto
     */
    public BigDecimal getTotbruto() {
        return totbruto;
    }

    /**
     * Setter method for totbruto.
     *
     * @param aTotbruto the new value for totbruto
     */
    public void setTotbruto(BigDecimal aTotbruto) {
        totbruto = aTotbruto;
    }

    /**
     * Access method for total.
     *
     * @return the current value of total
     */
    public BigDecimal getTotal() {
        return total;
    }

    /**
     * Setter method for total.
     *
     * @param aTotal the new value for total
     */
    public void setTotal(BigDecimal aTotal) {
        total = aTotal;
    }

    /**
     * Access method for tribcodprodunidade.
     *
     * @return the current value of tribcodprodunidade
     */
    public String getTribcodprodunidade() {
        return tribcodprodunidade;
    }

    /**
     * Setter method for tribcodprodunidade.
     *
     * @param aTribcodprodunidade the new value for tribcodprodunidade
     */
    public void setTribcodprodunidade(String aTribcodprodunidade) {
        tribcodprodunidade = aTribcodprodunidade;
    }

    /**
     * Access method for tribquantidade.
     *
     * @return the current value of tribquantidade
     */
    public BigDecimal getTribquantidade() {
        return tribquantidade;
    }

    /**
     * Setter method for tribquantidade.
     *
     * @param aTribquantidade the new value for tribquantidade
     */
    public void setTribquantidade(BigDecimal aTribquantidade) {
        tribquantidade = aTribquantidade;
    }

    /**
     * Access method for tribunitario.
     *
     * @return the current value of tribunitario
     */
    public BigDecimal getTribunitario() {
        return tribunitario;
    }

    /**
     * Setter method for tribunitario.
     *
     * @param aTribunitario the new value for tribunitario
     */
    public void setTribunitario(BigDecimal aTribunitario) {
        tribunitario = aTribunitario;
    }

    /**
     * Access method for tribfrete.
     *
     * @return the current value of tribfrete
     */
    public BigDecimal getTribfrete() {
        return tribfrete;
    }

    /**
     * Setter method for tribfrete.
     *
     * @param aTribfrete the new value for tribfrete
     */
    public void setTribfrete(BigDecimal aTribfrete) {
        tribfrete = aTribfrete;
    }

    /**
     * Access method for tribseguro.
     *
     * @return the current value of tribseguro
     */
    public BigDecimal getTribseguro() {
        return tribseguro;
    }

    /**
     * Setter method for tribseguro.
     *
     * @param aTribseguro the new value for tribseguro
     */
    public void setTribseguro(BigDecimal aTribseguro) {
        tribseguro = aTribseguro;
    }

    /**
     * Access method for tribacrescimo.
     *
     * @return the current value of tribacrescimo
     */
    public BigDecimal getTribacrescimo() {
        return tribacrescimo;
    }

    /**
     * Setter method for tribacrescimo.
     *
     * @param aTribacrescimo the new value for tribacrescimo
     */
    public void setTribacrescimo(BigDecimal aTribacrescimo) {
        tribacrescimo = aTribacrescimo;
    }

    /**
     * Access method for tribdesconto.
     *
     * @return the current value of tribdesconto
     */
    public BigDecimal getTribdesconto() {
        return tribdesconto;
    }

    /**
     * Setter method for tribdesconto.
     *
     * @param aTribdesconto the new value for tribdesconto
     */
    public void setTribdesconto(BigDecimal aTribdesconto) {
        tribdesconto = aTribdesconto;
    }

    /**
     * Access method for triboutro.
     *
     * @return the current value of triboutro
     */
    public BigDecimal getTriboutro() {
        return triboutro;
    }

    /**
     * Setter method for triboutro.
     *
     * @param aTriboutro the new value for triboutro
     */
    public void setTriboutro(BigDecimal aTriboutro) {
        triboutro = aTriboutro;
    }

    /**
     * Access method for impostotottrib.
     *
     * @return the current value of impostotottrib
     */
    public BigDecimal getImpostotottrib() {
        return impostotottrib;
    }

    /**
     * Setter method for impostotottrib.
     *
     * @param aImpostotottrib the new value for impostotottrib
     */
    public void setImpostotottrib(BigDecimal aImpostotottrib) {
        impostotottrib = aImpostotottrib;
    }

    /**
     * Access method for impostoicms.
     *
     * @return the current value of impostoicms
     */
    public BigDecimal getImpostoicms() {
        return impostoicms;
    }

    /**
     * Setter method for impostoicms.
     *
     * @param aImpostoicms the new value for impostoicms
     */
    public void setImpostoicms(BigDecimal aImpostoicms) {
        impostoicms = aImpostoicms;
    }

    /**
     * Access method for impostoipi.
     *
     * @return the current value of impostoipi
     */
    public BigDecimal getImpostoipi() {
        return impostoipi;
    }

    /**
     * Setter method for impostoipi.
     *
     * @param aImpostoipi the new value for impostoipi
     */
    public void setImpostoipi(BigDecimal aImpostoipi) {
        impostoipi = aImpostoipi;
    }

    /**
     * Access method for impostoii.
     *
     * @return the current value of impostoii
     */
    public BigDecimal getImpostoii() {
        return impostoii;
    }

    /**
     * Setter method for impostoii.
     *
     * @param aImpostoii the new value for impostoii
     */
    public void setImpostoii(BigDecimal aImpostoii) {
        impostoii = aImpostoii;
    }

    /**
     * Access method for impostopis.
     *
     * @return the current value of impostopis
     */
    public BigDecimal getImpostopis() {
        return impostopis;
    }

    /**
     * Setter method for impostopis.
     *
     * @param aImpostopis the new value for impostopis
     */
    public void setImpostopis(BigDecimal aImpostopis) {
        impostopis = aImpostopis;
    }

    /**
     * Access method for impostostpis.
     *
     * @return the current value of impostostpis
     */
    public BigDecimal getImpostostpis() {
        return impostostpis;
    }

    /**
     * Setter method for impostostpis.
     *
     * @param aImpostostpis the new value for impostostpis
     */
    public void setImpostostpis(BigDecimal aImpostostpis) {
        impostostpis = aImpostostpis;
    }

    /**
     * Access method for impostocofins.
     *
     * @return the current value of impostocofins
     */
    public BigDecimal getImpostocofins() {
        return impostocofins;
    }

    /**
     * Setter method for impostocofins.
     *
     * @param aImpostocofins the new value for impostocofins
     */
    public void setImpostocofins(BigDecimal aImpostocofins) {
        impostocofins = aImpostocofins;
    }

    /**
     * Access method for impostostcofins.
     *
     * @return the current value of impostostcofins
     */
    public BigDecimal getImpostostcofins() {
        return impostostcofins;
    }

    /**
     * Setter method for impostostcofins.
     *
     * @param aImpostostcofins the new value for impostostcofins
     */
    public void setImpostostcofins(BigDecimal aImpostostcofins) {
        impostostcofins = aImpostostcofins;
    }

    /**
     * Access method for impostoissqn.
     *
     * @return the current value of impostoissqn
     */
    public BigDecimal getImpostoissqn() {
        return impostoissqn;
    }

    /**
     * Setter method for impostoissqn.
     *
     * @param aImpostoissqn the new value for impostoissqn
     */
    public void setImpostoissqn(BigDecimal aImpostoissqn) {
        impostoissqn = aImpostoissqn;
    }

    /**
     * Access method for toticms.
     *
     * @return the current value of toticms
     */
    public BigDecimal getToticms() {
        return toticms;
    }

    /**
     * Setter method for toticms.
     *
     * @param aToticms the new value for toticms
     */
    public void setToticms(BigDecimal aToticms) {
        toticms = aToticms;
    }

    /**
     * Access method for totipi.
     *
     * @return the current value of totipi
     */
    public BigDecimal getTotipi() {
        return totipi;
    }

    /**
     * Setter method for totipi.
     *
     * @param aTotipi the new value for totipi
     */
    public void setTotipi(BigDecimal aTotipi) {
        totipi = aTotipi;
    }

    /**
     * Access method for totii.
     *
     * @return the current value of totii
     */
    public BigDecimal getTotii() {
        return totii;
    }

    /**
     * Setter method for totii.
     *
     * @param aTotii the new value for totii
     */
    public void setTotii(BigDecimal aTotii) {
        totii = aTotii;
    }

    /**
     * Access method for totpis.
     *
     * @return the current value of totpis
     */
    public BigDecimal getTotpis() {
        return totpis;
    }

    /**
     * Setter method for totpis.
     *
     * @param aTotpis the new value for totpis
     */
    public void setTotpis(BigDecimal aTotpis) {
        totpis = aTotpis;
    }

    /**
     * Access method for totstpis.
     *
     * @return the current value of totstpis
     */
    public BigDecimal getTotstpis() {
        return totstpis;
    }

    /**
     * Setter method for totstpis.
     *
     * @param aTotstpis the new value for totstpis
     */
    public void setTotstpis(BigDecimal aTotstpis) {
        totstpis = aTotstpis;
    }

    /**
     * Access method for totcofins.
     *
     * @return the current value of totcofins
     */
    public BigDecimal getTotcofins() {
        return totcofins;
    }

    /**
     * Setter method for totcofins.
     *
     * @param aTotcofins the new value for totcofins
     */
    public void setTotcofins(BigDecimal aTotcofins) {
        totcofins = aTotcofins;
    }

    /**
     * Access method for totstcofins.
     *
     * @return the current value of totstcofins
     */
    public BigDecimal getTotstcofins() {
        return totstcofins;
    }

    /**
     * Setter method for totstcofins.
     *
     * @param aTotstcofins the new value for totstcofins
     */
    public void setTotstcofins(BigDecimal aTotstcofins) {
        totstcofins = aTotstcofins;
    }

    /**
     * Access method for totissqn.
     *
     * @return the current value of totissqn
     */
    public BigDecimal getTotissqn() {
        return totissqn;
    }

    /**
     * Setter method for totissqn.
     *
     * @param aTotissqn the new value for totissqn
     */
    public void setTotissqn(BigDecimal aTotissqn) {
        totissqn = aTotissqn;
    }

    /**
     * Access method for baseicms.
     *
     * @return the current value of baseicms
     */
    public BigDecimal getBaseicms() {
        return baseicms;
    }

    /**
     * Setter method for baseicms.
     *
     * @param aBaseicms the new value for baseicms
     */
    public void setBaseicms(BigDecimal aBaseicms) {
        baseicms = aBaseicms;
    }

    /**
     * Access method for baseipi.
     *
     * @return the current value of baseipi
     */
    public BigDecimal getBaseipi() {
        return baseipi;
    }

    /**
     * Setter method for baseipi.
     *
     * @param aBaseipi the new value for baseipi
     */
    public void setBaseipi(BigDecimal aBaseipi) {
        baseipi = aBaseipi;
    }

    /**
     * Access method for baseii.
     *
     * @return the current value of baseii
     */
    public BigDecimal getBaseii() {
        return baseii;
    }

    /**
     * Setter method for baseii.
     *
     * @param aBaseii the new value for baseii
     */
    public void setBaseii(BigDecimal aBaseii) {
        baseii = aBaseii;
    }

    /**
     * Access method for basepis.
     *
     * @return the current value of basepis
     */
    public BigDecimal getBasepis() {
        return basepis;
    }

    /**
     * Setter method for basepis.
     *
     * @param aBasepis the new value for basepis
     */
    public void setBasepis(BigDecimal aBasepis) {
        basepis = aBasepis;
    }

    /**
     * Access method for basecofins.
     *
     * @return the current value of basecofins
     */
    public BigDecimal getBasecofins() {
        return basecofins;
    }

    /**
     * Setter method for basecofins.
     *
     * @param aBasecofins the new value for basecofins
     */
    public void setBasecofins(BigDecimal aBasecofins) {
        basecofins = aBasecofins;
    }

    /**
     * Access method for basestpis.
     *
     * @return the current value of basestpis
     */
    public BigDecimal getBasestpis() {
        return basestpis;
    }

    /**
     * Setter method for basestpis.
     *
     * @param aBasestpis the new value for basestpis
     */
    public void setBasestpis(BigDecimal aBasestpis) {
        basestpis = aBasestpis;
    }

    /**
     * Access method for basestcofins.
     *
     * @return the current value of basestcofins
     */
    public BigDecimal getBasestcofins() {
        return basestcofins;
    }

    /**
     * Setter method for basestcofins.
     *
     * @param aBasestcofins the new value for basestcofins
     */
    public void setBasestcofins(BigDecimal aBasestcofins) {
        basestcofins = aBasestcofins;
    }

    /**
     * Access method for baseissqn.
     *
     * @return the current value of baseissqn
     */
    public BigDecimal getBaseissqn() {
        return baseissqn;
    }

    /**
     * Setter method for baseissqn.
     *
     * @param aBaseissqn the new value for baseissqn
     */
    public void setBaseissqn(BigDecimal aBaseissqn) {
        baseissqn = aBaseissqn;
    }

    /**
     * Access method for totst.
     *
     * @return the current value of totst
     */
    public BigDecimal getTotst() {
        return totst;
    }

    /**
     * Setter method for totst.
     *
     * @param aTotst the new value for totst
     */
    public void setTotst(BigDecimal aTotst) {
        totst = aTotst;
    }

    /**
     * Access method for impostost.
     *
     * @return the current value of impostost
     */
    public BigDecimal getImpostost() {
        return impostost;
    }

    /**
     * Setter method for impostost.
     *
     * @param aImpostost the new value for impostost
     */
    public void setImpostost(BigDecimal aImpostost) {
        impostost = aImpostost;
    }

    /**
     * Access method for basest.
     *
     * @return the current value of basest
     */
    public BigDecimal getBasest() {
        return basest;
    }

    /**
     * Setter method for basest.
     *
     * @param aBasest the new value for basest
     */
    public void setBasest(BigDecimal aBasest) {
        basest = aBasest;
    }

    /**
     * Access method for servico.
     *
     * @return the current value of servico
     */
    public short getServico() {
        return servico;
    }

    /**
     * Setter method for servico.
     *
     * @param aServico the new value for servico
     */
    public void setServico(short aServico) {
        servico = aServico;
    }

    /**
     * Access method for totinss.
     *
     * @return the current value of totinss
     */
    public BigDecimal getTotinss() {
        return totinss;
    }

    /**
     * Setter method for totinss.
     *
     * @param aTotinss the new value for totinss
     */
    public void setTotinss(BigDecimal aTotinss) {
        totinss = aTotinss;
    }

    /**
     * Access method for totdeducoes.
     *
     * @return the current value of totdeducoes
     */
    public BigDecimal getTotdeducoes() {
        return totdeducoes;
    }

    /**
     * Setter method for totdeducoes.
     *
     * @param aTotdeducoes the new value for totdeducoes
     */
    public void setTotdeducoes(BigDecimal aTotdeducoes) {
        totdeducoes = aTotdeducoes;
    }

    /**
     * Access method for totir.
     *
     * @return the current value of totir
     */
    public BigDecimal getTotir() {
        return totir;
    }

    /**
     * Setter method for totir.
     *
     * @param aTotir the new value for totir
     */
    public void setTotir(BigDecimal aTotir) {
        totir = aTotir;
    }

    /**
     * Access method for totcsll.
     *
     * @return the current value of totcsll
     */
    public BigDecimal getTotcsll() {
        return totcsll;
    }

    /**
     * Setter method for totcsll.
     *
     * @param aTotcsll the new value for totcsll
     */
    public void setTotcsll(BigDecimal aTotcsll) {
        totcsll = aTotcsll;
    }

    /**
     * Access method for baseinss.
     *
     * @return the current value of baseinss
     */
    public BigDecimal getBaseinss() {
        return baseinss;
    }

    /**
     * Setter method for baseinss.
     *
     * @param aBaseinss the new value for baseinss
     */
    public void setBaseinss(BigDecimal aBaseinss) {
        baseinss = aBaseinss;
    }

    /**
     * Access method for baseir.
     *
     * @return the current value of baseir
     */
    public BigDecimal getBaseir() {
        return baseir;
    }

    /**
     * Setter method for baseir.
     *
     * @param aBaseir the new value for baseir
     */
    public void setBaseir(BigDecimal aBaseir) {
        baseir = aBaseir;
    }

    /**
     * Access method for basecsll.
     *
     * @return the current value of basecsll
     */
    public BigDecimal getBasecsll() {
        return basecsll;
    }

    /**
     * Setter method for basecsll.
     *
     * @param aBasecsll the new value for basecsll
     */
    public void setBasecsll(BigDecimal aBasecsll) {
        basecsll = aBasecsll;
    }

    /**
     * Access method for impostoinss.
     *
     * @return the current value of impostoinss
     */
    public BigDecimal getImpostoinss() {
        return impostoinss;
    }

    /**
     * Setter method for impostoinss.
     *
     * @param aImpostoinss the new value for impostoinss
     */
    public void setImpostoinss(BigDecimal aImpostoinss) {
        impostoinss = aImpostoinss;
    }

    /**
     * Access method for impostoir.
     *
     * @return the current value of impostoir
     */
    public BigDecimal getImpostoir() {
        return impostoir;
    }

    /**
     * Setter method for impostoir.
     *
     * @param aImpostoir the new value for impostoir
     */
    public void setImpostoir(BigDecimal aImpostoir) {
        impostoir = aImpostoir;
    }

    /**
     * Access method for impostocsll.
     *
     * @return the current value of impostocsll
     */
    public BigDecimal getImpostocsll() {
        return impostocsll;
    }

    /**
     * Setter method for impostocsll.
     *
     * @param aImpostocsll the new value for impostocsll
     */
    public void setImpostocsll(BigDecimal aImpostocsll) {
        impostocsll = aImpostocsll;
    }

    /**
     * Access method for totdesccondicionado.
     *
     * @return the current value of totdesccondicionado
     */
    public BigDecimal getTotdesccondicionado() {
        return totdesccondicionado;
    }

    /**
     * Setter method for totdesccondicionado.
     *
     * @param aTotdesccondicionado the new value for totdesccondicionado
     */
    public void setTotdesccondicionado(BigDecimal aTotdesccondicionado) {
        totdesccondicionado = aTotdesccondicionado;
    }

    /**
     * Access method for totdescincond.
     *
     * @return the current value of totdescincond
     */
    public BigDecimal getTotdescincond() {
        return totdescincond;
    }

    /**
     * Setter method for totdescincond.
     *
     * @param aTotdescincond the new value for totdescincond
     */
    public void setTotdescincond(BigDecimal aTotdescincond) {
        totdescincond = aTotdescincond;
    }

    /**
     * Access method for infadic.
     *
     * @return the current value of infadic
     */
    public String getInfadic() {
        return infadic;
    }

    /**
     * Setter method for infadic.
     *
     * @param aInfadic the new value for infadic
     */
    public void setInfadic(String aInfadic) {
        infadic = aInfadic;
    }

    /**
     * Access method for precovenda.
     *
     * @return the current value of precovenda
     */
    public BigDecimal getPrecovenda() {
        return precovenda;
    }

    /**
     * Setter method for precovenda.
     *
     * @param aPrecovenda the new value for precovenda
     */
    public void setPrecovenda(BigDecimal aPrecovenda) {
        precovenda = aPrecovenda;
    }

    /**
     * Access method for totvolume.
     *
     * @return the current value of totvolume
     */
    public BigDecimal getTotvolume() {
        return totvolume;
    }

    /**
     * Setter method for totvolume.
     *
     * @param aTotvolume the new value for totvolume
     */
    public void setTotvolume(BigDecimal aTotvolume) {
        totvolume = aTotvolume;
    }

    /**
     * Access method for cushist.
     *
     * @return the current value of cushist
     */
    public Set<Cushist> getCushist() {
        return cushist;
    }

    /**
     * Setter method for cushist.
     *
     * @param aCushist the new value for cushist
     */
    public void setCushist(Set<Cushist> aCushist) {
        cushist = aCushist;
    }

    /**
     * Access method for opsolicitacaodet.
     *
     * @return the current value of opsolicitacaodet
     */
    public Set<Opsolicitacaodet> getOpsolicitacaodet() {
        return opsolicitacaodet;
    }

    /**
     * Setter method for opsolicitacaodet.
     *
     * @param aOpsolicitacaodet the new value for opsolicitacaodet
     */
    public void setOpsolicitacaodet(Set<Opsolicitacaodet> aOpsolicitacaodet) {
        opsolicitacaodet = aOpsolicitacaodet;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for impcst.
     *
     * @return the current value of impcst
     */
    public Impcst getImpcst() {
        return impcst;
    }

    /**
     * Setter method for impcst.
     *
     * @param aImpcst the new value for impcst
     */
    public void setImpcst(Impcst aImpcst) {
        impcst = aImpcst;
    }

    /**
     * Access method for impcst2.
     *
     * @return the current value of impcst2
     */
    public Impcst getImpcst2() {
        return impcst2;
    }

    /**
     * Setter method for impcst2.
     *
     * @param aImpcst2 the new value for impcst2
     */
    public void setImpcst2(Impcst aImpcst2) {
        impcst2 = aImpcst2;
    }

    /**
     * Access method for impcstpc.
     *
     * @return the current value of impcstpc
     */
    public Impcstpc getImpcstpc() {
        return impcstpc;
    }

    /**
     * Setter method for impcstpc.
     *
     * @param aImpcstpc the new value for impcstpc
     */
    public void setImpcstpc(Impcstpc aImpcstpc) {
        impcstpc = aImpcstpc;
    }

    /**
     * Access method for impcstpc2.
     *
     * @return the current value of impcstpc2
     */
    public Impcstpc getImpcstpc2() {
        return impcstpc2;
    }

    /**
     * Setter method for impcstpc2.
     *
     * @param aImpcstpc2 the new value for impcstpc2
     */
    public void setImpcstpc2(Impcstpc aImpcstpc2) {
        impcstpc2 = aImpcstpc2;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Impplanofiscal getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Impplanofiscal aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Prodmodelo getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Prodmodelo aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Opoperacao getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Opoperacao aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for produnidade.
     *
     * @return the current value of produnidade
     */
    public Produnidade getProdunidade() {
        return produnidade;
    }

    /**
     * Setter method for produnidade.
     *
     * @param aProdunidade the new value for produnidade
     */
    public void setProdunidade(Produnidade aProdunidade) {
        produnidade = aProdunidade;
    }

    /**
     * Access method for optransacao2.
     *
     * @return the current value of optransacao2
     */
    public Optransacao getOptransacao2() {
        return optransacao2;
    }

    /**
     * Setter method for optransacao2.
     *
     * @param aOptransacao2 the new value for optransacao2
     */
    public void setOptransacao2(Optransacao aOptransacao2) {
        optransacao2 = aOptransacao2;
    }

    /**
     * Access method for optransacaodet4.
     *
     * @return the current value of optransacaodet4
     */
    public Set<Optransacaodet> getOptransacaodet4() {
        return optransacaodet4;
    }

    /**
     * Setter method for optransacaodet4.
     *
     * @param aOptransacaodet4 the new value for optransacaodet4
     */
    public void setOptransacaodet4(Set<Optransacaodet> aOptransacaodet4) {
        optransacaodet4 = aOptransacaodet4;
    }

    /**
     * Access method for optransacaodet3.
     *
     * @return the current value of optransacaodet3
     */
    public Optransacaodet getOptransacaodet3() {
        return optransacaodet3;
    }

    /**
     * Setter method for optransacaodet3.
     *
     * @param aOptransacaodet3 the new value for optransacaodet3
     */
    public void setOptransacaodet3(Optransacaodet aOptransacaodet3) {
        optransacaodet3 = aOptransacaodet3;
    }

    /**
     * Access method for impobs.
     *
     * @return the current value of impobs
     */
    public Impobs getImpobs() {
        return impobs;
    }

    /**
     * Setter method for impobs.
     *
     * @param aImpobs the new value for impobs
     */
    public void setImpobs(Impobs aImpobs) {
        impobs = aImpobs;
    }

    /**
     * Access method for ctbbem.
     *
     * @return the current value of ctbbem
     */
    public Ctbbem getCtbbem() {
        return ctbbem;
    }

    /**
     * Setter method for ctbbem.
     *
     * @param aCtbbem the new value for ctbbem
     */
    public void setCtbbem(Ctbbem aCtbbem) {
        ctbbem = aCtbbem;
    }

    /**
     * Access method for impcstipi.
     *
     * @return the current value of impcstipi
     */
    public Impcstipi getImpcstipi() {
        return impcstipi;
    }

    /**
     * Setter method for impcstipi.
     *
     * @param aImpcstipi the new value for impcstipi
     */
    public void setImpcstipi(Impcstipi aImpcstipi) {
        impcstipi = aImpcstipi;
    }

    /**
     * Access method for optransacaodetprodcarac.
     *
     * @return the current value of optransacaodetprodcarac
     */
    public Set<Optransacaodetprodcarac> getOptransacaodetprodcarac() {
        return optransacaodetprodcarac;
    }

    /**
     * Setter method for optransacaodetprodcarac.
     *
     * @param aOptransacaodetprodcarac the new value for optransacaodetprodcarac
     */
    public void setOptransacaodetprodcarac(Set<Optransacaodetprodcarac> aOptransacaodetprodcarac) {
        optransacaodetprodcarac = aOptransacaodetprodcarac;
    }

    /**
     * Access method for prdcordemetapaproduto.
     *
     * @return the current value of prdcordemetapaproduto
     */
    public Set<Prdcordemetapaproduto> getPrdcordemetapaproduto() {
        return prdcordemetapaproduto;
    }

    /**
     * Setter method for prdcordemetapaproduto.
     *
     * @param aPrdcordemetapaproduto the new value for prdcordemetapaproduto
     */
    public void setPrdcordemetapaproduto(Set<Prdcordemetapaproduto> aPrdcordemetapaproduto) {
        prdcordemetapaproduto = aPrdcordemetapaproduto;
    }

    /**
     * Compares the key for this instance with another Optransacaodet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Optransacaodet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Optransacaodet)) {
            return false;
        }
        Optransacaodet that = (Optransacaodet) other;
        if (this.getCodoptransacaodet() != that.getCodoptransacaodet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Optransacaodet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Optransacaodet)) return false;
        return this.equalKeys(other) && ((Optransacaodet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoptransacaodet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Optransacaodet |");
        sb.append(" codoptransacaodet=").append(getCodoptransacaodet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoptransacaodet", Integer.valueOf(getCodoptransacaodet()));
        return ret;
    }

}
