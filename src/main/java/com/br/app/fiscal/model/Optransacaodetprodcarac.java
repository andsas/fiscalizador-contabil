package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPTRANSACAODETPRODCARAC")
public class Optransacaodetprodcarac implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoptransacaodetprodcarac";

    @Id
    @Column(name="CODOPTRANSACAODETPRODCARAC", unique=true, nullable=false, precision=10)
    private int codoptransacaodetprodcarac;
    @Column(name="VALOR", length=250)
    private String valor;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAODET", nullable=false)
    private Optransacaodet optransacaodet;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODCARAC", nullable=false)
    private Prodcarac prodcarac;

    /** Default constructor. */
    public Optransacaodetprodcarac() {
        super();
    }

    /**
     * Access method for codoptransacaodetprodcarac.
     *
     * @return the current value of codoptransacaodetprodcarac
     */
    public int getCodoptransacaodetprodcarac() {
        return codoptransacaodetprodcarac;
    }

    /**
     * Setter method for codoptransacaodetprodcarac.
     *
     * @param aCodoptransacaodetprodcarac the new value for codoptransacaodetprodcarac
     */
    public void setCodoptransacaodetprodcarac(int aCodoptransacaodetprodcarac) {
        codoptransacaodetprodcarac = aCodoptransacaodetprodcarac;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public String getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(String aValor) {
        valor = aValor;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Optransacaodet getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Optransacaodet aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Access method for prodcarac.
     *
     * @return the current value of prodcarac
     */
    public Prodcarac getProdcarac() {
        return prodcarac;
    }

    /**
     * Setter method for prodcarac.
     *
     * @param aProdcarac the new value for prodcarac
     */
    public void setProdcarac(Prodcarac aProdcarac) {
        prodcarac = aProdcarac;
    }

    /**
     * Compares the key for this instance with another Optransacaodetprodcarac.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Optransacaodetprodcarac and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Optransacaodetprodcarac)) {
            return false;
        }
        Optransacaodetprodcarac that = (Optransacaodetprodcarac) other;
        if (this.getCodoptransacaodetprodcarac() != that.getCodoptransacaodetprodcarac()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Optransacaodetprodcarac.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Optransacaodetprodcarac)) return false;
        return this.equalKeys(other) && ((Optransacaodetprodcarac)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoptransacaodetprodcarac();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Optransacaodetprodcarac |");
        sb.append(" codoptransacaodetprodcarac=").append(getCodoptransacaodetprodcarac());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoptransacaodetprodcarac", Integer.valueOf(getCodoptransacaodetprodcarac()));
        return ret;
    }

}
