package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="OPTRANSACAOEVENTO")
public class Optransacaoevento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoptransacaoevento";

    @Id
    @Column(name="CODOPTRANSACAOEVENTO", unique=true, nullable=false, precision=10)
    private int codoptransacaoevento;
    @Column(name="CODOPTRANSACAO", nullable=false, precision=10)
    private int codoptransacao;
    @Column(name="CODOPEVENTO", nullable=false, precision=10)
    private int codopevento;
    @Column(name="STATUS", precision=5)
    private short status;
    @Column(name="OBS")
    private String obs;
    @Column(name="REALIZADO", precision=5)
    private short realizado;
    @OneToMany(mappedBy="optransacaoevento")
    private Set<Optransacao> optransacao;

    /** Default constructor. */
    public Optransacaoevento() {
        super();
    }

    /**
     * Access method for codoptransacaoevento.
     *
     * @return the current value of codoptransacaoevento
     */
    public int getCodoptransacaoevento() {
        return codoptransacaoevento;
    }

    /**
     * Setter method for codoptransacaoevento.
     *
     * @param aCodoptransacaoevento the new value for codoptransacaoevento
     */
    public void setCodoptransacaoevento(int aCodoptransacaoevento) {
        codoptransacaoevento = aCodoptransacaoevento;
    }

    /**
     * Access method for codoptransacao.
     *
     * @return the current value of codoptransacao
     */
    public int getCodoptransacao() {
        return codoptransacao;
    }

    /**
     * Setter method for codoptransacao.
     *
     * @param aCodoptransacao the new value for codoptransacao
     */
    public void setCodoptransacao(int aCodoptransacao) {
        codoptransacao = aCodoptransacao;
    }

    /**
     * Access method for codopevento.
     *
     * @return the current value of codopevento
     */
    public int getCodopevento() {
        return codopevento;
    }

    /**
     * Setter method for codopevento.
     *
     * @param aCodopevento the new value for codopevento
     */
    public void setCodopevento(int aCodopevento) {
        codopevento = aCodopevento;
    }

    /**
     * Access method for status.
     *
     * @return the current value of status
     */
    public short getStatus() {
        return status;
    }

    /**
     * Setter method for status.
     *
     * @param aStatus the new value for status
     */
    public void setStatus(short aStatus) {
        status = aStatus;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for realizado.
     *
     * @return the current value of realizado
     */
    public short getRealizado() {
        return realizado;
    }

    /**
     * Setter method for realizado.
     *
     * @param aRealizado the new value for realizado
     */
    public void setRealizado(short aRealizado) {
        realizado = aRealizado;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Compares the key for this instance with another Optransacaoevento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Optransacaoevento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Optransacaoevento)) {
            return false;
        }
        Optransacaoevento that = (Optransacaoevento) other;
        if (this.getCodoptransacaoevento() != that.getCodoptransacaoevento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Optransacaoevento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Optransacaoevento)) return false;
        return this.equalKeys(other) && ((Optransacaoevento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoptransacaoevento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Optransacaoevento |");
        sb.append(" codoptransacaoevento=").append(getCodoptransacaoevento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoptransacaoevento", Integer.valueOf(getCodoptransacaoevento()));
        return ret;
    }

}
