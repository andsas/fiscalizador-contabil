package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPTRANSACAORESP")
public class Optransacaoresp implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoptransacaoresp";

    @Id
    @Column(name="CODOPTRANSACAORESP", unique=true, nullable=false, precision=10)
    private int codoptransacaoresp;
    @Column(name="OBS")
    private String obs;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAO", nullable=false)
    private Optransacao optransacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTERESP", nullable=false)
    private Agagenteresp agagenteresp;

    /** Default constructor. */
    public Optransacaoresp() {
        super();
    }

    /**
     * Access method for codoptransacaoresp.
     *
     * @return the current value of codoptransacaoresp
     */
    public int getCodoptransacaoresp() {
        return codoptransacaoresp;
    }

    /**
     * Setter method for codoptransacaoresp.
     *
     * @param aCodoptransacaoresp the new value for codoptransacaoresp
     */
    public void setCodoptransacaoresp(int aCodoptransacaoresp) {
        codoptransacaoresp = aCodoptransacaoresp;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for agagenteresp.
     *
     * @return the current value of agagenteresp
     */
    public Agagenteresp getAgagenteresp() {
        return agagenteresp;
    }

    /**
     * Setter method for agagenteresp.
     *
     * @param aAgagenteresp the new value for agagenteresp
     */
    public void setAgagenteresp(Agagenteresp aAgagenteresp) {
        agagenteresp = aAgagenteresp;
    }

    /**
     * Compares the key for this instance with another Optransacaoresp.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Optransacaoresp and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Optransacaoresp)) {
            return false;
        }
        Optransacaoresp that = (Optransacaoresp) other;
        if (this.getCodoptransacaoresp() != that.getCodoptransacaoresp()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Optransacaoresp.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Optransacaoresp)) return false;
        return this.equalKeys(other) && ((Optransacaoresp)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoptransacaoresp();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Optransacaoresp |");
        sb.append(" codoptransacaoresp=").append(getCodoptransacaoresp());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoptransacaoresp", Integer.valueOf(getCodoptransacaoresp()));
        return ret;
    }

}
