package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="OPTRANSACAOVINC")
public class Optransacaovinc implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codoptransacaovinc";

    @Id
    @Column(name="CODOPTRANSACAOVINC", unique=true, nullable=false, precision=10)
    private int codoptransacaovinc;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAOMASTER", nullable=false)
    private Optransacao optransacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAODET", nullable=false)
    private Optransacao optransacao2;

    /** Default constructor. */
    public Optransacaovinc() {
        super();
    }

    /**
     * Access method for codoptransacaovinc.
     *
     * @return the current value of codoptransacaovinc
     */
    public int getCodoptransacaovinc() {
        return codoptransacaovinc;
    }

    /**
     * Setter method for codoptransacaovinc.
     *
     * @param aCodoptransacaovinc the new value for codoptransacaovinc
     */
    public void setCodoptransacaovinc(int aCodoptransacaovinc) {
        codoptransacaovinc = aCodoptransacaovinc;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for optransacao2.
     *
     * @return the current value of optransacao2
     */
    public Optransacao getOptransacao2() {
        return optransacao2;
    }

    /**
     * Setter method for optransacao2.
     *
     * @param aOptransacao2 the new value for optransacao2
     */
    public void setOptransacao2(Optransacao aOptransacao2) {
        optransacao2 = aOptransacao2;
    }

    /**
     * Compares the key for this instance with another Optransacaovinc.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Optransacaovinc and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Optransacaovinc)) {
            return false;
        }
        Optransacaovinc that = (Optransacaovinc) other;
        if (this.getCodoptransacaovinc() != that.getCodoptransacaovinc()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Optransacaovinc.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Optransacaovinc)) return false;
        return this.equalKeys(other) && ((Optransacaovinc)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodoptransacaovinc();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Optransacaovinc |");
        sb.append(" codoptransacaovinc=").append(getCodoptransacaovinc());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codoptransacaovinc", Integer.valueOf(getCodoptransacaovinc()));
        return ret;
    }

}
