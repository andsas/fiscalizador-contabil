package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="ORCPLANOCONTA")
public class Orcplanoconta implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codorcplanoconta";

    @Id
    @Column(name="CODORCPLANOCONTA", unique=true, nullable=false, length=20)
    private String codorcplanoconta;
    @Column(name="DESCORCPLANOCONTA", nullable=false, length=250)
    private String descorcplanoconta;
    @Column(name="NIVEL", precision=10)
    private int nivel;
    @Column(name="CODREDUZIDO", precision=10)
    private int codreduzido;
    @Column(name="DEBCRED", precision=5)
    private short debcred;
    @OneToMany(mappedBy="orcplanoconta")
    private Set<Agagente> agagente;
    @OneToMany(mappedBy="orcplanoconta2")
    private Set<Agagente> agagente2;
    @OneToMany(mappedBy="orcplanoconta")
    private Set<Cobrforma> cobrforma;
    @OneToMany(mappedBy="orcplanoconta2")
    private Set<Cobrforma> cobrforma2;
    @OneToMany(mappedBy="orcplanoconta")
    private Set<Ctbplanoconta> ctbplanoconta;
    @OneToMany(mappedBy="orcplanoconta")
    private Set<Finbaixa> finbaixa;
    @OneToMany(mappedBy="orcplanoconta")
    private Set<Fincaixa> fincaixa;
    @OneToMany(mappedBy="orcplanoconta2")
    private Set<Fincaixa> fincaixa2;
    @OneToMany(mappedBy="orcplanoconta")
    private Set<Folevento> folevento;
    @OneToMany(mappedBy="orcplanoconta2")
    private Set<Folevento> folevento2;
    @OneToMany(mappedBy="orcplanoconta")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="orcplanoconta2")
    private Set<Impplanofiscal> impplanofiscal2;
    @OneToMany(mappedBy="orcplanoconta")
    private Set<Orcplanoconta> orcplanocontaM;
    @ManyToOne
    @JoinColumn(name="CODPARENTORCPLANOCONTA")
    private Orcplanoconta orcplanoconta;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFEMPR", nullable=false)
    private Confempr confempr;

    /** Default constructor. */
    public Orcplanoconta() {
        super();
    }

    /**
     * Access method for codorcplanoconta.
     *
     * @return the current value of codorcplanoconta
     */
    public String getCodorcplanoconta() {
        return codorcplanoconta;
    }

    /**
     * Setter method for codorcplanoconta.
     *
     * @param aCodorcplanoconta the new value for codorcplanoconta
     */
    public void setCodorcplanoconta(String aCodorcplanoconta) {
        codorcplanoconta = aCodorcplanoconta;
    }

    /**
     * Access method for descorcplanoconta.
     *
     * @return the current value of descorcplanoconta
     */
    public String getDescorcplanoconta() {
        return descorcplanoconta;
    }

    /**
     * Setter method for descorcplanoconta.
     *
     * @param aDescorcplanoconta the new value for descorcplanoconta
     */
    public void setDescorcplanoconta(String aDescorcplanoconta) {
        descorcplanoconta = aDescorcplanoconta;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public int getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(int aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for codreduzido.
     *
     * @return the current value of codreduzido
     */
    public int getCodreduzido() {
        return codreduzido;
    }

    /**
     * Setter method for codreduzido.
     *
     * @param aCodreduzido the new value for codreduzido
     */
    public void setCodreduzido(int aCodreduzido) {
        codreduzido = aCodreduzido;
    }

    /**
     * Access method for debcred.
     *
     * @return the current value of debcred
     */
    public short getDebcred() {
        return debcred;
    }

    /**
     * Setter method for debcred.
     *
     * @param aDebcred the new value for debcred
     */
    public void setDebcred(short aDebcred) {
        debcred = aDebcred;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Set<Agagente> getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Set<Agagente> aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Set<Cobrforma> getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Set<Cobrforma> aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Access method for cobrforma2.
     *
     * @return the current value of cobrforma2
     */
    public Set<Cobrforma> getCobrforma2() {
        return cobrforma2;
    }

    /**
     * Setter method for cobrforma2.
     *
     * @param aCobrforma2 the new value for cobrforma2
     */
    public void setCobrforma2(Set<Cobrforma> aCobrforma2) {
        cobrforma2 = aCobrforma2;
    }

    /**
     * Access method for ctbplanoconta.
     *
     * @return the current value of ctbplanoconta
     */
    public Set<Ctbplanoconta> getCtbplanoconta() {
        return ctbplanoconta;
    }

    /**
     * Setter method for ctbplanoconta.
     *
     * @param aCtbplanoconta the new value for ctbplanoconta
     */
    public void setCtbplanoconta(Set<Ctbplanoconta> aCtbplanoconta) {
        ctbplanoconta = aCtbplanoconta;
    }

    /**
     * Access method for finbaixa.
     *
     * @return the current value of finbaixa
     */
    public Set<Finbaixa> getFinbaixa() {
        return finbaixa;
    }

    /**
     * Setter method for finbaixa.
     *
     * @param aFinbaixa the new value for finbaixa
     */
    public void setFinbaixa(Set<Finbaixa> aFinbaixa) {
        finbaixa = aFinbaixa;
    }

    /**
     * Access method for fincaixa.
     *
     * @return the current value of fincaixa
     */
    public Set<Fincaixa> getFincaixa() {
        return fincaixa;
    }

    /**
     * Setter method for fincaixa.
     *
     * @param aFincaixa the new value for fincaixa
     */
    public void setFincaixa(Set<Fincaixa> aFincaixa) {
        fincaixa = aFincaixa;
    }

    /**
     * Access method for fincaixa2.
     *
     * @return the current value of fincaixa2
     */
    public Set<Fincaixa> getFincaixa2() {
        return fincaixa2;
    }

    /**
     * Setter method for fincaixa2.
     *
     * @param aFincaixa2 the new value for fincaixa2
     */
    public void setFincaixa2(Set<Fincaixa> aFincaixa2) {
        fincaixa2 = aFincaixa2;
    }

    /**
     * Access method for folevento.
     *
     * @return the current value of folevento
     */
    public Set<Folevento> getFolevento() {
        return folevento;
    }

    /**
     * Setter method for folevento.
     *
     * @param aFolevento the new value for folevento
     */
    public void setFolevento(Set<Folevento> aFolevento) {
        folevento = aFolevento;
    }

    /**
     * Access method for folevento2.
     *
     * @return the current value of folevento2
     */
    public Set<Folevento> getFolevento2() {
        return folevento2;
    }

    /**
     * Setter method for folevento2.
     *
     * @param aFolevento2 the new value for folevento2
     */
    public void setFolevento2(Set<Folevento> aFolevento2) {
        folevento2 = aFolevento2;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for impplanofiscal2.
     *
     * @return the current value of impplanofiscal2
     */
    public Set<Impplanofiscal> getImpplanofiscal2() {
        return impplanofiscal2;
    }

    /**
     * Setter method for impplanofiscal2.
     *
     * @param aImpplanofiscal2 the new value for impplanofiscal2
     */
    public void setImpplanofiscal2(Set<Impplanofiscal> aImpplanofiscal2) {
        impplanofiscal2 = aImpplanofiscal2;
    }

    /**
     * Access method for orcplanocontaM.
     *
     * @return the current value of orcplanocontaM
     */
    public Set<Orcplanoconta> getOrcplanocontaM() {
        return orcplanocontaM;
    }

    /**
     * Setter method for orcplanocontaM.
     *
     * @param aOrcplanocontaM the new value for orcplanocontaM
     */
    public void setOrcplanocontaM(Set<Orcplanoconta> aOrcplanocontaM) {
        orcplanocontaM = aOrcplanocontaM;
    }

    /**
     * Access method for orcplanoconta.
     *
     * @return the current value of orcplanoconta
     */
    public Orcplanoconta getOrcplanoconta() {
        return orcplanoconta;
    }

    /**
     * Setter method for orcplanoconta.
     *
     * @param aOrcplanoconta the new value for orcplanoconta
     */
    public void setOrcplanoconta(Orcplanoconta aOrcplanoconta) {
        orcplanoconta = aOrcplanoconta;
    }

    /**
     * Access method for confempr.
     *
     * @return the current value of confempr
     */
    public Confempr getConfempr() {
        return confempr;
    }

    /**
     * Setter method for confempr.
     *
     * @param aConfempr the new value for confempr
     */
    public void setConfempr(Confempr aConfempr) {
        confempr = aConfempr;
    }

    /**
     * Compares the key for this instance with another Orcplanoconta.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Orcplanoconta and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Orcplanoconta)) {
            return false;
        }
        Orcplanoconta that = (Orcplanoconta) other;
        Object myCodorcplanoconta = this.getCodorcplanoconta();
        Object yourCodorcplanoconta = that.getCodorcplanoconta();
        if (myCodorcplanoconta==null ? yourCodorcplanoconta!=null : !myCodorcplanoconta.equals(yourCodorcplanoconta)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Orcplanoconta.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Orcplanoconta)) return false;
        return this.equalKeys(other) && ((Orcplanoconta)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodorcplanoconta() == null) {
            i = 0;
        } else {
            i = getCodorcplanoconta().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Orcplanoconta |");
        sb.append(" codorcplanoconta=").append(getCodorcplanoconta());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codorcplanoconta", getCodorcplanoconta());
        return ret;
    }

}
