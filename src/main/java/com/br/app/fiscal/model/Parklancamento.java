package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PARKLANCAMENTO")
public class Parklancamento implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codparklancamento";

    @Id
    @Column(name="CODPARKLANCAMENTO", unique=true, nullable=false, precision=10)
    private int codparklancamento;
    @Column(name="PLACA", nullable=false, length=20)
    private String placa;
    @Column(name="HORARIOENTRADA", nullable=false)
    private Timestamp horarioentrada;
    @Column(name="HORARIOSAIDA")
    private Timestamp horariosaida;
    @Column(name="CODCONFEMPR", precision=10)
    private int codconfempr;
    @Column(name="OBS")
    private String obs;
    @Column(name="VALOR", precision=15, scale=4)
    private BigDecimal valor;
    @Column(name="MINUTOS", precision=15, scale=4)
    private BigDecimal minutos;
    @Column(name="MODELO", length=40)
    private String modelo;
    @Column(name="COR", length=60)
    private String cor;
    @ManyToOne
    @JoinColumn(name="CODGERVEICULO")
    private Gerveiculo gerveiculo;
    @ManyToOne
    @JoinColumn(name="CODPRODMODELO")
    private Prodmodelo prodmodelo;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIO")
    private Confusuario confusuario;
    @ManyToOne
    @JoinColumn(name="CODPARKTIPO")
    private Parktipo parktipo;
    @ManyToOne
    @JoinColumn(name="CODPARKTABELA")
    private Parktabela parktabela;
    @ManyToOne
    @JoinColumn(name="CODCOBRFORMA")
    private Cobrforma cobrforma;

    /** Default constructor. */
    public Parklancamento() {
        super();
    }

    /**
     * Access method for codparklancamento.
     *
     * @return the current value of codparklancamento
     */
    public int getCodparklancamento() {
        return codparklancamento;
    }

    /**
     * Setter method for codparklancamento.
     *
     * @param aCodparklancamento the new value for codparklancamento
     */
    public void setCodparklancamento(int aCodparklancamento) {
        codparklancamento = aCodparklancamento;
    }

    /**
     * Access method for placa.
     *
     * @return the current value of placa
     */
    public String getPlaca() {
        return placa;
    }

    /**
     * Setter method for placa.
     *
     * @param aPlaca the new value for placa
     */
    public void setPlaca(String aPlaca) {
        placa = aPlaca;
    }

    /**
     * Access method for horarioentrada.
     *
     * @return the current value of horarioentrada
     */
    public Timestamp getHorarioentrada() {
        return horarioentrada;
    }

    /**
     * Setter method for horarioentrada.
     *
     * @param aHorarioentrada the new value for horarioentrada
     */
    public void setHorarioentrada(Timestamp aHorarioentrada) {
        horarioentrada = aHorarioentrada;
    }

    /**
     * Access method for horariosaida.
     *
     * @return the current value of horariosaida
     */
    public Timestamp getHorariosaida() {
        return horariosaida;
    }

    /**
     * Setter method for horariosaida.
     *
     * @param aHorariosaida the new value for horariosaida
     */
    public void setHorariosaida(Timestamp aHorariosaida) {
        horariosaida = aHorariosaida;
    }

    /**
     * Access method for codconfempr.
     *
     * @return the current value of codconfempr
     */
    public int getCodconfempr() {
        return codconfempr;
    }

    /**
     * Setter method for codconfempr.
     *
     * @param aCodconfempr the new value for codconfempr
     */
    public void setCodconfempr(int aCodconfempr) {
        codconfempr = aCodconfempr;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for valor.
     *
     * @return the current value of valor
     */
    public BigDecimal getValor() {
        return valor;
    }

    /**
     * Setter method for valor.
     *
     * @param aValor the new value for valor
     */
    public void setValor(BigDecimal aValor) {
        valor = aValor;
    }

    /**
     * Access method for minutos.
     *
     * @return the current value of minutos
     */
    public BigDecimal getMinutos() {
        return minutos;
    }

    /**
     * Setter method for minutos.
     *
     * @param aMinutos the new value for minutos
     */
    public void setMinutos(BigDecimal aMinutos) {
        minutos = aMinutos;
    }

    /**
     * Access method for modelo.
     *
     * @return the current value of modelo
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Setter method for modelo.
     *
     * @param aModelo the new value for modelo
     */
    public void setModelo(String aModelo) {
        modelo = aModelo;
    }

    /**
     * Access method for cor.
     *
     * @return the current value of cor
     */
    public String getCor() {
        return cor;
    }

    /**
     * Setter method for cor.
     *
     * @param aCor the new value for cor
     */
    public void setCor(String aCor) {
        cor = aCor;
    }

    /**
     * Access method for gerveiculo.
     *
     * @return the current value of gerveiculo
     */
    public Gerveiculo getGerveiculo() {
        return gerveiculo;
    }

    /**
     * Setter method for gerveiculo.
     *
     * @param aGerveiculo the new value for gerveiculo
     */
    public void setGerveiculo(Gerveiculo aGerveiculo) {
        gerveiculo = aGerveiculo;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Prodmodelo getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Prodmodelo aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for parktipo.
     *
     * @return the current value of parktipo
     */
    public Parktipo getParktipo() {
        return parktipo;
    }

    /**
     * Setter method for parktipo.
     *
     * @param aParktipo the new value for parktipo
     */
    public void setParktipo(Parktipo aParktipo) {
        parktipo = aParktipo;
    }

    /**
     * Access method for parktabela.
     *
     * @return the current value of parktabela
     */
    public Parktabela getParktabela() {
        return parktabela;
    }

    /**
     * Setter method for parktabela.
     *
     * @param aParktabela the new value for parktabela
     */
    public void setParktabela(Parktabela aParktabela) {
        parktabela = aParktabela;
    }

    /**
     * Access method for cobrforma.
     *
     * @return the current value of cobrforma
     */
    public Cobrforma getCobrforma() {
        return cobrforma;
    }

    /**
     * Setter method for cobrforma.
     *
     * @param aCobrforma the new value for cobrforma
     */
    public void setCobrforma(Cobrforma aCobrforma) {
        cobrforma = aCobrforma;
    }

    /**
     * Compares the key for this instance with another Parklancamento.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Parklancamento and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Parklancamento)) {
            return false;
        }
        Parklancamento that = (Parklancamento) other;
        if (this.getCodparklancamento() != that.getCodparklancamento()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Parklancamento.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Parklancamento)) return false;
        return this.equalKeys(other) && ((Parklancamento)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodparklancamento();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Parklancamento |");
        sb.append(" codparklancamento=").append(getCodparklancamento());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codparklancamento", Integer.valueOf(getCodparklancamento()));
        return ret;
    }

}
