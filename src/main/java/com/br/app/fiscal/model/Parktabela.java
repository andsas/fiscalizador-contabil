package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PARKTABELA")
public class Parktabela implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codparktabela";

    @Id
    @Column(name="CODPARKTABELA", unique=true, nullable=false, precision=10)
    private int codparktabela;
    @Column(name="DESCPARKTABELA", nullable=false, length=250)
    private String descparktabela;
    @Column(name="CODPARKTIPO", nullable=false, precision=10)
    private int codparktipo;
    @Column(name="PRINCIPAL", precision=5)
    private short principal;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @Column(name="TIPOPARKTABELA", precision=5)
    private short tipoparktabela;
    @Column(name="PRECOMAXIMO", precision=15, scale=4)
    private BigDecimal precomaximo;
    @OneToMany(mappedBy="parktabela")
    private Set<Parklancamento> parklancamento;
    @OneToMany(mappedBy="parktabela")
    private Set<Parktabeladet> parktabeladet;

    /** Default constructor. */
    public Parktabela() {
        super();
    }

    /**
     * Access method for codparktabela.
     *
     * @return the current value of codparktabela
     */
    public int getCodparktabela() {
        return codparktabela;
    }

    /**
     * Setter method for codparktabela.
     *
     * @param aCodparktabela the new value for codparktabela
     */
    public void setCodparktabela(int aCodparktabela) {
        codparktabela = aCodparktabela;
    }

    /**
     * Access method for descparktabela.
     *
     * @return the current value of descparktabela
     */
    public String getDescparktabela() {
        return descparktabela;
    }

    /**
     * Setter method for descparktabela.
     *
     * @param aDescparktabela the new value for descparktabela
     */
    public void setDescparktabela(String aDescparktabela) {
        descparktabela = aDescparktabela;
    }

    /**
     * Access method for codparktipo.
     *
     * @return the current value of codparktipo
     */
    public int getCodparktipo() {
        return codparktipo;
    }

    /**
     * Setter method for codparktipo.
     *
     * @param aCodparktipo the new value for codparktipo
     */
    public void setCodparktipo(int aCodparktipo) {
        codparktipo = aCodparktipo;
    }

    /**
     * Access method for principal.
     *
     * @return the current value of principal
     */
    public short getPrincipal() {
        return principal;
    }

    /**
     * Setter method for principal.
     *
     * @param aPrincipal the new value for principal
     */
    public void setPrincipal(short aPrincipal) {
        principal = aPrincipal;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for tipoparktabela.
     *
     * @return the current value of tipoparktabela
     */
    public short getTipoparktabela() {
        return tipoparktabela;
    }

    /**
     * Setter method for tipoparktabela.
     *
     * @param aTipoparktabela the new value for tipoparktabela
     */
    public void setTipoparktabela(short aTipoparktabela) {
        tipoparktabela = aTipoparktabela;
    }

    /**
     * Access method for precomaximo.
     *
     * @return the current value of precomaximo
     */
    public BigDecimal getPrecomaximo() {
        return precomaximo;
    }

    /**
     * Setter method for precomaximo.
     *
     * @param aPrecomaximo the new value for precomaximo
     */
    public void setPrecomaximo(BigDecimal aPrecomaximo) {
        precomaximo = aPrecomaximo;
    }

    /**
     * Access method for parklancamento.
     *
     * @return the current value of parklancamento
     */
    public Set<Parklancamento> getParklancamento() {
        return parklancamento;
    }

    /**
     * Setter method for parklancamento.
     *
     * @param aParklancamento the new value for parklancamento
     */
    public void setParklancamento(Set<Parklancamento> aParklancamento) {
        parklancamento = aParklancamento;
    }

    /**
     * Access method for parktabeladet.
     *
     * @return the current value of parktabeladet
     */
    public Set<Parktabeladet> getParktabeladet() {
        return parktabeladet;
    }

    /**
     * Setter method for parktabeladet.
     *
     * @param aParktabeladet the new value for parktabeladet
     */
    public void setParktabeladet(Set<Parktabeladet> aParktabeladet) {
        parktabeladet = aParktabeladet;
    }

    /**
     * Compares the key for this instance with another Parktabela.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Parktabela and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Parktabela)) {
            return false;
        }
        Parktabela that = (Parktabela) other;
        if (this.getCodparktabela() != that.getCodparktabela()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Parktabela.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Parktabela)) return false;
        return this.equalKeys(other) && ((Parktabela)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodparktabela();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Parktabela |");
        sb.append(" codparktabela=").append(getCodparktabela());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codparktabela", Integer.valueOf(getCodparktabela()));
        return ret;
    }

}
