package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PARKTABELADET")
public class Parktabeladet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codparktabeladet";

    @Id
    @Column(name="CODPARKTABELADET", unique=true, nullable=false, precision=10)
    private int codparktabeladet;
    @Column(name="CONDICAOVALORINICIAL", precision=15, scale=4)
    private BigDecimal condicaovalorinicial;
    @Column(name="CONDICAOVALORFINAL", precision=15, scale=4)
    private BigDecimal condicaovalorfinal;
    @Column(name="PRECO", precision=15, scale=4)
    private BigDecimal preco;
    @Column(name="FRACAOPRECO", precision=15, scale=4)
    private BigDecimal fracaopreco;
    @Column(name="CONDICAOTOLERANCIA", precision=15, scale=4)
    private BigDecimal condicaotolerancia;
    @Column(name="DESCONTO", precision=15, scale=4)
    private BigDecimal desconto;
    @Column(name="CONDICAOVALORTIPO", precision=5)
    private short condicaovalortipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPARKTABELA", nullable=false)
    private Parktabela parktabela;

    /** Default constructor. */
    public Parktabeladet() {
        super();
    }

    /**
     * Access method for codparktabeladet.
     *
     * @return the current value of codparktabeladet
     */
    public int getCodparktabeladet() {
        return codparktabeladet;
    }

    /**
     * Setter method for codparktabeladet.
     *
     * @param aCodparktabeladet the new value for codparktabeladet
     */
    public void setCodparktabeladet(int aCodparktabeladet) {
        codparktabeladet = aCodparktabeladet;
    }

    /**
     * Access method for condicaovalorinicial.
     *
     * @return the current value of condicaovalorinicial
     */
    public BigDecimal getCondicaovalorinicial() {
        return condicaovalorinicial;
    }

    /**
     * Setter method for condicaovalorinicial.
     *
     * @param aCondicaovalorinicial the new value for condicaovalorinicial
     */
    public void setCondicaovalorinicial(BigDecimal aCondicaovalorinicial) {
        condicaovalorinicial = aCondicaovalorinicial;
    }

    /**
     * Access method for condicaovalorfinal.
     *
     * @return the current value of condicaovalorfinal
     */
    public BigDecimal getCondicaovalorfinal() {
        return condicaovalorfinal;
    }

    /**
     * Setter method for condicaovalorfinal.
     *
     * @param aCondicaovalorfinal the new value for condicaovalorfinal
     */
    public void setCondicaovalorfinal(BigDecimal aCondicaovalorfinal) {
        condicaovalorfinal = aCondicaovalorfinal;
    }

    /**
     * Access method for preco.
     *
     * @return the current value of preco
     */
    public BigDecimal getPreco() {
        return preco;
    }

    /**
     * Setter method for preco.
     *
     * @param aPreco the new value for preco
     */
    public void setPreco(BigDecimal aPreco) {
        preco = aPreco;
    }

    /**
     * Access method for fracaopreco.
     *
     * @return the current value of fracaopreco
     */
    public BigDecimal getFracaopreco() {
        return fracaopreco;
    }

    /**
     * Setter method for fracaopreco.
     *
     * @param aFracaopreco the new value for fracaopreco
     */
    public void setFracaopreco(BigDecimal aFracaopreco) {
        fracaopreco = aFracaopreco;
    }

    /**
     * Access method for condicaotolerancia.
     *
     * @return the current value of condicaotolerancia
     */
    public BigDecimal getCondicaotolerancia() {
        return condicaotolerancia;
    }

    /**
     * Setter method for condicaotolerancia.
     *
     * @param aCondicaotolerancia the new value for condicaotolerancia
     */
    public void setCondicaotolerancia(BigDecimal aCondicaotolerancia) {
        condicaotolerancia = aCondicaotolerancia;
    }

    /**
     * Access method for desconto.
     *
     * @return the current value of desconto
     */
    public BigDecimal getDesconto() {
        return desconto;
    }

    /**
     * Setter method for desconto.
     *
     * @param aDesconto the new value for desconto
     */
    public void setDesconto(BigDecimal aDesconto) {
        desconto = aDesconto;
    }

    /**
     * Access method for condicaovalortipo.
     *
     * @return the current value of condicaovalortipo
     */
    public short getCondicaovalortipo() {
        return condicaovalortipo;
    }

    /**
     * Setter method for condicaovalortipo.
     *
     * @param aCondicaovalortipo the new value for condicaovalortipo
     */
    public void setCondicaovalortipo(short aCondicaovalortipo) {
        condicaovalortipo = aCondicaovalortipo;
    }

    /**
     * Access method for parktabela.
     *
     * @return the current value of parktabela
     */
    public Parktabela getParktabela() {
        return parktabela;
    }

    /**
     * Setter method for parktabela.
     *
     * @param aParktabela the new value for parktabela
     */
    public void setParktabela(Parktabela aParktabela) {
        parktabela = aParktabela;
    }

    /**
     * Compares the key for this instance with another Parktabeladet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Parktabeladet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Parktabeladet)) {
            return false;
        }
        Parktabeladet that = (Parktabeladet) other;
        if (this.getCodparktabeladet() != that.getCodparktabeladet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Parktabeladet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Parktabeladet)) return false;
        return this.equalKeys(other) && ((Parktabeladet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodparktabeladet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Parktabeladet |");
        sb.append(" codparktabeladet=").append(getCodparktabeladet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codparktabeladet", Integer.valueOf(getCodparktabeladet()));
        return ret;
    }

}
