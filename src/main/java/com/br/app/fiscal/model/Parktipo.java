package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PARKTIPO")
public class Parktipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codparktipo";

    @Id
    @Column(name="CODPARKTIPO", unique=true, nullable=false, precision=10)
    private int codparktipo;
    @Column(name="DESCPARKTIPO", nullable=false, length=250)
    private String descparktipo;
    @OneToMany(mappedBy="parktipo")
    private Set<Parklancamento> parklancamento;

    /** Default constructor. */
    public Parktipo() {
        super();
    }

    /**
     * Access method for codparktipo.
     *
     * @return the current value of codparktipo
     */
    public int getCodparktipo() {
        return codparktipo;
    }

    /**
     * Setter method for codparktipo.
     *
     * @param aCodparktipo the new value for codparktipo
     */
    public void setCodparktipo(int aCodparktipo) {
        codparktipo = aCodparktipo;
    }

    /**
     * Access method for descparktipo.
     *
     * @return the current value of descparktipo
     */
    public String getDescparktipo() {
        return descparktipo;
    }

    /**
     * Setter method for descparktipo.
     *
     * @param aDescparktipo the new value for descparktipo
     */
    public void setDescparktipo(String aDescparktipo) {
        descparktipo = aDescparktipo;
    }

    /**
     * Access method for parklancamento.
     *
     * @return the current value of parklancamento
     */
    public Set<Parklancamento> getParklancamento() {
        return parklancamento;
    }

    /**
     * Setter method for parklancamento.
     *
     * @param aParklancamento the new value for parklancamento
     */
    public void setParklancamento(Set<Parklancamento> aParklancamento) {
        parklancamento = aParklancamento;
    }

    /**
     * Compares the key for this instance with another Parktipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Parktipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Parktipo)) {
            return false;
        }
        Parktipo that = (Parktipo) other;
        if (this.getCodparktipo() != that.getCodparktipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Parktipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Parktipo)) return false;
        return this.equalKeys(other) && ((Parktipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodparktipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Parktipo |");
        sb.append(" codparktipo=").append(getCodparktipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codparktipo", Integer.valueOf(getCodparktipo()));
        return ret;
    }

}
