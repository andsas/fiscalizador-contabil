package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PRDCFORMULA")
public class Prdcformula implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprdcformula";

    @Id
    @Column(name="CODPRDCFORMULA", unique=true, nullable=false, precision=10)
    private int codprdcformula;
    @Column(name="FORMULA", nullable=false)
    private String formula;
    @Column(name="DESCPRDCFORMULA", nullable=false, length=250)
    private String descprdcformula;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODMODELO", nullable=false)
    private Prodmodelo prodmodelo;

    /** Default constructor. */
    public Prdcformula() {
        super();
    }

    /**
     * Access method for codprdcformula.
     *
     * @return the current value of codprdcformula
     */
    public int getCodprdcformula() {
        return codprdcformula;
    }

    /**
     * Setter method for codprdcformula.
     *
     * @param aCodprdcformula the new value for codprdcformula
     */
    public void setCodprdcformula(int aCodprdcformula) {
        codprdcformula = aCodprdcformula;
    }

    /**
     * Access method for formula.
     *
     * @return the current value of formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Setter method for formula.
     *
     * @param aFormula the new value for formula
     */
    public void setFormula(String aFormula) {
        formula = aFormula;
    }

    /**
     * Access method for descprdcformula.
     *
     * @return the current value of descprdcformula
     */
    public String getDescprdcformula() {
        return descprdcformula;
    }

    /**
     * Setter method for descprdcformula.
     *
     * @param aDescprdcformula the new value for descprdcformula
     */
    public void setDescprdcformula(String aDescprdcformula) {
        descprdcformula = aDescprdcformula;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Prodmodelo getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Prodmodelo aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Compares the key for this instance with another Prdcformula.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prdcformula and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prdcformula)) {
            return false;
        }
        Prdcformula that = (Prdcformula) other;
        if (this.getCodprdcformula() != that.getCodprdcformula()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prdcformula.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prdcformula)) return false;
        return this.equalKeys(other) && ((Prdcformula)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprdcformula();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prdcformula |");
        sb.append(" codprdcformula=").append(getCodprdcformula());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprdcformula", Integer.valueOf(getCodprdcformula()));
        return ret;
    }

}
