package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRDCORDEM")
public class Prdcordem implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprdcordem";

    @Id
    @Column(name="CODPRDCORDEM", unique=true, nullable=false, precision=10)
    private int codprdcordem;
    @Column(name="DATAENTRADA", nullable=false)
    private Timestamp dataentrada;
    @Column(name="DATAINICIO")
    private Timestamp datainicio;
    @Column(name="DATAFINAL")
    private Timestamp datafinal;
    @Column(name="OBSCLIENTE")
    private String obscliente;
    @Column(name="OBSGERAL")
    private String obsgeral;
    @Column(name="SITUACAO", precision=5)
    private short situacao;
    @Column(name="ID", length=40)
    private String id;
    @OneToMany(mappedBy="prdcordem2")
    private Set<Optransacao> optransacao2;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODOPTRANSACAO", nullable=false)
    private Optransacao optransacao;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIORESP")
    private Confusuario confusuario;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRDCPLANO", nullable=false)
    private Prdcplano prdcplano;
    @ManyToOne
    @JoinColumn(name="CODPRDCTIPO")
    private Prdcordemtipo prdcordemtipo;
    @OneToMany(mappedBy="prdcordem")
    private Set<Prdcordemetapa> prdcordemetapa;

    /** Default constructor. */
    public Prdcordem() {
        super();
    }

    /**
     * Access method for codprdcordem.
     *
     * @return the current value of codprdcordem
     */
    public int getCodprdcordem() {
        return codprdcordem;
    }

    /**
     * Setter method for codprdcordem.
     *
     * @param aCodprdcordem the new value for codprdcordem
     */
    public void setCodprdcordem(int aCodprdcordem) {
        codprdcordem = aCodprdcordem;
    }

    /**
     * Access method for dataentrada.
     *
     * @return the current value of dataentrada
     */
    public Timestamp getDataentrada() {
        return dataentrada;
    }

    /**
     * Setter method for dataentrada.
     *
     * @param aDataentrada the new value for dataentrada
     */
    public void setDataentrada(Timestamp aDataentrada) {
        dataentrada = aDataentrada;
    }

    /**
     * Access method for datainicio.
     *
     * @return the current value of datainicio
     */
    public Timestamp getDatainicio() {
        return datainicio;
    }

    /**
     * Setter method for datainicio.
     *
     * @param aDatainicio the new value for datainicio
     */
    public void setDatainicio(Timestamp aDatainicio) {
        datainicio = aDatainicio;
    }

    /**
     * Access method for datafinal.
     *
     * @return the current value of datafinal
     */
    public Timestamp getDatafinal() {
        return datafinal;
    }

    /**
     * Setter method for datafinal.
     *
     * @param aDatafinal the new value for datafinal
     */
    public void setDatafinal(Timestamp aDatafinal) {
        datafinal = aDatafinal;
    }

    /**
     * Access method for obscliente.
     *
     * @return the current value of obscliente
     */
    public String getObscliente() {
        return obscliente;
    }

    /**
     * Setter method for obscliente.
     *
     * @param aObscliente the new value for obscliente
     */
    public void setObscliente(String aObscliente) {
        obscliente = aObscliente;
    }

    /**
     * Access method for obsgeral.
     *
     * @return the current value of obsgeral
     */
    public String getObsgeral() {
        return obsgeral;
    }

    /**
     * Setter method for obsgeral.
     *
     * @param aObsgeral the new value for obsgeral
     */
    public void setObsgeral(String aObsgeral) {
        obsgeral = aObsgeral;
    }

    /**
     * Access method for situacao.
     *
     * @return the current value of situacao
     */
    public short getSituacao() {
        return situacao;
    }

    /**
     * Setter method for situacao.
     *
     * @param aSituacao the new value for situacao
     */
    public void setSituacao(short aSituacao) {
        situacao = aSituacao;
    }

    /**
     * Access method for id.
     *
     * @return the current value of id
     */
    public String getId() {
        return id;
    }

    /**
     * Setter method for id.
     *
     * @param aId the new value for id
     */
    public void setId(String aId) {
        id = aId;
    }

    /**
     * Access method for optransacao2.
     *
     * @return the current value of optransacao2
     */
    public Set<Optransacao> getOptransacao2() {
        return optransacao2;
    }

    /**
     * Setter method for optransacao2.
     *
     * @param aOptransacao2 the new value for optransacao2
     */
    public void setOptransacao2(Set<Optransacao> aOptransacao2) {
        optransacao2 = aOptransacao2;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Optransacao getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Optransacao aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for prdcplano.
     *
     * @return the current value of prdcplano
     */
    public Prdcplano getPrdcplano() {
        return prdcplano;
    }

    /**
     * Setter method for prdcplano.
     *
     * @param aPrdcplano the new value for prdcplano
     */
    public void setPrdcplano(Prdcplano aPrdcplano) {
        prdcplano = aPrdcplano;
    }

    /**
     * Access method for prdcordemtipo.
     *
     * @return the current value of prdcordemtipo
     */
    public Prdcordemtipo getPrdcordemtipo() {
        return prdcordemtipo;
    }

    /**
     * Setter method for prdcordemtipo.
     *
     * @param aPrdcordemtipo the new value for prdcordemtipo
     */
    public void setPrdcordemtipo(Prdcordemtipo aPrdcordemtipo) {
        prdcordemtipo = aPrdcordemtipo;
    }

    /**
     * Access method for prdcordemetapa.
     *
     * @return the current value of prdcordemetapa
     */
    public Set<Prdcordemetapa> getPrdcordemetapa() {
        return prdcordemetapa;
    }

    /**
     * Setter method for prdcordemetapa.
     *
     * @param aPrdcordemetapa the new value for prdcordemetapa
     */
    public void setPrdcordemetapa(Set<Prdcordemetapa> aPrdcordemetapa) {
        prdcordemetapa = aPrdcordemetapa;
    }

    /**
     * Compares the key for this instance with another Prdcordem.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prdcordem and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prdcordem)) {
            return false;
        }
        Prdcordem that = (Prdcordem) other;
        if (this.getCodprdcordem() != that.getCodprdcordem()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prdcordem.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prdcordem)) return false;
        return this.equalKeys(other) && ((Prdcordem)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprdcordem();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prdcordem |");
        sb.append(" codprdcordem=").append(getCodprdcordem());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprdcordem", Integer.valueOf(getCodprdcordem()));
        return ret;
    }

}
