package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRDCORDEMETAPA")
public class Prdcordemetapa implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprdcordemetapa";

    @Id
    @Column(name="CODPRDCORDEMETAPA", unique=true, nullable=false, precision=10)
    private int codprdcordemetapa;
    @Column(name="DATAENTRADA")
    private Timestamp dataentrada;
    @Column(name="DATASAIDA")
    private Timestamp datasaida;
    @Column(name="OBSRESPONSAVEL")
    private String obsresponsavel;
    @Column(name="OBSTECNICAS")
    private String obstecnicas;
    @Column(name="SITUACAO", precision=5)
    private short situacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRDCORDEM", nullable=false)
    private Prdcordem prdcordem;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRDCPLANOETAPA", nullable=false)
    private Prdcplanoetapa prdcplanoetapa;
    @OneToMany(mappedBy="prdcordemetapa")
    private Set<Prdcordemetapaproduto> prdcordemetapaproduto;

    /** Default constructor. */
    public Prdcordemetapa() {
        super();
    }

    /**
     * Access method for codprdcordemetapa.
     *
     * @return the current value of codprdcordemetapa
     */
    public int getCodprdcordemetapa() {
        return codprdcordemetapa;
    }

    /**
     * Setter method for codprdcordemetapa.
     *
     * @param aCodprdcordemetapa the new value for codprdcordemetapa
     */
    public void setCodprdcordemetapa(int aCodprdcordemetapa) {
        codprdcordemetapa = aCodprdcordemetapa;
    }

    /**
     * Access method for dataentrada.
     *
     * @return the current value of dataentrada
     */
    public Timestamp getDataentrada() {
        return dataentrada;
    }

    /**
     * Setter method for dataentrada.
     *
     * @param aDataentrada the new value for dataentrada
     */
    public void setDataentrada(Timestamp aDataentrada) {
        dataentrada = aDataentrada;
    }

    /**
     * Access method for datasaida.
     *
     * @return the current value of datasaida
     */
    public Timestamp getDatasaida() {
        return datasaida;
    }

    /**
     * Setter method for datasaida.
     *
     * @param aDatasaida the new value for datasaida
     */
    public void setDatasaida(Timestamp aDatasaida) {
        datasaida = aDatasaida;
    }

    /**
     * Access method for obsresponsavel.
     *
     * @return the current value of obsresponsavel
     */
    public String getObsresponsavel() {
        return obsresponsavel;
    }

    /**
     * Setter method for obsresponsavel.
     *
     * @param aObsresponsavel the new value for obsresponsavel
     */
    public void setObsresponsavel(String aObsresponsavel) {
        obsresponsavel = aObsresponsavel;
    }

    /**
     * Access method for obstecnicas.
     *
     * @return the current value of obstecnicas
     */
    public String getObstecnicas() {
        return obstecnicas;
    }

    /**
     * Setter method for obstecnicas.
     *
     * @param aObstecnicas the new value for obstecnicas
     */
    public void setObstecnicas(String aObstecnicas) {
        obstecnicas = aObstecnicas;
    }

    /**
     * Access method for situacao.
     *
     * @return the current value of situacao
     */
    public short getSituacao() {
        return situacao;
    }

    /**
     * Setter method for situacao.
     *
     * @param aSituacao the new value for situacao
     */
    public void setSituacao(short aSituacao) {
        situacao = aSituacao;
    }

    /**
     * Access method for prdcordem.
     *
     * @return the current value of prdcordem
     */
    public Prdcordem getPrdcordem() {
        return prdcordem;
    }

    /**
     * Setter method for prdcordem.
     *
     * @param aPrdcordem the new value for prdcordem
     */
    public void setPrdcordem(Prdcordem aPrdcordem) {
        prdcordem = aPrdcordem;
    }

    /**
     * Access method for prdcplanoetapa.
     *
     * @return the current value of prdcplanoetapa
     */
    public Prdcplanoetapa getPrdcplanoetapa() {
        return prdcplanoetapa;
    }

    /**
     * Setter method for prdcplanoetapa.
     *
     * @param aPrdcplanoetapa the new value for prdcplanoetapa
     */
    public void setPrdcplanoetapa(Prdcplanoetapa aPrdcplanoetapa) {
        prdcplanoetapa = aPrdcplanoetapa;
    }

    /**
     * Access method for prdcordemetapaproduto.
     *
     * @return the current value of prdcordemetapaproduto
     */
    public Set<Prdcordemetapaproduto> getPrdcordemetapaproduto() {
        return prdcordemetapaproduto;
    }

    /**
     * Setter method for prdcordemetapaproduto.
     *
     * @param aPrdcordemetapaproduto the new value for prdcordemetapaproduto
     */
    public void setPrdcordemetapaproduto(Set<Prdcordemetapaproduto> aPrdcordemetapaproduto) {
        prdcordemetapaproduto = aPrdcordemetapaproduto;
    }

    /**
     * Compares the key for this instance with another Prdcordemetapa.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prdcordemetapa and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prdcordemetapa)) {
            return false;
        }
        Prdcordemetapa that = (Prdcordemetapa) other;
        if (this.getCodprdcordemetapa() != that.getCodprdcordemetapa()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prdcordemetapa.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prdcordemetapa)) return false;
        return this.equalKeys(other) && ((Prdcordemetapa)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprdcordemetapa();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prdcordemetapa |");
        sb.append(" codprdcordemetapa=").append(getCodprdcordemetapa());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprdcordemetapa", Integer.valueOf(getCodprdcordemetapa()));
        return ret;
    }

}
