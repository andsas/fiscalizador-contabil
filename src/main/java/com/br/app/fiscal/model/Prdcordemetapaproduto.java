package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PRDCORDEMETAPAPRODUTO")
public class Prdcordemetapaproduto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprdcordemetapaproduto";

    @Id
    @Column(name="CODPRDCORDEMETAPAPRODUTO", unique=true, nullable=false, precision=10)
    private int codprdcordemetapaproduto;
    @Column(name="QUANTIDADEPREVISTA", precision=15, scale=4)
    private BigDecimal quantidadeprevista;
    @Column(name="QUANTIDADEREAL", precision=15, scale=4)
    private BigDecimal quantidadereal;
    @Column(name="SITUACAO", precision=5)
    private short situacao;
    @Column(name="DATAENTRADA")
    private Timestamp dataentrada;
    @Column(name="DATASAIDA")
    private Timestamp datasaida;
    @ManyToOne
    @JoinColumn(name="CODOPTRANSACAODET")
    private Optransacaodet optransacaodet;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRDCORDEMETAPA", nullable=false)
    private Prdcordemetapa prdcordemetapa;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;

    /** Default constructor. */
    public Prdcordemetapaproduto() {
        super();
    }

    /**
     * Access method for codprdcordemetapaproduto.
     *
     * @return the current value of codprdcordemetapaproduto
     */
    public int getCodprdcordemetapaproduto() {
        return codprdcordemetapaproduto;
    }

    /**
     * Setter method for codprdcordemetapaproduto.
     *
     * @param aCodprdcordemetapaproduto the new value for codprdcordemetapaproduto
     */
    public void setCodprdcordemetapaproduto(int aCodprdcordemetapaproduto) {
        codprdcordemetapaproduto = aCodprdcordemetapaproduto;
    }

    /**
     * Access method for quantidadeprevista.
     *
     * @return the current value of quantidadeprevista
     */
    public BigDecimal getQuantidadeprevista() {
        return quantidadeprevista;
    }

    /**
     * Setter method for quantidadeprevista.
     *
     * @param aQuantidadeprevista the new value for quantidadeprevista
     */
    public void setQuantidadeprevista(BigDecimal aQuantidadeprevista) {
        quantidadeprevista = aQuantidadeprevista;
    }

    /**
     * Access method for quantidadereal.
     *
     * @return the current value of quantidadereal
     */
    public BigDecimal getQuantidadereal() {
        return quantidadereal;
    }

    /**
     * Setter method for quantidadereal.
     *
     * @param aQuantidadereal the new value for quantidadereal
     */
    public void setQuantidadereal(BigDecimal aQuantidadereal) {
        quantidadereal = aQuantidadereal;
    }

    /**
     * Access method for situacao.
     *
     * @return the current value of situacao
     */
    public short getSituacao() {
        return situacao;
    }

    /**
     * Setter method for situacao.
     *
     * @param aSituacao the new value for situacao
     */
    public void setSituacao(short aSituacao) {
        situacao = aSituacao;
    }

    /**
     * Access method for dataentrada.
     *
     * @return the current value of dataentrada
     */
    public Timestamp getDataentrada() {
        return dataentrada;
    }

    /**
     * Setter method for dataentrada.
     *
     * @param aDataentrada the new value for dataentrada
     */
    public void setDataentrada(Timestamp aDataentrada) {
        dataentrada = aDataentrada;
    }

    /**
     * Access method for datasaida.
     *
     * @return the current value of datasaida
     */
    public Timestamp getDatasaida() {
        return datasaida;
    }

    /**
     * Setter method for datasaida.
     *
     * @param aDatasaida the new value for datasaida
     */
    public void setDatasaida(Timestamp aDatasaida) {
        datasaida = aDatasaida;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Optransacaodet getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Optransacaodet aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Access method for prdcordemetapa.
     *
     * @return the current value of prdcordemetapa
     */
    public Prdcordemetapa getPrdcordemetapa() {
        return prdcordemetapa;
    }

    /**
     * Setter method for prdcordemetapa.
     *
     * @param aPrdcordemetapa the new value for prdcordemetapa
     */
    public void setPrdcordemetapa(Prdcordemetapa aPrdcordemetapa) {
        prdcordemetapa = aPrdcordemetapa;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Prdcordemetapaproduto.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prdcordemetapaproduto and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prdcordemetapaproduto)) {
            return false;
        }
        Prdcordemetapaproduto that = (Prdcordemetapaproduto) other;
        if (this.getCodprdcordemetapaproduto() != that.getCodprdcordemetapaproduto()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prdcordemetapaproduto.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prdcordemetapaproduto)) return false;
        return this.equalKeys(other) && ((Prdcordemetapaproduto)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprdcordemetapaproduto();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prdcordemetapaproduto |");
        sb.append(" codprdcordemetapaproduto=").append(getCodprdcordemetapaproduto());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprdcordemetapaproduto", Integer.valueOf(getCodprdcordemetapaproduto()));
        return ret;
    }

}
