package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRDCORDEMTIPO")
public class Prdcordemtipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprdcordemtipo";

    @Id
    @Column(name="CODPRDCORDEMTIPO", unique=true, nullable=false, precision=10)
    private int codprdcordemtipo;
    @Column(name="DESCPRDCORDEMTIPO", nullable=false, length=250)
    private String descprdcordemtipo;
    @Column(name="CORPRDCORDEMTIPO", nullable=false, precision=10)
    private int corprdcordemtipo;
    @OneToMany(mappedBy="prdcordemtipo")
    private Set<Prdcordem> prdcordem;

    /** Default constructor. */
    public Prdcordemtipo() {
        super();
    }

    /**
     * Access method for codprdcordemtipo.
     *
     * @return the current value of codprdcordemtipo
     */
    public int getCodprdcordemtipo() {
        return codprdcordemtipo;
    }

    /**
     * Setter method for codprdcordemtipo.
     *
     * @param aCodprdcordemtipo the new value for codprdcordemtipo
     */
    public void setCodprdcordemtipo(int aCodprdcordemtipo) {
        codprdcordemtipo = aCodprdcordemtipo;
    }

    /**
     * Access method for descprdcordemtipo.
     *
     * @return the current value of descprdcordemtipo
     */
    public String getDescprdcordemtipo() {
        return descprdcordemtipo;
    }

    /**
     * Setter method for descprdcordemtipo.
     *
     * @param aDescprdcordemtipo the new value for descprdcordemtipo
     */
    public void setDescprdcordemtipo(String aDescprdcordemtipo) {
        descprdcordemtipo = aDescprdcordemtipo;
    }

    /**
     * Access method for corprdcordemtipo.
     *
     * @return the current value of corprdcordemtipo
     */
    public int getCorprdcordemtipo() {
        return corprdcordemtipo;
    }

    /**
     * Setter method for corprdcordemtipo.
     *
     * @param aCorprdcordemtipo the new value for corprdcordemtipo
     */
    public void setCorprdcordemtipo(int aCorprdcordemtipo) {
        corprdcordemtipo = aCorprdcordemtipo;
    }

    /**
     * Access method for prdcordem.
     *
     * @return the current value of prdcordem
     */
    public Set<Prdcordem> getPrdcordem() {
        return prdcordem;
    }

    /**
     * Setter method for prdcordem.
     *
     * @param aPrdcordem the new value for prdcordem
     */
    public void setPrdcordem(Set<Prdcordem> aPrdcordem) {
        prdcordem = aPrdcordem;
    }

    /**
     * Compares the key for this instance with another Prdcordemtipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prdcordemtipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prdcordemtipo)) {
            return false;
        }
        Prdcordemtipo that = (Prdcordemtipo) other;
        if (this.getCodprdcordemtipo() != that.getCodprdcordemtipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prdcordemtipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prdcordemtipo)) return false;
        return this.equalKeys(other) && ((Prdcordemtipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprdcordemtipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prdcordemtipo |");
        sb.append(" codprdcordemtipo=").append(getCodprdcordemtipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprdcordemtipo", Integer.valueOf(getCodprdcordemtipo()));
        return ret;
    }

}
