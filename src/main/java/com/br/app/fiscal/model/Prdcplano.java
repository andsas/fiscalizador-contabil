package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRDCPLANO")
public class Prdcplano implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprdcplano";

    @Id
    @Column(name="CODPRDCPLANO", unique=true, nullable=false, precision=10)
    private int codprdcplano;
    @Column(name="DESCPRDCPLANO", nullable=false, length=250)
    private String descprdcplano;
    @Column(name="DATAVALIDADE")
    private Timestamp datavalidade;
    @Column(name="DESCCOMPLETA")
    private String desccompleta;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @OneToMany(mappedBy="prdcplano")
    private Set<Optipo> optipo;
    @OneToMany(mappedBy="prdcplano")
    private Set<Prdcordem> prdcordem;
    @ManyToOne
    @JoinColumn(name="CODAGRESPONSAVEL")
    private Agagente agagente;
    @OneToMany(mappedBy="prdcplano")
    private Set<Prdcplanoetapa> prdcplanoetapa;

    /** Default constructor. */
    public Prdcplano() {
        super();
    }

    /**
     * Access method for codprdcplano.
     *
     * @return the current value of codprdcplano
     */
    public int getCodprdcplano() {
        return codprdcplano;
    }

    /**
     * Setter method for codprdcplano.
     *
     * @param aCodprdcplano the new value for codprdcplano
     */
    public void setCodprdcplano(int aCodprdcplano) {
        codprdcplano = aCodprdcplano;
    }

    /**
     * Access method for descprdcplano.
     *
     * @return the current value of descprdcplano
     */
    public String getDescprdcplano() {
        return descprdcplano;
    }

    /**
     * Setter method for descprdcplano.
     *
     * @param aDescprdcplano the new value for descprdcplano
     */
    public void setDescprdcplano(String aDescprdcplano) {
        descprdcplano = aDescprdcplano;
    }

    /**
     * Access method for datavalidade.
     *
     * @return the current value of datavalidade
     */
    public Timestamp getDatavalidade() {
        return datavalidade;
    }

    /**
     * Setter method for datavalidade.
     *
     * @param aDatavalidade the new value for datavalidade
     */
    public void setDatavalidade(Timestamp aDatavalidade) {
        datavalidade = aDatavalidade;
    }

    /**
     * Access method for desccompleta.
     *
     * @return the current value of desccompleta
     */
    public String getDesccompleta() {
        return desccompleta;
    }

    /**
     * Setter method for desccompleta.
     *
     * @param aDesccompleta the new value for desccompleta
     */
    public void setDesccompleta(String aDesccompleta) {
        desccompleta = aDesccompleta;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for optipo.
     *
     * @return the current value of optipo
     */
    public Set<Optipo> getOptipo() {
        return optipo;
    }

    /**
     * Setter method for optipo.
     *
     * @param aOptipo the new value for optipo
     */
    public void setOptipo(Set<Optipo> aOptipo) {
        optipo = aOptipo;
    }

    /**
     * Access method for prdcordem.
     *
     * @return the current value of prdcordem
     */
    public Set<Prdcordem> getPrdcordem() {
        return prdcordem;
    }

    /**
     * Setter method for prdcordem.
     *
     * @param aPrdcordem the new value for prdcordem
     */
    public void setPrdcordem(Set<Prdcordem> aPrdcordem) {
        prdcordem = aPrdcordem;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for prdcplanoetapa.
     *
     * @return the current value of prdcplanoetapa
     */
    public Set<Prdcplanoetapa> getPrdcplanoetapa() {
        return prdcplanoetapa;
    }

    /**
     * Setter method for prdcplanoetapa.
     *
     * @param aPrdcplanoetapa the new value for prdcplanoetapa
     */
    public void setPrdcplanoetapa(Set<Prdcplanoetapa> aPrdcplanoetapa) {
        prdcplanoetapa = aPrdcplanoetapa;
    }

    /**
     * Compares the key for this instance with another Prdcplano.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prdcplano and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prdcplano)) {
            return false;
        }
        Prdcplano that = (Prdcplano) other;
        if (this.getCodprdcplano() != that.getCodprdcplano()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prdcplano.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prdcplano)) return false;
        return this.equalKeys(other) && ((Prdcplano)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprdcplano();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prdcplano |");
        sb.append(" codprdcplano=").append(getCodprdcplano());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprdcplano", Integer.valueOf(getCodprdcplano()));
        return ret;
    }

}
