package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRDCPLANOETAPA")
public class Prdcplanoetapa implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprdcplanoetapa";

    @Id
    @Column(name="CODPRDCPLANOETAPA", unique=true, nullable=false, precision=10)
    private int codprdcplanoetapa;
    @Column(name="DESCPRDCPLANOETAPA", nullable=false, length=250)
    private String descprdcplanoetapa;
    @Column(name="TIPOPRODFINAL", precision=5)
    private short tipoprodfinal;
    @Column(name="TIPOPRODPARTE", precision=5)
    private short tipoprodparte;
    @Column(name="ETAPAGERATRANSACAO", precision=5)
    private short etapageratransacao;
    @OneToMany(mappedBy="prdcplanoetapa")
    private Set<Prdcordemetapa> prdcordemetapa;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRDCPLANO", nullable=false)
    private Prdcplano prdcplano;
    @ManyToOne
    @JoinColumn(name="CODAGRESPONSAVEL")
    private Agagente agagente;
    @ManyToOne
    @JoinColumn(name="CODOPOPERACAO")
    private Opoperacao opoperacao;
    @OneToMany(mappedBy="prdcplanoetapa")
    private Set<Prodprodutocomp> prodprodutocomp;

    /** Default constructor. */
    public Prdcplanoetapa() {
        super();
    }

    /**
     * Access method for codprdcplanoetapa.
     *
     * @return the current value of codprdcplanoetapa
     */
    public int getCodprdcplanoetapa() {
        return codprdcplanoetapa;
    }

    /**
     * Setter method for codprdcplanoetapa.
     *
     * @param aCodprdcplanoetapa the new value for codprdcplanoetapa
     */
    public void setCodprdcplanoetapa(int aCodprdcplanoetapa) {
        codprdcplanoetapa = aCodprdcplanoetapa;
    }

    /**
     * Access method for descprdcplanoetapa.
     *
     * @return the current value of descprdcplanoetapa
     */
    public String getDescprdcplanoetapa() {
        return descprdcplanoetapa;
    }

    /**
     * Setter method for descprdcplanoetapa.
     *
     * @param aDescprdcplanoetapa the new value for descprdcplanoetapa
     */
    public void setDescprdcplanoetapa(String aDescprdcplanoetapa) {
        descprdcplanoetapa = aDescprdcplanoetapa;
    }

    /**
     * Access method for tipoprodfinal.
     *
     * @return the current value of tipoprodfinal
     */
    public short getTipoprodfinal() {
        return tipoprodfinal;
    }

    /**
     * Setter method for tipoprodfinal.
     *
     * @param aTipoprodfinal the new value for tipoprodfinal
     */
    public void setTipoprodfinal(short aTipoprodfinal) {
        tipoprodfinal = aTipoprodfinal;
    }

    /**
     * Access method for tipoprodparte.
     *
     * @return the current value of tipoprodparte
     */
    public short getTipoprodparte() {
        return tipoprodparte;
    }

    /**
     * Setter method for tipoprodparte.
     *
     * @param aTipoprodparte the new value for tipoprodparte
     */
    public void setTipoprodparte(short aTipoprodparte) {
        tipoprodparte = aTipoprodparte;
    }

    /**
     * Access method for etapageratransacao.
     *
     * @return the current value of etapageratransacao
     */
    public short getEtapageratransacao() {
        return etapageratransacao;
    }

    /**
     * Setter method for etapageratransacao.
     *
     * @param aEtapageratransacao the new value for etapageratransacao
     */
    public void setEtapageratransacao(short aEtapageratransacao) {
        etapageratransacao = aEtapageratransacao;
    }

    /**
     * Access method for prdcordemetapa.
     *
     * @return the current value of prdcordemetapa
     */
    public Set<Prdcordemetapa> getPrdcordemetapa() {
        return prdcordemetapa;
    }

    /**
     * Setter method for prdcordemetapa.
     *
     * @param aPrdcordemetapa the new value for prdcordemetapa
     */
    public void setPrdcordemetapa(Set<Prdcordemetapa> aPrdcordemetapa) {
        prdcordemetapa = aPrdcordemetapa;
    }

    /**
     * Access method for prdcplano.
     *
     * @return the current value of prdcplano
     */
    public Prdcplano getPrdcplano() {
        return prdcplano;
    }

    /**
     * Setter method for prdcplano.
     *
     * @param aPrdcplano the new value for prdcplano
     */
    public void setPrdcplano(Prdcplano aPrdcplano) {
        prdcplano = aPrdcplano;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for opoperacao.
     *
     * @return the current value of opoperacao
     */
    public Opoperacao getOpoperacao() {
        return opoperacao;
    }

    /**
     * Setter method for opoperacao.
     *
     * @param aOpoperacao the new value for opoperacao
     */
    public void setOpoperacao(Opoperacao aOpoperacao) {
        opoperacao = aOpoperacao;
    }

    /**
     * Access method for prodprodutocomp.
     *
     * @return the current value of prodprodutocomp
     */
    public Set<Prodprodutocomp> getProdprodutocomp() {
        return prodprodutocomp;
    }

    /**
     * Setter method for prodprodutocomp.
     *
     * @param aProdprodutocomp the new value for prodprodutocomp
     */
    public void setProdprodutocomp(Set<Prodprodutocomp> aProdprodutocomp) {
        prodprodutocomp = aProdprodutocomp;
    }

    /**
     * Compares the key for this instance with another Prdcplanoetapa.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prdcplanoetapa and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prdcplanoetapa)) {
            return false;
        }
        Prdcplanoetapa that = (Prdcplanoetapa) other;
        if (this.getCodprdcplanoetapa() != that.getCodprdcplanoetapa()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prdcplanoetapa.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prdcplanoetapa)) return false;
        return this.equalKeys(other) && ((Prdcplanoetapa)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprdcplanoetapa();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prdcplanoetapa |");
        sb.append(" codprdcplanoetapa=").append(getCodprdcplanoetapa());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprdcplanoetapa", Integer.valueOf(getCodprdcplanoetapa()));
        return ret;
    }

}
