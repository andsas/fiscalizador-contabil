package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRODAPOLICE")
public class Prodapolice implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodapolice";

    @Id
    @Column(name="CODPRODAPOLICE", unique=true, nullable=false, precision=10)
    private int codprodapolice;
    @Column(name="DESCPRODAPOLICE", nullable=false, length=250)
    private String descprodapolice;
    @Column(name="DESCCOMPLETA")
    private String desccompleta;
    @Column(name="DATAVALIDADE", nullable=false)
    private Timestamp datavalidade;
    @Column(name="DATA")
    private Timestamp data;
    @Column(name="VALORPREMIO", nullable=false, precision=15, scale=4)
    private BigDecimal valorpremio;
    @Column(name="VALORFRANQUIA", precision=15, scale=4)
    private BigDecimal valorfranquia;
    @Column(name="VALORSEGURADO", precision=15, scale=4)
    private BigDecimal valorsegurado;
    @Column(name="ATIVO", precision=5)
    private short ativo;
    @Column(name="NUMPRODAPOLICE", nullable=false, length=40)
    private String numprodapolice;
    @OneToMany(mappedBy="prodapolice")
    private Set<Ctbbem> ctbbem;
    @OneToMany(mappedBy="prodapolice")
    private Set<Ctbbemimoapolice> ctbbemimoapolice;
    @OneToMany(mappedBy="prodapolice")
    private Set<Ctbbemveicapolice> ctbbemveicapolice;
    @OneToMany(mappedBy="prodapolice")
    private Set<Folconvenio> folconvenio;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODAGAGENTE", nullable=false)
    private Agagente agagente;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCTBBEMTIPO", nullable=false)
    private Ctbbemtipo ctbbemtipo;
    @ManyToOne
    @JoinColumn(name="CODAGCORRETOR")
    private Agagente agagente2;
    @OneToMany(mappedBy="prodapolice")
    private Set<Prodapolicecobertura> prodapolicecobertura;

    /** Default constructor. */
    public Prodapolice() {
        super();
    }

    /**
     * Access method for codprodapolice.
     *
     * @return the current value of codprodapolice
     */
    public int getCodprodapolice() {
        return codprodapolice;
    }

    /**
     * Setter method for codprodapolice.
     *
     * @param aCodprodapolice the new value for codprodapolice
     */
    public void setCodprodapolice(int aCodprodapolice) {
        codprodapolice = aCodprodapolice;
    }

    /**
     * Access method for descprodapolice.
     *
     * @return the current value of descprodapolice
     */
    public String getDescprodapolice() {
        return descprodapolice;
    }

    /**
     * Setter method for descprodapolice.
     *
     * @param aDescprodapolice the new value for descprodapolice
     */
    public void setDescprodapolice(String aDescprodapolice) {
        descprodapolice = aDescprodapolice;
    }

    /**
     * Access method for desccompleta.
     *
     * @return the current value of desccompleta
     */
    public String getDesccompleta() {
        return desccompleta;
    }

    /**
     * Setter method for desccompleta.
     *
     * @param aDesccompleta the new value for desccompleta
     */
    public void setDesccompleta(String aDesccompleta) {
        desccompleta = aDesccompleta;
    }

    /**
     * Access method for datavalidade.
     *
     * @return the current value of datavalidade
     */
    public Timestamp getDatavalidade() {
        return datavalidade;
    }

    /**
     * Setter method for datavalidade.
     *
     * @param aDatavalidade the new value for datavalidade
     */
    public void setDatavalidade(Timestamp aDatavalidade) {
        datavalidade = aDatavalidade;
    }

    /**
     * Access method for data.
     *
     * @return the current value of data
     */
    public Timestamp getData() {
        return data;
    }

    /**
     * Setter method for data.
     *
     * @param aData the new value for data
     */
    public void setData(Timestamp aData) {
        data = aData;
    }

    /**
     * Access method for valorpremio.
     *
     * @return the current value of valorpremio
     */
    public BigDecimal getValorpremio() {
        return valorpremio;
    }

    /**
     * Setter method for valorpremio.
     *
     * @param aValorpremio the new value for valorpremio
     */
    public void setValorpremio(BigDecimal aValorpremio) {
        valorpremio = aValorpremio;
    }

    /**
     * Access method for valorfranquia.
     *
     * @return the current value of valorfranquia
     */
    public BigDecimal getValorfranquia() {
        return valorfranquia;
    }

    /**
     * Setter method for valorfranquia.
     *
     * @param aValorfranquia the new value for valorfranquia
     */
    public void setValorfranquia(BigDecimal aValorfranquia) {
        valorfranquia = aValorfranquia;
    }

    /**
     * Access method for valorsegurado.
     *
     * @return the current value of valorsegurado
     */
    public BigDecimal getValorsegurado() {
        return valorsegurado;
    }

    /**
     * Setter method for valorsegurado.
     *
     * @param aValorsegurado the new value for valorsegurado
     */
    public void setValorsegurado(BigDecimal aValorsegurado) {
        valorsegurado = aValorsegurado;
    }

    /**
     * Access method for ativo.
     *
     * @return the current value of ativo
     */
    public short getAtivo() {
        return ativo;
    }

    /**
     * Setter method for ativo.
     *
     * @param aAtivo the new value for ativo
     */
    public void setAtivo(short aAtivo) {
        ativo = aAtivo;
    }

    /**
     * Access method for numprodapolice.
     *
     * @return the current value of numprodapolice
     */
    public String getNumprodapolice() {
        return numprodapolice;
    }

    /**
     * Setter method for numprodapolice.
     *
     * @param aNumprodapolice the new value for numprodapolice
     */
    public void setNumprodapolice(String aNumprodapolice) {
        numprodapolice = aNumprodapolice;
    }

    /**
     * Access method for ctbbem.
     *
     * @return the current value of ctbbem
     */
    public Set<Ctbbem> getCtbbem() {
        return ctbbem;
    }

    /**
     * Setter method for ctbbem.
     *
     * @param aCtbbem the new value for ctbbem
     */
    public void setCtbbem(Set<Ctbbem> aCtbbem) {
        ctbbem = aCtbbem;
    }

    /**
     * Access method for ctbbemimoapolice.
     *
     * @return the current value of ctbbemimoapolice
     */
    public Set<Ctbbemimoapolice> getCtbbemimoapolice() {
        return ctbbemimoapolice;
    }

    /**
     * Setter method for ctbbemimoapolice.
     *
     * @param aCtbbemimoapolice the new value for ctbbemimoapolice
     */
    public void setCtbbemimoapolice(Set<Ctbbemimoapolice> aCtbbemimoapolice) {
        ctbbemimoapolice = aCtbbemimoapolice;
    }

    /**
     * Access method for ctbbemveicapolice.
     *
     * @return the current value of ctbbemveicapolice
     */
    public Set<Ctbbemveicapolice> getCtbbemveicapolice() {
        return ctbbemveicapolice;
    }

    /**
     * Setter method for ctbbemveicapolice.
     *
     * @param aCtbbemveicapolice the new value for ctbbemveicapolice
     */
    public void setCtbbemveicapolice(Set<Ctbbemveicapolice> aCtbbemveicapolice) {
        ctbbemveicapolice = aCtbbemveicapolice;
    }

    /**
     * Access method for folconvenio.
     *
     * @return the current value of folconvenio
     */
    public Set<Folconvenio> getFolconvenio() {
        return folconvenio;
    }

    /**
     * Setter method for folconvenio.
     *
     * @param aFolconvenio the new value for folconvenio
     */
    public void setFolconvenio(Set<Folconvenio> aFolconvenio) {
        folconvenio = aFolconvenio;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for ctbbemtipo.
     *
     * @return the current value of ctbbemtipo
     */
    public Ctbbemtipo getCtbbemtipo() {
        return ctbbemtipo;
    }

    /**
     * Setter method for ctbbemtipo.
     *
     * @param aCtbbemtipo the new value for ctbbemtipo
     */
    public void setCtbbemtipo(Ctbbemtipo aCtbbemtipo) {
        ctbbemtipo = aCtbbemtipo;
    }

    /**
     * Access method for agagente2.
     *
     * @return the current value of agagente2
     */
    public Agagente getAgagente2() {
        return agagente2;
    }

    /**
     * Setter method for agagente2.
     *
     * @param aAgagente2 the new value for agagente2
     */
    public void setAgagente2(Agagente aAgagente2) {
        agagente2 = aAgagente2;
    }

    /**
     * Access method for prodapolicecobertura.
     *
     * @return the current value of prodapolicecobertura
     */
    public Set<Prodapolicecobertura> getProdapolicecobertura() {
        return prodapolicecobertura;
    }

    /**
     * Setter method for prodapolicecobertura.
     *
     * @param aProdapolicecobertura the new value for prodapolicecobertura
     */
    public void setProdapolicecobertura(Set<Prodapolicecobertura> aProdapolicecobertura) {
        prodapolicecobertura = aProdapolicecobertura;
    }

    /**
     * Compares the key for this instance with another Prodapolice.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodapolice and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodapolice)) {
            return false;
        }
        Prodapolice that = (Prodapolice) other;
        if (this.getCodprodapolice() != that.getCodprodapolice()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodapolice.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodapolice)) return false;
        return this.equalKeys(other) && ((Prodapolice)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodapolice();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodapolice |");
        sb.append(" codprodapolice=").append(getCodprodapolice());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodapolice", Integer.valueOf(getCodprodapolice()));
        return ret;
    }

}
