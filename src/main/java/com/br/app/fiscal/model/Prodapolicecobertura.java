package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PRODAPOLICECOBERTURA")
public class Prodapolicecobertura implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodapolicecobertura";

    @Id
    @Column(name="CODPRODAPOLICECOBERTURA", unique=true, nullable=false, precision=10)
    private int codprodapolicecobertura;
    @Column(name="DESCPRODAPOLICECOBERTURA", nullable=false, length=250)
    private String descprodapolicecobertura;
    @Column(name="VALORPREMIO", precision=15, scale=4)
    private BigDecimal valorpremio;
    @Column(name="VALORFRANQUIA", precision=15, scale=4)
    private BigDecimal valorfranquia;
    @Column(name="VALORSEGURADO", precision=15, scale=4)
    private BigDecimal valorsegurado;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODAPOLICE", nullable=false)
    private Prodapolice prodapolice;

    /** Default constructor. */
    public Prodapolicecobertura() {
        super();
    }

    /**
     * Access method for codprodapolicecobertura.
     *
     * @return the current value of codprodapolicecobertura
     */
    public int getCodprodapolicecobertura() {
        return codprodapolicecobertura;
    }

    /**
     * Setter method for codprodapolicecobertura.
     *
     * @param aCodprodapolicecobertura the new value for codprodapolicecobertura
     */
    public void setCodprodapolicecobertura(int aCodprodapolicecobertura) {
        codprodapolicecobertura = aCodprodapolicecobertura;
    }

    /**
     * Access method for descprodapolicecobertura.
     *
     * @return the current value of descprodapolicecobertura
     */
    public String getDescprodapolicecobertura() {
        return descprodapolicecobertura;
    }

    /**
     * Setter method for descprodapolicecobertura.
     *
     * @param aDescprodapolicecobertura the new value for descprodapolicecobertura
     */
    public void setDescprodapolicecobertura(String aDescprodapolicecobertura) {
        descprodapolicecobertura = aDescprodapolicecobertura;
    }

    /**
     * Access method for valorpremio.
     *
     * @return the current value of valorpremio
     */
    public BigDecimal getValorpremio() {
        return valorpremio;
    }

    /**
     * Setter method for valorpremio.
     *
     * @param aValorpremio the new value for valorpremio
     */
    public void setValorpremio(BigDecimal aValorpremio) {
        valorpremio = aValorpremio;
    }

    /**
     * Access method for valorfranquia.
     *
     * @return the current value of valorfranquia
     */
    public BigDecimal getValorfranquia() {
        return valorfranquia;
    }

    /**
     * Setter method for valorfranquia.
     *
     * @param aValorfranquia the new value for valorfranquia
     */
    public void setValorfranquia(BigDecimal aValorfranquia) {
        valorfranquia = aValorfranquia;
    }

    /**
     * Access method for valorsegurado.
     *
     * @return the current value of valorsegurado
     */
    public BigDecimal getValorsegurado() {
        return valorsegurado;
    }

    /**
     * Setter method for valorsegurado.
     *
     * @param aValorsegurado the new value for valorsegurado
     */
    public void setValorsegurado(BigDecimal aValorsegurado) {
        valorsegurado = aValorsegurado;
    }

    /**
     * Access method for prodapolice.
     *
     * @return the current value of prodapolice
     */
    public Prodapolice getProdapolice() {
        return prodapolice;
    }

    /**
     * Setter method for prodapolice.
     *
     * @param aProdapolice the new value for prodapolice
     */
    public void setProdapolice(Prodapolice aProdapolice) {
        prodapolice = aProdapolice;
    }

    /**
     * Compares the key for this instance with another Prodapolicecobertura.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodapolicecobertura and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodapolicecobertura)) {
            return false;
        }
        Prodapolicecobertura that = (Prodapolicecobertura) other;
        if (this.getCodprodapolicecobertura() != that.getCodprodapolicecobertura()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodapolicecobertura.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodapolicecobertura)) return false;
        return this.equalKeys(other) && ((Prodapolicecobertura)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodapolicecobertura();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodapolicecobertura |");
        sb.append(" codprodapolicecobertura=").append(getCodprodapolicecobertura());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodapolicecobertura", Integer.valueOf(getCodprodapolicecobertura()));
        return ret;
    }

}
