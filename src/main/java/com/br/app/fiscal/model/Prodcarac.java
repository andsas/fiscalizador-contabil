package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRODCARAC")
public class Prodcarac implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodcarac";

    @Id
    @Column(name="CODPRODCARAC", unique=true, nullable=false, precision=10)
    private int codprodcarac;
    @Column(name="DESCPRODCARAC", nullable=false, length=250)
    private String descprodcarac;
    @Column(name="TIPOPRODCARAC", precision=5)
    private short tipoprodcarac;
    @Column(name="TIPOPREENCHIMENTO", precision=5)
    private short tipopreenchimento;
    @OneToMany(mappedBy="prodcarac")
    private Set<Optransacaodetprodcarac> optransacaodetprodcarac;
    @OneToMany(mappedBy="prodcarac")
    private Set<Prodmodelocarac> prodmodelocarac;

    /** Default constructor. */
    public Prodcarac() {
        super();
    }

    /**
     * Access method for codprodcarac.
     *
     * @return the current value of codprodcarac
     */
    public int getCodprodcarac() {
        return codprodcarac;
    }

    /**
     * Setter method for codprodcarac.
     *
     * @param aCodprodcarac the new value for codprodcarac
     */
    public void setCodprodcarac(int aCodprodcarac) {
        codprodcarac = aCodprodcarac;
    }

    /**
     * Access method for descprodcarac.
     *
     * @return the current value of descprodcarac
     */
    public String getDescprodcarac() {
        return descprodcarac;
    }

    /**
     * Setter method for descprodcarac.
     *
     * @param aDescprodcarac the new value for descprodcarac
     */
    public void setDescprodcarac(String aDescprodcarac) {
        descprodcarac = aDescprodcarac;
    }

    /**
     * Access method for tipoprodcarac.
     *
     * @return the current value of tipoprodcarac
     */
    public short getTipoprodcarac() {
        return tipoprodcarac;
    }

    /**
     * Setter method for tipoprodcarac.
     *
     * @param aTipoprodcarac the new value for tipoprodcarac
     */
    public void setTipoprodcarac(short aTipoprodcarac) {
        tipoprodcarac = aTipoprodcarac;
    }

    /**
     * Access method for tipopreenchimento.
     *
     * @return the current value of tipopreenchimento
     */
    public short getTipopreenchimento() {
        return tipopreenchimento;
    }

    /**
     * Setter method for tipopreenchimento.
     *
     * @param aTipopreenchimento the new value for tipopreenchimento
     */
    public void setTipopreenchimento(short aTipopreenchimento) {
        tipopreenchimento = aTipopreenchimento;
    }

    /**
     * Access method for optransacaodetprodcarac.
     *
     * @return the current value of optransacaodetprodcarac
     */
    public Set<Optransacaodetprodcarac> getOptransacaodetprodcarac() {
        return optransacaodetprodcarac;
    }

    /**
     * Setter method for optransacaodetprodcarac.
     *
     * @param aOptransacaodetprodcarac the new value for optransacaodetprodcarac
     */
    public void setOptransacaodetprodcarac(Set<Optransacaodetprodcarac> aOptransacaodetprodcarac) {
        optransacaodetprodcarac = aOptransacaodetprodcarac;
    }

    /**
     * Access method for prodmodelocarac.
     *
     * @return the current value of prodmodelocarac
     */
    public Set<Prodmodelocarac> getProdmodelocarac() {
        return prodmodelocarac;
    }

    /**
     * Setter method for prodmodelocarac.
     *
     * @param aProdmodelocarac the new value for prodmodelocarac
     */
    public void setProdmodelocarac(Set<Prodmodelocarac> aProdmodelocarac) {
        prodmodelocarac = aProdmodelocarac;
    }

    /**
     * Compares the key for this instance with another Prodcarac.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodcarac and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodcarac)) {
            return false;
        }
        Prodcarac that = (Prodcarac) other;
        if (this.getCodprodcarac() != that.getCodprodcarac()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodcarac.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodcarac)) return false;
        return this.equalKeys(other) && ((Prodcarac)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodcarac();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodcarac |");
        sb.append(" codprodcarac=").append(getCodprodcarac());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodcarac", Integer.valueOf(getCodprodcarac()));
        return ret;
    }

}
