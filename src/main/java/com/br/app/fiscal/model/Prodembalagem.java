package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRODEMBALAGEM")
public class Prodembalagem implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodembalagem";

    @Id
    @Column(name="CODPRODEMBALAGEM", unique=true, nullable=false, length=20)
    private String codprodembalagem;
    @Column(name="DESCPRODEMBALAGEM", nullable=false, length=250)
    private String descprodembalagem;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODUNIDADEEQUIV", nullable=false)
    private Produnidadeequiv produnidadeequiv;
    @OneToMany(mappedBy="prodembalagem")
    private Set<Prodembprod> prodembprod;

    /** Default constructor. */
    public Prodembalagem() {
        super();
    }

    /**
     * Access method for codprodembalagem.
     *
     * @return the current value of codprodembalagem
     */
    public String getCodprodembalagem() {
        return codprodembalagem;
    }

    /**
     * Setter method for codprodembalagem.
     *
     * @param aCodprodembalagem the new value for codprodembalagem
     */
    public void setCodprodembalagem(String aCodprodembalagem) {
        codprodembalagem = aCodprodembalagem;
    }

    /**
     * Access method for descprodembalagem.
     *
     * @return the current value of descprodembalagem
     */
    public String getDescprodembalagem() {
        return descprodembalagem;
    }

    /**
     * Setter method for descprodembalagem.
     *
     * @param aDescprodembalagem the new value for descprodembalagem
     */
    public void setDescprodembalagem(String aDescprodembalagem) {
        descprodembalagem = aDescprodembalagem;
    }

    /**
     * Access method for produnidadeequiv.
     *
     * @return the current value of produnidadeequiv
     */
    public Produnidadeequiv getProdunidadeequiv() {
        return produnidadeequiv;
    }

    /**
     * Setter method for produnidadeequiv.
     *
     * @param aProdunidadeequiv the new value for produnidadeequiv
     */
    public void setProdunidadeequiv(Produnidadeequiv aProdunidadeequiv) {
        produnidadeequiv = aProdunidadeequiv;
    }

    /**
     * Access method for prodembprod.
     *
     * @return the current value of prodembprod
     */
    public Set<Prodembprod> getProdembprod() {
        return prodembprod;
    }

    /**
     * Setter method for prodembprod.
     *
     * @param aProdembprod the new value for prodembprod
     */
    public void setProdembprod(Set<Prodembprod> aProdembprod) {
        prodembprod = aProdembprod;
    }

    /**
     * Compares the key for this instance with another Prodembalagem.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodembalagem and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodembalagem)) {
            return false;
        }
        Prodembalagem that = (Prodembalagem) other;
        Object myCodprodembalagem = this.getCodprodembalagem();
        Object yourCodprodembalagem = that.getCodprodembalagem();
        if (myCodprodembalagem==null ? yourCodprodembalagem!=null : !myCodprodembalagem.equals(yourCodprodembalagem)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodembalagem.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodembalagem)) return false;
        return this.equalKeys(other) && ((Prodembalagem)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodprodembalagem() == null) {
            i = 0;
        } else {
            i = getCodprodembalagem().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodembalagem |");
        sb.append(" codprodembalagem=").append(getCodprodembalagem());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodembalagem", getCodprodembalagem());
        return ret;
    }

}
