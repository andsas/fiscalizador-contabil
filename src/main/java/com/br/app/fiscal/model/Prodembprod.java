package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="PRODEMBPROD", indexes={@Index(name="prodembprodProdembprodIdx1", columnList="CODPRODPRODUTO,CODPRODEMBALAGEM", unique=true)})
public class Prodembprod implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodembprod";

    @Id
    @Column(name="CODPRODEMBPROD", unique=true, nullable=false, precision=10)
    private int codprodembprod;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODEMBALAGEM", nullable=false)
    private Prodembalagem prodembalagem;

    /** Default constructor. */
    public Prodembprod() {
        super();
    }

    /**
     * Access method for codprodembprod.
     *
     * @return the current value of codprodembprod
     */
    public int getCodprodembprod() {
        return codprodembprod;
    }

    /**
     * Setter method for codprodembprod.
     *
     * @param aCodprodembprod the new value for codprodembprod
     */
    public void setCodprodembprod(int aCodprodembprod) {
        codprodembprod = aCodprodembprod;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for prodembalagem.
     *
     * @return the current value of prodembalagem
     */
    public Prodembalagem getProdembalagem() {
        return prodembalagem;
    }

    /**
     * Setter method for prodembalagem.
     *
     * @param aProdembalagem the new value for prodembalagem
     */
    public void setProdembalagem(Prodembalagem aProdembalagem) {
        prodembalagem = aProdembalagem;
    }

    /**
     * Gets the group fragment codprodproduto for member prodproduto.
     *
     * @return Current value of the group fragment
     * @see Prodproduto
     */
    public int getProdprodutoCodprodproduto() {
        return (getProdproduto() == null ? null : getProdproduto().getCodprodproduto());
    }

    /**
     * Sets the group fragment codprodproduto from member prodproduto.
     *
     * @param aCodprodproduto New value for the group fragment
     * @see Prodproduto
     */
    public void setProdprodutoCodprodproduto(int aCodprodproduto) {
        if (getProdproduto() != null) {
            getProdproduto().setCodprodproduto(aCodprodproduto);
        }
    }

    /**
     * Gets the group fragment codprodembalagem for member prodembalagem.
     *
     * @return Current value of the group fragment
     * @see Prodembalagem
     */
    public String getProdembalagemCodprodembalagem() {
        return (getProdembalagem() == null ? null : getProdembalagem().getCodprodembalagem());
    }

    /**
     * Sets the group fragment codprodembalagem from member prodembalagem.
     *
     * @param aCodprodembalagem New value for the group fragment
     * @see Prodembalagem
     */
    public void setProdembalagemCodprodembalagem(String aCodprodembalagem) {
        if (getProdembalagem() != null) {
            getProdembalagem().setCodprodembalagem(aCodprodembalagem);
        }
    }

    /**
     * Compares the key for this instance with another Prodembprod.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodembprod and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodembprod)) {
            return false;
        }
        Prodembprod that = (Prodembprod) other;
        if (this.getCodprodembprod() != that.getCodprodembprod()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodembprod.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodembprod)) return false;
        return this.equalKeys(other) && ((Prodembprod)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodembprod();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodembprod |");
        sb.append(" codprodembprod=").append(getCodprodembprod());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodembprod", Integer.valueOf(getCodprodembprod()));
        return ret;
    }

}
