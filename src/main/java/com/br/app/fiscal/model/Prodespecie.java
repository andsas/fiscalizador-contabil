package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRODESPECIE")
public class Prodespecie implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodespecie";

    @Id
    @Column(name="CODPRODESPECIE", unique=true, nullable=false, precision=10)
    private int codprodespecie;
    @Column(name="DESCPRODESPECIE", nullable=false, length=250)
    private String descprodespecie;
    @OneToMany(mappedBy="prodespecie")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="prodespecie")
    private Set<Prodproduto> prodproduto;

    /** Default constructor. */
    public Prodespecie() {
        super();
    }

    /**
     * Access method for codprodespecie.
     *
     * @return the current value of codprodespecie
     */
    public int getCodprodespecie() {
        return codprodespecie;
    }

    /**
     * Setter method for codprodespecie.
     *
     * @param aCodprodespecie the new value for codprodespecie
     */
    public void setCodprodespecie(int aCodprodespecie) {
        codprodespecie = aCodprodespecie;
    }

    /**
     * Access method for descprodespecie.
     *
     * @return the current value of descprodespecie
     */
    public String getDescprodespecie() {
        return descprodespecie;
    }

    /**
     * Setter method for descprodespecie.
     *
     * @param aDescprodespecie the new value for descprodespecie
     */
    public void setDescprodespecie(String aDescprodespecie) {
        descprodespecie = aDescprodespecie;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Prodespecie.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodespecie and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodespecie)) {
            return false;
        }
        Prodespecie that = (Prodespecie) other;
        if (this.getCodprodespecie() != that.getCodprodespecie()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodespecie.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodespecie)) return false;
        return this.equalKeys(other) && ((Prodespecie)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodespecie();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodespecie |");
        sb.append(" codprodespecie=").append(getCodprodespecie());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodespecie", Integer.valueOf(getCodprodespecie()));
        return ret;
    }

}
