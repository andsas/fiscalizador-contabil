package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="PRODGRADE")
public class Prodgrade implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodgrade";

    @Id
    @Column(name="CODPRODGRADE", unique=true, nullable=false, length=20)
    private String codprodgrade;
    @Column(name="DESCPRODGRADE", nullable=false, length=250)
    private String descprodgrade;
    @Column(name="CODPRODPARENTGRADE", length=20)
    private String codprodparentgrade;
    @Column(name="NIVEL", precision=5)
    private short nivel;

    /** Default constructor. */
    public Prodgrade() {
        super();
    }

    /**
     * Access method for codprodgrade.
     *
     * @return the current value of codprodgrade
     */
    public String getCodprodgrade() {
        return codprodgrade;
    }

    /**
     * Setter method for codprodgrade.
     *
     * @param aCodprodgrade the new value for codprodgrade
     */
    public void setCodprodgrade(String aCodprodgrade) {
        codprodgrade = aCodprodgrade;
    }

    /**
     * Access method for descprodgrade.
     *
     * @return the current value of descprodgrade
     */
    public String getDescprodgrade() {
        return descprodgrade;
    }

    /**
     * Setter method for descprodgrade.
     *
     * @param aDescprodgrade the new value for descprodgrade
     */
    public void setDescprodgrade(String aDescprodgrade) {
        descprodgrade = aDescprodgrade;
    }

    /**
     * Access method for codprodparentgrade.
     *
     * @return the current value of codprodparentgrade
     */
    public String getCodprodparentgrade() {
        return codprodparentgrade;
    }

    /**
     * Setter method for codprodparentgrade.
     *
     * @param aCodprodparentgrade the new value for codprodparentgrade
     */
    public void setCodprodparentgrade(String aCodprodparentgrade) {
        codprodparentgrade = aCodprodparentgrade;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Compares the key for this instance with another Prodgrade.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodgrade and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodgrade)) {
            return false;
        }
        Prodgrade that = (Prodgrade) other;
        Object myCodprodgrade = this.getCodprodgrade();
        Object yourCodprodgrade = that.getCodprodgrade();
        if (myCodprodgrade==null ? yourCodprodgrade!=null : !myCodprodgrade.equals(yourCodprodgrade)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodgrade.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodgrade)) return false;
        return this.equalKeys(other) && ((Prodgrade)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodprodgrade() == null) {
            i = 0;
        } else {
            i = getCodprodgrade().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodgrade |");
        sb.append(" codprodgrade=").append(getCodprodgrade());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodgrade", getCodprodgrade());
        return ret;
    }

}
