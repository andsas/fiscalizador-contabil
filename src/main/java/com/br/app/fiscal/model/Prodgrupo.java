package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRODGRUPO")
public class Prodgrupo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodgrupo";

    @Id
    @Column(name="CODPRODGRUPO", unique=true, nullable=false, length=20)
    private String codprodgrupo;
    @Column(name="DESCPRODGRUPO", nullable=false, length=250)
    private String descprodgrupo;
    @Column(name="CODPRODPARENTGRUPO", length=20)
    private String codprodparentgrupo;
    @Column(name="NIVEL", precision=5)
    private short nivel;
    @OneToMany(mappedBy="prodgrupo")
    private Set<Impplanofiscal> impplanofiscal;
    @ManyToOne
    @JoinColumn(name="CODCTBBEMTIPO")
    private Ctbbemtipo ctbbemtipo;
    @OneToMany(mappedBy="prodgrupo")
    private Set<Prodproduto> prodproduto;

    /** Default constructor. */
    public Prodgrupo() {
        super();
    }

    /**
     * Access method for codprodgrupo.
     *
     * @return the current value of codprodgrupo
     */
    public String getCodprodgrupo() {
        return codprodgrupo;
    }

    /**
     * Setter method for codprodgrupo.
     *
     * @param aCodprodgrupo the new value for codprodgrupo
     */
    public void setCodprodgrupo(String aCodprodgrupo) {
        codprodgrupo = aCodprodgrupo;
    }

    /**
     * Access method for descprodgrupo.
     *
     * @return the current value of descprodgrupo
     */
    public String getDescprodgrupo() {
        return descprodgrupo;
    }

    /**
     * Setter method for descprodgrupo.
     *
     * @param aDescprodgrupo the new value for descprodgrupo
     */
    public void setDescprodgrupo(String aDescprodgrupo) {
        descprodgrupo = aDescprodgrupo;
    }

    /**
     * Access method for codprodparentgrupo.
     *
     * @return the current value of codprodparentgrupo
     */
    public String getCodprodparentgrupo() {
        return codprodparentgrupo;
    }

    /**
     * Setter method for codprodparentgrupo.
     *
     * @param aCodprodparentgrupo the new value for codprodparentgrupo
     */
    public void setCodprodparentgrupo(String aCodprodparentgrupo) {
        codprodparentgrupo = aCodprodparentgrupo;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for ctbbemtipo.
     *
     * @return the current value of ctbbemtipo
     */
    public Ctbbemtipo getCtbbemtipo() {
        return ctbbemtipo;
    }

    /**
     * Setter method for ctbbemtipo.
     *
     * @param aCtbbemtipo the new value for ctbbemtipo
     */
    public void setCtbbemtipo(Ctbbemtipo aCtbbemtipo) {
        ctbbemtipo = aCtbbemtipo;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Prodgrupo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodgrupo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodgrupo)) {
            return false;
        }
        Prodgrupo that = (Prodgrupo) other;
        Object myCodprodgrupo = this.getCodprodgrupo();
        Object yourCodprodgrupo = that.getCodprodgrupo();
        if (myCodprodgrupo==null ? yourCodprodgrupo!=null : !myCodprodgrupo.equals(yourCodprodgrupo)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodgrupo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodgrupo)) return false;
        return this.equalKeys(other) && ((Prodgrupo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodprodgrupo() == null) {
            i = 0;
        } else {
            i = getCodprodgrupo().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodgrupo |");
        sb.append(" codprodgrupo=").append(getCodprodgrupo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodgrupo", getCodprodgrupo());
        return ret;
    }

}
