package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRODLOCAL")
public class Prodlocal implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodlocal";

    @Id
    @Column(name="CODPRODLOCAL", unique=true, nullable=false, length=20)
    private String codprodlocal;
    @Column(name="DESCPRODLOCAL", nullable=false, length=250)
    private String descprodlocal;
    @Column(name="CODPRODPARENTLOCAL", length=20)
    private String codprodparentlocal;
    @Column(name="NIVEL", precision=5)
    private short nivel;
    @OneToMany(mappedBy="prodlocal")
    private Set<Prodprodutolocal> prodprodutolocal;

    /** Default constructor. */
    public Prodlocal() {
        super();
    }

    /**
     * Access method for codprodlocal.
     *
     * @return the current value of codprodlocal
     */
    public String getCodprodlocal() {
        return codprodlocal;
    }

    /**
     * Setter method for codprodlocal.
     *
     * @param aCodprodlocal the new value for codprodlocal
     */
    public void setCodprodlocal(String aCodprodlocal) {
        codprodlocal = aCodprodlocal;
    }

    /**
     * Access method for descprodlocal.
     *
     * @return the current value of descprodlocal
     */
    public String getDescprodlocal() {
        return descprodlocal;
    }

    /**
     * Setter method for descprodlocal.
     *
     * @param aDescprodlocal the new value for descprodlocal
     */
    public void setDescprodlocal(String aDescprodlocal) {
        descprodlocal = aDescprodlocal;
    }

    /**
     * Access method for codprodparentlocal.
     *
     * @return the current value of codprodparentlocal
     */
    public String getCodprodparentlocal() {
        return codprodparentlocal;
    }

    /**
     * Setter method for codprodparentlocal.
     *
     * @param aCodprodparentlocal the new value for codprodparentlocal
     */
    public void setCodprodparentlocal(String aCodprodparentlocal) {
        codprodparentlocal = aCodprodparentlocal;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for prodprodutolocal.
     *
     * @return the current value of prodprodutolocal
     */
    public Set<Prodprodutolocal> getProdprodutolocal() {
        return prodprodutolocal;
    }

    /**
     * Setter method for prodprodutolocal.
     *
     * @param aProdprodutolocal the new value for prodprodutolocal
     */
    public void setProdprodutolocal(Set<Prodprodutolocal> aProdprodutolocal) {
        prodprodutolocal = aProdprodutolocal;
    }

    /**
     * Compares the key for this instance with another Prodlocal.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodlocal and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodlocal)) {
            return false;
        }
        Prodlocal that = (Prodlocal) other;
        Object myCodprodlocal = this.getCodprodlocal();
        Object yourCodprodlocal = that.getCodprodlocal();
        if (myCodprodlocal==null ? yourCodprodlocal!=null : !myCodprodlocal.equals(yourCodprodlocal)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodlocal.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodlocal)) return false;
        return this.equalKeys(other) && ((Prodlocal)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodprodlocal() == null) {
            i = 0;
        } else {
            i = getCodprodlocal().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodlocal |");
        sb.append(" codprodlocal=").append(getCodprodlocal());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodlocal", getCodprodlocal());
        return ret;
    }

}
