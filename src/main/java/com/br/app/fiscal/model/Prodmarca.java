package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRODMARCA")
public class Prodmarca implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodmarca";

    @Id
    @Column(name="CODPRODMARCA", unique=true, nullable=false, precision=10)
    private int codprodmarca;
    @Column(name="DESCPRODMARCA", nullable=false, length=250)
    private String descprodmarca;
    @OneToMany(mappedBy="prodmarca")
    private Set<Ctbbemveictacografo> ctbbemveictacografo;
    @OneToMany(mappedBy="prodmarca")
    private Set<Geroutro> geroutro;
    @OneToMany(mappedBy="prodmarca")
    private Set<Impplanofiscal> impplanofiscal;
    @ManyToOne
    @JoinColumn(name="CODAGAGENTE")
    private Agagente agagente;
    @OneToMany(mappedBy="prodmarca")
    private Set<Prodmodelo> prodmodelo;
    @OneToMany(mappedBy="prodmarca")
    private Set<Prodproduto> prodproduto;

    /** Default constructor. */
    public Prodmarca() {
        super();
    }

    /**
     * Access method for codprodmarca.
     *
     * @return the current value of codprodmarca
     */
    public int getCodprodmarca() {
        return codprodmarca;
    }

    /**
     * Setter method for codprodmarca.
     *
     * @param aCodprodmarca the new value for codprodmarca
     */
    public void setCodprodmarca(int aCodprodmarca) {
        codprodmarca = aCodprodmarca;
    }

    /**
     * Access method for descprodmarca.
     *
     * @return the current value of descprodmarca
     */
    public String getDescprodmarca() {
        return descprodmarca;
    }

    /**
     * Setter method for descprodmarca.
     *
     * @param aDescprodmarca the new value for descprodmarca
     */
    public void setDescprodmarca(String aDescprodmarca) {
        descprodmarca = aDescprodmarca;
    }

    /**
     * Access method for ctbbemveictacografo.
     *
     * @return the current value of ctbbemveictacografo
     */
    public Set<Ctbbemveictacografo> getCtbbemveictacografo() {
        return ctbbemveictacografo;
    }

    /**
     * Setter method for ctbbemveictacografo.
     *
     * @param aCtbbemveictacografo the new value for ctbbemveictacografo
     */
    public void setCtbbemveictacografo(Set<Ctbbemveictacografo> aCtbbemveictacografo) {
        ctbbemveictacografo = aCtbbemveictacografo;
    }

    /**
     * Access method for geroutro.
     *
     * @return the current value of geroutro
     */
    public Set<Geroutro> getGeroutro() {
        return geroutro;
    }

    /**
     * Setter method for geroutro.
     *
     * @param aGeroutro the new value for geroutro
     */
    public void setGeroutro(Set<Geroutro> aGeroutro) {
        geroutro = aGeroutro;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Agagente getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Agagente aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Set<Prodmodelo> getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Set<Prodmodelo> aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Prodmarca.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodmarca and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodmarca)) {
            return false;
        }
        Prodmarca that = (Prodmarca) other;
        if (this.getCodprodmarca() != that.getCodprodmarca()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodmarca.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodmarca)) return false;
        return this.equalKeys(other) && ((Prodmarca)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodmarca();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodmarca |");
        sb.append(" codprodmarca=").append(getCodprodmarca());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodmarca", Integer.valueOf(getCodprodmarca()));
        return ret;
    }

}
