package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PRODMINMAX")
public class Prodminmax implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodminmax";

    @Id
    @Column(name="CODPRODMINMAX", unique=true, nullable=false, precision=10)
    private int codprodminmax;
    @Column(name="PRODMIN", nullable=false, precision=15, scale=4)
    private BigDecimal prodmin;
    @Column(name="PRODMAX", nullable=false, precision=15, scale=4)
    private BigDecimal prodmax;
    @Column(name="OPCAOMIN", nullable=false, precision=5)
    private short opcaomin;
    @Column(name="OPCAOMAX", nullable=false, precision=5)
    private short opcaomax;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODINVSALDO", nullable=false)
    private Invsaldo invsaldo;

    /** Default constructor. */
    public Prodminmax() {
        super();
    }

    /**
     * Access method for codprodminmax.
     *
     * @return the current value of codprodminmax
     */
    public int getCodprodminmax() {
        return codprodminmax;
    }

    /**
     * Setter method for codprodminmax.
     *
     * @param aCodprodminmax the new value for codprodminmax
     */
    public void setCodprodminmax(int aCodprodminmax) {
        codprodminmax = aCodprodminmax;
    }

    /**
     * Access method for prodmin.
     *
     * @return the current value of prodmin
     */
    public BigDecimal getProdmin() {
        return prodmin;
    }

    /**
     * Setter method for prodmin.
     *
     * @param aProdmin the new value for prodmin
     */
    public void setProdmin(BigDecimal aProdmin) {
        prodmin = aProdmin;
    }

    /**
     * Access method for prodmax.
     *
     * @return the current value of prodmax
     */
    public BigDecimal getProdmax() {
        return prodmax;
    }

    /**
     * Setter method for prodmax.
     *
     * @param aProdmax the new value for prodmax
     */
    public void setProdmax(BigDecimal aProdmax) {
        prodmax = aProdmax;
    }

    /**
     * Access method for opcaomin.
     *
     * @return the current value of opcaomin
     */
    public short getOpcaomin() {
        return opcaomin;
    }

    /**
     * Setter method for opcaomin.
     *
     * @param aOpcaomin the new value for opcaomin
     */
    public void setOpcaomin(short aOpcaomin) {
        opcaomin = aOpcaomin;
    }

    /**
     * Access method for opcaomax.
     *
     * @return the current value of opcaomax
     */
    public short getOpcaomax() {
        return opcaomax;
    }

    /**
     * Setter method for opcaomax.
     *
     * @param aOpcaomax the new value for opcaomax
     */
    public void setOpcaomax(short aOpcaomax) {
        opcaomax = aOpcaomax;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for invsaldo.
     *
     * @return the current value of invsaldo
     */
    public Invsaldo getInvsaldo() {
        return invsaldo;
    }

    /**
     * Setter method for invsaldo.
     *
     * @param aInvsaldo the new value for invsaldo
     */
    public void setInvsaldo(Invsaldo aInvsaldo) {
        invsaldo = aInvsaldo;
    }

    /**
     * Compares the key for this instance with another Prodminmax.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodminmax and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodminmax)) {
            return false;
        }
        Prodminmax that = (Prodminmax) other;
        if (this.getCodprodminmax() != that.getCodprodminmax()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodminmax.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodminmax)) return false;
        return this.equalKeys(other) && ((Prodminmax)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodminmax();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodminmax |");
        sb.append(" codprodminmax=").append(getCodprodminmax());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodminmax", Integer.valueOf(getCodprodminmax()));
        return ret;
    }

}
