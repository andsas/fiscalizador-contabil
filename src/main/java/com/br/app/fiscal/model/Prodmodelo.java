package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRODMODELO")
public class Prodmodelo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodmodelo";

    @Id
    @Column(name="CODPRODMODELO", unique=true, nullable=false, precision=10)
    private int codprodmodelo;
    @Column(name="DESCPRODMODELO", nullable=false, length=250)
    private String descprodmodelo;
    @OneToMany(mappedBy="prodmodelo")
    private Set<Ctbbemveictacografo> ctbbemveictacografo;
    @OneToMany(mappedBy="prodmodelo")
    private Set<Geroutro> geroutro;
    @OneToMany(mappedBy="prodmodelo")
    private Set<Gerveiculo> gerveiculo;
    @OneToMany(mappedBy="prodmodelo")
    private Set<Optransacaodet> optransacaodet;
    @OneToMany(mappedBy="prodmodelo")
    private Set<Parklancamento> parklancamento;
    @OneToMany(mappedBy="prodmodelo")
    private Set<Prdcformula> prdcformula;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODMARCA", nullable=false)
    private Prodmarca prodmarca;
    @OneToMany(mappedBy="prodmodelo")
    private Set<Prodmodelocarac> prodmodelocarac;
    @OneToMany(mappedBy="prodmodelo")
    private Set<Prodmodeloprod> prodmodeloprod;
    @OneToMany(mappedBy="prodmodelo")
    private Set<Prodproduto> prodproduto;

    /** Default constructor. */
    public Prodmodelo() {
        super();
    }

    /**
     * Access method for codprodmodelo.
     *
     * @return the current value of codprodmodelo
     */
    public int getCodprodmodelo() {
        return codprodmodelo;
    }

    /**
     * Setter method for codprodmodelo.
     *
     * @param aCodprodmodelo the new value for codprodmodelo
     */
    public void setCodprodmodelo(int aCodprodmodelo) {
        codprodmodelo = aCodprodmodelo;
    }

    /**
     * Access method for descprodmodelo.
     *
     * @return the current value of descprodmodelo
     */
    public String getDescprodmodelo() {
        return descprodmodelo;
    }

    /**
     * Setter method for descprodmodelo.
     *
     * @param aDescprodmodelo the new value for descprodmodelo
     */
    public void setDescprodmodelo(String aDescprodmodelo) {
        descprodmodelo = aDescprodmodelo;
    }

    /**
     * Access method for ctbbemveictacografo.
     *
     * @return the current value of ctbbemveictacografo
     */
    public Set<Ctbbemveictacografo> getCtbbemveictacografo() {
        return ctbbemveictacografo;
    }

    /**
     * Setter method for ctbbemveictacografo.
     *
     * @param aCtbbemveictacografo the new value for ctbbemveictacografo
     */
    public void setCtbbemveictacografo(Set<Ctbbemveictacografo> aCtbbemveictacografo) {
        ctbbemveictacografo = aCtbbemveictacografo;
    }

    /**
     * Access method for geroutro.
     *
     * @return the current value of geroutro
     */
    public Set<Geroutro> getGeroutro() {
        return geroutro;
    }

    /**
     * Setter method for geroutro.
     *
     * @param aGeroutro the new value for geroutro
     */
    public void setGeroutro(Set<Geroutro> aGeroutro) {
        geroutro = aGeroutro;
    }

    /**
     * Access method for gerveiculo.
     *
     * @return the current value of gerveiculo
     */
    public Set<Gerveiculo> getGerveiculo() {
        return gerveiculo;
    }

    /**
     * Setter method for gerveiculo.
     *
     * @param aGerveiculo the new value for gerveiculo
     */
    public void setGerveiculo(Set<Gerveiculo> aGerveiculo) {
        gerveiculo = aGerveiculo;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Access method for parklancamento.
     *
     * @return the current value of parklancamento
     */
    public Set<Parklancamento> getParklancamento() {
        return parklancamento;
    }

    /**
     * Setter method for parklancamento.
     *
     * @param aParklancamento the new value for parklancamento
     */
    public void setParklancamento(Set<Parklancamento> aParklancamento) {
        parklancamento = aParklancamento;
    }

    /**
     * Access method for prdcformula.
     *
     * @return the current value of prdcformula
     */
    public Set<Prdcformula> getPrdcformula() {
        return prdcformula;
    }

    /**
     * Setter method for prdcformula.
     *
     * @param aPrdcformula the new value for prdcformula
     */
    public void setPrdcformula(Set<Prdcformula> aPrdcformula) {
        prdcformula = aPrdcformula;
    }

    /**
     * Access method for prodmarca.
     *
     * @return the current value of prodmarca
     */
    public Prodmarca getProdmarca() {
        return prodmarca;
    }

    /**
     * Setter method for prodmarca.
     *
     * @param aProdmarca the new value for prodmarca
     */
    public void setProdmarca(Prodmarca aProdmarca) {
        prodmarca = aProdmarca;
    }

    /**
     * Access method for prodmodelocarac.
     *
     * @return the current value of prodmodelocarac
     */
    public Set<Prodmodelocarac> getProdmodelocarac() {
        return prodmodelocarac;
    }

    /**
     * Setter method for prodmodelocarac.
     *
     * @param aProdmodelocarac the new value for prodmodelocarac
     */
    public void setProdmodelocarac(Set<Prodmodelocarac> aProdmodelocarac) {
        prodmodelocarac = aProdmodelocarac;
    }

    /**
     * Access method for prodmodeloprod.
     *
     * @return the current value of prodmodeloprod
     */
    public Set<Prodmodeloprod> getProdmodeloprod() {
        return prodmodeloprod;
    }

    /**
     * Setter method for prodmodeloprod.
     *
     * @param aProdmodeloprod the new value for prodmodeloprod
     */
    public void setProdmodeloprod(Set<Prodmodeloprod> aProdmodeloprod) {
        prodmodeloprod = aProdmodeloprod;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Prodmodelo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodmodelo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodmodelo)) {
            return false;
        }
        Prodmodelo that = (Prodmodelo) other;
        if (this.getCodprodmodelo() != that.getCodprodmodelo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodmodelo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodmodelo)) return false;
        return this.equalKeys(other) && ((Prodmodelo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodmodelo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodmodelo |");
        sb.append(" codprodmodelo=").append(getCodprodmodelo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodmodelo", Integer.valueOf(getCodprodmodelo()));
        return ret;
    }

}
