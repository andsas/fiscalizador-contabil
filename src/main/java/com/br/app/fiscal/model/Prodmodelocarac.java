package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRODMODELOCARAC")
public class Prodmodelocarac implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodmodelocarac";

    @Id
    @Column(name="CODPRODMODELOCARAC", unique=true, nullable=false, precision=10)
    private int codprodmodelocarac;
    @Column(name="VALORMIN", precision=15, scale=4)
    private BigDecimal valormin;
    @Column(name="VALORMAX", precision=15, scale=4)
    private BigDecimal valormax;
    @OneToMany(mappedBy="prodmodelocarac")
    private Set<Gerimovelcarac> gerimovelcarac;
    @OneToMany(mappedBy="prodmodelocarac")
    private Set<Geroutrocarac> geroutrocarac;
    @OneToMany(mappedBy="prodmodelocarac")
    private Set<Gerveiculocarac> gerveiculocarac;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODMODELO", nullable=false)
    private Prodmodelo prodmodelo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODCARAC", nullable=false)
    private Prodcarac prodcarac;

    /** Default constructor. */
    public Prodmodelocarac() {
        super();
    }

    /**
     * Access method for codprodmodelocarac.
     *
     * @return the current value of codprodmodelocarac
     */
    public int getCodprodmodelocarac() {
        return codprodmodelocarac;
    }

    /**
     * Setter method for codprodmodelocarac.
     *
     * @param aCodprodmodelocarac the new value for codprodmodelocarac
     */
    public void setCodprodmodelocarac(int aCodprodmodelocarac) {
        codprodmodelocarac = aCodprodmodelocarac;
    }

    /**
     * Access method for valormin.
     *
     * @return the current value of valormin
     */
    public BigDecimal getValormin() {
        return valormin;
    }

    /**
     * Setter method for valormin.
     *
     * @param aValormin the new value for valormin
     */
    public void setValormin(BigDecimal aValormin) {
        valormin = aValormin;
    }

    /**
     * Access method for valormax.
     *
     * @return the current value of valormax
     */
    public BigDecimal getValormax() {
        return valormax;
    }

    /**
     * Setter method for valormax.
     *
     * @param aValormax the new value for valormax
     */
    public void setValormax(BigDecimal aValormax) {
        valormax = aValormax;
    }

    /**
     * Access method for gerimovelcarac.
     *
     * @return the current value of gerimovelcarac
     */
    public Set<Gerimovelcarac> getGerimovelcarac() {
        return gerimovelcarac;
    }

    /**
     * Setter method for gerimovelcarac.
     *
     * @param aGerimovelcarac the new value for gerimovelcarac
     */
    public void setGerimovelcarac(Set<Gerimovelcarac> aGerimovelcarac) {
        gerimovelcarac = aGerimovelcarac;
    }

    /**
     * Access method for geroutrocarac.
     *
     * @return the current value of geroutrocarac
     */
    public Set<Geroutrocarac> getGeroutrocarac() {
        return geroutrocarac;
    }

    /**
     * Setter method for geroutrocarac.
     *
     * @param aGeroutrocarac the new value for geroutrocarac
     */
    public void setGeroutrocarac(Set<Geroutrocarac> aGeroutrocarac) {
        geroutrocarac = aGeroutrocarac;
    }

    /**
     * Access method for gerveiculocarac.
     *
     * @return the current value of gerveiculocarac
     */
    public Set<Gerveiculocarac> getGerveiculocarac() {
        return gerveiculocarac;
    }

    /**
     * Setter method for gerveiculocarac.
     *
     * @param aGerveiculocarac the new value for gerveiculocarac
     */
    public void setGerveiculocarac(Set<Gerveiculocarac> aGerveiculocarac) {
        gerveiculocarac = aGerveiculocarac;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Prodmodelo getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Prodmodelo aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Access method for prodcarac.
     *
     * @return the current value of prodcarac
     */
    public Prodcarac getProdcarac() {
        return prodcarac;
    }

    /**
     * Setter method for prodcarac.
     *
     * @param aProdcarac the new value for prodcarac
     */
    public void setProdcarac(Prodcarac aProdcarac) {
        prodcarac = aProdcarac;
    }

    /**
     * Compares the key for this instance with another Prodmodelocarac.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodmodelocarac and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodmodelocarac)) {
            return false;
        }
        Prodmodelocarac that = (Prodmodelocarac) other;
        if (this.getCodprodmodelocarac() != that.getCodprodmodelocarac()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodmodelocarac.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodmodelocarac)) return false;
        return this.equalKeys(other) && ((Prodmodelocarac)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodmodelocarac();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodmodelocarac |");
        sb.append(" codprodmodelocarac=").append(getCodprodmodelocarac());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodmodelocarac", Integer.valueOf(getCodprodmodelocarac()));
        return ret;
    }

}
