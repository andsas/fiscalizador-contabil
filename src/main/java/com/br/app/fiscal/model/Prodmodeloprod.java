package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PRODMODELOPROD")
public class Prodmodeloprod implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodmodeloprod";

    @Id
    @Column(name="CODPRODMODELOPROD", unique=true, nullable=false, precision=10)
    private int codprodmodeloprod;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODMODELO", nullable=false)
    private Prodmodelo prodmodelo;

    /** Default constructor. */
    public Prodmodeloprod() {
        super();
    }

    /**
     * Access method for codprodmodeloprod.
     *
     * @return the current value of codprodmodeloprod
     */
    public int getCodprodmodeloprod() {
        return codprodmodeloprod;
    }

    /**
     * Setter method for codprodmodeloprod.
     *
     * @param aCodprodmodeloprod the new value for codprodmodeloprod
     */
    public void setCodprodmodeloprod(int aCodprodmodeloprod) {
        codprodmodeloprod = aCodprodmodeloprod;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Prodmodelo getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Prodmodelo aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Compares the key for this instance with another Prodmodeloprod.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodmodeloprod and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodmodeloprod)) {
            return false;
        }
        Prodmodeloprod that = (Prodmodeloprod) other;
        if (this.getCodprodmodeloprod() != that.getCodprodmodeloprod()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodmodeloprod.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodmodeloprod)) return false;
        return this.equalKeys(other) && ((Prodmodeloprod)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodmodeloprod();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodmodeloprod |");
        sb.append(" codprodmodeloprod=").append(getCodprodmodeloprod());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodmodeloprod", Integer.valueOf(getCodprodmodeloprod()));
        return ret;
    }

}
