package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRODNBS")
public class Prodnbs implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodnbs";

    @Id
    @Column(name="CODPRODNBS", unique=true, nullable=false, length=20)
    private String codprodnbs;
    @Column(name="DESCPRODNBS", nullable=false, length=250)
    private String descprodnbs;
    @OneToMany(mappedBy="prodnbs")
    private Set<Prodproduto> prodproduto;

    /** Default constructor. */
    public Prodnbs() {
        super();
    }

    /**
     * Access method for codprodnbs.
     *
     * @return the current value of codprodnbs
     */
    public String getCodprodnbs() {
        return codprodnbs;
    }

    /**
     * Setter method for codprodnbs.
     *
     * @param aCodprodnbs the new value for codprodnbs
     */
    public void setCodprodnbs(String aCodprodnbs) {
        codprodnbs = aCodprodnbs;
    }

    /**
     * Access method for descprodnbs.
     *
     * @return the current value of descprodnbs
     */
    public String getDescprodnbs() {
        return descprodnbs;
    }

    /**
     * Setter method for descprodnbs.
     *
     * @param aDescprodnbs the new value for descprodnbs
     */
    public void setDescprodnbs(String aDescprodnbs) {
        descprodnbs = aDescprodnbs;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Prodnbs.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodnbs and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodnbs)) {
            return false;
        }
        Prodnbs that = (Prodnbs) other;
        Object myCodprodnbs = this.getCodprodnbs();
        Object yourCodprodnbs = that.getCodprodnbs();
        if (myCodprodnbs==null ? yourCodprodnbs!=null : !myCodprodnbs.equals(yourCodprodnbs)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodnbs.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodnbs)) return false;
        return this.equalKeys(other) && ((Prodnbs)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodprodnbs() == null) {
            i = 0;
        } else {
            i = getCodprodnbs().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodnbs |");
        sb.append(" codprodnbs=").append(getCodprodnbs());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodnbs", getCodprodnbs());
        return ret;
    }

}
