package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRODNCM")
public class Prodncm implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodncm";

    @Id
    @Column(name="CODPRODNCM", unique=true, nullable=false, length=20)
    private String codprodncm;
    @Column(name="DESCPRODNCM", nullable=false, length=250)
    private String descprodncm;
    @Column(name="CODNVE", length=20)
    private String codnve;
    @OneToMany(mappedBy="prodncm")
    private Set<Fisesdet> fisesdet;
    @OneToMany(mappedBy="prodncm")
    private Set<Impipi> impipi;
    @OneToMany(mappedBy="prodncm")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="prodncm")
    private Set<Prodproduto> prodproduto;

    /** Default constructor. */
    public Prodncm() {
        super();
    }

    /**
     * Access method for codprodncm.
     *
     * @return the current value of codprodncm
     */
    public String getCodprodncm() {
        return codprodncm;
    }

    /**
     * Setter method for codprodncm.
     *
     * @param aCodprodncm the new value for codprodncm
     */
    public void setCodprodncm(String aCodprodncm) {
        codprodncm = aCodprodncm;
    }

    /**
     * Access method for descprodncm.
     *
     * @return the current value of descprodncm
     */
    public String getDescprodncm() {
        return descprodncm;
    }

    /**
     * Setter method for descprodncm.
     *
     * @param aDescprodncm the new value for descprodncm
     */
    public void setDescprodncm(String aDescprodncm) {
        descprodncm = aDescprodncm;
    }

    /**
     * Access method for codnve.
     *
     * @return the current value of codnve
     */
    public String getCodnve() {
        return codnve;
    }

    /**
     * Setter method for codnve.
     *
     * @param aCodnve the new value for codnve
     */
    public void setCodnve(String aCodnve) {
        codnve = aCodnve;
    }

    /**
     * Access method for fisesdet.
     *
     * @return the current value of fisesdet
     */
    public Set<Fisesdet> getFisesdet() {
        return fisesdet;
    }

    /**
     * Setter method for fisesdet.
     *
     * @param aFisesdet the new value for fisesdet
     */
    public void setFisesdet(Set<Fisesdet> aFisesdet) {
        fisesdet = aFisesdet;
    }

    /**
     * Access method for impipi.
     *
     * @return the current value of impipi
     */
    public Set<Impipi> getImpipi() {
        return impipi;
    }

    /**
     * Setter method for impipi.
     *
     * @param aImpipi the new value for impipi
     */
    public void setImpipi(Set<Impipi> aImpipi) {
        impipi = aImpipi;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Prodncm.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodncm and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodncm)) {
            return false;
        }
        Prodncm that = (Prodncm) other;
        Object myCodprodncm = this.getCodprodncm();
        Object yourCodprodncm = that.getCodprodncm();
        if (myCodprodncm==null ? yourCodprodncm!=null : !myCodprodncm.equals(yourCodprodncm)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodncm.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodncm)) return false;
        return this.equalKeys(other) && ((Prodncm)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodprodncm() == null) {
            i = 0;
        } else {
            i = getCodprodncm().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodncm |");
        sb.append(" codprodncm=").append(getCodprodncm());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodncm", getCodprodncm());
        return ret;
    }

}
