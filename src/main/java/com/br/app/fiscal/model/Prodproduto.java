package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRODPRODUTO")
public class Prodproduto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodproduto";

    @Id
    @Column(name="CODPRODPRODUTO", unique=true, nullable=false, precision=10)
    private int codprodproduto;
    @Column(name="NOMEPRODPRODUTO", nullable=false, length=250)
    private String nomeprodproduto;
    @Column(name="NOMEAMIGAVEL", length=250)
    private String nomeamigavel;
    @Column(name="CODPRODEAN", length=60)
    private String codprodean;
    @Column(name="PESOLIQ", precision=15, scale=4)
    private BigDecimal pesoliq;
    @Column(name="PESOBRT", precision=15, scale=4)
    private BigDecimal pesobrt;
    @Column(name="CODBARRAS", length=40)
    private String codbarras;
    @Column(name="SERVICO", precision=5)
    private short servico;
    @Column(name="VALORSERVICO", precision=15, scale=4)
    private BigDecimal valorservico;
    @Column(name="OBSERVACOES")
    private String observacoes;
    @OneToMany(mappedBy="prodproduto")
    private Set<Crmslaprodproduto> crmslaprodproduto;
    @OneToMany(mappedBy="prodproduto")
    private Set<Ctbbem> ctbbem;
    @OneToMany(mappedBy="prodproduto")
    private Set<Cushist> cushist;
    @OneToMany(mappedBy="prodproduto")
    private Set<Fisesdet> fisesdet;
    @OneToMany(mappedBy="prodproduto")
    private Set<Gerimovel> gerimovel;
    @OneToMany(mappedBy="prodproduto")
    private Set<Geroutro> geroutro;
    @OneToMany(mappedBy="prodproduto")
    private Set<Gerveiculo> gerveiculo;
    @OneToMany(mappedBy="prodproduto")
    private Set<Impplanofiscal> impplanofiscal;
    @OneToMany(mappedBy="prodproduto")
    private Set<Invprodsaldo> invprodsaldo;
    @OneToMany(mappedBy="prodproduto")
    private Set<Invprodsaldomov> invprodsaldomov;
    @OneToMany(mappedBy="prodproduto")
    private Set<Oporcdet> oporcdet;
    @OneToMany(mappedBy="prodproduto")
    private Set<Opsolicitacaodet> opsolicitacaodet;
    @OneToMany(mappedBy="prodproduto")
    private Set<Optransacaodet> optransacaodet;
    @OneToMany(mappedBy="prodproduto")
    private Set<Prdcordemetapaproduto> prdcordemetapaproduto;
    @OneToMany(mappedBy="prodproduto")
    private Set<Prodembprod> prodembprod;
    @OneToMany(mappedBy="prodproduto")
    private Set<Prodminmax> prodminmax;
    @OneToMany(mappedBy="prodproduto")
    private Set<Prodmodeloprod> prodmodeloprod;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODUNIDADE", nullable=false)
    private Produnidade produnidade;
    @ManyToOne
    @JoinColumn(name="CODIMPCNAE")
    private Impcnae impcnae;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODGRUPO", nullable=false)
    private Prodgrupo prodgrupo;
    @ManyToOne
    @JoinColumn(name="CODPRODNCM")
    private Prodncm prodncm;
    @ManyToOne
    @JoinColumn(name="CODPRODMARCA")
    private Prodmarca prodmarca;
    @ManyToOne
    @JoinColumn(name="CODPRODESPECIE")
    private Prodespecie prodespecie;
    @ManyToOne
    @JoinColumn(name="CODPRODMODELO")
    private Prodmodelo prodmodelo;
    @ManyToOne
    @JoinColumn(name="CODCTBBEMTIPO")
    private Ctbbemtipo ctbbemtipo;
    @ManyToOne
    @JoinColumn(name="CODPRODNBS")
    private Prodnbs prodnbs;
    @ManyToOne
    @JoinColumn(name="CODPRODSERVICOLISTA")
    private Prodservicolista prodservicolista;
    @OneToMany(mappedBy="prodproduto")
    private Set<Prodprodutocomp> prodprodutocomp;
    @OneToMany(mappedBy="prodproduto2")
    private Set<Prodprodutocomp> prodprodutocomp2;
    @OneToMany(mappedBy="prodproduto")
    private Set<Prodprodutolocal> prodprodutolocal;
    @OneToMany(mappedBy="prodproduto")
    private Set<Prtabeladet> prtabeladet;

    /** Default constructor. */
    public Prodproduto() {
        super();
    }

    /**
     * Access method for codprodproduto.
     *
     * @return the current value of codprodproduto
     */
    public int getCodprodproduto() {
        return codprodproduto;
    }

    /**
     * Setter method for codprodproduto.
     *
     * @param aCodprodproduto the new value for codprodproduto
     */
    public void setCodprodproduto(int aCodprodproduto) {
        codprodproduto = aCodprodproduto;
    }

    /**
     * Access method for nomeprodproduto.
     *
     * @return the current value of nomeprodproduto
     */
    public String getNomeprodproduto() {
        return nomeprodproduto;
    }

    /**
     * Setter method for nomeprodproduto.
     *
     * @param aNomeprodproduto the new value for nomeprodproduto
     */
    public void setNomeprodproduto(String aNomeprodproduto) {
        nomeprodproduto = aNomeprodproduto;
    }

    /**
     * Access method for nomeamigavel.
     *
     * @return the current value of nomeamigavel
     */
    public String getNomeamigavel() {
        return nomeamigavel;
    }

    /**
     * Setter method for nomeamigavel.
     *
     * @param aNomeamigavel the new value for nomeamigavel
     */
    public void setNomeamigavel(String aNomeamigavel) {
        nomeamigavel = aNomeamigavel;
    }

    /**
     * Access method for codprodean.
     *
     * @return the current value of codprodean
     */
    public String getCodprodean() {
        return codprodean;
    }

    /**
     * Setter method for codprodean.
     *
     * @param aCodprodean the new value for codprodean
     */
    public void setCodprodean(String aCodprodean) {
        codprodean = aCodprodean;
    }

    /**
     * Access method for pesoliq.
     *
     * @return the current value of pesoliq
     */
    public BigDecimal getPesoliq() {
        return pesoliq;
    }

    /**
     * Setter method for pesoliq.
     *
     * @param aPesoliq the new value for pesoliq
     */
    public void setPesoliq(BigDecimal aPesoliq) {
        pesoliq = aPesoliq;
    }

    /**
     * Access method for pesobrt.
     *
     * @return the current value of pesobrt
     */
    public BigDecimal getPesobrt() {
        return pesobrt;
    }

    /**
     * Setter method for pesobrt.
     *
     * @param aPesobrt the new value for pesobrt
     */
    public void setPesobrt(BigDecimal aPesobrt) {
        pesobrt = aPesobrt;
    }

    /**
     * Access method for codbarras.
     *
     * @return the current value of codbarras
     */
    public String getCodbarras() {
        return codbarras;
    }

    /**
     * Setter method for codbarras.
     *
     * @param aCodbarras the new value for codbarras
     */
    public void setCodbarras(String aCodbarras) {
        codbarras = aCodbarras;
    }

    /**
     * Access method for servico.
     *
     * @return the current value of servico
     */
    public short getServico() {
        return servico;
    }

    /**
     * Setter method for servico.
     *
     * @param aServico the new value for servico
     */
    public void setServico(short aServico) {
        servico = aServico;
    }

    /**
     * Access method for valorservico.
     *
     * @return the current value of valorservico
     */
    public BigDecimal getValorservico() {
        return valorservico;
    }

    /**
     * Setter method for valorservico.
     *
     * @param aValorservico the new value for valorservico
     */
    public void setValorservico(BigDecimal aValorservico) {
        valorservico = aValorservico;
    }

    /**
     * Access method for observacoes.
     *
     * @return the current value of observacoes
     */
    public String getObservacoes() {
        return observacoes;
    }

    /**
     * Setter method for observacoes.
     *
     * @param aObservacoes the new value for observacoes
     */
    public void setObservacoes(String aObservacoes) {
        observacoes = aObservacoes;
    }

    /**
     * Access method for crmslaprodproduto.
     *
     * @return the current value of crmslaprodproduto
     */
    public Set<Crmslaprodproduto> getCrmslaprodproduto() {
        return crmslaprodproduto;
    }

    /**
     * Setter method for crmslaprodproduto.
     *
     * @param aCrmslaprodproduto the new value for crmslaprodproduto
     */
    public void setCrmslaprodproduto(Set<Crmslaprodproduto> aCrmslaprodproduto) {
        crmslaprodproduto = aCrmslaprodproduto;
    }

    /**
     * Access method for ctbbem.
     *
     * @return the current value of ctbbem
     */
    public Set<Ctbbem> getCtbbem() {
        return ctbbem;
    }

    /**
     * Setter method for ctbbem.
     *
     * @param aCtbbem the new value for ctbbem
     */
    public void setCtbbem(Set<Ctbbem> aCtbbem) {
        ctbbem = aCtbbem;
    }

    /**
     * Access method for cushist.
     *
     * @return the current value of cushist
     */
    public Set<Cushist> getCushist() {
        return cushist;
    }

    /**
     * Setter method for cushist.
     *
     * @param aCushist the new value for cushist
     */
    public void setCushist(Set<Cushist> aCushist) {
        cushist = aCushist;
    }

    /**
     * Access method for fisesdet.
     *
     * @return the current value of fisesdet
     */
    public Set<Fisesdet> getFisesdet() {
        return fisesdet;
    }

    /**
     * Setter method for fisesdet.
     *
     * @param aFisesdet the new value for fisesdet
     */
    public void setFisesdet(Set<Fisesdet> aFisesdet) {
        fisesdet = aFisesdet;
    }

    /**
     * Access method for gerimovel.
     *
     * @return the current value of gerimovel
     */
    public Set<Gerimovel> getGerimovel() {
        return gerimovel;
    }

    /**
     * Setter method for gerimovel.
     *
     * @param aGerimovel the new value for gerimovel
     */
    public void setGerimovel(Set<Gerimovel> aGerimovel) {
        gerimovel = aGerimovel;
    }

    /**
     * Access method for geroutro.
     *
     * @return the current value of geroutro
     */
    public Set<Geroutro> getGeroutro() {
        return geroutro;
    }

    /**
     * Setter method for geroutro.
     *
     * @param aGeroutro the new value for geroutro
     */
    public void setGeroutro(Set<Geroutro> aGeroutro) {
        geroutro = aGeroutro;
    }

    /**
     * Access method for gerveiculo.
     *
     * @return the current value of gerveiculo
     */
    public Set<Gerveiculo> getGerveiculo() {
        return gerveiculo;
    }

    /**
     * Setter method for gerveiculo.
     *
     * @param aGerveiculo the new value for gerveiculo
     */
    public void setGerveiculo(Set<Gerveiculo> aGerveiculo) {
        gerveiculo = aGerveiculo;
    }

    /**
     * Access method for impplanofiscal.
     *
     * @return the current value of impplanofiscal
     */
    public Set<Impplanofiscal> getImpplanofiscal() {
        return impplanofiscal;
    }

    /**
     * Setter method for impplanofiscal.
     *
     * @param aImpplanofiscal the new value for impplanofiscal
     */
    public void setImpplanofiscal(Set<Impplanofiscal> aImpplanofiscal) {
        impplanofiscal = aImpplanofiscal;
    }

    /**
     * Access method for invprodsaldo.
     *
     * @return the current value of invprodsaldo
     */
    public Set<Invprodsaldo> getInvprodsaldo() {
        return invprodsaldo;
    }

    /**
     * Setter method for invprodsaldo.
     *
     * @param aInvprodsaldo the new value for invprodsaldo
     */
    public void setInvprodsaldo(Set<Invprodsaldo> aInvprodsaldo) {
        invprodsaldo = aInvprodsaldo;
    }

    /**
     * Access method for invprodsaldomov.
     *
     * @return the current value of invprodsaldomov
     */
    public Set<Invprodsaldomov> getInvprodsaldomov() {
        return invprodsaldomov;
    }

    /**
     * Setter method for invprodsaldomov.
     *
     * @param aInvprodsaldomov the new value for invprodsaldomov
     */
    public void setInvprodsaldomov(Set<Invprodsaldomov> aInvprodsaldomov) {
        invprodsaldomov = aInvprodsaldomov;
    }

    /**
     * Access method for oporcdet.
     *
     * @return the current value of oporcdet
     */
    public Set<Oporcdet> getOporcdet() {
        return oporcdet;
    }

    /**
     * Setter method for oporcdet.
     *
     * @param aOporcdet the new value for oporcdet
     */
    public void setOporcdet(Set<Oporcdet> aOporcdet) {
        oporcdet = aOporcdet;
    }

    /**
     * Access method for opsolicitacaodet.
     *
     * @return the current value of opsolicitacaodet
     */
    public Set<Opsolicitacaodet> getOpsolicitacaodet() {
        return opsolicitacaodet;
    }

    /**
     * Setter method for opsolicitacaodet.
     *
     * @param aOpsolicitacaodet the new value for opsolicitacaodet
     */
    public void setOpsolicitacaodet(Set<Opsolicitacaodet> aOpsolicitacaodet) {
        opsolicitacaodet = aOpsolicitacaodet;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Access method for prdcordemetapaproduto.
     *
     * @return the current value of prdcordemetapaproduto
     */
    public Set<Prdcordemetapaproduto> getPrdcordemetapaproduto() {
        return prdcordemetapaproduto;
    }

    /**
     * Setter method for prdcordemetapaproduto.
     *
     * @param aPrdcordemetapaproduto the new value for prdcordemetapaproduto
     */
    public void setPrdcordemetapaproduto(Set<Prdcordemetapaproduto> aPrdcordemetapaproduto) {
        prdcordemetapaproduto = aPrdcordemetapaproduto;
    }

    /**
     * Access method for prodembprod.
     *
     * @return the current value of prodembprod
     */
    public Set<Prodembprod> getProdembprod() {
        return prodembprod;
    }

    /**
     * Setter method for prodembprod.
     *
     * @param aProdembprod the new value for prodembprod
     */
    public void setProdembprod(Set<Prodembprod> aProdembprod) {
        prodembprod = aProdembprod;
    }

    /**
     * Access method for prodminmax.
     *
     * @return the current value of prodminmax
     */
    public Set<Prodminmax> getProdminmax() {
        return prodminmax;
    }

    /**
     * Setter method for prodminmax.
     *
     * @param aProdminmax the new value for prodminmax
     */
    public void setProdminmax(Set<Prodminmax> aProdminmax) {
        prodminmax = aProdminmax;
    }

    /**
     * Access method for prodmodeloprod.
     *
     * @return the current value of prodmodeloprod
     */
    public Set<Prodmodeloprod> getProdmodeloprod() {
        return prodmodeloprod;
    }

    /**
     * Setter method for prodmodeloprod.
     *
     * @param aProdmodeloprod the new value for prodmodeloprod
     */
    public void setProdmodeloprod(Set<Prodmodeloprod> aProdmodeloprod) {
        prodmodeloprod = aProdmodeloprod;
    }

    /**
     * Access method for produnidade.
     *
     * @return the current value of produnidade
     */
    public Produnidade getProdunidade() {
        return produnidade;
    }

    /**
     * Setter method for produnidade.
     *
     * @param aProdunidade the new value for produnidade
     */
    public void setProdunidade(Produnidade aProdunidade) {
        produnidade = aProdunidade;
    }

    /**
     * Access method for impcnae.
     *
     * @return the current value of impcnae
     */
    public Impcnae getImpcnae() {
        return impcnae;
    }

    /**
     * Setter method for impcnae.
     *
     * @param aImpcnae the new value for impcnae
     */
    public void setImpcnae(Impcnae aImpcnae) {
        impcnae = aImpcnae;
    }

    /**
     * Access method for prodgrupo.
     *
     * @return the current value of prodgrupo
     */
    public Prodgrupo getProdgrupo() {
        return prodgrupo;
    }

    /**
     * Setter method for prodgrupo.
     *
     * @param aProdgrupo the new value for prodgrupo
     */
    public void setProdgrupo(Prodgrupo aProdgrupo) {
        prodgrupo = aProdgrupo;
    }

    /**
     * Access method for prodncm.
     *
     * @return the current value of prodncm
     */
    public Prodncm getProdncm() {
        return prodncm;
    }

    /**
     * Setter method for prodncm.
     *
     * @param aProdncm the new value for prodncm
     */
    public void setProdncm(Prodncm aProdncm) {
        prodncm = aProdncm;
    }

    /**
     * Access method for prodmarca.
     *
     * @return the current value of prodmarca
     */
    public Prodmarca getProdmarca() {
        return prodmarca;
    }

    /**
     * Setter method for prodmarca.
     *
     * @param aProdmarca the new value for prodmarca
     */
    public void setProdmarca(Prodmarca aProdmarca) {
        prodmarca = aProdmarca;
    }

    /**
     * Access method for prodespecie.
     *
     * @return the current value of prodespecie
     */
    public Prodespecie getProdespecie() {
        return prodespecie;
    }

    /**
     * Setter method for prodespecie.
     *
     * @param aProdespecie the new value for prodespecie
     */
    public void setProdespecie(Prodespecie aProdespecie) {
        prodespecie = aProdespecie;
    }

    /**
     * Access method for prodmodelo.
     *
     * @return the current value of prodmodelo
     */
    public Prodmodelo getProdmodelo() {
        return prodmodelo;
    }

    /**
     * Setter method for prodmodelo.
     *
     * @param aProdmodelo the new value for prodmodelo
     */
    public void setProdmodelo(Prodmodelo aProdmodelo) {
        prodmodelo = aProdmodelo;
    }

    /**
     * Access method for ctbbemtipo.
     *
     * @return the current value of ctbbemtipo
     */
    public Ctbbemtipo getCtbbemtipo() {
        return ctbbemtipo;
    }

    /**
     * Setter method for ctbbemtipo.
     *
     * @param aCtbbemtipo the new value for ctbbemtipo
     */
    public void setCtbbemtipo(Ctbbemtipo aCtbbemtipo) {
        ctbbemtipo = aCtbbemtipo;
    }

    /**
     * Access method for prodnbs.
     *
     * @return the current value of prodnbs
     */
    public Prodnbs getProdnbs() {
        return prodnbs;
    }

    /**
     * Setter method for prodnbs.
     *
     * @param aProdnbs the new value for prodnbs
     */
    public void setProdnbs(Prodnbs aProdnbs) {
        prodnbs = aProdnbs;
    }

    /**
     * Access method for prodservicolista.
     *
     * @return the current value of prodservicolista
     */
    public Prodservicolista getProdservicolista() {
        return prodservicolista;
    }

    /**
     * Setter method for prodservicolista.
     *
     * @param aProdservicolista the new value for prodservicolista
     */
    public void setProdservicolista(Prodservicolista aProdservicolista) {
        prodservicolista = aProdservicolista;
    }

    /**
     * Access method for prodprodutocomp.
     *
     * @return the current value of prodprodutocomp
     */
    public Set<Prodprodutocomp> getProdprodutocomp() {
        return prodprodutocomp;
    }

    /**
     * Setter method for prodprodutocomp.
     *
     * @param aProdprodutocomp the new value for prodprodutocomp
     */
    public void setProdprodutocomp(Set<Prodprodutocomp> aProdprodutocomp) {
        prodprodutocomp = aProdprodutocomp;
    }

    /**
     * Access method for prodprodutocomp2.
     *
     * @return the current value of prodprodutocomp2
     */
    public Set<Prodprodutocomp> getProdprodutocomp2() {
        return prodprodutocomp2;
    }

    /**
     * Setter method for prodprodutocomp2.
     *
     * @param aProdprodutocomp2 the new value for prodprodutocomp2
     */
    public void setProdprodutocomp2(Set<Prodprodutocomp> aProdprodutocomp2) {
        prodprodutocomp2 = aProdprodutocomp2;
    }

    /**
     * Access method for prodprodutolocal.
     *
     * @return the current value of prodprodutolocal
     */
    public Set<Prodprodutolocal> getProdprodutolocal() {
        return prodprodutolocal;
    }

    /**
     * Setter method for prodprodutolocal.
     *
     * @param aProdprodutolocal the new value for prodprodutolocal
     */
    public void setProdprodutolocal(Set<Prodprodutolocal> aProdprodutolocal) {
        prodprodutolocal = aProdprodutolocal;
    }

    /**
     * Access method for prtabeladet.
     *
     * @return the current value of prtabeladet
     */
    public Set<Prtabeladet> getPrtabeladet() {
        return prtabeladet;
    }

    /**
     * Setter method for prtabeladet.
     *
     * @param aPrtabeladet the new value for prtabeladet
     */
    public void setPrtabeladet(Set<Prtabeladet> aPrtabeladet) {
        prtabeladet = aPrtabeladet;
    }

    /**
     * Compares the key for this instance with another Prodproduto.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodproduto and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodproduto)) {
            return false;
        }
        Prodproduto that = (Prodproduto) other;
        if (this.getCodprodproduto() != that.getCodprodproduto()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodproduto.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodproduto)) return false;
        return this.equalKeys(other) && ((Prodproduto)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodproduto();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodproduto |");
        sb.append(" codprodproduto=").append(getCodprodproduto());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodproduto", Integer.valueOf(getCodprodproduto()));
        return ret;
    }

}
