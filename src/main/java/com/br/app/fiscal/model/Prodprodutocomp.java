package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PRODPRODUTOCOMP")
public class Prodprodutocomp implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodprodutocomp";

    @Id
    @Column(name="CODPRODPRODUTOCOMP", unique=true, nullable=false, precision=10)
    private int codprodprodutocomp;
    @Column(name="TIPOFINAL", precision=5)
    private short tipofinal;
    @Column(name="TIPOPARTE", precision=5)
    private short tipoparte;
    @Column(name="QUANTIDADEPARTE", precision=15, scale=4)
    private BigDecimal quantidadeparte;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTOFINAL", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTOPARTE", nullable=false)
    private Prodproduto prodproduto2;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRDCPLANOETAPA", nullable=false)
    private Prdcplanoetapa prdcplanoetapa;

    /** Default constructor. */
    public Prodprodutocomp() {
        super();
    }

    /**
     * Access method for codprodprodutocomp.
     *
     * @return the current value of codprodprodutocomp
     */
    public int getCodprodprodutocomp() {
        return codprodprodutocomp;
    }

    /**
     * Setter method for codprodprodutocomp.
     *
     * @param aCodprodprodutocomp the new value for codprodprodutocomp
     */
    public void setCodprodprodutocomp(int aCodprodprodutocomp) {
        codprodprodutocomp = aCodprodprodutocomp;
    }

    /**
     * Access method for tipofinal.
     *
     * @return the current value of tipofinal
     */
    public short getTipofinal() {
        return tipofinal;
    }

    /**
     * Setter method for tipofinal.
     *
     * @param aTipofinal the new value for tipofinal
     */
    public void setTipofinal(short aTipofinal) {
        tipofinal = aTipofinal;
    }

    /**
     * Access method for tipoparte.
     *
     * @return the current value of tipoparte
     */
    public short getTipoparte() {
        return tipoparte;
    }

    /**
     * Setter method for tipoparte.
     *
     * @param aTipoparte the new value for tipoparte
     */
    public void setTipoparte(short aTipoparte) {
        tipoparte = aTipoparte;
    }

    /**
     * Access method for quantidadeparte.
     *
     * @return the current value of quantidadeparte
     */
    public BigDecimal getQuantidadeparte() {
        return quantidadeparte;
    }

    /**
     * Setter method for quantidadeparte.
     *
     * @param aQuantidadeparte the new value for quantidadeparte
     */
    public void setQuantidadeparte(BigDecimal aQuantidadeparte) {
        quantidadeparte = aQuantidadeparte;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for prodproduto2.
     *
     * @return the current value of prodproduto2
     */
    public Prodproduto getProdproduto2() {
        return prodproduto2;
    }

    /**
     * Setter method for prodproduto2.
     *
     * @param aProdproduto2 the new value for prodproduto2
     */
    public void setProdproduto2(Prodproduto aProdproduto2) {
        prodproduto2 = aProdproduto2;
    }

    /**
     * Access method for prdcplanoetapa.
     *
     * @return the current value of prdcplanoetapa
     */
    public Prdcplanoetapa getPrdcplanoetapa() {
        return prdcplanoetapa;
    }

    /**
     * Setter method for prdcplanoetapa.
     *
     * @param aPrdcplanoetapa the new value for prdcplanoetapa
     */
    public void setPrdcplanoetapa(Prdcplanoetapa aPrdcplanoetapa) {
        prdcplanoetapa = aPrdcplanoetapa;
    }

    /**
     * Compares the key for this instance with another Prodprodutocomp.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodprodutocomp and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodprodutocomp)) {
            return false;
        }
        Prodprodutocomp that = (Prodprodutocomp) other;
        if (this.getCodprodprodutocomp() != that.getCodprodprodutocomp()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodprodutocomp.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodprodutocomp)) return false;
        return this.equalKeys(other) && ((Prodprodutocomp)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodprodutocomp();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodprodutocomp |");
        sb.append(" codprodprodutocomp=").append(getCodprodprodutocomp());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodprodutocomp", Integer.valueOf(getCodprodprodutocomp()));
        return ret;
    }

}
