package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="PRODPRODUTOGRADE")
public class Prodprodutograde implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodprodutograde";

    @Id
    @Column(name="CODPRODPRODUTOGRADE", unique=true, nullable=false, precision=10)
    private int codprodprodutograde;
    @Column(name="CODPRODPRODUTO", nullable=false, precision=10)
    private int codprodproduto;
    @Column(name="CODPRODGRADE", nullable=false, length=20)
    private String codprodgrade;

    /** Default constructor. */
    public Prodprodutograde() {
        super();
    }

    /**
     * Access method for codprodprodutograde.
     *
     * @return the current value of codprodprodutograde
     */
    public int getCodprodprodutograde() {
        return codprodprodutograde;
    }

    /**
     * Setter method for codprodprodutograde.
     *
     * @param aCodprodprodutograde the new value for codprodprodutograde
     */
    public void setCodprodprodutograde(int aCodprodprodutograde) {
        codprodprodutograde = aCodprodprodutograde;
    }

    /**
     * Access method for codprodproduto.
     *
     * @return the current value of codprodproduto
     */
    public int getCodprodproduto() {
        return codprodproduto;
    }

    /**
     * Setter method for codprodproduto.
     *
     * @param aCodprodproduto the new value for codprodproduto
     */
    public void setCodprodproduto(int aCodprodproduto) {
        codprodproduto = aCodprodproduto;
    }

    /**
     * Access method for codprodgrade.
     *
     * @return the current value of codprodgrade
     */
    public String getCodprodgrade() {
        return codprodgrade;
    }

    /**
     * Setter method for codprodgrade.
     *
     * @param aCodprodgrade the new value for codprodgrade
     */
    public void setCodprodgrade(String aCodprodgrade) {
        codprodgrade = aCodprodgrade;
    }

    /**
     * Compares the key for this instance with another Prodprodutograde.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodprodutograde and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodprodutograde)) {
            return false;
        }
        Prodprodutograde that = (Prodprodutograde) other;
        if (this.getCodprodprodutograde() != that.getCodprodprodutograde()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodprodutograde.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodprodutograde)) return false;
        return this.equalKeys(other) && ((Prodprodutograde)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodprodutograde();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodprodutograde |");
        sb.append(" codprodprodutograde=").append(getCodprodprodutograde());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodprodutograde", Integer.valueOf(getCodprodprodutograde()));
        return ret;
    }

}
