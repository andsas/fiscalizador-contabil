package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PRODPRODUTOLOCAL")
public class Prodprodutolocal implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodprodutolocal";

    @Id
    @Column(name="CODPRODPRODUTOLOCAL", unique=true, nullable=false, precision=10)
    private int codprodprodutolocal;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODLOCAL", nullable=false)
    private Prodlocal prodlocal;

    /** Default constructor. */
    public Prodprodutolocal() {
        super();
    }

    /**
     * Access method for codprodprodutolocal.
     *
     * @return the current value of codprodprodutolocal
     */
    public int getCodprodprodutolocal() {
        return codprodprodutolocal;
    }

    /**
     * Setter method for codprodprodutolocal.
     *
     * @param aCodprodprodutolocal the new value for codprodprodutolocal
     */
    public void setCodprodprodutolocal(int aCodprodprodutolocal) {
        codprodprodutolocal = aCodprodprodutolocal;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for prodlocal.
     *
     * @return the current value of prodlocal
     */
    public Prodlocal getProdlocal() {
        return prodlocal;
    }

    /**
     * Setter method for prodlocal.
     *
     * @param aProdlocal the new value for prodlocal
     */
    public void setProdlocal(Prodlocal aProdlocal) {
        prodlocal = aProdlocal;
    }

    /**
     * Compares the key for this instance with another Prodprodutolocal.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodprodutolocal and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodprodutolocal)) {
            return false;
        }
        Prodprodutolocal that = (Prodprodutolocal) other;
        if (this.getCodprodprodutolocal() != that.getCodprodprodutolocal()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodprodutolocal.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodprodutolocal)) return false;
        return this.equalKeys(other) && ((Prodprodutolocal)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprodprodutolocal();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodprodutolocal |");
        sb.append(" codprodprodutolocal=").append(getCodprodprodutolocal());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodprodutolocal", Integer.valueOf(getCodprodprodutolocal()));
        return ret;
    }

}
