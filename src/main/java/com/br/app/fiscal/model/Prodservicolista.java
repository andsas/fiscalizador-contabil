package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRODSERVICOLISTA")
public class Prodservicolista implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodservicolista";

    @Id
    @Column(name="CODPRODSERVICOLISTA", unique=true, nullable=false, length=20)
    private String codprodservicolista;
    @Column(name="DESCPRODSERVICOLISTA", nullable=false, length=250)
    private String descprodservicolista;
    @Column(name="CODPRODPARENTSERVICOLISTA", length=20)
    private String codprodparentservicolista;
    @Column(name="NIVEL", precision=5)
    private short nivel;
    @OneToMany(mappedBy="prodservicolista")
    private Set<Prodproduto> prodproduto;

    /** Default constructor. */
    public Prodservicolista() {
        super();
    }

    /**
     * Access method for codprodservicolista.
     *
     * @return the current value of codprodservicolista
     */
    public String getCodprodservicolista() {
        return codprodservicolista;
    }

    /**
     * Setter method for codprodservicolista.
     *
     * @param aCodprodservicolista the new value for codprodservicolista
     */
    public void setCodprodservicolista(String aCodprodservicolista) {
        codprodservicolista = aCodprodservicolista;
    }

    /**
     * Access method for descprodservicolista.
     *
     * @return the current value of descprodservicolista
     */
    public String getDescprodservicolista() {
        return descprodservicolista;
    }

    /**
     * Setter method for descprodservicolista.
     *
     * @param aDescprodservicolista the new value for descprodservicolista
     */
    public void setDescprodservicolista(String aDescprodservicolista) {
        descprodservicolista = aDescprodservicolista;
    }

    /**
     * Access method for codprodparentservicolista.
     *
     * @return the current value of codprodparentservicolista
     */
    public String getCodprodparentservicolista() {
        return codprodparentservicolista;
    }

    /**
     * Setter method for codprodparentservicolista.
     *
     * @param aCodprodparentservicolista the new value for codprodparentservicolista
     */
    public void setCodprodparentservicolista(String aCodprodparentservicolista) {
        codprodparentservicolista = aCodprodparentservicolista;
    }

    /**
     * Access method for nivel.
     *
     * @return the current value of nivel
     */
    public short getNivel() {
        return nivel;
    }

    /**
     * Setter method for nivel.
     *
     * @param aNivel the new value for nivel
     */
    public void setNivel(short aNivel) {
        nivel = aNivel;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Prodservicolista.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prodservicolista and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prodservicolista)) {
            return false;
        }
        Prodservicolista that = (Prodservicolista) other;
        Object myCodprodservicolista = this.getCodprodservicolista();
        Object yourCodprodservicolista = that.getCodprodservicolista();
        if (myCodprodservicolista==null ? yourCodprodservicolista!=null : !myCodprodservicolista.equals(yourCodprodservicolista)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prodservicolista.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prodservicolista)) return false;
        return this.equalKeys(other) && ((Prodservicolista)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodprodservicolista() == null) {
            i = 0;
        } else {
            i = getCodprodservicolista().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prodservicolista |");
        sb.append(" codprodservicolista=").append(getCodprodservicolista());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodservicolista", getCodprodservicolista());
        return ret;
    }

}
