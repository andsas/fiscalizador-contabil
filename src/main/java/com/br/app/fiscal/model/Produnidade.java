package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRODUNIDADE")
public class Produnidade implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodunidade";

    @Id
    @Column(name="CODPRODUNIDADE", unique=true, nullable=false, length=20)
    private String codprodunidade;
    @Column(name="DESCPRODUNIDADE", nullable=false, length=250)
    private String descprodunidade;
    @OneToMany(mappedBy="produnidade")
    private Set<Fisesdet> fisesdet;
    @OneToMany(mappedBy="produnidade")
    private Set<Optransacaodet> optransacaodet;
    @OneToMany(mappedBy="produnidade")
    private Set<Prodproduto> prodproduto;
    @OneToMany(mappedBy="produnidade")
    private Set<Produnidadeequiv> produnidadeequiv;

    /** Default constructor. */
    public Produnidade() {
        super();
    }

    /**
     * Access method for codprodunidade.
     *
     * @return the current value of codprodunidade
     */
    public String getCodprodunidade() {
        return codprodunidade;
    }

    /**
     * Setter method for codprodunidade.
     *
     * @param aCodprodunidade the new value for codprodunidade
     */
    public void setCodprodunidade(String aCodprodunidade) {
        codprodunidade = aCodprodunidade;
    }

    /**
     * Access method for descprodunidade.
     *
     * @return the current value of descprodunidade
     */
    public String getDescprodunidade() {
        return descprodunidade;
    }

    /**
     * Setter method for descprodunidade.
     *
     * @param aDescprodunidade the new value for descprodunidade
     */
    public void setDescprodunidade(String aDescprodunidade) {
        descprodunidade = aDescprodunidade;
    }

    /**
     * Access method for fisesdet.
     *
     * @return the current value of fisesdet
     */
    public Set<Fisesdet> getFisesdet() {
        return fisesdet;
    }

    /**
     * Setter method for fisesdet.
     *
     * @param aFisesdet the new value for fisesdet
     */
    public void setFisesdet(Set<Fisesdet> aFisesdet) {
        fisesdet = aFisesdet;
    }

    /**
     * Access method for optransacaodet.
     *
     * @return the current value of optransacaodet
     */
    public Set<Optransacaodet> getOptransacaodet() {
        return optransacaodet;
    }

    /**
     * Setter method for optransacaodet.
     *
     * @param aOptransacaodet the new value for optransacaodet
     */
    public void setOptransacaodet(Set<Optransacaodet> aOptransacaodet) {
        optransacaodet = aOptransacaodet;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Set<Prodproduto> getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Set<Prodproduto> aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Access method for produnidadeequiv.
     *
     * @return the current value of produnidadeequiv
     */
    public Set<Produnidadeequiv> getProdunidadeequiv() {
        return produnidadeequiv;
    }

    /**
     * Setter method for produnidadeequiv.
     *
     * @param aProdunidadeequiv the new value for produnidadeequiv
     */
    public void setProdunidadeequiv(Set<Produnidadeequiv> aProdunidadeequiv) {
        produnidadeequiv = aProdunidadeequiv;
    }

    /**
     * Compares the key for this instance with another Produnidade.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Produnidade and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Produnidade)) {
            return false;
        }
        Produnidade that = (Produnidade) other;
        Object myCodprodunidade = this.getCodprodunidade();
        Object yourCodprodunidade = that.getCodprodunidade();
        if (myCodprodunidade==null ? yourCodprodunidade!=null : !myCodprodunidade.equals(yourCodprodunidade)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Produnidade.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Produnidade)) return false;
        return this.equalKeys(other) && ((Produnidade)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodprodunidade() == null) {
            i = 0;
        } else {
            i = getCodprodunidade().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Produnidade |");
        sb.append(" codprodunidade=").append(getCodprodunidade());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodunidade", getCodprodunidade());
        return ret;
    }

}
