package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PRODUNIDADEEQUIV")
public class Produnidadeequiv implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprodunidadeequiv";

    @Id
    @Column(name="CODPRODUNIDADEEQUIV", unique=true, nullable=false, length=20)
    private String codprodunidadeequiv;
    @Column(name="DESCPRODUNIDADEEQUIV", nullable=false, length=250)
    private String descprodunidadeequiv;
    @Column(name="EQUIVALENCIA", nullable=false, precision=15, scale=10)
    private BigDecimal equivalencia;
    @OneToMany(mappedBy="produnidadeequiv")
    private Set<Prodembalagem> prodembalagem;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODUNIDADE", nullable=false)
    private Produnidade produnidade;

    /** Default constructor. */
    public Produnidadeequiv() {
        super();
    }

    /**
     * Access method for codprodunidadeequiv.
     *
     * @return the current value of codprodunidadeequiv
     */
    public String getCodprodunidadeequiv() {
        return codprodunidadeequiv;
    }

    /**
     * Setter method for codprodunidadeequiv.
     *
     * @param aCodprodunidadeequiv the new value for codprodunidadeequiv
     */
    public void setCodprodunidadeequiv(String aCodprodunidadeequiv) {
        codprodunidadeequiv = aCodprodunidadeequiv;
    }

    /**
     * Access method for descprodunidadeequiv.
     *
     * @return the current value of descprodunidadeequiv
     */
    public String getDescprodunidadeequiv() {
        return descprodunidadeequiv;
    }

    /**
     * Setter method for descprodunidadeequiv.
     *
     * @param aDescprodunidadeequiv the new value for descprodunidadeequiv
     */
    public void setDescprodunidadeequiv(String aDescprodunidadeequiv) {
        descprodunidadeequiv = aDescprodunidadeequiv;
    }

    /**
     * Access method for equivalencia.
     *
     * @return the current value of equivalencia
     */
    public BigDecimal getEquivalencia() {
        return equivalencia;
    }

    /**
     * Setter method for equivalencia.
     *
     * @param aEquivalencia the new value for equivalencia
     */
    public void setEquivalencia(BigDecimal aEquivalencia) {
        equivalencia = aEquivalencia;
    }

    /**
     * Access method for prodembalagem.
     *
     * @return the current value of prodembalagem
     */
    public Set<Prodembalagem> getProdembalagem() {
        return prodembalagem;
    }

    /**
     * Setter method for prodembalagem.
     *
     * @param aProdembalagem the new value for prodembalagem
     */
    public void setProdembalagem(Set<Prodembalagem> aProdembalagem) {
        prodembalagem = aProdembalagem;
    }

    /**
     * Access method for produnidade.
     *
     * @return the current value of produnidade
     */
    public Produnidade getProdunidade() {
        return produnidade;
    }

    /**
     * Setter method for produnidade.
     *
     * @param aProdunidade the new value for produnidade
     */
    public void setProdunidade(Produnidade aProdunidade) {
        produnidade = aProdunidade;
    }

    /**
     * Compares the key for this instance with another Produnidadeequiv.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Produnidadeequiv and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Produnidadeequiv)) {
            return false;
        }
        Produnidadeequiv that = (Produnidadeequiv) other;
        Object myCodprodunidadeequiv = this.getCodprodunidadeequiv();
        Object yourCodprodunidadeequiv = that.getCodprodunidadeequiv();
        if (myCodprodunidadeequiv==null ? yourCodprodunidadeequiv!=null : !myCodprodunidadeequiv.equals(yourCodprodunidadeequiv)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Produnidadeequiv.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Produnidadeequiv)) return false;
        return this.equalKeys(other) && ((Produnidadeequiv)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodprodunidadeequiv() == null) {
            i = 0;
        } else {
            i = getCodprodunidadeequiv().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Produnidadeequiv |");
        sb.append(" codprodunidadeequiv=").append(getCodprodunidadeequiv());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprodunidadeequiv", getCodprodunidadeequiv());
        return ret;
    }

}
