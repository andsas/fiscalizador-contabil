package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="PROJPROJETO")
public class Projprojeto implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprojprojeto";

    @Id
    @Column(name="CODPROJPROJETO", unique=true, nullable=false, precision=10)
    private int codprojprojeto;
    @Column(name="DESCPROJPROJETO", nullable=false, length=250)
    private String descprojprojeto;
    @Column(name="DATAINICIO", nullable=false)
    private Timestamp datainicio;
    @Column(name="DATAFINAL")
    private Timestamp datafinal;
    @Column(name="OBS")
    private String obs;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPROJTIPO", nullable=false)
    private Projtipo projtipo;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIORESP", nullable=false)
    private Confusuario confusuario;
    @OneToMany(mappedBy="projprojeto")
    private Set<Projprojetodet> projprojetodet;
    @OneToMany(mappedBy="projprojeto")
    private Set<Projprojetousuario> projprojetousuario;

    /** Default constructor. */
    public Projprojeto() {
        super();
    }

    /**
     * Access method for codprojprojeto.
     *
     * @return the current value of codprojprojeto
     */
    public int getCodprojprojeto() {
        return codprojprojeto;
    }

    /**
     * Setter method for codprojprojeto.
     *
     * @param aCodprojprojeto the new value for codprojprojeto
     */
    public void setCodprojprojeto(int aCodprojprojeto) {
        codprojprojeto = aCodprojprojeto;
    }

    /**
     * Access method for descprojprojeto.
     *
     * @return the current value of descprojprojeto
     */
    public String getDescprojprojeto() {
        return descprojprojeto;
    }

    /**
     * Setter method for descprojprojeto.
     *
     * @param aDescprojprojeto the new value for descprojprojeto
     */
    public void setDescprojprojeto(String aDescprojprojeto) {
        descprojprojeto = aDescprojprojeto;
    }

    /**
     * Access method for datainicio.
     *
     * @return the current value of datainicio
     */
    public Timestamp getDatainicio() {
        return datainicio;
    }

    /**
     * Setter method for datainicio.
     *
     * @param aDatainicio the new value for datainicio
     */
    public void setDatainicio(Timestamp aDatainicio) {
        datainicio = aDatainicio;
    }

    /**
     * Access method for datafinal.
     *
     * @return the current value of datafinal
     */
    public Timestamp getDatafinal() {
        return datafinal;
    }

    /**
     * Setter method for datafinal.
     *
     * @param aDatafinal the new value for datafinal
     */
    public void setDatafinal(Timestamp aDatafinal) {
        datafinal = aDatafinal;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for projtipo.
     *
     * @return the current value of projtipo
     */
    public Projtipo getProjtipo() {
        return projtipo;
    }

    /**
     * Setter method for projtipo.
     *
     * @param aProjtipo the new value for projtipo
     */
    public void setProjtipo(Projtipo aProjtipo) {
        projtipo = aProjtipo;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for projprojetodet.
     *
     * @return the current value of projprojetodet
     */
    public Set<Projprojetodet> getProjprojetodet() {
        return projprojetodet;
    }

    /**
     * Setter method for projprojetodet.
     *
     * @param aProjprojetodet the new value for projprojetodet
     */
    public void setProjprojetodet(Set<Projprojetodet> aProjprojetodet) {
        projprojetodet = aProjprojetodet;
    }

    /**
     * Access method for projprojetousuario.
     *
     * @return the current value of projprojetousuario
     */
    public Set<Projprojetousuario> getProjprojetousuario() {
        return projprojetousuario;
    }

    /**
     * Setter method for projprojetousuario.
     *
     * @param aProjprojetousuario the new value for projprojetousuario
     */
    public void setProjprojetousuario(Set<Projprojetousuario> aProjprojetousuario) {
        projprojetousuario = aProjprojetousuario;
    }

    /**
     * Compares the key for this instance with another Projprojeto.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Projprojeto and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Projprojeto)) {
            return false;
        }
        Projprojeto that = (Projprojeto) other;
        if (this.getCodprojprojeto() != that.getCodprojprojeto()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Projprojeto.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Projprojeto)) return false;
        return this.equalKeys(other) && ((Projprojeto)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprojprojeto();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Projprojeto |");
        sb.append(" codprojprojeto=").append(getCodprojprojeto());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprojprojeto", Integer.valueOf(getCodprojprojeto()));
        return ret;
    }

}
