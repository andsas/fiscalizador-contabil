package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PROJPROJETODET")
public class Projprojetodet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprojprojetodet";

    @Id
    @Column(name="CODPROJPROJETODET", unique=true, nullable=false, precision=10)
    private int codprojprojetodet;
    @Column(name="DESCPROJPROJETODET", nullable=false, length=250)
    private String descprojprojetodet;
    @Column(name="DATAINICIO", nullable=false)
    private Timestamp datainicio;
    @Column(name="DATAFINAL")
    private Timestamp datafinal;
    @Column(name="OBS")
    private String obs;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPROJPROJETO", nullable=false)
    private Projprojeto projprojeto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIORESP", nullable=false)
    private Confusuario confusuario;

    /** Default constructor. */
    public Projprojetodet() {
        super();
    }

    /**
     * Access method for codprojprojetodet.
     *
     * @return the current value of codprojprojetodet
     */
    public int getCodprojprojetodet() {
        return codprojprojetodet;
    }

    /**
     * Setter method for codprojprojetodet.
     *
     * @param aCodprojprojetodet the new value for codprojprojetodet
     */
    public void setCodprojprojetodet(int aCodprojprojetodet) {
        codprojprojetodet = aCodprojprojetodet;
    }

    /**
     * Access method for descprojprojetodet.
     *
     * @return the current value of descprojprojetodet
     */
    public String getDescprojprojetodet() {
        return descprojprojetodet;
    }

    /**
     * Setter method for descprojprojetodet.
     *
     * @param aDescprojprojetodet the new value for descprojprojetodet
     */
    public void setDescprojprojetodet(String aDescprojprojetodet) {
        descprojprojetodet = aDescprojprojetodet;
    }

    /**
     * Access method for datainicio.
     *
     * @return the current value of datainicio
     */
    public Timestamp getDatainicio() {
        return datainicio;
    }

    /**
     * Setter method for datainicio.
     *
     * @param aDatainicio the new value for datainicio
     */
    public void setDatainicio(Timestamp aDatainicio) {
        datainicio = aDatainicio;
    }

    /**
     * Access method for datafinal.
     *
     * @return the current value of datafinal
     */
    public Timestamp getDatafinal() {
        return datafinal;
    }

    /**
     * Setter method for datafinal.
     *
     * @param aDatafinal the new value for datafinal
     */
    public void setDatafinal(Timestamp aDatafinal) {
        datafinal = aDatafinal;
    }

    /**
     * Access method for obs.
     *
     * @return the current value of obs
     */
    public String getObs() {
        return obs;
    }

    /**
     * Setter method for obs.
     *
     * @param aObs the new value for obs
     */
    public void setObs(String aObs) {
        obs = aObs;
    }

    /**
     * Access method for projprojeto.
     *
     * @return the current value of projprojeto
     */
    public Projprojeto getProjprojeto() {
        return projprojeto;
    }

    /**
     * Setter method for projprojeto.
     *
     * @param aProjprojeto the new value for projprojeto
     */
    public void setProjprojeto(Projprojeto aProjprojeto) {
        projprojeto = aProjprojeto;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Compares the key for this instance with another Projprojetodet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Projprojetodet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Projprojetodet)) {
            return false;
        }
        Projprojetodet that = (Projprojetodet) other;
        if (this.getCodprojprojetodet() != that.getCodprojprojetodet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Projprojetodet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Projprojetodet)) return false;
        return this.equalKeys(other) && ((Projprojetodet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprojprojetodet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Projprojetodet |");
        sb.append(" codprojprojetodet=").append(getCodprojprojetodet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprojprojetodet", Integer.valueOf(getCodprojprojetodet()));
        return ret;
    }

}
