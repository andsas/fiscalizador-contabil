package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PROJPROJETOUSUARIO")
public class Projprojetousuario implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprojprojetousuario";

    @Id
    @Column(name="CODPROJPROJETOUSUARIO", unique=true, nullable=false, precision=10)
    private int codprojprojetousuario;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPROJPROJETO", nullable=false)
    private Projprojeto projprojeto;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFUSUARIO", nullable=false)
    private Confusuario confusuario;

    /** Default constructor. */
    public Projprojetousuario() {
        super();
    }

    /**
     * Access method for codprojprojetousuario.
     *
     * @return the current value of codprojprojetousuario
     */
    public int getCodprojprojetousuario() {
        return codprojprojetousuario;
    }

    /**
     * Setter method for codprojprojetousuario.
     *
     * @param aCodprojprojetousuario the new value for codprojprojetousuario
     */
    public void setCodprojprojetousuario(int aCodprojprojetousuario) {
        codprojprojetousuario = aCodprojprojetousuario;
    }

    /**
     * Access method for projprojeto.
     *
     * @return the current value of projprojeto
     */
    public Projprojeto getProjprojeto() {
        return projprojeto;
    }

    /**
     * Setter method for projprojeto.
     *
     * @param aProjprojeto the new value for projprojeto
     */
    public void setProjprojeto(Projprojeto aProjprojeto) {
        projprojeto = aProjprojeto;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Compares the key for this instance with another Projprojetousuario.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Projprojetousuario and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Projprojetousuario)) {
            return false;
        }
        Projprojetousuario that = (Projprojetousuario) other;
        if (this.getCodprojprojetousuario() != that.getCodprojprojetousuario()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Projprojetousuario.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Projprojetousuario)) return false;
        return this.equalKeys(other) && ((Projprojetousuario)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprojprojetousuario();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Projprojetousuario |");
        sb.append(" codprojprojetousuario=").append(getCodprojprojetousuario());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprojprojetousuario", Integer.valueOf(getCodprojprojetousuario()));
        return ret;
    }

}
