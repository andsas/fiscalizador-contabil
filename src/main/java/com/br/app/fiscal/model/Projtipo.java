package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PROJTIPO")
public class Projtipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprojtipo";

    @Id
    @Column(name="CODPROJTIPO", unique=true, nullable=false, precision=10)
    private int codprojtipo;
    @Column(name="DESCPROJTIPO", nullable=false, length=250)
    private String descprojtipo;
    @OneToMany(mappedBy="projtipo")
    private Set<Projprojeto> projprojeto;

    /** Default constructor. */
    public Projtipo() {
        super();
    }

    /**
     * Access method for codprojtipo.
     *
     * @return the current value of codprojtipo
     */
    public int getCodprojtipo() {
        return codprojtipo;
    }

    /**
     * Setter method for codprojtipo.
     *
     * @param aCodprojtipo the new value for codprojtipo
     */
    public void setCodprojtipo(int aCodprojtipo) {
        codprojtipo = aCodprojtipo;
    }

    /**
     * Access method for descprojtipo.
     *
     * @return the current value of descprojtipo
     */
    public String getDescprojtipo() {
        return descprojtipo;
    }

    /**
     * Setter method for descprojtipo.
     *
     * @param aDescprojtipo the new value for descprojtipo
     */
    public void setDescprojtipo(String aDescprojtipo) {
        descprojtipo = aDescprojtipo;
    }

    /**
     * Access method for projprojeto.
     *
     * @return the current value of projprojeto
     */
    public Set<Projprojeto> getProjprojeto() {
        return projprojeto;
    }

    /**
     * Setter method for projprojeto.
     *
     * @param aProjprojeto the new value for projprojeto
     */
    public void setProjprojeto(Set<Projprojeto> aProjprojeto) {
        projprojeto = aProjprojeto;
    }

    /**
     * Compares the key for this instance with another Projtipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Projtipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Projtipo)) {
            return false;
        }
        Projtipo that = (Projtipo) other;
        if (this.getCodprojtipo() != that.getCodprojtipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Projtipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Projtipo)) return false;
        return this.equalKeys(other) && ((Projtipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprojtipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Projtipo |");
        sb.append(" codprojtipo=").append(getCodprojtipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprojtipo", Integer.valueOf(getCodprojtipo()));
        return ret;
    }

}
