package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Date;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="PRTABELA")
public class Prtabela implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprtabela";

    @Id
    @Column(name="CODPRTABELA", unique=true, nullable=false, precision=10)
    private int codprtabela;
    @Column(name="DESCPRTABELA", nullable=false, length=250)
    private String descprtabela;
    @Column(name="FORMULA")
    private String formula;
    @Column(name="DATAVALIDADE")
    private Date datavalidade;
    @Column(name="PRODPRECOFIXO", precision=5)
    private short prodprecofixo;
    @OneToMany(mappedBy="prtabela")
    private Set<Agagente> agagente;
    @OneToMany(mappedBy="prtabela")
    private Set<Optransacao> optransacao;
    @OneToMany(mappedBy="prtabela")
    private Set<Prtabeladet> prtabeladet;

    /** Default constructor. */
    public Prtabela() {
        super();
    }

    /**
     * Access method for codprtabela.
     *
     * @return the current value of codprtabela
     */
    public int getCodprtabela() {
        return codprtabela;
    }

    /**
     * Setter method for codprtabela.
     *
     * @param aCodprtabela the new value for codprtabela
     */
    public void setCodprtabela(int aCodprtabela) {
        codprtabela = aCodprtabela;
    }

    /**
     * Access method for descprtabela.
     *
     * @return the current value of descprtabela
     */
    public String getDescprtabela() {
        return descprtabela;
    }

    /**
     * Setter method for descprtabela.
     *
     * @param aDescprtabela the new value for descprtabela
     */
    public void setDescprtabela(String aDescprtabela) {
        descprtabela = aDescprtabela;
    }

    /**
     * Access method for formula.
     *
     * @return the current value of formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Setter method for formula.
     *
     * @param aFormula the new value for formula
     */
    public void setFormula(String aFormula) {
        formula = aFormula;
    }

    /**
     * Access method for datavalidade.
     *
     * @return the current value of datavalidade
     */
    public Date getDatavalidade() {
        return datavalidade;
    }

    /**
     * Setter method for datavalidade.
     *
     * @param aDatavalidade the new value for datavalidade
     */
    public void setDatavalidade(Date aDatavalidade) {
        datavalidade = aDatavalidade;
    }

    /**
     * Access method for prodprecofixo.
     *
     * @return the current value of prodprecofixo
     */
    public short getProdprecofixo() {
        return prodprecofixo;
    }

    /**
     * Setter method for prodprecofixo.
     *
     * @param aProdprecofixo the new value for prodprecofixo
     */
    public void setProdprecofixo(short aProdprecofixo) {
        prodprecofixo = aProdprecofixo;
    }

    /**
     * Access method for agagente.
     *
     * @return the current value of agagente
     */
    public Set<Agagente> getAgagente() {
        return agagente;
    }

    /**
     * Setter method for agagente.
     *
     * @param aAgagente the new value for agagente
     */
    public void setAgagente(Set<Agagente> aAgagente) {
        agagente = aAgagente;
    }

    /**
     * Access method for optransacao.
     *
     * @return the current value of optransacao
     */
    public Set<Optransacao> getOptransacao() {
        return optransacao;
    }

    /**
     * Setter method for optransacao.
     *
     * @param aOptransacao the new value for optransacao
     */
    public void setOptransacao(Set<Optransacao> aOptransacao) {
        optransacao = aOptransacao;
    }

    /**
     * Access method for prtabeladet.
     *
     * @return the current value of prtabeladet
     */
    public Set<Prtabeladet> getPrtabeladet() {
        return prtabeladet;
    }

    /**
     * Setter method for prtabeladet.
     *
     * @param aPrtabeladet the new value for prtabeladet
     */
    public void setPrtabeladet(Set<Prtabeladet> aPrtabeladet) {
        prtabeladet = aPrtabeladet;
    }

    /**
     * Compares the key for this instance with another Prtabela.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prtabela and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prtabela)) {
            return false;
        }
        Prtabela that = (Prtabela) other;
        if (this.getCodprtabela() != that.getCodprtabela()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prtabela.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prtabela)) return false;
        return this.equalKeys(other) && ((Prtabela)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprtabela();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prtabela |");
        sb.append(" codprtabela=").append(getCodprtabela());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprtabela", Integer.valueOf(getCodprtabela()));
        return ret;
    }

}
