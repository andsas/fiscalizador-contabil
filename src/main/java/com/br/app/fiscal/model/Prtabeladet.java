package com.br.app.fiscal.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="PRTABELADET")
public class Prtabeladet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codprtabeladet";

    @Id
    @Column(name="CODPRTABELADET", unique=true, nullable=false, precision=10)
    private int codprtabeladet;
    @Column(name="PRECO", precision=15, scale=4)
    private BigDecimal preco;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRTABELA", nullable=false)
    private Prtabela prtabela;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODPRODPRODUTO", nullable=false)
    private Prodproduto prodproduto;

    /** Default constructor. */
    public Prtabeladet() {
        super();
    }

    /**
     * Access method for codprtabeladet.
     *
     * @return the current value of codprtabeladet
     */
    public int getCodprtabeladet() {
        return codprtabeladet;
    }

    /**
     * Setter method for codprtabeladet.
     *
     * @param aCodprtabeladet the new value for codprtabeladet
     */
    public void setCodprtabeladet(int aCodprtabeladet) {
        codprtabeladet = aCodprtabeladet;
    }

    /**
     * Access method for preco.
     *
     * @return the current value of preco
     */
    public BigDecimal getPreco() {
        return preco;
    }

    /**
     * Setter method for preco.
     *
     * @param aPreco the new value for preco
     */
    public void setPreco(BigDecimal aPreco) {
        preco = aPreco;
    }

    /**
     * Access method for prtabela.
     *
     * @return the current value of prtabela
     */
    public Prtabela getPrtabela() {
        return prtabela;
    }

    /**
     * Setter method for prtabela.
     *
     * @param aPrtabela the new value for prtabela
     */
    public void setPrtabela(Prtabela aPrtabela) {
        prtabela = aPrtabela;
    }

    /**
     * Access method for prodproduto.
     *
     * @return the current value of prodproduto
     */
    public Prodproduto getProdproduto() {
        return prodproduto;
    }

    /**
     * Setter method for prodproduto.
     *
     * @param aProdproduto the new value for prodproduto
     */
    public void setProdproduto(Prodproduto aProdproduto) {
        prodproduto = aProdproduto;
    }

    /**
     * Compares the key for this instance with another Prtabeladet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Prtabeladet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Prtabeladet)) {
            return false;
        }
        Prtabeladet that = (Prtabeladet) other;
        if (this.getCodprtabeladet() != that.getCodprtabeladet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Prtabeladet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Prtabeladet)) return false;
        return this.equalKeys(other) && ((Prtabeladet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodprtabeladet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Prtabeladet |");
        sb.append(" codprtabeladet=").append(getCodprtabeladet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codprtabeladet", Integer.valueOf(getCodprtabeladet()));
        return ret;
    }

}
