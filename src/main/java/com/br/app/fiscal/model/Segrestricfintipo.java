package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="SEGRESTRICFINTIPO")
public class Segrestricfintipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codsegrestricfintipo";

    @Id
    @Column(name="CODSEGRESTRICFINTIPO", unique=true, nullable=false, precision=10)
    private int codsegrestricfintipo;
    @Column(name="CODFINTIPO", nullable=false, precision=10)
    private int codfintipo;
    @Column(name="TIPORESTRICAO", precision=5)
    private short tiporestricao;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIO")
    private Confusuario confusuario;
    @ManyToOne
    @JoinColumn(name="CODCONFTIPOUSUARIO")
    private Confusuariotipo confusuariotipo;
    @ManyToOne
    @JoinColumn(name="CODCONFALCADAUSUARIO")
    private Confusuarioalcada confusuarioalcada;

    /** Default constructor. */
    public Segrestricfintipo() {
        super();
    }

    /**
     * Access method for codsegrestricfintipo.
     *
     * @return the current value of codsegrestricfintipo
     */
    public int getCodsegrestricfintipo() {
        return codsegrestricfintipo;
    }

    /**
     * Setter method for codsegrestricfintipo.
     *
     * @param aCodsegrestricfintipo the new value for codsegrestricfintipo
     */
    public void setCodsegrestricfintipo(int aCodsegrestricfintipo) {
        codsegrestricfintipo = aCodsegrestricfintipo;
    }

    /**
     * Access method for codfintipo.
     *
     * @return the current value of codfintipo
     */
    public int getCodfintipo() {
        return codfintipo;
    }

    /**
     * Setter method for codfintipo.
     *
     * @param aCodfintipo the new value for codfintipo
     */
    public void setCodfintipo(int aCodfintipo) {
        codfintipo = aCodfintipo;
    }

    /**
     * Access method for tiporestricao.
     *
     * @return the current value of tiporestricao
     */
    public short getTiporestricao() {
        return tiporestricao;
    }

    /**
     * Setter method for tiporestricao.
     *
     * @param aTiporestricao the new value for tiporestricao
     */
    public void setTiporestricao(short aTiporestricao) {
        tiporestricao = aTiporestricao;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for confusuariotipo.
     *
     * @return the current value of confusuariotipo
     */
    public Confusuariotipo getConfusuariotipo() {
        return confusuariotipo;
    }

    /**
     * Setter method for confusuariotipo.
     *
     * @param aConfusuariotipo the new value for confusuariotipo
     */
    public void setConfusuariotipo(Confusuariotipo aConfusuariotipo) {
        confusuariotipo = aConfusuariotipo;
    }

    /**
     * Access method for confusuarioalcada.
     *
     * @return the current value of confusuarioalcada
     */
    public Confusuarioalcada getConfusuarioalcada() {
        return confusuarioalcada;
    }

    /**
     * Setter method for confusuarioalcada.
     *
     * @param aConfusuarioalcada the new value for confusuarioalcada
     */
    public void setConfusuarioalcada(Confusuarioalcada aConfusuarioalcada) {
        confusuarioalcada = aConfusuarioalcada;
    }

    /**
     * Compares the key for this instance with another Segrestricfintipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Segrestricfintipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Segrestricfintipo)) {
            return false;
        }
        Segrestricfintipo that = (Segrestricfintipo) other;
        if (this.getCodsegrestricfintipo() != that.getCodsegrestricfintipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Segrestricfintipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Segrestricfintipo)) return false;
        return this.equalKeys(other) && ((Segrestricfintipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodsegrestricfintipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Segrestricfintipo |");
        sb.append(" codsegrestricfintipo=").append(getCodsegrestricfintipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codsegrestricfintipo", Integer.valueOf(getCodsegrestricfintipo()));
        return ret;
    }

}
