package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="SEGRESTRICMENU")
public class Segrestricmenu implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codsegrestricmenu";

    @Id
    @Column(name="CODSEGRESTRICMENU", unique=true, nullable=false, precision=10)
    private int codsegrestricmenu;
    @Column(name="MENURESTRIC")
    private String menurestric;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIO")
    private Confusuario confusuario;
    @ManyToOne
    @JoinColumn(name="CODCONFTIPOUSUARIO")
    private Confusuariotipo confusuariotipo;
    @ManyToOne
    @JoinColumn(name="CODCONFALCADAUSUARIO")
    private Confusuarioalcada confusuarioalcada;

    /** Default constructor. */
    public Segrestricmenu() {
        super();
    }

    /**
     * Access method for codsegrestricmenu.
     *
     * @return the current value of codsegrestricmenu
     */
    public int getCodsegrestricmenu() {
        return codsegrestricmenu;
    }

    /**
     * Setter method for codsegrestricmenu.
     *
     * @param aCodsegrestricmenu the new value for codsegrestricmenu
     */
    public void setCodsegrestricmenu(int aCodsegrestricmenu) {
        codsegrestricmenu = aCodsegrestricmenu;
    }

    /**
     * Access method for menurestric.
     *
     * @return the current value of menurestric
     */
    public String getMenurestric() {
        return menurestric;
    }

    /**
     * Setter method for menurestric.
     *
     * @param aMenurestric the new value for menurestric
     */
    public void setMenurestric(String aMenurestric) {
        menurestric = aMenurestric;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for confusuariotipo.
     *
     * @return the current value of confusuariotipo
     */
    public Confusuariotipo getConfusuariotipo() {
        return confusuariotipo;
    }

    /**
     * Setter method for confusuariotipo.
     *
     * @param aConfusuariotipo the new value for confusuariotipo
     */
    public void setConfusuariotipo(Confusuariotipo aConfusuariotipo) {
        confusuariotipo = aConfusuariotipo;
    }

    /**
     * Access method for confusuarioalcada.
     *
     * @return the current value of confusuarioalcada
     */
    public Confusuarioalcada getConfusuarioalcada() {
        return confusuarioalcada;
    }

    /**
     * Setter method for confusuarioalcada.
     *
     * @param aConfusuarioalcada the new value for confusuarioalcada
     */
    public void setConfusuarioalcada(Confusuarioalcada aConfusuarioalcada) {
        confusuarioalcada = aConfusuarioalcada;
    }

    /**
     * Compares the key for this instance with another Segrestricmenu.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Segrestricmenu and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Segrestricmenu)) {
            return false;
        }
        Segrestricmenu that = (Segrestricmenu) other;
        if (this.getCodsegrestricmenu() != that.getCodsegrestricmenu()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Segrestricmenu.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Segrestricmenu)) return false;
        return this.equalKeys(other) && ((Segrestricmenu)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodsegrestricmenu();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Segrestricmenu |");
        sb.append(" codsegrestricmenu=").append(getCodsegrestricmenu());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codsegrestricmenu", Integer.valueOf(getCodsegrestricmenu()));
        return ret;
    }

}
