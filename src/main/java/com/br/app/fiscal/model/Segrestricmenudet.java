package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="SEGRESTRICMENUDET")
public class Segrestricmenudet implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codsegrestricmenudet";

    @Id
    @Column(name="CODSEGRESTRICMENUDET", unique=true, nullable=false, precision=10)
    private int codsegrestricmenudet;
    @Column(name="CODSEGRESTRICMENU", nullable=false, precision=10)
    private int codsegrestricmenu;
    @Column(name="CODCONFMENU", nullable=false, length=40)
    private String codconfmenu;
    @Column(name="TIPORESTRICAO", precision=5)
    private short tiporestricao;

    /** Default constructor. */
    public Segrestricmenudet() {
        super();
    }

    /**
     * Access method for codsegrestricmenudet.
     *
     * @return the current value of codsegrestricmenudet
     */
    public int getCodsegrestricmenudet() {
        return codsegrestricmenudet;
    }

    /**
     * Setter method for codsegrestricmenudet.
     *
     * @param aCodsegrestricmenudet the new value for codsegrestricmenudet
     */
    public void setCodsegrestricmenudet(int aCodsegrestricmenudet) {
        codsegrestricmenudet = aCodsegrestricmenudet;
    }

    /**
     * Access method for codsegrestricmenu.
     *
     * @return the current value of codsegrestricmenu
     */
    public int getCodsegrestricmenu() {
        return codsegrestricmenu;
    }

    /**
     * Setter method for codsegrestricmenu.
     *
     * @param aCodsegrestricmenu the new value for codsegrestricmenu
     */
    public void setCodsegrestricmenu(int aCodsegrestricmenu) {
        codsegrestricmenu = aCodsegrestricmenu;
    }

    /**
     * Access method for codconfmenu.
     *
     * @return the current value of codconfmenu
     */
    public String getCodconfmenu() {
        return codconfmenu;
    }

    /**
     * Setter method for codconfmenu.
     *
     * @param aCodconfmenu the new value for codconfmenu
     */
    public void setCodconfmenu(String aCodconfmenu) {
        codconfmenu = aCodconfmenu;
    }

    /**
     * Access method for tiporestricao.
     *
     * @return the current value of tiporestricao
     */
    public short getTiporestricao() {
        return tiporestricao;
    }

    /**
     * Setter method for tiporestricao.
     *
     * @param aTiporestricao the new value for tiporestricao
     */
    public void setTiporestricao(short aTiporestricao) {
        tiporestricao = aTiporestricao;
    }

    /**
     * Compares the key for this instance with another Segrestricmenudet.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Segrestricmenudet and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Segrestricmenudet)) {
            return false;
        }
        Segrestricmenudet that = (Segrestricmenudet) other;
        if (this.getCodsegrestricmenudet() != that.getCodsegrestricmenudet()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Segrestricmenudet.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Segrestricmenudet)) return false;
        return this.equalKeys(other) && ((Segrestricmenudet)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodsegrestricmenudet();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Segrestricmenudet |");
        sb.append(" codsegrestricmenudet=").append(getCodsegrestricmenudet());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codsegrestricmenudet", Integer.valueOf(getCodsegrestricmenudet()));
        return ret;
    }

}
