package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="SEGRESTRICOPTIPO")
public class Segrestricoptipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codsegrestricoptipo";

    @Id
    @Column(name="CODSEGRESTRICOPTIPO", unique=true, nullable=false, precision=10)
    private int codsegrestricoptipo;
    @Column(name="CODOPTIPO", nullable=false, precision=10)
    private int codoptipo;
    @Column(name="TIPORESTRICAO", precision=5)
    private short tiporestricao;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIO")
    private Confusuario confusuario;
    @ManyToOne
    @JoinColumn(name="CODCONFTIPOUSUARIO")
    private Confusuariotipo confusuariotipo;
    @ManyToOne
    @JoinColumn(name="CODCONFALCADAUSUARIO")
    private Confusuarioalcada confusuarioalcada;

    /** Default constructor. */
    public Segrestricoptipo() {
        super();
    }

    /**
     * Access method for codsegrestricoptipo.
     *
     * @return the current value of codsegrestricoptipo
     */
    public int getCodsegrestricoptipo() {
        return codsegrestricoptipo;
    }

    /**
     * Setter method for codsegrestricoptipo.
     *
     * @param aCodsegrestricoptipo the new value for codsegrestricoptipo
     */
    public void setCodsegrestricoptipo(int aCodsegrestricoptipo) {
        codsegrestricoptipo = aCodsegrestricoptipo;
    }

    /**
     * Access method for codoptipo.
     *
     * @return the current value of codoptipo
     */
    public int getCodoptipo() {
        return codoptipo;
    }

    /**
     * Setter method for codoptipo.
     *
     * @param aCodoptipo the new value for codoptipo
     */
    public void setCodoptipo(int aCodoptipo) {
        codoptipo = aCodoptipo;
    }

    /**
     * Access method for tiporestricao.
     *
     * @return the current value of tiporestricao
     */
    public short getTiporestricao() {
        return tiporestricao;
    }

    /**
     * Setter method for tiporestricao.
     *
     * @param aTiporestricao the new value for tiporestricao
     */
    public void setTiporestricao(short aTiporestricao) {
        tiporestricao = aTiporestricao;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for confusuariotipo.
     *
     * @return the current value of confusuariotipo
     */
    public Confusuariotipo getConfusuariotipo() {
        return confusuariotipo;
    }

    /**
     * Setter method for confusuariotipo.
     *
     * @param aConfusuariotipo the new value for confusuariotipo
     */
    public void setConfusuariotipo(Confusuariotipo aConfusuariotipo) {
        confusuariotipo = aConfusuariotipo;
    }

    /**
     * Access method for confusuarioalcada.
     *
     * @return the current value of confusuarioalcada
     */
    public Confusuarioalcada getConfusuarioalcada() {
        return confusuarioalcada;
    }

    /**
     * Setter method for confusuarioalcada.
     *
     * @param aConfusuarioalcada the new value for confusuarioalcada
     */
    public void setConfusuarioalcada(Confusuarioalcada aConfusuarioalcada) {
        confusuarioalcada = aConfusuarioalcada;
    }

    /**
     * Compares the key for this instance with another Segrestricoptipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Segrestricoptipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Segrestricoptipo)) {
            return false;
        }
        Segrestricoptipo that = (Segrestricoptipo) other;
        if (this.getCodsegrestricoptipo() != that.getCodsegrestricoptipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Segrestricoptipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Segrestricoptipo)) return false;
        return this.equalKeys(other) && ((Segrestricoptipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodsegrestricoptipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Segrestricoptipo |");
        sb.append(" codsegrestricoptipo=").append(getCodsegrestricoptipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codsegrestricoptipo", Integer.valueOf(getCodsegrestricoptipo()));
        return ret;
    }

}
