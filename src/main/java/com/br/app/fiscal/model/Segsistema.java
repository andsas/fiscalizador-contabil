package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="SEGSISTEMA")
public class Segsistema implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codsegsistema";

    @Id
    @Column(name="CODSEGSISTEMA", unique=true, nullable=false, precision=10)
    private int codsegsistema;
    @Column(name="CODVERVERSAO", nullable=false, length=20)
    private String codverversao;

    /** Default constructor. */
    public Segsistema() {
        super();
    }

    /**
     * Access method for codsegsistema.
     *
     * @return the current value of codsegsistema
     */
    public int getCodsegsistema() {
        return codsegsistema;
    }

    /**
     * Setter method for codsegsistema.
     *
     * @param aCodsegsistema the new value for codsegsistema
     */
    public void setCodsegsistema(int aCodsegsistema) {
        codsegsistema = aCodsegsistema;
    }

    /**
     * Access method for codverversao.
     *
     * @return the current value of codverversao
     */
    public String getCodverversao() {
        return codverversao;
    }

    /**
     * Setter method for codverversao.
     *
     * @param aCodverversao the new value for codverversao
     */
    public void setCodverversao(String aCodverversao) {
        codverversao = aCodverversao;
    }

    /**
     * Compares the key for this instance with another Segsistema.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Segsistema and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Segsistema)) {
            return false;
        }
        Segsistema that = (Segsistema) other;
        if (this.getCodsegsistema() != that.getCodsegsistema()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Segsistema.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Segsistema)) return false;
        return this.equalKeys(other) && ((Segsistema)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodsegsistema();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Segsistema |");
        sb.append(" codsegsistema=").append(getCodsegsistema());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codsegsistema", Integer.valueOf(getCodsegsistema()));
        return ret;
    }

}
