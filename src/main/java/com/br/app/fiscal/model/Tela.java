package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="TELA")
public class Tela implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codtela";

    @Id
    @Column(name="CODTELA", unique=true, nullable=false, precision=10)
    private int codtela;
    @Column(name="DESCTELA", nullable=false, length=250)
    private String desctela;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODTIPOTELA", nullable=false)
    private Telatipo telatipo;

    /** Default constructor. */
    public Tela() {
        super();
    }

    /**
     * Access method for codtela.
     *
     * @return the current value of codtela
     */
    public int getCodtela() {
        return codtela;
    }

    /**
     * Setter method for codtela.
     *
     * @param aCodtela the new value for codtela
     */
    public void setCodtela(int aCodtela) {
        codtela = aCodtela;
    }

    /**
     * Access method for desctela.
     *
     * @return the current value of desctela
     */
    public String getDesctela() {
        return desctela;
    }

    /**
     * Setter method for desctela.
     *
     * @param aDesctela the new value for desctela
     */
    public void setDesctela(String aDesctela) {
        desctela = aDesctela;
    }

    /**
     * Access method for telatipo.
     *
     * @return the current value of telatipo
     */
    public Telatipo getTelatipo() {
        return telatipo;
    }

    /**
     * Setter method for telatipo.
     *
     * @param aTelatipo the new value for telatipo
     */
    public void setTelatipo(Telatipo aTelatipo) {
        telatipo = aTelatipo;
    }

    /**
     * Compares the key for this instance with another Tela.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Tela and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Tela)) {
            return false;
        }
        Tela that = (Tela) other;
        if (this.getCodtela() != that.getCodtela()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Tela.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Tela)) return false;
        return this.equalKeys(other) && ((Tela)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodtela();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Tela |");
        sb.append(" codtela=").append(getCodtela());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codtela", Integer.valueOf(getCodtela()));
        return ret;
    }

}
