package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="TELATIPO")
public class Telatipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codtelatipo";

    @Id
    @Column(name="CODTELATIPO", unique=true, nullable=false, precision=10)
    private int codtelatipo;
    @Column(name="DESCTELATIPO", nullable=false, length=250)
    private String desctelatipo;
    @OneToMany(mappedBy="telatipo")
    private Set<Tela> tela;

    /** Default constructor. */
    public Telatipo() {
        super();
    }

    /**
     * Access method for codtelatipo.
     *
     * @return the current value of codtelatipo
     */
    public int getCodtelatipo() {
        return codtelatipo;
    }

    /**
     * Setter method for codtelatipo.
     *
     * @param aCodtelatipo the new value for codtelatipo
     */
    public void setCodtelatipo(int aCodtelatipo) {
        codtelatipo = aCodtelatipo;
    }

    /**
     * Access method for desctelatipo.
     *
     * @return the current value of desctelatipo
     */
    public String getDesctelatipo() {
        return desctelatipo;
    }

    /**
     * Setter method for desctelatipo.
     *
     * @param aDesctelatipo the new value for desctelatipo
     */
    public void setDesctelatipo(String aDesctelatipo) {
        desctelatipo = aDesctelatipo;
    }

    /**
     * Access method for tela.
     *
     * @return the current value of tela
     */
    public Set<Tela> getTela() {
        return tela;
    }

    /**
     * Setter method for tela.
     *
     * @param aTela the new value for tela
     */
    public void setTela(Set<Tela> aTela) {
        tela = aTela;
    }

    /**
     * Compares the key for this instance with another Telatipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Telatipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Telatipo)) {
            return false;
        }
        Telatipo that = (Telatipo) other;
        if (this.getCodtelatipo() != that.getCodtelatipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Telatipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Telatipo)) return false;
        return this.equalKeys(other) && ((Telatipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodtelatipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Telatipo |");
        sb.append(" codtelatipo=").append(getCodtelatipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codtelatipo", Integer.valueOf(getCodtelatipo()));
        return ret;
    }

}
