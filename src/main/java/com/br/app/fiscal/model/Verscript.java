package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="VERSCRIPT")
public class Verscript implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codverscript";

    @Id
    @Column(name="CODVERSCRIPT", unique=true, nullable=false, precision=10)
    private int codverscript;
    @Column(name="DESCVERSCRIPT", nullable=false, length=250)
    private String descverscript;
    @Column(name="SCRIPT", nullable=false)
    private String script;
    @Column(name="DATAATUALIZACAO")
    private Timestamp dataatualizacao;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODVERVERSAO", nullable=false)
    private Verversao verversao;

    /** Default constructor. */
    public Verscript() {
        super();
    }

    /**
     * Access method for codverscript.
     *
     * @return the current value of codverscript
     */
    public int getCodverscript() {
        return codverscript;
    }

    /**
     * Setter method for codverscript.
     *
     * @param aCodverscript the new value for codverscript
     */
    public void setCodverscript(int aCodverscript) {
        codverscript = aCodverscript;
    }

    /**
     * Access method for descverscript.
     *
     * @return the current value of descverscript
     */
    public String getDescverscript() {
        return descverscript;
    }

    /**
     * Setter method for descverscript.
     *
     * @param aDescverscript the new value for descverscript
     */
    public void setDescverscript(String aDescverscript) {
        descverscript = aDescverscript;
    }

    /**
     * Access method for script.
     *
     * @return the current value of script
     */
    public String getScript() {
        return script;
    }

    /**
     * Setter method for script.
     *
     * @param aScript the new value for script
     */
    public void setScript(String aScript) {
        script = aScript;
    }

    /**
     * Access method for dataatualizacao.
     *
     * @return the current value of dataatualizacao
     */
    public Timestamp getDataatualizacao() {
        return dataatualizacao;
    }

    /**
     * Setter method for dataatualizacao.
     *
     * @param aDataatualizacao the new value for dataatualizacao
     */
    public void setDataatualizacao(Timestamp aDataatualizacao) {
        dataatualizacao = aDataatualizacao;
    }

    /**
     * Access method for verversao.
     *
     * @return the current value of verversao
     */
    public Verversao getVerversao() {
        return verversao;
    }

    /**
     * Setter method for verversao.
     *
     * @param aVerversao the new value for verversao
     */
    public void setVerversao(Verversao aVerversao) {
        verversao = aVerversao;
    }

    /**
     * Compares the key for this instance with another Verscript.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Verscript and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Verscript)) {
            return false;
        }
        Verscript that = (Verscript) other;
        if (this.getCodverscript() != that.getCodverscript()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Verscript.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Verscript)) return false;
        return this.equalKeys(other) && ((Verscript)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodverscript();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Verscript |");
        sb.append(" codverscript=").append(getCodverscript());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codverscript", Integer.valueOf(getCodverscript()));
        return ret;
    }

}
