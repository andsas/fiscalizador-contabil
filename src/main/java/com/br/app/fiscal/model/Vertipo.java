package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="VERTIPO")
public class Vertipo implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codvertipo";

    @Id
    @Column(name="CODVERTIPO", unique=true, nullable=false, precision=10)
    private int codvertipo;
    @Column(name="DESCVERTIPO", nullable=false, length=250)
    private String descvertipo;
    @OneToMany(mappedBy="vertipo")
    private Set<Verversao> verversao;

    /** Default constructor. */
    public Vertipo() {
        super();
    }

    /**
     * Access method for codvertipo.
     *
     * @return the current value of codvertipo
     */
    public int getCodvertipo() {
        return codvertipo;
    }

    /**
     * Setter method for codvertipo.
     *
     * @param aCodvertipo the new value for codvertipo
     */
    public void setCodvertipo(int aCodvertipo) {
        codvertipo = aCodvertipo;
    }

    /**
     * Access method for descvertipo.
     *
     * @return the current value of descvertipo
     */
    public String getDescvertipo() {
        return descvertipo;
    }

    /**
     * Setter method for descvertipo.
     *
     * @param aDescvertipo the new value for descvertipo
     */
    public void setDescvertipo(String aDescvertipo) {
        descvertipo = aDescvertipo;
    }

    /**
     * Access method for verversao.
     *
     * @return the current value of verversao
     */
    public Set<Verversao> getVerversao() {
        return verversao;
    }

    /**
     * Setter method for verversao.
     *
     * @param aVerversao the new value for verversao
     */
    public void setVerversao(Set<Verversao> aVerversao) {
        verversao = aVerversao;
    }

    /**
     * Compares the key for this instance with another Vertipo.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Vertipo and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Vertipo)) {
            return false;
        }
        Vertipo that = (Vertipo) other;
        if (this.getCodvertipo() != that.getCodvertipo()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Vertipo.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Vertipo)) return false;
        return this.equalKeys(other) && ((Vertipo)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodvertipo();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Vertipo |");
        sb.append(" codvertipo=").append(getCodvertipo());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codvertipo", Integer.valueOf(getCodvertipo()));
        return ret;
    }

}
