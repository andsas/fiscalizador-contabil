package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity(name="VERVERSAO")
public class Verversao implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codverversao";

    @Id
    @Column(name="CODVERVERSAO", unique=true, nullable=false, length=20)
    private String codverversao;
    @Column(name="DESCVERVERSAO", nullable=false, length=250)
    private String descverversao;
    @Column(name="DATALIBERACAO", nullable=false)
    private Timestamp dataliberacao;
    @Column(name="DATAATUALIZACAO")
    private Timestamp dataatualizacao;
    @OneToMany(mappedBy="verversao")
    private Set<Verscript> verscript;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODVERTIPO", nullable=false)
    private Vertipo vertipo;

    /** Default constructor. */
    public Verversao() {
        super();
    }

    /**
     * Access method for codverversao.
     *
     * @return the current value of codverversao
     */
    public String getCodverversao() {
        return codverversao;
    }

    /**
     * Setter method for codverversao.
     *
     * @param aCodverversao the new value for codverversao
     */
    public void setCodverversao(String aCodverversao) {
        codverversao = aCodverversao;
    }

    /**
     * Access method for descverversao.
     *
     * @return the current value of descverversao
     */
    public String getDescverversao() {
        return descverversao;
    }

    /**
     * Setter method for descverversao.
     *
     * @param aDescverversao the new value for descverversao
     */
    public void setDescverversao(String aDescverversao) {
        descverversao = aDescverversao;
    }

    /**
     * Access method for dataliberacao.
     *
     * @return the current value of dataliberacao
     */
    public Timestamp getDataliberacao() {
        return dataliberacao;
    }

    /**
     * Setter method for dataliberacao.
     *
     * @param aDataliberacao the new value for dataliberacao
     */
    public void setDataliberacao(Timestamp aDataliberacao) {
        dataliberacao = aDataliberacao;
    }

    /**
     * Access method for dataatualizacao.
     *
     * @return the current value of dataatualizacao
     */
    public Timestamp getDataatualizacao() {
        return dataatualizacao;
    }

    /**
     * Setter method for dataatualizacao.
     *
     * @param aDataatualizacao the new value for dataatualizacao
     */
    public void setDataatualizacao(Timestamp aDataatualizacao) {
        dataatualizacao = aDataatualizacao;
    }

    /**
     * Access method for verscript.
     *
     * @return the current value of verscript
     */
    public Set<Verscript> getVerscript() {
        return verscript;
    }

    /**
     * Setter method for verscript.
     *
     * @param aVerscript the new value for verscript
     */
    public void setVerscript(Set<Verscript> aVerscript) {
        verscript = aVerscript;
    }

    /**
     * Access method for vertipo.
     *
     * @return the current value of vertipo
     */
    public Vertipo getVertipo() {
        return vertipo;
    }

    /**
     * Setter method for vertipo.
     *
     * @param aVertipo the new value for vertipo
     */
    public void setVertipo(Vertipo aVertipo) {
        vertipo = aVertipo;
    }

    /**
     * Compares the key for this instance with another Verversao.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Verversao and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Verversao)) {
            return false;
        }
        Verversao that = (Verversao) other;
        Object myCodverversao = this.getCodverversao();
        Object yourCodverversao = that.getCodverversao();
        if (myCodverversao==null ? yourCodverversao!=null : !myCodverversao.equals(yourCodverversao)) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Verversao.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Verversao)) return false;
        return this.equalKeys(other) && ((Verversao)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        if (getCodverversao() == null) {
            i = 0;
        } else {
            i = getCodverversao().hashCode();
        }
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Verversao |");
        sb.append(" codverversao=").append(getCodverversao());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codverversao", getCodverversao());
        return ret;
    }

}
