package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="WF")
public class Wf implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codwf";

    @Id
    @Column(name="CODWF", unique=true, nullable=false, precision=10)
    private int codwf;
    @Column(name="DESCWF", nullable=false, length=250)
    private String descwf;
    @Column(name="OBSERVACOES")
    private String observacoes;
    @Column(name="DATAINICIAL")
    private Timestamp datainicial;
    @Column(name="DATAFINAL")
    private Timestamp datafinal;
    @OneToMany(mappedBy="wf")
    private Set<Wfent> wfent;

    /** Default constructor. */
    public Wf() {
        super();
    }

    /**
     * Access method for codwf.
     *
     * @return the current value of codwf
     */
    public int getCodwf() {
        return codwf;
    }

    /**
     * Setter method for codwf.
     *
     * @param aCodwf the new value for codwf
     */
    public void setCodwf(int aCodwf) {
        codwf = aCodwf;
    }

    /**
     * Access method for descwf.
     *
     * @return the current value of descwf
     */
    public String getDescwf() {
        return descwf;
    }

    /**
     * Setter method for descwf.
     *
     * @param aDescwf the new value for descwf
     */
    public void setDescwf(String aDescwf) {
        descwf = aDescwf;
    }

    /**
     * Access method for observacoes.
     *
     * @return the current value of observacoes
     */
    public String getObservacoes() {
        return observacoes;
    }

    /**
     * Setter method for observacoes.
     *
     * @param aObservacoes the new value for observacoes
     */
    public void setObservacoes(String aObservacoes) {
        observacoes = aObservacoes;
    }

    /**
     * Access method for datainicial.
     *
     * @return the current value of datainicial
     */
    public Timestamp getDatainicial() {
        return datainicial;
    }

    /**
     * Setter method for datainicial.
     *
     * @param aDatainicial the new value for datainicial
     */
    public void setDatainicial(Timestamp aDatainicial) {
        datainicial = aDatainicial;
    }

    /**
     * Access method for datafinal.
     *
     * @return the current value of datafinal
     */
    public Timestamp getDatafinal() {
        return datafinal;
    }

    /**
     * Setter method for datafinal.
     *
     * @param aDatafinal the new value for datafinal
     */
    public void setDatafinal(Timestamp aDatafinal) {
        datafinal = aDatafinal;
    }

    /**
     * Access method for wfent.
     *
     * @return the current value of wfent
     */
    public Set<Wfent> getWfent() {
        return wfent;
    }

    /**
     * Setter method for wfent.
     *
     * @param aWfent the new value for wfent
     */
    public void setWfent(Set<Wfent> aWfent) {
        wfent = aWfent;
    }

    /**
     * Compares the key for this instance with another Wf.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Wf and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Wf)) {
            return false;
        }
        Wf that = (Wf) other;
        if (this.getCodwf() != that.getCodwf()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Wf.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Wf)) return false;
        return this.equalKeys(other) && ((Wf)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodwf();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Wf |");
        sb.append(" codwf=").append(getCodwf());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codwf", Integer.valueOf(getCodwf()));
        return ret;
    }

}
