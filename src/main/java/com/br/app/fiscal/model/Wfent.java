package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="WFENT")
public class Wfent implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codwfent";

    @Id
    @Column(name="CODWFENT", unique=true, nullable=false, precision=10)
    private int codwfent;
    @Column(name="FORMULA")
    private String formula;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODWF", nullable=false)
    private Wf wf;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODCONFDICTAB", nullable=false)
    private Confdictab confdictab;

    /** Default constructor. */
    public Wfent() {
        super();
    }

    /**
     * Access method for codwfent.
     *
     * @return the current value of codwfent
     */
    public int getCodwfent() {
        return codwfent;
    }

    /**
     * Setter method for codwfent.
     *
     * @param aCodwfent the new value for codwfent
     */
    public void setCodwfent(int aCodwfent) {
        codwfent = aCodwfent;
    }

    /**
     * Access method for formula.
     *
     * @return the current value of formula
     */
    public String getFormula() {
        return formula;
    }

    /**
     * Setter method for formula.
     *
     * @param aFormula the new value for formula
     */
    public void setFormula(String aFormula) {
        formula = aFormula;
    }

    /**
     * Access method for wf.
     *
     * @return the current value of wf
     */
    public Wf getWf() {
        return wf;
    }

    /**
     * Setter method for wf.
     *
     * @param aWf the new value for wf
     */
    public void setWf(Wf aWf) {
        wf = aWf;
    }

    /**
     * Access method for confdictab.
     *
     * @return the current value of confdictab
     */
    public Confdictab getConfdictab() {
        return confdictab;
    }

    /**
     * Setter method for confdictab.
     *
     * @param aConfdictab the new value for confdictab
     */
    public void setConfdictab(Confdictab aConfdictab) {
        confdictab = aConfdictab;
    }

    /**
     * Compares the key for this instance with another Wfent.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Wfent and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Wfent)) {
            return false;
        }
        Wfent that = (Wfent) other;
        if (this.getCodwfent() != that.getCodwfent()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Wfent.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Wfent)) return false;
        return this.equalKeys(other) && ((Wfent)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodwfent();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Wfent |");
        sb.append(" codwfent=").append(getCodwfent());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codwfent", Integer.valueOf(getCodwfent()));
        return ret;
    }

}
