package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity(name="WFFILA")
public class Wffila implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codwffila";

    @Id
    @Column(name="CODWFFILA", unique=true, nullable=false, precision=10)
    private int codwffila;
    @Column(name="CODWF", nullable=false, precision=10)
    private int codwf;
    @Column(name="DESCWFFILA", nullable=false, length=250)
    private String descwffila;
    @Column(name="DATAINICIAL")
    private Timestamp datainicial;
    @Column(name="DATAFINAL")
    private Timestamp datafinal;
    @Column(name="NUMERO", precision=10)
    private int numero;
    @OneToMany(mappedBy="wffila")
    private Set<Wffilareg> wffilareg;
    @OneToMany(mappedBy="wffila")
    private Set<Wffilaseg> wffilaseg;

    /** Default constructor. */
    public Wffila() {
        super();
    }

    /**
     * Access method for codwffila.
     *
     * @return the current value of codwffila
     */
    public int getCodwffila() {
        return codwffila;
    }

    /**
     * Setter method for codwffila.
     *
     * @param aCodwffila the new value for codwffila
     */
    public void setCodwffila(int aCodwffila) {
        codwffila = aCodwffila;
    }

    /**
     * Access method for codwf.
     *
     * @return the current value of codwf
     */
    public int getCodwf() {
        return codwf;
    }

    /**
     * Setter method for codwf.
     *
     * @param aCodwf the new value for codwf
     */
    public void setCodwf(int aCodwf) {
        codwf = aCodwf;
    }

    /**
     * Access method for descwffila.
     *
     * @return the current value of descwffila
     */
    public String getDescwffila() {
        return descwffila;
    }

    /**
     * Setter method for descwffila.
     *
     * @param aDescwffila the new value for descwffila
     */
    public void setDescwffila(String aDescwffila) {
        descwffila = aDescwffila;
    }

    /**
     * Access method for datainicial.
     *
     * @return the current value of datainicial
     */
    public Timestamp getDatainicial() {
        return datainicial;
    }

    /**
     * Setter method for datainicial.
     *
     * @param aDatainicial the new value for datainicial
     */
    public void setDatainicial(Timestamp aDatainicial) {
        datainicial = aDatainicial;
    }

    /**
     * Access method for datafinal.
     *
     * @return the current value of datafinal
     */
    public Timestamp getDatafinal() {
        return datafinal;
    }

    /**
     * Setter method for datafinal.
     *
     * @param aDatafinal the new value for datafinal
     */
    public void setDatafinal(Timestamp aDatafinal) {
        datafinal = aDatafinal;
    }

    /**
     * Access method for numero.
     *
     * @return the current value of numero
     */
    public int getNumero() {
        return numero;
    }

    /**
     * Setter method for numero.
     *
     * @param aNumero the new value for numero
     */
    public void setNumero(int aNumero) {
        numero = aNumero;
    }

    /**
     * Access method for wffilareg.
     *
     * @return the current value of wffilareg
     */
    public Set<Wffilareg> getWffilareg() {
        return wffilareg;
    }

    /**
     * Setter method for wffilareg.
     *
     * @param aWffilareg the new value for wffilareg
     */
    public void setWffilareg(Set<Wffilareg> aWffilareg) {
        wffilareg = aWffilareg;
    }

    /**
     * Access method for wffilaseg.
     *
     * @return the current value of wffilaseg
     */
    public Set<Wffilaseg> getWffilaseg() {
        return wffilaseg;
    }

    /**
     * Setter method for wffilaseg.
     *
     * @param aWffilaseg the new value for wffilaseg
     */
    public void setWffilaseg(Set<Wffilaseg> aWffilaseg) {
        wffilaseg = aWffilaseg;
    }

    /**
     * Compares the key for this instance with another Wffila.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Wffila and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Wffila)) {
            return false;
        }
        Wffila that = (Wffila) other;
        if (this.getCodwffila() != that.getCodwffila()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Wffila.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Wffila)) return false;
        return this.equalKeys(other) && ((Wffila)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodwffila();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Wffila |");
        sb.append(" codwffila=").append(getCodwffila());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codwffila", Integer.valueOf(getCodwffila()));
        return ret;
    }

}
