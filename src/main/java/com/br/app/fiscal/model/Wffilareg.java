package com.br.app.fiscal.model;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="WFFILAREG")
public class Wffilareg implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codwffilareg";

    @Id
    @Column(name="CODWFFILAREG", unique=true, nullable=false, precision=10)
    private int codwffilareg;
    @Column(name="DESCWFFILAREG", nullable=false, length=250)
    private String descwffilareg;
    @Column(name="OBSERVACOES")
    private String observacoes;
    @Column(name="DATAENTRADA", nullable=false)
    private Timestamp dataentrada;
    @Column(name="DATASAIDA")
    private Timestamp datasaida;
    @Column(name="CODTABELA", nullable=false, length=60)
    private String codtabela;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODWFFILA", nullable=false)
    private Wffila wffila;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIOENTRADA")
    private Confusuario confusuario;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIOSAIDA")
    private Confusuario confusuario2;
    @ManyToOne
    @JoinColumn(name="CODCONFDICTAB")
    private Confdictab confdictab;

    /** Default constructor. */
    public Wffilareg() {
        super();
    }

    /**
     * Access method for codwffilareg.
     *
     * @return the current value of codwffilareg
     */
    public int getCodwffilareg() {
        return codwffilareg;
    }

    /**
     * Setter method for codwffilareg.
     *
     * @param aCodwffilareg the new value for codwffilareg
     */
    public void setCodwffilareg(int aCodwffilareg) {
        codwffilareg = aCodwffilareg;
    }

    /**
     * Access method for descwffilareg.
     *
     * @return the current value of descwffilareg
     */
    public String getDescwffilareg() {
        return descwffilareg;
    }

    /**
     * Setter method for descwffilareg.
     *
     * @param aDescwffilareg the new value for descwffilareg
     */
    public void setDescwffilareg(String aDescwffilareg) {
        descwffilareg = aDescwffilareg;
    }

    /**
     * Access method for observacoes.
     *
     * @return the current value of observacoes
     */
    public String getObservacoes() {
        return observacoes;
    }

    /**
     * Setter method for observacoes.
     *
     * @param aObservacoes the new value for observacoes
     */
    public void setObservacoes(String aObservacoes) {
        observacoes = aObservacoes;
    }

    /**
     * Access method for dataentrada.
     *
     * @return the current value of dataentrada
     */
    public Timestamp getDataentrada() {
        return dataentrada;
    }

    /**
     * Setter method for dataentrada.
     *
     * @param aDataentrada the new value for dataentrada
     */
    public void setDataentrada(Timestamp aDataentrada) {
        dataentrada = aDataentrada;
    }

    /**
     * Access method for datasaida.
     *
     * @return the current value of datasaida
     */
    public Timestamp getDatasaida() {
        return datasaida;
    }

    /**
     * Setter method for datasaida.
     *
     * @param aDatasaida the new value for datasaida
     */
    public void setDatasaida(Timestamp aDatasaida) {
        datasaida = aDatasaida;
    }

    /**
     * Access method for codtabela.
     *
     * @return the current value of codtabela
     */
    public String getCodtabela() {
        return codtabela;
    }

    /**
     * Setter method for codtabela.
     *
     * @param aCodtabela the new value for codtabela
     */
    public void setCodtabela(String aCodtabela) {
        codtabela = aCodtabela;
    }

    /**
     * Access method for wffila.
     *
     * @return the current value of wffila
     */
    public Wffila getWffila() {
        return wffila;
    }

    /**
     * Setter method for wffila.
     *
     * @param aWffila the new value for wffila
     */
    public void setWffila(Wffila aWffila) {
        wffila = aWffila;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Access method for confusuario2.
     *
     * @return the current value of confusuario2
     */
    public Confusuario getConfusuario2() {
        return confusuario2;
    }

    /**
     * Setter method for confusuario2.
     *
     * @param aConfusuario2 the new value for confusuario2
     */
    public void setConfusuario2(Confusuario aConfusuario2) {
        confusuario2 = aConfusuario2;
    }

    /**
     * Access method for confdictab.
     *
     * @return the current value of confdictab
     */
    public Confdictab getConfdictab() {
        return confdictab;
    }

    /**
     * Setter method for confdictab.
     *
     * @param aConfdictab the new value for confdictab
     */
    public void setConfdictab(Confdictab aConfdictab) {
        confdictab = aConfdictab;
    }

    /**
     * Compares the key for this instance with another Wffilareg.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Wffilareg and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Wffilareg)) {
            return false;
        }
        Wffilareg that = (Wffilareg) other;
        if (this.getCodwffilareg() != that.getCodwffilareg()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Wffilareg.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Wffilareg)) return false;
        return this.equalKeys(other) && ((Wffilareg)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodwffilareg();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Wffilareg |");
        sb.append(" codwffilareg=").append(getCodwffilareg());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codwffilareg", Integer.valueOf(getCodwffilareg()));
        return ret;
    }

}
