package com.br.app.fiscal.model;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity(name="WFFILASEG")
public class Wffilaseg implements Serializable {
    private static final long serialVersionUID = 1L;

    /** Primary key. */
    protected static final String PK = "codwffilaseg";

    @Id
    @Column(name="CODWFFILASEG", unique=true, nullable=false, precision=10)
    private int codwffilaseg;
    @Column(name="ACESSO", precision=5)
    private short acesso;
    @Column(name="MOVER", precision=5)
    private short mover;
    @Column(name="RETROCEDER", precision=5)
    private short retroceder;
    @Column(name="AVANCAR", precision=5)
    private short avancar;
    @ManyToOne(optional=false)
    @JoinColumn(name="CODWFFILA", nullable=false)
    private Wffila wffila;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIOALCADA")
    private Confusuarioalcada confusuarioalcada;
    @ManyToOne
    @JoinColumn(name="CODCONFTIPOUSUARIO")
    private Confusuariotipo confusuariotipo;
    @ManyToOne
    @JoinColumn(name="CODCONFUSUARIO")
    private Confusuario confusuario;

    /** Default constructor. */
    public Wffilaseg() {
        super();
    }

    /**
     * Access method for codwffilaseg.
     *
     * @return the current value of codwffilaseg
     */
    public int getCodwffilaseg() {
        return codwffilaseg;
    }

    /**
     * Setter method for codwffilaseg.
     *
     * @param aCodwffilaseg the new value for codwffilaseg
     */
    public void setCodwffilaseg(int aCodwffilaseg) {
        codwffilaseg = aCodwffilaseg;
    }

    /**
     * Access method for acesso.
     *
     * @return the current value of acesso
     */
    public short getAcesso() {
        return acesso;
    }

    /**
     * Setter method for acesso.
     *
     * @param aAcesso the new value for acesso
     */
    public void setAcesso(short aAcesso) {
        acesso = aAcesso;
    }

    /**
     * Access method for mover.
     *
     * @return the current value of mover
     */
    public short getMover() {
        return mover;
    }

    /**
     * Setter method for mover.
     *
     * @param aMover the new value for mover
     */
    public void setMover(short aMover) {
        mover = aMover;
    }

    /**
     * Access method for retroceder.
     *
     * @return the current value of retroceder
     */
    public short getRetroceder() {
        return retroceder;
    }

    /**
     * Setter method for retroceder.
     *
     * @param aRetroceder the new value for retroceder
     */
    public void setRetroceder(short aRetroceder) {
        retroceder = aRetroceder;
    }

    /**
     * Access method for avancar.
     *
     * @return the current value of avancar
     */
    public short getAvancar() {
        return avancar;
    }

    /**
     * Setter method for avancar.
     *
     * @param aAvancar the new value for avancar
     */
    public void setAvancar(short aAvancar) {
        avancar = aAvancar;
    }

    /**
     * Access method for wffila.
     *
     * @return the current value of wffila
     */
    public Wffila getWffila() {
        return wffila;
    }

    /**
     * Setter method for wffila.
     *
     * @param aWffila the new value for wffila
     */
    public void setWffila(Wffila aWffila) {
        wffila = aWffila;
    }

    /**
     * Access method for confusuarioalcada.
     *
     * @return the current value of confusuarioalcada
     */
    public Confusuarioalcada getConfusuarioalcada() {
        return confusuarioalcada;
    }

    /**
     * Setter method for confusuarioalcada.
     *
     * @param aConfusuarioalcada the new value for confusuarioalcada
     */
    public void setConfusuarioalcada(Confusuarioalcada aConfusuarioalcada) {
        confusuarioalcada = aConfusuarioalcada;
    }

    /**
     * Access method for confusuariotipo.
     *
     * @return the current value of confusuariotipo
     */
    public Confusuariotipo getConfusuariotipo() {
        return confusuariotipo;
    }

    /**
     * Setter method for confusuariotipo.
     *
     * @param aConfusuariotipo the new value for confusuariotipo
     */
    public void setConfusuariotipo(Confusuariotipo aConfusuariotipo) {
        confusuariotipo = aConfusuariotipo;
    }

    /**
     * Access method for confusuario.
     *
     * @return the current value of confusuario
     */
    public Confusuario getConfusuario() {
        return confusuario;
    }

    /**
     * Setter method for confusuario.
     *
     * @param aConfusuario the new value for confusuario
     */
    public void setConfusuario(Confusuario aConfusuario) {
        confusuario = aConfusuario;
    }

    /**
     * Compares the key for this instance with another Wffilaseg.
     *
     * @param other The object to compare to
     * @return True if other object is instance of class Wffilaseg and the key objects are equal
     */
    private boolean equalKeys(Object other) {
        if (this==other) {
            return true;
        }
        if (!(other instanceof Wffilaseg)) {
            return false;
        }
        Wffilaseg that = (Wffilaseg) other;
        if (this.getCodwffilaseg() != that.getCodwffilaseg()) {
            return false;
        }
        return true;
    }

    /**
     * Compares this instance with another Wffilaseg.
     *
     * @param other The object to compare to
     * @return True if the objects are the same
     */
    @Override
    public boolean equals(Object other) {
        if (!(other instanceof Wffilaseg)) return false;
        return this.equalKeys(other) && ((Wffilaseg)other).equalKeys(this);
    }

    /**
     * Returns a hash code for this instance.
     *
     * @return Hash code
     */
    @Override
    public int hashCode() {
        int i;
        int result = 17;
        i = getCodwffilaseg();
        result = 37*result + i;
        return result;
    }

    /**
     * Returns a debug-friendly String representation of this instance.
     *
     * @return String representation of this instance
     */
    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer("[Wffilaseg |");
        sb.append(" codwffilaseg=").append(getCodwffilaseg());
        sb.append("]");
        return sb.toString();
    }

    /**
     * Return all elements of the primary key.
     *
     * @return Map of key names to values
     */
    public Map<String, Object> getPrimaryKey() {
        Map<String, Object> ret = new LinkedHashMap<String, Object>(6);
        ret.put("codwffilaseg", Integer.valueOf(getCodwffilaseg()));
        return ret;
    }

}
