package com.br.app.fiscal.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.br.app.fiscal.model.Agagente;

@Repository
public interface AgagenteRepository extends JpaRepository<Agagente, Integer> {

}
