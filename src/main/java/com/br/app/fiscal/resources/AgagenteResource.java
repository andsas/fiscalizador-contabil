package com.br.app.fiscal.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.br.app.fiscal.domain.AgagenteDto;
import com.br.app.fiscal.model.Agagente;
import com.br.app.fiscal.services.AgagenteService;
import com.br.app.fiscal.services.converters.AgagenteConverter;

@RestController
@RequestMapping(value="/participantes")
public class AgagenteResource {
	
	@Autowired
	private AgagenteService service;
	
	@Autowired
	private AgagenteConverter converter;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<AgagenteDto>> findAll() {
		List<Agagente> list = service.findAll();
		List<AgagenteDto> listDto = list.stream().map(obj -> converter.ModelToDto(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Agagente> find(@PathVariable Integer id) {
		Agagente obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/page", method=RequestMethod.GET)
	public ResponseEntity<Page<AgagenteDto>> findPage(
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="nome") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		Page<Agagente> list = service.findPage(page, linesPerPage, orderBy, direction);
		Page<AgagenteDto> listDto = list.map(obj -> converter.ModelToDto(obj));
		return ResponseEntity.ok().body(listDto);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> save(@Valid @RequestBody AgagenteDto objDto) {
		Agagente obj = converter.DtoToModel(objDto);
		obj = service.save(objDto);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getCodagagente()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody AgagenteDto objDto, @PathVariable Integer id) {
		Agagente obj = converter.DtoToModel(objDto);
		obj.setCodagagente(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

}
