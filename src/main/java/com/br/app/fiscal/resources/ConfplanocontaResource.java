package com.br.app.fiscal.resources;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.br.app.fiscal.domain.ConfplanocontaDto;
import com.br.app.fiscal.model.Confplanoconta;
import com.br.app.fiscal.services.ConfplanocontaService;
import com.br.app.fiscal.services.converters.ConfplanocontaConverter;

@RestController
@RequestMapping(value="/plano_de_contas")
public class ConfplanocontaResource {
	
	@Autowired
	private ConfplanocontaService service;
	
	@Autowired
	private ConfplanocontaConverter converter;
	
	@RequestMapping(method=RequestMethod.GET)
	public ResponseEntity<List<ConfplanocontaDto>> findAll() {
		List<Confplanoconta> list = service.findAll();
		List<ConfplanocontaDto> listDto = list.stream().map(obj -> converter.ModelToDto(obj)).collect(Collectors.toList());
		return ResponseEntity.ok().body(listDto);
	}
	
	@RequestMapping(value="/{id}", method=RequestMethod.GET)
	public ResponseEntity<Confplanoconta> find(@PathVariable Integer id) {
		Confplanoconta obj = service.find(id);
		return ResponseEntity.ok().body(obj);
	}
	
	@RequestMapping(value="/page", method=RequestMethod.GET)
	public ResponseEntity<Page<ConfplanocontaDto>> findPage(
			@RequestParam(value="page", defaultValue="0") Integer page, 
			@RequestParam(value="linesPerPage", defaultValue="24") Integer linesPerPage, 
			@RequestParam(value="orderBy", defaultValue="nome") String orderBy, 
			@RequestParam(value="direction", defaultValue="ASC") String direction) {
		Page<Confplanoconta> list = service.findPage(page, linesPerPage, orderBy, direction);
		Page<ConfplanocontaDto> listDto = list.map(obj -> converter.ModelToDto(obj));
		return ResponseEntity.ok().body(listDto);
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(method=RequestMethod.POST)
	public ResponseEntity<Void> save(@Valid @RequestBody ConfplanocontaDto objDto) {
		Confplanoconta obj = converter.DtoToModel(objDto);
		obj = service.save(objDto);
		URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(obj.getCodconfplanoconta()).toUri();
		return ResponseEntity.created(uri).build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/{id}", method=RequestMethod.PUT)
	public ResponseEntity<Void> update(@Valid @RequestBody ConfplanocontaDto objDto, @PathVariable Integer id) {
		Confplanoconta obj = converter.DtoToModel(objDto);
		obj.setCodconfplanoconta(id);
		obj = service.update(obj);
		return ResponseEntity.noContent().build();
	}
	
	@PreAuthorize("hasAnyRole('ADMIN')")
	@RequestMapping(value="/{id}", method=RequestMethod.DELETE)
	public ResponseEntity<Void> delete(@PathVariable Integer id) {
		service.delete(id);
		return ResponseEntity.noContent().build();
	}

}
