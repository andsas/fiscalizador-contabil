package com.br.app.fiscal.security.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.br.app.fiscal.security.model.Authority;

public interface AuthorityRepository extends JpaRepository<Authority, String>{
	
	Authority findByName(String name);
	
}
