package com.br.app.fiscal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.br.app.fiscal.domain.AgagenteDto;
import com.br.app.fiscal.model.Agagente;
import com.br.app.fiscal.repositories.AgagenteRepository;
import com.br.app.fiscal.services.converters.AgagenteConverter;
import com.br.app.fiscal.services.exceptions.DataIntegrityException;
import com.br.app.fiscal.services.exceptions.ObjectNotFoundException;

@Service
public class AgagenteService {
	
	@Autowired
	private AgagenteRepository repository;
	
	@Autowired
	private AgagenteConverter converter;
	
	public Agagente find(Integer id) {
		Agagente obj = repository.findOne(id);
		if (obj == null) {
			throw new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Agagente.class.getName());
		}
		return obj;
	}
	
	public Agagente save(AgagenteDto objDto) {
		objDto.setCodagagente(null);
		return repository.save(converter.DtoToModel(objDto));
	}
	
	public Agagente update(Agagente objDto) {
		Agagente newObj = find(objDto.getCodagagente());
		return repository.save(newObj);
	}

	public void delete(Integer id) {
		find(id);
		try {
			repository.delete(id);
		}
		catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir uma categoria que possui produtos");
		}
	}
	
	public List<Agagente> findAll() {
		List<Agagente> lista = repository.findAll();
		System.out.println("Tamanho da lista: " + lista.size());
		return lista;
	}
	
	public Page<Agagente> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = new PageRequest(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repository.findAll(pageRequest);
	}

}
