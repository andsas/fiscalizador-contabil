package com.br.app.fiscal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.br.app.fiscal.domain.ConfemprDto;
import com.br.app.fiscal.model.Confempr;
import com.br.app.fiscal.repositories.ConfemprRepository;
import com.br.app.fiscal.services.converters.ConfemprConverter;
import com.br.app.fiscal.services.exceptions.DataIntegrityException;
import com.br.app.fiscal.services.exceptions.ObjectNotFoundException;

@Service
public class ConfemprService {
	
	@Autowired
	private ConfemprRepository repository;
	
	@Autowired
	private ConfemprConverter converter;
	
	public Confempr find(Integer id) {
		Confempr obj = repository.findOne(id);
		if (obj == null) {
			throw new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Confempr.class.getName());
		}
		return obj;
	}
	
	public Confempr save(ConfemprDto objDto) {
		objDto.setCodconfempr(null);
		return repository.save(converter.DtoToModel(objDto));
	}
	
	public Confempr update(Confempr objDto) {
		Confempr newObj = find(objDto.getCodconfempr());
		return repository.save(newObj);
	}

	public void delete(Integer id) {
		find(id);
		try {
			repository.delete(id);
		}
		catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir uma categoria que possui produtos");
		}
	}
	
	public List<Confempr> findAll() {
		List<Confempr> lista = repository.findAll();
		System.out.println("Tamanho da lista: " + lista.size());
		return lista;
	}
	
	public Page<Confempr> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = new PageRequest(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repository.findAll(pageRequest);
	}

}
