package com.br.app.fiscal.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;

import com.br.app.fiscal.domain.ConfplanocontaDto;
import com.br.app.fiscal.model.Confplanoconta;
import com.br.app.fiscal.repositories.ConfplanocontaRepository;
import com.br.app.fiscal.services.converters.ConfplanocontaConverter;
import com.br.app.fiscal.services.exceptions.DataIntegrityException;
import com.br.app.fiscal.services.exceptions.ObjectNotFoundException;

@Service
public class ConfplanocontaService {
	
	@Autowired
	private ConfplanocontaRepository repository;
	
	@Autowired
	private ConfplanocontaConverter converter;
	
	public Confplanoconta find(Integer id) {
		Confplanoconta obj = repository.findOne(id);
		if (obj == null) {
			throw new ObjectNotFoundException("Objeto não encontrado! Id: " + id + ", Tipo: " + Confplanoconta.class.getName());
		}
		return obj;
	}
	
	public Confplanoconta save(ConfplanocontaDto objDto) {
		objDto.setCodconfplanoconta(null);
		return repository.save(converter.DtoToModel(objDto));
	}
	
	public Confplanoconta update(Confplanoconta objDto) {
		Confplanoconta newObj = find(objDto.getCodconfplanoconta());
		return repository.save(newObj);
	}

	public void delete(Integer id) {
		find(id);
		try {
			repository.delete(id);
		}
		catch (DataIntegrityViolationException e) {
			throw new DataIntegrityException("Não é possível excluir uma categoria que possui produtos");
		}
	}
	
	public List<Confplanoconta> findAll() {
		List<Confplanoconta> lista = repository.findAll();
		System.out.println("Tamanho da lista: " + lista.size());
		return lista;
	}
	
	public Page<Confplanoconta> findPage(Integer page, Integer linesPerPage, String orderBy, String direction) {
		PageRequest pageRequest = new PageRequest(page, linesPerPage, Direction.valueOf(direction), orderBy);
		return repository.findAll(pageRequest);
	}

}
