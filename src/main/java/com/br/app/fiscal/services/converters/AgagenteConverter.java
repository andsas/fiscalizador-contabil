package com.br.app.fiscal.services.converters;

import org.springframework.stereotype.Service;

import com.br.app.fiscal.domain.AgagenteDto;
import com.br.app.fiscal.model.Agagente;

@Service
public class AgagenteConverter {
	
	public Agagente DtoToModel(AgagenteDto dto) {
		
		Agagente entity = new Agagente();
		entity.setCodagagente(dto.getCodagagente());
		entity.setNomeagagente(dto.getNomeagagente());
		entity.setCnpjcpf(dto.getCnpjcpf());
		entity.setIerg(dto.getIerg());
		entity.setInscmun(dto.getInscmun());
		entity.setNfeindfinal(dto.getNfeindfinal());
		entity.setNfeindiedest(dto.getNfeindiedest());
		entity.setInscsuframa(dto.getInscsuframa());
		entity.setIndoptributaria(dto.getIndoptributaria());
		entity.setIndenquadramento(dto.getIndenquadramento());
		entity.setIndtipoempresa(dto.getIndtipoempresa());
		entity.setInsccrc(dto.getInsccrc());
		entity.setIndsubsttrib(dto.getIndsubsttrib());
		entity.setIndtipocomercio(dto.getIndtipocomercio());
		entity.setPassnumero(dto.getPassnumero());
		entity.setPassemissao(dto.getPassemissao());
		entity.setPassvalidade(dto.getPassvalidade());
		entity.setEstrnumero(dto.getEstrnumero());
		entity.setEstrvalidade(dto.getEstrvalidade());
		entity.setCnhnumero(dto.getCnhnumero());
		entity.setCnhformulario(dto.getCnhformulario());
		entity.setCnhexpedicao(dto.getCnhexpedicao());
		entity.setCnhvalidade(dto.getCnhvalidade());
		entity.setCnhprimeirahab(dto.getCnhprimeirahab());
		entity.setEleitornumero(dto.getEleitornumero());
		entity.setEleitorzona(dto.getEleitorzona());
		entity.setEleitorsecao(dto.getEleitorsecao());
		entity.setEleitoremissao(dto.getEleitoremissao());
		entity.setReservnumero(dto.getReservnumero());
		entity.setReservlotacao(dto.getReservlotacao());
		entity.setReservemissao(dto.getReservemissao());
		entity.setDatanascabertura(dto.getDatanascabertura());
		entity.setDatasituacao(dto.getDatasituacao());
		entity.setAgtipo(dto.getAgtipo());
		entity.setFinplanoconta(dto.getFinplanoconta());
		entity.setFinplanoconta2(dto.getFinplanoconta2());
		entity.setFinfluxoplanoconta(dto.getFinfluxoplanoconta());
		entity.setFinfluxoplanoconta2(dto.getFinfluxoplanoconta2());
		entity.setOrcplanoconta(dto.getOrcplanoconta());
		entity.setOrcplanoconta2(dto.getOrcplanoconta2());
		entity.setAgvistotipo(dto.getAgvistotipo());
		entity.setAgagentetiporeceita(dto.getAgagentetiporeceita());
		entity.setFisnatjur(dto.getFisnatjur());
		entity.setAggrupo(dto.getAggrupo());
		entity.setCtbplanoconta(dto.getCtbplanoconta());
		entity.setCtbplanoconta2(dto.getCtbplanoconta2());
		entity.setAgagente7(dto.getAgagente7());
		entity.setAgagente6(dto.getAgagente6());
		entity.setAgagente5(dto.getAgagente5());
		entity.setAgagente4(dto.getAgagente4());
		entity.setCobrplano(dto.getCobrplano());
		entity.setOpoperacao(dto.getOpoperacao());
		entity.setPrtabela(dto.getPrtabela());
		entity.setAgagenteimpcnae(dto.getAgagenteimpcnae());
		entity.setAgagenteresp(dto.getAgagenteresp());
		entity.setAgagentetransp(dto.getAgagentetransp());
		entity.setAglimcredito(dto.getAglimcredito());
		entity.setAglimcreditosol(dto.getAglimcreditosol());
		entity.setComtabela(dto.getComtabela());
		entity.setConfempr(dto.getConfempr());
		entity.setConfemprpart(dto.getConfemprpart());
		entity.setContcontrato(dto.getContcontrato());
		entity.setContcontrato2(dto.getContcontrato2());
		entity.setCrmchamado(dto.getCrmchamado());
		entity.setCtbbem(dto.getCtbbem());
		entity.setCtbbemveicinfracao(dto.getCtbbemveicinfracao());
		entity.setCtbfechamento(dto.getCtbfechamento());
		entity.setCtbfechamento2(dto.getCtbfechamento2());
		entity.setEcarquivamento(dto.getEcarquivamento());
		entity.setEccertificado(dto.getEccertificado());
		entity.setEccertificado2(dto.getEccertificado2());
		entity.setEccertificado3(dto.getEccertificado3());
		entity.setEccheckagenda(dto.getEccheckagenda());
		entity.setEccheckstatus(dto.getEccheckstatus());
		entity.setEccheckstatusagente(dto.getEccheckstatusagente());
		entity.setEccnd(dto.getEccnd());
		entity.setEccnd2(dto.getEccnd2());
		entity.setEccorresplancamento(dto.getEccorresplancamento());
		entity.setEccorresplancamento2(dto.getEccorresplancamento2());
		entity.setEcdecore(dto.getEcdecore());
		entity.setEcdoclancamento(dto.getEcdoclancamento());
		entity.setEcpgtolancamento(dto.getEcpgtolancamento());
		entity.setFinagencia(dto.getFinagencia());
		entity.setFinagencia2(dto.getFinagencia2());
		entity.setFinconta(dto.getFinconta());
		entity.setFinemprestimo(dto.getFinemprestimo());
		entity.setFinemprestimo2(dto.getFinemprestimo2());
		entity.setFinlancamento(dto.getFinlancamento());
		entity.setFinoperadora(dto.getFinoperadora());
		entity.setFises(dto.getFises());
		entity.setFolcolaborador(dto.getFolcolaborador());
		entity.setFolcolaborador2(dto.getFolcolaborador2());
		entity.setFolcolaborador3(dto.getFolcolaborador3());
		entity.setFolcolaboradordep(dto.getFolcolaboradordep());
		entity.setFolconvenio(dto.getFolconvenio());
		entity.setFolconveniotipo(dto.getFolconveniotipo());
		entity.setFolsindicato(dto.getFolsindicato());
		entity.setGerimovel(dto.getGerimovel());
		entity.setGerveiculo(dto.getGerveiculo());
		entity.setOporcdetag(dto.getOporcdetag());
		entity.setOptransacao(dto.getOptransacao());
		entity.setPrdcplano(dto.getPrdcplano());
		entity.setPrdcplanoetapa(dto.getPrdcplanoetapa());
		entity.setProdapolice(dto.getProdapolice());
		entity.setProdapolice2(dto.getProdapolice2());
		entity.setProdmarca(dto.getProdmarca());
		return entity;
		
	}
	
	public AgagenteDto ModelToDto(Agagente entity) {

		AgagenteDto dto = new AgagenteDto();
		dto.setCodagagente(entity.getCodagagente());
		dto.setNomeagagente(entity.getNomeagagente());
		dto.setCnpjcpf(entity.getCnpjcpf());
		dto.setIerg(entity.getIerg());
		dto.setInscmun(entity.getInscmun());
		dto.setNfeindfinal(entity.getNfeindfinal());
		dto.setNfeindiedest(entity.getNfeindiedest());
		dto.setInscsuframa(entity.getInscsuframa());
		dto.setIndoptributaria(entity.getIndoptributaria());
		dto.setIndenquadramento(entity.getIndenquadramento());
		dto.setIndtipoempresa(entity.getIndtipoempresa());
		dto.setInsccrc(entity.getInsccrc());
		dto.setIndsubsttrib(entity.getIndsubsttrib());
		dto.setIndtipocomercio(entity.getIndtipocomercio());
		dto.setPassnumero(entity.getPassnumero());
		dto.setPassemissao(entity.getPassemissao());
		dto.setPassvalidade(entity.getPassvalidade());
		dto.setEstrnumero(entity.getEstrnumero());
		dto.setEstrvalidade(entity.getEstrvalidade());
		dto.setCnhnumero(entity.getCnhnumero());
		dto.setCnhformulario(entity.getCnhformulario());
		dto.setCnhexpedicao(entity.getCnhexpedicao());
		dto.setCnhvalidade(entity.getCnhvalidade());
		dto.setCnhprimeirahab(entity.getCnhprimeirahab());
		dto.setEleitornumero(entity.getEleitornumero());
		dto.setEleitorzona(entity.getEleitorzona());
		dto.setEleitorsecao(entity.getEleitorsecao());
		dto.setEleitoremissao(entity.getEleitoremissao());
		dto.setReservnumero(entity.getReservnumero());
		dto.setReservlotacao(entity.getReservlotacao());
		dto.setReservemissao(entity.getReservemissao());
		dto.setDatanascabertura(entity.getDatanascabertura());
		dto.setDatasituacao(entity.getDatasituacao());
		dto.setAgtipo(entity.getAgtipo());
		dto.setFinplanoconta(entity.getFinplanoconta());
		dto.setFinplanoconta2(entity.getFinplanoconta2());
		dto.setFinfluxoplanoconta(entity.getFinfluxoplanoconta());
		dto.setFinfluxoplanoconta2(entity.getFinfluxoplanoconta2());
		dto.setOrcplanoconta(entity.getOrcplanoconta());
		dto.setOrcplanoconta2(entity.getOrcplanoconta2());
		dto.setAgvistotipo(entity.getAgvistotipo());
		dto.setAgagentetiporeceita(entity.getAgagentetiporeceita());
		dto.setFisnatjur(entity.getFisnatjur());
		dto.setAggrupo(entity.getAggrupo());
		dto.setCtbplanoconta(entity.getCtbplanoconta());
		dto.setCtbplanoconta2(entity.getCtbplanoconta2());
		dto.setAgagente7(entity.getAgagente7());
		dto.setAgagente6(entity.getAgagente6());
		dto.setAgagente5(entity.getAgagente5());
		dto.setAgagente4(entity.getAgagente4());
		dto.setCobrplano(entity.getCobrplano());
		dto.setOpoperacao(entity.getOpoperacao());
		dto.setPrtabela(entity.getPrtabela());
		dto.setAgagenteimpcnae(entity.getAgagenteimpcnae());
		dto.setAgagenteresp(entity.getAgagenteresp());
		dto.setAgagentetransp(entity.getAgagentetransp());
		dto.setAglimcredito(entity.getAglimcredito());
		dto.setAglimcreditosol(entity.getAglimcreditosol());
		dto.setComtabela(entity.getComtabela());
		dto.setConfempr(entity.getConfempr());
		dto.setConfemprpart(entity.getConfemprpart());
		dto.setContcontrato(entity.getContcontrato());
		dto.setContcontrato2(entity.getContcontrato2());
		dto.setCrmchamado(entity.getCrmchamado());
		dto.setCtbbem(entity.getCtbbem());
		dto.setCtbbemveicinfracao(entity.getCtbbemveicinfracao());
		dto.setCtbfechamento(entity.getCtbfechamento());
		dto.setCtbfechamento2(entity.getCtbfechamento2());
		dto.setEcarquivamento(entity.getEcarquivamento());
		dto.setEccertificado(entity.getEccertificado());
		dto.setEccertificado2(entity.getEccertificado2());
		dto.setEccertificado3(entity.getEccertificado3());
		dto.setEccheckagenda(entity.getEccheckagenda());
		dto.setEccheckstatus(entity.getEccheckstatus());
		dto.setEccheckstatusagente(entity.getEccheckstatusagente());
		dto.setEccnd(entity.getEccnd());
		dto.setEccnd2(entity.getEccnd2());
		dto.setEccorresplancamento(entity.getEccorresplancamento());
		dto.setEccorresplancamento2(entity.getEccorresplancamento2());
		dto.setEcdecore(entity.getEcdecore());
		dto.setEcdoclancamento(entity.getEcdoclancamento());
		dto.setEcpgtolancamento(entity.getEcpgtolancamento());
		dto.setFinagencia(entity.getFinagencia());
		dto.setFinagencia2(entity.getFinagencia2());
		dto.setFinconta(entity.getFinconta());
		dto.setFinemprestimo(entity.getFinemprestimo());
		dto.setFinemprestimo2(entity.getFinemprestimo2());
		dto.setFinlancamento(entity.getFinlancamento());
		dto.setFinoperadora(entity.getFinoperadora());
		dto.setFises(entity.getFises());
		dto.setFolcolaborador(entity.getFolcolaborador());
		dto.setFolcolaborador2(entity.getFolcolaborador2());
		dto.setFolcolaborador3(entity.getFolcolaborador3());
		dto.setFolcolaboradordep(entity.getFolcolaboradordep());
		dto.setFolconvenio(entity.getFolconvenio());
		dto.setFolconveniotipo(entity.getFolconveniotipo());
		dto.setFolsindicato(entity.getFolsindicato());
		dto.setGerimovel(entity.getGerimovel());
		dto.setGerveiculo(entity.getGerveiculo());
		dto.setOporcdetag(entity.getOporcdetag());
		dto.setOptransacao(entity.getOptransacao());
		dto.setPrdcplano(entity.getPrdcplano());
		dto.setPrdcplanoetapa(entity.getPrdcplanoetapa());
		dto.setProdapolice(entity.getProdapolice());
		dto.setProdapolice2(entity.getProdapolice2());
		dto.setProdmarca(entity.getProdmarca());
		return dto;
		
	}

}
