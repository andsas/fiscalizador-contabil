package com.br.app.fiscal.services.converters;

import org.springframework.stereotype.Service;

import com.br.app.fiscal.domain.ConfemprDto;
import com.br.app.fiscal.model.Confempr;

@Service
public class ConfemprConverter {
	
	public Confempr DtoToModel(ConfemprDto dto) {
		
		Confempr entity = new Confempr();		
		entity.setCodconfempr(dto.getCodconfempr());
		entity.setNomeconfempr(dto.getNomeconfempr());
		entity.setNomefantasia(dto.getNomefantasia());
		entity.setInscest(dto.getInscest());
		entity.setCnpj(dto.getCnpj());
		entity.setNfecertificado(dto.getNfecertificado());
		entity.setNfeenviopath(dto.getNfeenviopath());
		entity.setNfecancpath(dto.getNfecancpath());
		entity.setNfeinupath(dto.getNfeinupath());
		entity.setNfeconspath(dto.getNfeconspath());
		entity.setLogo(dto.getLogo());
		entity.setNfeambiente(dto.getNfeambiente());
		entity.setTipoconfpaiempr(dto.getTipoconfpaiempr());
		entity.setEmailhost(dto.getEmailhost());
		entity.setEmailnome(dto.getEmailnome());
		entity.setEmaillogin(dto.getEmaillogin());
		entity.setEmailsenha(dto.getEmailsenha());
		entity.setEmailssl(dto.getEmailssl());
		entity.setEmailseg(dto.getEmailseg());
		entity.setEmailcopia(dto.getEmailcopia());
		entity.setEmailporta(dto.getEmailporta());
		entity.setNfsecertificado(dto.getNfsecertificado());
		entity.setNfseenviopath(dto.getNfseenviopath());
		entity.setNfseambiente(dto.getNfseambiente());
		entity.setCormenu(dto.getCormenu());
		entity.setAgconf(dto.getAgconf());
		entity.setComtabela(dto.getComtabela());
		entity.setAgagente(dto.getAgagente());
		entity.setConfempr3(dto.getConfempr3());
		entity.setConfempr2(dto.getConfempr2());
		entity.setConfemprpart(dto.getConfemprpart());
		entity.setConfplanoconta(dto.getConfplanoconta());
		entity.setCtbbem(dto.getCtbbem());
		entity.setCtbfechamento(dto.getCtbfechamento());
		entity.setCtblancamento(dto.getCtblancamento());
		entity.setCtbplanoconta(dto.getCtbplanoconta());
		entity.setEcconf(dto.getEcconf());
		entity.setEcdoclancamento(dto.getEcdoclancamento());
		entity.setFincaixa(dto.getFincaixa());
		entity.setFinconf(dto.getFinconf());
		entity.setFinfluxoplanoconta(dto.getFinfluxoplanoconta());
		entity.setFinplanoconta(dto.getFinplanoconta());
		entity.setFolcolaborador(dto.getFolcolaborador());
		entity.setFolhorariotrabalho(dto.getFolhorariotrabalho());
		entity.setImpoptributaria(dto.getImpoptributaria());
		entity.setImpplanofiscal(dto.getImpplanofiscal());
		entity.setOpconfentrada(dto.getOpconfentrada());
		entity.setOpconfsaida(dto.getOpconfsaida());
		entity.setOporc(dto.getOporc());
		entity.setOpsolicitacao(dto.getOpsolicitacao());
		entity.setOrcplanoconta(dto.getOrcplanoconta());		
		return entity;
		
	}
	
	public ConfemprDto ModelToDto(Confempr entity) {
		
		ConfemprDto dto = new ConfemprDto();
		dto.setCodconfempr(entity.getCodconfempr());
		dto.setNomeconfempr(dto.getNomeconfempr());
		dto.setNomefantasia(entity.getNomefantasia());
		dto.setInscest(entity.getInscest());
		dto.setCnpj(entity.getCnpj());
		dto.setNfecertificado(entity.getNfecertificado());
		dto.setNfeenviopath(entity.getNfeenviopath());
		dto.setNfecancpath(entity.getNfecancpath());
		dto.setNfeinupath(entity.getNfeinupath());
		dto.setNfeconspath(entity.getNfeconspath());
		dto.setLogo(entity.getLogo());
		dto.setNfeambiente(entity.getNfeambiente());
		dto.setTipoconfpaiempr(entity.getTipoconfpaiempr());
		dto.setEmailhost(entity.getEmailhost());
		dto.setEmailnome(entity.getEmailnome());
		dto.setEmaillogin(entity.getEmaillogin());
		dto.setEmailsenha(entity.getEmailsenha());
		dto.setEmailssl(entity.getEmailssl());
		dto.setEmailseg(entity.getEmailseg());
		dto.setEmailcopia(entity.getEmailcopia());
		dto.setEmailporta(entity.getEmailporta());
		dto.setNfsecertificado(entity.getNfsecertificado());
		dto.setNfseenviopath(entity.getNfseenviopath());
		dto.setNfseambiente(entity.getNfseambiente());
		dto.setCormenu(entity.getCormenu());
		dto.setAgconf(entity.getAgconf());
		dto.setComtabela(entity.getComtabela());
		dto.setAgagente(entity.getAgagente());
		dto.setConfempr3(entity.getConfempr3());
		dto.setConfempr2(entity.getConfempr2());
		dto.setConfemprpart(entity.getConfemprpart());
		dto.setConfplanoconta(entity.getConfplanoconta());
		dto.setCtbbem(entity.getCtbbem());
		dto.setCtbfechamento(entity.getCtbfechamento());
		dto.setCtblancamento(entity.getCtblancamento());
		dto.setCtbplanoconta(entity.getCtbplanoconta());
		dto.setEcconf(entity.getEcconf());
		dto.setEcdoclancamento(entity.getEcdoclancamento());
		dto.setFincaixa(entity.getFincaixa());
		dto.setFinconf(entity.getFinconf());
		dto.setFinfluxoplanoconta(entity.getFinfluxoplanoconta());
		dto.setFinplanoconta(entity.getFinplanoconta());
		dto.setFolcolaborador(entity.getFolcolaborador());
		dto.setFolhorariotrabalho(entity.getFolhorariotrabalho());
		dto.setImpoptributaria(entity.getImpoptributaria());
		dto.setImpplanofiscal(entity.getImpplanofiscal());
		dto.setOpconfentrada(entity.getOpconfentrada());
		dto.setOpconfsaida(entity.getOpconfsaida());
		dto.setOporc(entity.getOporc());
		dto.setOpsolicitacao(entity.getOpsolicitacao());
		dto.setOrcplanoconta(entity.getOrcplanoconta());			
		return dto;
		
	}

}
