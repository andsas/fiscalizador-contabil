package com.br.app.fiscal.services.converters;

import org.springframework.stereotype.Service;

import com.br.app.fiscal.domain.ConfplanocontaDto;
import com.br.app.fiscal.model.Confplanoconta;

@Service
public class ConfplanocontaConverter {
	
	public Confplanoconta DtoToModel(ConfplanocontaDto dto) {
		
		Confplanoconta entity = new Confplanoconta();
		entity.setCodconfplanoconta(dto.getCodconfplanoconta());
		entity.setTipoconfplanoconta(dto.getTipoconfplanoconta());
		entity.setMascara(dto.getMascara());
		entity.setMascstr(dto.getMascstr());
		entity.setNiveis(dto.getNiveis());
		entity.setNivel1(dto.getNivel1());
		entity.setNivel2(dto.getNivel2());
		entity.setNivel3(dto.getNivel3());
		entity.setNivel4(dto.getNivel4());
		entity.setNivel5(dto.getNivel5());
		entity.setNivel6(dto.getNivel6());
		entity.setNivel7(dto.getNivel7());
		entity.setNivel8(dto.getNivel8());
		entity.setNivel9(dto.getNivel9());
		entity.setConfempr(dto.getConfempr());
		return entity;
		
	}
	
	public ConfplanocontaDto ModelToDto(Confplanoconta entity) {
		
		ConfplanocontaDto dto = new ConfplanocontaDto();		
		dto.setCodconfplanoconta(entity.getCodconfplanoconta());
		dto.setTipoconfplanoconta(entity.getTipoconfplanoconta());
		dto.setMascara(entity.getMascara());
		dto.setMascstr(entity.getMascstr());
		dto.setNiveis(entity.getNiveis());
		dto.setNivel1(entity.getNivel1());
		dto.setNivel2(entity.getNivel2());
		dto.setNivel3(entity.getNivel3());
		dto.setNivel4(entity.getNivel4());
		dto.setNivel5(entity.getNivel5());
		dto.setNivel6(entity.getNivel6());
		dto.setNivel7(entity.getNivel7());
		dto.setNivel8(entity.getNivel8());
		dto.setNivel9(entity.getNivel9());
		dto.setConfempr(entity.getConfempr());
		return dto;
		
	}

}
